# japanese_grammar_guide
 
## Credit

This application is a Flutter port of Tae Kim’s ‘A Guide to Japanese Grammar’ ([CC BY-NC-SA 3.0 US](https://creativecommons.org/licenses/by-nc-sa/3.0/us/)), which can be found online [here](https://creativecommons.org/licenses/by-nc-sa/3.0/us/).

## Changes Made

Minor alterations have been made to the grammar guide, including:
- The correction of typographical errors
- The addition of tooltips for Japanese vocabulary in later lessons
- The reformatting of certain content for better clarityThe updating of expired links

As this app is a work in progress, some copying errors may remain.

## Features

- Switch between light and dark themes
- Find lessons using search
- Save lesson sections for future reference with bookmarks

## License

This work is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Source Code

This app is open source and its source code can be found [here](https://gitlab.com/ibasho1/japanese-grammar-guide).