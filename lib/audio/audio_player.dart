import 'package:audioplayers/audioplayers.dart';

class Player {
  static final AudioPlayer _player = AudioPlayer();

  static Future playAudio(String path) async {
    await _player.play(AssetSource('audio/$path'));
  }
}
