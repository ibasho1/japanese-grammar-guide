import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/preferences/preferences.dart';

class BookmarksManager with ChangeNotifier{
  List<List<int>> _bookmarks;

  BookmarksManager(this._bookmarks);

  get bookmarks => _bookmarks;

  loadBookmarks() {
    _bookmarks = Preferences.getBookmarks();
  }

  updateBookmarks() {
    notifyListeners();
  }
}