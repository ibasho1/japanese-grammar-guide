import 'package:flutter/material.dart';

const kDefaultPagePadding = EdgeInsets.only(left: 10, right: 10, bottom: 50);
const kDefaultHeading3Padding = EdgeInsets.only(top: 15);
const kDefaultHeading2Padding = EdgeInsets.only(top: 20);
const kDefaultSectionPadding = EdgeInsets.only(top: 40);
const kDefaultListPadding = EdgeInsets.only(top: 10, left: 15);
const kDefaultListItemPadding = EdgeInsets.only(left: 5);
const kDefaultParagraphPadding = EdgeInsets.only(top: 10);
const kDefaultTableCellPadding = EdgeInsets.all(5);
const kDefaultNoteSectionPadding = EdgeInsets.symmetric(vertical: 20, horizontal: 10);