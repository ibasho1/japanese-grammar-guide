const String shizuka = 'しずか - quiet';

const String genki = 'げんき - healthy; lively';

const String gakusei = 'がくせい - student';

const String watashi = 'わたし - me, myself, I';

const String un = 'うん - yeah, uh huh';

const String dare = 'だれ - who';

const String kyou = 'きょう - today';

const String ashita = 'あした - tomorrow';

const String shiken = 'しけん - exam';

const String hito = 'ひと - person';

const String kirei = 'きれい - pretty; clean';

const String tomodachi = 'ともだち - friend';

const String shinsetsu = 'しんせつ - kind, friendly';

const String sakana = 'さかな - fish';

const String niku = 'にく - meat';

const String yasai = 'やさい - vegetables';

const String tabemono = 'たべもの - food';

const String oishii = 'おいしい - tasty';

const String takai = 'たかい - high; tall; expensive';

const String kirai = 'きらい - distasteful, hateful';

const String kirau = 'きらう - to hate';

const String suki = 'すき - like';

const String biru = 'ビル - building';

const String nedan = 'ねだん - price';

const String resutoran = 'レストラン - restaurant';

const String amari = 'あまり - not very (when used with negative)';

const String anmari = 'あんまり - not very (when used with negative)';

const String yoi = 'よい - good';

const String ii = 'いい - good';

const String kakkoii = 'かっこいい - good-looking, cool';

const String kakkou = 'かっこう - looks';

const String kare = 'かれ - he; boyfriend';

const String taberu = 'たべる - to eat';

const String kuru = 'くる - to come';

const String suru = 'する - to do';

const String aru = 'ある - to exist (inanimate)';

const String kau = 'かう - to buy';

const String iru = 'いる- to exist (animate)';

const String iruNeed = 'いる - to need';

const String aseru = 'あせる - to be in a hurry';

const String azakeru = 'あざける - to ridicule';

const String kaeru = 'かえる - to go home';

const String kagiru = 'かぎる - to limit';

const String kutsugaeru = 'くつがえる - to overturn';

const String kiru = 'きる - to cut';

const String keru = 'ける - to kick';

const String saegiru = 'さえぎる - to interrupt';

const String shaberu = 'しゃべる - to talk';

const String suberu = 'すべる - to be slippery';

const String nonoshiru = 'ののしる - to abuse verbally';

const String shiru = 'しる - to know';

const String nigiru = 'にぎる - to grasp';

const String hineru = 'ひねる - to twist';

const String hairu = 'はいる - to enter';

const String neru = 'ねる - to knead';

const String hirugaeru = 'ひるがえる - to turn over; to wave';

const String hashiru = 'はしる - to run';

const String mairu = 'まいる (hum.)  - to go; to come';

const String meiru = 'めいる - to feel depressed';

const String heru = 'へる - to decrease';

const String majiru = 'まじる - to mingle';

const String yomigaeru = 'よみがえる - to be resurrected';

const String asobu = 'あそぶ - to play';

const String deru = 'でる - to come out';

const String suteru = 'すてる - to throw away';

const String iku = 'いく - to go';

const String hanasu = 'はなす - to speak';

const String kaku = 'かく - to write';

const String oyogu = 'およぐ - to swim';

const String nomu = 'のむ - to drink';

const String shinu = 'しぬ - to die';

const String motsu = 'もつ - to hold';

const String okane = 'おかね - money';

const String neko = 'ねこ - cat';

const String gohan = 'ごはん - meal';

const String eiga = 'えいが - movie';

const String zenbu = 'ぜんぶ - all';

const String benkyou = 'べんきょう - study';

const String juusu = 'ジュース - juice';

const String machi = 'まち - town';

const String burabura = 'ぶらぶら - wandering; aimlessly';

const String kousoku = 'こうそく - high-speed';

const String douro = 'どうろ - route';

const String aruku = 'あるく - to walk';

const String mainichi = 'まいにち - everyday';

const String nihongo = 'にほんご - Japanese language';

const String meeruadoresu = 'メールアドレス - email address';

const String touroku = 'とうろく - register';

const String nihon = 'にほん - Japan';

const String ie = 'いえ - house; うち - home';

const String heya = 'へや - room';

const String amerika = 'アメリカ - America';

const String shukudai = 'しゅくだい - homework';

const String isu = 'いす - chair';

const String daidokoro = 'だいどころ - kitchen';

const String isha = 'いしゃ - doctor';

const String senshuu = 'せんしゅう - last week';

const String au = 'あう - to meet';

const String auMatch = 'あう - to match';

const String naru = 'なる - to become';

const String toshokan = 'としょかん - library';

const String rainen = 'らいねん - next year';

const String kachi = 'かち - victory';

const String mukau = 'むかう - to face; to go towards';

const String eigakan = 'えいがかん - movie theatre';

const String basu = 'バス - bus';

const String hirugohan = 'ひるごはん - lunch';

const String miru = 'みる - to see';

const String miruWatch = 'みる - to watch';

const String nani = 'なに - what';

const String nande = 'なんで - why';

const String hima = 'ひま -  having nothing to do';

const String gakkou = 'がっこう - school';

const String doko = 'どこ - where';

const String dou = 'どう - how';

const String itaria = 'イタリア - Italy';

const String narau = 'ならう - to learn';

const String booru = 'ボール - ball';

const String ochiru = 'おちる - to be dropped';

const String otosu = 'おとす - to drop';

const String bako = 'はこ - box';

const String ireru = 'いれる - to insert';

const String dasu = 'だす - to take out';

const String akeru = 'あける - to open';

const String shimeru = 'しめる - to close';

const String tsukeru = 'つける - to attach';

const String tsukeruAttachTurnOn = 'つける - to attach; to turn on';

const String kesu = 'けす - to erase';

const String nuku = 'ぬく - to extract';

const String aku = 'あく - to be opened';

const String shimaru = 'しまる - to be closed';

const String tsuku = 'つく - to be attached';

const String kieru = 'きえる - to disappear';

const String nukeru = 'ぬける - to be extracted';

const String denki = 'でんき - electricity, (electric) light';

const String mado = 'まど - window';

const String doushite = 'どうして - why';

const String kokusai = 'こくさい - international';

const String kyouiku = 'きょういく - education';

const String senta = 'センタ - center';

const String sentaa = 'センター - center';

const String toujoujinbutsu = 'とうじょうじんぶつ - cast of characters';

const String tachiirikinshi = 'たちいりきんし - no trespassing';

const String tsuukinteate = 'つうきんてあて - commuting allowance';

const String kodomo = 'こども - child';

const String rippa = 'りっぱ - fine, elegant';

const String otona = 'おとな - adult';

const String shigoto = 'しごと - job';

const String yameru = 'やめる - to quit';

const String yameruStop = 'やめる - to stop';

const String yameruStopQuit = 'やめる - to stop; to quit';

const String itsumo = 'いつも - always';

const String akai = 'あかい - red';

const String zubon = 'ズボン - pants';

const String bangohan = 'ばんごはん - dinner';

const String ginkou = 'ぎんこう - bank';

const String kouen = 'こうえん - park';

const String obentou = 'おべんとう - box lunch';

const String naifu = 'ナイフ - knife';

const String fooku = 'フォーク - fork';

const String suteeki = 'ステーキ - steak';

const String hon = 'ほん - book';

const String zasshi = 'ざっし - magazine';

const String hagaki = 'はがき - postcard';

const String sensei = 'せんせい - teacher';

const String nomimono = 'のみもの - beverage';

const String kappu = 'カップ - cup';

const String napukin = 'ナプキン - napkin';

const String kutsu = 'くつ - shoes';

const String shatsu = 'シャツ - shirt';

const String daigaku = 'だいがく - college';

const String sono = 'その - that';

const String sore = 'それ - that';

const String kono = 'この - this';

const String kore = 'これ - this';

const String ano = 'あの - that (over there)';

const String anoHey = 'あの - hey';

const String are = 'あれ - that (over there)';

const String shiroi = 'しろい - white';

const String kawaii = 'かわいい - cute';

const String jugyou = 'じゅぎょう - class';

const String wasureru = 'わすれる - to forget';

const String mono = 'もの - object';

const String koto = 'こと - event, matter';

const String taihen = 'たいへん - tough, hard time';

const String onaji = 'おなじ - same';

const String omoshiroi = 'おもしろい - interesting';

const String ima = 'いま - now';

const String isogashii = 'いそがしい - busy';

const String osoi = 'おそい - late';

const String uun = 'ううん - no';

const String asagohan = 'あさごはん - breakfast';

const String hayai = 'はやい - fast, early';

const String jibun = 'じぶん - oneself';

const String zenzen = 'ぜんぜん - completely, not at all';

const String takusan = 'たくさん - a lot';

const String saikin = 'さいきん - lately';

const String koe = 'こえ - voice';

const String kekkou = 'けっこう - fairly';

const String ookii = 'おおきい - large';

const String kawaru = 'かわる - to change';

const String naka = 'なか - inside';

const String tenki = 'てんき - weather';

const String sou = 'そう - that is so';

const String jikan = 'じかん - time';

const String daijoubu = 'だいじょうぶ - ok';

const String demo = 'でも - but';

const String ame = 'あめ - rain';

const String furu = 'ふる - to precipitate';

const String teineigo = 'ていねいご - polite language';

const String sonkeigo = 'そんけいご - honorific language';

const String kenjougo = 'けんじょうご - humble language';

const String hai = 'はい - yes';

const String iie = 'いいえ - no';

const String ikaru = 'いかる - to get angry';

const String tekken = 'てっけん - fist';

const String okoru = 'おこる - to get angry';

const String yasumi = 'やすみ - rest, vacation';

const String kinou = 'きのう - yesterday';

const String tanoshimu = 'たのしむ - to enjoy';

const String kirikaeru = 'きりかえる - to switch';

const String tsukekuwaeru = 'つけくわえる - to add one thing to another';

const String iidasu = 'いいだす - to start talking';

const String hashiridasu = 'はしりだす - to break into a run';

const String koinu = 'こいぬ - puppy';

const String totemo = 'とても - very';

const String omou = 'おもう - to think';

const String kotaeru = 'こたえる - to answer';

const String watakushi = 'わたくし - me, myself, I';

const String boku = 'ぼく - me, myself, I';

const String ore = 'おれ - me, myself, I';

const String atashi = 'あたし - me, myself, I';

const String washi = 'わし - me, myself, I';

const String namae = 'なまえ - name';

const String shachou = 'しゃちょう - company president';

const String kachou = 'かちょう - section manager';

const String anata = 'あなた - you';

const String kimi = 'きみ - you';

const String omae = 'おまえ - you';

const String omee = 'おめえ - you';

const String anta = 'あんた - you';

const String temae = 'てまえ - you';

const String temee = 'てめえ - you';

const String kisama = 'きさま - you';

const String gaarufurendo = 'ガールフレンド - girlfriend';

const String booifurendo = 'ボーイフレンド - boyfriend';

const String kanojo = 'かのじょ - she, girlfriend';

const String haha = 'はは - mother';

const String okaasan = 'おかあさん - mother';

const String ryoushin = 'りょうしん - parents';

const String chichi = 'ちち - father';

const String tsuma = 'つま - wife';

const String otto = 'おっと - husband';

const String ane = 'あね - older sister';

const String ani = 'あに - older brother';

const String imouto = 'いもうと - younger sister';

const String otouto = 'おとうと - younger brother';

const String musuko = 'むすこ - son';

const String musume = 'むすめ - daughter';

const String goryoushin = 'ごりょうしん - parents';

const String otousan = 'おとうさん - father';

const String okusan = 'おくさん - wife';

const String goshujin = 'ごしゅじん - husband';

const String oneesan = 'おねえさん - older sister';

const String oniisan = 'おにいさん - older brother';

const String imoutosan = 'いもうとさん - younger sister';

const String otoutosan = 'おとうとさん - younger brother';

const String musukosan = 'むすこさん - son';

const String musumesan = 'むすめさん - daughter';

const String kanai = 'かない - wife';

const String tanaka = 'たなか - Tanaka';

const String suzuki = 'すずき - Suzuki';

const String kaimono = 'かいもの - shopping';

const String ryouri = 'りょうり - dish, cuisine';

const String chotto = 'ちょっと - a little';

const String onaka = 'おなか - stomach';

const String ippai = 'いっぱい - full';

const String sumimasen = 'すみません - sorry';

const String gomennasai = 'ごめんなさい - sorry';

const String gomen = 'ごめん - sorry';

const String konna = 'こんな - this sort of';

const String sonna = 'そんな - that sort of';

const String hontou = 'ほんとう - really';

const String iu = 'いう - to say';

const String wakaru = '分かる - to understand';

const String oshieru = 'おしえる - to teach';

const String itsu = 'いつ - when';

const String dore = 'どれ - which';

const String dareka = 'だれか - someone';

const String kukkii = 'クッキー - cookie';

const String nusumu = 'ぬすむ - to steal';

const String hannin = 'はんにん - criminal';

const String dokoka = 'どこか - somewhere';

const String doreka = 'どれか - whichever (one from many)';

const String erabu = 'えらぶ - to select';

const String daremo = 'だれも - nobody';

const String nanimo = 'なにも - nothing';

const String dokomo = 'どこも - everywhere';

const String doremo = 'どれも - any and all';

const String minna = 'みんな - everybody';

const String minasan = 'みなさん - everybody';

const String shitsumon = 'しつもん - question';

const String kotae = 'こたえ - answer';

const String okureru = 'おくれる - to be late';

const String konshuumatsu = 'こんしゅうまつ - this weekend';

const String dokonimo = 'どこにも - nowhere';

const String koko = 'ここ - here';

const String nandemo = 'なんでも - anything';

const String daredemo = 'だれでも - anybody';

const String dokodemo = 'どこでも - anywhere';

const String ippanteki = 'いっぱんてき - in general';

const String semai = 'せまい - narrow';

const String okanemochi = 'おかねもち - rich';

const String miryokuteki = 'みりょくてき - charming';

const String shokudou = 'しょくどう - cafeteria';

const String hirune = 'ひるね - afternoon nap';

const String paatii = 'パーティー - party';

const String purezento = 'プレゼント - present';

const String yamada = 'やまだ - Yamada';

const String ichirou = 'いちろう - Ichiro';

const String naoko = 'なおこ - Naoko';

const String sorosoro = 'そろそろ - gradually, soon';

const String shitsurei = 'しつれい - discourtesy';

const String odayaka = 'おだやか - calm, peaceful';

const String undou = 'うんどう - exercise';

const String yaseru = 'やせる - to become thin';

const String depaato = 'デパート - department store';

const String hoshii = 'ほしい - desirable';

const String himaFree = 'ひま - free, leisure';

const String kiku = 'きく - to listen, to ask';

const String kikuListen = 'きく - to listen';

const String mada = 'まだ - yet';

const String toshiue = 'としうえ - older';

const String yasashii = 'やさしい - gentle, kind';

const String kantan = 'かんたん - simple';

const String yomu = 'よむ - to read';

const String muzukashii = 'むずかしい - difficult';

const String kyoukasho = 'きょうかしょ - textbook';

const String hanashi = 'はなし - story';

const String kekkon = 'けっこん - marriage';

const String uta = 'うた - song';

const String michi = 'みち - road';

const String mie = 'みえ - Mie';

const String mou = 'もう - already';

const String saki = 'さき - before';

const String junbi = 'じゅんび - preparations';

const String ryokou = 'りょこう - travel';

const String keikaku = 'けいかく - plans';

const String owaru = 'おわる - to end';

const String kippu = 'きっぷ - ticket';

const String hoteru = 'ホテル - hotel';

const String yoyaku = 'よやく - reservation';

const String oku = 'おく - to place';

const String tsukuru = 'つくる - to make';

const String denchi = 'でんち - battery';

const String enpitsu = 'えんぴつ - pencil';

const String eki = 'えき - station';

const String hou = 'ほう - direction, way';

const String isshoukenmei = 'いっしょうけんめい - with all one\'s might';

const String ganbaru = 'がんばる - to try one\'s best';

const String iroiro = 'いろいろ - various';

const String tsukiau = 'つきあう - to go out with, to keep in company with';

const String zutto = 'ずっと - long, far';

const String mae = 'まえ - front, before';

const String kekkyoku = 'けっきょく - eventually';

const String mitsukaru = 'みつかる - to be found';

const String dekiru = 'できる - to be able to do';

const String kiruWear = 'きる - to wear';

const String shinjiru = 'しんじる - to believe';

const String neruSleep = 'ねる - to sleep';

const String okiru = 'おきる - to get up';

const String kakeru = 'かける - to hang';

const String kakeruTake = 'かける - to take (time, money)';

const String shiraberu = 'しらべる - to investigate';

const String toru = 'とる - to take';

const String matsu = 'まつ - to wait';

const String kanji = 'かんじ - kanji';

const String zannen = 'ざんねん - unfortunate';

const String fujisan = 'ふじさん - Mt. Fuji';

const String noboru = 'のぼる - to climb';

const String omoi = 'おもい - heavy';

const String nimotsu = 'にもつ - baggage';

const String mieru = 'みえる - to be visible';

const String kikoeru = 'きこえる - to be audible';

const String hareru = 'はれる - to be sunny';

const String okage = 'おかげ - thanks to';

const String tada = 'ただ - free of charge, only';

const String hisashiburi = 'ひさしぶり - after a long time';

const String mawari = 'まわり - surroundings';

const String urusai = 'うるさい - noisy';

const String eru = 'える - to obtain';

const String arieruAriuru = 'ありうる／ありえる - to be possible';

const String arieru = 'ありえる - to be possible';

const String ariuru = 'ありうる - to be possible';

const String nebou = 'ねぼう - oversleep';

const String jouzu = 'じょうず - skillful';

const String yuumei = 'ゆうめい - famous';

const String hanbaagaa = 'ハンバーガー - hamburger';

const String sarada = 'サラダ - salad';

const String hoka = 'ほか - other';

const String yappari = 'やっぱり - as I thought';

const String kyonen = 'きょねん - last year';

const String se = 'せ - height';

const String tsuyoi = 'つよい - strong';

const String atama = 'あたま - head';

const String you = 'よう - appearance, manner';

const String kaigai = 'かいがい - overseas';

const String sushi = 'すし - sushi';

const String ichinenkan = 'いちねんかん - span of 1 year';

const String renshuu = 'れんしゅう - practice';

const String piano = 'ピアノ - piano';

const String hiku = 'ひく - to play (piano, guitar)';

const String chika = 'ちか - underground';

const String kurai = 'くらい - dark';

const String futoru = 'ふとる - to become fatter';

const String kitto = 'きっと - for sure';

const String san = 'さん - polite name suffix';

const String mondai = 'もんだい - problem';

const String asoko = 'あそこ - over there';

const String okashii = 'おかしい - funny';

const String tanoshii = 'たのしい - fun';

const String byouki = 'びょうき - disease, sickness';

const String jidou = 'じどう - automatic';

const String waribiki = 'わりびき - discount';

const String moshi = 'もし - if by any chance';

const String dame = 'だめ - no good';

const String ikenai = 'いけない - must not do, not good';

const String yoru = 'よる - evening';

const String made = 'まで - until';

const String denwa = 'でんわ - telephone';

const String ikeru = 'いける - is possible, will work out';

const String kamau = 'かまう - to mind, to be concerned about';

const String onsen = 'おんせん - hotspring';

const String keeki = 'ケーキ - cake';

const String issho = 'いっしょ - together';

const String inu = 'いぬ - dog';

const String nuigurumi = 'ぬいぐるみ - stuffed doll';

const String naoru = 'なおる - to be fixed';

const String teemapaaku = 'テーマパーク - theme park';

const String karee = 'カレー - curry';

const String tamani = 'たまに - once in a while';

const String sakebu = 'さけぶ - to scream';

const String yobu = 'よぶ - to call';

const String tsubuyaku = 'つぶやく - to mutter';

const String samui = 'さむい - cold';

const String kangaeru = 'かんがえる - to ponder';

const String nan = 'なん - what';

const String koukousei = 'こうこうせい - high school student';

const String tomoko = 'ともこ - Tomoko';

const String sugoi = 'すごい - to a great extent';

const String tai = 'たい - Tai (fish)';

const String eigo = 'えいご - English';

const String imi = 'いみ - meaning';

const String shujinkou = 'しゅじんこう - main character';

const String ichiban = 'いちばん - best; first';

const String nihonjin = 'にほんじん - Japanese person';

const String osake = 'おさけ - alcohol';

const String yowai = 'よわい - weak';

const String dokushin = 'どくしん - single, unmarried';

const String uso = 'うそ - lie';

const String ribuuto = 'リブート - reboot';

const String pasokon = 'パソコン - computer';

const String saikidou = 'さいきどう - reboot';

const String kou = 'こう - like this';

const String souLikeThat = 'そう - like that';

const String aa = 'ああ - like that';

const String komaru = 'こまる - to be bothered, troubled';

const String iya = 'いや - dislike';

const String shiawase = 'しあわせ - happiness';

const String toki = 'とき - time';

const String ikiru = 'いきる - to live';

const String tabun = 'たぶん - maybe';

const String souThatWay = 'そう - that way';

const String yousuke = 'ようすけ - Yousuke';

const String wakareru = 'わかれる - to separate';

const String kareshi = 'かれし - he; boyfriend';

const String ryuugaku = 'りゅうがく - study abroad';

const String yuu = 'ゆう - to say';

const String okonomiyaki = 'おこのみやき - pizza-like pancake fried with various ingredients';

const String hajimete = 'はじめて - for the first time';

const String atarashii = 'あたらしい - new';

const String hiroshima = 'ひろしま - Hiroshima';

const String nemui = 'ねむい - sleepy';

const String sakeru = 'さける - to avoid';

const String muriyari = 'むりやり - forcibly, against one\'s will';

const String tetsuya = 'てつや - staying up all night';

const String tomeru = 'とめる - to stop';

const String kimeru = 'きめる - to decide';

const String narubeku = 'なるべく - as much as possible';

const String jimu = 'ジム - gym';

const String oseibo = 'おせいぼ - year-end presents';

const String ochuugen = 'おちゅうげん - Bon Festival gifts';

const String ageru = 'あげる - to give, to raise';

const String kureru = 'くれる - to give';

const String morau = 'もらう - to receive';

const String kuruma = 'くるま - car';

const String kawari = 'かわり - substitute';

const String oshieruInform = 'おしえる - to teach, to inform';

const String yaru = 'やる - to do';

const String esa = 'えさ - food for animals';

const String ageruRaise = 'あげる - to raise';

const String kudasaru = 'くださる - to give (hon)';

const String ue = 'うえ - above';

const String shita = 'した - below';

const String chekku = 'チェック - check';

const String muri = 'むり - impossible';

const String tokei = 'とけい - watch';

const String senen = 'せんえん - 1000 yen';

const String kasu = 'かす - lend';

const String nasaru = 'なさる - to do (hon)';

const String kudasai = 'ください - please give, please do';

const String yukkuri = 'ゆっくり - slowly';

const String rakugaki = 'らくがき - scribble, graffiti';

const String keshigomu = 'けしゴム - eraser';

const String tooi = 'とおい - far';

const String tokoro = 'ところ - place';

const String kowareru = 'こわれる - to become broken';

const String choudai = 'ちょうだい - accept, receive (hon)';

const String itasu = 'いたす - to do (hum)';

const String supuun = 'スプーン - spoon';

const String suwaru = 'すわる - to sit';

const String hen = 'へん - strange';

const String ichi = 'いち - one';

const String ni = 'に - two';

const String sanThree = 'さん - three';

const String shiYon = 'し、よん - four';

const String go = 'ご - five';

const String roku = 'ろく - six';

const String shichiNana = 'しち、なな - seven';

const String hachi = 'はち - eight';

const String kyuu = 'きゅう - nine';

const String juu = 'じゅう - ten';

const String shi = 'し - four';

const String yon = 'よん - four';

const String shichi = 'しち - seven';

const String nana = 'なな - seven';

const String hyaku = 'ひゃく - unit for hundred';

const String sen = 'せん - unit for thousand';

const String man = 'まん - unit for ten-thousand';

const String okuHundredMillion = 'おく - unit for hundred-million';

const String chou = 'ちょう - unit for trillion';

const String hyakuOneHundred = 'ひゃく - one hundred';

const String senOneThousand = 'せん - one thousand';

const String ichiman = 'いちまん - ten thousand';

const String ichioku = 'いちおく - one hundred million';

const String icchou = 'いっちょう - one trillion';

const String roppyaku = 'ろっぴゃく - six hundred';

const String sanzen = 'さんぜん - three thousand';

const String sanbyaku = 'さんびゃく - three hundred';

const String happyaku = 'はっぴゃく - eight hundred';

const String hassen = 'はっせん - eight thousand';

const String rei = 'れい - zero';

const String zero = 'ゼロ - zero';

const String maru = 'マル - \'O\' or zero';

const String ten = 'てん - period, point';

const String mainasu = 'マイナス - minus';

const String nen = 'ねん - counter for year';

const String nisensannen = 'にせんさんねん - Year 2003';

const String heisei = 'へいせい - Heisei era';

const String juuninen = 'じゅうにねん - Year 12';

const String shouwa = 'しょうわ - Showa era';

const String gojuurokunen = 'ごじゅうろく - Year 56';

const String wareki = 'われき - Japanese calendar';

const String gatsu = 'がつ - counter for month';

const String shigatsu = 'しがつ - April';

const String shichigatsu = 'しちがつ - July';

const String kugatsu = 'くがつ - September';

const String tsuitachi = 'ついたち - the first of the month';

const String ichinichi = 'いちにち - one day';

const String nichi = 'にち - counter for day';

const String nijuurokunichi = 'にじゅうろくにち - the 26th of the month';

const String juunigatsu = 'じゅうにがつ - December';

const String futsuka = 'ふつか - the 2nd of the month';

const String ji = 'じ - counter for hour(s)';

const String yoji = 'よじ - 4 o\'clock';

const String shichiji = 'しちじ - 7 o\'clock';

const String kuji = 'くじ - 9 o\'clock';

const String fun = 'ふん - counter for minute(s)';

const String ippun = 'いっぷん - 1 minute';

const String sanpun = 'さんぷん - 3 minutes';

const String yonpun = 'よんぷん - 4 minutes';

const String roppun = 'ろっぷん - 6 minutes';

const String happun = 'はっぷん - 8 minutes';

const String juppun = 'じゅっぷん - 10 minutes';

const String nijuuyonpun = 'にじゅうよんぷん - 24 minutes';

const String sanjuppun = 'さんじゅっぷん - 30 minutes';

const String hachifun = 'はちふん - 8 minutes';

const String jippun = 'じっぷん - 10 minutes';

const String byou = 'びょう - counter for second(s)';

const String kan = 'かん - designates a span of time or space';

const String isshuukan = 'いっしゅうかん - span of one week';

const String hasshuukan = 'はっしゅうかん - span of eight weeks';

const String getsu = 'げつ - counter for number of month(s)';

const String sendagaya = 'げつ - counter for number of month(s)';

const String ikkagetsu = 'いっかげつ - span of one month';

const String rokkagetsu = 'ろっかげつ - span of six months';

const String jukkagetsu = 'じゅっかげつ - span of ten months';

const String tsu = 'つ - generic counter';

const String kasho = 'かしょ - counter for number of locations';

const String kai = 'かい - counter for number of times';

const String ko = 'こ - counter for small (often round) objects';

const String sai = 'さい - counter for age';

const String hiki = 'ひき - counter for small animals';

const String satsu = 'さつ - counter for bound objects';

const String mai = 'まい - counter for thin objects';

const String honCounter = 'ほん - counter for long, cylindrical objects';

const String nin = 'にん - counter for people';

const String too = 'とお - ten things';

const String hitori = 'ひとり - one person';

const String futari = 'ふたり - two people';

const String juuichinin = 'じゅういちにん - eleven people';

const String saiSimplified = 'さい - simplified counter for age';

const String hatachi = 'はたち - 20 years old';

const String me = 'め - counter for the nth item or event, eye(s)';

const String ban = 'ばん - counter for place or ranking';

const String ichibanme = 'いちばんめ - the first one';

const String ikkaime = 'いっかいめ - the first time';

const String nikaime = 'にかいめ - the second time';

const String yoninme = 'よにんめ - the fourth person';

const String tsumaranai = 'つまらない – boring';

const String mattaku = 'まったく – entirely; indeed; good grief (expression of exasperation)';

const String guzuguzu = 'ぐずぐず – tardily; hesitatingly';

const String sarariiman = 'サラリーマン – office worker (salary man)';

const String zangyou = 'ざんぎょう – overtime';

const String maa = 'まあ – well';

const String hora = 'ほら – look';

const String repooto = 'レポート – report';

const String kigaeru = 'きがえる – to change clothes';

const String kun = 'くん – name suffix';

const String yappa = 'やっぱ – as I thought';

const String sugu = 'すぐ – soon; nearby';

const String tonari = 'となり – next to';

const String karaoke = 'カラオケ – karaoke';

const String chikai = 'ちかい – close, near';

const String chigau = 'ちがう – to be different';

const String deeto = 'デート – date';

const String naninan = 'なに、なん – what';

const String naniiro = 'なにいろ – what color';

const String nannin = 'なんにん – how many people';

const String nanika = 'なにか – something';

const String ofuro = 'おふろ – bath';

const String kimochi = 'きもち – feeling';

const String modoru = 'もどる – to return';

const String anna = 'あんな – that sort of';

const String yatsu = 'やつ – guy (derogatory)';

const String makeru = 'まける – to lose';

const String ki = 'き – mood; intent';

const String sassato = 'さっさと – quickly';

const String nanka = 'なんか - something like that';

const String konoaida = 'このあいだ - the other day';

const String dizuniirando = 'ディズニーランド - Disney Land';

const String komu = 'こむ - to become crowded';

const String oi = 'おい – hey';

const String ittai = 'いったい – forms an emphatic question (e.g. “why on earth?”)';

const String nanji = 'なんじ – what time';

const String tsumori = 'つもり – intention, plan';

const String doyoubi = 'どようび – Saturday';

const String owari = 'おわり - end';

const String kaga = 'かが - Kaga';

const String daitai = 'だいたい - mostly';

const String konnichiha = 'こんにちは - good afternoon';

const String tadashi = 'ただし - however';

const String konnichiwa = 'こんにちわ - good afternoon';

const String hyougen = 'ひょうげん - expression';

const String oboeru = 'おぼえる - to memorize';

const String asa = 'あさ - morning';

const String ohayou = 'おはよう - good morning';

const String ohayougozaimasu = 'おはようございます - good morning';

const String machigaeru = 'まちがえる - to make mistake';

const String raishuu = 'らいしゅう - next week';

const String mokuyoubi = 'もくようび - Thursday';

const String namakeru = 'なまける - to neglect, to be lazy about';

const String arigatou = 'ありがとう - thanks';

const String yasumu = 'やすむ - to rest';

const String buchou = 'ぶちょう - section manager';

const String choujikan = 'ちょうじかん - long period of time';

const String hataraku = 'はたらく - to work';

const String toire = 'トイレ - bathroom';

const String sasu = 'さす - to point';

const String nankai = 'なんかい - number of times';

const String porijji = 'ポリッジ - porridge';

const String hikari = 'ひかり - light';

const String hayasa = 'はやさ - speed';

const String koeru = 'こえる - to exceed';

const String fukanou = 'ふかのう - impossible';

const String ooku = 'おおく - many, large number';

const String gaikokujin = 'がいこくじん - foreigner';

const String pakkeeji = 'パッケージ - package';

const String arayuru = 'あらゆる - all';

const String fukumu = 'ふくむ - to include';

const String ryoushuushou = 'りょうしゅうしょう - receipt';

const String kaigi = 'かいぎ - meeting';

const String ooi = 'おおい - many';

const String aitsu = 'あいつ - that guy (colloquial)';

const String nijikan = 'にじかん - two hours';

const String oya = 'おや - parent(s)';

const String tatsu = 'たつ - to stand';

const String rouka = 'ろうか - hall, corridor';

const String irassharu = 'いらっしゃる (hon.) - to go, to come, to exist (animate)';

const String oideninaru = 'おいでになる (hon.) - to go, to come, to exist (animate)';

const String goran = 'ごらん (hon.) - seeing';

const String haiken = 'はいけん (hum.) - seeing';

const String ukagau = 'うかがう (hum.) - to ask, to enquire, to hear';

const String ossharu = 'おっしゃる (hon.) - to say';

const String mousu = 'もうす (hum.) - to say';

const String moushiageru = 'もうしあげる (hum.) - to say';

const String sashiageru = 'さしあげる - to give, to offer';

const String itadaku = 'いただく - to receive';

const String gosonji = 'ごぞんじ (hon.) - knowing';

const String sonjiru = 'ぞんじる (hum.) - to know';

const String oru = 'おる (hum.) - to exist (animate)';

const String meshiagaru = 'めしあがる (hon.) - to eat';

const String gozaru = 'ござる (formal) - to exist (inanimate)';

const String suisenjou = 'すいせんじょう - recommendation';

const String dochira = 'どちら - which direction';

const String kochira = 'こちら - this way, over here';

const String otearai = 'おてあらい - restroom';

const String nikai = 'にかい - second floor';

const String yoroshii = 'よろしい - good';

const String suimasen = 'すいません - sorry';

const String warui = 'わるい - bad';

const String moushiwake = 'もうしわけ - excuse';

const String iiwake = 'いいわけ - excuse';

const String osoreiru = 'おそれいる - to be sorry';

const String kyoushuku = 'きょうしゅく - shame';

const String sama = 'さま - polite suffix';

const String okyakusama = 'おきゃくさま - customer';

const String kamisama = 'かみさま - god';

const String ocha = 'おちゃ - tea';

const String onyomi = 'おんよみ - Chinese reading';

const String iken = 'いけん - opinion';

const String kunyomi = 'くんよみ - Japanese reading';

const String omiyage = 'おみやげ - souvenir';

const String henji = 'へんじ - reply';

const String tennai = 'てんない - inside store';

const String nijuukeigo = 'にじゅうけいご -  doubly-polite phrase';

const String shoushou = 'しょうしょう - just a bit';

const String doa = 'ドア - door';

const String chuui = 'ちゅうい - caution';

const String negau = 'ねがう - to request';

const String azukaru = 'あずかる - to keep charge of';

const String arigatougozaimasu = 'ありがとうございます (pol) - thank you';

const String mata = 'また - again';

const String kosu = 'こす - to go over';

const String douzo = 'どうぞ - please';

const String shimau = 'しまう - to do something by accident, to finish completely';

const String kousuke = 'こうすけ - Kousuke';

const String nikiro = 'にキロ - 2 kilograms';

const String chanto = 'ちゃんと - properly';

const String kingyo = 'きんぎょ - goldfish';

const String dokka = 'どっか - somewhere (from どこか)';

const String chikoku = 'ちこく - tardiness';

const String tsui = 'つい - unconsciously';

const String pari = 'パリ - Paris';

const String osushi = 'おすし - sushi';

const String yooroppa = 'ヨーロッパ - Europe';

const String ichido = 'いちど - one time';

const String choudo = 'ちょうど - exactly';

const String souiu = 'そういう - that type of thing';

const String korekara = 'これから - from now';

const String taikutsu = 'たいくつ - boredom';

const String shiai = 'しあい - match; game';

const String chuushi = 'ちゅうし - cancellation';

const String ikkai = 'いっかい - one time';

const String yoyogi = 'よよぎ - Yoyogi';

const String nigeru = 'にげる - to run away';

const String souji = 'そうじ - cleaning';

const String tetsudau = 'てつだう - to help; to assist';

const String ringo = 'りんご';

const String utau = 'うたう - to sing';

const String hanbaiki = 'はんばいき - vending machine';

const String gohyakuendama = 'ごひゃくえんだま - 500 yen coin';

const String tsukau = 'つかう - to use';

const String kobayashi = 'こばやし - Kobayashi';

const String joushaken = 'じょうしゃけん - passenger ticket';

const String hatsubai = 'はつばい - sale';

const String toujitsu = 'とうじつ - that very day';

const String yuukou = 'ゆうこう - effective';

const String ankeeto = 'アンケート - survey';

const String taishou = 'たいしょう - target';

const String daigakusei = 'だいがくせい - college student';

const String kusaru = 'くさる - to rot';

const String obasan = 'おばさん - middle-aged lady';

const String takashi = 'たかし - Takashi';

const String manga = 'まんが - comic book';

const String kakkowarui = 'かっこわるい - unattractive; not cool';

const String maajan = 'マージャン - mahjong';

const String naomi = 'なおみ - Naomi';

const String chan = 'ちゃん - name suffix';

const String sugiru = 'すぎる - to exceed; to pass';

const String nai = 'ない - to not exist';

const String mottainai = 'もったいない - wasteful';

const String nasakenai = 'なさけない - pitiable';

const String abunai = 'あぶない - dangerous';

const String sukunai = 'すくない - few';

const String satou = 'さとう - Satou';

const String kiwotsukeru = 'きをつける - to be careful';

const String toranku = 'トランク - trunk';

const String wana = 'わな - trap';

const String tariru = 'たりる - to be sufficient';

const String sankai = 'さんかい - three times';

const String sanjikan = 'さんじかん - 3 hours';

const String juukiro = 'じゅうきろ - ten kilograms';

const String kotoshi = 'ことし - this year';

const String sakuban = 'さくばん - last night';

const String atsui = 'あつい - hot';

const String kankoku = 'かんこく - Korea';

const String mayou = 'まよう - to get lost';

const String haadodisuku = 'ハードディスク - hard disk';

const String youryou = 'ようりょう - capacity';

const String motto = 'もっと - much more';

const String kyoku = 'きょく - tune';

const String hozon = 'ほぞん - save';

const String koukuuken = 'こうくうけん - plane ticket';

const String yasui = 'やすい - cheap';

const String bunshou = 'ぶんしょう - sentence';

const String mijikai = 'みじかい - short';

const String hikui = 'ひくい - low';

const String choukaku = 'ちょうかく - sense of hearing';

const String ningen = 'にんげん - human';

const String kuraberu = 'くらべる - to compare';

const String haruka = 'はるか - far more';

const String binkan = 'びんかん - sensitive';

const String funiki = 'ふんいき - atmosphere';

const String urikire = 'うりきれ - sold-out';

const String seifuku = 'せいふく - uniform';

const String sugata = 'すがた - figure';

const String piza = 'ピザ - pizza';

const String baransu = 'バランス - balance';

const String kuzureru = 'くずれる - to collapse; to break down';

const String isshun = 'いっしゅん - an instant';

const String taoreru = 'たおれる - to collapse';

const String atari = 'あたり - vicinity';

const String tsukemono = 'つけもの - pickled vegetables';

const String kinpatsu = 'きんぱつ - blond hair';

const String juuji = 'じゅうじ - 10 o\'clock';

const String kawaisou = 'かわいそう - poor thing';

const String onna = 'おんな - woman';

const String oosawagi = 'おおさわぎ - clamor, uproar';

const String miyuki = 'みゆき - Miyuki';

const String kankokujin = 'かんこくじん - Korean person';

const String kyouko = 'きょうこ - Kyouko';

const String houKata = 'ほう - direction, side; かた - person, way of doing';

const String kata = 'かた - person, way of doing';

const String kataWay = 'かた - way of doing';

const String wakai = 'わかい - young';

const String akachan = 'あかちゃん - baby';

const String kenkou = 'けんこう - health';

const String kowai = 'こわい - scary';

const String hana = 'はな - flower';

const String dango = 'だんご - rice dumpling';

const String pan = 'パン - bread';

const String mashi = 'まし - not as bad';

const String hanami = 'はなみ - cherry-blossom viewing';

const String shinjuku = 'しんじゅく - Shinjuku';

const String karada = 'からだ - body';

const String shouhin = 'しょうひん - product';

const String hinshitsu = 'ひんしつ - quality of a good';

const String taisetsu = 'たいせつ - important';

const String kisetsu = 'きせつ - season';

const String kudamono = 'くだもの - fruit';

const String mazui = 'まずい - unpleasant';

const String kazuko = 'かずこ - Kazuko';

const String daiki = 'だいき - Daiki';

const String yuuko = 'ゆうこ - Yuuko';

const String yohou = 'よほう - forecast';

const String yatto = 'やっと - at last';

const String mitsukeru = 'みつける - to find';

const String kakuteru = 'カクテル - cocktail';

const String biiru = 'ビール - beer';

const String minikui = 'みにくい - ugly';

const String yasashiiEasy = 'やさしい - easy';

const String youi = 'ようい - simple';

const String nikuiKatai = 'にくい／かたい - difficult';

const String omoide = 'おもいで - memories';

const String tsurai = 'つらい - painful';

const String karai = 'からい - spicy';

const String machiawase = 'まちあわせ - meeting arrangement';

const String basho = 'ばしょ - location';

const String ha = 'は - tooth';

const String migaku = 'みがく - to brush';

const String soudan = 'そうだん - consultation';

const String touzen = 'とうぜん - naturally';

const String yopparau = 'よっぱらう - to get drunk';

const String toudai = 'とうだい - Tokyo University （東京大学）';

const String suman = 'すまん - sorry';

const String sumanai = 'すまない - sorry';

const String ojisan = 'おじさん - middle-aged man; uncle';

const String mogi = 'もぎ - mock';

const String shippai = 'しっぱい - failure';

const String jissai = 'じっさい - actual';

const String ukeru = 'うける - to receive';

const String kekka = 'けっか - result';

const String wake = 'わけ - reason; circumstances';

const String ikura = 'いくら - how much';

const String umai = 'うまい - tasty; skillful';

const String gogaku = 'ごがく - language study';

const String nouryoku = 'のうりょく - ability';

const String chuugokugo = 'ちゅうごくご - Chinese';

const String hiroko = 'ひろこ - Hiroko';

const String bisekibun = 'びせきぶん - (differential and integral) calculus';

const String goukaku = 'ごうかく - pass';

const String kondo = 'こんど - this time';

const String akirameru = 'あきらめる - to give up';

const String kankyaku = 'かんきゃく - spectator';

const String sanka = 'さんか - participation';

const String higaisha = 'ひがいしゃ - victim';

const String hijou = 'ひじょう - extreme';

const String saiwai = 'さいわい - fortunate';

const String hiru = 'ひる - afternoon';

const String suku = 'すく - to become empty';

const String tsumari = 'つまり - in short';

const String terebi = 'テレビ - television';

const String aite = 'あいて - other party';

const String tannaru = 'たんなる - simply';

const String wagamama = 'わがまま - selfish';

const String poppukoon = 'ポップコーン - popcorn';

const String kuchibue = 'くちぶえ - whistle';

const String tegami = 'てがみ - letter';

const String nagara = 'ながら - while';

const String ongaku = 'おんがく - music';

const String tobu = 'とぶ - to jump';

const String masaka = 'まさか - you can\'t mean...';

const String binbou = 'びんぼう - poor';

const String koukyuu = 'こうきゅう - high-quality';

const String baggu = 'バッグ - bag';

const String shoshinsha = 'しょしんしゃ - beginner';

const String jitsuryoku = 'じつりょく - actual ability';

const String puro = 'プロ - pro (professional)';

const String nagaramo = 'ながらも - even while';

const String geemu = 'ゲーム - game';

const String hamaru = 'はまる - to get hooked';

const String koora = 'コーラ - cola';

const String kurabu = 'クラブ - club';

const String kanashii = 'かなしい - sad';

const String hanbun = 'はんぶん - half';

const String ka = 'か - mosquito';

const String nemureru = 'ねむれる - to fall sleep';

const String hottarakasu = 'ほったらかす - to neglect';

const String hottoku = 'ほっとく - to leave alone';

const String hanasuSetLoose = 'はなす - to set loose';

const String wagahai = 'わがはい – I; we';

const String natsume = 'なつめ – Natsume';

const String souseki = 'そうせき – Souseki';

const String omakase = 'おまかせ – leaving a decision to someone else';

const String hyouji = 'ひょうじ – display';

const String kongoubutsu = 'こんごうぶつ – mixture, amalgam';

const String shurui = 'しゅるい – type, kind, category';

const String ijou = 'いじょう – greater or equal';

const String junbusshitsu = 'じゅんぶっしつ – pure material';

const String majiriau = 'まじりあう – to mix together';

const String busshitsu = 'ぶっしつ – pure material';

const String fukouhei = 'ふこうへい – unfair';

const String gengo = 'げんご – language';

const String masutaa = 'マスター – master';

const String hanabi = 'はなび – fireworks';

const String kayaku = 'かやく – gunpowder';

const String kinzoku = 'きんぞく – metal';

const String funmatsu = 'ふんまつ – fine powder';

const String mazeru = 'まぜる – to mix';

const String hi = 'ひ – flame, light';

const String nenshouji = 'ねんしょうじ – at time of combustion';

const String hibana = 'ひばな – spark';

const String tame = 'ため – for the sake/benefit of';

const String kigyounai = 'きぎょうない – company-internal';

const String kokyaku = 'こきゃく – customer, client';

const String deeta = 'データ – data';

const String riyou = 'りよう – usage';

const String yukue = 'ゆくえ – whereabouts';

const String fuutou = 'ふうとう – envelope';

const String shashin = 'しゃしん – photograph';

const String suumai = 'すうまい – several sheets (flat objects)';

const String soeru = 'そえる – to garnish; to accompany (as a card does a gift)';

const String fairu = 'ファイル – file';

const String pasuwaado = 'パスワード – password';

const String settei = 'せってい – setting';

const String hiraku = 'ひらく – to open';

const String saiOccasion = '～さい – on the occasion of';

const String nyuuryoku = 'にゅうりょく – input';

const String hitsuyou = 'ひつよう – necessity';

const String zettai = 'ぜったい – absolutely, unconditionally';

const String suishou = 'すいしょう – recommendation';

const String suruRub = 'する – to rub';

const String okonau = 'おこなう – to conduct, to carry out';

const String tatoe = 'たとえ – example';

const String kuni = 'くに – country';

const String kokumin = 'こくみん – people, citizen';

const String damasu = 'だます – to trick, to cheat, to deceive';

const String yokinsha = 'よきんしゃ – depositor';

const String oote = 'おおて – large corporation';

const String soshou = 'そしょう – litigation, lawsuit';

const String okosu = 'おこす – to cause, to wake someone';

const String keesu = 'ケース – case';

const String kinyuuchou = 'きんゆうちょう – Financial Services Agency';

const String kyuusai = 'きゅうさい – relief, aid';

const String yuusen = 'ゆうせん – preference, priority, precedence';

const String kinyuu = 'きんゆう – financing';

const String kikan = 'きかん – institution';

const String hanzai = 'はんざい – crime';

const String boushi = 'ぼうし – prevention';

const String kyouka = 'きょうか – strengthen';

const String unagasu = 'うながす – to urge';

const String handan = 'はんだん – judgement, decision';

const String asahi = 'あさひ – Asahi';

const String shinbun = 'しんぶん – newspaper';

const String taisaku = 'たいさく – measure';

const String nichiyoubi = 'にちようび – Sunday';

const String kanou = 'かのう – possible';

const String mania = 'マニア – mania';

const String ra = '～ら – pluralizing suffix';

const String kogeru = 'こげる – to burn, to be burned';

const String yotei = 'よてい – plans, arrangement';

const String kurisumasu = 'クリスマス – Christmas';

const String naosu = 'なおす  – to correct, to fix';

const String uchiawase = 'うちあわせ – meeting';

const String maishuu = 'まいしゅう – every week';

const String niji = 'にじ - 2 o\'clock';

const String hajimaru = 'はじまる – to begin';

const String renyoukei = 'れんようけい – conjunctive form';

const String hajimeru = 'はじめる – to begin';

const String kongo = 'こんご – from now on';

const String taiwa = 'たいわ – interaction';

const String madoguchi = 'まどぐち – teller window, counter; point of contact';

const String yori = 'より – more';

const String juujitsu = 'じゅうじつ – fulfilled';

const String doryoku = 'どりょく – effort';

const String mizenkei = 'みぜんけい – imperfective form';

const String gomi = 'ゴミ – garbage';

const String anzen = 'あんぜん – safety';

const String sochi = 'そち – measures';

const String rakuchin = 'らくちん – easy';

const String bitamin = 'ビタミン – vitamin';

const String hoshou = 'ほしょう – guarantee';

const String ayamachi = 'あやまち – fault, error';

const String mitomeru = 'みとめる – to recognize, to acknowledge';

const String kaiketsu = 'かいけつ – resolution';

const String hitokoto = 'ひとこと – a few words';

const String tensai = 'てんさい – genius';

const String kinchou = 'きんちょう – nervousness';

const String chiratto = 'ちらっと – a peek';

const String seito = 'せいと – student';

const String oroka = 'おろか – foolish';

const String hiragana = 'ひらがな – Hiragana';

const String nikagetsu = 'にかげつ – 2 months';

const String koukou = 'こうこう – high school';

const String sotsugyou = 'そつぎょう – graduate';

const String ureshii = 'うれしい – happy';

const String hazukashii = 'はずかしい – embarrassing';

const String taipu = 'タイプ – type';

const String uchi = 'うち – one’s in-group';

const String puuru = 'プール – pool';

const String riyuu = 'りゆう – reason';

const String yosan = 'よさん – budget';

const String ayashii = 'あやしい – suspicious; dubious; doubtful';

const String mon = 'もん – object';

const String minesota = 'ミネソタ – Minnesota';

const String hazukashihgariya = 'はずかしがりや – one who easily feels or acts embarrassed';

const String samugariya = 'さむがりや – one who easily feels cold';

const String atsugariya = 'あつがりや – one who easily feels hot';

const String kurasu = 'くらす – to live';

const String bakuhatsu = 'ばくはつ – explosion';

const String fukuramu = 'ふくらむ – to expand; to swell';

const String kankei = 'かんけい – relation, relationship';

const String mushi = 'むし – ignore';

const String kenka = 'けんか – quarrel';

const String heiki = 'へいき – coolness; calmness';

const String kao = 'かお – face';

const String nazo = 'なぞ – puzzle';

const String himitsu = 'ひみつ – secret';

const String hiniku = 'ひにく – irony';

const String kouyou = 'こうよう – leaves changing color';

const String sukkari = 'すっかり – completely';

const String aki = 'あき – autumn';

const String kuuki = 'くうき – air; atmosphere';

const String setsumei = 'せつめい – explanation';

const String ruiviton = 'ルイヴィトン – Louis Vuitton';

const String ito = 'いと – intention; aim; design';

const String gaman = 'がまん – tolerance; self-control';

const String joutai = 'じょうたい – situation';

const String haisha = 'はいしゃ – dentist';

const String joushi = 'じょうし – superior; boss';

const String doushitemo = 'どうしても – by any means, no matter what';

const String shucchou = 'しゅっちょう – business trip';

const String yamu = 'やむ – to stop';

const String shikata = 'しかた – way, method';

const String shouganai = 'しょうがない – it can’t be helped, nothing can be done';

const String jiyuu = 'じゆう – reason; cause';

const String tetsudzuki = 'てつづき – procedure, paperwork';

const String kanarazu = 'かならず – without exception, without fail';

const String renraku = 'れんらく – contact';

const String kibishii = 'きびしい – strict';

const String fukeiki = 'ふけいき – recession, depression';

const String baai = 'ばあい – case, situation';

const String kaneru = 'かねる – to be unable to; to find difficult (unpleasant, awkward, painful) to do';

const String ba = 'ば – place, spot';

const String betto = 'べっと – separate';

const String moukeru = 'もうける – to establish';

const String kojin = 'こじん – personal';

const String rouei = 'ろうえい – disclosure; leakage';

const String sumiyaka = 'すみやか – speedy; prompt';

const String taiou = 'たいおう – dealing with; support';

const String jouhou = 'じょうほう – information';

const String kakutei = 'かくてい – decision; settlement';

const String shinkoku = 'しんこく – report; statement; filing a return';

const String kakuteishinkoku = 'かくていしんこく – final income tax return';

const String rusu = 'るす – being away from home';

const String katei = 'かてい – household';

const String susume = 'すすめ – recommendation';

const String chichioya = 'ちちおや – father';

const String shinpai = 'しんぱい – worry; concern';

const String futsukayoi = 'ふつかよい – hangover';

const String itamu = 'いたむ – to feel pain';

const String osaeru = 'おさえる – to hold something down; to grasp';

const String seihin = 'せいひん – manufactured goods, product';

const String hatten = 'はってん – development; growth; advancement';

const String tsureru = 'つれる – to lead';

const String masumasu = 'ますます – increasingly';

const String shuushin = 'しゅうしん – lifetime';

const String koyou = 'こよう – employment';

const String nenkou = 'ねんこう – long service';

const String joretsu = 'じょれつ – order';

const String nenkoujoretsu = 'ねんこうじょれつ – seniority system';

const String kankou = 'かんこう – customary practice';

const String izonshou = 'いぞんしょう – dependence; addiction';

const String shimekiribi = 'しめきりび – closing day; deadline';

const String girigiri = 'ぎりぎり – at the last moment; just barely';

const String koodingu = 'コーディング – coding';

const String kaihatsusha = 'かいはつしゃ – developer';

const String dokyumento = 'ドキュメント – document';

const String sakusei = 'さくせい – creation';

const String juubun = 'じゅうぶん – sufficient, adequate';

const String tesuto = 'テスト – test';

const String okotaru = 'おこたる – to shirk';

const String hitotsu = 'ひとつ – one';

const String ken = 'けん – sword';

const String tatsujin = 'たつじん – master, expert';

const String katsu = 'かつ – to win';

const String mucha = 'むちゃ – unreasonable; excessive';

const String shudan = 'しゅだん – method';

const String kesshite = 'けっして – by no means; decidedly';

const String kokoro = 'こころ – heart; mind';

const String chikau = 'ちかう – to swear, to pledge';

const String maniau = 'まにあう – to be in time';

const String wirusu = 'ウィルス – virus';

const String kyouryoku = 'きょうりょく – powerful, strong';

const String puroguramu = 'プログラム – program';

const String jikkou = 'じっこう – execute';

const String kansen = 'かんせん – infection';

const String konnan = 'こんなん (na-adj) – difficulty, distress';

const String gojuunen = 'ごじゅうねん – 50 years';

const String chokumen = 'ちょくめん – confrontation';

const String shoumen = 'しょうめん – front; facade';

const String mukiau = 'むきあう – to face each other';

const String mizukara = 'みずから – for one’s self';

const String hakaru = 'はかる – to plan, to devise';

const String nouhau = 'ノウハウ – know-how';

const String tsugi = 'つぎ – next';

const String sangyou = 'さんぎょう – industry';

const String shinario = 'シナリオ – scenario';

const String mochiron = 'もちろん – of course';

const String seitosuu = 'せいとすう – number of students';

const String genshou = 'げんしょう – decline, reduction';

const String genzai = 'げんざい – present time';

const String gakka = 'がっか – course of study';

const String shinsetsuNewlyOrganised = 'しんせつ – newly organized or established';

const String shokugyouka = 'しょくぎょうか – occupational studies';

const String touhaigou = 'とうはいごう – reorganization';

const String kanaiWithinStudy = 'かない – within study course';

const String koosu = 'コース – course';

const String kaihen = 'かいへん – reorganization';

const String jidai = 'じだい – period, era';

const String henkaku = 'へんかく – reform';

const String motomeru = 'もとめる – to request; to seek';

const String netto = 'ネット – net';

const String hanbai = 'はんばい – selling';

const String uriage = 'うりあげ – amount sold, sales';

const String nobiru = 'のびる – to extend, to lengthen';

const String kyuujitsu = 'きゅうじつ – holiday, day off';

const String donna = 'どんな – what kind of';

const String nido = 'にど – 2 times';

const String peeji = 'ページ – page';

const String machigai = 'まちがい – mistake';

const String hokori = 'ほこり – dust';

const String yakunitatsu = 'やくにたつ – to be useful';

const String keitai = 'けいたい – handheld (phone)';

const String kizu = 'きず – injury; scratch; scrape';

const String fuku = 'ふく – to wipe; to dry';

const String chi = 'ち – blood';

const String abura = 'あぶら – oil';

const String shuuri = 'しゅうり – repair';

const String tatta = 'たった – only, merely';

const String ichikiro = '１キロ – 1 kilometer';

const String ase = 'あせ – sweat';

const String kuro = 'くろ – black';

const String shiro = 'しろ – white';

const String dantai = 'だんたい – group';

const String nyuusu = 'ニュース – news';

const String sheeku = 'シェーク – shake';

const String eiyou = 'えいよう – nutrition';

const String tappuri = 'たっぷり – filled with';

const String ninenkan = 'にねんかん - span of 2 years';

const String kyouju = 'きょうじゅ – professor';

const String kyoushitsu = 'きょうしつ – classroom';

const String hourikomu = 'ほうりこむ – to throw into';

const String hitei = 'ひてい – denial';

const String toujou = 'とうじょう – boarding';

const String anaunsu = 'アナウンス – announcement';

const String geeto = 'ゲート – gate';

const String chirakasu = 'ちらかす – to scatter around; to leave untidy';

const String kuchi = 'くち – mouth';

const String hiruma = 'ひるま – daytime';

const String kaikei = 'かいけい – accountant; bill';

const String gosenen = 'ごせんえん – 5,000 yen';

const String sanpo = 'さんぽ – walk, stroll';

const String tabako = 'タバコ – tobacco; cigarettes';

const String hakubutsukan = 'はくぶつかん – museum';

const String ageku = 'あげく – in the end (after a long process); at last';

const String agekunohate = 'あげくのはて – in the end (after a long process); at last';

const String jijou = 'じじょう – circumstances';

const String nattoku = 'なっとく – understanding; agreement';

const String taigaku = 'たいがく – dropping out of school';

const String aisu = 'アイス – ice-cream';