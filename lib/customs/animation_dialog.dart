import 'package:flutter/material.dart';

class AnimationDialog {
  static showKanaAnimationDialog(String path, BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Image.asset('assets/animations/$path'),
        );
      },
    );
  }
}
