import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

class BlankList extends StatelessWidget {
  const BlankList({Key? key, required this.items}) : super(key: key);
  final List<Widget> items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items.mapIndexed<Widget>((index, item) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
              child: item,
          );
        }).toList(),
      ),
    );
  }
}
