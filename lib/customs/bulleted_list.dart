import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:collection/collection.dart';

class BulletedList extends StatelessWidget {
  const BulletedList({Key? key, required this.items}) : super(key: key);
  final List<Widget> items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultListPadding,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items.mapIndexed<Widget>((index, item) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SelectionContainer.disabled(child: Text('\u2022')),
                Expanded(child: Padding(
                  padding: kDefaultListItemPadding,
                  child: item,
                )),
              ],
            ),
          );
        }).toList(),
      ),
    );
  }
}