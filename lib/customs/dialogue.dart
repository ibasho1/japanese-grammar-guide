import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:collection/collection.dart';

class Dialogue extends StatelessWidget {
  const Dialogue({
    Key? key,
    required this.items,
  }) : super(key: key);
  final List<DialogueLine> items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultListPadding,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items.mapIndexed<DialogueLine>((index, item) {
          return item;
        }).toList(),
      ),
    );
  }
}
