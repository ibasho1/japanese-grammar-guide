import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class DialogueLine extends StatelessWidget {
  const DialogueLine(
      {Key? key,
      required this.speaker,
      required this.text,
      this.english = false})
      : super(key: key);
  final Text speaker;
  final Text text;
  final bool english;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              speaker,
              english ? const Text(': ') : const Text('：'),
            ],
          ),
          Expanded(
            child: Padding(
              padding: kDefaultListItemPadding,
              child: text,
            ),
          ),
        ],
      ),
    );
  }
}
