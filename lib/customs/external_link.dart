import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ExternalLink extends WidgetSpan {
  ExternalLink({Key? key, required this.uri, required this.inlineSpan})
      : super(
          baseline: TextBaseline.alphabetic,
          alignment: PlaceholderAlignment.baseline,
          child: InkWell(
            child: Text.rich(
              inlineSpan,
              style: const TextStyle(
                decoration: TextDecoration.underline,
              ),
            ),
            onTap: () async {
              if (!await launchUrl(Uri.parse(uri))) {
                throw Exception('Could not launch $uri');
              }
            },
          ),
        );
  final InlineSpan inlineSpan;
  final String uri;
}
