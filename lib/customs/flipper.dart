import 'dart:async';
import 'package:flutter/material.dart';

class Flipper extends StatefulWidget {
  const Flipper({Key? key, required this.value, required this.stream})
      : super(key: key);
  final String value;
  final Stream stream;

  @override
  State<Flipper> createState() => _FlipperState();
}

class _FlipperState extends State<Flipper> {
  StreamSubscription? streamSubscription;
  bool flipped = false;

  @override
  initState() {
    super.initState();
    streamSubscription = widget.stream.listen((flipped) => setFlipped(flipped));
  }

  @override
  Widget build(BuildContext context) {  
    final buffer = StringBuffer('');
    buffer.write('　' * (widget.value.length));
    final blank = buffer.toString();

    return InkWell(
      child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1.0, color: Theme.of(context).colorScheme.outline),
            ),
          ),
          child: flipped ? Text(widget.value) : Text(blank)),
      onTap: () {
        setState(
          () {
            flipped = !flipped;
          },
        );
      },
    );
  }

  setFlipped(bool flipped) {
    setState(() {
      this.flipped = flipped;
    });
  }
}
