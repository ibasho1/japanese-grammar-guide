import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class Heading extends StatelessWidget {
  const Heading({Key? key, required this.text, required this.level}) : super(key: key);
  final String text;
  final int level;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: paddingLevel(level),
      child: Text(
        text,
        style: headingLevel(level, context),
      ),
    );
  }

  headingLevel(int level, BuildContext context) {
    switch(level) {
          case 1:
            return Theme.of(context).textTheme.titleLarge;
          case 2:
            return Theme.of(context).textTheme.titleMedium;
          case 3:
            return Theme.of(context).textTheme.titleSmall;
          default:
            return const TextStyle(color: Colors.yellow);
    }
  }

  paddingLevel(int level) {
    switch(level) {
          case 1:
            return const EdgeInsets.all(0);
          case 2:
            return kDefaultHeading2Padding;
          case 3:
            return kDefaultHeading3Padding;
          default:
            return kDefaultHeading3Padding;
    }
  }
}
