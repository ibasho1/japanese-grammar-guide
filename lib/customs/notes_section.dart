import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:dotted_border/dotted_border.dart';

class NotesSection extends StatelessWidget {
  const NotesSection({Key? key, required this.content, required this.heading})
      : super(key: key);
  final String heading;
  final Widget content;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: DottedBorder(
          color: Theme.of(context).colorScheme.surfaceVariant,
          strokeWidth: 1,
          child: Container(
            color: Theme.of(context).colorScheme.surfaceVariant,
            child: Padding(
              padding: kDefaultNoteSectionPadding,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 5,
                      right: 5,
                      top: 5,
                      bottom: 15,
                    ),
                    child: Text(
                      heading,
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  content
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
