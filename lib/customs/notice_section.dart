import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class NoticeSection extends StatelessWidget {
  const NoticeSection({
    Key? key,
    required this.text,
  }) : super(key: key);
  final Text text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultParagraphPadding,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.primaryContainer,
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Center(
            child:  text,
          ),
        ),
      ),
    );
  }
}
