import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:collection/collection.dart';

class NumberedList extends StatelessWidget {
  const NumberedList({Key? key, required this.items, this.centered = false}) : super(key: key);
  final List<Widget> items;
  final bool centered;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultListPadding,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items.mapIndexed<Widget>((index, item) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              crossAxisAlignment: centered ? CrossAxisAlignment.center : CrossAxisAlignment.start,
              children: [
                SelectionContainer.disabled(child: Text('${index + 1}.')),
                Expanded(child: Padding(
                  padding: kDefaultListItemPadding,
                  child: item,
                )),
              ],
            ),
          );
        }).toList(),
      ),
    );
  }
}