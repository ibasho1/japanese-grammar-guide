import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class Paragraph extends StatelessWidget {
  const Paragraph({Key? key, required this.texts}) : super(key: key);
  final List<InlineSpan> texts;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultParagraphPadding,
      child: Text.rich(
        TextSpan(
          children: texts
        )
      ),
    );
  }
}
