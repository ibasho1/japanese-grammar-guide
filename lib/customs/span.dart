import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class Span extends StatelessWidget {
  const Span({Key? key, required this.text}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultParagraphPadding,
      child: Text(
        text,
      ),
    );
  }
}
