import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class TableCaption extends StatelessWidget {
  const TableCaption({Key? key, required this.caption}) : super(key: key);
  final String caption;

  @override
  Widget build(BuildContext context) {
    return Table(
      children: [
        TableRow(
          children: [
            Container(
              color: Theme.of(context).colorScheme.inversePrimary,
              child: Padding(
                padding: kDefaultTableCellPadding,
                child: Center(
                  child: Text(
                    caption,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
