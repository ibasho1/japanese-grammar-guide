import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class TableData extends StatelessWidget {
  const TableData({Key? key, required this.content, this.centered = true}) : super(key: key);
  final Widget content;
  final bool centered;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultTableCellPadding,
      child: centered ? Center(
        child: content,
      ) : content,
    );
  }
}
