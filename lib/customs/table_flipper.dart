import 'dart:async';
import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';

class TableFlipper extends StatefulWidget {
  const TableFlipper({Key? key, required this.value, required this.stream, this.blank = false}) : super(key: key);
  final bool blank;
  final String value;
  final Stream stream;

  @override
  State<TableFlipper> createState() => _TableFlipperState();
}

class _TableFlipperState extends State<TableFlipper> {
  StreamSubscription? streamSubscription;
  bool flipped = false;

@override
  initState() {
    super.initState();
    streamSubscription = widget.stream.listen((flipped) => setFlipped(flipped));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: TableData(
        content: flipped ? Text(widget.value) : widget.blank ? const Text(' ') : const Text('flip'),
      ),
      onTap: () {
        setState(() {
          flipped = !flipped;
        });
      },
    );
  }

  setFlipped(bool flipped) {
    setState(() {
      this.flipped = flipped;
    });
  } 
}
