import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';

class TableHeading extends StatelessWidget {
  const TableHeading({Key? key, required this.text}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultTableCellPadding,
      child: Center(
        child: Text(
          text,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
