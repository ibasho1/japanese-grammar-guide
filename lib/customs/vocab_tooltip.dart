import 'package:flutter/material.dart';

class VocabTooltip extends WidgetSpan {
  VocabTooltip({required this.message, required this.inlineSpan})
      : super(
          baseline: TextBaseline.alphabetic,
          alignment: PlaceholderAlignment.baseline,
          child: Tooltip(
            message: message,
            child: Text.rich(
              inlineSpan,
            ),
          ),
        );
  final InlineSpan inlineSpan;
  final String message;
}
