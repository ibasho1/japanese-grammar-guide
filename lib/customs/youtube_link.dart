import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class YouTubeLink extends StatelessWidget {
  const YouTubeLink({Key? key, required this.title, required this.uri})
      : super(key: key);
  final String title;
  final String uri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kDefaultParagraphPadding,
      child: InkWell(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: SvgPicture.asset(
                'assets/icons/youtube2.svg',
                height: 48,
                semanticsLabel: 'YouTube Logo',
              ),
            ),
            Expanded(
              child: SelectionContainer.disabled(
                child: Text(
                  title,
                ),
              ),
            ),
          ],
        ),
        onTap: () async {
          if (!await launchUrl(Uri.parse(uri))) {
            throw Exception('Could not launch $uri');
          }
        },
      ),
    );
  }
}
