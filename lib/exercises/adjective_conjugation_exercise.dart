import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class AdjectiveConjugationExercise extends StatefulWidget {
  const AdjectiveConjugationExercise({Key? key}) : super(key: key);

  @override
  State<AdjectiveConjugationExercise> createState() => _AdjectiveConjugationExerciseState();
}

class _AdjectiveConjugationExerciseState extends State<AdjectiveConjugationExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Table(
            border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
            children: [
              const TableRow(
                children: [
                  TableHeading(
                    text: 'plain',
                  ),
                  TableHeading(
                    text: 'declarative',
                  ),
                  TableHeading(
                    text: 'negative',
                  ),
                  TableHeading(
                    text: 'past',
                  ),
                  TableHeading(
                    text: 'negative-past',
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '面白い',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '面白くない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '面白かった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '面白くなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '有名',
                  ),
                  TableFlipper(
                    value: '有名だ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '有名じゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '有名だった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '有名じゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '嫌い',
                  ),
                  TableFlipper(
                    value: '嫌いだ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '嫌いじゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '嫌いだった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '嫌いじゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '好き',
                  ),
                  TableFlipper(
                    value: '好きだ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '好きじゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '好きだった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '好きじゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '大きい',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大きくない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大きかった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大きくなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'きれい',
                  ),
                  TableFlipper(
                    value: 'きれいだ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'きれいじゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'きれいだった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'きれいじゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '小さい',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '小さくない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '小さかった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '小さくなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'いい',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'よくない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'よかった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'よくなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '静か',
                  ),
                  TableFlipper(
                    value: '静かだ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '静かじゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '静かだった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '静かじゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '高い',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '高くない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '高かった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '高くなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'かっこいい',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'かっこよくない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'かっこよかった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: 'かっこよくなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '楽しい',
                  ),
                  TableFlipper(
                    value: 'n/a',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '楽しくない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '楽しかった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '楽しくなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '大切',
                  ),
                  TableFlipper(
                    value: '大切だ',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大切じゃない',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大切だった',
                    stream: changeNotifier.stream,
                  ),
                  TableFlipper(
                    value: '大切じゃなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
