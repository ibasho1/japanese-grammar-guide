import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';

class BasicExercise extends StatefulWidget {
  const BasicExercise({Key? key, required this.entries}) : super(key: key);

  final Map<String, String> entries;

  @override
  State<BasicExercise> createState() => _BasicExerciseState();
}

class _BasicExerciseState extends State<BasicExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NumberedList(
          items: [
            for (MapEntry e in widget.entries.entries)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(child: Text(e.key)),
                  const SizedBox(width: 15,),
                  const Text('='),
                  const SizedBox(width: 15,),
                  Expanded(child: Flipper(value: e.value, stream: changeNotifier.stream))
                ],
              )
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
