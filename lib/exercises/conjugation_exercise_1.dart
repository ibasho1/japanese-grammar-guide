import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';

class ConjugationExercise1 extends StatefulWidget {
  const ConjugationExercise1({Key? key}) : super(key: key);

  @override
  State<ConjugationExercise1> createState() => _ConjugationExercise1State();
}

class _ConjugationExercise1State extends State<ConjugationExercise1> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NumberedList(
          items: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'これ',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('declarative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: 'これだ', stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: 'これじゃない',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: 'これだった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative-past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: 'これじゃなかった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  '大人',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('declarative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '大人だ', stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '大人じゃない',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '大人だった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative-past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '大人じゃなかった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  '学校',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('declarative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学校だ', stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学校じゃない',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学校だった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative-past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学校じゃなかった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  '友達',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('declarative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '友達だ', stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '友達じゃない',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '友達だった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative-past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '友達じゃなかった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  '学生',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('declarative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学生だ', stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学生じゃない',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学生だった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: Text('negative-past')),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text('='),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                              child: Flipper(
                                  value: '学生じゃなかった',
                                  stream: changeNotifier.stream))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
