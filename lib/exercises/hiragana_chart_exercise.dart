import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class HiraganaChartExercise extends StatefulWidget {
  const HiraganaChartExercise({Key? key}) : super(key: key);

  @override
  State<HiraganaChartExercise> createState() => _HiraganaChartExerciseState();
}

class _HiraganaChartExerciseState extends State<HiraganaChartExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const TableCaption(caption: 'Hiragana Table'),
              Table(
                border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
                children: [
                  const TableRow(
                    children: [
                      TableHeading(
                        text: 'n',
                      ),
                      TableHeading(
                        text: 'w',
                      ),
                      TableHeading(
                        text: 'r',
                      ),
                      TableHeading(
                        text: 'y',
                      ),
                      TableHeading(
                        text: 'm',
                      ),
                      TableHeading(
                        text: 'h',
                      ),
                      TableHeading(
                        text: 'n',
                      ),
                      TableHeading(
                        text: 't',
                      ),
                      TableHeading(
                        text: 's',
                      ),
                      TableHeading(
                        text: 'k',
                      ),
                      TableHeading(
                        text: ' ',
                      ),
                      TableHeading(
                        text: ' ',
                      )
                    ],
                  ),
                  TableRow(
                    children: [
                      TableFlipper(
                        value: 'ん',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'わ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ら',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'や',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ま',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'は',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'な',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'た',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'さ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'か',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'あ',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'a',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'り',
                        stream: changeNotifier.stream,
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'み',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ひ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'に',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ち',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'し',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'き',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'い',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'i',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'る',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ゆ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'む',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ふ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ぬ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'つ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'す',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'く',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'う',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'u',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'れ',
                        stream: changeNotifier.stream,
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'め',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'へ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ね',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'て',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'せ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'け',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'え',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'e',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'を',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ろ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'よ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'も',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ほ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'の',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'と',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'そ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'こ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'お',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'o',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
