import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class KatakanaChartExercise extends StatefulWidget {
  const KatakanaChartExercise({Key? key}) : super(key: key);

  @override
  State<KatakanaChartExercise> createState() => _KatakanaChartExerciseState();
}

class _KatakanaChartExerciseState extends State<KatakanaChartExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Column(
            children: [
              const TableCaption(caption: 'Katakana Table'),
              Table(
                border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
                children: [
                  const TableRow(
                    children: [
                      TableHeading(
                        text: 'n',
                      ),
                      TableHeading(
                        text: 'w',
                      ),
                      TableHeading(
                        text: 'r',
                      ),
                      TableHeading(
                        text: 'y',
                      ),
                      TableHeading(
                        text: 'm',
                      ),
                      TableHeading(
                        text: 'h',
                      ),
                      TableHeading(
                        text: 'n',
                      ),
                      TableHeading(
                        text: 't',
                      ),
                      TableHeading(
                        text: 's',
                      ),
                      TableHeading(
                        text: 'k',
                      ),
                      TableHeading(
                        text: ' ',
                      ),
                      TableHeading(
                        text: ' ',
                      )
                    ],
                  ),
                  TableRow(
                    children: [
                      TableFlipper(
                        value: 'ン',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ワ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ラ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ヤ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'マ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ハ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ナ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'タ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'サ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'カ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ア',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'a',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'リ',
                        stream: changeNotifier.stream,
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'ミ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ヒ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ニ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'チ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'シ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'キ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'イ',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'i',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'ル',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ユ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ム',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'フ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ヌ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ツ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ス',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ク',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ウ',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'u',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'レ',
                        stream: changeNotifier.stream,
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'メ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ヘ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ネ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'テ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'セ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ケ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'エ',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'e',
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableData(
                        content: Text(' '),
                      ),
                      const TableData(
                        content: Text(' '),
                      ),
                      TableFlipper(
                        value: 'ロ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ヨ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'モ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ホ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ノ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ト',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'ソ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'コ',
                        stream: changeNotifier.stream,
                      ),
                      TableFlipper(
                        value: 'オ',
                        stream: changeNotifier.stream,
                      ),
                      const TableHeading(
                        text: 'o',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
