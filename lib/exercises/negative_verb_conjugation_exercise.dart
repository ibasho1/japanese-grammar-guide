import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class NegativeVerbConjugationExercise extends StatefulWidget {
  const NegativeVerbConjugationExercise({Key? key}) : super(key: key);

  @override
  State<NegativeVerbConjugationExercise> createState() => _NegativeVerbConjugationExerciseState();
}

class _NegativeVerbConjugationExerciseState extends State<NegativeVerbConjugationExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Table(
            border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
            children: [
              const TableRow(
                children: [
                  TableHeading(
                    text: 'verb',
                  ),
                  TableHeading(
                    text: 'negative',
                  ),
                ],
              ),
              const TableRow(
                children: [
                  TableHeading(
                    text: '行く',
                  ),
                  TableData(
                    content: Text('行かない'),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '出る',
                  ),
                  TableFlipper(
                    value: '出ない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'する',
                  ),
                  TableFlipper(
                    value: 'しない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '買う',
                  ),
                  TableFlipper(
                    value: '買わない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '売る',
                  ),
                  TableFlipper(
                    value: '売らない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '食べる',
                  ),
                  TableFlipper(
                    value: '食べない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '入る',
                  ),
                  TableFlipper(
                    value: '入らない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '来る',
                  ),
                  TableFlipper(
                    value: 'こない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '飲む',
                  ),
                  TableFlipper(
                    value: '飲まない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'しゃべる',
                  ),
                  TableFlipper(
                    value: 'しゃべらない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '見る',
                  ),
                  TableFlipper(
                    value: '見ない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '切る',
                  ),
                  TableFlipper(
                    value: '切らない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '帰る',
                  ),
                  TableFlipper(
                    value: '帰らない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '書く',
                  ),
                  TableFlipper(
                    value: '書かない',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
