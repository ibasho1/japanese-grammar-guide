import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';

class ParticleExerciseWa extends StatefulWidget {
  const ParticleExerciseWa({Key? key}) : super(key: key);

  @override
  State<ParticleExerciseWa> createState() => _ParticleExerciseWaState();
}

class _ParticleExerciseWaState extends State<ParticleExerciseWa> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  // TODO: Figure out whether there's a better way
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NumberedList(
          items: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('どこ？'),
                    Text('(Topic: 学校)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '学校はどこ？', stream: changeNotifier.stream),
                    const Text('(Where is school?)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('どうして？'),
                    Text('(Topic: それ)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: 'それはどうして？', stream: changeNotifier.stream),
                    const Text('(Why is that?)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('いつ？'),
                    Text('(Topic: ミーティング)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: 'ミーティングはいつ？', stream: changeNotifier.stream),
                    const Text('(When is meeting?)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('何？'),
                    Text('(Topic: これ)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: 'これは何？', stream: changeNotifier.stream),
                    const Text('(What is this?)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('どう？'),
                    Text('(Topic: 映画)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '映画はどう？', stream: changeNotifier.stream),
                    const Text('(How is movie?)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('中学生だ。'),
                    Text('(Topic: 彼)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '彼は中学生だ。', stream: changeNotifier.stream),
                    const Text('	(He is middle school student.)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('先生だ。'),
                    Text('(Topic: 彼女)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '彼女は先生だ。', stream: changeNotifier.stream),
                    const Text('(She is teacher.)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('雨。'),
                    Text('(Topic: 今日)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '今日は雨。', stream: changeNotifier.stream),
                    const Text('(Today is rain.)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('友達。'),
                    Text('(Topic: ボブ)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: 'ボブは友達。', stream: changeNotifier.stream),
                    const Text('(Bob is friend.)'),
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(' 知り合い？'),
                    Text('(Topic: 彼)'),
                  ],
                ),
                const Text('＝	'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flipper(value: '彼は知り合い？', stream: changeNotifier.stream),
                    const Text('(Is he an acquaintance?)'),
                  ],
                ),
              ],
            ),
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
