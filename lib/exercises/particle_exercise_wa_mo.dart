import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';

class ParticleExerciseWaMo extends StatefulWidget {
  const ParticleExerciseWaMo({Key? key}) : super(key: key);

  @override
  State<ParticleExerciseWaMo> createState() => _ParticleExerciseWaMoState();
}

class _ParticleExerciseWaMoState extends State<ParticleExerciseWaMo> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NumberedList(
          items: [
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: '今日は雨だ。昨日',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　も　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: '雨だった。',
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: 'ジムは大学生だ。でも、私',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　は　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: '大学生じゃない。',
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: 'これは水。これ',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　も　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: 'そう。',
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: 'これはボールペンだ。でも、それ',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　は　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: 'ボールペンじゃない。',
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: '仕事は明日。今日',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　は　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: '仕事じゃなかった。',
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: 'ここは入口。出口',
                  ),
                  WidgetSpan(
                    baseline: TextBaseline.alphabetic,
                    alignment: PlaceholderAlignment.baseline,
                    child: Flipper(value: '　も　', stream: changeNotifier.stream),
                  ),
                  const TextSpan(
                    text: 'ここだ。',
                  ),
                ],
              ),
            ),
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
