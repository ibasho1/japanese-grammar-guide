import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class ParticleExerciseWaMoGa extends StatefulWidget {
  const ParticleExerciseWaMoGa({Key? key}) : super(key: key);

  @override
  State<ParticleExerciseWaMoGa> createState() => _ParticleExerciseWaMoGaState();
}

class _ParticleExerciseWaMoGaState extends State<ParticleExerciseWaMoGa> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Table(
                columnWidths: const {
                  0: IntrinsicColumnWidth(),
                  1: FlexColumnWidth(),
                },
                children: [
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ジム：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'アリス',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '誰？',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ボブ：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: '友達だ。彼女',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　が　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: 'アリスだ。',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  const TableRow(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'アリス：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'これ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '何？',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ボブ：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'それ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '鉛筆。',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'アリス：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'あれ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　も　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '鉛筆？',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ボブ：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'あれ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: 'ペンだ。',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  const TableRow(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'アリス：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: '図書館',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: 'どこ？',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ボブ：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'ここ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　が　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '図書館だ。',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'アリス：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'そこ',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: '図書館じゃない？',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      const TableHeading(
                        text: 'ボブ：',
                      ),
                      TableData(
                        centered: false,
                        content: Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'そこじゃない。図書館',
                              ),
                              WidgetSpan(
                                baseline: TextBaseline.alphabetic,
                                alignment: PlaceholderAlignment.baseline,
                                child: Flipper(
                                    value: '　は　',
                                    stream: changeNotifier.stream),
                              ),
                              const TextSpan(
                                text: 'ここだ。',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: kDefaultParagraphPadding,
            child: Row(
              children: [
                TextButton(
                  onPressed: () {
                    changeNotifier.sink.add(true);
                  },
                  child: const Text('Show all answers'),
                ),
                TextButton(
                  onPressed: () {
                    changeNotifier.sink.add(false);
                  },
                  child: const Text('Hide all answers'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
