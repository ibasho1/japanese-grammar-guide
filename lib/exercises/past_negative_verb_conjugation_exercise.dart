import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class PastNegativeVerbConjugationExercise extends StatefulWidget {
  const PastNegativeVerbConjugationExercise({Key? key}) : super(key: key);

  @override
  State<PastNegativeVerbConjugationExercise> createState() => _PastNegativeVerbConjugationExerciseState();
}

class _PastNegativeVerbConjugationExerciseState extends State<PastNegativeVerbConjugationExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Table(
            border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
            children: [
              const TableRow(
                children: [
                  TableHeading(
                    text: 'verb',
                  ),
                  TableHeading(
                    text: 'past negative tense',
                  ),
                ],
              ),
              const TableRow(
                children: [
                  TableHeading(
                    text: '出る',
                  ),
                  TableData(
                    content: Text('出なかった'),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '行く',
                  ),
                  TableFlipper(
                    value: '行かなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'する',
                  ),
                  TableFlipper(
                    value: 'しなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '買う',
                  ),
                  TableFlipper(
                    value: '買わなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '売る',
                  ),
                  TableFlipper(
                    value: '売らなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '食べる',
                  ),
                  TableFlipper(
                    value: '食べなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '入る',
                  ),
                  TableFlipper(
                    value: '入らなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '来る',
                  ),
                  TableFlipper(
                    value: 'こなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '飲む',
                  ),
                  TableFlipper(
                    value: '飲まなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'しゃべる',
                  ),
                  TableFlipper(
                    value: 'しゃべらなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '見る',
                  ),
                  TableFlipper(
                    value: '見なかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '切る',
                  ),
                  TableFlipper(
                    value: '切らなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '帰る',
                  ),
                  TableFlipper(
                    value: '帰らなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '書く',
                  ),
                  TableFlipper(
                    value: '書かなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '待つ',
                  ),
                  TableFlipper(
                    value: '待たなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '話す',
                  ),
                  TableFlipper(
                    value: '話さなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '泳ぐ',
                  ),
                  TableFlipper(
                    value: '泳がなかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '死ぬ',
                  ),
                  TableFlipper(
                    value: '死ななかった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
