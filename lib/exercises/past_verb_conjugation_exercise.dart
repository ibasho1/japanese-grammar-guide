import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class PastVerbConjugationExercise extends StatefulWidget {
  const PastVerbConjugationExercise({Key? key}) : super(key: key);

  @override
  State<PastVerbConjugationExercise> createState() => _PastVerbConjugationExerciseState();
}

class _PastVerbConjugationExerciseState extends State<PastVerbConjugationExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Table(
            border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
            children: [
              const TableRow(
                children: [
                  TableHeading(
                    text: 'verb',
                  ),
                  TableHeading(
                    text: 'past tense',
                  ),
                ],
              ),
              const TableRow(
                children: [
                  TableHeading(
                    text: '出る',
                  ),
                  TableData(
                    content: Text('出た'),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '行く',
                  ),
                  TableFlipper(
                    value: '行った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'する',
                  ),
                  TableFlipper(
                    value: 'した',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '買う',
                  ),
                  TableFlipper(
                    value: '買った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '売る',
                  ),
                  TableFlipper(
                    value: '売った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '食べる',
                  ),
                  TableFlipper(
                    value: '食べた',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '入る',
                  ),
                  TableFlipper(
                    value: '入った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '来る',
                  ),
                  TableFlipper(
                    value: 'きた',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '飲む',
                  ),
                  TableFlipper(
                    value: '飲んだ',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'しゃべる',
                  ),
                  TableFlipper(
                    value: 'しゃべった',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '見る',
                  ),
                  TableFlipper(
                    value: '見た',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '切る',
                  ),
                  TableFlipper(
                    value: '切った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '帰る',
                  ),
                  TableFlipper(
                    value: '帰った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '書く',
                  ),
                  TableFlipper(
                    value: '書いた',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '待つ',
                  ),
                  TableFlipper(
                    value: '待った',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '話す',
                  ),
                  TableFlipper(
                    value: '話した',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '泳ぐ',
                  ),
                  TableFlipper(
                    value: '泳いだ',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '死ぬ',
                  ),
                  TableFlipper(
                    value: '死んだ',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
