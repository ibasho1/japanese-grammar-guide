import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';

class QuestionAnswerExercise extends StatefulWidget {
  const QuestionAnswerExercise({Key? key}) : super(key: key);

  @override
  State<QuestionAnswerExercise> createState() => _QuestionAnswerExerciseState();
}

class _QuestionAnswerExerciseState extends State<QuestionAnswerExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ１）　'),
                Text('友達？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ１）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'うん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '友達', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。 (female)',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ２）　'),
                Text('学校？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ２）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '学校じゃない', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ３）　'),
                Text('それだった？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ３）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: 'それじゃなかった', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ４）　'),
                Text('そう？ (Is that so?)'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ４）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'うん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: 'そうだ', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。 (male)',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ５）　'),
                Text('これ？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ５）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: 'それじゃない', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。 (object is away from the speaker)',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ６）　'),
                Text('先生だった？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ６）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'うん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '先生だった', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ７）　'),
                Text('小学校だった？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ７）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '小学校じゃなかった',
                              stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Ｑ８）　'),
                Text('子供？'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Ａ８）　'),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'うん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '子供', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。 (female)',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
