import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/flipper.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class SentenceCompletionExercise extends StatefulWidget {
  const SentenceCompletionExercise({Key? key}) : super(key: key);

  @override
  State<SentenceCompletionExercise> createState() =>
      _SentenceCompletionExerciseState();
}

class _SentenceCompletionExerciseState
    extends State<SentenceCompletionExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Table(
          columnWidths: const {
            0: IntrinsicColumnWidth(),
            1: FlexColumnWidth(),
          },
          children: [
            TableRow(
              children: [
                const TableHeading(
                  text: 'ジム：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'アリス、今',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　は　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '忙しい？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '忙しくない', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: '何',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　が　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '楽しい？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ゲーム、',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　が　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '楽しい。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        WidgetSpan(
                          child: Flipper(
                              value: '大切な', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '人は誰？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ジム',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　が　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '大切だ。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        WidgetSpan(
                          child: Flipper(
                              value: '辛い', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '料理は、好き？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、辛くない料理',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　が　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '好きだ。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ジム',
                        ),
                        WidgetSpan(
                          baseline: TextBaseline.alphabetic,
                          alignment: PlaceholderAlignment.baseline,
                          child: Flipper(
                              value: '　は　', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '、かっこいい人？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: 'かっこよくない', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ボブは、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: '有名な', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '人？',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text(
                    'ううん、有名じゃない。',
                  ),
                ),
              ],
            ),
            const TableRow(
              children: [
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            const TableRow(
              children: [
                TableHeading(
                  text: 'アリス：',
                ),
                TableData(
                  centered: false,
                  content: Text('昨日のテストは、よかった？'),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableHeading(
                  text: 'ボブ：',
                ),
                TableData(
                  centered: false,
                  content: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'ううん、',
                        ),
                        WidgetSpan(
                          child: Flipper(
                              value: 'よくなかった', stream: changeNotifier.stream),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
