import 'dart:async';

import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_flipper.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';

class VerbClassificationExercise extends StatefulWidget {
  const VerbClassificationExercise({Key? key}) : super(key: key);

  @override
  State<VerbClassificationExercise> createState() => _VerbClassificationExerciseState();
}

class _VerbClassificationExerciseState extends State<VerbClassificationExercise> {
  final changeNotifier = StreamController.broadcast();

  @override
  void dispose() {
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: kDefaultParagraphPadding,
          child: Table(
            border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
            children: [
              const TableRow(
                children: [
                  TableHeading(
                    text: 'verb',
                  ),
                  TableHeading(
                    text: 'ru-verb',
                  ),
                  TableHeading(
                    text: 'u-verb',
                  ),
                  TableHeading(
                    text: 'exception verb',
                  ),
                ],
              ),
              const TableRow(
                children: [
                  TableHeading(
                    text: '行く',
                  ),
                  TableData(
                    content: Text(' '),
                  ),
                  TableData(
                    centered: true,
                    content: Text('●'),
                  ),
                  TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '出る',
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'する',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '買う',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '売る',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '食べる',
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '入る',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '来る',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '飲む',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: 'しゃべる',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '見る',
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '切る',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '帰る',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
              TableRow(
                children: [
                  const TableHeading(
                    text: '書く',
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                  TableFlipper(
                    blank: true,
                    value: '●',
                    stream: changeNotifier.stream,
                  ),
                  const TableData(
                    content: Text(' '),
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: kDefaultParagraphPadding,
          child: Row(
            children: [
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(true);
                },
                child: const Text('Show all answers'),
              ),
              TextButton(
                onPressed: () {
                  changeNotifier.sink.add(false);
                },
                child: const Text('Hide all answers'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
