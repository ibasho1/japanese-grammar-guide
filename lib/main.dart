import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/bookmarks/bookmarks_manager.dart';
import 'package:japanese_grammar_guide/routes/router.dart';
import 'package:japanese_grammar_guide/preferences/preferences.dart';
import 'package:japanese_grammar_guide/theme/theme_mode_manager.dart';
import 'package:japanese_grammar_guide/theme/themes.dart';
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // DartPluginRegistrant.ensureInitialized();
  await Preferences.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => ThemeModeManager(ThemeMode.system)),
      ChangeNotifierProvider(create: (_) => BookmarksManager([])),
    ], child: const MaterialAppRouter());
  }
}

class MaterialAppRouter extends StatefulWidget {
  const MaterialAppRouter({super.key});

  @override
  State<MaterialAppRouter> createState() => _MaterialAppRouterState();
}

class _MaterialAppRouterState extends State<MaterialAppRouter> {
  late RouterConfig<Object> _routerConfig;

  @override
  void initState() {
    super.initState();
    AppRouter appRouter = AppRouter();
    _routerConfig = appRouter.config();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeModeManager>(context);
    theme.loadTheme();
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: _routerConfig,
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: theme.themeMode,
    );
  }
}
