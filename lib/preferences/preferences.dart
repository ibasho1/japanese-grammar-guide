import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static SharedPreferences? _preferences;

  static const _themeKey = 'theme';

  static const _bookmarksKey = 'bookmarks';

  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static Future addBookmark(List<int> bookmark) async {
    String stringBookmark =
        bookmark.map((i) => i.toString()).toList().join(' ');
    List<String>? bookmarks;
    _preferences?.getStringList(_bookmarksKey) != null ? bookmarks = _preferences?.getStringList(_bookmarksKey) : bookmarks = [];
    bookmarks?.add(stringBookmark);
    await _preferences?.setStringList(_bookmarksKey, bookmarks!);
  }

  static Future removeBookmark(List<int> bookmark) async {
    String stringBookmark =
        bookmark.map((i) => i.toString()).toList().join(' ');
    List<String>? bookmarks = _preferences?.getStringList(_bookmarksKey);
    bookmarks?.remove(stringBookmark);
    await _preferences?.setStringList(_bookmarksKey, bookmarks!);
  }

  static List<List<int>> getBookmarks() {
    var bookmarks = _preferences?.getStringList(_bookmarksKey);
    List<List<int>> intBookmarks = [];
    if (bookmarks != null) {
      for (var bookmark in bookmarks) {
        intBookmarks.add(bookmark.split(' ').map((i) => int.parse(i)).toList());
      }
    }
    return intBookmarks;
  }

  static bool containsBookmark(List<int> bookmark) {
    String stringBookmark =
        bookmark.map((i) => i.toString()).toList().join(' ');
    List<String>? bookmarks = _preferences?.getStringList(_bookmarksKey);
    return bookmarks?.contains(stringBookmark) ?? false;
  }

  static Future clearBookmarks() async {
    await _preferences?.setStringList(_bookmarksKey, []);
  }

  static Future setTheme(String theme) async {
    await _preferences?.setString(_themeKey, theme);
  }

  static String? getTheme() {
    return _preferences?.getString(_themeKey);
  }

  static ThemeMode getThemeMode() {
    if (getTheme() == 'light') {
      return ThemeMode.light;
    } else if (getTheme() == 'dark') {
      return ThemeMode.dark;
    } else {
      return ThemeMode.system;
    }
  }
}
