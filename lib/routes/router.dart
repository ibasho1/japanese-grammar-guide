import 'package:auto_route/auto_route.dart';
import 'router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/',
          page: Home.page,
          initial: true,
          children: [
            AutoRoute(
              path: 'guide',
              page: Guide.page,
              children: [
                AutoRoute(
                  path: 'lessons',
                  page: Lessons.page,
                  initial: true,
                ),
                AutoRoute(
                  path: 'lessons/:lesson',
                  page: LessonRoute.page,
                ),
              ],
            ),
            AutoRoute(
              path: 'settings',
              page: Settings.page,
            ),
            AutoRoute(
              path: 'about',
              page: About.page,
            )
          ],
        )
      ];
}
