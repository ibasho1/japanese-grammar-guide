// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;
import 'package:japanese_grammar_guide/home.dart' as _i3;
import 'package:japanese_grammar_guide/tabs/about.dart' as _i1;
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart' as _i9;
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson_page.dart'
    as _i4;
import 'package:japanese_grammar_guide/tabs/grammar_guide/lessons.dart' as _i5;
import 'package:japanese_grammar_guide/tabs/guide.dart' as _i2;
import 'package:japanese_grammar_guide/tabs/settings.dart' as _i6;

abstract class $AppRouter extends _i7.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    About.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.About(),
      );
    },
    Guide.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.Guide(),
      );
    },
    Home.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.Home(),
      );
    },
    LessonRoute.name: (routeData) {
      final args = routeData.argsAs<LessonRouteArgs>();
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.LessonPage(
          key: args.key,
          lesson: args.lesson,
          section: args.section,
          backEnabled: args.backEnabled,
        ),
      );
    },
    Lessons.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i5.Lessons(),
      );
    },
    Settings.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.Settings(),
      );
    },
  };
}

/// generated route for
/// [_i1.About]
class About extends _i7.PageRouteInfo<void> {
  const About({List<_i7.PageRouteInfo>? children})
      : super(
          About.name,
          initialChildren: children,
        );

  static const String name = 'About';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i2.Guide]
class Guide extends _i7.PageRouteInfo<void> {
  const Guide({List<_i7.PageRouteInfo>? children})
      : super(
          Guide.name,
          initialChildren: children,
        );

  static const String name = 'Guide';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i3.Home]
class Home extends _i7.PageRouteInfo<void> {
  const Home({List<_i7.PageRouteInfo>? children})
      : super(
          Home.name,
          initialChildren: children,
        );

  static const String name = 'Home';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i4.LessonPage]
class LessonRoute extends _i7.PageRouteInfo<LessonRouteArgs> {
  LessonRoute({
    _i8.Key? key,
    required _i9.Lesson lesson,
    int section = 0,
    bool backEnabled = false,
    List<_i7.PageRouteInfo>? children,
  }) : super(
          LessonRoute.name,
          args: LessonRouteArgs(
            key: key,
            lesson: lesson,
            section: section,
            backEnabled: backEnabled,
          ),
          initialChildren: children,
        );

  static const String name = 'LessonRoute';

  static const _i7.PageInfo<LessonRouteArgs> page =
      _i7.PageInfo<LessonRouteArgs>(name);
}

class LessonRouteArgs {
  const LessonRouteArgs({
    this.key,
    required this.lesson,
    this.section = 0,
    this.backEnabled = false,
  });

  final _i8.Key? key;

  final _i9.Lesson lesson;

  final int section;

  final bool backEnabled;

  @override
  String toString() {
    return 'LessonRouteArgs{key: $key, lesson: $lesson, section: $section, backEnabled: $backEnabled}';
  }
}

/// generated route for
/// [_i5.Lessons]
class Lessons extends _i7.PageRouteInfo<void> {
  const Lessons({List<_i7.PageRouteInfo>? children})
      : super(
          Lessons.name,
          initialChildren: children,
        );

  static const String name = 'Lessons';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i6.Settings]
class Settings extends _i7.PageRouteInfo<void> {
  const Settings({List<_i7.PageRouteInfo>? children})
      : super(
          Settings.name,
          initialChildren: children,
        );

  static const String name = 'Settings';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}
