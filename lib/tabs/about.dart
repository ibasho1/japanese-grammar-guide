import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';

@RoutePage()
class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('About'),
      ),
      body: SelectionArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: kDefaultPagePadding,
            child: Padding(
              padding: kDefaultSectionPadding,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Heading(text: 'About this app', level: 1),
                  const Heading(text: 'Credit', level: 2),
                  Paragraph(
                    texts: [
                      const TextSpan(
                          text:
                              'This application is a Flutter port of Tae Kim’s ‘A Guide to Japanese Grammar’ '),
                      ExternalLink(
                        inlineSpan: const TextSpan(
                          text: '(CC BY-NC-SA 3.0 US)',
                        ),
                        uri:
                            'https://creativecommons.org/licenses/by-nc-sa/3.0/us/',
                      ),
                      const TextSpan(text: ', which can be found online '),
                      ExternalLink(
                        inlineSpan: const TextSpan(
                          text: 'here',
                        ),
                        uri: 'https://guidetojapanese.org/learn/grammar',
                      ),
                      const TextSpan(text: '.'),
                    ],
                  ),
                  const Heading(text: 'Changes Made', level: 2),
                  const Paragraph(
                    texts: [
                      TextSpan(
                          text:
                              'Minor alterations have been made to the grammar guide, including:'),
                    ],
                  ),
                  const BulletedList(
                    items: [
                      Text('The correction of typographical errors'),
                      Text(
                          'The addition of tooltips for Japanese vocabulary in later lessons'),
                      Text(
                          'The reformatting of certain content for better clarity'),
                      Text(
                          'The updating of expired links'),
                    ],
                  ),
                  const Paragraph(texts: [
                    TextSpan(
                        text:
                            'As this app is a work in progress, some copying errors may remain.'),
                  ]),
                  const Heading(text: 'License', level: 2),
                  Paragraph(
                    texts: [
                      const TextSpan(text: 'This work is licensed under '),
                      ExternalLink(
                        inlineSpan: const TextSpan(
                          text:
                              'CC BY-NC-SA 4.0',
                        ),
                        uri: 'https://creativecommons.org/licenses/by-nc-sa/4.0/',
                      ),
                      const TextSpan(text: '.'),
                    ],
                  ),
                  const Heading(text: 'Source Code', level: 2),
                  Paragraph(
                    texts: [
                      const TextSpan(text: 'This app is open source and its source code can be found '),
                      ExternalLink(
                        inlineSpan: const TextSpan(
                          text:
                              'here',
                        ),
                        uri: 'https://gitlab.com/ibasho1/japanese-grammar-guide',
                      ),
                      const TextSpan(text: '.'),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
