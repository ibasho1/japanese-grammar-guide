import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson introduction = Lesson(
  title: 'Introduction',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'This guide was created as a resource for those who want to learn Japanese grammar in a rational, intuitive way that makes sense in Japanese. The explanations are focused on how to make sense of the grammar not from English but from a Japanese point of view.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This guide was created as a resource for those who want to learn Japanese grammar in a rational, intuitive way that makes sense in Japanese. The explanations are focused on how to make sense of the grammar not from English but from a Japanese point of view.',
      grammars: [],
      keywords: ['intro'],
    ),
    Section(
      heading: 'The problem with conventional textbooks',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The problem with conventional textbooks is that they often have the following goals.'),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'They want readers to be able to use functional and polite Japanese as quickly as possible.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'They don’t want to scare readers away with terrifying Japanese script and Chinese characters.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'They want to teach you how to say English phrases in Japanese.',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Traditionally with romance languages such as Spanish, these goals present no problems or are nonexistent due to the similarities to English. However, because Japanese is different in just about every way down to the fundamental ways of thinking, these goals create many of the confusing textbooks you see today. They are usually filled with complicated rules and countless number of grammar for specific English phrases. They also contain almost no Kanji and so when you finally arrive in Japan, lo and behold, you discover you can’t read menus, maps, or essentially anything at all because the book decided you weren’t smart enough to memorize Chinese characters.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The root of this problem lies in the fact that these textbooks try to teach you Japanese with English. They want to teach you on the first page how to say, “Hi, my name is Smith,” but they don’t tell you about all the arbitrary decisions that were made behind your back. They probably decided to use the polite form even though learning the polite form before the dictionary form makes no sense. They also might have decided to include the subject even though it’s not necessary and omitted most of the time. In fact, the most common way to say something like “My name is Smith” in Japanese is to say “Smith”. That’s because most of the information is understood from the context and is therefore omitted. But do most textbooks explain the way things work in Japanese fundamentally? No, because they’re too busy trying to push you out the door with “useful” phrases right off the bat. The result is a confusing mess of “use this if you want to say this” type of text and the reader is left with a feeling of confusion about how things actually '),
                  TextSpan(
                    text: 'work',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                    text: '.',
                  ),
                ],
              ),
              Paragraph(texts: [
                TextSpan(
                  text:
                      'The solution to this problem is to explain Japanese from a Japanese point of view. Take Japanese and explain how it works and forget about trying to force what you want to say in English into Japanese. To go along with this, it is also important to explain things in an order that makes sense in Japanese. If you need to know [A] in order to understand [B], don’t cover [B] first just because you want to teach a certain phrase.',
                )
              ]),
              Paragraph(
                texts: [
                  TextSpan(
                    text: 'Essentially, what we need is a ',
                  ),
                  TextSpan(
                      text: 'Japanese',
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  TextSpan(
                    text: ' guide to learning Japanese grammar.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      
      plaintext: 'The problem with conventional textbooks is that they often have the following goals. They want readers to be able to use functional and polite Japanese as quickly as possible. They don’t want to scare readers away with terrifying Japanese script and Chinese characters. They want to teach you how to say English phrases in Japanese. Traditionally with romance languages such as Spanish, these goals present no problems or are nonexistent due to the similarities to English. However, because Japanese is different in just about every way down to the fundamental ways of thinking, these goals create many of the confusing textbooks you see today. They are usually filled with complicated rules and countless number of grammar for specific English phrases. They also contain almost no Kanji and so when you finally arrive in Japan, lo and behold, you discover you can’t read menus, maps, or essentially anything at all because the book decided you weren’t smart enough to memorize Chinese characters. The root of this problem lies in the fact that these textbooks try to teach you Japanese with English. They want to teach you on the first page how to say, “Hi, my name is Smith,” but they don’t tell you about all the arbitrary decisions that were made behind your back. They probably decided to use the polite form even though learning the polite form before the dictionary form makes no sense. They also might have decided to include the subject even though it’s not necessary and omitted most of the time. In fact, the most common way to say something like “My name is Smith” in Japanese is to say “Smith”. That’s because most of the information is understood from the context and is therefore omitted. But do most textbooks explain the way things work in Japanese fundamentally? No, because they’re too busy trying to push you out the door with “useful” phrases right off the bat. The result is a confusing mess of “use this if you want to say this” type of text and the reader is left with a feeling of confusion about how things actually work. The solution to this problem is to explain Japanese from a Japanese point of view. Take Japanese and explain how it works and forget about trying to force what you want to say in English into Japanese. To go along with this, it is also important to explain things in an order that makes sense in Japanese. If you need to know [A] in order to understand [B], don’t cover [B] first just because you want to teach a certain phrase. Essentially, what we need is a Japanese guide to learning Japanese grammar.',
      grammars: [],
      keywords: [],
    ),
    Section(
      heading: 'A Japanese guide to learning Japanese grammar',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This guide is an attempt to systematically build up the grammatical structures that make up the Japanese language in a way that makes sense in Japanese. It may not be a practical tool for quickly learning immediately usable phrases such as for travel. However, it will logically create successive building blocks that will result in a solid grammatical foundation. For those of you who have learned Japanese from textbooks, you may see some big differences in how the material is ordered and presented. This is because this guide does not seek to forcibly create artificial ties between English and Japanese by presenting the material in a way that makes sense in English. Instead, examples with translations will show how ideas are expressed in Japanese resulting in simpler explanations that are easier to understand.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In the beginning, the English translations for the examples will also be as literal as possible to convey the Japanese sense of the meaning. This will often result in grammatically incorrect translations in English. For example, the translations might not have a subject because Japanese does not require one. In addition, since the articles “the” and “a” do not exist in Japanese, the translations will not have them as well. And since Japanese does not distinguish between a future action and a general statement (such as “I will go to the store” vs. “I go to the store”), no distinction will necessarily be made in the translation. It is my hope that the explanation of the examples will convey an accurate sense of what the sentences actually mean '),
                  TextSpan(
                    text: 'in Japanese',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text:
                        '. Once the reader becomes familiar and comfortable thinking in Japanese, the translations will be less literal in order to make the sentences more readable and focused on the more advanced topics.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Be aware that there are advantages and disadvantages to systematically building a grammatical foundation from the ground up. In Japanese, the most fundamental grammatical concepts are often the most difficult to truly understand. This means that the hardest part of the language will come first. Textbooks usually don’t take this approach; afraid that this will scare away or frustrate those interested in the language. Instead, they try to delay going deeply into the hardest conjugation rules with patchwork and gimmicks so that they can start teaching useful expressions right away. This is a fine approach for some, however; it can create more confusion and trouble along the way, much like building a house on a poor foundation. The hard parts must be covered no matter what. However, if you cover them in the beginning, the easier parts will be all that much easier because they’ll fit nicely on top of the foundation you have built. Japanese is syntactically much more consistent than English. If you learn the hardest conjugation rules, most of remaining grammar builds upon similar or identical rules. The only difficult part from there on is sorting out and remembering all the various possible expressions and combinations in order to use them in the correct situations.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '※Note: You will see half brackets like these: 「」 in the text. These are the Japanese version of quotation marks.',
                  )
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This guide is an attempt to systematically build up the grammatical structures that make up the Japanese language in a way that makes sense in Japanese. It may not be a practical tool for quickly learning immediately usable phrases such as for travel. However, it will logically create successive building blocks that will result in a solid grammatical foundation. For those of you who have learned Japanese from textbooks, you may see some big differences in how the material is ordered and presented. This is because this guide does not seek to forcibly create artificial ties between English and Japanese by presenting the material in a way that makes sense in English. Instead, examples with translations will show how ideas are expressed in Japanese resulting in simpler explanations that are easier to understand. In the beginning, the English translations for the examples will also be as literal as possible to convey the Japanese sense of the meaning. This will often result in grammatically incorrect translations in English. For example, the translations might not have a subject because Japanese does not require one. In addition, since the articles “the” and “a” do not exist in Japanese, the translations will not have them as well. And since Japanese does not distinguish between a future action and a general statement (such as “I will go to the store” vs. “I go to the store”), no distinction will necessarily be made in the translation. It is my hope that the explanation of the examples will convey an accurate sense of what the sentences actually mean in Japanese. Once the reader becomes familiar and comfortable thinking in Japanese, the translations will be less literal in order to make the sentences more readable and focused on the more advanced topics. Be aware that there are advantages and disadvantages to systematically building a grammatical foundation from the ground up. In Japanese, the most fundamental grammatical concepts are often the most difficult to truly understand. This means that the hardest part of the language will come first. Textbooks usually don’t take this approach; afraid that this will scare away or frustrate those interested in the language. Instead, they try to delay going deeply into the hardest conjugation rules with patchwork and gimmicks so that they can start teaching useful expressions right away. This is a fine approach for some, however; it can create more confusion and trouble along the way, much like building a house on a poor foundation. The hard parts must be covered no matter what. However, if you cover them in the beginning, the easier parts will be all that much easier because they’ll fit nicely on top of the foundation you have built. Japanese is syntactically much more consistent than English. If you learn the hardest conjugation rules, most of remaining grammar builds upon similar or identical rules. The only difficult part from there on is sorting out and remembering all the various possible expressions and combinations in order to use them in the correct situations. ※Note: You will see half brackets like these: 「」 in the text. These are the Japanese version of quotation marks.',
      grammars: [],
      keywords: [],
    ),
    Section(
      heading: 'Suggestions',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here’s my advice for practicing Japanese: if you find yourself trying to figure out how to say an English thought in Japanese, save yourself the trouble and stop because you won’t get it right most of the time. You should always keep in mind that '),
                  TextSpan(
                    text:
                        'if you don’t know how to say it already, then you don’t know how to say it.',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' Instead, if you can, ask someone how to say it in Japanese including a full explanation of the answer and start practicing '),
                  TextSpan(
                    text: 'from Japanese',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                      text:
                          '. Language is not a math problem; you don’t have to figure out the answer. If you practice from the answer, you will develop good habits that will help you formulate correct and natural Japanese sentences.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'This is why I’m a firm believer of learning by example. Examples and experience will be your main tools in mastering Japanese. Therefore, even if you don’t understand something completely the first time, just move on and keep referring back as you see more examples. This will allow you to get a better sense of how it’s used in many different contexts. Even this guide will not have all the examples to cover every situation. But lucky for you, Japanese is everywhere, especially on the web. I recommend practicing Japanese as much as possible and referring to this guide only when you cannot understand the grammar. '),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'The Internet alone has a rich variety of reading materials including websites, bulletin boards, and online chat. Buying Japanese books or comic books is also an excellent (and fun) way to increase your vocabulary and practice reading skills. It’s also important to keep in mind that it is impossible to learn good speaking and listening skills without actually conversing in Japanese. Practicing listening and speaking skills with fluent speakers of Japanese is a '),
                  const TextSpan(
                      text: 'must',
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  const TextSpan(
                    text:
                        ' if you wish to master conversational skills. While audio listening material can be very educational, there is nothing better than interacting with a real human for learning pronunciation, intonation, and natural conversation flow. If you have specific questions that are not addressed in this guide, you can ask them on the ',
                  ),
                  ExternalLink(uri: 'https://www.facebook.com/groups/214419481973565/', inlineSpan: const TextSpan(text:'facebook group'),),
                  const TextSpan(
                    text:
                        '.',
                  )
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Don’t feel discouraged by the vast amount of material that you will need to master. Remember that every new word or grammar learned is one step closer to mastering the language!')
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Here’s my advice for practicing Japanese: if you find yourself trying to figure out how to say an English thought in Japanese, save yourself the trouble and stop because you won’t get it right most of the time. You should always keep in mind that if you don’t know how to say it already, then you don’t know how to say it. Instead, if you can, ask someone how to say it in Japanese including a full explanation of the answer and start practicing from Japanese. Language is not a math problem; you don’t have to figure out the answer. If you practice from the answer, you will develop good habits that will help you formulate correct and natural Japanese sentences. This is why I’m a firm believer of learning by example. Examples and experience will be your main tools in mastering Japanese. Therefore, even if you don’t understand something completely the first time, just move on and keep referring back as you see more examples. This will allow you to get a better sense of how it’s used in many different contexts. Even this guide will not have all the examples to cover every situation. But lucky for you, Japanese is everywhere, especially on the web. I recommend practicing Japanese as much as possible and referring to this guide only when you cannot understand the grammar. The Internet alone has a rich variety of reading materials including websites, bulletin boards, and online chat. Buying Japanese books or comic books is also an excellent (and fun) way to increase your vocabulary and practice reading skills. It’s also important to keep in mind that it is impossible to learn good speaking and listening skills without actually conversing in Japanese. Practicing listening and speaking skills with fluent speakers of Japanese is a must if you wish to master conversational skills. While audio listening material can be very educational, there is nothing better than interacting with a real human for learning pronunciation, intonation, and natural conversation flow. If you have specific questions that are not addressed in this guide, you can ask them on the facebook group. Don’t feel discouraged by the vast amount of material that you will need to master. Remember that every new word or grammar learned is one step closer to mastering the language!',
      grammars: [],
      keywords: [],
    ),
  ],
);
