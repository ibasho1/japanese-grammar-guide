import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/audio/audio_player.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/animation_dialog.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/exercises/hiragana_chart_exercise.dart';
import 'package:japanese_grammar_guide/exercises/basic_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson hiragana = Lesson(
  title: 'Hiragana',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(title: 'Learn Japanese from Scratch 1.1.2 - Vowel Sounds', uri: 'https://youtu.be/t1hs-l68lOU'),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Hiragana',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                    text:
                        ' is the basic Japanese phonetic script. It represents every sound in the Japanese language. Therefore, you can theoretically write everything in Hiragana. However, because Japanese is written with no spaces, this will create nearly indecipherable text.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here is a table of Hiragana and similar-sounding English consonant-vowel pronunciations. It is read up to down and right to left, which is how most Japanese books are written. In Japanese, writing the strokes in the correct order and direction is important, especially for Kanji. Because handwritten letters look slightly different from typed letters (just like how ‘a’ looks totally different when typed), you will want to use a resource that uses handwritten style fonts to show you how to write the characters (see below for links). I must also stress the importance of correctly learning how to pronounce each sound. Since every word in Japanese is composed of these sounds, learning an incorrect pronunciation for a letter can severely damage the very foundation on which your pronunciation lies.')
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(
                        caption: 'Hiragana – Tap for stroke order and sound'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'n',
                            ),
                            TableHeading(
                              text: 'w',
                            ),
                            TableHeading(
                              text: 'r',
                            ),
                            TableHeading(
                              text: 'y',
                            ),
                            TableHeading(
                              text: 'm',
                            ),
                            TableHeading(
                              text: 'h',
                            ),
                            TableHeading(
                              text: 'n',
                            ),
                            TableHeading(
                              text: 't',
                            ),
                            TableHeading(
                              text: 's',
                            ),
                            TableHeading(
                              text: 'k',
                            ),
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: ' ',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ん',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(n)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/n.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/n.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'わ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/wa.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/wa.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ら',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ra.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ra.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'や',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ya.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ya.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ま',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ma.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ma.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'は',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ha.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ha.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'な',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/na.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/na.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'た',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ta.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ta.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'さ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sa.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/sa.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'か',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ka.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ka.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'あ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/a.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/a.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'a',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text('ゐ*'),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'り',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ri.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ri.gif', context);
                              },
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'み',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/mi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ひ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/hi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/hi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'に',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ni.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ni.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ち',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(chi)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/chi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/chi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'し',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(shi)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/shi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/shi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'き',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ki.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ki.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'い',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/i.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/i.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'i',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'る',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ru.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ru.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ゆ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/yu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/yu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'む',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/mu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ふ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(fu)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/fu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/fu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぬ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/nu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/nu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'つ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(tsu)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/tsu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/tsu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'す',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/su.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/su.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'く',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ku.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ku.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'う',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/u.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/u.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'u',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text('ゑ*'),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'れ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/re.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/re.gif', context);
                              },
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'め',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/me.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/me.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'へ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/he.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/he.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ね',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ne.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ne.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'て',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/te.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/te.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'せ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/se.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/se.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'け',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ke.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ke.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'え',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/e.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/e.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'e',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'を',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(o)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/wo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/wo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ろ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ro.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ro.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'よ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/yo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/yo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'も',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/mo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ほ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ho.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ho.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'の',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/no.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/no.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'と',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/to.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/to.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'そ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/so.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/so.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'こ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ko.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/ko.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'お',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/o.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'hiragana/o.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'o',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: '* = no longer used'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can listen to the pronunciation for each character by tapping on it in chart.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Hiragana is not too tough to master or teach and as a result, there are a variety of web sites and free programs that are already available on the web. I also suggest recording yourself and comparing the sounds to make sure you’re getting it right.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'When practicing writing Hiragana by hand, the important thing to remember is that the stroke order and direction of the strokes ',
                  ),
                  TextSpan(
                      text: 'matter',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        decoration: TextDecoration.underline,
                        color: Theme.of(context).colorScheme.primary,
                      )),
                  const TextSpan(
                    text:
                        '. There, I underlined, italicized, bolded, and highlighted it to boot. Trust me, you’ll eventually find out why when you read other people’s hasty notes that are nothing more than chicken scrawls. The only thing that will help you is that everybody writes in the same order and so the “flow” of the characters is fairly consistent. I ',
                  ),
                  const TextSpan(
                    text: 'strongly',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' recommend that you pay close attention to stroke order from the beginning starting with Hiragana to avoid falling into bad habits. While there are many tools online that aim to help you learn Hiragana, the best way to learn how to write it is the old fashioned way: a piece of paper and pen/pencil.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '※ As an aside, an old Japanese poem called 「いろは」 was often used as the base for ordering of Hiragana until recent times. The poem contains every single Hiragana character except for 「ん」 which probably did not exist at the time it was written.',
                  ),
                ],
              ),
              const NotesSection(
                heading: 'Notes',
                content: NumberedList(
                  items: [
                    Text(
                      'Except for 「し」、「ち」、「つ」、and 「ん」、you can get a sense of how each letter is pronounced by matching the consonant on the top row to the vowel. For example, 「き」 would become / ki / and 「ゆ」 would become / yu / and so on.',
                    ),
                    Text(
                      'As you can see, not all sounds match the way our consonant system works. As written in the table, 「ち」 is pronounced “chi” and 「つ」 is pronounced “tsu”.',
                    ),
                    Text(
                      'The / r / or / l / sound in Japanese is quite different from any sound in English. It involves more of a roll and a clip by hitting the roof of your mouth with your tongue. Pay careful attention to that whole column.',
                    ),
                    Text(
                      'Pay careful attention to the difference between / tsu / and / su /.',
                    ),
                    Text(
                      'The 「ん」 character is a special character because it is rarely used by itself and does not have a vowel sound. It is attached to another character to add a / n / sound. For example, 「かん」 becomes ‘kan’ instead of ‘ka’, 「まん」 becomes ‘man’ instead of ‘ma’, and so on and so forth.',
                    ),
                    Text(
                      'You must learn the correct stroke order and direction!',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Hiragana is the basic Japanese phonetic script. It represents every sound in the Japanese language. Therefore, you can theoretically write everything in Hiragana. However, because Japanese is written with no spaces, this will create nearly indecipherable text. Here is a table of Hiragana and similar-sounding English consonant-vowel pronunciations. It is read up to down and right to left, which is how most Japanese books are written. In Japanese, writing the strokes in the correct order and direction is important, especially for Kanji. Because handwritten letters look slightly different from typed letters (just like how ‘a’ looks totally different when typed), you will want to use a resource that uses handwritten style fonts to show you how to write the characters (see below for links). I must also stress the importance of correctly learning how to pronounce each sound. Since every word in Japanese is composed of these sounds, learning an incorrect pronunciation for a letter can severely damage the very foundation on which your pronunciation lies. You can listen to the pronunciation for each character by tapping on it in chart. Hiragana is not too tough to master or teach and as a result, there are a variety of web sites and free programs that are already available on the web. I also suggest recording yourself and comparing the sounds to make sure you’re getting it right. When practicing writing Hiragana by hand, the important thing to remember is that the stroke order and direction of the strokes matter. There, I underlined, italicized, bolded, and highlighted it to boot. Trust me, you’ll eventually find out why when you read other people’s hasty notes that are nothing more than chicken scrawls. The only thing that will help you is that everybody writes in the same order and so the “flow” of the characters is fairly consistent. I strongly recommend that you pay close attention to stroke order from the beginning starting with Hiragana to avoid falling into bad habits. While there are many tools online that aim to help you learn Hiragana, the best way to learn how to write it is the old fashioned way: a piece of paper and pen/pencil.※ As an aside, an old Japanese poem called 「いろは」 was often used as the base for ordering of Hiragana until recent times. The poem contains every single Hiragana character except for 「ん」 which probably did not exist at the time it was written. Notes Except for 「し」、「ち」、「つ」、and 「ん」、you can get a sense of how each letter is pronounced by matching the consonant on the top row to the vowel. For example, 「き」 would become / ki / and 「ゆ」 would become / yu / and so on. As you can see, not all sounds match the way our consonant system works. As written in the table, 「ち」 is pronounced “chi” and 「つ」 is pronounced “tsu”. The / r / or / l / sound in Japanese is quite different from any sound in English. It involves more of a roll and a clip by hitting the roof of your mouth with your tongue. Pay careful attention to that whole column. Pay careful attention to the difference between / tsu / and / su /. The 「ん」 character is a special character because it is rarely used by itself and does not have a vowel sound. It is attached to another character to add a / n / sound. For example, 「かん」 becomes ‘kan’ instead of ‘ka’, 「まん」 becomes ‘man’ instead of ‘ma’, and so on and so forth. You must learn the correct stroke order and direction!',
      grammars: ['ひらがな'],
      keywords: ['hiragana','kana'],
    ),
    Section(
      heading: 'The muddied sounds',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(title: 'Learn Japanese from Scratch 1.1.7 - Voiced sounds in Hiragana', uri: 'https://youtu.be/Am-n2HVwVG4'),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Once you memorize all the characters in Hiragana, there are still some additional sounds left to be learned. There are five more consonant sounds that are written by either affixing two tiny lines similar to a double quotation mark called dakuten （濁点） or a tiny circle called handakuten （半濁点）. This essentially creates a “muddy” or less clipped version of the consonant (technically called a voiced consonant or 「濁り」, which literally means to become muddy).'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'All the voiced consonant sounds are shown in the table below.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(
                        caption: 'Voiced Hiragana – Tap for sound'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'p',
                            ),
                            TableHeading(
                              text: 'b',
                            ),
                            TableHeading(
                              text: 'd',
                            ),
                            TableHeading(
                              text: 'z',
                            ),
                            TableHeading(
                              text: 'g',
                            ),
                            TableHeading(
                              text: ' ',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぱ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pa.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ば',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ba.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'だ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/da.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ざ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/za.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'が',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ga.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'a',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぴ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pi.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'び',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/bi.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ぢ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(ji)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ji.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'じ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(ji)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ji.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぎ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/gi.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'i',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぷ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぶ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/bu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'づ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(dzu)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/dzu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ず',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/zu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぐ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/gu.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'u',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぺ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pe.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'べ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/be.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'で',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/de.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぜ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ze.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'げ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ge.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'e',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぽ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/po.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぼ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/bo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ど',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/do.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぞ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/zo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ご',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/go.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'o',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const NotesSection(
                heading: 'Note',
                content: BlankList(
                  items: [
                    Text(
                        'Notice that 「ぢ」 sounds essentially identical to 「じ」 and both are pronounced as / ji /, while 「づ」 is pronounced like / dzu /.'),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Once you memorize all the characters in Hiragana, there are still some additional sounds left to be learned. There are five more consonant sounds that are written by either affixing two tiny lines similar to a double quotation mark called dakuten （濁点） or a tiny circle called handakuten （半濁点）. This essentially creates a “muddy” or less clipped version of the consonant (technically called a voiced consonant or 「濁り」, which literally means to become muddy). All the voiced consonant sounds are shown in the table below. Note Notice that 「ぢ」 sounds essentially identical to 「じ」 and both are pronounced as / ji /, while 「づ」 is pronounced like / dzu /.',
      grammars: [],
      keywords: ['voiced','muddied'],
    ),
    Section(
      heading: 'The small 「や」、「ゆ」、and 「よ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(title: 'Learn Japanese from Scratch 1.1.8 - y vowel and double consonants in Hiragana', uri: 'https://youtu.be/gjh8rHqLrfk'),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can also combine a consonant with a / ya / yu / yo / sound by attaching a small 「や」、「ゆ」、or 「よ」 to the / i / vowel character of each consonant.',
                  )
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(
                        caption:
                            'All small や、ゆ、and よ combinations in Hiragana – Tap for sound'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'p',
                            ),
                            TableHeading(
                              text: 'b',
                            ),
                            TableHeading(
                              text: 'j',
                            ),
                            TableHeading(
                              text: 'g',
                            ),
                            TableHeading(
                              text: 'r',
                            ),
                            TableHeading(
                              text: 'm',
                            ),
                            TableHeading(
                              text: 'h',
                            ),
                            TableHeading(
                              text: 'n',
                            ),
                            TableHeading(
                              text: 'c',
                            ),
                            TableHeading(
                              text: 's',
                            ),
                            TableHeading(
                              text: 'k',
                            ),
                            TableHeading(
                              text: ' ',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぴゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'びゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/bya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'じゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ja.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぎゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/gya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'りゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/rya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'みゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ひゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/hya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'にゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/nya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ちゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/cha.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'しゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sha.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'きゃ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/kya.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'ya',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぴゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'びゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/byu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'じゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/jyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぎゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/gyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'りゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ryu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'みゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/myu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ひゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/hyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'にゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/nyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ちゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/chu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'しゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/shu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'きゅ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/kyu.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'yu',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぴょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/pyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'びょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/byo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'じょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/jyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ぎょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/gyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'りょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ryo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'みょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/myo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ひょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/hyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'にょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/nyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ちょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/cho.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'しょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sho.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'きょ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/kyo.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'yo',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const NotesSection(
                heading: 'Notes',
                content: NumberedList(
                  items: [
                    Text(
                      'The above table is the same as before. Match the top consonants to the vowel sound on the right. Ex: きゃ = kya.',
                    ),
                    Text(
                      'Also note that since 「じ」 is pronounced / ji /, all the small 「や」、「ゆ」、「よ」 sounds are also based off of that, namely: / jya / jyu / jyo /.',
                    ),
                    Text(
                      'The same thing also applies to 「ち」 which becomes / cha / chu / cho / and 「し」 which becomes / sha / shu / sho /. (Though arguably, you can still think of it as / sya / syu / syo /.)',
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'You can also combine a consonant with a / ya / yu / yo / sound by attaching a small 「や」、「ゆ」、or 「よ」 to the / i / vowel character of each consonant. Notes The above table is the same as before. Match the top consonants to the vowel sound on the right. Ex: きゃ = kya. Also note that since 「じ」 is pronounced / ji /, all the small 「や」、「ゆ」、「よ」 sounds are also based off of that, namely: / jya / jyu / jyo /. The same thing also applies to 「ち」 which becomes / cha / chu / cho / and 「し」 which becomes / sha / shu / sho /. (Though arguably, you can still think of it as / sya / syu / syo /.)',
      grammars: [],
      keywords: ['ya','yu,''yo'],
    ),
    Section(
      heading: 'The small 「つ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'A small 「つ」 is inserted between two characters to carry the consonant sound of the second character to the end of the first. For example, if you inserted a small 「つ」 between 「び」 and 「く」 to make 「びっく」, the / k / consonant sound is carried back to the end of the first character to produce “bikku”. Similarly, 「はっぱ」 becomes “happa”, 「ろっく」 becomes “rokku” and so on and so forth.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                centered: true,
                items: [
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ざ',
                            ),
                            TextSpan(
                              text: 'っ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'し (za',
                            ),
                            TextSpan(
                              text: 's',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '-shi)',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/zasshi.wav');},
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– magazine'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'か',
                            ),
                            TextSpan(
                              text: 'っ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'ぷ (ka',
                            ),
                            TextSpan(
                              text: 'p',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '-pu)',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/kappu.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– cup'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'A small 「つ」 is inserted between two characters to carry the consonant sound of the second character to the end of the first. For example, if you inserted a small 「つ」 between 「び」 and 「く」 to make 「びっく」, the / k / consonant sound is carried back to the end of the first character to produce “bikku”. Similarly, 「はっぱ」 becomes “happa”, 「ろっく」 becomes “rokku” and so on and so forth. Examples ざっし (zas-shi)– magazine かっぷ (kap-pu)– cup',
      grammars: [],
      keywords: ['tsu'],
    ),
    Section(
      heading: 'The long vowel sound',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(title: 'Learn Japanese from Scratch 1.1.9 - Long vowel sounds in Hiragana', uri: 'https://youtu.be/SLP1BxU6AR4',),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Whew! You’re almost done. In this last portion, we will go over the long vowel sound which is simply extending the duration of a vowel sound. You can extend the vowel sound of a character by adding either 「あ」、「い」、or 「う」 depending on the vowel in accordance to the following chart.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Extending Vowel Sounds'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: const [
                        TableRow(
                          children: [
                            TableHeading(text: 'Vowel Sound'),
                            TableHeading(text: 'Extended by'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(content: Text('/ a /')),
                            TableData(content: Text('あ')),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(content: Text('/ i / e /')),
                            TableData(content: Text('い')),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(content: Text('/ u / o /')),
                            TableData(content: Text('う')),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'For example, if you wanted to create an extended vowel sound from 「か」, you would add 「あ」 to create 「かあ」. Other examples would include: 「き → きい」, 「く → くう」, 「け → けい」, 「こ → こう」, 「さ → さあ」 and so on. The reasoning for this is quite simple. Try saying 「か」 and 「あ」 separately. Then say them in succession as fast as you can. You’ll notice that soon enough, it sounds like you’re dragging out the / ka / for a longer duration than just saying / ka / by itself. When pronouncing long vowel sounds, try to remember that they are really two sounds merged together.')
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'It’s important to make sure you hold the vowel sound long enough because you can be saying things like “here” （ここ） instead of “high school” （こうこう） or “middle-aged lady” （おばさん） instead of “grandmother” （おばあさん） if you don’t stretch it out correctly!')
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                centered: true,
                items: [
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'がくせ',
                            ),
                            TextSpan(
                              text: 'い',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' (ga-ku-s',
                            ),
                            TextSpan(
                              text: 'e',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/gakusei.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– student'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'せんせ',
                            ),
                            TextSpan(
                              text: 'い',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' (sen-s',
                            ),
                            TextSpan(
                              text: 'e',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/sensei.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– teacher'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'きょ',
                            ),
                            TextSpan(
                              text: 'う',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' (ky',
                            ),
                            TextSpan(
                              text: 'o',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/kyou.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– today'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'おはよ',
                            ),
                            TextSpan(
                              text: 'う',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' (o-ha-y',
                            ),
                            TextSpan(
                              text: 'o',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/ohayou.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– good morning'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'おか',
                            ),
                            TextSpan(
                              text: 'あ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'さん (o-k',
                            ),
                            TextSpan(
                              text: 'a',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '-san)',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/okaasan.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– mother'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'There are rare exceptions where an / e / vowel sound is extended by adding 「え」 or an / o / vowel sound is extended by 「お」. Some examples of this include 「おねえさん」、「おおい」、and 「おおきい」. Pay careful attention to these exceptions but don’t worry, there aren’t too many of them.')
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Whew! You’re almost done. In this last portion, we will go over the long vowel sound which is simply extending the duration of a vowel sound. You can extend the vowel sound of a character by adding either 「あ」、「い」、or 「う」 depending on the vowel in accordance to the following chart. For example, if you wanted to create an extended vowel sound from 「か」, you would add 「あ」 to create 「かあ」. Other examples would include: 「き → きい」, 「く → くう」, 「け → けい」, 「こ → こう」, 「さ → さあ」 and so on. The reasoning for this is quite simple. Try saying 「か」 and 「あ」 separately. Then say them in succession as fast as you can. You’ll notice that soon enough, it sounds like you’re dragging out the / ka / for a longer duration than just saying / ka / by itself. When pronouncing long vowel sounds, try to remember that they are really two sounds merged together. It’s important to make sure you hold the vowel sound long enough because you can be saying things like “here” （ここ） instead of “high school” （こうこう） or “middle-aged lady” （おばさん） instead of “grandmother” （おばあさん） if you don’t stretch it out correctly! Examples がくせい (ga-ku-se) – student せんせい (sen-se) – teacher きょう (kyo) – today おはよう (o-ha-yo) – good morning おかあさん (o-ka-san) – mother There are rare exceptions where an / e / vowel sound is extended by adding 「え」 or an / o / vowel sound is extended by 「お」. Some examples of this include 「おねえさん」、「おおい」、and 「おおきい」. Pay careful attention to these exceptions but don’t worry, there aren’t too many of them.',
      grammars: [],
      keywords: ['long vowel'],
    ),
    Section(
      heading: 'Hiragana practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Though I already mentioned that there are many sites and helper programs for learning Hiragana, I figured I should put in some exercises of my own in the interest of completeness. I’ve removed the obsolete characters since you won’t need to know them. I suggest playing around with this chart and a scrap piece of paper to test your knowledge of Hiragana.')
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(text: 'Tap on the '),
                  TextSpan(
                    text: 'flip',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(text: ' text to show or hide each character.'),
                ],
              ),
              HiraganaChartExercise(),
              Heading(text: 'Hiragana Writing Practice', level: 2),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this section, we will practice writing some words in Hiragana. This is the only part of this guide where we will be using the English alphabet to represent Japanese sounds. I’ve added bars between each letter to prevent the ambiguities that is caused by romaji such as “un | yo” vs “u | nyo”. Don’t get too caught up in the romaji spellings. Remember, the whole point is to test your aural memory with Hiragana. I hope to replace this with sound in the future to remove the use of romaji altogether.')
                ],
              ),
              Heading(text: 'Hiragana Writing Exercise 1', level: 3),
              Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' ta | be | mo | no ＝',
                  ),
                  TextSpan(
                    text: 'たべもの',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              BasicExercise(
                entries: {
                  'ku | ru | ma': 'くるま',
                  'a | shi | ta': 'あした',
                  'ko | ku | se | ki': 'こくせき',
                  'o | su | shi': 'おすし',
                  'ta | be | ru': 'たべる',
                  'wa | ka | ra | na | i': 'わからない',
                  'sa | zu | ke | ru': 'さずける',
                  'ri | ku | tsu': 'りくつ',
                  'ta | chi | yo | mi': 'たちよみ',
                  'mo | no | ma | ne': 'ものまね',
                  'hi | ga | e | ri': 'ひがえり',
                  'pon | zu': 'ぽんず',
                  'hi | ru | me | shi': 'ひるめし',
                  're | ki | shi': 'れきし',
                  'fu | yu | ka | i': 'ふゆかい',
                },
              ),
              Heading(text: 'More Hiragana Writing Practice', level: 2),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Now we’re going to move on to practice writing Hiragana with the small 「や」、「ゆ」、「よ」 、and the long vowel sound. For the purpose of this exercise, I will denote the long vowel sound as “－” and leave you to figure out which Hiragana to use based on the letter preceding it.')
                ],
              ),
              Heading(text: 'Hiragana Writing Exercise 2', level: 3),
              Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' jyu | gyo－ ＝',
                  ),
                  TextSpan(
                    text: 'じゅぎょう',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              BasicExercise(
                entries: {
                  'nu | ru | i | o | cha': 'ぬるいおちゃ',
                  'kyu－ | kyo | ku': 'きゅうきょく',
                  'un | yo－| jyo－ | ho－': 'うんようじょうほう',
                  'byo－ | do－': 'びょうどう',
                  'jyo－ | to－ | shu | dan': 'じょうとうしゅだん',
                  'gyu－ | nyu－': 'ぎゅうにゅう',
                  'sho－ | rya | ku': 'しょうりゃく',
                  'hya | ku | nen | ha | ya | i': 'ひゃくねんはやい',
                  'so | tsu | gyo－ | shi | ki': 'そつぎょうしき',
                  'to－ | nyo－ | byo－': 'とうにょうびょう',
                  'mu | ryo－': 'むりょう',
                  'myo－ | ji': 'みょうじ',
                  'o | ka－ | san': 'おかあさん',
                  'ro－ | nin': 'ろうにん',
                  'ryu－ | ga | ku | se | i': 'りゅうがくせい',
                },
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Now let’s practice reading some Hiragana. I want to particularly focus on correctly reading the small 「つ」. Remember to not get too caught up in the unavoidable inconsistencies of romaji. The point is to check whether you can figure out how it’s supposed to sound in your mind.')
                ],
              ),
              Heading(text: 'Hiragana Reading Exercise', level: 3),
              Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' とった ＝ ',
                  ),
                  TextSpan(
                    text: 'totta',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              BasicExercise(
                entries: {
                  'きゃっかんてき': 'kyakkanteki',
                  'はっぴょうけっか': 'happyoukekka',
                  'ちょっかん': 'chokkan',
                  'ひっし': 'hisshi',
                  'ぜったい': 'zettai',
                  'けっちゃく': 'ketchaku',
                  'しっぱい': 'shippai',
                  'ちゅうとはんぱ': 'chuutohanpa',
                  'やっかい': 'yakkai',
                  'しょっちゅう': 'shotchuu',
                },
              ),
            ],
          );
        },
      ),
      plaintext: 'Though I already mentioned that there are many sites and helper programs for learning Hiragana, I figured I should put in some exercises of my own in the interest of completeness. I’ve removed the obsolete characters since you won’t need to know them. I suggest playing around with this chart and a scrap piece of paper to test your knowledge of Hiragana. Tap on the flip text to show or hide each character. Hiragana Writing Practice In this section, we will practice writing some words in Hiragana. This is the only part of this guide where we will be using the English alphabet to represent Japanese sounds. I’ve added bars between each letter to prevent the ambiguities that is caused by romaji such as “un | yo” vs “u | nyo”. Don’t get too caught up in the romaji spellings. Remember, the whole point is to test your aural memory with Hiragana. I hope to replace this with sound in the future to remove the use of romaji altogether. Hiragana Writing Exercise 1 くるま あした こくせき おすし たべる わからない さずける りくつ たちよみ ものまね ひがえり ぽんず ひるめし れきし ふゆかい More Hiragana Writing Practice Now we’re going to move on to practice writing Hiragana with the small 「や」、「ゆ」、「よ」 、and the long vowel sound. For the purpose of this exercise, I will denote the long vowel sound as “－” and leave you to figure out which Hiragana to use based on the letter preceding it. Hiragana Writing Exercise 2 ぬるいおちゃ きゅうきょく うんようじょうほう びょうどう じょうとうしゅだん ぎゅうにゅう しょうりゃく ひゃくねんはやい そつぎょうしき とうにょうびょう むりょう みょうじ おかあさん ろうにん りゅうがくせい Now let’s practice reading some Hiragana. I want to particularly focus on correctly reading the small 「つ」. Remember to not get too caught up in the unavoidable inconsistencies of romaji. The point is to check whether you can figure out how it’s supposed to sound in your mind. Hiragana Reading Exercise きゃっかんてき kyakkanteki はっぴょうけっか happyoukekka ちょっかん chokkan ひっし hisshi ぜったい zettai けっちゃく ketchaku しっぱい shippai ちゅうとはんぱ chuutohanpa やっかい yakkai しょっちゅう shotchuu',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
