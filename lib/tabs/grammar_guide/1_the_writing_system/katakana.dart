import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/audio/audio_player.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/animation_dialog.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/exercises/katakana_chart_exercise.dart';
import 'package:japanese_grammar_guide/exercises/basic_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson katakana = Lesson(
  title: 'Katakana',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(
                  title: 'Learn Japanese from Scratch 1.2.1 - Katakana',
                  uri: 'https://youtu.be/ZIbKs0ZaPUg'),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Katakana',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text:
                        ' is mainly used for words imported from foreign languages. It can also be used to emphasize certain words similar to the function of ',
                  ),
                  const TextSpan(
                    text: 'italics',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text: '. For a more complete list of usages, refer to the ',
                  ),
                  ExternalLink(
                    inlineSpan:
                        const TextSpan(text: 'Wikipedia entry on katakana'),
                    uri: 'http://en.wikipedia.org/wiki/Katakana#Usage',
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Katakana represents the same set of phonetic sounds as Hiragana except all the characters are different. Since foreign words must fit into this limited set of [consonants+vowel] sounds, they undergo many radical changes resulting in instances where English speakers can’t understand words that are supposed to be derived from English! As a result, the use of Katakana is extremely difficult for English speakers because they expect English words to sound like… well… English. Instead, it is better to completely forget the original English word, and treat the word as an entirely separate Japanese word, otherwise you can run into the habit of saying English words with English pronunciations (whereupon a Japanese person may or may not understand what you are saying).')
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(
                        caption: 'Katakana – Tap for stroke order and sound'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'n',
                            ),
                            TableHeading(
                              text: 'w',
                            ),
                            TableHeading(
                              text: 'r',
                            ),
                            TableHeading(
                              text: 'y',
                            ),
                            TableHeading(
                              text: 'm',
                            ),
                            TableHeading(
                              text: 'h',
                            ),
                            TableHeading(
                              text: 'n',
                            ),
                            TableHeading(
                              text: 't',
                            ),
                            TableHeading(
                              text: 's',
                            ),
                            TableHeading(
                              text: 'k',
                            ),
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: ' ',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ン',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(n)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/n.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/n.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ワ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/wa.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/wa.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ラ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ra.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ra.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ヤ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ya.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ya.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'マ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ma.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ma.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ハ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ha.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ha.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ナ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/na.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/na.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'タ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ta.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ta.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'サ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sa.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/sa.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'カ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ka.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ka.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ア',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/a.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/a.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'a',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text('ヰ*'),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'リ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ri.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ri.gif', context);
                              },
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ミ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/mi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ヒ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/hi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/hi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ニ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ni.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ni.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'チ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(chi)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/chi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/chi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'シ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(shi)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/shi.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/shi.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'キ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ki.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ki.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'イ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/i.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/i.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'i',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ル',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ru.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ru.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ユ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/yu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/yu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ム',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/mu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'フ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(fu)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/fu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/fu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ヌ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/nu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/nu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Column(
                                  children: [
                                    Text(
                                      'ツ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                    const Text(
                                      '(tsu)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/tsu.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/tsu.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ス',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/su.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/su.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ク',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ku.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ku.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ウ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/u.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/u.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'u',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            const TableData(
                              content: Text('ヱ*'),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'レ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/re.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/re.gif', context);
                              },
                            ),
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'メ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/me.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/nme.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ヘ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/he.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/he.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ネ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ne.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ne.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'テ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/te.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/te.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'セ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/se.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/se.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ケ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ke.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ke.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'エ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/e.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/e.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'e',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text(' '),
                            ),
                            InkWell(
                              child: const TableData(
                                content: Column(
                                  children: [
                                    Text('ヲ*'),
                                    Text(
                                      '(o)',
                                      textScaler: TextScaler.linear(0.65),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/wo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/wo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ロ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ro.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ro.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ヨ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/yo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/yo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'モ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/mo.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/mo.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ホ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ho.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ho.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ノ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/no.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/no.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ト',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/to.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/to.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ソ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/so.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/so.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'コ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ko.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/ko.gif', context);
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'オ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/o.wav');
                                AnimationDialog.showKanaAnimationDialog(
                                    'katakana/o.gif', context);
                              },
                            ),
                            const TableHeading(
                              text: 'o',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: '* = obsolete or rarely used'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Katakana is significantly tougher to master compared to Hiragana because it is only used for certain words and you don’t get nearly as much practice as you do with Hiragana.'),
                  const TextSpan(
                      text:
                          ' To learn the proper stroke order (and yes, you need to), '),
                  ExternalLink(
                      inlineSpan: const TextSpan(text: 'here'),
                      uri:
                          'http://japanese-lesson.com/characters/katakana/katakana_writing.html'),
                  const TextSpan(
                      text: ' is a link to practice sheets for Katakana.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Also, since Japanese doesn’t have any spaces, sometimes the symbol 「・」 is used to show the spaces like 「ロック・アンド・ロール」 for “rock and roll”. Using the symbol is completely optional so sometimes nothing will be used at all.',
                  ),
                ],
              ),
              const NotesSection(
                heading: 'Notes',
                content: NumberedList(
                  items: [
                    Text(
                      'All the sounds are identical to what they were for Hiragana.',
                    ),
                    Text(
                      'As we will learn later, 「を」 is only ever used as a particle and all particles are in Hiragana. Therefore, you will almost never need to use 「ヲ」 and it can be safely ignored. (Unless you are reading very old telegrams or something.)',
                    ),
                    Text(
                      'The four characters 「シ」、「ン」、「ツ」、and 「ソ」 are fiendishly similar to each other. Basically, the difference is that the first two are more “horizontal” than the second two. The little lines are slanted more horizontally and the long line is drawn in a curve from bottom to top. The second two have almost vertical little lines and the long line doesn’t curve as much as it is drawn from top to bottom. It is almost like a slash while the former is more like an arc. These characters are hard to sort out and require some patience and practice.',
                    ),
                    Text(
                      'The characters 「ノ」、「メ」、and 「ヌ」 are also something to pay careful attention to, as well as, 「フ」、「ワ」、 and 「ウ」. Yes, they all look very similar. No, I can’t do anything about it.',
                    ),
                    Text(
                      'You must learn the correct stroke order and direction!',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    Text(
                      'Sometimes 「・」 is used to denote what would be spaces in English.',
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Katakana is mainly used for words imported from foreign languages. It can also be used to emphasize certain words similar to the function of italics. For a more complete list of usages, refer to the Wikipedia entry on katakana. Katakana represents the same set of phonetic sounds as Hiragana except all the characters are different. Since foreign words must fit into this limited set of [consonants+vowel] sounds, they undergo many radical changes resulting in instances where English speakers can’t understand words that are supposed to be derived from English! As a result, the use of Katakana is extremely difficult for English speakers because they expect English words to sound like… well… English. Instead, it is better to completely forget the original English word, and treat the word as an entirely separate Japanese word, otherwise you can run into the habit of saying English words with English pronunciations (whereupon a Japanese person may or may not understand what you are saying). Katakana is significantly tougher to master compared to Hiragana because it is only used for certain words and you don’t get nearly as much practice as you do with Hiragana. To learn the proper stroke order (and yes, you need to), here is a link to practice sheets for Katakana. Also, since Japanese doesn’t have any spaces, sometimes the symbol 「・」 is used to show the spaces like 「ロック・アンド・ロール」 for “rock and roll”. Using the symbol is completely optional so sometimes nothing will be used at all. Notes All the sounds are identical to what they were for Hiragana. As we will learn later, 「を」 is only ever used as a particle and all particles are in Hiragana. Therefore, you will almost never need to use 「ヲ」 and it can be safely ignored. (Unless you are reading very old telegrams or something.) The four characters 「シ」、「ン」、「ツ」、and 「ソ」 are fiendishly similar to each other. Basically, the difference is that the first two are more “horizontal” than the second two. The little lines are slanted more horizontally and the long line is drawn in a curve from bottom to top. The second two have almost vertical little lines and the long line doesn’t curve as much as it is drawn from top to bottom. It is almost like a slash while the former is more like an arc. These characters are hard to sort out and require some patience and practice. The characters 「ノ」、「メ」、and 「ヌ」 are also something to pay careful attention to, as well as, 「フ」、「ワ」、 and 「ウ」. Yes, they all look very similar. No, I can’t do anything about it. You must learn the correct stroke order and direction! Sometimes 「・」 is used to denote what would be spaces in English.',
      grammars: ['カタカナ'],
      keywords: ['katakana','kana'],
    ),
    Section(
      heading: 'The Long Vowel Sound',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Long vowels have been radically simplified in Katakana. Instead of having to muck around thinking about vowel sounds, all long vowel sounds are denoted by a simple dash like so: ー.',
                  ),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                centered: true,
                items: [
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ツア',
                            ),
                            TextSpan(
                              text: 'ー',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' (tsu-',
                            ),
                            TextSpan(
                              text: 'a',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/tsua.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– tour'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'メ',
                            ),
                            TextSpan(
                              text: 'ー',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'ル (m',
                            ),
                            TextSpan(
                              text: 'e',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '-ru)',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/meeru.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– email'),
                    ],
                  ),
                  Row(
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ケ',
                            ),
                            TextSpan(
                              text: 'ー',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'キ (k',
                            ),
                            TextSpan(
                              text: 'e',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '-ki)',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          Player.playAudio('vocab/keiki.wav');
                        },
                        icon: const Icon(Icons.play_arrow),
                      ),
                      const Text('– cake'),
                    ],
                  ),
                ],
              ),
              const NotesSection(
                heading: 'Summary',
                content: BlankList(
                  items: [
                    Text(
                        'All long vowel sounds in Katakana are denoted by a dash. For example, “cute” would be written in Katakana like so: 「キュート」.'),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Long vowels have been radically simplified in Katakana. Instead of having to muck around thinking about vowel sounds, all long vowel sounds are denoted by a simple dash like so: ー. Examples ツアー (tsu-a) – tour メール (me-ru) – email ケーキ (ke-ki) – cake Summary All long vowel sounds in Katakana are denoted by a dash. For example, “cute” would be written in Katakana like so: 「キュート」.',
      grammars: [],
      keywords: ['long vowel'],
    ),
    Section(
      heading: 'The Small 「ア、イ、ウ、エ、オ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(
                title:
                    'Learn Japanese from Scratch 1.2.2 - More sounds in Katakana',
                uri: 'https://youtu.be/gBdX3kZEUTw',
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Due to the limitations of the sound set in Hiragana, some new combinations have been devised over the years to account for sounds that were not originally in Japanese. Most notable is the lack of the / ti / di / and / tu / du / sounds (because of the / chi / tsu / sounds), and the lack of the / f / consonant sound except for 「ふ」. The / sh / j / ch / consonants are also missing for the / e / vowel sound. The decision to resolve these deficiencies was to add small versions of the five vowel sounds. This has also been done for the / w / consonant sound to replace the obsolete characters. In addition, the convention of using the little double slashes on the 「ウ」 vowel （ヴ） with the small 「ア、イ、エ、オ」 to designate the / v / consonant has also been established but it’s not often used probably due to the fact that Japanese people still have difficulty pronouncing / v /. For instance, while you may guess that “volume” would be pronounced with a / v / sound, the Japanese have opted for the easier to pronounce “bolume” （ボリューム）. In the same way, vodka is written as “wokka” （ウォッカ） and not 「ヴォッカ」. You can write “violin” as either 「バイオリン」 or 「ヴァイオリン」. It really doesn’t matter however because almost all Japanese people will pronounce it with a / b / sound anyway. The following table shows the added sounds that were lacking in bold. Other sounds that already existed are reused as appropriate.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Additional sounds'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'v',
                            ),
                            TableHeading(
                              text: 'w',
                            ),
                            TableHeading(
                              text: 'f',
                            ),
                            TableHeading(
                              text: 'ch',
                            ),
                            TableHeading(
                              text: 'd',
                            ),
                            TableHeading(
                              text: 't',
                            ),
                            TableHeading(
                              text: 'j',
                            ),
                            TableHeading(
                              text: 'sh',
                            ),
                            TableHeading(
                              text: ' ',
                            ),
                          ],
                        ),
                        // TODO: generate additional files?
                        TableRow(
                          children: [
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ヴァ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/va.wav');
                              // },
                            ),
                            InkWell(
                              child: const TableData(
                                content: Text(
                                  'ワ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/wa.wav');
                              },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ファ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/fa.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'チャ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/cha.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ダ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/da.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'タ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ta.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ジャ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/jya.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'シャ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sha.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'a',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ヴィ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/vi.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ウィ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/wi.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'フィ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/fi.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'チ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/chi.wav');
                              },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ディ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/di.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ティ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/ti.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ジ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/ji.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'シ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/shi.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'i',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ヴ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/vu.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ウ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/u.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'フ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/fu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'チュ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/chu.wav');
                              },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ドゥ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/du.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'トゥ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/tu.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ジュ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/jyu.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'シュ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/shu.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'u',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ヴェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/ve.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ウェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/we.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'フェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/fe.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'チェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/che.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'デ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/de.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'テ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/te.wav');
                              },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ジェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/je.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'シェ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/she.wav');
                              // },
                            ),
                            const TableHeading(
                              text: 'e',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ヴォ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/vo.wav');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'ウォ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/uo');
                              // },
                            ),
                            const InkWell(
                              child: TableData(
                                content: Text(
                                  'フォ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              // onTap: () {
                              //   Player.playAudio('kana/fo.wav');
                              // },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'チョ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/cho.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ド',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/do.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ト',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/to.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ジョ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/jyo.wav');
                              },
                            ),
                            InkWell(
                              child: TableData(
                                content: Text(
                                  'ショ',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Player.playAudio('kana/sho.wav');
                              },
                            ),
                            const TableHeading(
                              text: 'o',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              NotesSection(
                heading: 'Notes',
                content: NumberedList(
                  items: [
                    const Text(
                      'Notice that there is no / wu / sound. For example, the Katakana for “woman” is written as “u-man” （ウーマン）.',
                    ),
                    const Text(
                      'While the / tu / sound (as in “too”) can technically be produced given the rules as 「トゥ」, foreign words that have become popular before these sounds were available simply used / tsu / to make do. For instance, “tool” is still 「ツール」 and “tour” is similarly still 「ツアー」.',
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text:
                                'Back in the old days, without these new sounds, there was no choice but to just take characters off the regular table without regard for actual pronunciation. On old buildings, you may still see 「ビル',
                          ),
                          TextSpan(
                            text: 'ヂ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'ング」 instead of the modern spelling 「ビル',
                          ),
                          TextSpan(
                            text: 'ディ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'ング」.',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Due to the limitations of the sound set in Hiragana, some new combinations have been devised over the years to account for sounds that were not originally in Japanese. Most notable is the lack of the / ti / di / and / tu / du / sounds (because of the / chi / tsu / sounds), and the lack of the / f / consonant sound except for 「ふ」. The / sh / j / ch / consonants are also missing for the / e / vowel sound. The decision to resolve these deficiencies was to add small versions of the five vowel sounds. This has also been done for the / w / consonant sound to replace the obsolete characters. In addition, the convention of using the little double slashes on the 「ウ」 vowel （ヴ） with the small 「ア、イ、エ、オ」 to designate the / v / consonant has also been established but it’s not often used probably due to the fact that Japanese people still have difficulty pronouncing / v /. For instance, while you may guess that “volume” would be pronounced with a / v / sound, the Japanese have opted for the easier to pronounce “bolume” （ボリューム）. In the same way, vodka is written as “wokka” （ウォッカ） and not 「ヴォッカ」. You can write “violin” as either 「バイオリン」 or 「ヴァイオリン」. It really doesn’t matter however because almost all Japanese people will pronounce it with a / b / sound anyway. The following table shows the added sounds that were lacking in bold. Other sounds that already existed are reused as appropriate. Notes Notice that there is no / wu / sound. For example, the Katakana for “woman” is written as “u-man” （ウーマン）. While the / tu / sound (as in “too”) can technically be produced given the rules as 「トゥ」, foreign words that have become popular before these sounds were available simply used / tsu / to make do. For instance, “tool” is still 「ツール」 and “tour” is similarly still 「ツアー」. Back in the old days, without these new sounds, there was no choice but to just take characters off the regular table without regard for actual pronunciation. On old buildings, you may still see 「ビルヂング」 instead of the modern spelling 「ビルディング」.',
      grammars: [],
      keywords: [],
    ),
    Section(
      heading: 'Some examples of words in Katakana',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Translating English words into Japanese is a knack that requires quite a bit of practice and luck. To give you a sense of how English words become “Japanified”, here are a few examples of words in Katakana. Sometimes the words in Katakana may not even be correct English or have a different meaning from the English word it’s supposed to represent. Of course, not all Katakana words are derived from English.',
                  )
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Sample Katakana Words'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: const [
                        TableRow(
                          children: [
                            TableHeading(
                              text: 'English',
                            ),
                            TableHeading(
                              text: 'Japanese',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('Russia'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('ロシア'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('cheating'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('カンニング (cunning)'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('tour'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('ツアー'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('company employee'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('サラリーマン (salary man)'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('Mozart'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('モーツァルト'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('car horn'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('クラクション (klaxon)'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('sofa'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('ソファ or ソファー'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('Halloween'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('ハロウィーン'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('French fries'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('フライドポテト (fried potato)'),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Translating English words into Japanese is a knack that requires quite a bit of practice and luck. To give you a sense of how English words become “Japanified”, here are a few examples of words in Katakana. Sometimes the words in Katakana may not even be correct English or have a different meaning from the English word it’s supposed to represent. Of course, not all Katakana words are derived from English. Sample Katakana Words English Japanese Russia ロシア cheating カンニング (cunning) tour ツアー company employee サラリーマン (salary man) Mozart モーツァルト car horn クラクション (klaxon) sofa ソファ or ソファー Halloween ハロウィーン French fries フライドポテト (fried potato)',
      grammars: [],
      keywords: [],
    ),
    Section(
      heading: 'Katakana Practice Exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Fill in the Katakana Chart', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here is the katakana chart you can use to help test your memory. 「ヲ」 has been removed since you’ll probably never need it.')
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'Tap on the '),
                  TextSpan(
                    text: 'flip',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(text: ' text to show or hide each character.'),
                ],
              ),
              const KatakanaChartExercise(),
              const Heading(text: 'Katakana Writing Practice', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here, we will practice writing some words in Katakana. Plus, you’ll get a little taste of what foreign words sound like in Japanese.')
                ],
              ),
              const Heading(text: 'Katakana Writing Exercise 1', level: 3),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' ta | be | mo | no ＝',
                  ),
                  TextSpan(
                    text: 'タベモノ',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              const BasicExercise(
                entries: {
                  'pan': 'パン',
                  'kon | pyu－ | ta': 'コンピュータ',
                  'myu－ | ji | ka | ru': 'ミュージカル',
                  'u－ | man': 'ウーマン',
                  'he | a | pi－ | su': 'ヘアピース',
                  'nu－ | do': 'ヌード',
                  'me | nyu－': 'メニュー',
                  'ro－ | te－ | shon': 'ローテーション',
                  'ha | i | kin | gu': 'ハイキング',
                  'kyan | se | ru': 'キャンセル',
                  'ha | ne | mu－n': 'ハネムーン',
                  'ku | ri | su | ma | su | tsu | ri－': 'クリスマスツリー',
                  'ra | i | to': 'ライト',
                  'na | i | to | ge－ | mu': 'ナイトゲーム',
                },
              ),
              const Heading(text: 'More Katakana Writing Practice', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'This Katakana writing exercise includes all the irregular sounds that don’t exist in Hiragana.')
                ],
              ),
              const Heading(text: 'Katakana Writing Exercise 2', level: 3),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' bi－ | chi ＝ ',
                  ),
                  TextSpan(
                    text: 'ビーチ',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              const BasicExercise(
                entries: {
                  'e | i | zu | wi | ru | su': 'エイズウイルス',
                  'no－ | su | sa | i | do': 'ノースサイド',
                  'in | fo | me－ | shon': 'インフォメーション',
                  'pu | ro | je | ku | to': 'プロジェクト',
                  'fa | su | to | fu－ | do': 'ファストフード',
                  'she | ru | su | ku | ri | pu | to': 'シェルスクリプト',
                  'we－ | to | re | su': 'ウェートレス',
                  'ma | i | ho－ | mu': 'マイホーム',
                  'chi－ | mu | wa－ | ku': 'チームワーク',
                  'mi | ni | su | ka－ | to': 'ミニスカート',
                  're－ | za－ | di | su | ku': 'レーザーディスク',
                  'chen | ji': 'チェンジ',
                  're | gyu | ra－': 'レギュラー',
                  'u | e | i | to | ri | fu | tin | gu': 'ウエイトリフティング',
                },
              ),
              const Heading(
                  text: 'Changing English words to katakana', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Just for fun, let’s try figuring out the katakana for some English words. I’ve listed some common patterns below but they are only guidelines and may not apply for some words.')
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'As you know, since Japanese sounds almost always consist of consonant-vowel pairs, any English words that deviate from this pattern will cause problems. Here are some trends you may have noticed.')
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'If you’ve seen the move “Lost in Translation”, you know that / l / and / r / are indistinguishable.')
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Ready → '),
                        TextSpan(
                            text: ' レ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'ディ'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Lady → '),
                        TextSpan(
                            text: ' レ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'ディ'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'If you have more than one vowel in a row or a vowel sound that ends in / r /, it usually becomes a long vowel sound.')
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: 'Tar',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'get →'),
                        TextSpan(
                            text: ' ター',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'ゲット'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: 'Shoo',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 't →'),
                        TextSpan(
                            text: ' シュー',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'ト'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Abrupt cut-off sounds usually denoted by a / t / or / c / employ the small 「ッ」.')
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Ca'),
                        TextSpan(
                            text: 't',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'ch → キャ'),
                        TextSpan(
                            text: 'ッ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'チ'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Ca'),
                        TextSpan(
                            text: 'c',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'he → キャ'),
                        TextSpan(
                            text: 'ッ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: 'シュ'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Any word that ends in a consonant sound requires another vowel to complete the consonant-vowel pattern. (Except for “n” and “m” for which we have 「ン」)')
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'For “t” and “d”, it’s usually “o”. For everything else, it’s usually “u”.')
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Goo'),
                        TextSpan(
                            text: 'd',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: ' → グッ'),
                        TextSpan(
                            text: 'ド',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'To'),
                        TextSpan(
                            text: 'p',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: ' → トッ'),
                        TextSpan(
                            text: 'プ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: 'Jac'),
                        TextSpan(
                            text: 'k',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                        const TextSpan(text: ' → ジャッ'),
                        TextSpan(
                            text: 'ク',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            )),
                      ],
                    ),
                  ),
                ],
              ),
              const Heading(text: 'English to Katakana Exercise', level: 3),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' Europe ＝ ',
                  ),
                  TextSpan(
                    text: 'ヨーロッパ',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              const BasicExercise(
                entries: {
                  'check': 'チェック',
                  'violin': 'バイオリン',
                  'jet coaster (roller coaster)': '	ジェットコースター',
                  'window shopping': 'ウィンドーショッピング',
                  'salsa': 'サルサ',
                  'hotdog': 'ホットドッグ',
                  'suitcase': 'スーツケース',
                  'kitchen': 'キッチン',
                  'restaurant': 'レストラン',
                  'New York': 'ニューヨーク',
                },
              ),
            ],
          );
        },
      ),
      plaintext: 'Here is the katakana chart you can use to help test your memory. 「ヲ」 has been removed since you’ll probably never need it. Tap on the flip text to show or hide each character. Katakana Writing Practice Here, we will practice writing some words in Katakana. Plus, you’ll get a little taste of what foreign words sound like in Japanese. Katakana Writing Exercise 1 pan パン kon | pyu－ | ta コンピュータ myu－ | ji | ka | ru ミュージカル u－ | man ウーマン he | a | pi－ | su ヘアピースnu－ | do ヌード me | nyu－ メニューro－ | te－ | shon ローテーション ha | i | kin | gu ハイキング kyan | se | ru キャンセル ha | ne | mu－n ハネムーン ku | ri | su | ma | su | tsu | ri－ クリスマスツリーra | i | to ライト na | i | to | ge－ | mu ナイトゲーム More Katakana Writing Practice This Katakana writing exercise includes all the irregular sounds that don’t exist in Hiragana. Katakana Writing Exercise 2 e | i | zu | wi | ru | su エイズウイルス no－ | su | sa | i | do ノースサイド in | fo | me－ | shon インフォメーション pu | ro | je | ku | to プロジェクト fa | su | to | fu－ | do ファストフード she | ru | su | ku | ri | pu | to シェルスクリプト we－ | to | re | su ウェートレス ma | i | ho－ | mu マイホーム chi－ | mu | wa－ | ku チームワーク mi | ni | su | ka－ | to ミニスカート re－ | za－ | di | su | ku レーザーディスク chen | ji チェンジ re | gyu | ra－ レギュラー u | e | i | to | ri | fu | tin | gu ウエイトリフティング Changing English words to katakana Just for fun, let’s try figuring out the katakana for some English words. I’ve listed some common patterns below but they are only guidelines and may not apply for some words. As you know, since Japanese sounds almost always consist of consonant-vowel pairs, any English words that deviate from this pattern will cause problems. Here are some trends you may have noticed. If you’ve seen the move “Lost in Translation”, you know that / l / and / r / are indistinguishable. Ready →  レディ Lady →  レディ If you have more than one vowel in a row or a vowel sound that ends in / r /, it usually becomes a long vowel sound. Target → ターゲット Shoot → シュート Abrupt cut-off sounds usually denoted by a / t / or / c / employ the small 「ッ」. Catch → キャッチ Cache → キャッシュ Any word that ends in a consonant sound requires another vowel to complete the consonant-vowel pattern. (Except for “n” and “m” for which we have 「ン」) For “t” and “d”, it’s usually “o”. For everything else, it’s usually “u”. Good → グッド Top → トップ Jack → ジャック English to Katakana Exercise check チェック violin バイオリン jet coaster (roller coaster) ジェットコースター window shopping ウィンドーショッピング salsa サルサ hotdog ホットドッグ suitcase スーツケース kitchen キッチン restaurant レストラン New York ニューヨーク',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
