import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson writing = Lesson(
  title: 'Chapter Overview',
  sections: [
    Section(
      heading: 'The scripts',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Japanese consists of two scripts (referred to as '),
                  TextSpan(
                      text: 'kana',
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  TextSpan(text: ') called '),
                  TextSpan(
                    text: 'Hiragana',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(text: ' and '),
                  TextSpan(
                    text: 'Katakana',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                      text:
                          ', which are two versions of the same set of sounds in the language. Hiragana and Katakana consist of a little less than 50 “letters”, which are actually simplified Chinese characters adopted to form a phonetic script.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(text: 'Chinese characters, called '),
                  TextSpan(
                    text: 'Kanji',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                      text:
                          ' in Japanese, are also heavily used in the Japanese writing. Most of the words in the Japanese written language are written in Kanji (nouns, verbs, adjectives). There exists over 40,000 Kanji where about 2,000 represent over 95% of characters actually used in written text. There are no spaces in Japanese so Kanji is necessary in distinguishing between separate words within a sentence. Kanji is also useful for discriminating between homophones, which occurs quite often given the limited number of distinct sounds in Japanese.')
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Hiragana is used mainly for grammatical purposes. We will see this as we learn about particles. Words with extremely difficult or rare Kanji, colloquial expressions, and onomatopoeias are also written in Hiragana. It’s also often used for beginning Japanese students and children in place of Kanji they don’t know.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'While Katakana represents the same sounds as Hiragana, it is mainly used to represent newer words imported from western countries (since there are no Kanji associated with words based on the roman alphabet). The next three sections will cover Hiragana, Katakana, and Kanji.'),
                ],
              ),
              YouTubeLink(
                title: 'Learn Japanese from Scratch 1.1.1 - Writing Systems',
                uri: 'https://youtu.be/KBYMuJ7B5Dg',
              ),
            ],
          );
        },
      ),
      plaintext:
          'Japanese consists of two scripts (referred to as kana) called Hiragana and Katakana, which are two versions of the same set of sounds in the language. Hiragana and Katakana consist of a little less than 50 “letters”, which are actually simplified Chinese characters adopted to form a phonetic script. Chinese characters, called Kanji in Japanese, are also heavily used in the Japanese writing. Most of the words in the Japanese written language are written in Kanji (nouns, verbs, adjectives). There exists over 40,000 Kanji where about 2,000 represent over 95% of characters actually used in written text. There are no spaces in Japanese so Kanji is necessary in distinguishing between separate words within a sentence. Kanji is also useful for discriminating between homophones, which occurs quite often given the limited number of distinct sounds in Japanese. Hiragana is used mainly for grammatical purposes. We will see this as we learn about particles. Words with extremely difficult or rare Kanji, colloquial expressions, and onomatopoeias are also written in Hiragana. It’s also often used for beginning Japanese students and children in place of Kanji they don’t know. While Katakana represents the same sounds as Hiragana, it is mainly used to represent newer words imported from western countries (since there are no Kanji associated with words based on the roman alphabet). The next three sections will cover Hiragana, Katakana, and Kanji.',
      grammars: [],
      keywords: ['kanji', 'hiragana', 'katakana','writing','chapter','overview'],
    ),
    Section(
      heading: 'Intonation',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'As you will find out in the next section, every character in Hiragana (and the Katakana equivalent) corresponds to a [vowel] or [consonant + vowel] syllable sound with the single exception of the 「ん」 and 「ン」 characters (more on this later). This system of letter for each syllable sound makes pronunciation absolutely clear with no ambiguities. However, the simplicity of this system does not mean that pronunciation in Japanese is simple. In fact, the rigid structure of the fixed syllable sound in Japanese creates the challenge of learning proper intonation. Intonation of high and low pitches is a crucial aspect of the spoken language. For example, homophones can have different pitches of low and high tones resulting in a slightly different sound despite sharing the same pronunciation. The biggest obstacle for obtaining proper and natural sounding speech is incorrect intonation. Many students often speak without paying attention to the correct enunciation of pitches making speech sound unnatural (the classic foreigner’s accent). It is not practical to memorize or attempt to logically create rules for pitches, especially since it can change depending on the context or the dialect. The only practical approach is to get the general sense of pitches by mimicking native Japanese speakers with careful listening and practice.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Intonation of high and low pitches is a crucial aspect of the spoken language. For example, homophones can have different pitches of low and high tones resulting in a slightly different sound despite sharing the same pronunciation. The biggest obstacle for obtaining proper and natural sounding speech is incorrect intonation. Many students often speak without paying attention to the correct enunciation of pitches making speech sound unnatural (the classic foreigner’s accent). It is not practical to memorize or attempt to logically create rules for pitches, especially since it can change depending on the context or the dialect. The only practical approach is to get the general sense of pitches by mimicking native Japanese speakers with careful listening and practice.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext:
          'Intonation As you will find out in the next section, every character in Hiragana (and the Katakana equivalent) corresponds to a [vowel] or [consonant + vowel] syllable sound with the single exception of the 「ん」 and 「ン」 characters (more on this later). This system of letter for each syllable sound makes pronunciation absolutely clear with no ambiguities. However, the simplicity of this system does not mean that pronunciation in Japanese is simple. In fact, the rigid structure of the fixed syllable sound in Japanese creates the challenge of learning proper intonation. Intonation of high and low pitches is a crucial aspect of the spoken language. For example, homophones can have different pitches of low and high tones resulting in a slightly different sound despite sharing the same pronunciation. The biggest obstacle for obtaining proper and natural sounding speech is incorrect intonation. Many students often speak without paying attention to the correct enunciation of pitches making speech sound unnatural (the classic foreigner’s accent). It is not practical to memorize or attempt to logically create rules for pitches, especially since it can change depending on the context or the dialect. The only practical approach is to get the general sense of pitches by mimicking native Japanese speakers with careful listening and practice.',
      grammars: [],
      keywords: [
        'pronunciation',
        'intonation'
      ],
    ),
  ],
);
