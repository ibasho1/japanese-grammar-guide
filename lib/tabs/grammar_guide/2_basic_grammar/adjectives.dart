import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/notice_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/exercises/adjective_conjugation_exercise.dart';
import 'package:japanese_grammar_guide/exercises/sentence_completion_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson adjectives = Lesson(
  title: 'Adjectives',
  sections: [
    Section(
      heading: 'Properties of adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              YouTubeLink(
                  title: 'Learn Japanese from Scratch 2.1.4 - Adjectives',
                  uri: 'https://youtu.be/LGXVzWwz0Oc'),
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now that we can connect two nouns together in various ways using particles, we want to describe our nouns with adjectives. An adjective can directly modify a noun that immediately follows it. It can also be connected in the same way we did with nouns using particles. All adjectives fall under two categories: ',
                  ),
                  TextSpan(
                    text: 'na-adjectives',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text: ' and ',
                  ),
                  TextSpan(
                    text: 'i-adjectives',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text: '.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Now that we can connect two nouns together in various ways using particles, we want to describe our nouns with adjectives. An adjective can directly modify a noun that immediately follows it. It can also be connected in the same way we did with nouns using particles. All adjectives fall under two categories: na-adjectives and i-adjectives.',
      grammars: [],
      keywords: ['adjective','properties'],
    ),
    Section(
      heading: 'The na-adjective',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '静か 【しず・か】 (na-adj) – quiet',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '人 【ひと】 – person',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'きれい (na-adj) – pretty; clean',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '友達 【とも・だち】 – friend',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '親切 【しん・せつ】 (na-adj) – kind',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '魚 【さかな】 – fish',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '好き 【す・き】 (na-adj) – likable; desirable',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '肉 【にく】 – meat',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '野菜 【や・さい】 – vegetables',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The na-adjective is very simple to learn because it acts essentially like a noun. All the conjugation rules for both nouns and na-adjectives are the same. One main difference is that a na-adjective can directly modify a noun following it by sticking 「な」 between the adjective and noun. (Hence the name, na-adjective.)',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Quiet person.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kirei,
                              inlineSpan: const TextSpan(
                                text: 'きれい',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Pretty person.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can also use adjectives with particles just like we did in the last lesson with nouns.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: shinsetsu,
                              inlineSpan: const TextSpan(
                                text: '親切',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Friend is kind.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: shinsetsu,
                              inlineSpan: const TextSpan(
                                text: '親切',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Friend is kind person.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'As shown by the following examples, the conjugation rules for na-adjectives are the same as nouns.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好き',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob likes fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きじゃない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob does not like fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きだった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob liked fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きじゃなかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob did not like fish.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'If it bothers you that “like” is an adjective and not a verb in Japanese, you can think of 「好き」 as meaning “desirable”. Also, you can see a good example of the topic and identifier particle working in harmony. The sentence is about the topic “Bob” and “fish” identifies specifically what Bob likes.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can also use the last three conjugations to directly modify the noun. (Remember to attach 「な」 for positive non-past tense.)'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きな',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Person that likes fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きじゃない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Person that does not like fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きだった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Person that liked fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好きじゃなかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Person that did not like fish.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Here, the entire clause 「',
                  ),
                  VocabTooltip(
                    message: sakana,
                    inlineSpan: const TextSpan(
                      text: '魚',
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: 'すき - like',
                    inlineSpan: const TextSpan(
                      text: '好き',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: sakana,
                    inlineSpan: const TextSpan(
                      text: '魚',
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: 'すき - like',
                    inlineSpan: const TextSpan(
                      text: '好きじゃない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」、etc. is modifying “person” to talk about people that like or dislike fish. You can see why this type of sentence is useful because 「',
                  ),
                  VocabTooltip(
                    message: hito,
                    inlineSpan: const TextSpan(
                      text: '人',
                    ),
                  ),
                  const TextSpan(
                    text: 'は',
                  ),
                  VocabTooltip(
                    message: sakana,
                    inlineSpan: const TextSpan(
                      text: '魚',
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: 'すき - like',
                    inlineSpan: TextSpan(
                      text: '好き',
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だ」 would mean “People like fish”, which isn’t always the case.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We can even treat the whole descriptive noun clause as we would a single noun. For instance, we can make the whole clause a topic like the following example.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: 'すき - like',
                              inlineSpan: TextSpan(
                                text: '好きじゃない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: 'すき - like',
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Person who does not like fish likes meat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: 'すき - like',
                              inlineSpan: TextSpan(
                                text: '好きな',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: yasai,
                              inlineSpan: const TextSpan(
                                text: '野菜',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: 'すき - like',
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Person who likes fish also likes vegetables.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 静か 【しず・か】 (na-adj) – quiet 人 【ひと】 – person きれい (na-adj) – pretty; clean 友達 【とも・だち】 – friend 親切 【しん・せつ】 (na-adj) – kind 魚 【さかな】 – fish 好き 【す・き】 (na-adj) – likable; desirable 肉 【にく】 – meat 野菜 【や・さい】 – vegetables The na-adjective is very simple to learn because it acts essentially like a noun. All the conjugation rules for both nouns and na-adjectives are the same. One main difference is that a na-adjective can directly modify a noun following it by sticking 「な」 between the adjective and noun. (Hence the name, na-adjective.) Examples 静かな人。 Quiet person. きれいな人。 Pretty person. You can also use adjectives with particles just like we did in the last lesson with nouns. Examples 友達は親切。 Friend is kind. 友達は親切な人だ。 Friend is kind person. As shown by the following examples, the conjugation rules for na-adjectives are the same as nouns. Examples ボブは魚が好きだ。 Bob likes fish. ボブは魚が好きじゃない。 Bob does not like fish. ボブは魚が好きだった。 Bob liked fish. ボブは魚が好きじゃなかった。 Bob did not like fish. If it bothers you that “like” is an adjective and not a verb in Japanese, you can think of 「好き」 as meaning “desirable”. Also, you can see a good example of the topic and identifier particle working in harmony. The sentence is about the topic “Bob” and “fish” identifies specifically what Bob likes. You can also use the last three conjugations to directly modify the noun. (Remember to attach 「な」 for positive non-past tense.) Examples 魚が好きな人。 Person that likes fish. 魚が好きじゃない人。 Person that does not like fish. 魚が好きだった人。 Person that liked fish. 魚が好きじゃなかった人。 Person that did not like fish. Here, the entire clause 「魚が好き」、「魚が好きじゃないだ」、etc. is modifying “person” to talk about people that like or dislike fish. You can see why this type of sentence is useful because 「人は魚が好き」 would mean “People like fish”, which isn’t always the case. We can even treat the whole descriptive noun clause as we would a single noun. For instance, we can make the whole clause a topic like the following example. Examples 魚が好きじゃない人は、肉が好きだ。 Person who does not like fish likes meat. 魚が好きな人は、野菜も好きだ。 Person who likes fish also likes vegetables.',
      grammars: ['形容動詞'],
      keywords: ['na-adjectives'],
    ),
    Section(
      heading: 'The i-adjective',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '嫌い 【きら・い】 (na-adj) – distasteful, hateful',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '食べ物 【た・べ・もの】 – food',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'おいしい (i-adj) – tasty',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '高い 【たか・い】 (i-adj) – high; tall; expensive',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ビル – building',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '値段 【ね・だん】 – price',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'レストラン – restaurant',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'あまり／あんまり – not very (when used with negative)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '好き 【す・き】 (na-adj) – likable; desirable',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'いい (i-adj) – good',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'All i-adjectives always end in the Hiragana character: 「い」. However, you may have noticed that some na-adjectives also end in 「い」 such as 「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: 'きれい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '（な）」. So how can you tell the difference? There are actually very few na-adjectives that end with 「い」 that is usually not written in Kanji. Two of the most common include: 「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: 'きれい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Almost all other na-adjectives that end in 「い」 are usually written in Kanji and so you can easily tell that it’s not an i-adjective. For instance, 「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: 'きれい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 written in Kanji looks like 「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: '綺麗',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: '奇麗',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Since the 「い」 part of 「麗」 is part of a Kanji character, you know that it can’t be an i-adjective. That’s because the whole point of the 「い」 in i-adjectives is to allow conjugation without changing the Kanji. In fact, 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is one of the rare na-adjectives that ends in 「い」 without a Kanji. This has to do with the fact that 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌い',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is actually derived from the verb 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌う',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Unlike na-adjectives, you do ',
                  ),
                  TextSpan(
                    text: 'not',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' need to add 「な」 to directly modify a noun with an i-adjective.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kirai,
                              inlineSpan: const TextSpan(
                                text: '嫌い',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: tabemono,
                              inlineSpan: const TextSpan(
                                text: '食べ物',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Hated food.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしい',
                              ),
                            ),
                            VocabTooltip(
                              message: tabemono,
                              inlineSpan: const TextSpan(
                                text: '食べ物',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Tasty food.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Remember how the negative state-of-being for nouns also ended in 「い」 （じゃな',
                  ),
                  TextSpan(
                    text: 'い',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  const TextSpan(
                    text:
                        '）? Well, just like the negative state-of-being for nouns, you can never attach the declarative 「だ」 to i-adjectives.',
                  ),
                ],
              ),
              const NoticeSection(
                text: Text(
                  'Do NOT attach 「だ」 to i-adjectives.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now that we got that matter cleared up, below are the rules for conjugating i-adjectives. Notice that the rule for conjugating to negative past tense is the same as the rule for the past tense.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Conjugation rules for i-adjectives',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Negative:',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    ' First remove the trailing 「い」 from the i-adjective and then attach 「くない」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'くない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Past-tense:',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    ' First remove the trailing 「い」 from the i-adjective or negative i-adjective and then attach 「かった」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高くな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高くな',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const TableCaption(
                            caption: 'Summary of i-adjective conjugations'),
                        Table(
                          border: TableBorder.all(
                            color: Theme.of(context).colorScheme.outline,
                          ),
                          children: [
                            const TableRow(
                              children: [
                                TableHeading(
                                  text: ' ',
                                ),
                                TableHeading(
                                  text: 'Positive',
                                ),
                                TableHeading(
                                  text: '	Negative',
                                )
                              ],
                            ),
                            TableRow(
                              children: [
                                const TableHeading(
                                  text: 'Non-Past',
                                ),
                                TableData(
                                  centered: false,
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: takai,
                                      inlineSpan: const TextSpan(
                                        text: '高い',
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  centered: false,
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: takai,
                                      inlineSpan: const TextSpan(
                                        text: '高くない',
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                const TableHeading(
                                  text: 'Past',
                                ),
                                TableData(
                                  centered: false,
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: takai,
                                      inlineSpan: const TextSpan(
                                        text: '高かった',
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  centered: false,
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: takai,
                                      inlineSpan: const TextSpan(
                                        text: '高くなかった',
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Tall building.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高くない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Not tall building.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高かった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Building that was tall.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高くなかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Building that was not tall.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Note that you can make the same type of descriptive noun clause as we have done with na-adjectives. The only difference is that we don’t need 「な」 to directly modify the noun.'),
                ],
              ),
              const Heading(
                text: 'Example',
                level: 2,
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nedan,
                              inlineSpan: TextSpan(
                                text: '値段',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: resutoran,
                              inlineSpan: TextSpan(
                                text: 'レストラン',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: amari,
                              inlineSpan: const TextSpan(
                                text: 'あまり',
                              ),
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好きじゃない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Don’t like high price restaurants very much.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'In this example, the descriptive clause 「',
                  ),
                  VocabTooltip(
                    message: nedan,
                    inlineSpan: const TextSpan(
                      text: '値段',
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: takai,
                    inlineSpan: const TextSpan(
                      text: '高い',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is directly modifying 「',
                  ),
                  VocabTooltip(
                    message: resutoran,
                    inlineSpan: const TextSpan(
                      text: 'レストラン',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 嫌い 【きら・い】 (na-adj) – distasteful, hateful 食べ物 【た・べ・もの】 – food おいしい (i-adj) – tasty 高い 【たか・い】 (i-adj) – high; tall; expensive ビル – building 値段 【ね・だん】 – price レストラン – restaurant あまり／あんまり – not very (when used with negative) 好き 【す・き】 (na-adj) – likable; desirable いい (i-adj) – good All i-adjectives always end in the Hiragana character: 「い」. However, you may have noticed that some na-adjectives also end in 「い」 such as 「きれい（な）」. So how can you tell the difference? There are actually very few na-adjectives that end with 「い」 that is usually not written in Kanji. Two of the most common include: 「きれい」 and 「嫌い」. Almost all other na-adjectives that end in 「い」 are usually written in Kanji and so you can easily tell that it’s not an i-adjective. For instance, 「きれい」 written in Kanji looks like 「綺麗」 or 「奇麗」. Since the 「い」 part of 「麗」 is part of a Kanji character, you know that it can’t be an i-adjective. That’s because the whole point of the 「い」 in i-adjectives is to allow conjugation without changing the Kanji. In fact, 「嫌い」 is one of the rare na-adjectives that ends in 「い」 without a Kanji. This has to do with the fact that 「嫌い」 is actually derived from the verb 「嫌う」. Unlike na-adjectives, you do not need to add 「な」 to directly modify a noun with an i-adjective. Examples 嫌いな食べ物。 Hated food. おいしい食べ物。 Tasty food. Remember how the negative state-of-being for nouns also ended in 「い」 （じゃない）? Well, just like the negative state-of-being for nouns, you can never attach the declarative 「だ」 to i-adjectives. Do NOT attach 「だ」 to i-adjectives. Now that we got that matter cleared up, below are the rules for conjugating i-adjectives. Notice that the rule for conjugating to negative past tense is the same as the rule for the past tense. Conjugation rules for i-adjectives Negative: First remove the trailing 「い」 from the i-adjective and then attach 「くない」 Example: 高い → 高くない Past-tense: First remove the trailing 「い」 from the i-adjective or negative i-adjective and then attach 「かった」 Examples: 高い → 高かった高くない → 高くなかった Examples 高いビル。 Tall building. 高くないビル。 Not tall building. 高かったビル。 Building that was tall. 高くなかったビル。 Building that was not tall. Note that you can make the same type of descriptive noun clause as we have done with na-adjectives. The only difference is that we don’t need 「な」 to directly modify the noun. Example 値段が高いレストランはあまり好きじゃない。 Don’t like high price restaurants very much. In this example, the descriptive clause 「値段が高い」 is directly modifying 「レストラン」.',
      grammars: ['形容詞'],
      keywords: ['i-adjectives'],
    ),
    Section(
      heading: 'An annoying exception',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '値段 【ね・だん】 – price',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'あまり／あんまり – not very (when used with negative)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'いい (i-adj) – good',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '彼 【かれ】 – he; boyfriend',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'かっこいい (i-adj) – cool; handsome',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'There is one i-adjective meaning “good” that acts slightly differently from all other i-adjectives. This is a classic case of how learning Japanese is harder for beginners because the most common and useful words also have the most exceptions. The word for “good” was originally 「よい（'),
                  VocabTooltip(
                    message: yoi,
                    inlineSpan: const TextSpan(
                      text: '良い',
                    ),
                  ),
                  const TextSpan(
                      text: '）」. However, with time, it soon became 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. When it is written in Kanji, it is usually read as 「'),
                  VocabTooltip(
                    message: yoi,
                    inlineSpan: const TextSpan(
                      text: 'よい',
                    ),
                  ),
                  const TextSpan(text: '」 so 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is almost always Hiragana. That’s all fine and good. Unfortunately, all the conjugations are still derived from 「'),
                  VocabTooltip(
                    message: yoi,
                    inlineSpan: const TextSpan(
                      text: 'よい',
                    ),
                  ),
                  const TextSpan(text: '」 and not 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」. This is shown in the next table.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'Another adjective that acts like this is 「'),
                  VocabTooltip(
                    message: kakkoii,
                    inlineSpan: const TextSpan(
                      text: 'かっこいい',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 because it is an abbreviated version of two words merged together: 「'),
                  VocabTooltip(
                    message: kakkou,
                    inlineSpan: const TextSpan(
                      text: '格好',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」. Since it uses the same 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(
                      text: '」, you need to use the same conjugations.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Conjugation for 「いい」'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Positive',
                            ),
                            TableHeading(
                              text: '	Negative',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Non-Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: ii,
                                  inlineSpan: const TextSpan(
                                    text: 'いい',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: ii,
                                  inlineSpan: const TextSpan(
                                    text: 'よくない',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: ii,
                                  inlineSpan: const TextSpan(
                                    text: 'よかった',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: ii,
                                  inlineSpan: const TextSpan(
                                    text: 'よくなかった',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Conjugation for 「かっこいい」'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Positive',
                            ),
                            TableHeading(
                              text: '	Negative',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Non-Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakkoii,
                                  inlineSpan: const TextSpan(
                                    text: 'かっこいい',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakkoii,
                                  inlineSpan: const TextSpan(
                                    text: 'かっこよくない',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakkoii,
                                  inlineSpan: const TextSpan(
                                    text: '	かっこよかった',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakkoii,
                                  inlineSpan: const TextSpan(
                                    text: 'かっこよくなかった',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'Take care to make all the conjugations from 「'),
                  VocabTooltip(
                    message: yoi,
                    inlineSpan: const TextSpan(
                      text: 'よい',
                    ),
                  ),
                  const TextSpan(text: '」 not 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nedan,
                              inlineSpan: const TextSpan(
                                text: '値段',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: anmari,
                              inlineSpan: const TextSpan(
                                text: 'あんまり',
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'よくない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Price isn’t very good.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'かっこよかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text('He looked really cool!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 値段 【ね・だん】 – price あまり／あんまり – not very (when used with negative) いい (i-adj) – good 彼 【かれ】 – he; boyfriend かっこいい (i-adj) – cool; handsome There is one i-adjective meaning “good” that acts slightly differently from all other i-adjectives. This is a classic case of how learning Japanese is harder for beginners because the most common and useful words also have the most exceptions. The word for “good” was originally 「よい（良い）」. However, with time, it soon became 「いい」. When it is written in Kanji, it is usually read as 「よい」 so 「いい」  is almost always Hiragana. That’s all fine and good. Unfortunately, all the conjugations are still derived from 「よい」 and not 「いい」. This is shown in the next table. Another adjective that acts like this is 「かっこいい」 because it is an abbreviated version of two words merged together: 「格好」 and 「いい」. Since it uses the same 「いい」, you need to use the same conjugations. Take care to make all the conjugations from 「よい」 not 「いい」. Examples 値段があんまりよくない。 Price isn’t very good. 彼はかっこよかった！ He looked really cool!',
      grammars: [],
      keywords: ['ii','yoi','good'],
    ),
    Section(
      heading: 'Adjective practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In the following exercises, we will practice the conjugations for adjectives. But first, you might want to learn or review the following useful adjectives that will be used in the exercises.',
                  ),
                ],
              ),
              NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                              'I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                          NumberedList(
                            items: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%9D%A2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '面 – mask; face'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E7%99%BD%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '白 – white'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%9C%89%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '有 – exist'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%90%8D%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '名 – name'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%AB%8C%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '嫌 – hate'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%A5%BD%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '好 – like'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%9D%99%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '静 – quiet'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%A5%BD%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '楽 – music; comfort'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%88%87%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '切 – cut'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%BE%9B%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '辛 – spicy; bitter'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%96%99%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '料 – materials'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E7%90%86%20%23kanji',
                                      inlineSpan:
                                          const TextSpan(text: '理 – reason'),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Kanji'),
              const NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here is a list of some simple adjectives (and one noun) that might be used in the exercises.'),
                        NumberedList(
                          items: [
                            Text(
                              'きれい – pretty; neat',
                            ),
                            Text(
                              'いい – good',
                            ),
                            Text(
                              'かっこいい – cool; good-looking',
                            ),
                            Text(
                              '面白い 【おもしろい】 – interesting',
                            ),
                            Text(
                              '有名 【ゆうめい】 – famous',
                            ),
                            Text(
                              '嫌い 【きらい】 – dislike; hate',
                            ),
                            Text(
                              '好き 【すき】 – like',
                            ),
                            Text(
                              '大きい 【おおきい】 – big',
                            ),
                            Text(
                              '小さい 【ちいさい】 – small',
                            ),
                            Text(
                              '静か 【しずか】 – quiet',
                            ),
                            Text(
                              '高い 【たかい】 – high; expensive',
                            ),
                            Text(
                              '楽しい 【たのしい】 – fun',
                            ),
                            Text(
                              '大切 【たいせつ】 – important',
                            ),
                            Text(
                              '辛い 【からい】 – spicy',
                            ),
                            Text(
                              '料理 【りょうり】 – cuisine',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Vocabulary',
              ),
              const Heading(text: 'Conjugation Exercise', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We are now going to practice the adjectives conjugations in order. Take each adjective and conjugate it to the following forms: the declarative (when applicable), negative, past, and negative past. In order to emphasize the fact that you can’t use the declarative 「だ」 with i-adjectives, you should just write “n/a” (or just leave it blank) when a conjugation does not apply.',
                  ),
                ],
              ),
              const AdjectiveConjugationExercise(),
              const Heading(text: 'Sentence completion exercise', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now that we’ve practiced the basic conjugations for adjectives, we are going to practice using them in actual sentences using the particles covered in the last section.',
                  ),
                ],
              ),
              const Heading(
                  text:
                      'Fill in the blank with the appropriate adjective or particle',
                  level: 3),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Ｑ）　学生？',
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Ａ）　ううん、',
                        ),
                        TextSpan(
                          text: '学生じゃない',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SentenceCompletionExercise(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section In the following exercises, we will practice the conjugations for adjectives. But first, you might want to learn or review the following useful adjectives that will be used in the exercises. Kanji I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 面 – mask; face 白 – white 有 – exist 名 – name 嫌 – hate 好 – like 静 – quiet 楽 – music; comfort 切 – cut 辛 – spicy; bitter 料 – materials 理 – reason Vocabulary Here is a list of some simple adjectives (and one noun) that might be used in the exercises. きれい – pretty; neat いい – good かっこいい – cool; good-looking 面白い 【おもしろい】 – interesting 有名 【ゆうめい】 – famous 嫌い 【きらい】 – dislike; hate 好き 【すき】 – like 大きい 【おおきい】 – big 小さい 【ちいさい】 – small 静か 【しずか】 – quiet 高い 【たかい】 – high; expensive 楽しい 【たのしい】 – fun 大切 【たいせつ】 – important 辛い 【からい】 – spicy 料理 【りょうり】 – cuisine Conjugation Exercise We are now going to practice the adjectives conjugations in order. Take each adjective and conjugate it to the following forms: the declarative (when applicable), negative, past, and negative past. In order to emphasize the fact that you can’t use the declarative 「だ」 with i-adjectives, you should just write “n/a” (or just leave it blank) when a conjugation does not apply. Sentence completion exercise Now that we’ve practiced the basic conjugations for adjectives, we are going to practice using them in actual sentences using the particles covered in the last section. Fill in the blank with the appropriate adjective or particle ジム：アリス、今　は　忙しい？ アリス：ううん、忙しくない。 アリス：何　が　楽しい？ ボブ：ゲーム、　が　楽しい。 アリス：大切な人は誰？ ボブ：ジム　が　大切だ。 アリス：辛い料理は、好き？ ボブ：ううん、辛くない料理　が　好きだ。 アリス：ジム　は　、かっこいい人？ ボブ：ううん、かっこよくない。 アリス：ボブは、有名な人？ ボブ：ううん、有名じゃない。 アリス：昨日のテストは、よかった？ ボブ：ううん、よくなかった。',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
