import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson adverbs = Lesson(
  title: 'Adverbs and Sentence-Ending Particles',
  sections: [
    Section(
      heading: 'Properties of adverbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('きれい (na-adj) – pretty; clean'),
                  Text('朝ご飯 【あさ・ご・はん】 – breakfast'),
                  Text('食べる 【た・べる】(ru-verb) – to eat'),
                  Text('自分 【じ・ぶん】 – oneself'),
                  Text('部屋 【へ・や】 – room'),
                  Text('映画 【えい・が】 – movie'),
                  Text('たくさん – a lot (amount)'),
                  Text('見る 【み・る】 – to see; to watch'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('声 【こえ】 – voice'),
                  Text('結構 【けっ・こう】 – fairly, reasonably'),
                  Text('大きい 【おお・きい】(i-adj) – big'),
                  Text('この – this （abbr. of これの）'),
                  Text('町 【まち】 – town'),
                  Text('変わる 【か・わる】(u-verb) – to change'),
                  Text('図書館 【と・しょ・かん】 – library'),
                  Text('中 【なか】 – inside'),
                  Text('静か 【しず・か】(na-adj) – quiet'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Unlike English, changing adjectives to adverbs is a very simple and straightforward process. In addition, since the system of particles make sentence ordering flexible, adverbs can be placed anywhere in the clause that it applies to as long as it comes before the verb that it refers to. As usual, we have two separate rules: one for i-adjectives, and one for na-adjectives.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For i-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Substitute the 「い」 with 「く」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '早',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '早',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach the target particle 「に」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kirei,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'きれい',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kirei,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'きれい',
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'に',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'How to change an adjective to an adverb',
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: asagohan,
                              inlineSpan: const TextSpan(
                                text: '朝ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: TextSpan(
                                text: '早く',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Bob quickly ate breakfast.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The adverb 「',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早く',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a little different from the English word ‘fast’ in that it can mean quickly in terms of speed or time. In other words, Bob may have eaten his breakfast early or he may have eaten it quickly depending on the context. In other types of sentences such as 「',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早く',
                    ),
                  ),
                  VocabTooltip(
                    message: hashiru,
                    inlineSpan: const TextSpan(
                      text: '走った',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, it is quite obvious that it probably means quickly and not early. (Of course this also depends on the context.)',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスは',
                            ),
                            VocabTooltip(
                              message: jibun,
                              inlineSpan: const TextSpan(
                                text: '自分',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kirei,
                              inlineSpan: TextSpan(
                                text: 'きれい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Alice did her own room toward clean.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The literal translation kind of gives you a sense of why the target particle is used. There is some argument against calling this an adverb at all but it is convenient for us to do so because of the grouping of i-adjectives and na-adjectives. Thinking of it as an adverb, we can interpret the sentence to mean: “Alice did her room cleanly.” or less literally: “Alice cleaned her room.” （「',
                  ),
                  VocabTooltip(
                    message: kirei,
                    inlineSpan: const TextSpan(
                      text: 'きれい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 literally means “pretty” but if it helps, you can think of it as, “Alice prettied up her own room.”）',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Note: Not all adverbs are derived from adjectives. Some words like 「',
                  ),
                  VocabTooltip(
                    message: zenzen,
                    inlineSpan: const TextSpan(
                      text: '全然',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: takusan,
                    inlineSpan: const TextSpan(
                      text: 'たくさん',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 are adverbs in themselves without any conjugation. These words can be used without particles just like regular adverbs.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: TextSpan(
                                text: 'たくさん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Saw a lot of movies.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: zenzen,
                              inlineSpan: TextSpan(
                                text: '全然',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Lately, don’t eat at all.',
                      ),
                    ],
                  ),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Here are some more examples of using adverbs.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブの',
                            ),
                            VocabTooltip(
                              message: koe,
                              inlineSpan: const TextSpan(
                                text: '声',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: kekkou,
                              inlineSpan: TextSpan(
                                text: '結構',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: const TextSpan(
                                text: '大きい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Bob’s voice is ',
                            ),
                            TextSpan(
                              text: 'fairly',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' large.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: machi,
                              inlineSpan: const TextSpan(
                                text: '町',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: TextSpan(
                                text: '大きく',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kawaru,
                              inlineSpan: const TextSpan(
                                text: '変わった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'This town had changed ',
                            ),
                            TextSpan(
                              text: 'greatly',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' lately.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: toshokan,
                              inlineSpan: const TextSpan(
                                text: '図書館',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: naka,
                              inlineSpan: const TextSpan(
                                text: '中',
                              ),
                            ),
                            const TextSpan(
                              text: 'では、',
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: TextSpan(
                                text: '静か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Within the library, [we] do things ',
                            ),
                            TextSpan(
                              text: 'quietly',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 早い 【はや・い】 (i-adj) – fast; early きれい (na-adj) – pretty; clean 朝ご飯 【あさ・ご・はん】 – breakfast 食べる 【た・べる】(ru-verb) – to eat 自分 【じ・ぶん】 – oneself 部屋 【へ・や】 – room 映画 【えい・が】 – movie たくさん – a lot (amount) 見る 【み・る】 – to see; to watch 最近 【さい・きん】 – recent; lately 全然 【ぜん・ぜん】 – not at all (when used with negative) 声 【こえ】 – voice 結構 【けっ・こう】 – fairly, reasonably 大きい 【おお・きい】(i-adj) – big この – this （abbr. of これの） 町 【まち】 – town 変わる 【か・わる】(u-verb) – to change 図書館 【と・しょ・かん】 – library 中 【なか】 – inside 静か 【しず・か】(na-adj) – quiet Unlike English, changing adjectives to adverbs is a very simple and straightforward process. In addition, since the system of particles make sentence ordering flexible, adverbs can be placed anywhere in the clause that it applies to as long as it comes before the verb that it refers to. As usual, we have two separate rules: one for i-adjectives, and one for na-adjectives. How to change an adjective to an adverb For i-adjectives: Substitute the 「い」 with 「く」. Example: 早い → 早く For na-adjectives: Attach the target particle 「に」. Example: きれい → きれいにボブは朝ご飯を早く食べた。 Bob quickly ate breakfast. The adverb 「早く」 is a little different from the English word ‘fast’ in that it can mean quickly in terms of speed or time. In other words, Bob may have eaten his breakfast early or he may have eaten it quickly depending on the context. In other types of sentences such as 「早く走った」, it is quite obvious that it probably means quickly and not early. (Of course this also depends on the context.) アリスは自分の部屋をきれいにした。 Alice did her own room toward clean. The literal translation kind of gives you a sense of why the target particle is used. There is some argument against calling this an adverb at all but it is convenient for us to do so because of the grouping of i-adjectives and na-adjectives. Thinking of it as an adverb, we can interpret the sentence to mean: “Alice did her room cleanly.” or less literally: “Alice cleaned her room.” （「きれい」 literally means “pretty” but if it helps, you can think of it as, “Alice prettied up her own room.”） Note: Not all adverbs are derived from adjectives. Some words like 「全然」 and 「たくさん」 are adverbs in themselves without any conjugation. These words can be used without particles just like regular adverbs. 映画をたくさん見た。 Saw a lot of movies. 最近、全然食べない。 Lately, don’t eat at all. Examples Here are some more examples of using adverbs. ボブの声は、結構大きい。 Bob’s voice is fairly large. この町は、最近大きく変わった。 This town had changed greatly lately. 図書館の中では、静かにする。 Within the library, [we] do things quietly.',
      grammars: ['副詞'],
      keywords: ['adverb','sentence-ending particle','sentence-ending'],
    ),
    Section(
      heading: 'Sentence-ending particles',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              NumberedList(
                items: [
                  Text('いい (i-adj) – good'),
                  Text('天気 【てん・き】 – weather'),
                  Text('そう – (things are) that way'),
                  Text('面白い 【おも・しろ・い】(i-adj) – interesting'),
                  Text('映画 【えい・が】 – movie'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('大丈夫 【だい・じょう・ぶ】 (na-adj) – ok'),
                  Text('今日 【きょう】 – today'),
                  Text('うん – yes (casual)'),
                  Text('でも – but'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('雨 【あめ】 – rain'),
                  Text('降る 【ふ・る】(u-verb) – to precipitate'),
                  Text('魚 【さかな】 – fish'),
                  Text('好き 【す・き】 (na-adj) – likable'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Sentence-ending particles are particles that always come at the end of sentences to change the “tone” or “feel” of a sentence. In this section, we will cover the two most commonly used sentence-ending particles.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary いい (i-adj) – good 天気 【てん・き】 – weather そう – (things are) that way 面白い 【おも・しろ・い】(i-adj) – interesting 映画 【えい・が】 – movie 全然 【ぜん・ぜん】 – not at all (when used with negative) 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 大丈夫 【だい・じょう・ぶ】 (na-adj) – ok 今日 【きょう】 – today うん – yes (casual) でも – but 明日 【あした】 – tomorrow 雨 【あめ】 – rain 降る 【ふ・る】(u-verb) – to precipitate 魚 【さかな】 – fish 好き 【す・き】 (na-adj) – likable Sentence-ending particles are particles that always come at the end of sentences to change the “tone” or “feel” of a sentence. In this section, we will cover the two most commonly used sentence-ending particles.',
      grammars: [],
      keywords: ['particles'],
    ),
    Section(
      heading: '「ね」 sentence ending',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'People usually add 「ね」 to the end of their sentence when they are looking for (and expecting) agreement to what they are saying. This is equivalent to saying, “right?” or “isn’t it?” in English.',
                  ),
                ],
              ),
              const Heading(
                text: 'Example 1',
                level: 2,
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          VocabTooltip(
                            message: tenki,
                            inlineSpan: const TextSpan(
                              text: '天気',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ',
                          ),
                          TextSpan(
                            text: 'ね',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Bob'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'Good weather, ',
                          ),
                          TextSpan(
                            text: 'huh',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          TextSpan(
                            text: 'ね',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Alice'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'That is so, ',
                          ),
                          TextSpan(
                            text: 'isn’t it',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The literal translation of 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'ね」 sounds a bit odd but it basically means something like, “Sure is”. Males would probably say, 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: 'だね」.',
                  ),
                ],
              ),
              const Heading(
                text: 'Example 2',
                level: 2,
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: omoshiroi,
                            inlineSpan: const TextSpan(
                              text: 'おもしろい',
                            ),
                          ),
                          VocabTooltip(
                            message: eiga,
                            inlineSpan: const TextSpan(
                              text: '映画',
                            ),
                          ),
                          const TextSpan(
                            text: 'だった',
                          ),
                          TextSpan(
                            text: 'ね',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Alice'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'That was interesting movie, ',
                          ),
                          TextSpan(
                            text: 'wasn’t it',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'え？',
                          ),
                          VocabTooltip(
                            message: zenzen,
                            inlineSpan: const TextSpan(
                              text: '全然',
                            ),
                          ),
                          VocabTooltip(
                            message: omoshiroi,
                            inlineSpan: const TextSpan(
                              text: 'おもしろくなかった',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text(
                      'Bob: Huh? No, it wasn’t interesting at all.',
                    ),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Since Alice is expecting agreement that the movie was interesting Bob is surprised because he didn’t find the movie interesting at all. (「え」 is a sound of surprise and confusion.)',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'People usually add 「ね」 to the end of their sentence when they are looking for (and expecting) agreement to what they are saying. This is equivalent to saying, “right?” or “isn’t it?” in English. Example 1 ボブ：いい天気だね。 Bob: Good weather, huh? アリス：そうね。 Alice: That is so, isn’t it? The literal translation of 「そうね」 sounds a bit odd but it basically means something like, “Sure is”. Males would probably say, 「そうだね」. Example 2 アリス：おもしろい映画だったね。 Alice: That was interesting movie, wasn’t it? ボブ：え？全然おもしろくなかった。 Bob: Bob: Huh? No, it wasn’t interesting at all. Since Alice is expecting agreement that the movie was interesting Bob is surprised because he didn’t find the movie interesting at all. (「え」 is a sound of surprise and confusion.)',
      grammars: ['ね'],
      keywords: ['ne','particle','sentence-ending particle'],
    ),
    Section(
      heading: '「よ」 sentence ending',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When 「よ」 is attached to the end of a sentence, it means that the speaker is informing the listener of something new. In English, we might say this with a, “You know…” such as the sentence, “You know, I’m actually a genius.”',
                  ),
                ],
              ),
              const Heading(
                text: 'Example 1',
                level: 2,
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(
                              text: '時間',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ない',
                            ),
                          ),
                          TextSpan(
                            text: 'よ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Alice'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'You know',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: ', there is no time.',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: daijoubu,
                            inlineSpan: const TextSpan(
                              text: '大丈夫',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ',
                          ),
                          TextSpan(
                            text: 'よ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Bob'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'It’s ok, ',
                          ),
                          TextSpan(
                            text: 'you know',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '.',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              const Heading(
                text: 'Example 2',
                level: 2,
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kyou,
                            inlineSpan: const TextSpan(
                              text: '今日',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          VocabTooltip(
                            message: tenki,
                            inlineSpan: const TextSpan(
                              text: '天気',
                            ),
                          ),
                          const TextSpan(
                            text: 'だね。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                      'Alice: Good weather today, huh?',
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                          VocabTooltip(
                            message: demo,
                            inlineSpan: const TextSpan(
                              text: 'でも',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: ashita,
                            inlineSpan: const TextSpan(
                              text: '明日',
                            ),
                          ),
                          VocabTooltip(
                            message: ame,
                            inlineSpan: const TextSpan(
                              text: '雨',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: furu,
                            inlineSpan: const TextSpan(
                              text: '降る',
                            ),
                          ),
                          TextSpan(
                            text: 'よ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Bob'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'Yeah. But it will rain tomorrow, ',
                          ),
                          TextSpan(
                            text: 'you know',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '.',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'When 「よ」 is attached to the end of a sentence, it means that the speaker is informing the listener of something new. In English, we might say this with a, “You know…” such as the sentence, “You know, I’m actually a genius.”Example 1 アリス：時間がないよ。 Alice: You know, there is no time. ボブ：大丈夫だよ。 Bob: It’s ok, you know. Example 2 アリス：今日はいい天気だね。 Alice: Alice: Good weather today, huh? ボブ：うん。 でも、明日雨が降るよ。 Bob: Yeah. But it will rain tomorrow, you know.',
      grammars: ['よ'],
      keywords: ['yo','particle','sentence-ending particle'],
    ),
    Section(
      heading: 'Combining both to get 「よね」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can also combine the two particles we just learned to create 「よね」. This is essentially used when you want to inform the listener of some new point you’re trying to make and when you’re seeking agreement on it at the same time. When combining the two, the order must always be 「よね」. You cannot reverse the order.',
                  ),
                ],
              ),
              const Heading(
                text: 'Example',
                level: 2,
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ボブは、',
                          ),
                          VocabTooltip(
                            message: sakana,
                            inlineSpan: const TextSpan(
                              text: '魚',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: suki,
                            inlineSpan: const TextSpan(
                              text: '好き',
                            ),
                          ),
                          const TextSpan(
                            text: 'なんだ',
                          ),
                          TextSpan(
                            text: 'よね',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Alice'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'You know',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: ', you like fish, ',
                          ),
                          TextSpan(
                            text: 'dontcha',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(
                            text: 'だね。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Bob'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'That is so, ',
                          ),
                          TextSpan(
                            text: 'huh',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'You can also combine the two particles we just learned to create 「よね」. This is essentially used when you want to inform the listener of some new point you’re trying to make and when you’re seeking agreement on it at the same time. When combining the two, the order must always be 「よね」. You cannot reverse the order. Example アリス：ボブは、魚が好きなんだよね。 Alice: You know, you like fish, dontcha? ボブ：そうだね。 Bob: That is so, huh?',
      grammars: ['よね'],
      keywords: ['yone','particle','sentence-ending particle'],
    ),
  ],
);
