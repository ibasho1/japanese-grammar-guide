import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson basic = Lesson(
  title: 'Chapter Overview',
  sections: [
    Section(
      heading: 'Basic grammatical structures',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Now that we have learned how to write Japanese, we can begin going over the basic grammatical structure of the language. This section primarily covers all the parts of speech: nouns, adjectives, verbs, and adverbs. It will also describe how to integrate the various parts of speech into a coherent sentence by using particles. By the end of this section, you should have an understanding of how basic sentences are constructed.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Now that we have learned how to write Japanese, we can begin going over the basic grammatical structure of the language. This section primarily covers all the parts of speech: nouns, adjectives, verbs, and adverbs. It will also describe how to integrate the various parts of speech into a coherent sentence by using particles. By the end of this section, you should have an understanding of how basic sentences are constructed.',
      grammars: [],
      keywords: ['basic','chapter overview','chapter','overview'],
    ),
  ],
);
