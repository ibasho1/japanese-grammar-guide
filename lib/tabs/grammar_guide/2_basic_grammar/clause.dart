import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/notice_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson clause = Lesson(
  title: 'Relative Clauses and Sentence Order',
  sections: [
    Section(
      heading: 'Treating verbs and state-of-being like adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Have you noticed how, many forms of verbs and the state-of-being conjugate in a similar manner to i-adjectives? Well, that is because, in a sense, they are adjectives. For example, consider the sentence: “The person who did not eat went to bank.” The “did not eat” describes the person and in Japanese, you can directly modify the noun ‘person’ with the clause ‘did not eat’ just like a regular adjective. This very simple realization will allow us to modify a noun with any arbitrary verb phrase!'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Have you noticed how, many forms of verbs and the state-of-being conjugate in a similar manner to i-adjectives? Well, that is because, in a sense, they are adjectives. For example, consider the sentence: “The person who did not eat went to bank.” The “did not eat” describes the person and in Japanese, you can directly modify the noun ‘person’ with the clause ‘did not eat’ just like a regular adjective. This very simple realization will allow us to modify a noun with any arbitrary verb phrase!',
      grammars: [],
      keywords: ['relative clause','sentence order'],
    ),
    Section(
      heading: 'Using state-of-being clauses as adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('国際 【こく・さい】 – international'),
                  Text('教育 【きょう・いく】 – education'),
                  Text('センター – center'),
                  Text('登場 【とう・じょう】 – entry (on stage)'),
                  Text('人物 【じん・ぶつ】 – character'),
                  Text('立入 【たち・いり】 – entering'),
                  Text('禁止 【きん・し】 – prohibition, ban'),
                  Text('学生 【がく・せい】 – student'),
                  Text('人 【ひと】 – person'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('子供 【こ・ども】 – child'),
                  Text('立派 【りっ・ぱ】 (na-adj) – fine, elegant'),
                  Text('大人 【おとな】 – adult'),
                  Text('なる (u-verb) – to become'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('いい (i-adj) – good'),
                  Text('先週 【せん・しゅう】 – last week'),
                  Text('医者 【い・しゃ】 – doctor'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('辞める 【や・める】 (ru-verb) – to quit'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The negative, past, and negative past conjugations of verbs can be used just like adjectives to directly modify nouns. However, we ',
                  ),
                  TextSpan(
                    text: 'cannot',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' do this with the plain non-past state-of-being using 「だ」. (I told you this was a pain in the butt.) The language has particles for this purpose, which will be covered in the next section.',
                  ),
                ],
              ),
              const NoticeSection(
                text: Text(
                  'You cannot use 「だ」 to directly modify a noun with a noun like you can with 「だった」、「じゃない」、and 「じゃなかった」.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'You can, however, have a string of nouns placed together when they’re not meant to modify each other. For example, in a phrase such as “International Education Center” you can see that it is just a string of nouns without any grammatical modifications between them. It’s not an “Education Center that is International” or a “Center for International Education”, etc., it’s just “International Education Center”. In Japanese, you can express this as simply 「',
                  ),
                  VocabTooltip(
                    message: kokusai,
                    inlineSpan: const TextSpan(
                      text: '国際',
                    ),
                  ),
                  VocabTooltip(
                    message: kyouiku,
                    inlineSpan: const TextSpan(
                      text: '教育',
                    ),
                  ),
                  VocabTooltip(
                    message: senta,
                    inlineSpan: const TextSpan(
                      text: 'センタ',
                    ),
                  ),
                  const TextSpan(
                    text: '」 (or 「',
                  ),
                  VocabTooltip(
                    message: sentaa,
                    inlineSpan: const TextSpan(
                      text: 'センター',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」). You will see this chaining of nouns in many combinations. Sometimes a certain combination is so commonly used that it has almost become a separate word and is even listed as a separate entry in some dictionaries. Some examples include: 「',
                  ),
                  VocabTooltip(
                    message: toujoujinbutsu,
                    inlineSpan: const TextSpan(
                      text: '登場人物',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: tachiirikinshi,
                    inlineSpan: const TextSpan(
                      text: '立入禁止',
                    ),
                  ),
                  const TextSpan(
                    text: '」、or 「',
                  ),
                  VocabTooltip(
                    message: tsuukinteate,
                    inlineSpan: const TextSpan(
                      text: '通勤手当',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. If you have difficulties in figuring out where to separate the words, you can paste them into the WWWJDICs ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'Text Glossing',
                    ),
                    uri: 'https://www.edrdg.org/cgi-bin/wwwjdic/wwwjdic?9T',
                  ),
                  const TextSpan(
                    text:
                        ' function and it’ll parse the words for you (most of the time).',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here are some examples of direct noun modifications with a ',
                  ),
                  TextSpan(
                    text: 'conjugated',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text: ' noun clause. The noun clause has been highlighted.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: TextSpan(
                                text: '学生',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Person who is not student do not go to school.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kodomo,
                              inlineSpan: TextSpan(
                                text: '子供',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'アリスが',
                            ),
                            VocabTooltip(
                              message: rippa,
                              inlineSpan: const TextSpan(
                                text: '立派',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: otona,
                              inlineSpan: const TextSpan(
                                text: '大人',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'The Alice that was a child became a fine adult.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: TextSpan(
                                text: '友達',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'じゃなかった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'アリスは、',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Alice who was not a friend, became a good friend.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senshuu,
                              inlineSpan: TextSpan(
                                text: '先週',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: isha,
                              inlineSpan: TextSpan(
                                text: '医者',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'ボブは、',
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yameru,
                              inlineSpan: const TextSpan(
                                text: '辞めた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Bob who was a doctor last week quit his job.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 国際 【こく・さい】 – international 教育 【きょう・いく】 – education センター – center 登場 【とう・じょう】 – entry (on stage) 人物 【じん・ぶつ】 – character 立入 【たち・いり】 – entering 禁止 【きん・し】 – prohibition, ban 学生 【がく・せい】 – student 人 【ひと】 – person 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 子供 【こ・ども】 – child 立派 【りっ・ぱ】 (na-adj) – fine, elegant 大人 【おとな】 – adult なる (u-verb) – to become 友達 【とも・だち】 – friend いい (i-adj) – good 先週 【せん・しゅう】 – last week 医者 【い・しゃ】 – doctor 仕事 【し・ごと】 – job 辞める 【や・める】 (ru-verb) – to quit The negative, past, and negative past conjugations of verbs can be used just like adjectives to directly modify nouns. However, we cannot do this with the plain non-past state-of-being using 「だ」. (I told you this was a pain in the butt.) The language has particles for this purpose, which will be covered in the next section. You cannot use 「だ」 to directly modify a noun with a noun like you can with 「だった」、「じゃない」、and 「じゃなかった」. You can, however, have a string of nouns placed together when they’re not meant to modify each other. For example, in a phrase such as “International Education Center” you can see that it is just a string of nouns without any grammatical modifications between them. It’s not an “Education Center that is International” or a “Center for International Education”, etc., it’s just “International Education Center”. In Japanese, you can express this as simply 「国際教育センタ」 (or 「センター」). You will see this chaining of nouns in many combinations. Sometimes a certain combination is so commonly used that it has almost become a separate word and is even listed as a separate entry in some dictionaries. Some examples include: 「登場人物」、「立入禁止」、or 「通勤手当」. If you have difficulties in figuring out where to separate the words, you can paste them into the WWWJDICs Text Glossing function and it’ll parse the words for you (most of the time). Examples Here are some examples of direct noun modifications with a conjugated noun clause. The noun clause has been highlighted. 学生じゃない人は、学校に行かない。 Person who is not student do not go to school. 子供だったアリスが立派な大人になった。 The Alice that was a child became a fine adult. 友達じゃなかったアリスは、いい友達になった。 Alice who was not a friend, became a good friend. 先週医者だったボブは、仕事を辞めた。 Bob who was a doctor last week quit his job.',
      grammars: [],
      keywords: ['state-of-being clause'],
    ),
    Section(
      heading: 'Using relative verb clauses as adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('先週 【せん・しゅう】 – last week'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('人 【ひと】 – person'),
                  Text('誰 【だれ】 – who'),
                  Text('いつも – always'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('赤い 【あか・い】 (i-adj) – red'),
                  Text('ズボン – pants'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('晩ご飯 【ばん・ご・はん】 – dinner'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('銀行 【ぎん・こう】 – bank'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Verb clauses can also be used just like adjectives to modify nouns. The following examples show us how this will allow us to make quite detailed and complicated sentences. The verb clause is highlighted.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senshuu,
                              inlineSpan: TextSpan(
                                text: '先週',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: TextSpan(
                                text: '映画',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見た',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text('Who is person who watched movie last week?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは、',
                            ),
                            VocabTooltip(
                              message: itsumo,
                              inlineSpan: TextSpan(
                                text: 'いつも',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: TextSpan(
                                text: '勉強',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob is a person who always studies.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: akai,
                              inlineSpan: TextSpan(
                                text: '赤い',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: zubon,
                              inlineSpan: TextSpan(
                                text: 'ズボン',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買う',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'はボブだ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Friend who buy red pants is Bob.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bangohan,
                              inlineSpan: TextSpan(
                                text: '晩ご飯',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: isha,
                              inlineSpan: TextSpan(
                                text: '食べなかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: TextSpan(
                                text: '映画',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見た',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: ginkou,
                              inlineSpan: const TextSpan(
                                text: '銀行',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Person who did not eat dinner went to the bank she saw at movie.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 先週 【せん・しゅう】 – last week 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see 人 【ひと】 – person 誰 【だれ】 – who いつも – always 勉強 【べん・きょう】 – study する (exception) – to do 赤い 【あか・い】 (i-adj) – red ズボン – pants 買う 【か・う】 (u-verb) – to buy 友達 【とも・だち】 – friend 晩ご飯 【ばん・ご・はん】 – dinner 食べる 【た・べる】 (ru-verb) – to eat 銀行 【ぎん・こう】 – bank Verb clauses can also be used just like adjectives to modify nouns. The following examples show us how this will allow us to make quite detailed and complicated sentences. The verb clause is highlighted. Examples 先週に映画を見た人は、誰？ Who is person who watched movie last week? ボブは、いつも勉強する人だ。 Bob is a person who always studies. 赤いズボンを買う友達はボブだ。 Friend who buy red pants is Bob. 晩ご飯を食べなかった人は、映画で見た銀行に行った。 Person who did not eat dinner went to the bank she saw at movie.',
      grammars: [],
      keywords: ['verb clause'],
    ),
    Section(
      heading: 'Japanese sentence order',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('私 【わたし】 – me; myself; I'),
                  Text('公園 【こう・えん】 – (public) park'),
                  Text('お弁当 【お・べん・とう】 – box lunch'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('学生 【がく・せい】 – student'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now that we’ve learned the concept of relative clauses and how they are used as building blocks to make sentences, I can go over how Japanese sentence ordering works. There’s this myth that keeps floating around about Japanese sentence order that continues to plague many hapless beginners to Japanese. Here’s how it goes.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The most basic sentence structure in English can be described as consisting of the following elements in this specific order: [Subject] [Verb] [Object]. A sentence is not grammatically correct if any of those elements are missing or out of order.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Japanese students will tell you that Japanese, on the other hand, while frothing at the mouth, is completely backwards!! Even some Japanese teacher might tell you that the basic Japanese sentence order is [Subject] [Object] [Verb]. This is a classic example of trying to fit Japanese into an English-based type of thinking. Of course, we all know (right?) that the real order of the fundamental Japanese sentence is: [Verb]. Anything else that comes before the verb doesn’t have to come in any particular order and nothing more than the verb is required to make a complete sentence. In addition, the verb must always come at the end. That’s the whole point of even having particles so that they can identify what grammatical function a word serves no matter where it is in the sentence. In fact, nothing will stop us from making a sentence with [Object] [Subject] [Verb] or just [Object] [Verb]. The following sentences are all complete and correct because the verb is at the end of the sentence.',
                  ),
                ],
              ),
              const Heading(
                  text:
                      'Grammatically complete and correctly ordered sentences',
                  level: 2),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: watashi,
                          inlineSpan: const TextSpan(
                            text: '私',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: kouen,
                          inlineSpan: const TextSpan(
                            text: '公園',
                          ),
                        ),
                        const TextSpan(
                          text: 'で、',
                        ),
                        VocabTooltip(
                          message: obentou,
                          inlineSpan: const TextSpan(
                            text: 'お弁当',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: TextSpan(
                            text: '食べた',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kouen,
                          inlineSpan: const TextSpan(
                            text: '公園',
                          ),
                        ),
                        const TextSpan(
                          text: 'で、',
                        ),
                        VocabTooltip(
                          message: watashi,
                          inlineSpan: const TextSpan(
                            text: '私',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: obentou,
                          inlineSpan: const TextSpan(
                            text: 'お弁当',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: TextSpan(
                            text: '食べた',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: obentou,
                          inlineSpan: const TextSpan(
                            text: 'お弁当',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: watashi,
                          inlineSpan: const TextSpan(
                            text: '私',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: kouen,
                          inlineSpan: const TextSpan(
                            text: '公園',
                          ),
                        ),
                        const TextSpan(
                          text: 'で、',
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: TextSpan(
                            text: '食べた',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: obentou,
                          inlineSpan: const TextSpan(
                            text: 'お弁当',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: TextSpan(
                            text: '食べた',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: TextSpan(
                            text: '食べた',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'So don’t sweat over whether your sentence is in the correct order. Just remember the following rules.',
                  ),
                ],
              ),
              NotesSection(
                  content: NumberedList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'A complete sentence requires a main verb that must come at the end. This also includes the implied state-of-being.',
                          ),
                          const Text(
                            'Examples: ',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          NumberedList(
                            items: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: const TextSpan(
                                        text: '食べた',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: gakusei,
                                      inlineSpan: const TextSpan(
                                        text: '学生',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: '（だ）',
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Complete sentences (relative clauses) can be used to modify nouns to make sentences with nested relative clauses except in the case of 「だ」.',
                          ),
                          const Text(
                            'Example: ',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          BulletedList(
                            items: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: obentou,
                                          inlineSpan: TextSpan(
                                            text: 'お弁当',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'を',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                        VocabTooltip(
                                          message: taberu,
                                          inlineSpan: TextSpan(
                                            text: '食べた',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ),
                                        VocabTooltip(
                                          message: gakusei,
                                          inlineSpan: const TextSpan(
                                            text: '学生',
                                          ),
                                        ),
                                        const TextSpan(
                                          text: 'が',
                                        ),
                                        VocabTooltip(
                                          message: kouen,
                                          inlineSpan: const TextSpan(
                                            text: '公園',
                                          ),
                                        ),
                                        const TextSpan(
                                          text: 'に、',
                                        ),
                                        VocabTooltip(
                                          message: iku,
                                          inlineSpan: const TextSpan(
                                            text: '行った',
                                          ),
                                        ),
                                        const TextSpan(
                                          text: '。',
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'Student who ate lunch',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                        const TextSpan(
                                          text: ' went to the park.',
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Japanese sentence order'),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 私 【わたし】 – me; myself; I 公園 【こう・えん】 – (public) park お弁当 【お・べん・とう】 – box lunch 食べる 【た・べる】 (ru-verb) – to eat 学生 【がく・せい】 – student 行く 【い・く】 (u-verb) – to go Now that we’ve learned the concept of relative clauses and how they are used as building blocks to make sentences, I can go over how Japanese sentence ordering works. There’s this myth that keeps floating around about Japanese sentence order that continues to plague many hapless beginners to Japanese. Here’s how it goes. The most basic sentence structure in English can be described as consisting of the following elements in this specific order: [Subject] [Verb] [Object]. A sentence is not grammatically correct if any of those elements are missing or out of order. Japanese students will tell you that Japanese, on the other hand, while frothing at the mouth, is completely backwards!! Even some Japanese teacher might tell you that the basic Japanese sentence order is [Subject] [Object] [Verb]. This is a classic example of trying to fit Japanese into an English-based type of thinking. Of course, we all know (right?) that the real order of the fundamental Japanese sentence is: [Verb]. Anything else that comes before the verb doesn’t have to come in any particular order and nothing more than the verb is required to make a complete sentence. In addition, the verb must always come at the end. That’s the whole point of even having particles so that they can identify what grammatical function a word serves no matter where it is in the sentence. In fact, nothing will stop us from making a sentence with [Object] [Subject] [Verb] or just [Object] [Verb]. The following sentences are all complete and correct because the verb is at the end of the sentence. Grammatically complete and correctly ordered sentences 私は公園で、お弁当を食べた。 公園で、私はお弁当を食べた。 お弁当を私は公園で、食べた。 お弁当を食べた。 食べた。 So don’t sweat over whether your sentence is in the correct order. Just remember the following rules. Japanese sentence order A complete sentence requires a main verb that must come at the end. This also includes the implied state-of-being. Examples: 食べた学生（だ） Complete sentences (relative clauses) can be used to modify nouns to make sentences with nested relative clauses except in the case of 「だ」. Example: お弁当を食べた学生が公園に、行った。 Student who ate lunch went to the park.',
      grammars: [],
      keywords: ['sentence order'],
    ),
  ],
);
