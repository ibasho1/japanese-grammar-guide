import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson inTransitive = Lesson(
  title: 'Transitive and Intransitive Verbs',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In Japanese, sometimes there are two types of the same verb often referred to as ',
                  ),
                  const TextSpan(
                    text: 'transitive',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text: ' and ',
                  ),
                  const TextSpan(
                    text: 'intransitive verbs',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        '. The difference between the two is that one verb is an action done by an active agent while the other is something that occurs without a direct agent. In English, this is sometimes expressed with the same verb, such as: “The ball dropped” vs “I dropped the ball” but in Japanese it becomes　「',
                  ),
                  VocabTooltip(
                    message: booru,
                    inlineSpan: const TextSpan(
                      text: 'ボール',
                    ),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: ochiru,
                    inlineSpan: TextSpan(
                      children: [
                        const TextSpan(
                          text: '落',
                        ),
                        TextSpan(
                          text: 'ちた',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const TextSpan(text: '」 vs 「'),
                  VocabTooltip(
                    message: booru,
                    inlineSpan: const TextSpan(
                      text: 'ボール',
                    ),
                  ),
                  const TextSpan(text: 'を'),
                  VocabTooltip(
                    message: otosu,
                    inlineSpan: TextSpan(
                      children: [
                        const TextSpan(
                          text: '落',
                        ),
                        TextSpan(
                          text: 'とした',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」.　Sometimes, the verbs changes when translated into English such as “To put it in the box” ('),
                  VocabTooltip(
                    message: bako,
                    inlineSpan: const TextSpan(
                      text: '箱',
                    ),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: ireru,
                    inlineSpan: const TextSpan(
                      text: '入れる',
                    ),
                  ),
                  const TextSpan(text: '） vs “To enter the box” （'),
                  VocabTooltip(
                    message: bako,
                    inlineSpan: const TextSpan(
                      text: '箱',
                    ),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: hairu,
                    inlineSpan: const TextSpan(
                      text: '入る',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '） but this is only from the differences in the languages. If you think in Japanese, intransitive and transitive verbs have the same meaning except that one indicates that someone had a direct hand in the action (direct object) while the other does not. While knowing the terminology is not important, it is important to know which is which in order to use the correct particle for the correct verb.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Since the basic meaning and the kanji is the same, you can learn two verbs for the price of just one kanji! Let’s look at a sample list of intransitive and transitive verbs.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'Transitive and Intransitive Verbs'),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Table(
                              border: TableBorder.all(
                                color: Theme.of(context).colorScheme.outline,
                              ),
                              children: const [
                                TableRow(children: [
                                  TableHeading(text: 'Intransitive'),
                                ]),
                              ],
                            ),
                            Table(
                              border: TableBorder.all(
                                color: Theme.of(context).colorScheme.outline,
                              ),
                              children: [
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: otosu,
                                          inlineSpan: const TextSpan(
                                            text: '落とす',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to drop'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: dasu,
                                          inlineSpan: const TextSpan(
                                            text: '出す',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to take out'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: ireru,
                                          inlineSpan: const TextSpan(
                                            text: '入れる',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to insert'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: akeru,
                                          inlineSpan: const TextSpan(
                                            text: '開ける',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to open'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: shimeru,
                                          inlineSpan: const TextSpan(
                                            text: '閉める',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to close'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: tsukeru,
                                          inlineSpan: const TextSpan(
                                            text: 'つける',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to attach'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: kesu,
                                          inlineSpan: const TextSpan(
                                            text: '消す',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to erase'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: nuku,
                                          inlineSpan: const TextSpan(
                                            text: '抜く',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to extract'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Table(
                              border: TableBorder.all(
                                color: Theme.of(context).colorScheme.outline,
                              ),
                              children: const [
                                TableRow(children: [
                                  TableHeading(text: 'Transitive'),
                                ]),
                              ],
                            ),
                            Table(
                              border: TableBorder.all(
                                color: Theme.of(context).colorScheme.outline,
                              ),
                              children: [
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: ochiru,
                                          inlineSpan: const TextSpan(
                                            text: '落ちる',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to drop'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: deru,
                                          inlineSpan: const TextSpan(
                                            text: '出る',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to come out; to leave'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: hairu,
                                          inlineSpan: const TextSpan(
                                            text: '入る',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to enter'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: aku,
                                          inlineSpan: const TextSpan(
                                            text: '開く',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to be opened'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: shimaru,
                                          inlineSpan: const TextSpan(
                                            text: '閉まる',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to be closed'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: tsuku,
                                          inlineSpan: const TextSpan(
                                            text: 'つく',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to be attached'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: kieru,
                                          inlineSpan: const TextSpan(
                                            text: '消える',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to disappear'),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    TableData(
                                      content: Text.rich(
                                        VocabTooltip(
                                          message: nukeru,
                                          inlineSpan: const TextSpan(
                                            text: '抜ける',
                                          ),
                                        ),
                                      ),
                                    ),
                                    const TableData(
                                      content: Text('to be extracted'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'In Japanese, sometimes there are two types of the same verb often referred to as transitive and intransitive verbs. The difference between the two is that one verb is an action done by an active agent while the other is something that occurs without a direct agent. In English, this is sometimes expressed with the same verb, such as: “The ball dropped” vs “I dropped the ball” but in Japanese it becomes　「ボールが落ちた」 vs 「ボールを落とした」.　Sometimes, the verbs changes when translated into English such as “To put it in the box” (箱に入れる） vs “To enter the box” （箱に入る） but this is only from the differences in the languages. If you think in Japanese, intransitive and transitive verbs have the same meaning except that one indicates that someone had a direct hand in the action (direct object) while the other does not. While knowing the terminology is not important, it is important to know which is which in order to use the correct particle for the correct verb. Since the basic meaning and the kanji is the same, you can learn two verbs for the price of just one kanji! Let’s look at a sample list of intransitive and transitive verbs. Transitive and Intransitive Verbs Intransitive 落とす to drop 出す to take out 入れる to insert 開ける to open 閉める to close つける to attach 消す to erase 抜く to extract Transitive 落ちる to drop 出る to come out; to leave 入る to enter 開く to be opened 閉まる to be closed つくto be attached 消える to disappear 抜ける to be extracted',
      grammars: ['他動詞','自動詞'],
      keywords: ['transitive verbs','intransitive verbs'],
    ),
    Section(
      heading: 'Pay attention to particles!',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'The important lesson to take away here is to learn how to use the correct particle for the correct type of verb. It might be difficult at first to grasp which is which when learning new verbs or whether there even is a transitive/intransitive distinction. If you’re not sure, you can always check whether a verb is transitive or intransitive by using an online dictionary such as '),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'jisho.org',
                    ),
                    uri: 'http://jisho.org/',
                  ),
                  const TextSpan(text: '.'),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: tsukeru,
                              inlineSpan: const TextSpan(
                                text: 'つけた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Learn Japanese.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: tsuku,
                              inlineSpan: const TextSpan(
                                text: 'ついた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The lights turned on.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kesu,
                              inlineSpan: const TextSpan(
                                text: '消す',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Turn off the lights.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kieru,
                              inlineSpan: const TextSpan(
                                text: '消える',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Lights turn off.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: mado,
                              inlineSpan: const TextSpan(
                                text: '窓',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: akeru,
                              inlineSpan: const TextSpan(
                                text: '開けた',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Who opened the window?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mado,
                              inlineSpan: TextSpan(
                                text: '窓',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: doushite,
                              inlineSpan: const TextSpan(
                                text: 'どうして',
                              ),
                            ),
                            VocabTooltip(
                              message: aku,
                              inlineSpan: const TextSpan(
                                text: '開いた',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Why has the window opened?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The important thing to remember is that intransitive verbs ',
                  ),
                  TextSpan(
                      text: 'cannot',
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                      )),
                  TextSpan(
                    text:
                        ' have a direct object because there is no direct acting agent. The following sentences are grammatically incorrect.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough),
                            ),
                            VocabTooltip(
                              message: tsuku,
                              inlineSpan: const TextSpan(
                                text: 'ついた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '（「を」 should be replaced with 「が」 or 「は」）',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough),
                            ),
                            VocabTooltip(
                              message: kieru,
                              inlineSpan: const TextSpan(
                                text: '消える',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '（「を」 should be replaced with 「が」 or 「は」）',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: doushite,
                              inlineSpan: const TextSpan(
                                text: 'どうして',
                              ),
                            ),
                            VocabTooltip(
                              message: mado,
                              inlineSpan: const TextSpan(
                                text: '窓',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aku,
                              inlineSpan: const TextSpan(
                                text: '開いた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '（「を」 should be replaced with 「が」 or 「は」）',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The only time you can use the 「を」 particle for intransitive verbs is when a location is the direct object of a motion verb as briefly described in the previous section.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: deru,
                              inlineSpan: const TextSpan(
                                text: '出た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I left room.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'The important lesson to take away here is to learn how to use the correct particle for the correct type of verb. It might be difficult at first to grasp which is which when learning new verbs or whether there even is a transitive/intransitive distinction. If you’re not sure, you can always check whether a verb is transitive or intransitive by using an online dictionary such as jisho.org. Examples 私が電気をつけた。 Learn Japanese. 電気がついた。 The lights turned on. 電気を消す。 Turn off the lights. 電気が消える。 Lights turn off. 誰が窓を開けた？ Who opened the window? 窓がどうして開いた？ Why has the window opened? The important thing to remember is that intransitive verbs cannot have a direct object because there is no direct acting agent. The following sentences are grammatically incorrect. 電気をついた。（「を」 should be replaced with 「が」 or 「は」）電気を消える。（「を」 should be replaced with 「が」 or 「は」）どうして窓を開いた。（「を」 should be replaced with 「が」 or 「は」） The only time you can use the 「を」 particle for intransitive verbs is when a location is the direct object of a motion verb as briefly described in the previous section. 部屋を出た。 I left room.',
      grammars: [],
      keywords: ['particles'],
    ),
  ],
);
