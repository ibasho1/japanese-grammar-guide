import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/exercises/negative_verb_conjugation_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson negativeVerbs = Lesson(
  title: 'Negative Verbs',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now that we’ve seen how to declare things and perform actions with verbs, we want to be able to say the negative. In other words, we want to say that such-and-such action was not performed. This is done by conjugating the verb to the negative form just like the state-of-being for nouns and adjectives. However, the rules are a tad more complicated.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Now that we’ve seen how to declare things and perform actions with verbs, we want to be able to say the negative. In other words, we want to say that such-and-such action was not performed. This is done by conjugating the verb to the negative form just like the state-of-being for nouns and adjectives. However, the rules are a tad more complicated.',
      grammars: [],
      keywords: ['negative'],
    ),
    Section(
      heading: 'Conjugating verbs into the negative',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('お金 【お・かね】 – money'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('猫 【ねこ】 – cat'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We will now make use of the verb classifications we learned in the last section to define the rules for conjugation. But before we get into that, we need to cover one very important exception to the negative conjugation rules: 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const BulletedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('いる (ru-verb) – to exist (animate)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is an u-verb used to express existence of inanimate objects. The equivalent verb for animate objects (such as people or animals) is 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, which is a normal ru-verb. For example, if you wanted to say that a chair is in the room, you would use the verb 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」, but if you wanted to say that a ',
                  ),
                  const TextSpan(
                    text: 'person',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text: ' is in the room, you must use the verb 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 instead. These two verbs 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 are quite different from all other verbs because they describe existence and are not actual actions. You also need to be careful to choose the correct one based on animate or inanimate objects.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Anyway, the reason I bring it up here is because the negative of 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 (meaning that something does not exist). The conjugation rules for all other verbs are listed below as well as a list of example verbs and their negative forms.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: '＊ = exceptions particular to this conjugation',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Conjugation rules for negative verbs',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Drop the 「る」 and attach 「ない」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + ない = ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: '＊For u-verbs that end in 「う」:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace 「う」 with 「わ」 and attach 「ない」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kau,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '買',
                                        ),
                                        TextSpan(
                                          text: 'う',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + わ + ない = ',
                                  ),
                                  VocabTooltip(
                                    message: kau,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '買',
                                        ),
                                        TextSpan(
                                          text: 'わない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For all other u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Replace the u-vowel sound with the a-vowel equivalent and attach 「ない」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'つ',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + た + ない = ',
                                  ),
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'たない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Exceptions:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'しない',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'くる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'こない',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '＊',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ある',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ない',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'Negative form conjugation examples'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'ru-verb',
                            ),
                            TableHeading(
                              text: 'u-verb',
                            ),
                            TableHeading(
                              text: 'exception',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: miru,
                                      inlineSpan: const TextSpan(
                                        text: '見る',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: miru,
                                      inlineSpan: const TextSpan(
                                        text: '見ない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hanasu,
                                      inlineSpan: const TextSpan(
                                        text: '話す',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: hanasu,
                                      inlineSpan: const TextSpan(
                                        text: '話さない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: const TextSpan(
                                        text: 'する',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: const TextSpan(
                                        text: 'しない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: const TextSpan(
                                        text: '食べる',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: const TextSpan(
                                        text: '食べない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kiku,
                                      inlineSpan: const TextSpan(
                                        text: '聞く',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kiku,
                                      inlineSpan: const TextSpan(
                                        text: '聞かない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kuru,
                                      inlineSpan: const TextSpan(
                                        text: 'くる',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kuru,
                                      inlineSpan: const TextSpan(
                                        text: 'こない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: neruSleep,
                                      inlineSpan: const TextSpan(
                                        text: '寝る',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: neruSleep,
                                      inlineSpan: const TextSpan(
                                        text: '寝ない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: oyogu,
                                      inlineSpan: const TextSpan(
                                        text: '泳ぐ',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: oyogu,
                                      inlineSpan: const TextSpan(
                                        text: '泳がない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: '＊',
                                    ),
                                    VocabTooltip(
                                      message: aru,
                                      inlineSpan: const TextSpan(
                                        text: 'ある',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: aru,
                                      inlineSpan: const TextSpan(
                                        text: 'ない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: okiru,
                                      inlineSpan: const TextSpan(
                                        text: '起きる',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: okiru,
                                      inlineSpan: const TextSpan(
                                        text: '起きない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: const TextSpan(
                                        text: '遊ぶ',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: const TextSpan(
                                        text: '遊ばない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kangaeru,
                                      inlineSpan: const TextSpan(
                                        text: '考える',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kangaeru,
                                      inlineSpan: const TextSpan(
                                        text: '考えない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: matsu,
                                      inlineSpan: const TextSpan(
                                        text: '待つ',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: matsu,
                                      inlineSpan: const TextSpan(
                                        text: '待たない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: oshieru,
                                      inlineSpan: const TextSpan(
                                        text: '教える',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: oshieru,
                                      inlineSpan: const TextSpan(
                                        text: '教えない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: const TextSpan(
                                        text: '飲む',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: const TextSpan(
                                        text: '飲まない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: deru,
                                      inlineSpan: const TextSpan(
                                        text: '出る',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: deru,
                                      inlineSpan: const TextSpan(
                                        text: '出ない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: '＊',
                                    ),
                                    VocabTooltip(
                                      message: kau,
                                      inlineSpan: const TextSpan(
                                        text: '買う',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kau,
                                      inlineSpan: const TextSpan(
                                        text: '買わない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kiruWear,
                                      inlineSpan: const TextSpan(
                                        text: '着る',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kiruWear,
                                      inlineSpan: const TextSpan(
                                        text: '着ない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kaeru,
                                      inlineSpan: const TextSpan(
                                        text: '帰る',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: kaeru,
                                      inlineSpan: const TextSpan(
                                        text: '帰らない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: iru,
                                      inlineSpan: const TextSpan(
                                        text: 'いる',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: iru,
                                      inlineSpan: const TextSpan(
                                        text: 'いない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shinu,
                                      inlineSpan: const TextSpan(
                                        text: '死ぬ',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: ' → ',
                                    ),
                                    VocabTooltip(
                                      message: shinu,
                                      inlineSpan: const TextSpan(
                                        text: '死なない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here are the example sentences from the last section conjugated to the negative form.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスは',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                  text: '食べない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for Alice, does not eat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ジムが',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: TextSpan(
                                  text: '遊ばない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Jim is the one that does not play.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブも',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                  text: 'しない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob also does not do.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                  text: 'ない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There is no money. (lit: Money is the thing that does not exist.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                  text: '買わない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for me, not buy.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: TextSpan(
                                  text: 'いない',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There is no cat. (lit: As for cat, does not exist.)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ある (u-verb) – to exist (inanimate) いる (ru-verb) – to exist (animate) 食べる 【た・べる】 (ru-verb) – to eat 買う 【か・う】 (u-verb) – to buy 待つ 【ま・つ】 (u-verb) – to wait する (exception) – to do 来る 【く・る】 (exception) – to come 見る 【み・る】 (ru-verb) – to see 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 考える 【かんが・える】 (ru-verb) – to think 教える 【おし・える】 (ru-verb) – to teach; to inform 出る 【で・る】 (ru-verb) – to come out 着る 【き・る】 (ru-verb) – to wear 話す 【はな・す】 (u-verb) – to speak 聞く 【き・く】 (u-verb) – to ask; to listen 泳ぐ 【およ・ぐ】 (u-verb) – to swim 遊ぶ 【あそ・ぶ】 (u-verb) – to play 飲む 【の・む】 (u-verb) – to drink 帰る 【かえ・る】 (u-verb) – to go home 死ぬ 【し・ぬ】 (u-verb) – to die お金 【お・かね】 – money 私 【わたし】 – me, myself, I 猫 【ねこ】 – cat We will now make use of the verb classifications we learned in the last section to define the rules for conjugation. But before we get into that, we need to cover one very important exception to the negative conjugation rules: 「ある」. ある (u-verb) – to exist (inanimate) いる (ru-verb) – to exist (animate) 「ある」 is an u-verb used to express existence of inanimate objects. The equivalent verb for animate objects (such as people or animals) is 「いる」, which is a normal ru-verb. For example, if you wanted to say that a chair is in the room, you would use the verb 「ある」, but if you wanted to say that a person is in the room, you must use the verb 「いる」 instead. These two verbs 「ある」 and 「いる」 are quite different from all other verbs because they describe existence and are not actual actions. You also need to be careful to choose the correct one based on animate or inanimate objects. Anyway, the reason I bring it up here is because the negative of 「ある」 is 「ない」 (meaning that something does not exist). The conjugation rules for all other verbs are listed below as well as a list of example verbs and their negative forms. ＊ = exceptions particular to this conjugation Conjugation rules for negative verbs For ru-verbs: Drop the 「る」 and attach 「ない」 Example: 食べる + ない = 食べない＊For u-verbs that end in 「う」: Replace 「う」 with 「わ」 and attach 「ない」 Example: 買う + わ + ない = 買わない For all other u-verbs: Replace the u-vowel sound with the a-vowel equivalent and attach 「ない」 Example: 待つ + た + ない = 待たない Exceptions: する → しないくる → こない＊ある → ない Examples Here are the example sentences from the last section conjugated to the negative form. アリスは食べない。 As for Alice, does not eat. ジムが遊ばない。 Jim is the one that does not play. ボブもしない。 Bob also does not do. お金がない。 There is no money. (lit: Money is the thing that does not exist.) 私は買わない。 As for me, not buy. 猫はいない。 There is no cat. (lit: As for cat, does not exist.)',
      grammars: ['～ない'],
      keywords: ['negative'],
    ),
    Section(
      heading: 'Negative verb practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is the same list of verbs from the previous practice exercise. We will use the same verbs from the last exercise to practice conjugating to the negative.',
                  ),
                ],
              ),
              NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                              'I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                          NumberedList(
                            items: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%A6%8B%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '見 – see'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%9D%A5%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '来 – come; next'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%9D%A2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '行 – go; conduct'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%A1%8C%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '帰 – go home'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%A3%9F%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '食 – eat; food'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%A3%B2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '飲 – drink'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%B2%B7%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '買 – buy'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%A3%B2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '売 – sell'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%8C%81%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '持 – hold'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%BE%85%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '待 – wait'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%AA%AD%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '読 – read'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%AD%A9%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '歩 – walk'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%B5%B0%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '走 – run'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%81%8A%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '遊 – play'),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Kanji'),
              const NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here is a list of some common verbs you will definitely want to learn at some point.'),
                        NumberedList(
                          items: [
                            Text(
                              'する – to do',
                            ),
                            Text(
                              'しゃべる – to talk; to chat',
                            ),
                            Text(
                              '見る【み・る】 – to see',
                            ),
                            Text(
                              '来る【く・る】 – to come',
                            ),
                            Text(
                              '行く【い・く】 – to go',
                            ),
                            Text(
                              '帰る 【かえ・る】 – to go home',
                            ),
                            Text(
                              '食べる 【たべ・る】 – to eat',
                            ),
                            Text(
                              '飲む 【の・む】 – to drink',
                            ),
                            Text(
                              '買う 【か・う】 – to buy',
                            ),
                            Text(
                              '売る 【う・る】 – to sell',
                            ),
                            Text(
                              '切る 【き・る】 – to cut',
                            ),
                            Text(
                              '入る 【はい・る】 – to enter',
                            ),
                            Text(
                              '出る 【で・る】 – to come out',
                            ),
                            Text(
                              '持つ 【も・つ】 – to hold',
                            ),
                            Text(
                              '待つ 【ま・つ】 – to wait',
                            ),
                            Text(
                              '書く【か・く】 – to write',
                            ),
                            Text(
                              '読む 【よ・む】 – to read',
                            ),
                            Text(
                              '歩く 【ある・く】 – to walk',
                            ),
                            Text(
                              '走る 【はし・る】 – to run',
                            ),
                            Text(
                              '遊ぶ 【あそ・ぶ】 – to play',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Vocabulary',
              ),
              const Heading(
                  text: 'Practice with Negative Verb Conjugations', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We learned how to classify the following verbs in the previous practice exercise. Now, we are going to put that knowledge to use by conjugating the same verbs into the negative depending on which type of verb it is. The first answer has been given as an example.',
                  ),
                ],
              ),
              const NegativeVerbConjugationExercise(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section This is the same list of verbs from the previous practice exercise. We will use the same verbs from the last exercise to practice conjugating to the negative. Kanji I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 見 – see 来 – come; next 行 – go; conduct 帰 – go home 食 – eat; food 飲 – drink 買 – buy 売 – sell 持 – hold 待 – wait 読 – read 歩 – walk 走 – run 遊 – play Vocabulary Here is a list of some common verbs you will definitely want to learn at some point. する – to do しゃべる – to talk; to chat 見る【み・る】 – to see 来る【く・る】 – to come 行く【い・く】 – to go 帰る 【かえ・る】 – to go home 食べる 【たべ・る】 – to eat 飲む 【の・む】 – to drink 買う 【か・う】 – to buy 売る 【う・る】 – to sell 切る 【き・る】 – to cut 入る 【はい・る】 – to enter 出る 【で・る】 – to come out 持つ 【も・つ】 – to hold 待つ 【ま・つ】 – to wait 書く【か・く】 – to write 読む 【よ・む】 – to read 歩く 【ある・く】 – to walk 走る 【はし・る】 – to run 遊ぶ 【あそ・ぶ】 – to play Practice with Negative Verb Conjugations We learned how to classify the following verbs in the previous practice exercise. Now, we are going to put that knowledge to use by conjugating the same verbs into the negative depending on which type of verb it is. The first answer has been given as an example. 行く 行かない 出る 出ない する しない 買う 買わない 売る 売らない 食べる 食べない 入る 入らない 来る こない 飲む 飲まない しゃべる しゃべらない 見る 見ない 切る 切らない 帰る 帰らない 書く 書かない',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
