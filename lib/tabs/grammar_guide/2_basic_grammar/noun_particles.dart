import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson nounParticles = Lesson(
  title: 'Noun-Related Particles',
  sections: [
    Section(
      heading: 'The last three particles (not!)',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Paragraph(
              texts: [
                TextSpan(
                  text:
                      'We have already gone over very powerful constructs that can express almost anything we want. We will see the 「の」 particle will give us even more power by allowing us to define a generic, abstract noun. We will also learn how to modify nouns directly with nouns. The three particles we will cover can group nouns together in different ways.',
                ),
              ],
            ),
            Paragraph(
              texts: [
                TextSpan(
                  text:
                      'This is the last lesson that will be specifically focused on particles but that does not mean that there are no more particles to learn. We will learn many more particles along the way but they may not be labeled as such. As long as you know what they mean and how to use them, it is not too important to know whether they are particles or not.',
                ),
              ],
            ),],
          );
        },
      ),
      plaintext: 'We have already gone over very powerful constructs that can express almost anything we want. We will see the 「の」 particle will give us even more power by allowing us to define a generic, abstract noun. We will also learn how to modify nouns directly with nouns. The three particles we will cover can group nouns together in different ways. This is the last lesson that will be specifically focused on particles but that does not mean that there are no more particles to learn. We will learn many more particles along the way but they may not be labeled as such. As long as you know what they mean and how to use them, it is not too important to know whether they are particles or not.',
      grammars: [],
      keywords: ['particle','noun-related'],
    ),
Section(
      heading: 'The inclusive 「と」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
            const NumberedList(
              items: [
                Text('ナイフ – knife'),
                Text('フォーク – fork'),
                Text('ステーキ – steak'),
                Text('食べる 【た・べる】 (ru-verb) – to eat'),
                Text('本 【ほん】 – book'),
                Text('雑誌 【ざっ・し】 – magazine'),
                Text('葉書 【はがき】 – postcard'),
                Text('買う 【か・う】 (u-verb) – to buy'),
                Text('友達 【とも・だち】 – friend'),
                Text('話す 【はな・す】 (u-verb) – to speak'),
                Text('先生 【せん・せい】 – teacher'),
                Text('会う 【あ・う】 (u-verb) – to meet'),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The 「と」 particle is similar to the 「も」 particle in that it contains a meaning of inclusion. It can combine two or more nouns together to mean “and”.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: naifu,
                                inlineSpan: const TextSpan(
                                  text: 'ナイフ',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: fooku,
                                inlineSpan: const TextSpan(
                                  text: 'フォーク',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'で',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: suteeki,
                                inlineSpan: const TextSpan(
                                  text: 'ステーキ',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: const TextSpan(
                                  text: '食べた',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Ate steak by means of knife and fork.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: hon,
                                inlineSpan: const TextSpan(
                                  text: '本',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: zasshi,
                                inlineSpan: const TextSpan(
                                  text: '雑誌',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: hagaki,
                                inlineSpan: const TextSpan(
                                  text: '葉書',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kau,
                                inlineSpan: const TextSpan(
                                  text: '買った',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Bought book, magazine, and post card.')
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'Another similar use of the 「と」 particle is to show an action that was done together with someone or something else.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: tomodachi,
                                inlineSpan: const TextSpan(
                                  text: '友達',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: hanasu,
                                inlineSpan: const TextSpan(
                                  text: '話した',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Talked with friend.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: sensei,
                                inlineSpan: const TextSpan(
                                  text: '先生',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: au,
                                inlineSpan: const TextSpan(
                                  text: '会った',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Met with teacher.')
                  ],
                ),
              ],
            ),],
          );
        },
      ),
      plaintext: 'Vocabulary ナイフ – knife フォーク – fork ステーキ – steak 食べる 【た・べる】 (ru-verb) – to eat 本 【ほん】 – book 雑誌 【ざっ・し】 – magazine 葉書 【はがき】 – postcard 買う 【か・う】 (u-verb) – to buy 友達 【とも・だち】 – friend 話す 【はな・す】 (u-verb) – to speak 先生 【せん・せい】 – teacher 会う 【あ・う】 (u-verb) – to meet The 「と」 particle is similar to the 「も」 particle in that it contains a meaning of inclusion. It can combine two or more nouns together to mean “and”. ナイフとフォークでステーキを食べた。 Ate steak by means of knife and fork. 本と雑誌と葉書を買った。 Bought book, magazine, and post card. Another similar use of the 「と」 particle is to show an action that was done together with someone or something else. 友達と話した。 Talked with friend. 先生と会った。 Met with teacher.',
      grammars: ['と'],
      keywords: ['to','particle'],
    ),
Section(
      heading: 'The vague listing 「や」 and 「とか」 particles',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
            const NumberedList(
              items: [
                Text('飲み物 【の・み・もの】 – beverage'),
                Text('カップ – cup'),
                Text('ナプキン – napkin'),
                Text('いる (u-verb) – to need'),
                Text('靴 【くつ】 – shoes'),
                Text('シャツ – shirt'),
                Text('買う 【か・う】 (u-verb) – to buy'),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The 「や」 particle, just like the 「と」 particle, is used to list one or more nouns except that it is much more vague than the 「と」 particle. It implies that there may be other things that are unlisted and that not all items in the list may apply. In English, you might think of this as an “and/or, etc.” type of listing.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: nomimono,
                                inlineSpan: const TextSpan(
                                  text: '飲み物',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'や',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kappu,
                                inlineSpan: const TextSpan(
                                  text: 'カップ',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'や',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: napukin,
                                inlineSpan: const TextSpan(
                                  text: 'ナプキン',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: iruNeed,
                                inlineSpan: const TextSpan(
                                  text: 'いらない',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                        'You don’t need (things like) drink, cup, or napkin, etc.?')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kutsu,
                                inlineSpan: const TextSpan(
                                  text: '靴',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'や',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: shatsu,
                                inlineSpan: const TextSpan(
                                  text: 'シャツ',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kau,
                                inlineSpan: const TextSpan(
                                  text: '買う',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Buy (things like) shoes and shirt, etc…')
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      '「とか」 also has the same meaning as 「や」 but is a slightly more colloquial expression.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: nomimono,
                                inlineSpan: const TextSpan(
                                  text: '飲み物',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'とか',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kappu,
                                inlineSpan: const TextSpan(
                                  text: 'カップ',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'とか',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: napukin,
                                inlineSpan: const TextSpan(
                                  text: 'ナプキン',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: iruNeed,
                                inlineSpan: const TextSpan(
                                  text: 'いらない',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                        'You don’t need (things like) drink, cup, or napkin, etc.?')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kutsu,
                                inlineSpan: const TextSpan(
                                  text: '靴',
                                ),
                              ),
                            ],
                          ),
                          TextSpan(
                            text: 'とか',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: shatsu,
                                inlineSpan: const TextSpan(
                                  text: 'シャツ',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kau,
                                inlineSpan: const TextSpan(
                                  text: '買う',
                                ),
                              ),
                            ],
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Buy (things like) shoes and shirt, etc…')
                  ],
                ),
              ],
            ),],
          );
        },
      ),
      plaintext: 'Vocabulary 飲み物 【の・み・もの】 – beverage カップ – cup ナプキン – napkin いる (u-verb) – to need 靴 【くつ】 – shoes シャツ – shirt 買う 【か・う】 (u-verb) – to buy The 「や」 particle, just like the 「と」 particle, is used to list one or more nouns except that it is much more vague than the 「と」 particle. It implies that there may be other things that are unlisted and that not all items in the list may apply. In English, you might think of this as an “and/or, etc.” type of listing. 飲み物やカップやナプキンは、いらない？ You don’t need (things like) drink, cup, or napkin, etc.? 靴やシャツを買う。 Buy (things like) shoes and shirt, etc…「とか」 also has the same meaning as 「や」 but is a slightly more colloquial expression. 飲み物とかカップとかナプキンは、いらない？ You don’t need (things like) drink, cup, or napkin, etc.? 靴とかシャツを買う。 Buy (things like) shoes and shirt, etc…',
      grammars: ['や','とか'],
      keywords: ['ya','toka','particle','list','listing'],
    ),
Section(
      heading: 'The 「の」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
            const NumberedList(
              items: [
                Text('本 【ほん】 – book'),
                Text('アメリカ – America'),
                Text('大学 【だい・がく】 – college'),
                Text('学生 【がく・せい】 – student'),
                Text('それ – that'),
                Text('その – abbreviation of 「それの」'),
                Text('シャツ – shirt'),
                Text('誰 【だれ】 – who'),
                Text('これ – this'),
                Text('この – abbreviation of 「これの」'),
                Text('あれ – that (over there)'),
                Text('あの – abbreviation of 「あれの」'),
                Text('白い 【し・ろい】 (i-adj) – white'),
                Text('かわいい (i-adj) – cute'),
                Text('授業 【じゅ・ぎょう】 – class'),
                Text('行く 【い・く】 (u-verb) – to go'),
                Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                Text('こと – event, matter'),
                Text('毎日 【まい・にち】 – every day'),
                Text('勉強 【べん・きょう】 – study'),
                Text('する (exception) – to do'),
                Text('大変 【たい・へん】 (na-adj) – tough, hard time'),
                Text('同じ 【おな・じ】 – same'),
                Text('物 【もの】 – object'),
                Text('食べる 【た・べる】 (ru-verb) – to eat'),
                Text('面白い 【おも・し・ろい】 (i-adj) – interesting'),
                Text('静か 【しず・か】 (na-adj) – quiet'),
                Text('部屋 【へ・や】 – room'),
                Text('人 【ひと】 – person'),
                Text('学校 【がっ・こう】 – school'),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The 「の」 particle has many uses and it is a very powerful particle. It is introduced here because like the 「と」 and 「や」 particle, it can be used to connect one or more nouns. Let’s look at a few examples.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ボブ',
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: hon,
                            inlineSpan: const TextSpan(
                              text: '本',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Book of Bob.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hon,
                            inlineSpan: const TextSpan(
                              text: '本',
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: 'ボブ',
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Bob of book.')
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The first sentence essentially means, “Bob’s book.” (not a lost bible chapter). The second sentence means, “Book’s Bob” which is probably a mistake. I’ve translated the first example as “book of Bob” because the 「の」 particle doesn’t always imply possession as the next example shows.',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ボブは、',
                          ),
                          VocabTooltip(
                            message: amerika,
                            inlineSpan: const TextSpan(
                              text: 'アメリカ',
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: daigaku,
                            inlineSpan: const TextSpan(
                              text: '大学',
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Bob is student of college of America.')
                  ],
                ),
              ],
            ),
            Paragraph(
              texts: [
                const TextSpan(
                  text:
                      'In normal English, this would translate to, “Bob is a student of an American college.” The order of modification is backwards so Bob is a student of a college that is American. 「',
                ),
                VocabTooltip(
                  message: gakusei,
                  inlineSpan: const TextSpan(
                    text: '学生',
                  ),
                ),
                const TextSpan(
                  text: 'の',
                ),
                VocabTooltip(
                  message: daigaku,
                  inlineSpan: const TextSpan(
                    text: '大学',
                  ),
                ),
                const TextSpan(
                  text: 'の',
                ),
                VocabTooltip(
                  message: amerika,
                  inlineSpan: const TextSpan(
                    text: 'アメリカ',
                  ),
                ),
                const TextSpan(
                  text:
                      '」 means “America of college of student” which is probably an error and makes little sense. (America of student’s college?)',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The noun that is being modified can be omitted if the context clearly indicates what is being omitted. The following highlighted redundant words can be omitted.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sono,
                            inlineSpan: const TextSpan(
                              text: 'その',
                            ),
                          ),
                          VocabTooltip(
                            message: shatsu,
                            inlineSpan: const TextSpan(
                              text: 'シャツ',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: dare,
                            inlineSpan: const TextSpan(
                              text: '誰',
                            ),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: shatsu,
                            inlineSpan: TextSpan(
                              text: 'シャツ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                    const Text('Whose shirt is that shirt?')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ボブの',
                          ),
                          VocabTooltip(
                            message: shatsu,
                            inlineSpan: TextSpan(
                              text: 'シャツ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                    const Text('It is shirt of Bob.')
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text: 'to become:',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sono,
                            inlineSpan: const TextSpan(
                              text: 'その',
                            ),
                          ),
                          VocabTooltip(
                            message: shatsu,
                            inlineSpan: const TextSpan(
                              text: 'シャツ',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: dare,
                            inlineSpan: const TextSpan(
                              text: '誰',
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                    const Text('Whose shirt is that?')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ボブ',
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                    const Text('It is of Bob.')
                  ],
                ),
              ],
            ),
            Paragraph(
              texts: [
                const TextSpan(
                  text: '（「',
                ),
                VocabTooltip(
                  message: sono,
                  inlineSpan: const TextSpan(
                    text: 'その',
                  ),
                ),
                const TextSpan(
                  text: '」 is an abbreviation of 「',
                ),
                VocabTooltip(
                  message: sore,
                  inlineSpan: const TextSpan(
                    text: 'それ',
                  ),
                ),
                const TextSpan(
                  text:
                      '+の」 so it directly modifies the noun because the 「の」 particle is intrinsically attached. Other words include 「',
                ),
                VocabTooltip(
                  message: kono,
                  inlineSpan: const TextSpan(
                    text: 'この',
                  ),
                ),
                const TextSpan(
                  text: '」 from 「',
                ),
                VocabTooltip(
                  message: kore,
                  inlineSpan: const TextSpan(
                    text: 'これ',
                  ),
                ),
                const TextSpan(
                  text: 'の」 and 「',
                ),
                VocabTooltip(
                  message: ano,
                  inlineSpan: const TextSpan(
                    text: 'あの',
                  ),
                ),
                const TextSpan(
                  text: '」 from 「',
                ),
                VocabTooltip(
                  message: are,
                  inlineSpan: const TextSpan(
                    text: 'あれ',
                  ),
                ),
                const TextSpan(
                  text: 'の」.）',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The 「の」 particle in this usage essentially replaces the noun and takes over the role as a noun itself. We can essentially treat adjectives and verbs just like nouns by adding the 「の」 particle to it. The particle then becomes a generic noun, which we can treat just like a regular noun.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shiroi,
                            inlineSpan: TextSpan(
                              text: '白い',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          VocabTooltip(
                            message: kawaii,
                            inlineSpan: const TextSpan(
                              text: 'かわいい',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Thing that is white is cute.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jugyou,
                            inlineSpan: TextSpan(
                              text: '授業',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'に',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: TextSpan(
                              text: '行く',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'の',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: wasureru,
                            inlineSpan: const TextSpan(
                              text: '忘れた',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Forgot the event of going to class.')
                  ],
                ),
              ],
            ),
            Paragraph(
              texts: [
                const TextSpan(
                  text:
                      'Now we can use the direct object, topic, and identifier particle with verbs and adjectives. We don’t necessarily have to use the 「の」 particle here. We can use the noun 「',
                ),
                VocabTooltip(
                  message: mono,
                  inlineSpan: const TextSpan(
                    text: '物',
                  ),
                ),
                const TextSpan(
                  text: '」, which is a generic object or 「',
                ),
                VocabTooltip(
                  message: koto,
                  inlineSpan: const TextSpan(
                    text: 'こと',
                  ),
                ),
                const TextSpan(
                  text: '」 for a generic event. For example, we can also say:',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shiroi,
                            inlineSpan: const TextSpan(
                              text: '白い',
                            ),
                          ),
                          VocabTooltip(
                            message: mono,
                            inlineSpan: TextSpan(
                              text: '物',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          VocabTooltip(
                            message: kawaii,
                            inlineSpan: const TextSpan(
                              text: 'かわいい',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Thing that is white is cute.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jugyou,
                            inlineSpan: const TextSpan(
                              text: '授業',
                            ),
                          ),
                          TextSpan(
                            text: 'に',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行く',
                            ),
                          ),
                          VocabTooltip(
                            message: koto,
                            inlineSpan: TextSpan(
                              text: 'こと',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: wasureru,
                            inlineSpan: const TextSpan(
                              text: '忘れた',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('Forgot the thing of going to class.')
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'However, the 「の」 particle is very useful in that you don’t have to specify a particular noun. In the next examples, the 「の」 particle is not replacing any particular noun, it just allows us to modify verb and adjective clauses like noun clauses. The relative clauses are highlighted.',
                ),
              ],
            ),
            NumberedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: mainichi,
                            inlineSpan: TextSpan(
                              text: '毎日',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: benkyou,
                            inlineSpan: TextSpan(
                              text: '勉強',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'する',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'のは',
                          ),
                          VocabTooltip(
                            message: taihen,
                            inlineSpan: const TextSpan(
                              text: '大変',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text('The thing of studying every day is tough.')
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: mainichi,
                            inlineSpan: TextSpan(
                              text: '毎日',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: onaji,
                            inlineSpan: TextSpan(
                              text: '同じ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: mono,
                            inlineSpan: TextSpan(
                              text: '物',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'を',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: taberu,
                            inlineSpan: TextSpan(
                              text: '食べる',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'のは、',
                          ),
                          VocabTooltip(
                            message: omoshiroi,
                            inlineSpan: const TextSpan(
                              text: '面白くない',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'It’s not interesting to eat same thing every day.',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'Even when substituting 「の」 for a noun, you still need the 「な」 to modify the noun when a na-adjective is being used.',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shizuka,
                            inlineSpan: const TextSpan(
                              text: '静か',
                            ),
                          ),
                          TextSpan(
                            text: 'な',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: heya,
                            inlineSpan: const TextSpan(
                              text: '部屋',
                            ),
                          ),
                          const TextSpan(
                            text: 'が、アリスの',
                          ),
                          VocabTooltip(
                            message: heya,
                            inlineSpan: const TextSpan(
                              text: '部屋',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'Quiet room is room of Alice.',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text: 'becomes:',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shizuka,
                            inlineSpan: const TextSpan(
                              text: '静か',
                            ),
                          ),
                          TextSpan(
                            text: 'な',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'のが、アリスの',
                          ),
                          VocabTooltip(
                            message: heya,
                            inlineSpan: const TextSpan(
                              text: '部屋',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'Quiet one is room of Alice.',
                    ),
                  ],
                ),
              ],
            ),
            Paragraph(
              texts: [
                const TextSpan(
                  text:
                      '*Warning: This may make things seem like you can replace any arbitrary nouns with 「の」 but this is not so. It is important to realize that the sentence must be about the clause and not the noun that was replaced. For example, in the last section we had the sentence, 「',
                ),
                VocabTooltip(
                  message: gakusei,
                  inlineSpan: const TextSpan(
                    text: '学生',
                  ),
                ),
                const TextSpan(
                  text: 'じゃない',
                ),
                VocabTooltip(
                  message: hito,
                  inlineSpan: const TextSpan(
                    text: '人',
                  ),
                ),
                const TextSpan(
                  text: 'は、 ',
                ),
                VocabTooltip(
                  message: gakkou,
                  inlineSpan: const TextSpan(
                    text: '学校',
                  ),
                ),
                const TextSpan(
                  text: 'に',
                ),
                VocabTooltip(
                  message: iku,
                  inlineSpan: const TextSpan(
                    text: '行かない',
                  ),
                ),
                const TextSpan(
                  text: '」. You may think that you can just replace 「',
                ),
                VocabTooltip(
                  message: hito,
                  inlineSpan: const TextSpan(
                    text: '人',
                  ),
                ),
                const TextSpan(
                  text: '」 with 「の」 to produce 「',
                ),
                VocabTooltip(
                  message: gakusei,
                  inlineSpan: const TextSpan(
                    text: '学生',
                  ),
                ),
                const TextSpan(
                  text: 'じゃない',
                ),
                TextSpan(
                    text: 'の',
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.primary)),
                const TextSpan(
                  text: 'は、 ',
                ),
                VocabTooltip(
                  message: gakkou,
                  inlineSpan: const TextSpan(
                    text: '学校',
                  ),
                ),
                const TextSpan(
                  text: 'に',
                ),
                VocabTooltip(
                  message: iku,
                  inlineSpan: const TextSpan(
                    text: '行かない',
                  ),
                ),
                const TextSpan(
                  text:
                      '」. But in fact, this makes no sense because the sentence is now about the clause “Is not student”. The sentence becomes, “The thing of not being student does not go to school” which is complete gibberish because not being a student is a state and it doesn’t make sense for a state to go anywhere much less school.',
                ),
              ],
            ),],
          );
        },
      ),
      plaintext: 'Vocabulary 本 【ほん】 – book アメリカ – America 大学 【だい・がく】 – college 学生 【がく・せい】 – student それ – that その – abbreviation of 「それの」シャツ – shirt 誰 【だれ】 – who これ – this この – abbreviation of 「これの」あれ – that (over there) あの – abbreviation of 「あれの」白い 【し・ろい】 (i-adj) – white かわいい (i-adj) – cute 授業 【じゅ・ぎょう】 – class 行く 【い・く】 (u-verb) – to go 忘れる 【わす・れる】 (ru-verb) – to forget こと – event, matter 毎日 【まい・にち】 – every day 勉強 【べん・きょう】 – study する (exception) – to do 大変 【たい・へん】 (na-adj) – tough, hard time 同じ 【おな・じ】 – same 物 【もの】 – object 食べる 【た・べる】 (ru-verb) – to eat 面白い 【おも・し・ろい】 (i-adj) – interesting 静か 【しず・か】 (na-adj) – quiet 部屋 【へ・や】 – room 人 【ひと】 – person 学校 【がっ・こう】 – school The 「の」 particle has many uses and it is a very powerful particle. It is introduced here because like the 「と」 and 「や」 particle, it can be used to connect one or more nouns. Let’s look at a few examples. ボブの本。 Book of Bob. 本のボブ。 Bob of book. The first sentence essentially means, “Bob’s book.” (not a lost bible chapter). The second sentence means, “Book’s Bob” which is probably a mistake. I’ve translated the first example as “book of Bob” because the 「の」 particle doesn’t always imply possession as the next example shows. ボブは、アメリカの大学の学生だ。 Bob is student of college of America. In normal English, this would translate to, “Bob is a student of an American college.” The order of modification is backwards so Bob is a student of a college that is American. 「学生の大学のアメリカ」 means “America of college of student” which is probably an error and makes little sense. (America of student’s college?) The noun that is being modified can be omitted if the context clearly indicates what is being omitted. The following highlighted redundant words can be omitted. そのシャツは誰のシャツ？ Whose shirt is that shirt? ボブのシャツだ。 It is shirt of Bob.to become: そのシャツは誰の？ Whose shirt is that? ボブのだ。 It is of Bob.+の」 so it directly modifies the noun because the 「の」 particle is intrinsically attached. Other words include 「（「その」 is an abbreviation of 「それこの」 from 「これの」 and 「あの」 from 「あれの」.） The 「の」 particle in this usage essentially replaces the noun and takes over the role as a noun itself. We can essentially treat adjectives and verbs just like nouns by adding the 「の」 particle to it. The particle then becomes a generic noun, which we can treat just like a regular noun. 白いのは、かわいい。 Thing that is white is cute. 授業に行くのを忘れた。 Forgot the event of going to class. Now we can use the direct object, topic, and identifier particle with verbs and adjectives. We don’t necessarily have to use the 「の」 particle here. We can use the noun 「物」, which is a generic object or 「こと」 for a generic event. For example, we can also say: 白い物は、かわいい。 Thing that is white is cute. 授業に行くことを忘れた。 Forgot the thing of going to class. However, the 「の」 particle is very useful in that you don’t have to specify a particular noun. In the next examples, the 「の」 particle is not replacing any particular noun, it just allows us to modify verb and adjective clauses like noun clauses. The relative clauses are highlighted. 毎日勉強するのは大変。 The thing of studying every day is tough. 毎日同じ物を食べるのは、面白くない。 It’s not interesting to eat same thing every day. Even when substituting 「の」 for a noun, you still need the 「な」 to modify the noun when a na-adjective is being used. 静かな部屋が、アリスの部屋だ。 Quiet room is room of Alice. becomes: 静かなのが、アリスの部屋だ。 Quiet one is room of Alice. *Warning: This may make things seem like you can replace any arbitrary nouns with 「の」 but this is not so. It is important to realize that the sentence must be about the clause and not the noun that was replaced. For example, in the last section we had the sentence, 「学生じゃない人は、 学校に行かない」. You may think that you can just replace 「人」 with 「の」 to produce 「学生じゃないのは、 学校に行かない」. But in fact, this makes no sense because the sentence is now about the clause “Is not student”. The sentence becomes, “The thing of not being student does not go to school” which is complete gibberish because not being a student is a state and it doesn’t make sense for a state to go anywhere much less school.',
      grammars: ['の'],
      keywords: ['no','particle'],
    ),
Section(
      heading: 'The 「の」 particle as explanation',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            const YouTubeLink(title: 'Japanese from Scratch 2.3.0 - Explanations and expectations', uri: 'https://youtu.be/D7wRJug13d0'),
            const YouTubeLink(title: 'Japanese from Scratch 2.3.1 - Conjugating with the explanatory 「の」', uri: 'https://youtu.be/obOAAmHHHVI'),
            const Heading(text: 'Vocabulary', level: 2),
            const NumberedList(
              items: [
                Text('今 【いま】 – now'),
                Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                Text('学生 【がく・せい】 – student'),
                Text('飲む 【のむ】 – to drink'),
                Text('どこ – where'),
                Text('行く 【い・く】 (u-verb) – to go'),
                Text('授業 【じゅ・ぎょう】 – class'),
                Text('ある (u-verb) – to exist (inanimate)'),
                Text('ううん – casual word for “no” (nah, uh-uh)'),
                Text('その – that （abbr. of それの）'),
                Text('人 【ひと】 – person'),
                Text('買う 【か・う】 (u-verb) – to buy'),
                Text('先生 【せん・せい】 – teacher'),
                Text('朝ご飯 【あさ・ご・はん】 – breakfast'),
                Text('食べる 【た・べる】 (ru-verb) – to eat'),
                Text('どうして – why'),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'The 「の」 particle attached at the end of the last clause of a sentence can also convey an explanatory tone to your sentence. For example, if someone asked you if you have time, you might respond, “The thing is I’m kind of busy right now.” The abstract generic noun of “the thing is…” can also be expressed with the 「の」 particle. This type of sentence has an embedded meaning that explains the reason(s) for something else.',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text: 'The sentence would be expressed like so:',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ima,
                            inlineSpan: const TextSpan(
                              text: '今',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: isogashii,
                            inlineSpan: const TextSpan(
                              text: '忙しい',
                            ),
                          ),
                          TextSpan(
                              text: 'の',
                              style: TextStyle(
                                  color:
                                      Theme.of(context).colorScheme.primary)),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'The thing is that (I’m) busy now.',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'This sounds very soft and feminine. In fact, adult males will almost always add a declarative 「だ」 unless they want to sound cute for some reason.',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ima,
                            inlineSpan: const TextSpan(
                              text: '今',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: isogashii,
                            inlineSpan: const TextSpan(
                              text: '忙しい',
                            ),
                          ),
                          TextSpan(
                              text: 'のだ',
                              style: TextStyle(
                                  color:
                                      Theme.of(context).colorScheme.primary)),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'The thing is that (I’m) busy now.',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'However, since the declarative 「だ」 cannot be used in a question, the same 「の」 in questions do not carry a feminine tone at all and is used by both males and females.',
                ),
              ],
            ),
            BulletedList(
              items: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ima,
                            inlineSpan: const TextSpan(
                              text: '今',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: isogashii,
                            inlineSpan: const TextSpan(
                              text: '忙しい',
                            ),
                          ),
                          TextSpan(
                              text: 'の',
                              style: TextStyle(
                                  color:
                                      Theme.of(context).colorScheme.primary)),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'Is it that (you) are busy now? (gender-neutral)',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'To express state-of-being, when the 「の」 particle is used to convey this explanatory tone, ',
                ),
                TextSpan(
                    text: 'we need to add 「な」',
                    style: TextStyle(decoration: TextDecoration.underline)),
                TextSpan(
                  text:
                      ' to distinguish it from the 「の」 particle that simply means “of”.',
                ),
              ],
            ),
            BulletedList(
              items: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'ジムのだ。',
                    ),
                    Text(
                      'It is of Jim. (It is Jim’s.)',
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ジム',
                          ),
                          TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color:
                                      Theme.of(context).colorScheme.primary)),
                          const TextSpan(
                            text: 'のだ。',
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      'It is Jim (with explanatory tone).',
                    ),
                  ],
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'Besides this one case, everything else remains the same as before.',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'In actuality, while this type of explanatory tone is used all the time, 「のだ」 is usually substituted by 「んだ」. This is probably due to the fact that 「んだ」 is easier to say than 「のだ」. This grammar can have what seems like many different meanings because not only can it be used with all forms of adjectives, nouns, and verbs it itself can also be conjugated just like the state-of-being. A conjugation chart will show you what this means.',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'There’s really nothing new here. The first chart is just adding 「んだ」 (or 「なんだ」) to a conjugated verb, noun, or adjective. The second chart adds 「んだ」 (or 「なんだ」) to a non-conjugated verb, noun, adjective and then conjugates the 「だ」 part of 「んだ」 just like a regular state-of-being for nouns and na-adjectives. Just don’t forget to attach the 「な」 for nouns as well as na-adjectives.',
                ),
              ],
            ),
            Padding(
              padding: kDefaultParagraphPadding,
              child: Column(
                children: [
                  const TableCaption(caption: '「んだ」 attached to different conjugations (Substitute 「の」 or 「のだ」 for 「んだ」)'),
                  Table(
                    border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
                    children: [
                      const TableRow(
                        children: [
                          TableHeading(
                            text: ' ',
                          ),
                          TableHeading(
                            text: 'Noun/Na-Adj',
                          ),
                          TableHeading(
                            text: '	Verb/I-Adj',
                          )
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Plain',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'なんだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Negative',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃない',
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲まない',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Past',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'だった',
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲んだ',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Past-Neg',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃなかった',
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲まなかった',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: kDefaultParagraphPadding,
              child: Column(
                children: [
                  const TableCaption(caption: '「んだ」 is conjugated (Substitute 「の」 for 「ん」 and 「の」 or 「のだ」 for 「んだ」)'),
                  Table(
                    border: TableBorder.all(
              color: Theme.of(context).colorScheme.outline,
            ),
                    children: [
                      const TableRow(
                        children: [
                          TableHeading(
                            text: ' ',
                          ),
                          TableHeading(
                            text: 'Noun/Na-Adj',
                          ),
                          TableHeading(
                            text: '	Verb/I-Adj',
                          )
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Plain',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'なんだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Negative',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'なんじゃない',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んじゃない',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Past',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'なんだった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んだった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      TableRow(
                        children: [
                          const TableHeading(
                            text: 'Past-Neg',
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'なんじゃなかった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          TableData(
                            centered: false,
                            content: Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'んじゃなかった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'I would say that the past and past-negative forms for noun/na-adjective in the second chart are almost never used (especially with 「の」) but they are presented for completeness.',
                ),
              ],
            ),
            Paragraph(
              texts: [
                const TextSpan(
                  text:
                      'The crucial difference between using the explanatory 「の」 and not using anything at all is that you are telling the listener, “Look, here’s the reason” as opposed to simply imparting new information. For example, if someone asked you, “Are you busy now?” you can simply answer, 「',
                ),
                VocabTooltip(
                  message: ima,
                  inlineSpan: const TextSpan(
                    text: '今',
                  ),
                ),
                const TextSpan(
                  text: 'は',
                ),
                VocabTooltip(
                  message: isogashii,
                  inlineSpan: const TextSpan(
                    text: '忙しい',
                  ),
                ),
                const TextSpan(
                  text:
                      '」. However, if someone asked you, “How come you can’t talk to me?” since you obviously have some explaining to do, you would answer, 「',
                ),
                VocabTooltip(
                  message: ima,
                  inlineSpan: const TextSpan(
                    text: '今',
                  ),
                ),
                const TextSpan(
                  text: 'は',
                ),
                VocabTooltip(
                  message: isogashii,
                  inlineSpan: const TextSpan(
                    text: '忙しい',
                  ),
                ),
                const TextSpan(
                  text: 'の」 or 「',
                ),
                VocabTooltip(
                  message: ima,
                  inlineSpan: const TextSpan(
                    text: '今',
                  ),
                ),
                const TextSpan(
                  text: 'は',
                ),
                VocabTooltip(
                  message: isogashii,
                  inlineSpan: const TextSpan(
                    text: '忙しい',
                  ),
                ),
                const TextSpan(
                  text:
                      'んだ」. This grammar is indispensable for seeking explanations in questions. For instance, if you want to ask, “Hey, isn’t it late?” you can’t just ask, 「',
                ),
                VocabTooltip(
                  message: osoi,
                  inlineSpan: const TextSpan(
                    text: '遅くない',
                  ),
                ),
                const TextSpan(
                  text:
                      '？」 because that means, “It’s not late?” You need to indicate that you are seeking explanation in the form of 「',
                ),
                VocabTooltip(
                  message: osoi,
                  inlineSpan: const TextSpan(
                    text: '遅い',
                  ),
                ),
                const TextSpan(
                  text: 'んじゃない？」.',
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'Let’s see some examples of the types of situations where this grammar is used. The examples will have literal translation to make it easier to see how the meaning stays the same and carries over into what would be very different types of sentences in normal English. A more natural English translation is provided as well because the literal translations can get a bit convoluted.',
                ),
              ],
            ),
            const Heading(text: 'Example 1', level: 2),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'アリス',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: doko,
                          inlineSpan: const TextSpan(
                            text: 'どこ',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iku,
                          inlineSpan: const TextSpan(
                            text: '行く',
                          ),
                        ),
                        TextSpan(
                          text: 'の',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Where is it that (you) are going?'),
                  english: true,
                ),
              ],
            ),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ボブ',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: jugyou,
                          inlineSpan: const TextSpan(
                            text: '授業',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iku,
                          inlineSpan: const TextSpan(
                            text: '行く',
                          ),
                        ),
                        TextSpan(
                          text: 'んだ',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('It is that (I) go to class.'),
                  english: true,
                ),
              ],
            ),
            const Dialogue(
              items: [
                DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Where are you going? (Seeking explanation)'),
                  english: true,
                ),
                DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('I’m going to class. (Explanatory)'),
                  english: true,
                ),
              ],
            ),
            const Heading(text: 'Example 2', level: 2),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'アリス',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ima,
                          inlineSpan: const TextSpan(
                            text: '今',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: jugyou,
                          inlineSpan: const TextSpan(
                            text: '授業',
                          ),
                        ),
                        const TextSpan(
                          text: 'が',
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(
                            text: 'ある',
                          ),
                        ),
                        TextSpan(
                          text: 'んじゃない',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Isn’t it that there is class now?'),
                  english: true,
                ),
              ],
            ),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ボブ',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ima,
                          inlineSpan: const TextSpan(
                            text: '今',
                          ),
                        ),
                        const TextSpan(
                          text: 'は、',
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(
                            text: 'ない',
                          ),
                        ),
                        TextSpan(
                          text: 'んだ',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('Now it is that there is no class.'),
                  english: true,
                ),
              ],
            ),
            const Dialogue(
              items: [
                DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Don’t you have class now? (Expecting that there is class)'),
                  english: true,
                ),
                DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('No, there is no class now. (Explanatory)'),
                  english: true,
                ),
              ],
            ),
            const Heading(text: 'Example 3', level: 2),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'アリス',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ima,
                          inlineSpan: const TextSpan(
                            text: '今',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: jugyou,
                          inlineSpan: const TextSpan(
                            text: '授業',
                          ),
                        ),
                        const TextSpan(
                          text: 'が',
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(
                            text: 'ない',
                          ),
                        ),
                        TextSpan(
                          text: 'んじゃない',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Isn’t it that there isn’t class now?'),
                  english: true,
                ),
              ],
            ),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ボブ',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: uun,
                          inlineSpan: const TextSpan(
                            text: 'ううん',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(
                            text: 'ある',
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('No, there is.'),
                  english: true,
                ),
              ],
            ),
            const Dialogue(
              items: [
                DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Don’t you not have class now? (Expecting that there is no class)'),
                  english: true,
                ),
                DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('No, I do have class.'),
                  english: true,
                ),
              ],
            ),
            const Heading(text: 'Example 4', level: 2),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'アリス',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sono,
                          inlineSpan: const TextSpan(
                            text: 'その',
                          ),
                        ),
                        VocabTooltip(
                          message: hito,
                          inlineSpan: const TextSpan(
                            text: '人',
                          ),
                        ),
                        const TextSpan(
                          text: 'が',
                        ),
                        VocabTooltip(
                          message: kau,
                          inlineSpan: const TextSpan(
                            text: '買う',
                          ),
                        ),
                        TextSpan(
                          text: 'んじゃなかったの',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Wasn’t it that that person was the one to buy?'),
                  english: true,
                ),
              ],
            ),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ボブ',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: uun,
                          inlineSpan: const TextSpan(
                            text: 'ううん',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: sensei,
                          inlineSpan: const TextSpan(
                            text: '先生',
                          ),
                        ),
                        const TextSpan(
                          text: 'が',
                        ),
                        VocabTooltip(
                          message: kau,
                          inlineSpan: const TextSpan(
                            text: '買う',
                          ),
                        ),
                        TextSpan(
                          text: 'んだ',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('No, it is that teacher is the one to buy.'),
                  english: true,
                ),
              ],
            ),
            const Dialogue(
              items: [
                DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Wasn’t that person going to buy? (Expecting that the person would buy)'),
                  english: true,
                ),
                DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('No, the teacher is going to. (Explanatory)'),
                  english: true,
                ),
              ],
            ),
            const Heading(text: 'Example 5', level: 2),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'アリス',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: asagohan,
                          inlineSpan: const TextSpan(
                            text: '朝ご飯',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: const TextSpan(
                            text: '食べる',
                          ),
                        ),
                        TextSpan(
                          text: 'んじゃなかった',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('It is that breakfast wasn’t to eat.'),
                  english: true,
                ),
              ],
            ),
            Dialogue(
              items: [
                DialogueLine(
                  speaker: const Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ボブ',
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: doushite,
                          inlineSpan: const TextSpan(
                            text: 'どうして',
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('Why?'),
                  english: true,
                ),
              ],
            ),
            const Dialogue(
              items: [
                DialogueLine(
                  speaker: Text('Alice'),
                  text: Text('Should not have eaten breakfast, you know. (Explaining that breakfast wasn’t to be eaten)'),
                  english: true,
                ),
                DialogueLine(
                  speaker: Text('Bob'),
                  text: Text('How come?'),
                  english: true,
                ),
              ],
            ),
            const Paragraph(
              texts: [
                TextSpan(
                  text:
                      'Don’t worry if you are thoroughly confused by now, we will see many more examples along the way. Once you get the sense of how everything works, it’s better to forget the English because the double and triple negatives can get quite confusing such as Example 3. However, in Japanese it is a perfectly normal expression, as you will begin to realize once you get accustomed to Japanese.',
                ),
              ],
            ),],
          );
        },
      ),
      plaintext: 'Vocabulary 今 【いま】 – now 忙しい 【いそが・しい】 (i-adj) – busy 学生 【がく・せい】 – student 飲む 【のむ】 – to drink どこ – where 行く 【い・く】 (u-verb) – to go 授業 【じゅ・ぎょう】 – class ある (u-verb) – to exist (inanimate) ううん – casual word for “no” (nah, uh-uh) その – that （abbr. of それの） 人 【ひと】 – person 買う 【か・う】 (u-verb) – to buy 先生 【せん・せい】 – teacher 朝ご飯 【あさ・ご・はん】 – breakfast 食べる 【た・べる】 (ru-verb) – to eat どうして – why The 「の」 particle attached at the end of the last clause of a sentence can also convey an explanatory tone to your sentence. For example, if someone asked you if you have time, you might respond, “The thing is I’m kind of busy right now.” The abstract generic noun of “the thing is…” can also be expressed with the 「の」 particle. This type of sentence has an embedded meaning that explains the reason(s) for something else. The sentence would be expressed like so: 今は忙しいの。 The thing is that (I’m) busy now. This sounds very soft and feminine. In fact, adult males will almost always add a declarative 「だ」 unless they want to sound cute for some reason. 今は忙しいのだ。 The thing is that (I’m) busy now. However, since the declarative 「だ」 cannot be used in a question, the same 「の」 in questions do not carry a feminine tone at all and is used by both males and females. 今は忙しいの？ Is it that (you) are busy now? (gender-neutral) To express state-of-being, when the 「の」 particle is used to convey this explanatory tone, we need to add 「な」 to distinguish it from the 「の」 particle that simply means “of”. ジムのだ。 It is of Jim. (It is Jim’s.) ジムなのだ。 It is Jim (with explanatory tone). Besides this one case, everything else remains the same as before. In actuality, while this type of explanatory tone is used all the time, 「のだ」 is usually substituted by 「んだ」. This is probably due to the fact that 「んだ」 is easier to say than 「のだ」. This grammar can have what seems like many different meanings because not only can it be used with all forms of adjectives, nouns, and verbs it itself can also be conjugated just like the state-of-being. A conjugation chart will show you what this means. There’s really nothing new here. The first chart is just adding 「んだ」 (or 「なんだ」) to a conjugated verb, noun, or adjective. The second chart adds 「んだ」 (or 「なんだ」) to a non-conjugated verb, noun, adjective and then conjugates the 「だ」 part of 「んだ」 just like a regular state-of-being for nouns and na-adjectives. Just don’t forget to attach the 「な」 for nouns as well as na-adjectives. I would say that the past and past-negative forms for noun/na-adjective in the second chart are almost never used (especially with 「の」) but they are presented for completeness. The crucial difference between using the explanatory 「の」 and not using anything at all is that you are telling the listener, “Look, here’s the reason” as opposed to simply imparting new information. For example, if someone asked you, “Are you busy now?” you can simply answer, 「今は忙しい」. However, if someone asked you, “How come you can’t talk to me?” since you obviously have some explaining to do, you would answer, 「今は忙しいの」 or 「今は忙しいんだ」. This grammar is indispensable for seeking explanations in questions. For instance, if you want to ask, “Hey, isn’t it late?” you can’t just ask, 「遅くない？」 because that means, “It’s not late?” You need to indicate that you are seeking explanation in the form of 「遅いんじゃない？」. Let’s see some examples of the types of situations where this grammar is used. The examples will have literal translation to make it easier to see how the meaning stays the same and carries over into what would be very different types of sentences in normal English. A more natural English translation is provided as well because the literal translations can get a bit convoluted. Example 1 アリス：どこに行くの？ Alice: Where is it that (you) are going? ボブ：授業に行くんだ。 Bob: It is that (I) go to class. Alice: Where are you going? (Seeking explanation) Bob: I’m going to class. (Explanatory) Example 2 アリス：今、授業があるんじゃない？ Alice: Isn’t it that there is class now? ボブ：今は、ないんだ。 Bob: Now it is that there is no class. Alice: Don’t you have class now? (Expecting that there is class) Bob: No, there is no class now. (Explanatory) Example 3 アリス：今、授業がないんじゃない？ Alice: Isn’t it that there isn’t class now? ボブ：ううん、ある。 Bob: No, there is. Alice: Don’t you not have class now? (Expecting that there is no class) Bob: No, I do have class. Example 4 アリス：その人が買うんじゃなかったの？ Alice: Wasn’t it that that person was the one to buy? ボブ：ううん、先生が買うんだ。 Bob: No, it is that teacher is the one to buy. Alice: Wasn’t that person going to buy? (Expecting that the person would buy) Bob: No, the teacher is going to. (Explanatory) Example 5 アリス：朝ご飯を食べるんじゃなかった。 Alice: It is that breakfast wasn’t to eat. ボブ：どうして？ Bob: Why? Alice: Should not have eaten breakfast, you know. (Explaining that breakfast wasn’t to be eaten) Bob: How come? Don’t worry if you are thoroughly confused by now, we will see many more examples along the way. Once you get the sense of how everything works, it’s better to forget the English because the double and triple negatives can get quite confusing such as Example 3. However, in Japanese it is a perfectly normal expression, as you will begin to realize once you get accustomed to Japanese.',
      grammars: [],
      keywords: ['explanatory no','no','particle','sentence-ending particle'],
    ),
  ],
);
