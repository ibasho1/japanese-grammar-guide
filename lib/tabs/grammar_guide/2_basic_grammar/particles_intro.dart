import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/exercises/particle_exercise_wa.dart';
import 'package:japanese_grammar_guide/exercises/particle_exercise_wa_mo.dart';
import 'package:japanese_grammar_guide/exercises/particle_exercise_wa_mo_ga.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson particlesIntro = Lesson(
  title: 'Introduction to Particles',
  sections: [
    Section(
      heading: 'Defining grammatical functions with particles',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We want to now make good use of what we learned in the last lesson by associating a noun with another noun. This is done with something called particles. Particles are one or more Hiragana characters that attach to the end of a word to define the grammatical function of that word in the sentence. Using the correct particles is very important because the meaning of a sentence can completely change just by changing the particles. For example, the sentence “Eat fish.” can become “The fish eats.” simply by changing one particle.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We want to now make good use of what we learned in the last lesson by associating a noun with another noun. This is done with something called particles. Particles are one or more Hiragana characters that attach to the end of a word to define the grammatical function of that word in the sentence. Using the correct particles is very important because the meaning of a sentence can completely change just by changing the particles. For example, the sentence “Eat fish.” can become “The fish eats.” simply by changing one particle.',
      grammars: [],
      keywords: ['particle','intro'],
    ),
    Section(
      heading: 'The 「は」 topic particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const YouTubeLink(title: 'Learn Japanese from Scratch 2.1.2 - Topic Particle', uri: 'https://youtu.be/uZb5IOXBByQ'),
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'うん – yes (casual)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '明日 【あした】 – tomorrow',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ううん – no (casual)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '今日 【きょう】 – today',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '試験 【しけん】 – exam',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The first particle we will learn is the topic particle. The topic particle identifies what it is that you’re talking about, essentially the topic of your sentence. Let’s say a person says, “Not student.” This is a perfectly valid sentence in Japanese but it doesn’t tell us much without knowing what the person is talking about. The topic particle will allow us to express what our sentences are about. The topic particle is the character 「は」. Now, while this character is normally pronounced as /ha/, it is pronounced /wa/ only when it is being used as the topic particle.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'アリス',
                          ),
                          TextSpan(
                            text: 'は',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Is Alice (you) student?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Yeah, (I) am.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here, Bob is indicating that his question is about Alice. Notice that once the topic is established, Alice does not have to repeat the topic to answer the question about herself.'),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ジョン',
                          ),
                          TextSpan(
                            text: 'は',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: ashita,
                            inlineSpan: const TextSpan(
                              text: '明日',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('John is tomorrow?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: uun,
                            inlineSpan: const TextSpan(
                              text: 'ううん',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: ashita,
                            inlineSpan: const TextSpan(
                              text: '明日',
                            ),
                          ),
                          const TextSpan(
                            text: 'じゃない。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('No, not tomorrow.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Since we have no context, we don’t have enough information to make any sense of this conversation. It obviously makes no sense for John to actually '),
                  TextSpan(
                    text: 'be',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' tomorrow. Given a context, as long as the sentence has something to do with John and tomorrow, it can mean anything. For instance, they could be talking about when John is taking an exam.'),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kyou,
                            inlineSpan: const TextSpan(
                              text: '今日',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: shiken,
                            inlineSpan: const TextSpan(
                              text: '試験',
                            ),
                          ),
                          const TextSpan(
                            text: 'だ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Today is exam.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ジョンは？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('What about John?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ジョンは',
                          ),
                          VocabTooltip(
                            message: ashita,
                            inlineSpan: const TextSpan(
                              text: '明日',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'John is tomorrow. (As for John, the exam is tomorrow.)'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The last example shows how generic the topic of a sentence is. A topic can be referring to any action or object from anywhere even including other sentences. For example, in the last sentence from the previous example, even though the sentence is about when the exam is for John, the word “exam” doesn’t appear anywhere in the sentence!'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We’ll see a more specific particle that ties more closely into the sentence at the end of this lesson with the identifier particle.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学生 【がく・せい】 – student うん – yes (casual) 明日 【あした】 – tomorrow ううん – no (casual) 今日 【きょう】 – today 試験 【しけん】 – exam The first particle we will learn is the topic particle. The topic particle identifies what it is that you’re talking about, essentially the topic of your sentence. Let’s say a person says, “Not student.” This is a perfectly valid sentence in Japanese but it doesn’t tell us much without knowing what the person is talking about. The topic particle will allow us to express what our sentences are about. The topic particle is the character 「は」. Now, while this character is normally pronounced as /ha/, it is pronounced /wa/ only when it is being used as the topic particle. Example 1 ボブ：アリスは学生？ Bob: Is Alice (you) student? アリス：うん、学生。 Alice: Yeah, (I) am. Here, Bob is indicating that his question is about Alice. Notice that once the topic is established, Alice does not have to repeat the topic to answer the question about herself. Example 2 ボブ：ジョンは明日？ Bob: John is tomorrow? アリス：ううん、明日じゃない。 Alice: No, not tomorrow. Since we have no context, we don’t have enough information to make any sense of this conversation. It obviously makes no sense for John to actually be tomorrow. Given a context, as long as the sentence has something to do with John and tomorrow, it can mean anything. For instance, they could be talking about when John is taking an exam. Example 3 アリス：今日は試験だ。 Alice: Today is exam. ボブ：ジョンは？ Bob: What about John? アリス：ジョンは明日。 Alice: John is tomorrow. (As for John, the exam is tomorrow.) The last example shows how generic the topic of a sentence is. A topic can be referring to any action or object from anywhere even including other sentences. For example, in the last sentence from the previous example, even though the sentence is about when the exam is for John, the word “exam” doesn’t appear anywhere in the sentence! We’ll see a more specific particle that ties more closely into the sentence at the end of this lesson with the identifier particle.',
      grammars: ['は'],
      keywords: ['wa','ha','particle'],
    ),
    Section(
      heading: 'The 「も」 inclusive topic particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'うん – yes (casual)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'でも – but',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ううん – no (casual)',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Another particle that is very similar to the topic particle is the inclusive topic particle. It is essentially the topic particle with the additional meaning of “also”. Basically, it can introduce another topic in addition to the current topic. The inclusive topic particle is the 「も」 character and its use is best explained by an example.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'アリスは',
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Is Alice (you) student?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '、トム',
                          ),
                          TextSpan(
                            text: 'も',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Alice'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(text: 'Yeah, and Tom is '),
                          TextSpan(
                            text: 'also',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(text: ' student.'),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The inclusion of 「も」 must be consistent with the answer. It would not make sense to say, “I am a student, and Tom is also not a student.” Instead, use the 「は」 particle to make a break from the inclusion as seen in the next example.'),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'アリスは',
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Is Alice (you) student?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '、でもトム',
                          ),
                          TextSpan(
                            text: 'は',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: 'じゃない。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Yeah, but Tom is not student.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Below is an example of inclusion with the negative.'),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'アリスは',
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Is Alice (you) student?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: uun,
                            inlineSpan: const TextSpan(
                              text: 'ううん',
                            ),
                          ),
                          const TextSpan(
                            text: '、トム',
                          ),
                          TextSpan(
                            text: 'も',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: 'じゃない。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('No, and Tom is also not student.'),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学生 【がく・せい】 – student うん – yes (casual) でも – but ううん – no (casual) Another particle that is very similar to the topic particle is the inclusive topic particle. It is essentially the topic particle with the additional meaning of “also”. Basically, it can introduce another topic in addition to the current topic. The inclusive topic particle is the 「も」 character and its use is best explained by an example. Example 1 ボブ：アリスは学生? Bob: Is Alice (you) student? アリス：うん、トムも学生。 Alice: Yeah, and Tom is also student. The inclusion of 「も」 must be consistent with the answer. It would not make sense to say, “I am a student, and Tom is also not a student.” Instead, use the 「は」 particle to make a break from the inclusion as seen in the next example. Example 2 ボブ：アリスは学生? Bob: Is Alice (you) student? アリス：うん、でもトムは学生じゃない。 Alice: Yeah, but Tom is not student. Below is an example of inclusion with the negative. Example 3 ボブ：アリスは学生? Bob: Is Alice (you) student? アリス：ううん、トムも学生じゃない。 Alice: No, and Tom is also not student.',
      grammars: ['も'],
      keywords: ['mo','particles'],
    ),
    Section(
      heading: 'The 「が」 identifier particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '誰 【だれ】 – who',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '私 【わたし】 – me; myself; I',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Ok, so we can make a topic using the 「は」 and 「も」 particle. But what if we don’t know what the topic is? What if I wanted to ask, “Who is the student?” What I need is some kind of identifier because I don’t know who the student is. If I use the topic particle, the question would become, “Is who the student?” and that doesn’t make any sense because “who” is not an actual person.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is where the 「が」 particle comes into play. It is also referred to as the subject particle but I hate that name since “subject” means something completely different in English grammar. Instead, I call it the ',
                  ),
                  TextSpan(
                    text: 'identifier particle',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' because the particle indicates that the speaker wants to identify something unspecified.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: dare,
                            inlineSpan: const TextSpan(
                              text: '誰',
                            ),
                          ),
                          TextSpan(
                            text: 'が',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Who is the one that is student?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'ジョン',
                          ),
                          TextSpan(
                            text: 'が',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: gakusei,
                            inlineSpan: const TextSpan(
                              text: '学生',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('John is the one who is student.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Bob wants to identify who among all the possible candidates is a student. Alice responds that John is the one. Notice, Alice could also have answered with the topic particle to indicate that, speaking of John, she knows that he is a student (maybe not the student). You can see the difference in the next example.',
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: '?',
                            ),
                          ],
                        ),
                      ),
                      const Text('Who is the one that is student?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text('(The) student is who?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The first sentence seeks to identify a specific person for “student” while the second sentence is simply talking about the student. You cannot replace 「が」 with 「は」 in the first sentence because “who” would become the topic and the question would become, “Is who a student?”',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The two particles 「は」 and 「が」 may seem very similar only because it is impossible to translate them directly into English. For example, the two sentences below have the same English translation.*',
                  ),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I (am) student.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I (am) student.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'However, they only seem similar because English cannot express information about the context as succinctly as Japanese sometimes can. In the first sentence, since 「私」 is the topic, the sentence means, “Speaking about me, I am a student”.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'However, the second sentence is specifying who the 「学生」 is. If we want to know who the student is, the 「が」 particle tells us it’s 「私」. You can also think about the 「が」 particle as always answering a silent question. The second sentence might be answering a question, “Who is the student?” I often translate the topic particle as “as for; about” and the identifier particle as “the one; the thing” to illustrate the difference.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'As for',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: ' me, (I am) student.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'I (am) '),
                            TextSpan(
                              text: 'the one',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: ' (that is) student.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The 「は」 and 「が」 particles are actually quite different if you think of it the right way. The 「が」 particle identifies a specific property of something while the 「は」 particle is used only to bring up a new topic of conversation. This is why, in longer sentences, it is common to separate the topic with commas to remove ambiguity about which part of the sentence the topic applies to.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '*Well technically, it’s the most likely translation given the lack of context.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '*Note: The order of topics covered are different in the videos so you may want to read about Adjectives first.',
                  ),
                ],
              ),
              const YouTubeLink(title: 'Learn Japanese from Scratch 2.2.2 - が particle', uri: 'https://youtu.be/fQKy44eVYT4'),
            ],
          );
        },
      ),
      plaintext: '誰 【だれ】 – who 学生 【がく・せい】 – student 私 【わたし】 – me; myself; I Ok, so we can make a topic using the 「は」 and 「も」 particle. But what if we don’t know what the topic is? What if I wanted to ask, “Who is the student?” What I need is some kind of identifier because I don’t know who the student is. If I use the topic particle, the question would become, “Is who the student?” and that doesn’t make any sense because “who” is not an actual person. This is where the 「が」 particle comes into play. It is also referred to as the subject particle but I hate that name since “subject” means something completely different in English grammar. Instead, I call it the identifier particle because the particle indicates that the speaker wants to identify something unspecified. Example 1 ボブ：誰が学生? Bob: Who is the one that is student? アリス：ジョンが学生。 Alice: John is the one who is student. Bob wants to identify who among all the possible candidates is a student. Alice responds that John is the one. Notice, Alice could also have answered with the topic particle to indicate that, speaking of John, she knows that he is a student (maybe not the student). You can see the difference in the next example. Example 2 誰が学生? Who is the one that is student? 学生は誰？(The) student is who? The first sentence seeks to identify a specific person for “student” while the second sentence is simply talking about the student. You cannot replace 「が」 with 「は」 in the first sentence because “who” would become the topic and the question would become, “Is who a student?”The two particles 「は」 and 「が」 may seem very similar only because it is impossible to translate them directly into English. For example, the two sentences below have the same English translation.* Example 3 私は学生。 I (am) student. 私が学生。 I (am) student. However, they only seem similar because English cannot express information about the context as succinctly as Japanese sometimes can. In the first sentence, since 「私」 is the topic, the sentence means, “Speaking about me, I am a student”. However, the second sentence is specifying who the 「学生」 is. If we want to know who the student is, the 「が」 particle tells us it’s 「私」. You can also think about the 「が」 particle as always answering a silent question. The second sentence might be answering a question, “Who is the student?” I often translate the topic particle as “as for; about” and the identifier particle as “the one; the thing” to illustrate the difference. 私は学生。 As for me, (I am) student. 私が学生。 I (am) the one (that is) student. The 「は」 and 「が」 particles are actually quite different if you think of it the right way. The 「が」 particle identifies a specific property of something while the 「は」 particle is used only to bring up a new topic of conversation. This is why, in longer sentences, it is common to separate the topic with commas to remove ambiguity about which part of the sentence the topic applies to.*Well technically, it’s the most likely translation given the lack of context.*Note: The order of topics covered are different in the videos so you may want to read about Adjectives first.',
      grammars: ['が'],
      keywords: ['ga','particles'],
    ),
    Section(
      heading: 'Particle practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                              'To start with, I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                          NumberedList(
                            items: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E4%BD%95%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '何 – what'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%98%A0%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '映 – projection'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E7%94%BB%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '画 – picture'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%BD%BC%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '彼 – he'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%A5%B3%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '女 – female'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%98%A8%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '昨 – previous'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%97%A5%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '日 – day'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E4%BB%8A%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '今 – now'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%98%8E%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '明 – bright'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E7%9F%A5%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '知 – know'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%90%88%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '合 – match'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%89%9B%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '鉛 – lead'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E7%AD%86%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '筆 – brush'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%85%A5%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '入 – enter'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%8F%A3%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '口 – mouth'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%87%BA%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '出 – exit'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%9B%B3%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '図 – plan'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%9B%B8%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '書 – write'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%A4%A8%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '館 – building'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%B0%B4%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '水 – water'),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Kanji'),
              const NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              'Here is the list of some simple nouns that might be used in the exercises.'),
                          NumberedList(
                            items: [
                              Text(
                                'どこ – where',
                              ),
                              Text(
                                'いつ – when',
                              ),
                              Text(
                                'どうして – why',
                              ),
                              Text(
                                'どう – how',
                              ),
                              Text(
                                'どれ – which',
                              ),
                              Text(
                                'ミーティング – meeting',
                              ),
                              Text(
                                'ボールペン – ball-point pen',
                              ),
                              Text(
                                '何【なに】 – what',
                              ),
                              Text(
                                '誰【だれ】 – who',
                              ),
                              Text(
                                '映画【えいが】 – movie',
                              ),
                              Text(
                                '彼【かれ】 – he; boyfriend',
                              ),
                              Text(
                                '彼女【かのじょ】 – she; girlfriend',
                              ),
                              Text(
                                '雨【あめ】 – rain',
                              ),
                              Text(
                                '水【みず】 – water',
                              ),
                              Text(
                                '昨日【きのう】 – yesterday',
                              ),
                              Text(
                                '今日【きょう】 – today',
                              ),
                              Text(
                                '明日【あした】 – tomorrow',
                              ),
                              Text(
                                '知り合い【しりあい】 – acquaintance',
                              ),
                              Text(
                                '鉛筆【えんぴつ】 – pencil',
                              ),
                              Text(
                                '仕事【しごと】 – work',
                              ),
                              Text(
                                '入口【いりぐち】 – entrance',
                              ),
                              Text(
                                '出口【でぐち】 – exit',
                              ),
                              Text(
                                '図書館【としょかん】 – library',
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Vocabulary'),
              const Heading(text: 'Basic Particle Exercise with 「は」', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Let’s first get used to the basic concept of particles by making some very simple sentences with them. In this first exercise, we are going to use the topic particle to explain the current topic of conversation. Remember, the topic particle 「は」 is always pronounced as /wa/.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: '  誰？ （Topic: アリス） ＝ アリスは誰？',
                  ),
                ],
              ),
              const ParticleExerciseWa(),
              const Heading(text: 'Particle Exercise with 「は」 and 「も」', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now we are going to practice getting used to the differences between the 「は」 and 「も」 particles. The sentences are actually pretty lame but this was the only way I could think of to make obvious which particle should be used. Remember, the point is to get a sense of when and when not to use the inclusive particle instead of the topic particle.',
                  ),
                ],
              ),
              const Heading(
                  text:
                      'Fill in the blank with the correct particle, either 「は」 or 「も」',
                  level: 3),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: 'これは鉛筆だ。それ',
                  ),
                  TextSpan(
                    text: 'も',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: '鉛筆だ。',
                  ),
                ],
              ),
              const ParticleExerciseWaMo(),
              const Heading(text: 'Particle Exercise with 「は」, 「も」, 「が」', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In this last exercise, we will practice all three particles by identifying which one should be used for different types of situations. Remember that the 「が」 particle is only used when you want to identify something out of many other possibilities. While there are some cases where both 「は」 and 「が」 makes sense grammatically, because they mean different things, the correct one all depends on what you want to say.',
                  ),
                ],
              ),
              const Heading(
                  text:
                      'Fill in the blank with the correct particle, either 「は」、 「も」、 or 「が」',
                  level: 3),
              const ParticleExerciseWaMoGa(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section Kanji To start with, I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 何 – what 映 – projection 画 – picture 彼 – he 女 – female 昨 – previous 日 – day 今 – now 明 – bright 知 – know 合 – match 鉛 – lead 筆 – brush 入 – enter 口 – mouth 出 – exit 図 – plan 書 – write 館 – building 水 – water Vocabulary Here is the list of some simple nouns that might be used in the exercises. どこ – where いつ – when どうして – why どう – how どれ – which ミーティング – meeting ボールペン – ball-point pen 何【なに】 – what 誰【だれ】 – who 映画【えいが】 – movie 彼【かれ】 – he; boyfriend 彼女【かのじょ】 – she; girlfriend 雨【あめ】 – rain 水【みず】 – water 昨日【きのう】 – yesterday 今日【きょう】 – today 明日【あした】 – tomorrow 知り合い【しりあい】 – acquaintance 鉛筆【えんぴつ】 – pencil 仕事【しごと】 – work 入口【いりぐち】 – entrance 出口【でぐち】 – exit 図書館【としょかん】 – library Basic Particle Exercise with 「は」 Let’s first get used to the basic concept of particles by making some very simple sentences with them. In this first exercise, we are going to use the topic particle to explain the current topic of conversation. Remember, the topic particle 「は」 is always pronounced as /wa/. どこ？＝	学校はどこ？ (Topic: 学校) (Where is school?) どうして？＝	それはどうして？ (Topic: それ) (Why is that?) いつ？＝	ミーティングはいつ？ (Topic: ミーティング) (When is meeting?) 何？＝	これは何？ (Topic: これ) (What is this?) どう？＝	映画はどう？ (Topic: 映画) (How is movie?) 中学生だ。＝	彼は中学生だ。 (Topic: 彼)	(He is middle school student.) 先生だ。＝	彼女は先生だ。 (Topic: 彼女)(She is teacher.) 雨。＝	今日は雨。 (Topic: 今日) (Today is rain.) 友達。＝	ボブは友達。 (Topic: ボブ) (Bob is friend.) 知り合い？＝	彼は知り合い？ (Topic: 彼) (Is he an acquaintance?) Particle Exercise with 「は」 and 「も」 Now we are going to practice getting used to the differences between the 「は」 and 「も」 particles. The sentences are actually pretty lame but this was the only way I could think of to make obvious which particle should be used. Remember, the point is to get a sense of when and when not to use the inclusive particle instead of the topic particle. Fill in the blank with the correct particle, either 「は」 or 「も」今日は雨だ。 昨日　も　雨だった。 ジムは大学生だ。 でも、私　は　大学生じゃない。 これは水。 これ　も　そう。 これはボールペンだ。 でも、それ　は　ボールペンじゃない。 仕事は明日。 今日　は　仕事じゃなかった。 ここは入口。 出口　も　ここだ。 Particle Exercise with 「は」, 「も」, 「が」 In this last exercise, we will practice all three particles by identifying which one should be used for different types of situations. Remember that the 「が」 particle is only used when you want to identify something out of many other possibilities. While there are some cases where both 「は」 and 「が」 makes sense grammatically, because they mean different things, the correct one all depends on what you want to say. Fill in the blank with the correct particle, either 「は」、 「も」、 or 「が」ジム：アリス　は　誰？ ボブ：友達だ。 彼女　が　アリスだ。 アリス：これ　は　何？ ボブ：それ　は　鉛筆。 アリス：あれ　も　鉛筆？ ボブ：あれ　は　ペンだ。 アリス：図書館　は　どこ？ ボブ：ここ　が　図書館だ。 アリス：そこ　は　図書館じゃない？ ボブ：そこじゃない。 図書館　は　ここだ。',
      grammars: [],
      keywords: ['exercise'],
    ),
  ],
);
