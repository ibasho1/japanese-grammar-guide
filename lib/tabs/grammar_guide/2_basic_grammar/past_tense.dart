import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/exercises/past_negative_verb_conjugation_exercise.dart';
import 'package:japanese_grammar_guide/exercises/past_verb_conjugation_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson pastTense = Lesson(
  title: 'Past Tense',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We will finish defining all the basic properties of verbs by learning how to express the past and past-negative tense of actions. I will warn you in advance that the conjugation rules in this section will be the most complex rules you will learn in all of Japanese. On the one hand, once you have this section nailed, all other rules of conjugation will seem simple. On the other hand, you might need to refer back to this section many times before you finally get all the rules. You will probably need a great deal of practice until you can become familiar with all the different conjugations.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We will finish defining all the basic properties of verbs by learning how to express the past and past-negative tense of actions. I will warn you in advance that the conjugation rules in this section will be the most complex rules you will learn in all of Japanese. On the one hand, once you have this section nailed, all other rules of conjugation will seem simple. On the other hand, you might need to refer back to this section many times before you finally get all the rules. You will probably need a great deal of practice until you can become familiar with all the different conjugations.',
      grammars: [],
      keywords: ['past','verbs'],
    ),
    Section(
      heading: 'Past tense for ru-verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('ご飯 【ご・はん】 – rice; meal'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('映画 【えい・が】 – movie'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We will start off with the easy ru-verb category. To change a ru-verb from the dictionary form into the past tense, you simply drop the 「る」 and add 「た」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'To change ru-verbs into the past tense',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    'Drop the 「る」 part of the ru-verb and add 「た」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: deru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '出',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: deru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '出',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '捨て',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '捨て',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gohan,
                              inlineSpan: const TextSpan(
                                text: 'ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                  text: '食べた',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for meal, ate.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                  text: '見た',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'As for movie, saw them all.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 出る 【で・る】 (ru-verb) – to come out 捨てる 【す・てる】 (ru-verb) – to throw away ご飯 【ご・はん】 – rice; meal 食べる 【た・べる】 (ru-verb) – to eat 映画 【えい・が】 – movie 全部 【ぜん・ぶ】 – everything 見る 【み・る】 (ru-verb) – to see We will start off with the easy ru-verb category. To change a ru-verb from the dictionary form into the past tense, you simply drop the 「る」 and add 「た」. To change ru-verbs into the past tense Drop the 「る」 part of the ru-verb and add 「た」. Examples: 出る → 出た捨てる → 捨てた Examples ご飯は、食べた。 As for meal, ate. 映画は、全部見た。 As for movie, saw them all.',
      grammars: ['～た'],
      keywords: ['past','past tense','ru-verbs','ichidan verbs'],
    ),
    Section(
      heading: 'Past tense for u-verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('切る 【き・る】 (u-verb) – to cut'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('持つ 【も・つ】 (u-verb) – to hold'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('今日 【きょう】 – today'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('勉強 【べん・きょう】 – study'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Changing a u-verb from dictionary form to the past tense is difficult because we must break up u-verbs into four additional categories. These four categories depend on the last character of the verb. The table below illustrates the different sub-categories. In addition, there is one exception to the rules, which is the verb 「'),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. I’ve bundled it with the regular exception verbs 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来る',
                    ),
                  ),
                  const TextSpan(text: '」 even though 「'),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                      text: '」 is a regular u-verb in all other conjugations.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'Past tense conjugations for u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'Ending',
                            ),
                            TableHeading(
                              text: 'Non-Past',
                            ),
                            TableHeading(
                              text: 'changes to…',
                            ),
                            TableHeading(
                              text: 'Past',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              centered: false,
                              content: Text('す'),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hanasu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '話',
                                          ),
                                          TextSpan(
                                            text: 'す',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Text('す→した'),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hanasu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '話',
                                          ),
                                          TextSpan(
                                            text: 'した',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('く'),
                                  Text('ぐ'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kaku,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '書',
                                              ),
                                              TextSpan(
                                                text: 'く',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: oyogu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '泳',
                                              ),
                                              TextSpan(
                                                text: 'く',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('く→いた'),
                                  Text('ぐ→いだ'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kaku,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '書',
                                              ),
                                              TextSpan(
                                                text: 'いた',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kaku,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '泳',
                                              ),
                                              TextSpan(
                                                text: 'いだ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('む'),
                                  Text('ぶ'),
                                  Text('ぬ'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: nomu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '飲',
                                              ),
                                              TextSpan(
                                                text: 'む',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '遊',
                                              ),
                                              TextSpan(
                                                text: 'ぶ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shinu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '死',
                                              ),
                                              TextSpan(
                                                text: 'ぬ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('む→んだ'),
                                  Text('ぶ→んだ'),
                                  Text('ぬ→んだ'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: nomu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '飲',
                                              ),
                                              TextSpan(
                                                text: 'んだ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '遊',
                                              ),
                                              TextSpan(
                                                text: 'んだ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shinu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '死',
                                              ),
                                              TextSpan(
                                                text: 'んだ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('る'),
                                  Text('う'),
                                  Text('つ'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kiru,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '切',
                                              ),
                                              TextSpan(
                                                text: 'る',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kau,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '買',
                                              ),
                                              TextSpan(
                                                text: 'う',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: motsu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '持',
                                              ),
                                              TextSpan(
                                                text: 'つ',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('る→った'),
                                  Text('う→った'),
                                  Text('つ→った'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kiru,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '切',
                                              ),
                                              TextSpan(
                                                text: 'った',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kau,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '買',
                                              ),
                                              TextSpan(
                                                text: 'った',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: motsu,
                                          inlineSpan: TextSpan(
                                            children: [
                                              const TextSpan(
                                                text: '持',
                                              ),
                                              TextSpan(
                                                text: 'った',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exceptions'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'Non-Past',
                            ),
                            TableHeading(
                              text: '	Past',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: const TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'する',
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'し',
                                          ),
                                          TextSpan(
                                            text: 'た',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kuru,
                                      inlineSpan: const TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'くる',
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kuru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'き',
                                          ),
                                          TextSpan(
                                            text: 'た',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: const TextSpan(
                                        children: [
                                          TextSpan(
                                            text: '行く',
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '行',
                                          ),
                                          TextSpan(
                                            text: 'った',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const TextSpan(
                                      text: '*',
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: '* exceptions particular to this conjugation',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: hashiru,
                              inlineSpan: TextSpan(
                                text: '走った',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for today, ran.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                  text: '来た',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Friend is the one that came.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: TextSpan(
                                  text: '遊んだ',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I also played.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                  text: 'した',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('About study, did it.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 話す 【はな・す】 (u-verb) – to speak 書く 【か・く】 (u-verb) – to write 泳ぐ 【およ・ぐ】 (u-verb) – to swim 飲む 【の・む】 (u-verb) – to drink 遊ぶ 【あそ・ぶ】 (u-verb) – to play 死ぬ 【し・ぬ】 (u-verb) – to die 切る 【き・る】 (u-verb) – to cut 買う 【か・う】 (u-verb) – to buy 持つ 【も・つ】 (u-verb) – to hold する (exception) – to do 来る 【く・る】 (exception) – to come 行く 【い・く】 (u-verb) – to go 今日 【きょう】 – today 走る 【はし・る】 (u-verb) – to run 友達 【とも・だち】 – friend 私 【わたし】 – me, myself, I 勉強 【べん・きょう】 – study Changing a u-verb from dictionary form to the past tense is difficult because we must break up u-verbs into four additional categories. These four categories depend on the last character of the verb. The table below illustrates the different sub-categories. In addition, there is one exception to the rules, which is the verb 「行く」. I’ve bundled it with the regular exception verbs 「する」 and 「来る」 even though 「行く」 is a regular u-verb in all other conjugations. Examples 今日は、走った。 As for today, ran. 友達が来た。 Friend is the one that came. 私も遊んだ。 I also played. 勉強は、した。 About study, did it.',
      grammars: [],
      keywords: ['past','past tense','u-verbs','godan verbs'],
    ),
    Section(
      heading: 'Past-negative tense for all verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('する (exception) – to do'),
                  Text('お金 【お・かね】 – money'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('猫　【ねこ】 – cat'),
                  Text('いる (ru-verb) – to exist (animate)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The conjugation rules for the past-negative tense are the same for all verbs. You might have noticed that the negative of just about everything always end in 「ない」. The conjugation rule for the past-negative tense of verbs is pretty much the same as all the other negatives that end in 「ない」. You simply take the negative of any verb, remove the 「い」 from the 「ない」 ending, and replace it with 「かった」.'),
                ],
              ),
              NotesSection(
                heading: 'To change verbs into the past-negative tense',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Change the verb to the negative and replace the 「い」 with 「かった」.',
                        ),
                        const Text(
                          'Examples: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '捨て',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '捨てな',
                                        ),
                                        TextSpan(
                                          text: 'い ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '捨てな',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行かな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suteru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行かな',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスは',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '食べな',
                                  ),
                                  TextSpan(
                                    text: 'かった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for Alice, did not eat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ジムが',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'しな',
                                  ),
                                  TextSpan(
                                    text: 'かった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Jim is the one that did not do.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブも',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '行かな',
                                  ),
                                  TextSpan(
                                    text: 'かった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob also did not go.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                  text: 'なかった',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There was no money. (lit: As for money, did not exist.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                  text: '買わなかった',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for me, did not buy.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: TextSpan(
                                  text: 'いなかった',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There was no cat. (lit: As for cat, did not exist.)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 捨てる 【す・てる】 (ru-verb) – to throw away 行く 【い・く】 (u-verb) – to go 食べる 【たべ・る】 (ru-verb) – to eat する (exception) – to do お金 【お・かね】 – money ある (u-verb) – to exist (inanimate) 私 【わたし】 – me, myself, I 買う 【か・う】 (u-verb) – to buy 猫　【ねこ】 – cat いる (ru-verb) – to exist (animate) The conjugation rules for the past-negative tense are the same for all verbs. You might have noticed that the negative of just about everything always end in 「ない」. The conjugation rule for the past-negative tense of verbs is pretty much the same as all the other negatives that end in 「ない」. You simply take the negative of any verb, remove the 「い」 from the 「ない」 ending, and replace it with 「かった」. To change verbs into the past-negative tense Change the verb to the negative and replace the 「い」 with 「かった」. Examples: 捨てる → 捨てない  → 捨てなかった行く → 行かない → 行かなかった Examples アリスは食べなかった。 As for Alice, did not eat. ジムがしなかった。 Jim is the one that did not do. ボブも行かなかった。 Bob also did not go. お金がなかった。 There was no money. (lit: As for money, did not exist.) 私は買わなかった。 As for me, did not buy. 猫はいなかった。 There was no cat. (lit: As for cat, did not exist.)',
      grammars: ['～なかった'],
      keywords: ['past-negative','past-negative tense'],
    ),
    Section(
      heading: 'Past verb practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is the same list of verbs from the previous practice exercise with a couple additions. We will use mostly the same verbs from the last exercise to practice conjugating to the past and the past negative tense.',
                  ),
                ],
              ),
              NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                              'I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                          NumberedList(
                            items: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%A9%B1%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '話 – story'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%A6%8B%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '見 – see'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%9D%A5%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '来 – come; next'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%A1%8C%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '行 – go; conduct'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%B8%B0%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '帰 – go home'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%A3%9F%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '食 – eat; food'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%A3%B2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '飲 – drink'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%B2%B7%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '買 – buy'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%A3%B2%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '売 – sell'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%8C%81%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '持 – hold'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E5%BE%85%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '待 – wait'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%AA%AD%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '読 – read'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%AD%A9%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '歩 – walk'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E8%B5%B0%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '走 – run'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E9%81%8A%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '遊 – play'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%B3%B3%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '泳 – swim'),
                                    ),
                                  ],
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  children: [
                                    ExternalLink(
                                      uri:
                                          'https://jisho.org/search/%E6%AD%BB%20%23kanji',
                                      inlineSpan: const TextSpan(
                                          text: '死 – death'),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Kanji'),
              const NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here is a list of some common verbs you will definitely want to learn at some point.'),
                        NumberedList(
                          items: [
                            Text(
                              'する – to do',
                            ),
                            Text(
                              'しゃべる – to talk; to chat',
                            ),
                            Text(
                              '見る【み・る】 – to see',
                            ),
                            Text(
                              '来る【く・る】 – to come',
                            ),
                            Text(
                              '行く【い・く】 – to go',
                            ),
                            Text(
                              '帰る 【かえ・る】 – to go home',
                            ),
                            Text(
                              '食べる 【たべ・る】 – to eat',
                            ),
                            Text(
                              '飲む 【の・む】 – to drink',
                            ),
                            Text(
                              '買う 【か・う】 – to buy',
                            ),
                            Text(
                              '売る 【う・る】 – to sell',
                            ),
                            Text(
                              '切る 【き・る】 – to cut',
                            ),
                            Text(
                              '入る 【はい・る】 – to enter',
                            ),
                            Text(
                              '出る 【で・る】 – to come out',
                            ),
                            Text(
                              '持つ 【も・つ】 – to hold',
                            ),
                            Text(
                              '待つ 【ま・つ】 – to wait',
                            ),
                            Text(
                              '書く【か・く】 – to write',
                            ),
                            Text(
                              '読む 【よ・む】 – to read',
                            ),
                            Text(
                              '歩く 【ある・く】 – to walk',
                            ),
                            Text(
                              '走る 【はし・る】 – to run',
                            ),
                            Text(
                              '遊ぶ 【あそ・ぶ】 – to play',
                            ),
                            Text(
                              '泳ぐ 【およ・ぐ】 – to swim',
                            ),
                            Text(
                              '死ぬ 【し・ぬ】 – to die',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Vocabulary',
              ),
              const Heading(text: 'Practice with Past Verb Conjugations', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We learned how to classify the following verbs in the first verb practice exercise. Now, we are going to put that knowledge to use by conjugating the same verbs into the past tense depending on which type of verb it is. The first answer has been given as an example.',
                  ),
                ],
              ),
              const PastVerbConjugationExercise(),
              const Heading(
                  text: 'Practice with Past Negative Verb Conjugations',
                  level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Now, we are going to do the same thing for the past negative verb conjugations.',
                  ),
                ],
              ),
              const PastNegativeVerbConjugationExercise(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section This is the same list of verbs from the previous practice exercise with a couple additions. We will use mostly the same verbs from the last exercise to practice conjugating to the past and the past negative tense. Kanji I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 話 – story 見 – see 来 – come; next 行 – go; conduct 帰 – go home 食 – eat; food 飲 – drink 買 – buy 売 – sell 持 – hold 待 – wait 読 – read 歩 – walk 走 – run 遊 – play 泳 – swim 死 – death Vocabulary Here is a list of some common verbs you will definitely want to learn at some point. する – to do しゃべる – to talk; to chat 見る【み・る】 – to see 来る【く・る】 – to come 行く【い・く】 – to go 帰る 【かえ・る】 – to go home 食べる 【たべ・る】 – to eat 飲む 【の・む】 – to drink 買う 【か・う】 – to buy 売る 【う・る】 – to sell 切る 【き・る】 – to cut 入る 【はい・る】 – to enter 出る 【で・る】 – to come out 持つ 【も・つ】 – to hold 待つ 【ま・つ】 – to wait 書く【か・く】 – to write 読む 【よ・む】 – to read 歩く 【ある・く】 – to walk 走る 【はし・る】 – to run 遊ぶ 【あそ・ぶ】 – to play 泳ぐ 【およ・ぐ】 – to swim 死ぬ 【し・ぬ】 – to die Practice with Past Verb Conjugations We learned how to classify the following verbs in the first verb practice exercise. Now, we are going to put that knowledge to use by conjugating the same verbs into the past tense depending on which type of verb it is. The first answer has been given as an example. Practice with Past Negative Verb Conjugations Now, we are going to do the same thing for the past negative verb conjugations.',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
