import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/exercises/basic_exercise.dart';
import 'package:japanese_grammar_guide/exercises/conjugation_exercise_1.dart';
import 'package:japanese_grammar_guide/exercises/question_answer_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson stateOfBeing = Lesson(
  title: 'Expressing State-of-Being',
  sections: [
    Section(
      heading: 'Declaring something is so and so using 「だ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '人 【ひと】 – person',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '元気 【げん・き】 – healthy; lively',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '＊Used as a greeting to indicate whether one is well',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'One of the trickiest part of Japanese is that there is no verb for the state-of-being like the verb “to be” in English. You can, however, declare what something is by attaching the Hiragana character 「だ」 to a noun or na-adjective ',
                  ),
                  TextSpan(
                    text: 'only',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text:
                        '. (We will learn about na-adjectives in the section on adjectives later.)',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Declaring that something is so using 「だ」',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「だ」 to the noun or na-adjective',
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hito,
                                    inlineSpan: const TextSpan(
                                      text: '人',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋だ＝',
                                  ),
                                  VocabTooltip(
                                    message: hito,
                                    inlineSpan: const TextSpan(
                                      text: '人',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: hito,
                                  inlineSpan: const TextSpan(
                                    text: '人',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is person.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: gakusei,
                                  inlineSpan: const TextSpan(
                                    text: '学生',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is student.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: genki,
                                  inlineSpan: const TextSpan(
                                    text: '元気',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is well.')
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Seems easy enough. Here’s the real kicker though.',
                  ),
                ],
              ),
              const Padding(
                padding: kDefaultParagraphPadding,
                child: Center(
                  child: Text(
                    'A state-of-being can be implied without using 「だ」!',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can say you’re doing well or someone is a student without using 「だ」 at all. For example, below is an example of a very typical greeting among friends. Also notice how the subject isn’t even specified when it’s obvious from the context.',
                  ),
                ],
              ),
              const Heading(text: 'Typical casual greeting', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: genki,
                            inlineSpan: const TextSpan(
                              text: '元気',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('(Are you) well?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: genki,
                            inlineSpan: const TextSpan(
                              text: '元気',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('(I’m) well.'),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'So you may be wondering, “What’s the point of using 「だ」?” Well, the main difference is that a declarative statement makes the sentence sound more emphatic and forceful in order to make it more… well declarative. Therefore, it is more common to hear men use 「だ」 at the end of sentences.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The declarative 「だ」 is also needed in various grammatical structures where a state-of-being must be explicitly declared. There are also times when you cannot attach it. It’s all quite a pain in the butt really but you don’t have to worry about it yet.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 人 【ひと】 – person 学生 【がく・せい】 – student 元気 【げん・き】 – healthy; lively＊Used as a greeting to indicate whether one is well One of the trickiest part of Japanese is that there is no verb for the state-of-being like the verb “to be” in English. You can, however, declare what something is by attaching the Hiragana character 「だ」 to a noun or na-adjective only. (We will learn about na-adjectives in the section on adjectives later.) Declaring that something is so using 「だ」 Attach 「だ」 to the noun or na-adjective Example: 人＋だ＝人だ Examples 人だ。 Is person. 学生だ。 Is student. 元気だ。 Is well. Seems easy enough. Here’s the real kicker though. A state-of-being can be implied without using 「だ」! You can say you’re doing well or someone is a student without using 「だ」 at all. For example, below is an example of a very typical greeting among friends. Also notice how the subject isn’t even specified when it’s obvious from the context. Typical casual greetingＡ：元気？ A: (Are you) well? Ｂ：元気。 B：(I’m) well. So you may be wondering, “What’s the point of using 「だ」?” Well, the main difference is that a declarative statement makes the sentence sound more emphatic and forceful in order to make it more… well declarative. Therefore, it is more common to hear men use 「だ」 at the end of sentences. The declarative 「だ」 is also needed in various grammatical structures where a state-of-being must be explicitly declared. There are also times when you cannot attach it. It’s all quite a pain in the butt really but you don’t have to worry about it yet.',
      grammars: ['だ'],
      keywords: ['state-of-being','expressing'],
    ),
    Section(
      heading: 'Conjugating to the negative state-of-being',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '友達 【とも・だち】 – friend',
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '元気 【げん・き】 – healthy; lively',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '＊Used as a greeting to indicate whether one is well',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In Japanese, negative and past tense are all expressed by conjugation. We can conjugate a noun or adjective to either its negative or past tense to say that something is not [X] or that something was [X]. This may be a bit hard to grasp at first but none of these state-of-being conjugations make anything declarative like 「だ」 does. We’ll learn how to make these tenses declarative by attaching 「だ」 to the end of the sentence in a later lesson.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'First, for the negative, attach 「じゃない」 to the noun or na-adjective.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Conjugation rules for the negative state-of-being',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「じゃない」 to the noun or na-adjective',
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋じゃない＝',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'じゃない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: gakusei,
                                  inlineSpan: const TextSpan(
                                    text: '学生',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is not student.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: tomodachi,
                                  inlineSpan: const TextSpan(
                                    text: '友達',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is not friend.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: genki,
                                  inlineSpan: const TextSpan(
                                    text: '元気',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Is not well.')
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学生 【がく・せい】 – student 友達 【とも・だち】 – friend 元気 【げん・き】 – healthy; lively ＊Used as a greeting to indicate whether one is well In Japanese, negative and past tense are all expressed by conjugation. We can conjugate a noun or adjective to either its negative or past tense to say that something is not [X] or that something was [X]. This may be a bit hard to grasp at first but none of these state-of-being conjugations make anything declarative like 「だ」 does. We’ll learn how to make these tenses declarative by attaching 「だ」 to the end of the sentence in a later lesson. First, for the negative, attach 「じゃない」 to the noun or na-adjective. Conjugation rules for the negative state-of-being Attach 「じゃない」 to the noun or na-adjective Example: 学生＋じゃない＝学生じゃない Examples 学生じゃない。 Is not student. 友達じゃない。 Is not friend. 元気じゃない。 Is not well.',
      grammars: ['じゃない'],
      keywords: ['negative','state-of-being'],
    ),
    Section(
      heading: 'Conjugating to the past state-of-being',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '学生 【がく・せい】 – student',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '友達 【とも・だち】 – friend',
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '元気 【げん・き】 – healthy; lively',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '＊Used as a greeting to indicate whether one is well',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We will now learn the past tense of the state-of-being. To say something was something, attach 「だった」 to the noun or na-adjective.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'In order to say the negative past (',
                  ),
                  TextSpan(
                      text: 'was not',
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  TextSpan(
                    text:
                        '), conjugate the negative to the negative past tense by dropping the 「い」 from 「じゃない」 and adding 「かった」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Conjugation rules for the past state-of-being',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Past state-of-being:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「だった」 to the noun or na-adjective.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: tomodachi,
                                    inlineSpan: const TextSpan(
                                      text: '友達',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋',
                                  ),
                                  TextSpan(
                                      text: 'だった',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary)),
                                  const TextSpan(
                                    text: ' ＝',
                                  ),
                                  VocabTooltip(
                                    message: tomodachi,
                                    inlineSpan: const TextSpan(
                                      text: '友達',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'だった',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Negative past state-of-being:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const TextSpan(
                                text:
                                    ' Conjugate the noun or na-adjective to the negative first and then replace the 「い」 of 「じゃな',
                              ),
                              TextSpan(
                                  text: 'い',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                              const TextSpan(
                                text: '」 with 「かった」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: tomodachi,
                                    inlineSpan: const TextSpan(
                                      text: '友達',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋',
                                  ),
                                  TextSpan(
                                      text: 'じゃな',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary)),
                                  TextSpan(
                                      text: 'い ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough,
                                      )),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tomodachi,
                                    inlineSpan: const TextSpan(
                                      text: '友達',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃな',
                                  ),
                                  TextSpan(
                                      text: 'かった',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary)),
                                  const TextSpan(
                                    text: '＝',
                                  ),
                                  VocabTooltip(
                                    message: tomodachi,
                                    inlineSpan: const TextSpan(
                                      text: '友達',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃなかった',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: gakusei,
                                  inlineSpan: const TextSpan(
                                    text: '学生',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'だった',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Was student.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: tomodachi,
                                  inlineSpan: const TextSpan(
                                    text: '友達',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'じゃなかった',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Was not friend.')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              children: [
                                VocabTooltip(
                                  message: genki,
                                  inlineSpan: const TextSpan(
                                    text: '元気',
                                  ),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: 'じゃなかった',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Was not well.')
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学生 【がく・せい】 – student 友達 【とも・だち】 – friend 元気 【げん・き】 – healthy; lively ＊Used as a greeting to indicate whether one is well We will now learn the past tense of the state-of-being. To say something was something, attach 「だった」 to the noun or na-adjective. In order to say the negative past (was not), conjugate the negative to the negative past tense by dropping the 「い」 from 「じゃない」 and adding 「かった」. Conjugation rules for the past state-of-being Past state-of-being: Attach 「だった」 to the noun or na-adjective. Example: 友達＋だった ＝ 友達だった Negative past state-of-being: Conjugate the noun or na-adjective to the negative first and then replace the 「い」 of 「じゃない」 with 「かった」. Example: 友達＋じゃない  → 友達じゃなかった ＝ 友達じゃなかった Examples 学生だった。 Was student. 友達じゃなかった。 Was not friend. 元気じゃなかった。 Was not well.',
      grammars: ['だった','じゃなかった'],
      keywords: ['past','past-tense','state-of-being'],
    ),
    Section(
      heading: 'Conjugation summary',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We’ve now learned how to express state-of-being in all four tenses. Next we will learn some particles, which will allow us assign roles to words. Here is a summary chart of the conjugations we learned in this section.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Summary of state-of-being'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Positive',
                            ),
                            TableHeading(
                              text: 'Negative',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Non-Past',
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: gakusei,
                                          inlineSpan: const TextSpan(
                                            text: '学生',
                                          ),
                                        ),
                                        const TextSpan(text: '（だ）'),
                                      ],
                                    ),
                                  ),
                                  const Text('Is student'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: gakusei,
                                          inlineSpan: const TextSpan(
                                            text: '学生',
                                          ),
                                        ),
                                        const TextSpan(text: 'じゃない'),
                                      ],
                                    ),
                                  ),
                                  const Text('Is not student'),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: gakusei,
                                          inlineSpan: const TextSpan(
                                            text: '学生',
                                          ),
                                        ),
                                        const TextSpan(text: 'だった'),
                                      ],
                                    ),
                                  ),
                                  const Text('Was student'),
                                ],
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: gakusei,
                                          inlineSpan: const TextSpan(
                                            text: '学生',
                                          ),
                                        ),
                                        const TextSpan(text: 'じゃなかった'),
                                      ],
                                    ),
                                  ),
                                  const Text('Was not student'),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'We’ve now learned how to express state-of-being in all four tenses. Next we will learn some particles, which will allow us assign roles to words. Here is a summary chart of the conjugations we learned in this section.',
      grammars: [],
      keywords: ['conjugation'],
    ),
    Section(
      heading: 'State-of-being practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In the following exercises, we will practice the state-of-being conjugations we just covered. But first, you might want to learn or review the following useful nouns that will be used in the exercises.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'To start with, I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E4%BA%BA%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '人 – person'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%AD%90%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '子 – child'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%B0%8F%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '小 – small'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E4%B8%AD20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '中 – middle'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%A4%A7%20%23kanji',
                                    inlineSpan: const TextSpan(text: '大 – big'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%8F%8B%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '友 – friend'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E7%94%9F%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '生 – life'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%85%88%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '先 – ahead'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%AD%A6%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '学 – study'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E6%A0%A1%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '校 – school'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E9%AB%98%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '高 – high'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%BB%8A%20%23kanji',
                                    inlineSpan: const TextSpan(text: '車 – car'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E4%BE%9B%20%23kanji',
                                    inlineSpan: const TextSpan(
                                        text: '供 – accompanying'),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E9%81%94%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '達 – reach'),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Kanji',
              ),
              const NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here is the list of some simple nouns that might be used in the exercises.'),
                        NumberedList(
                          items: [
                            Text(
                              'うん – casual word for “yes” (yeah, uh-huh)',
                            ),
                            Text(
                              'ううん – casual word for “no” (nah, uh-uh)',
                            ),
                            Text(
                              'これ – this',
                            ),
                            Text(
                              'それ – that',
                            ),
                            Text(
                              'あれ – that over there',
                            ),
                            Text(
                              'こう – (things are) this way',
                            ),
                            Text(
                              'そう – (things are) that way',
                            ),
                            Text(
                              '人 【ひと】 – person',
                            ),
                            Text(
                              '大人 【おとな】 – adult',
                            ),
                            Text(
                              '子供 【こども】 – child',
                            ),
                            Text(
                              '友達 【ともだち】 – friend',
                            ),
                            Text(
                              '車 【くるま】 – car',
                            ),
                            Text(
                              '学生 【がくせい】 – student',
                            ),
                            Text(
                              '先生 【せんせい】 – teacher',
                            ),
                            Text(
                              '学校 【がっこう】 – school',
                            ),
                            Text(
                              '小学校 【しょうがっこう】 – elementary school',
                            ),
                            Text(
                              '中学校 【ちゅうがっこう】 – middle school',
                            ),
                            Text(
                              '高校 【こうこう】 – high school',
                            ),
                            Text(
                              '大学 【だいがく】 – college',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Vocabulary',
              ),
              const Heading(text: 'Conjugation Exercise 1', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We are now going to practice the state-of-being conjugations in order. Take each noun and conjugate it to the following forms: the declarative, negative state-of-being, past state-of-being, and negative past state-of-being.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' 人 ＝ ',
                  ),
                  TextSpan(
                    text: '人だ',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: '、',
                  ),
                  TextSpan(
                    text: '人じゃない',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: '、',
                  ),
                  TextSpan(
                    text: '人だった',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: '、',
                  ),
                  TextSpan(
                    text: '人じゃなかった',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              const ConjugationExercise1(),
              const Heading(text: 'Conjugation Exercise 2', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In this second exercise, we are really going to test your conjugation knowledge as well as the vocabulary by translating some simple English sentences.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Please note that while the positive, non-past state-of-being can be implied, for the purpose of this exercise, we will assume it’s always declaratory. Don’t forget that this creates a very firm and declaratory tone.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: ' Is student. ＝ ',
                  ),
                  TextSpan(
                    text: '学生だ。',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              const BasicExercise(
                entries: {
                  'Is college.': '大学だ。',
                  'Is not high school.': '高校じゃない。',
                  'Was teacher.': '先生だった。',
                  'Is adult.': '大人だ。',
                  'Was not child.': '子供じゃなかった。',
                  'This was the way it was.': 'こうだった。',
                  'Wasn’t that over there.': 'あれじゃなかった。',
                  'Is not middle school.': '中学校じゃない。',
                  'Is friend.': '友達だ。',
                  'Was not car.': '車じゃなかった。',
                  'Was this.': 'これだった。',
                  'That’s not the way it is.': 'そうじゃない。',
                },
              ),
              const Heading(text: 'Question Answer Exercise', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In this last exercise, we’ll practice answering very simple questions using the state-of-being. The yes or no answer （うん or ううん） will be given and it is your job to complete the sentence. In deciding whether to use the declaratory 「だ」, I’ve decided to be sexist here and assume all males use the declaratory 「だ」 and all females use the implicit state-of-being (not the case in the real world).',
                  ),
                ],
              ),
              const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sample:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Ｑ）　学生？',
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Ａ）　ううん、',
                        ),
                        TextSpan(
                          text: '学生じゃない',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const QuestionAnswerExercise(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section In the following exercises, we will practice the state-of-being conjugations we just covered. But first, you might want to learn or review the following useful nouns that will be used in the exercises. Kanji To start with, I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 人 – person 子 – child 小 – small 中 – middle 大 – big 友 – friend 生 – life 先 – ahead 学 – study 校 – school 高 – high 車 – car 供 – accompanying 達 – reach Vocabulary Here is the list of some simple nouns that might be used in the exercises. うん – casual word for “yes” (yeah, uh-huh) ううん – casual word for “no” (nah, uh-uh) これ – this それ – that あれ – that over there こう – (things are) this way そう – (things are) that way 人 【ひと】 – person 大人 【おとな】 – adult 子供 【こども】 – child 友達 【ともだち】 – friend 車 【くるま】 – car 学生 【がくせい】 – student 先生 【せんせい】 – teacher 学校 【がっこう】 – school 小学校 【しょうがっこう】 – elementary school 中学校 【ちゅうがっこう】 – middle school 高校 【こうこう】 – high school 大学 【だいがく】 – college Conjugation Exercise 1 We are now going to practice the state-of-being conjugations in order. Take each noun and conjugate it to the following forms: the declarative, negative state-of-being, past state-of-being, and negative past state-of-being. これ declarative=これだ negative=これじゃない past=これだった negative-past=これじゃなかった 大人 declarative=大人だ negative=大人じゃない past=大人だった negative-past=大人じゃなかった 学校 declarative=学校だ negative=学校じゃない past=学校だった negative-past=学校じゃなかった 友達 declarative=友達だ negative=友達じゃない past=友達だった negative-past=友達じゃなかった 学生 declarative=学生だ negative=学生じゃない past=学生だった negative-past=学生じゃなかった Conjugation Exercise 2 In this second exercise, we are really going to test your conjugation knowledge as well as the vocabulary by translating some simple English sentences. Please note that while the positive, non-past state-of-being can be implied, for the purpose of this exercise, we will assume it’s always declaratory. Don’t forget that this creates a very firm and declaratory tone. Is college.=大学だ。 Is not high school.=高校じゃない。 Was teacher.=先生だった。 Is adult.=大人だ。 Was not child.=子供じゃなかった。 This was the way it was.=こうだった。 Wasn’t that over there.=あれじゃなかった。 Is not middle school.=中学校じゃない。 Is friend.=友達だ。 Was not car.=車じゃなかった。 Was this.=これだった。 That’s not the way it is.=そうじゃない。 Question Answer Exercise In this last exercise, we’ll practice answering very simple questions using the state-of-being. The yes or no answer （うん or ううん） will be given and it is your job to complete the sentence. In deciding whether to use the declaratory 「だ」, I’ve decided to be sexist here and assume all males use the declaratory 「だ」 and all females use the implicit state-of-being (not the case in the real world). Ｑ）　学生？ Ａ）　ううん、学生じゃない。 Ｑ１）　友達？ Ａ１）　うん、友達。 (female) Ｑ２）　学校？ Ａ２）　ううん、学校じゃない。 Ｑ３）　それだった？ Ａ３）　ううん、それじゃなかった。 Ｑ４）　そう？ (Is that so?) Ａ４）　うん、そうだ。 (male) Ｑ５）　これ？ Ａ５）　ううん、それじゃない。 (object is away from the speaker) Ｑ６）　先生だった？ Ａ６）　うん、先生だった。 Ｑ７）　小学校だった？ Ａ７）　ううん、小学校じゃなかった。 Ｑ８）　子供？ Ａ８）　うん、子供。 (female)',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);