import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson verbParticles = Lesson(
  title: 'Particles Used With Verbs',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this section, we will learn some new particles essential for using verbs. We will learn how to specify the direct object of a verb and the location where a verb takes place whether it’s physical or abstract.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this section, we will learn some new particles essential for using verbs. We will learn how to specify the direct object of a verb and the location where a verb takes place whether it’s physical or abstract.',
      grammars: [],
      keywords: ['verb','particles'],
    ),
    Section(
      heading: 'The direct object 「を」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('魚 【さかな】 – fish'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('ジュース – juice'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('街 【まち】 – town'),
                  Text('ぶらぶら – wandering; aimlessly'),
                  Text('歩く 【ある・く】 (u-verb) – to walk'),
                  Text('高速 【こう・そく】 – high-speed'),
                  Text('道路 【どう・ろ】 – route'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('メールアドレス – email address'),
                  Text('登録 【とう・ろく】 – register'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The first particle we will learn is the object particle because it is a very straightforward particle. The 「を」 character is attached to the end of a word to signify that that word is the direct object of the verb. This character is essentially never used anywhere else. That is why the katakana equivalent 「ヲ」 is almost never used since particles are always written in hiragana. The 「を」 character, while technically pronounced as /wo/ essentially sounds like /o/ in real speech. Here are some examples of the direct object particle in action.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Eat fish.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: juusu,
                              inlineSpan: const TextSpan(
                                text: 'ジュース',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: const TextSpan(
                                text: '飲んだ',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Drank juice.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Unlike the direct object we’re familiar with in English, places can also be the direct object of motion verbs such as 「',
                  ),
                  VocabTooltip(message: aruku, inlineSpan: const TextSpan(text: '歩く'),),
                  const TextSpan(
                    text:
                        '」 and 「',
                  ),
                  VocabTooltip(message: hashiru, inlineSpan: const TextSpan(text: '走る'),),
                  const TextSpan(
                    text:
                        '」. Since the motion verb is done to the location, the concept of direct object is the same in Japanese. However, as you can see by the next examples, it often translates to something different in English due to the slight difference of the concept of direct object.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: machi,
                              inlineSpan: const TextSpan(
                                text: '街',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: burabura,
                              inlineSpan: const TextSpan(
                                text: 'ぶらぶら',
                              ),
                            ),
                            VocabTooltip(
                              message: aruku,
                              inlineSpan: const TextSpan(
                                text: '歩く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Aimlessly walk through town. (Lit: Aimlessly walk town)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kousoku,
                              inlineSpan: const TextSpan(
                                text: '高速',
                              ),
                            ),
                            VocabTooltip(
                              message: douro,
                              inlineSpan: const TextSpan(
                                text: '道路',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hashiru,
                              inlineSpan: const TextSpan(
                                text: '走る',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Run through expressway. (Lit: Run expressway)',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When you use 「する」 with a noun, the 「を」 particle is optional and you can treat the whole [noun+する] as one verb.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: TextSpan(
                                text: '勉強',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Study Japanese everyday.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: meeruadoresu,
                              inlineSpan: const TextSpan(
                                text: 'メールアドレス',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: touroku,
                              inlineSpan: const TextSpan(
                                text: '登録',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Registered email address.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 魚 【さかな】 – fish 食べる 【た・べる】 (ru-verb) – to eat ジュース – juice 飲む 【の・む】 (u-verb) – to drink 街 【まち】 – town ぶらぶら – wandering; aimlessly 歩く 【ある・く】 (u-verb) – to walk 高速 【こう・そく】 – high-speed 道路 【どう・ろ】 – route 走る 【はし・る】 (u-verb) – to run 毎日 【まい・にち】 – everyday 日本語 【に・ほん・ご】 – Japanese (language) 勉強 【べん・きょう】 – study する (exception) – to do メールアドレス – email address 登録 【とう・ろく】 – register The first particle we will learn is the object particle because it is a very straightforward particle. The 「を」 character is attached to the end of a word to signify that that word is the direct object of the verb. This character is essentially never used anywhere else. That is why the katakana equivalent 「ヲ」 is almost never used since particles are always written in hiragana. The 「を」 character, while technically pronounced as /wo/ essentially sounds like /o/ in real speech. Here are some examples of the direct object particle in action. Examples 魚を食べる。 Eat fish. ジュースを飲んだ。 Drank juice. Unlike the direct object we’re familiar with in English, places can also be the direct object of motion verbs such as 「歩く」 and 「走る」. Since the motion verb is done to the location, the concept of direct object is the same in Japanese. However, as you can see by the next examples, it often translates to something different in English due to the slight difference of the concept of direct object. 街をぶらぶら歩く。 Aimlessly walk through town. (Lit: Aimlessly walk town) 高速道路を走る。 Run through expressway. (Lit: Run expressway) When you use 「する」 with a noun, the 「を」 particle is optional and you can treat the whole [noun+する] as one verb. 毎日、日本語を勉強する。 Study Japanese everyday. メールアドレスを登録した。 Registered email address.',
      grammars: ['を'],
      keywords: ['wo','o','particle','verb particle'],
    ),
    Section(
      heading: 'The target 「に」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('日本 【に・ほん】 – Japan'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('部屋 【へ・や】 – room'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('アメリカ – America'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('今日 【きょう】 – today'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('猫 【ねこ】 – cat'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('いす – chair'),
                  Text('台所 【だい・どころ】 – kitchen'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('いい (i-adj) – good'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('会う 【あう】 (u-verb) – to meet'),
                  Text('医者 【い・しゃ】 – doctor'),
                  Text('なる (u-verb) – to become'),
                  Text('先週 【せん・しゅう】 – last week'),
                  Text('図書館 【と・しょ・かん】 – library'),
                  Text('来年 【らい・ねん】 – next year'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The 「に」 particle can specify a target of a verb. This is different from the 「を」 particle in which the verb does something to the direct object. With the 「に」 particle, the verb does something ',
                  ),
                  TextSpan(
                    text: 'toward',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' the word associated with the 「に」 particle. For example, the target of any motion verb is specified by the 「に」 particle.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob went to Japan.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰らない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Not go back home.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'くる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Come to room.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'As you can see in the last example, the target particle always targets “to” rather than “from”. If you wanted to say, “come from” for example, you would need to use 「から」, which means “from”. If you used 「に」, it would instead mean “come to“. 「から」 is also often paired with 「まで」, which means “up to”.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスは、',
                            ),
                            VocabTooltip(
                              message: amerika,
                              inlineSpan: const TextSpan(
                                text: 'アメリカ',
                              ),
                            ),
                            TextSpan(
                              text: 'から',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Alice came from America.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            TextSpan(
                              text: 'から',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            TextSpan(
                              text: 'まで',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Will do homework from today to tomorrow.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The idea of a target in Japanese is very general and is not restricted to motion verbs. For example, the location of an object is defined as the target of the verb for existence （ある and いる）. Time is also a common target. Here are some examples of non-motion verbs and their targets.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Cat is in room.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: isu,
                              inlineSpan: const TextSpan(
                                text: 'いす',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: daidokoro,
                              inlineSpan: const TextSpan(
                                text: '台所',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Chair was in the kitchen.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(
                                text: '会った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Met good friend.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ジムは',
                            ),
                            VocabTooltip(
                              message: isha,
                              inlineSpan: const TextSpan(
                                text: '医者',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Jim will become doctor.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senshuu,
                              inlineSpan: const TextSpan(
                                text: '先週',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: toshokan,
                              inlineSpan: const TextSpan(
                                text: '図書館',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Went to library last week.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Note: Don’t forget to use 「ある」 for inanimate objects such as the chair and 「いる」 for animate objects such as the cat.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'While the 「に」 particle is not always required to indicate time, there is a slight difference in meaning between using the target particle and not using anything at all. In the following examples, the target particle makes the date a specific target emphasizing that the friend will go to Japan at that time. Without the particle, there is no special emphasis.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: rainen,
                              inlineSpan: const TextSpan(
                                text: '来年',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Next year, friend go to Japan.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: rainen,
                              inlineSpan: const TextSpan(
                                text: '来年',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Friend go to Japan next year.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 日本 【に・ほん】 – Japan 行く 【い・く】 (u-verb) – to go 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home 部屋 【へ・や】 – room 来る 【く・る】 (exception) – to come アメリカ – America 宿題 【しゅく・だい】 – homework 今日 【きょう】 – today 明日 【あした】 – tomorrow 猫 【ねこ】 – cat いる (ru-verb) – to exist (animate) いす – chair 台所 【だい・どころ】 – kitchen ある (u-verb) – to exist (inanimate) いい (i-adj) – good 友達 【とも・だち】 – friend 会う 【あう】 (u-verb) – to meet 医者 【い・しゃ】 – doctor なる (u-verb) – to become 先週 【せん・しゅう】 – last week 図書館 【と・しょ・かん】 – library 来年 【らい・ねん】 – next year The 「に」 particle can specify a target of a verb. This is different from the 「を」 particle in which the verb does something to the direct object. With the 「に」 particle, the verb does something toward the word associated with the 「に」 particle. For example, the target of any motion verb is specified by the 「に」 particle. Examples ボブは日本に行った。 Bob went to Japan. 家に帰らない。 Not go back home. 部屋にくる。 Come to room. As you can see in the last example, the target particle always targets “to” rather than “from”. If you wanted to say, “come from” for example, you would need to use 「から」, which means “from”. If you used 「に」, it would instead mean “come to“. 「から」 is also often paired with 「まで」, which means “up to”. アリスは、アメリカからきた。 Alice came from America. 宿題を今日から明日までする。 Will do homework from today to tomorrow. The idea of a target in Japanese is very general and is not restricted to motion verbs. For example, the location of an object is defined as the target of the verb for existence （ある and いる）. Time is also a common target. Here are some examples of non-motion verbs and their targets. 猫は部屋にいる。 Cat is in room. いすが台所にあった。 Chair was in the kitchen. いい友達に会った。 Met good friend. ジムは医者になる。 Jim will become doctor. 先週に図書館に行った。 Went to library last week. Note: Don’t forget to use 「ある」 for inanimate objects such as the chair and 「いる」 for animate objects such as the cat. While the 「に」 particle is not always required to indicate time, there is a slight difference in meaning between using the target particle and not using anything at all. In the following examples, the target particle makes the date a specific target emphasizing that the friend will go to Japan at that time. Without the particle, there is no special emphasis. 友達は、来年、日本に行く。 Next year, friend go to Japan. 友達は、来年に日本に行く。 Friend go to Japan next year.',
      grammars: ['に'],
      keywords: ['ni','particle','verb particle'],
    ),
    Section(
      heading: 'The directional 「へ」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('日本 【に・ほん】 – Japan'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('部屋 【へ・や】 – room'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('医者 【い・しゃ】 – doctor'),
                  Text('なる (u-verb) – to become'),
                  Text('勝ち 【か・ち】 – victory'),
                  Text('向かう 【むか・う】 (u-verb) – to face; to go towards'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'While 「へ」 is normally pronounced /he/, when it is being used as a particle, it is always pronounced /e/ （え）. The primary difference between the 「に」 and 「へ」 particle is that 「に」 goes to a target as the final, intended destination (both physical or abstract). The 「へ」 particle, on the other hand, is used to express the fact that one is setting out towards ',
                  ),
                  TextSpan(
                    text: 'the direction ',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' of the target. As a result, it is only used with directional motion verbs. It also does not guarantee whether the target is the final intended destination, only that one is heading towards that direction. In other words, the 「に」 particle sticks to the destination while the 「へ」 particle is fuzzy about where one is ultimately headed. For example, if we choose to replace 「に」 with 「へ」 in the first three examples of the previous section, the nuance changes slightly. ',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブは',
                            ),
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Bob ',
                            ),
                            TextSpan(
                              text: 'headed towards',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' Japan.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰らない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Not go home ',
                            ),
                            TextSpan(
                              text: 'toward',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' house.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'くる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Come ',
                            ),
                            TextSpan(
                              text: 'towards',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' room.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Note that we cannot use the 「へ」 particle with verbs that have no physical direction. For example, the following is incorrect.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: isha,
                              inlineSpan: const TextSpan(
                                text: '医者',
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '（Grammatically incorrect version of 「',
                            ),
                            VocabTooltip(
                              message: isha,
                              inlineSpan: const TextSpan(
                                text: '医者',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: '」.）',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This does not mean to say that 「へ」 cannot set out towards an abstract concept. In fact, because of the fuzzy directional meaning of this particle, the 「へ」 particle can also be used to talk about setting out towards certain future goals or expectations.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kachi,
                              inlineSpan: const TextSpan(
                                text: '勝ち',
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: mukau,
                              inlineSpan: const TextSpan(
                                text: '向かう',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Go towards victory.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 日本 【に・ほん】 – Japan 行く 【い・く】 (u-verb) – to go 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home 部屋 【へ・や】 – room 来る 【く・る】 (exception) – to come 医者 【い・しゃ】 – doctor なる (u-verb) – to become 勝ち 【か・ち】 – victory 向かう 【むか・う】 (u-verb) – to face; to go towards While 「へ」 is normally pronounced /he/, when it is being used as a particle, it is always pronounced /e/ （え）. The primary difference between the 「に」 and 「へ」 particle is that 「に」 goes to a target as the final, intended destination (both physical or abstract). The 「へ」 particle, on the other hand, is used to express the fact that one is setting out towards the direction  of the target. As a result, it is only used with directional motion verbs. It also does not guarantee whether the target is the final intended destination, only that one is heading towards that direction. In other words, the 「に」 particle sticks to the destination while the 「へ」 particle is fuzzy about where one is ultimately headed. For example, if we choose to replace 「に」 with 「へ」 in the first three examples of the previous section, the nuance changes slightly. Examples ボブは日本へ行った。 Bob headed towards Japan. 家へ帰らない。 Not go home toward house. 部屋へくる。 Come towards room. Note that we cannot use the 「へ」 particle with verbs that have no physical direction. For example, the following is incorrect. 医者へなる。（Grammatically incorrect version of 「医者になる」.） This does not mean to say that 「へ」 cannot set out towards an abstract concept. In fact, because of the fuzzy directional meaning of this particle, the 「へ」 particle can also be used to talk about setting out towards certain future goals or expectations. 勝ちへ向かう。 Go towards victory.',
      grammars: ['へ'],
      keywords: ['he','e','particle','verb particle'],
    ),
    Section(
      heading: 'The contextual 「で」 particle',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('映画館 【えい・が・かん】 – movie theatre'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('バス – bus'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('レストラン – restaurant'),
                  Text('昼ご飯 【ひる・ご・はん】 – lunch'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('何 【なに／なん】 – what'),
                  Text('暇 【ひま】 – free　(as in not busy)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The 「で」 particle will allow us to specify the context in which the action is performed. For example, if a person ate a fish, where did he eat it? If a person went to school, by what means did she go? With what will you eat the soup? All of these questions can be answered with the 「で」 particle. Here are some examples.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eigakan,
                              inlineSpan: const TextSpan(
                                text: '映画館',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Saw at movie theater.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: basu,
                              inlineSpan: const TextSpan(
                                text: 'バス',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰る',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Go home by bus.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: resutoran,
                              inlineSpan: const TextSpan(
                                text: 'レストラン',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hirugohan,
                              inlineSpan: const TextSpan(
                                text: '昼ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Ate lunch at restaurant.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'It may help to think of 「で」 as meaning “by way of”. This way, the same meaning will kind of translate into what the sentence means. The examples will then read: “Saw by way of movie theater”, “Go home by way of bus”, and “Ate lunch by way of restaurant.”',
                  ),
                ],
              ),
              const Heading(text: 'Using 「で」 with 「何」', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The word for “what” （何） is quite annoying because while it’s usually read as 「なに」, sometimes it is read as 「なん」 depending on how it’s used. And since it’s always written in Kanji, you can’t tell which it is. I would suggest sticking with 「なに」 until someone corrects you for when it should be 「なん」. With the 「で」 particle, it is read as 「なに」 as well. (Hold the mouse cursor over the word to check the reading.)',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Came by the way of what?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: basu,
                              inlineSpan: const TextSpan(
                                text: 'バス',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Came by the way of bus.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here’s the confusing part. There is a colloquial version of the word “why” that is used much more often than the less colloquial version 「どうして」 or the more forceful 「なぜ」. It is also written as 「何で」 but it is read as 「なんで」. This is a completely separate word and has nothing to do with the 「で」 particle.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nande,
                              inlineSpan: TextSpan(
                                text: '何で',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Why did you come?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hima,
                              inlineSpan: const TextSpan(
                                text: '暇',
                              ),
                            ),
                            const TextSpan(
                              text: 'だから。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Because I am free (as in have nothing to do).',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The 「から」 here meaning “because” is different from the 「から」 we just learned and will be covered later in the compound sentence section. Basically the point is that the two sentences, while written the same way, are read differently and mean completely different things. Don’t worry. This causes less confusion than you think because 95% of the time, the latter is used rather than the former. And even when 「なにで」 is intended, the context will leave no mistake on which one is being used. Even in this short example snippet, you can tell which it is by looking at the answer to the question.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 映画館 【えい・が・かん】 – movie theatre 見る 【み・る】 (ru-verb) – to see バス – bus 帰る 【かえ・る】 (u-verb) – to go home レストラン – restaurant 昼ご飯 【ひる・ご・はん】 – lunch 食べる 【た・べる】 (ru-verb) – to eat 何 【なに／なん】 – what 暇 【ひま】 – free　(as in not busy) The 「で」 particle will allow us to specify the context in which the action is performed. For example, if a person ate a fish, where did he eat it? If a person went to school, by what means did she go? With what will you eat the soup? All of these questions can be answered with the 「で」 particle. Here are some examples. Examples 映画館で見た。 Saw at movie theater. バスで帰る。 Go home by bus. レストランで昼ご飯を食べた。 Ate lunch at restaurant. It may help to think of 「で」 as meaning “by way of”. This way, the same meaning will kind of translate into what the sentence means. The examples will then read: “Saw by way of movie theater”, “Go home by way of bus”, and “Ate lunch by way of restaurant.” Using 「で」 with 「何」 The word for “what” （何） is quite annoying because while it’s usually read as 「なに」, sometimes it is read as 「なん」 depending on how it’s used. And since it’s always written in Kanji, you can’t tell which it is. I would suggest sticking with 「なに」 until someone corrects you for when it should be 「なん」. With the 「で」 particle, it is read as 「なに」 as well. (Hold the mouse cursor over the word to check the reading.) 何できた？ Came by the way of what? バスできた。 Came by the way of bus. Here’s the confusing part. There is a colloquial version of the word “why” that is used much more often than the less colloquial version 「どうして」 or the more forceful 「なぜ」. It is also written as 「何で」 but it is read as 「なんで」. This is a completely separate word and has nothing to do with the 「で」 particle. 何できた？ Why did you come? 暇だから。 Because I am free (as in have nothing to do). The 「から」 here meaning “because” is different from the 「から」 we just learned and will be covered later in the compound sentence section. Basically the point is that the two sentences, while written the same way, are read differently and mean completely different things. Don’t worry. This causes less confusion than you think because 95% of the time, the latter is used rather than the former. And even when 「なにで」 is intended, the context will leave no mistake on which one is being used. Even in this short example snippet, you can tell which it is by looking at the answer to the question.',
      grammars: ['で'],
      keywords: ['de','particle','verb particle'],
    ),
    Section(
      heading: 'When location is the topic',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('図書館 【と・しょ・かん】 – library'),
                  Text('どこ – where'),
                  Text('イタリア – Italy'),
                  Text('レストラン – restaurant'),
                  Text('どう – how'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'There are times when the location of an action is also the topic of a sentence. You can attach the topic particle （「は」 and 「も」） to the three particles that indicate location （「に」、「へ」、「で」） when the location is the topic. We’ll see how location might become the topic in the following examples.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: gakkou,
                            inlineSpan: const TextSpan(
                              text: '学校',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行った',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('(Did you) go to school?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行かなかった',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Didn’t go.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: toshokan,
                            inlineSpan: const TextSpan(
                              text: '図書館',
                            ),
                          ),
                          TextSpan(
                            text: 'には',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('What about library?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: toshokan,
                            inlineSpan: const TextSpan(
                              text: '図書館',
                            ),
                          ),
                          TextSpan(
                            text: 'にも',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行かなかった',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Also didn’t go to library.'),
                    english: true,
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In this example, Bob brings up a new topic (library) and so the location becomes the topic. The sentence is actually an abbreviated version of 「',
                  ),
                  VocabTooltip(
                    message: toshokan,
                    inlineSpan: const TextSpan(
                      text: '図書館',
                    ),
                  ),
                  const TextSpan(
                    text: 'には',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行った',
                    ),
                  ),
                  const TextSpan(
                    text: '？」 which you can ascertain from the context.',
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'ボブ',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: doko,
                            inlineSpan: const TextSpan(
                              text: 'どこ',
                            ),
                          ),
                          const TextSpan(
                            text: 'で',
                          ),
                          VocabTooltip(
                            message: taberu,
                            inlineSpan: const TextSpan(
                              text: '食べる',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('Eat where?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'アリス',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: itaria,
                            inlineSpan: const TextSpan(
                              text: 'イタリア',
                            ),
                          ),
                          VocabTooltip(
                            message: resutoran,
                            inlineSpan: const TextSpan(
                              text: 'レストラン',
                            ),
                          ),
                          TextSpan(
                            text: 'では',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: dou,
                            inlineSpan: const TextSpan(
                              text: 'どう',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('How about Italian restaurant?'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Bob asks, “Where shall we eat?” and Alice suggests an Italian restaurant. A sentence like, “How about…” usually brings up a new topic because the person is suggesting something new. In this case, the location (restaurant) is being suggested so it becomes the topic.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 図書館 【と・しょ・かん】 – library どこ – where イタリア – Italy レストラン – restaurant どう – how There are times when the location of an action is also the topic of a sentence. You can attach the topic particle （「は」 and 「も」） to the three particles that indicate location （「に」、「へ」、「で」） when the location is the topic. We’ll see how location might become the topic in the following examples. Example 1 ボブ：学校に行った？ Bob: (Did you) go to school? アリス：行かなかった。 Alice: Didn’t go. ボブ：図書館には？ Bob: What about library? アリス：図書館にも行かなかった。 Alice: Also didn’t go to library. In this example, Bob brings up a new topic (library) and so the location becomes the topic. The sentence is actually an abbreviated version of 「図書館には行った？」 which you can ascertain from the context. Example 2 ボブ：どこで食べる？ Bob: Eat where? アリス：イタリアレストランではどう？ Alice: How about Italian restaurant? Bob asks, “Where shall we eat?” and Alice suggests an Italian restaurant. A sentence like, “How about…” usually brings up a new topic because the person is suggesting something new. In this case, the location (restaurant) is being suggested so it becomes the topic.',
      grammars: ['には','にも','では'],
      keywords: ['niwa','niha','nimo','particle','verb particle'],
    ),
    Section(
      heading: 'When direct object is the topic',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('日本語 【に・ほん・ご】 – Japanese (language'),
                  Text('習う 【なら・う】 (u-verb) – to learn'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The direct object particle is different from particles related to location in that you cannot use any other particles at the same time. For example, going by the previous section, you might have guessed that you can say 「をは」 to express a direct object that is also the topic but this is not the case. A topic can be a direct object without using the 「を」 particle. In fact, putting the 「を」 particle in will make it wrong.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: narau,
                              inlineSpan: const TextSpan(
                                text: '習う',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Learn Japanese.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: narau,
                              inlineSpan: const TextSpan(
                                text: '習う',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'About Japanese, (will) learn it.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Please take care to not make this mistake.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            TextSpan(
                              text: 'をは',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: narau,
                              inlineSpan: const TextSpan(
                                text: '習う',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(This is incorrect.)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 日本語 【に・ほん・ご】 – Japanese (language 習う 【なら・う】 (u-verb) – to learn The direct object particle is different from particles related to location in that you cannot use any other particles at the same time. For example, going by the previous section, you might have guessed that you can say 「をは」 to express a direct object that is also the topic but this is not the case. A topic can be a direct object without using the 「を」 particle. In fact, putting the 「を」 particle in will make it wrong. Examples 日本語を習う。 Learn Japanese. 日本語は、習う。 About Japanese, (will) learn it. Please take care to not make this mistake. 日本語をは、習う。(This is incorrect.)',
      grammars: [],
      keywords: ['wo','o','particle','verb particle'],
    ),
  ],
);
