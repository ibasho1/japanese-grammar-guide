import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/notice_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/exercises/verb_classification_exercise.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson verbs = Lesson(
  title: 'Verb Basics',
  sections: [
    Section(
      heading: 'Role of verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '食べる 【た・べる】 (ru-verb) – to eat',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '分かる 【わ・かる】 (u-verb) – to understand',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '見る 【み・る】 (ru-verb) – to see',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '寝る 【ね・る】 (ru-verb) – to sleep',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '起きる 【お・きる】 (ru-verb) – to wake; to occur',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '考える 【かんが・える】 (ru-verb) – to think',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '教える 【おし・える】 (ru-verb) – to teach; to inform',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '出る 【で・る】 (ru-verb) – to come out',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'いる (ru-verb) – to exist (animate)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '着る 【き・る】 (ru-verb) – to wear',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '話す 【はな・す】 (u-verb) – to speak',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '聞く 【き・く】 (u-verb) – to ask; to listen',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '泳ぐ 【およ・ぐ】 (u-verb) – to swim',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '遊ぶ 【あそ・ぶ】 (u-verb) – to play',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '待つ 【ま・つ】 (u-verb) – to wait',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '飲む 【の・む】 (u-verb) – to drink',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '買う 【か・う】 (u-verb) – to buy',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'ある (u-verb) – to exist (inanimate)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '死ぬ 【し・ぬ】 (u-verb) – to die',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'する (exception) – to do',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '来る 【く・る】 (exception) – to come',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'お金 【お・かね】 – money',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '私 【わたし】 – me, myself, I',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '猫 【ねこ】 – cat',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We’ve now learned how to describe nouns in various ways with other nouns and adjectives. This gives us quite a bit of expressive power. However, we still cannot express actions. This is where verbs come in. Verbs, in Japanese, always come at the end of clauses. Since we have not yet learned how to create more than one clause, for now it means that any sentence with a verb must end with the verb. We will now learn the three main categories of verbs, which will allow us to define conjugation rules. Before learning about verbs, there is one important thing to keep in mind.',
                  ),
                ],
              ),
              const NoticeSection(
                text: Text.rich(
                  TextSpan(
                    children: [TextSpan(
                    text: 'A grammatically complete sentence requires a verb ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: 'only',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text: ' (including state-of-being).',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),],
                  ),
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Or to rephrase, unlike English, the only thing you need to make a grammatically complete sentence is a verb and nothing else! That’s why even the simplest, most basic Japanese sentence cannot be translated into English!',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'A grammatically complete sentence:',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Eat. (possible translations include: I eat/she eats/they eat)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 分かる 【わ・かる】 (u-verb) – to understand 見る 【み・る】 (ru-verb) – to see 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 考える 【かんが・える】 (ru-verb) – to think 教える 【おし・える】 (ru-verb) – to teach; to inform 出る 【で・る】 (ru-verb) – to come out いる (ru-verb) – to exist (animate) 着る 【き・る】 (ru-verb) – to wear 話す 【はな・す】 (u-verb) – to speak 聞く 【き・く】 (u-verb) – to ask; to listen 泳ぐ 【およ・ぐ】 (u-verb) – to swim 遊ぶ 【あそ・ぶ】 (u-verb) – to play 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 買う 【か・う】 (u-verb) – to buy ある (u-verb) – to exist (inanimate) 死ぬ 【し・ぬ】 (u-verb) – to die する (exception) – to do 来る 【く・る】 (exception) – to come お金 【お・かね】 – money 私 【わたし】 – me, myself, I 猫 【ねこ】 – cat We’ve now learned how to describe nouns in various ways with other nouns and adjectives. This gives us quite a bit of expressive power. However, we still cannot express actions. This is where verbs come in. Verbs, in Japanese, always come at the end of clauses. Since we have not yet learned how to create more than one clause, for now it means that any sentence with a verb must end with the verb. We will now learn the three main categories of verbs, which will allow us to define conjugation rules. Before learning about verbs, there is one important thing to keep in mind. A grammatically complete sentence requires a verb only (including state-of-being). Or to rephrase, unlike English, the only thing you need to make a grammatically complete sentence is a verb and nothing else! That’s why even the simplest, most basic Japanese sentence cannot be translated into English! A grammatically complete sentence: 食べる。 Eat. (possible translations include: I eat/she eats/they eat)',
      grammars: [],
      keywords: ['verb','basics'],
    ),
    Section(
      heading: 'Classifying verbs into ru-verbs and u-verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Before we can learn any verb conjugations, we first need to learn how verbs are categorized. With the exception of only two exception verbs, all verbs fall into the category of ',
                  ),
                  TextSpan(
                    text: 'ru-verb',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                    text: ' or ',
                  ),
                  TextSpan(
                    text: 'u-verb',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(text: '. '),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'All ru-verbs end in 「る」 while u-verbs can end in a number of u-vowel sounds including 「る」. Therefore, if a verb does ',
                  ),
                  TextSpan(
                    text: 'not',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text:
                        ' end in 「る」, it will always be an u-verb. For verbs ending in 「る」, if the vowel sound preceding the 「る」 is an /a/, /u/ or /o/ vowel sound, it will always be an u-verb. Otherwise, if the preceding sound is an /i/ or /e/ vowel sound, it will be a ru-verb ',
                  ),
                  TextSpan(
                    text: 'in most cases',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                      text:
                          '. A list of common exceptions are at the end of this section.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              const NumberedList(
                items: [
                  Text('食べる – 「べ」 is an e-vowel sound so it is a ru-verb'),
                  Text('分かる – 「か」 is an a-vowel sound so it is an u-verb'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'If you’re unsure which category a verb falls in, you can verify which kind it is with most dictionaries. There are only two exception verbs that are neither ru-verbs nor u-verbs as shown in the table below.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'Examples of different verb types'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: const [
                        TableRow(
                          children: [
                            TableHeading(
                              text: 'ru-verb',
                            ),
                            TableHeading(
                              text: 'u-verb',
                            ),
                            TableHeading(
                              text: 'exception',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('見る'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('話す'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('する'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('食べる'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('聞く'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('来る'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('寝る'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('泳ぐ'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('起きる'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('遊ぶ'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('考える'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('待つ'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('教える'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('飲む'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('出る'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('買う'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('いる'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('ある'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text('着る'),
                            ),
                            TableData(
                              centered: false,
                              content: Text('死ぬ'),
                            ),
                            TableData(
                              centered: false,
                              content: Text(' '),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here are some example sentences using ru-verbs, u-verbs, and exception verbs.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスは',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                  text: '食べる',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for Alice, eat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ジムが',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                  text: '来る',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Jim is the one that comes.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'ボブも',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                  text: 'する',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Bob also do.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                  text: 'ある',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There is money. (lit: Money is the thing that exists.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買う',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('As for me, buy.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: TextSpan(
                                  text: 'いる',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary)),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('There is cat. (lit: As for cat, it exists.)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Before we can learn any verb conjugations, we first need to learn how verbs are categorized. With the exception of only two exception verbs, all verbs fall into the category of ru-verb or u-verb. All ru-verbs end in 「る」 while u-verbs can end in a number of u-vowel sounds including 「る」. Therefore, if a verb does not end in 「る」, it will always be an u-verb. For verbs ending in 「る」, if the vowel sound preceding the 「る」 is an /a/, /u/ or /o/ vowel sound, it will always be an u-verb. Otherwise, if the preceding sound is an /i/ or /e/ vowel sound, it will be a ru-verb in most cases. A list of common exceptions are at the end of this section. Examples 食べる – 「べ」 is an e-vowel sound so it is a ru-verb 分かる – 「か」 is an a-vowel sound so it is an u-verb If you’re unsure which category a verb falls in, you can verify which kind it is with most dictionaries. There are only two exception verbs that are neither ru-verbs nor u-verbs as shown in the table below. 見る 話す する 食べる 聞く 来る 寝る 泳ぐ 起きる 遊ぶ 考える 待つ 教える 飲む 出る 買う いる ある 着る 死ぬ Examples Here are some example sentences using ru-verbs, u-verbs, and exception verbs. アリスは食べる。 As for Alice, eat. ジムが来る。 Jim is the one that comes. ボブもする。 Bob also do. お金がある。 There is money. (lit: Money is the thing that exists.) 私は買う。 As for me, buy. 猫はいる。 There is cat. (lit: As for cat, it exists.)',
      grammars: ['一段動詞','五段動詞'],
      keywords: ['ru-verbs','u-verbs','ichidan verbs','godan verbs'],
    ),
    Section(
      heading: 'Appendix: iru/eru u-verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('要る 【い・る】 (u-verb) – to need'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('切る 【き・る】 (u-verb) – to cut'),
                  Text('しゃべる (u-verb) – to talk'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('減る 【へ・る】 (u-verb) – to decrease'),
                  Text('焦る 【あせ・る】 (u-verb) – to be in a hurry'),
                  Text('限る 【かぎ・る】 (u-verb) – to limit'),
                  Text('蹴る 【け・る】 (u-verb) – to kick'),
                  Text('滑る 【すべ・る】 (u-verb) – to be slippery'),
                  Text('握る 【にぎ・る】 (u-verb) – to grasp'),
                  Text('練る 【ね・る】 (u-verb) – to knead'),
                  Text('参る 【まい・る】 (u-verb) – to go; to come'),
                  Text('交じる 【まじ・る】 (u-verb) – to mingle'),
                  Text('嘲る 【あざけ・る】 (u-verb) – to ridicule'),
                  Text('覆る 【くつがえ・る】 (u-verb) – to overturn'),
                  Text('遮る 【さえぎ・る】 (u-verb) – to interrupt'),
                  Text('罵る 【ののし・る】 (u-verb) – to abuse verbally'),
                  Text('捻る 【ひね・る】 (u-verb) – to twist'),
                  Text('翻る 【ひるが・える】 (u-verb) – to turn over; to wave'),
                  Text('滅入る 【めい・る】 (u-verb) – to feel depressed'),
                  Text('蘇る 【よみがえ・る】 (u-verb) – to be resurrected'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Below is a list of u-verbs with a preceding vowel sound of /i/ or /e/ (“iru” or “eru” sound endings). The list is not comprehensive but it does include many of the more common verbs categorized roughly into three levels.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'iru/eru u-verbs grouped (roughly) by level'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'Basic',
                            ),
                            TableHeading(
                              text: 'Intermediate',
                            ),
                            TableHeading(
                              text: '	Advanced',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: iruNeed,
                                      inlineSpan: const TextSpan(
                                        text: '要る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: aseru,
                                      inlineSpan: const TextSpan(
                                        text: '焦る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: azakeru,
                                      inlineSpan: const TextSpan(
                                        text: '嘲る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kaeru,
                                      inlineSpan: const TextSpan(
                                        text: '帰る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kagiru,
                                      inlineSpan: const TextSpan(
                                        text: '限る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kutsugaeru,
                                      inlineSpan: const TextSpan(
                                        text: '覆る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kiru,
                                      inlineSpan: const TextSpan(
                                        text: '切る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: keru,
                                      inlineSpan: const TextSpan(
                                        text: '蹴る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: saegiru,
                                      inlineSpan: const TextSpan(
                                        text: '遮る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shaberu,
                                      inlineSpan: const TextSpan(
                                        text: 'しゃべる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: suberu,
                                      inlineSpan: const TextSpan(
                                        text: '滑る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: nonoshiru,
                                      inlineSpan: const TextSpan(
                                        text: '罵る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shiru,
                                      inlineSpan: const TextSpan(
                                        text: '知る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: nigiru,
                                      inlineSpan: const TextSpan(
                                        text: '握る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hineru,
                                      inlineSpan: const TextSpan(
                                        text: '捻る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hairu,
                                      inlineSpan: const TextSpan(
                                        text: '入る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: neru,
                                      inlineSpan: const TextSpan(
                                        text: '練る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hirugaeru,
                                      inlineSpan: const TextSpan(
                                        text: '翻る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: hashiru,
                                      inlineSpan: const TextSpan(
                                        text: '走る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: mairu,
                                      inlineSpan: const TextSpan(
                                        text: '参る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: meiru,
                                      inlineSpan: const TextSpan(
                                        text: '滅入る',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 要る 【い・る】 (u-verb) – to need 帰る 【かえ・る】 (u-verb) – to go home 切る 【き・る】 (u-verb) – to cut しゃべる (u-verb) – to talk 知る 【し・る】 (u-verb) – to know 入る 【はい・る】 (u-verb) – to enter 走る 【はし・る】 (u-verb) – to run 減る 【へ・る】 (u-verb) – to decrease 焦る 【あせ・る】 (u-verb) – to be in a hurry 限る 【かぎ・る】 (u-verb) – to limit 蹴る 【け・る】 (u-verb) – to kick 滑る 【すべ・る】 (u-verb) – to be slippery 握る 【にぎ・る】 (u-verb) – to grasp 練る 【ね・る】 (u-verb) – to knead 参る 【まい・る】 (u-verb) – to go; to come 交じる 【まじ・る】 (u-verb) – to mingle 嘲る 【あざけ・る】 (u-verb) – to ridicule 覆る 【くつがえ・る】 (u-verb) – to overturn 遮る 【さえぎ・る】 (u-verb) – to interrupt 罵る 【ののし・る】 (u-verb) – to abuse verbally 捻る 【ひね・る】 (u-verb) – to twist 翻る 【ひるが・える】 (u-verb) – to turn over; to wave 滅入る 【めい・る】 (u-verb) – to feel depressed 蘇る 【よみがえ・る】 (u-verb) – to be resurrected Below is a list of u-verbs with a preceding vowel sound of /i/ or /e/ (“iru” or “eru” sound endings). The list is not comprehensive but it does include many of the more common verbs categorized roughly into three levels.iru/eru u-verbs grouped (roughly) by level 要る 焦る 嘲る 帰る 限る 覆る 切る 蹴る 遮る しゃべる 滑る 罵る 知る 握る 捻る 入る 練る 翻る 走る 参る 滅入る',
      grammars: [],
      keywords: ['u-verbs'],
    ),
    Section(
      heading: 'Verb practice exercises',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary used in this section', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here is a list of a few verbs and the accompanying kanji that you will find useful.',
                  ),
                ],
              ),
              NotesSection(
                  content: BlankList(
                    items: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                              'I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below).'),
                          NumberedList(
                            items: [
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%A6%8B%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '見 – see'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E6%9D%A5%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '来 – come; next'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%A1%8C%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '行 – go; conduct'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%B8%B0%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '帰 – go home'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E9%A3%9F%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '食 – eat; food'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E9%A3%B2%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '飲 – drink'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%B2%B7%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '買 – buy'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%A3%B2%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '売 – sell'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E6%8C%81%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '持 – hold'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%BE%85%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '待 – wait'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%AA%AD%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '読 – read'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E6%AD%A9%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '歩 – walk'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E8%B5%B0%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '走 – run'),
                                  ),
                                ],
                              ),
                            ),
                              Text.rich(
                              TextSpan(
                                children: [
                                  ExternalLink(
                                    uri:
                                        'https://jisho.org/search/%E5%88%87%20%23kanji',
                                    inlineSpan:
                                        const TextSpan(text: '切 – cut'),
                                  ),
                                ],
                              ),
                            ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  heading: 'Kanji'),
              const NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here is a list of some common verbs you will definitely want to learn at some point.'),
                        NumberedList(
                          items: [
                            Text(
                              'する – to do',
                            ),
                            Text(
                              'しゃべる – to talk; to chat',
                            ),
                            Text(
                              '見る【み・る】 – to see',
                            ),
                            Text(
                              '来る【く・る】 – to come',
                            ),
                            Text(
                              '行く【い・く】 – to go',
                            ),
                            Text(
                              '帰る 【かえ・る】 – to go home',
                            ),
                            Text(
                              '食べる 【たべ・る】 – to eat',
                            ),
                            Text(
                              '飲む 【の・む】 – to drink',
                            ),
                            Text(
                              '買う 【か・う】 – to buy',
                            ),
                            Text(
                              '売る 【う・る】 – to sell',
                            ),
                            Text(
                              '切る 【き・る】 – to cut',
                            ),
                            Text(
                              '入る 【はい・る】 – to enter',
                            ),
                            Text(
                              '出る 【で・る】 – to come out',
                            ),
                            Text(
                              '持つ 【も・つ】 – to hold',
                            ),
                            Text(
                              '待つ 【ま・つ】 – to wait',
                            ),
                            Text(
                              '書く【か・く】 – to write',
                            ),
                            Text(
                              '読む 【よ・む】 – to read',
                            ),
                            Text(
                              '歩く 【ある・く】 – to walk',
                            ),
                            Text(
                              '走る 【はし・る】 – to run',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Vocabulary',
              ),
              const Heading(text: 'Practice with Verb Classification', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'There’s really not much to do at this point except to practice classifying verbs as either a ru-verb or an u-verb. You can also take this opportunity to learn some useful verbs if you do not know them already. We’ll learn how to conjugate these verbs according to their category in the next few sections.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In the chart below, you should mark whether the given verb is either an u-verb or a ru-verb. The first answer is given as an example of what you need to do. Obviously, verbs that do not end in 「る」 are always going to be u-verbs so the tricky part is figuring out the category for verbs that end in 「る」. Remember that verbs that do not end in “eru” or “iru” will always be u-verbs. While most verbs that do end in “eru” or “iru” are ru-verbs, to make things interesting, I’ve also included a number of u-verbs that also end in eru/iru. Though you do not need to memorize every word in the list by any means, you should at least memorize the basic verbs.',
                  ),
                ],
              ),
              const VerbClassificationExercise(),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary used in this section Here is a list of a few verbs and the accompanying kanji that you will find useful. Kanji I have listed the kanji you will need for the vocabulary for your convenience. The link will take you to a diagram of the stroke order. I recommend practicing the kanji in the context of real words (such as the ones below). 見 – see 来 – come; next 行 – go; conduct 帰 – go home 食 – eat; food 飲 – drink 買 – buy 売 – sell 持 – hold 待 – wait 読 – read 歩 – walk 走 – run 切 – cut Vocabulary Here is a list of some common verbs you will definitely want to learn at some point. する – to do しゃべる – to talk; to chat 見る【み・る】 – to see 来る【く・る】 – to come 行く【い・く】 – to go 帰る 【かえ・る】 – to go home 食べる 【たべ・る】 – to eat 飲む 【の・む】 – to drink 買う 【か・う】 – to buy 売る 【う・る】 – to sell 切る 【き・る】 – to cut 入る 【はい・る】 – to enter 出る 【で・る】 – to come out 持つ 【も・つ】 – to hold 待つ 【ま・つ】 – to wait 書く【か・く】 – to write 読む 【よ・む】 – to read 歩く 【ある・く】 – to walk 走る 【はし・る】 – to run Practice with Verb Classification There’s really not much to do at this point except to practice classifying verbs as either a ru-verb or an u-verb. You can also take this opportunity to learn some useful verbs if you do not know them already. We’ll learn how to conjugate these verbs according to their category in the next few sections. In the chart below, you should mark whether the given verb is either an u-verb or a ru-verb. The first answer is given as an example of what you need to do. Obviously, verbs that do not end in 「る」 are always going to be u-verbs so the tricky part is figuring out the category for verbs that end in 「る」. Remember that verbs that do not end in “eru” or “iru” will always be u-verbs. While most verbs that do end in “eru” or “iru” are ru-verbs, to make things interesting, I’ve also included a number of u-verbs that also end in eru/iru. Though you do not need to memorize every word in the list by any means, you should at least memorize the basic verbs.',
      grammars: [],
      keywords: ['exercises'],
    ),
  ],
);
