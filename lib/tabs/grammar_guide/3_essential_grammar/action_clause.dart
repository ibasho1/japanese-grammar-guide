import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson actionClause = Lesson(
  title: 'Acting on Relative Clauses',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In the section about modifying relative clauses, we learned how to treat a relative clause like an adjective to directly modify a noun. We will extend the functionality of relative clauses by learning how to perform an action on a relative clause. Obviously, we cannot simply attach the 「を」 particle to a relative clause because the 「を」 particle only applies to noun phrases. We need something to encapsulate the relative clause into a unit that we can perform actions on. This is done by making a quoted phrase.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'While in English, you can just add quotes and a comma to make a quotation, Japanese requires attaching 「と」 at the end of the quote. This is completely different from the 「と」 particle and the 「と」 conditional. Unlike quotes in English, we can perform many different types of actions on the quote besides the standard “he said”, “she said”, etc. For example, we can perform the action, “to think” or “to hear” to produce phrases such as, “I think [clause]” or “I heard [clause]” This is very important in Japanese because Japanese people seldom affirm definite statements. This is also why we will have to eventually cover many other types of grammar to express uncertainty or probability.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In the section about modifying relative clauses, we learned how to treat a relative clause like an adjective to directly modify a noun. We will extend the functionality of relative clauses by learning how to perform an action on a relative clause. Obviously, we cannot simply attach the 「を」 particle to a relative clause because the 「を」 particle only applies to noun phrases. We need something to encapsulate the relative clause into a unit that we can perform actions on. This is done by making a quoted phrase. While in English, you can just add quotes and a comma to make a quotation, Japanese requires attaching 「と」 at the end of the quote. This is completely different from the 「と」 particle and the 「と」 conditional. Unlike quotes in English, we can perform many different types of actions on the quote besides the standard “he said”, “she said”, etc. For example, we can perform the action, “to think” or “to hear” to produce phrases such as, “I think [clause]” or “I heard [clause]” This is very important in Japanese because Japanese people seldom affirm definite statements. This is also why we will have to eventually cover many other types of grammar to express uncertainty or probability.',
      grammars: [],
      keywords: ['relative clause','acting'],
    ),
    Section(
      heading: 'The direct quote',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('叫ぶ 【さけ・ぶ】 (u-verb) – to scream'),
                  Text('呼ぶ 【よ・ぶ】 (u-verb) – to call'),
                  Text('呟く 【つぶや・く】 (u-verb) – to mutter'),
                  Text('寒い 【さむ・い】 (i-adj) – cold'),
                  Text('今日 【きょう】 – today'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We’ll learn the simplest type of quoted phrase, which is the direct quote. Basically, you are directly quoting something that was said. This is done by simply enclosing the statement in quotes, adding 「と」 and then inserting the appropriate verb. The most common verbs associated with a direct quote would be 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kiku,
                    inlineSpan: const TextSpan(
                      text: '聞く',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 but you may use any verbs related to direct quotation such as: 「',
                  ),
                  VocabTooltip(
                    message: sakebu,
                    inlineSpan: const TextSpan(
                      text: '叫ぶ',
                    ),
                  ),
                  const TextSpan(
                    text: '」, 「',
                  ),
                  VocabTooltip(
                    message: yobu,
                    inlineSpan: const TextSpan(
                      text: '呼ぶ',
                    ),
                  ),
                  const TextSpan(
                    text: '」, 「',
                  ),
                  VocabTooltip(
                    message: tsubuyaku,
                    inlineSpan: const TextSpan(
                      text: '呟く',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, etc. This type of quotation is often used for dialogue in novels and other narrative works.',
                  ),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスが、',
                            ),
                            TextSpan(
                              text: '「',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: samui,
                              inlineSpan: TextSpan(
                                text: '寒い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: '」と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Alice said, “Cold”.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '「',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: TextSpan(
                                text: '今日',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: TextSpan(
                                text: '授業',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: '」と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞いた',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだけど。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It is that I heard from the teacher, “There is no class today.”',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The verb does not need to be directly connected to the relative clause. As long as the verb that applies to the relative clause comes before any other verb, you can have any number of adjectives, adverbs or nouns in between.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '「',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: samui,
                              inlineSpan: TextSpan(
                                text: '寒い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: '」と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'アリスが、',
                            ),
                            VocabTooltip(
                              message: tanaka,
                              inlineSpan: const TextSpan(
                                text: '田中',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言った',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '“Cold,” Alice said to Tanaka.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 言う 【い・う】 (u-verb) – to say 聞く 【き・く】 (u-verb) – to ask; to listen 叫ぶ 【さけ・ぶ】 (u-verb) – to scream 呼ぶ 【よ・ぶ】 (u-verb) – to call 呟く 【つぶや・く】 (u-verb) – to mutter 寒い 【さむ・い】 (i-adj) – cold 今日 【きょう】 – today 授業 【じゅ・ぎょう】 – class 先生 【せん・せい】 – teacher 田中 【た・なか】 – Tanaka (last name) We’ll learn the simplest type of quoted phrase, which is the direct quote. Basically, you are directly quoting something that was said. This is done by simply enclosing the statement in quotes, adding 「と」 and then inserting the appropriate verb. The most common verbs associated with a direct quote would be 「言う」 and 「聞く」 but you may use any verbs related to direct quotation such as: 「叫ぶ」, 「呼ぶ」, 「呟く」, etc. This type of quotation is often used for dialogue in novels and other narrative works. Examples アリスが、「寒い」と言った。 Alice said, “Cold”. 「今日は授業がない」と先生から聞いたんだけど。 It is that I heard from the teacher, “There is no class today.” The verb does not need to be directly connected to the relative clause. As long as the verb that applies to the relative clause comes before any other verb, you can have any number of adjectives, adverbs or nouns in between. 「寒い」とアリスが、田中に言った。“Cold,” Alice said to Tanaka.',
      grammars: ['と'],
      keywords: ['to','particle','quote'],
    ),
    Section(
      heading: 'The interpreted quote',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('先生 【せん・せい】 – teacher'),
                  Text('今日 【きょう】 – today'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('これ – this'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('カレー – curry'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('時間 【じ・かん】 – time'),
                  Text('今 【いま】 – now'),
                  Text('どこ – where'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('高校生 【こう・こう・せい】 – high school student'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'The second type of quote is the quote along the lines of what someone actually said. It’s not a word-for-word quote. Since this is not a direct quote, no quotations are needed. You can also express thoughts as an interpreted quote as well. By using this and the verb 「'),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 you can say you think that something is so-and-so. You will hear Japanese people use this all the time. You can also use the verb 「'),
                  VocabTooltip(
                    message: kangaeru,
                    inlineSpan: const TextSpan(
                      text: '考える',
                    ),
                  ),
                  const TextSpan(text: '」 when you are considering something.'),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: TextSpan(
                                text: '今日',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: TextSpan(
                                text: '授業',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言った',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだけど。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I heard from the teacher that there is no class today.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: TextSpan(
                                text: '何',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言います',
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What do you call this in Japanese? (lit: About this, what do you say in Japanese?)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、アリス',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言います',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I am called Alice. (lit: As for me, you say Alice.)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'In an interpreted quote, the meaning of 「'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 may change as you see in examples 2 and 3. Actually, as you can see from the literal translation, the meaning remains the same in Japanese but changes only when translated to normal English. (We’ll learn more about various ways to use 「'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(text: '」 in the next lesson.)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here are some examples of thoughts being used as quoted relative clauses. In example 2 below, the question marker is used with the volitional to insert an embedded question.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: karee,
                              inlineSpan: const TextSpan(
                                text: 'カレー',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べよう',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思った',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I thought about setting out to eat curry but I didn’t have time to eat.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(
                                text: 'どこ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行こう',
                              ),
                            ),
                            const TextSpan(
                              text: 'か',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kangaeru,
                              inlineSpan: TextSpan(
                                text: '考えている',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Now, I’m considering where to set out to go.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Unlike the direct quotation, which you can just copy as is, if the quoted relative clause is a state-of-being for a noun or na-adjective, you have to explicitly include the declarative 「だ」 to show this.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            TextSpan(
                              text: 'だと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言いました',
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'What did he say this ',
                            ),
                            TextSpan(
                              text: 'is',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '?',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: koukousei,
                              inlineSpan: const TextSpan(
                                text: '高校生',
                              ),
                            ),
                            TextSpan(
                              text: 'だと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞いた',
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: shinjiru,
                              inlineSpan: const TextSpan(
                                text: '信じられない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'I heard that he ',
                            ),
                            TextSpan(
                              text: 'is',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text:
                                  ' a high school student but I can’t believe it.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Notice how 「だ」 was added to explicitly declare the state-of-being that is highlighted in the English translation. You can really see how important the 「だ」 is here by comparing the following two sentences.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            TextSpan(
                              text: 'だと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言いました',
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'What did (he) say this ',
                            ),
                            TextSpan(
                              text: 'is',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '?',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言いました',
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What did (he) say?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 先生 【せん・せい】 – teacher 今日 【きょう】 – today 授業 【じゅ・ぎょう】 – class 聞く 【き・く】 (u-verb) – to ask; to listen これ – this 日本語 【に・ほん・ご】 – Japanese (language) 言う 【い・う】 (u-verb) – to say 私 【わたし】 – me; myself; I カレー – curry 食べる 【た・べる】 (ru-verb) – to eat 思う 【おも・う】 (u-verb) – to think 時間 【じ・かん】 – time 今 【いま】 – now どこ – where 行く 【い・く】 (u-verb) – to go 考える 【かんが・える】 (ru-verb) – to think 彼 【かれ】 – he; boyfriend 高校生 【こう・こう・せい】 – high school student 信じる 【しん・じる】 (ru-verb) – to believe The second type of quote is the quote along the lines of what someone actually said. It’s not a word-for-word quote. Since this is not a direct quote, no quotations are needed. You can also express thoughts as an interpreted quote as well. By using this and the verb 「思う」 you can say you think that something is so-and-so. You will hear Japanese people use this all the time. You can also use the verb 「考える」 when you are considering something. Examples 先生から今日は授業がないと言ったんだけど。 I heard from the teacher that there is no class today. これは、日本語で何と言いますか。 What do you call this in Japanese? (lit: About this, what do you say in Japanese?) 私は、アリスと言います。 I am called Alice. (lit: As for me, you say Alice.) In an interpreted quote, the meaning of 「言う」 may change as you see in examples 2 and 3. Actually, as you can see from the literal translation, the meaning remains the same in Japanese but changes only when translated to normal English. (We’ll learn more about various ways to use 「いう」 in the next lesson.) Here are some examples of thoughts being used as quoted relative clauses. In example 2 below, the question marker is used with the volitional to insert an embedded question. カレーを食べようと思ったけど、食べる時間がなかった。 I thought about setting out to eat curry but I didn’t have time to eat. 今、どこに行こうかと考えている。 Now, I’m considering where to set out to go. Unlike the direct quotation, which you can just copy as is, if the quoted relative clause is a state-of-being for a noun or na-adjective, you have to explicitly include the declarative 「だ」 to show this. 彼は、これは何だと言いましたか。 What did he say this is? 彼は高校生だと聞いたけど、信じられない。 I heard that he is a high school student but I can’t believe it. Notice how 「だ」 was added to explicitly declare the state-of-being that is highlighted in the English translation. You can really see how important the 「だ」 is here by comparing the following two sentences. これは何だと言いましたか。 What did (he) say this is? 何と言いましたか。 What did (he) say?',
      grammars: [],
      keywords: ['to','particle','quote'],
    ),
    Section(
      heading: 'Using 「って」 as a casual version of 「と」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('智子 【とも・こ】 – Tomoko (first name)'),
                  Text('来年 【らい・ねん】 – next year'),
                  Text('海外 【かい・がい】 – overseas'),
                  Text('もう – already'),
                  Text('お金 【お・かね】 – money'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('本当 【ほん・とう】 – real'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('雨 【あめ】 – rain'),
                  Text('降る 【ふ・る】(u-verb) – to precipitate'),
                  Text('すごい (i-adj) – to a great extent'),
                  Text('いい (i-adj) – good'),
                  Text('人 【ひと】 – person'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You may be surprised to hear that there is a shorter and casual version of the quoted relative clause since it’s already only one hiragana character, 「と」. However, the important point here is that by using this casual shortcut, you can drop the rest of the sentence and hope your audience can understand everything from context.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomoko,
                              inlineSpan: const TextSpan(
                                text: '智子',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: rainen,
                              inlineSpan: const TextSpan(
                                text: '来年',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kaigai,
                              inlineSpan: const TextSpan(
                                text: '海外',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだ',
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Tomoko said that she’s going overseas next year.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I already told you I have no money.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'え？',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ',
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Huh? What did you say?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞いた',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだけど、',
                            ),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I heard you don’t have time now, is that true?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You don’t have time now (I heard), is that true?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「って」 can also be used to talk about practically anything, not just to quote something that was said. You can hear 「って」 being used just about everywhere in casual speech. Most of the time it is used in place of the 「は」 particle to simply bring up a topic.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: furu,
                              inlineSpan: const TextSpan(
                                text: '降るん',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ',
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'About tomorrow, I hear that it’s going to rain.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリス',
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sugoi,
                              inlineSpan: const TextSpan(
                                text: 'すごく',
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'でしょ？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'About Alice, she’s a very good person, right?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 智子 【とも・こ】 – Tomoko (first name) 来年 【らい・ねん】 – next year 海外 【かい・がい】 – overseas もう – already お金 【お・かね】 – money ある (u-verb) – to exist (inanimate) 本当 【ほん・とう】 – real 明日 【あした】 – tomorrow 雨 【あめ】 – rain 降る 【ふ・る】(u-verb) – to precipitate すごい (i-adj) – to a great extent いい (i-adj) – good 人 【ひと】 – person You may be surprised to hear that there is a shorter and casual version of the quoted relative clause since it’s already only one hiragana character, 「と」. However, the important point here is that by using this casual shortcut, you can drop the rest of the sentence and hope your audience can understand everything from context. 智子は来年、海外に行くんだって。 Tomoko said that she’s going overseas next year. もうお金がないって。 I already told you I have no money. え？ 何だって？ Huh? What did you say? 今、時間がないって聞いたんだけど、本当？ I heard you don’t have time now, is that true? 今、時間がないって、本当？ You don’t have time now (I heard), is that true? 「って」 can also be used to talk about practically anything, not just to quote something that was said. You can hear 「って」 being used just about everywhere in casual speech. Most of the time it is used in place of the 「は」 particle to simply bring up a topic. 明日って、雨が降るんだって。 About tomorrow, I hear that it’s going to rain. アリスって、すごくいい人でしょ？ About Alice, she’s a very good person, right?',
      grammars: ['って'],
      keywords: [],
    ),
  ],
);
