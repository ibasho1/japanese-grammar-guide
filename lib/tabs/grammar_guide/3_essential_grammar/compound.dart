import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson compound = Lesson(
  title: 'Compound Sentences',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this section, we will learn various ways to combine multiple simple sentences into one complex sentence. For example, we will learn how to chain separate sentences together to express multiple actions or states. In other words, if we have two simple sentences with the same subject, “I ran” and “I ate”, we will learn how to group them together to mean, “I ran and ate.” We will also learn how to do this with adjectives and nouns. (Ex: He is rich, handsome, and charming.)'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this section, we will learn various ways to combine multiple simple sentences into one complex sentence. For example, we will learn how to chain separate sentences together to express multiple actions or states. In other words, if we have two simple sentences with the same subject, “I ran” and “I ate”, we will learn how to group them together to mean, “I ran and ate.” We will also learn how to do this with adjectives and nouns. (Ex: He is rich, handsome, and charming.)',
      grammars: [],
      keywords: ['compound','and'],
    ),
    Section(
      heading: 'Expressing a sequence of states',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('一般的 【いっ・ぱん・てき】 – in general'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('狭い 【せま・い】 (i-adj) – narrow'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('いい (i-adj) – good'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('部屋 【へ・や】 – room'),
                  Text('きれい (na-adj) – pretty; clean'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('学生 【がく・せい】 – student'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                  Text('お金持ち 【お・かね・も・ち】 – rich'),
                  Text('かっこいい (i-adj) – cool; handsome'),
                  Text('魅力的 【み・りょく・てき】 – charming'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'It is very easy to combine a chain of nouns and adjectives to describe a person or object. For example, in English if we wanted to say, “He is X. He is Y. He is Z.” since all three sentences have the same noun, we would usually say, “He is X, Y, and Z.” In Japanese, we can do the same thing by conjugating the noun or adjective. The last noun or adjective remains the same as before.'),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For nouns and na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「で」 to the noun or na-adjective.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: ippanteki,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '一般的',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: ippanteki,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '一般的',
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'で',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shizuka,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '静か',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shizuka,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '静か',
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'で',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    'For i-adjectives and negative noun/adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace the 「い」 with 「くて」.',
                              ),
                            ],
                          ),
                        ),
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: '※For 「',
                              ),
                              VocabTooltip(
                                message: ii,
                                inlineSpan: const TextSpan(
                                  text: 'いい',
                                ),
                              ),
                              const TextSpan(
                                text: '」 and 「',
                              ),
                              VocabTooltip(
                                message: kakkoii,
                                inlineSpan: const TextSpan(
                                  text: 'かっこいい',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    '」, the 「い→よ」 exception applies here as well.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: semai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '狭',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: semai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '狭',
                                        ),
                                        TextSpan(
                                          text: 'くて',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kanojo,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '彼女',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃな',
                                  ),
                                  TextSpan(
                                    text: 'い',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kanojo,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '彼女',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'じゃな',
                                  ),
                                  TextSpan(
                                    text: 'くて',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: ii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'いい',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: ii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'よくて',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'How to chain nouns and adjectives together',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: kirei,
                              inlineSpan: const TextSpan(
                                text: 'きれい',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My room is clean, quiet, and I like it a lot.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'じゃな',
                            ),
                            TextSpan(
                              text: 'くて',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'She is not a student, she is a teacher.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tanaka,
                              inlineSpan: const TextSpan(
                                text: '田中',
                              ),
                            ),
                            const TextSpan(
                              text: 'さんは、',
                            ),
                            VocabTooltip(
                              message: okanemochi,
                              inlineSpan: const TextSpan(
                                text: 'お金持ち',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kakkoii,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'かっこ',
                                  ),
                                  TextSpan(
                                    text: 'よくて',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: miryokuteki,
                              inlineSpan: const TextSpan(
                                text: '魅力的',
                              ),
                            ),
                            const TextSpan(
                              text: 'ですね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Tanaka-san is rich, handsome, and charming, isn’t he?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'As you can see, the 「で」 attached to 「'),
                  VocabTooltip(
                    message: okanemochi,
                    inlineSpan: const TextSpan(
                      text: 'お金持ち',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 obviously cannot be the context particle 「で」 here because there is no verb. It might be helpful to think of 「で」 as merely a substitution for 「だ」 that can be chained together.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 一般的 【いっ・ぱん・てき】 – in general 静か 【しず・か】 (na-adj) – quiet 狭い 【せま・い】 (i-adj) – narrow 彼女 【かの・じょ】 – she; girlfriend いい (i-adj) – good 私 【わたし】 – me; myself; I 部屋 【へ・や】 – room きれい (na-adj) – pretty; clean 好き 【す・き】 (na-adj) – likable; desirable 学生 【がく・せい】 – student 先生 【せん・せい】 – teacher 田中 【た・なか】 – Tanaka (last name) お金持ち 【お・かね・も・ち】 – rich かっこいい (i-adj) – cool; handsome 魅力的 【み・りょく・てき】 – charming It is very easy to combine a chain of nouns and adjectives to describe a person or object. For example, in English if we wanted to say, “He is X. He is Y. He is Z.” since all three sentences have the same noun, we would usually say, “He is X, Y, and Z.” In Japanese, we can do the same thing by conjugating the noun or adjective. The last noun or adjective remains the same as before. How to chain nouns and adjectives together For nouns and na-adjectives: Attach 「で」 to the noun or na-adjective. Examples: 一般的 → 一般的で静か → 静かで For i-adjectives and negative noun/adjectives: Replace the 「い」 with 「くて」.※For 「いい」 and 「かっこいい」, the 「い→よ」 exception applies here as well. Examples: 狭い → 狭くて彼女じゃない → 彼女じゃなくていい → よくて Examples 私の部屋は、きれいで、静かで、とても好き。 My room is clean, quiet, and I like it a lot. 彼女は、学生じゃなくて、先生だ。 She is not a student, she is a teacher. 田中さんは、お金持ちで、かっこよくて、魅力的ですね。 Tanaka-san is rich, handsome, and charming, isn’t he? As you can see, the 「で」 attached to 「お金持ち」 obviously cannot be the context particle 「で」 here because there is no verb. It might be helpful to think of 「で」 as merely a substitution for 「だ」 that can be chained together.',
      grammars: ['で'],
      keywords: ['de'],
    ),
    Section(
      heading: 'Expressing a sequence of verbs with the te-form',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('学生 【がく・せい】 – student'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('する (exception) – to do'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('食堂 【しょく・どう】 – cafeteria'),
                  Text('昼ご飯 【ひる・ご・はん】 – lunch'),
                  Text('昼寝 【ひる・ね】 – afternoon nap'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In a similar fashion, you can express multiple actions. It is usually interpreted as a sequence of event. (I did [X], then I did [Y], then I finally did [Z].) There are two forms: positive and negative. The tense of all the actions is determined by the tense of the last verb.'),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'Positive:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' Conjugate the verb to its past tense and replace 「た」 with 「て」 or 「だ」 with 「で」. This is often called the ',
                          ),
                          TextSpan(
                            text: 'te-form:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                          TextSpan(
                            text: ' even though it could sometimes be ‘de’.',
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Negative:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Same as i-adjectives, replace 「い」 with 「くて」. This rule also works for the polite 「です」 and 「ます」 endings. ',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'で',
                                  ),
                                  TextSpan(
                                    text: 'す',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '学生',
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'でし',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'た',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'でし',
                                  ),
                                  TextSpan(
                                    text: 'て',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kau,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '買いま',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kau,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '買いま',
                                        ),
                                        TextSpan(
                                          text: 'し',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kau,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '買いま',
                                        ),
                                        const TextSpan(
                                          text: 'し',
                                        ),
                                        TextSpan(
                                          text: 'て',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'How to chain verbs together',
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample conjugations'),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Table(
                          border: TableBorder.all(
                            color: Theme.of(context).colorScheme.outline,
                          ),
                          children: [
                            const TableRow(
                              children: [
                                TableHeading(text: 'Past Tense'),
                                TableHeading(text: 'Te-form'),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '食べ',
                                          ),
                                          TextSpan(
                                            text: 'た',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '食べ',
                                          ),
                                          TextSpan(
                                            text: 'て',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '行っ',
                                          ),
                                          TextSpan(
                                            text: 'た',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '行っ',
                                          ),
                                          TextSpan(
                                            text: 'て',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'し',
                                          ),
                                          TextSpan(
                                            text: 'た',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'し',
                                          ),
                                          TextSpan(
                                            text: 'て',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '遊ん',
                                          ),
                                          TextSpan(
                                            text: 'だ',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '遊ん',
                                          ),
                                          TextSpan(
                                            text: 'で',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '飲ん',
                                          ),
                                          TextSpan(
                                            text: 'だ',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '飲ん',
                                          ),
                                          TextSpan(
                                            text: 'で',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Table(
                          border: TableBorder.all(
                            color: Theme.of(context).colorScheme.outline,
                          ),
                          children: [
                            const TableRow(
                              children: [
                                TableHeading(text: 'Negative'),
                                TableHeading(text: 'Te-form'),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '食べな',
                                          ),
                                          TextSpan(
                                            text: 'い',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: taberu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '食べな',
                                          ),
                                          TextSpan(
                                            text: 'くて',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '行かな',
                                          ),
                                          TextSpan(
                                            text: 'い',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: iku,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '行かな',
                                          ),
                                          TextSpan(
                                            text: 'くて',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'しな',
                                          ),
                                          TextSpan(
                                            text: 'い',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: 'しな',
                                          ),
                                          TextSpan(
                                            text: 'くて',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '遊ばな',
                                          ),
                                          TextSpan(
                                            text: 'い',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: asobu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '遊ばな',
                                          ),
                                          TextSpan(
                                            text: 'くて',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '飲まな',
                                          ),
                                          TextSpan(
                                            text: 'い',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TableData(
                                  content: Text.rich(
                                    VocabTooltip(
                                      message: nomu,
                                      inlineSpan: TextSpan(
                                        children: [
                                          const TextSpan(
                                            text: '飲まな',
                                          ),
                                          TextSpan(
                                            text: 'くて',
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shokudou,
                              inlineSpan: const TextSpan(
                                text: '食堂',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hirugohan,
                              inlineSpan: const TextSpan(
                                text: '昼ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hirune,
                              inlineSpan: const TextSpan(
                                text: '昼寝',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I will go to cafeteria, eat lunch, and take a nap.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shokudou,
                              inlineSpan: const TextSpan(
                                text: '食堂',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hirugohan,
                              inlineSpan: const TextSpan(
                                text: '昼ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hirune,
                              inlineSpan: const TextSpan(
                                text: '昼寝',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I went to cafeteria, ate lunch, and took a nap.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ありまして',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見ました',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'There was time and I watched a movie.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 学生 【がく・せい】 – student 買う 【か・う】 (u-verb) – to buy 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go する (exception) – to do 遊ぶ 【あそ・ぶ】 (u-verb) – to play 飲む 【の・む】 (u-verb) – to drink 食堂 【しょく・どう】 – cafeteria 昼ご飯 【ひる・ご・はん】 – lunch 昼寝 【ひる・ね】 – afternoon nap 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see In a similar fashion, you can express multiple actions. It is usually interpreted as a sequence of event. (I did [X], then I did [Y], then I finally did [Z].) There are two forms: positive and negative. The tense of all the actions is determined by the tense of the last verb. How to chain verbs together Positive: Conjugate the verb to its past tense and replace 「た」 with 「て」 or 「だ」 with 「で」. This is often called the te-form: even though it could sometimes be ‘de’. Negative: Same as i-adjectives, replace 「い」 with 「くて」. This rule also works for the polite 「です」 and 「ます」 endings. Examples: 学生です → 学生でした → 学生でして買います → 買いました → 買いまして Examples 食堂に行って、昼ご飯を食べて、昼寝をする。 I will go to cafeteria, eat lunch, and take a nap. 食堂に行って、昼ご飯を食べて、昼寝をした。 I went to cafeteria, ate lunch, and took a nap. 時間がありまして、映画を見ました。 There was time and I watched a movie.',
      grammars: ['～て'],
      keywords: ['te-form','te'],
    ),
    Section(
      heading: 'Expressing reason or causation using 「から」 and 「ので」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('パーティー – party'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('プレゼント – present'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                  Text('どうして – why'),
                  Text('山田 【や・まだ】 – Yamada (last name)'),
                  Text('直子 【なお・こ】 – Naoko (first name)'),
                  Text('ちょっと – a little'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('そろそろ – gradually; soon'),
                  Text('失礼 【しつ・れい】 – discourtesy'),
                  Text('する (exception) – to do'),
                  Text('学生 【がく・せい】 – student'),
                  Text('お金 【お・かね】 – money'),
                  Text('ここ – here'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('とても – very'),
                  Text('穏やか 【おだ・やか】 (na-adj) – calm, peaceful'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You can connect two complete sentences using 「から」 to indicate a reason for something. The two sentences are always ordered [reason] から [result]. When the reason is a non-conjugated noun or na-adjective, you '),
                  TextSpan(
                    text: 'must',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' add 「だ」 to explicitly declare the reason in the form of 「(noun/na-adjective)'),
                  TextSpan(
                    text: 'だ',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                      text:
                          'から」. If you forget to add the declarative 「だ」 to 「から」, it will end up sounding like the 「から」 meaning “from” which was first introduced in the section on particles, earlier.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'なかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'から',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: paatii,
                              inlineSpan: const TextSpan(
                                text: 'パーティー',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きませんでした',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'There was no time so didn’t go to party.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            TextSpan(
                              text: 'から',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: '来た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Present came ',
                            ),
                            TextSpan(
                              text: 'from',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' friend.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            TextSpan(
                              text: 'だから',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: '来た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Present came ',
                            ),
                            TextSpan(
                              text: 'because',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text:
                                  ' (the person is) friend. (This sentence sounds a bit odd.)',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Either the reason or the result can be omitted if it is clear from the context. In the case of polite speech, you would treat 「から」 just like a regular noun and add 「です」.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tanaka,
                            inlineSpan: const TextSpan(text: '田中'),
                          ),
                          const TextSpan(
                            text: 'さん',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: doushite,
                            inlineSpan: const TextSpan(text: 'どうして'),
                          ),
                          VocabTooltip(
                            message: paatii,
                            inlineSpan: const TextSpan(text: 'パーティー'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行きませんでしたか'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Tanaka-san'),
                    text: Text('Why didn’t you go to the party?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yamada,
                            inlineSpan: const TextSpan(text: '山田'),
                          ),
                          const TextSpan(
                            text: 'さん',
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(text: '時間'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(text: 'なかった'),
                          ),
                          TextSpan(
                            text: 'から',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'です。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Yamada-san'),
                    text: Text('It’s because I didn’t have time.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ichirou,
                            inlineSpan: const TextSpan(text: '一郎'),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: paatii,
                            inlineSpan: const TextSpan(text: 'パーティー'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行かなかった'),
                          ),
                          const TextSpan(
                            text: 'の？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Ichiro'),
                    text: Text('You didn’t go to the party?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: naoko,
                            inlineSpan: const TextSpan(text: '直子'),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(text: 'うん'),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(text: '時間'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(text: 'なかった'),
                          ),
                          TextSpan(
                            text: 'から',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Naoko'),
                    text: Text('Yeah, because I didn’t have time.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'When you omit the reason, you must include the declarative 「だ」 or 「です」.'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: naoko,
                            inlineSpan: const TextSpan(text: '直子'),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(text: '時間'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(text: 'なかった'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Naoko'),
                    text: Text('I didn’t have time.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ichirou,
                            inlineSpan: const TextSpan(text: '一郎'),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'だから',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: paatii,
                            inlineSpan: const TextSpan(text: 'パーティー'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行かなかった'),
                          ),
                          const TextSpan(
                            text: 'の？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Ichiro'),
                    text: Text('Is that why you didn’t go to the party?'),
                    english: true,
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Notice that we could have also used the explanatory 「の」 to express the same thing. In other words, '),
                  VocabTooltip(
                    message: tanaka,
                    inlineSpan: const TextSpan(text: '山田'),
                  ),
                  const TextSpan(text: 'さん could have also said, 「'),
                  VocabTooltip(
                    message: jikan,
                    inlineSpan: const TextSpan(text: '時間'),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'なかった'),
                  ),
                  const TextSpan(text: 'のです」 or 「'),
                  VocabTooltip(
                    message: jikan,
                    inlineSpan: const TextSpan(text: '時間'),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'なかった'),
                  ),
                  const TextSpan(text: 'んです」 while '),
                  VocabTooltip(
                    message: naoko,
                    inlineSpan: const TextSpan(text: '直子'),
                  ),
                  const TextSpan(text: ' could have said 「'),
                  VocabTooltip(
                    message: jikan,
                    inlineSpan: const TextSpan(text: '時間'),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'なかった'),
                  ),
                  const TextSpan(
                      text:
                          'の」 (we’ll assume she wants to use the more feminine form). In fact, this is where 「ので」 possibly came from. Let’s say you want to combine two sentences: 「'),
                  VocabTooltip(
                    message: jikan,
                    inlineSpan: const TextSpan(text: '時間'),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'なかった'),
                  ),
                  const TextSpan(text: 'のだ」 and 「'),
                  VocabTooltip(
                    message: paatii,
                    inlineSpan: const TextSpan(text: 'パーティー'),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(text: '行かなかった'),
                  ),
                  const TextSpan(
                      text:
                          '」. Remember we can treat the 「の」 just like a noun so we can use what we just learned in the first section of this lesson. '),
                ],
              ),
              Padding(
                padding: kDefaultListPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(text: '時間'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(text: 'なかった'),
                          ),
                          const TextSpan(
                            text: 'のだ',
                          ),
                        ],
                      ),
                    ),
                    const Text('＋'),
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: paatii,
                            inlineSpan: const TextSpan(text: 'パーティー'),
                          ),
                          const TextSpan(text: 'に'),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行かなかった'),
                          ),
                        ],
                      ),
                    ),
                    const Text('becomes:'),
                    Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(text: '時間'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(text: 'なかった'),
                          ),
                          TextSpan(
                            text: 'ので',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: paatii,
                            inlineSpan: const TextSpan(text: 'パーティー'),
                          ),
                          const TextSpan(text: 'に'),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行かなかった'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In fact, 「ので」 is almost interchangeable with 「から」 with a few subtle differences. 「から」 explicitly states that the sentence preceding is the reason for something while 「ので」 is merely putting two sentences together, the first with an explanatory tone. This is something I call causation where [X] happened, therefore [Y] happened. This is slightly different from 「から」 where [Y] happened explicitly '),
                  TextSpan(
                    text: 'because',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                      text:
                          ' [X] happened. This difference tends to make 「ので」 sound softer and slightly more polite and it is favored over 「から」 when explaining a reason for doing something that is considered discourteous.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            TextSpan(
                              text: 'ので',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sorosoro,
                              inlineSpan: const TextSpan(
                                text: 'そろそろ',
                              ),
                            ),
                            VocabTooltip(
                              message: shitsurei,
                              inlineSpan: const TextSpan(
                                text: '失礼',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'します',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Because I’m a little busy, I’ll be making my leave soon.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: '（「'),
                  VocabTooltip(
                    message: shitsurei,
                    inlineSpan: const TextSpan(
                      text: '失礼',
                    ),
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'します',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, which literally means “I’m doing a discourtesy”, is commonly used as a polite way to make your leave or disturb someone’s time.）',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Reminder:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  VocabTooltip(
                    message: shitsurei,
                    inlineSpan: const TextSpan(
                      text:
                          ' Don’t forget that the explanatory 「の」 requires a 「な」 for both non-conjugated nouns and na-adjectives. Review Noun-related Particles to see why.',
                    ),
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'ので、',
                            ),
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'んです。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Because I’m a student, I have no money (lit: there is no money).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'ので、',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: odayaka,
                              inlineSpan: const TextSpan(
                                text: '穏やか',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It is very calm here because it is quiet.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'ので、',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(text: '会う'),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(text: 'ない'),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That’s why there’s no time to meet friend.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Just like how the explanatory 「の」 can be shortened to 「ん」, in speech, the 「ので」 can be changed to 「んで」 simply because it’s easier to slur the sounds together rather than pronouncing the / o / syllable.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なかった',
                              ),
                            ),
                            TextSpan(
                              text: 'んで',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: paatii,
                              inlineSpan: const TextSpan(
                                text: 'パーティー',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かなかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Didn’t go to the party because there was no time.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'んで',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: odayaka,
                              inlineSpan: const TextSpan(
                                text: '穏やか',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It is very calm here because it is quiet.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'なんで',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(text: '会う'),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(text: 'ない'),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That’s why there’s no time to meet friend.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) パーティー – party 行く 【い・く】 (u-verb) – to go 友達 【とも・だち】 – friend プレゼント – present 来る 【く・る】 (exception) – to come 田中 【た・なか】 – Tanaka (last name) どうして – why 山田 【や・まだ】 – Yamada (last name) 直子 【なお・こ】 – Naoko (first name) ちょっと – a little 忙しい 【いそが・しい】 (i-adj) – busy そろそろ – gradually; soon 失礼 【しつ・れい】 – discourtesy する (exception) – to do 学生 【がく・せい】 – student お金 【お・かね】 – money ここ – here 静か 【しず・か】 (na-adj) – quiet とても – very 穏やか 【おだ・やか】 (na-adj) – calm, peaceful 会う 【あ・う】 (u-verb) – to meet You can connect two complete sentences using 「から」 to indicate a reason for something. The two sentences are always ordered [reason] から [result]. When the reason is a non-conjugated noun or na-adjective, you must add 「だ」 to explicitly declare the reason in the form of 「(noun/na-adjective)だから」. If you forget to add the declarative 「だ」 to 「から」, it will end up sounding like the 「から」 meaning “from” which was first introduced in the section on particles, earlier. Examples 時間がなかったからパーティーに行きませんでした。 There was no time so didn’t go to party. 友達からプレゼントが来た。 Present came from friend. 友達だからプレゼントが来た。 Present came because (the person is) friend. (This sentence sounds a bit odd.) Either the reason or the result can be omitted if it is clear from the context. In the case of polite speech, you would treat 「から」 just like a regular noun and add 「です」. Example 1 田中さん：どうしてパーティーに行きませんでしたか。 Tanaka-san: Why didn’t you go to the party? 山田さん：時間がなかったからです。 Yamada-san: It’s because I didn’t have time. Example 2 一郎：パーティーに行かなかったの？ Ichiro: You didn’t go to the party? 直子：うん、時間がなかったから。 Naoko: Yeah, because I didn’t have time. Example 3 When you omit the reason, you must include the declarative 「だ」 or 「です」. 直子：時間がなかった。 Naoko: I didn’t have time. 一郎：だからパーティーに行かなかったの？ Ichiro: Is that why you didn’t go to the party? Notice that we could have also used the explanatory 「の」 to express the same thing. In other words, 山田さん could have also said, 「時間がなかったのです」 or 「時間がなかったんです」 while 直子 could have said 「時間がなかったの」 (we’ll assume she wants to use the more feminine form). In fact, this is where 「ので」 possibly came from. Let’s say you want to combine two sentences: 「時間がなかったのだ」 and 「パーティーに行かなかった」. Remember we can treat the 「の」 just like a noun so we can use what we just learned in the first section of this lesson. 時間がなかったのだ＋パーティーに行かなかった becomes: 時間がなかったのでパーティーに行かなかった。 In fact, 「ので」 is almost interchangeable with 「から」 with a few subtle differences. 「から」 explicitly states that the sentence preceding is the reason for something while 「ので」 is merely putting two sentences together, the first with an explanatory tone. This is something I call causation where [X] happened, therefore [Y] happened. This is slightly different from 「から」 where [Y] happened explicitly because [X] happened. This difference tends to make 「ので」 sound softer and slightly more polite and it is favored over 「から」 when explaining a reason for doing something that is considered discourteous. ちょっと忙しいので、そろそろ失礼します。 Because I’m a little busy, I’ll be making my leave soon. （「失礼します」, which literally means “I’m doing a discourtesy”, is commonly used as a polite way to make your leave or disturb someone’s time.） Reminder: Don’t forget that the explanatory 「の」 requires a 「な」 for both non-conjugated nouns and na-adjectives. Review Noun-related Particles to see why. 私は学生なので、お金がないんです。 Because I’m a student, I have no money (lit: there is no money). ここは静かなので、とても穏やかです。 It is very calm here because it is quiet. なので、友達に会う時間がない。 That’s why there’s no time to meet friend. Just like how the explanatory 「の」 can be shortened to 「ん」, in speech, the 「ので」 can be changed to 「んで」 simply because it’s easier to slur the sounds together rather than pronouncing the / o / syllable. 時間がなかったんでパーティーに行かなかった。 Didn’t go to the party because there was no time. ここは静かんで、とても穏やかです。 It is very calm here because it is quiet. なんで友達に会う時間がない。 That’s why there’s no time to meet friend.',
      grammars: ['から','ので'],
      keywords: ['kara','node','particle','because'],
    ),
    Section(
      heading: 'Using 「のに」 to mean “despite”',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('運動 【うん・どう】 – exercise'),
                  Text('する (exception) – to do'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('痩せる 【や・せる】 (ru-verb) – to become thin'),
                  Text('学生 【がく・せい】 – student'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('勉強 【べん・きょう】 – study'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Grammatically, 「のに」 is used exactly the same way as 「ので」. When used to combine two simple sentences together, it means “[Sentence 1] despite the fact that [Sentence 2].” However the order is reversed: [Sentence 2]のに[Sentence 1].'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: '運動',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            TextSpan(
                              text: 'のに',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: zenzen,
                              inlineSpan: const TextSpan(
                                text: '全然',
                              ),
                            ),
                            VocabTooltip(
                              message: yaseru,
                              inlineSpan: const TextSpan(
                                text: '痩せなかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Despite exercising every day, I didn’t get thinner.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            TextSpan(
                              text: 'のに',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Despite being a student, she does not study.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 毎日 【まい・にち】 – everyday 運動 【うん・どう】 – exercise する (exception) – to do 全然 【ぜん・ぜん】 – not at all (when used with negative) 痩せる 【や・せる】 (ru-verb) – to become thin 学生 【がく・せい】 – student 彼女 【かの・じょ】 – she; girlfriend 勉強 【べん・きょう】 – study Grammatically, 「のに」 is used exactly the same way as 「ので」. When used to combine two simple sentences together, it means “[Sentence 1] despite the fact that [Sentence 2].” However the order is reversed: [Sentence 2]のに[Sentence 1]. Examples 毎日運動したのに、全然痩せなかった。 Despite exercising every day, I didn’t get thinner. 学生なのに、彼女は勉強しない。 Despite being a student, she does not study.',
      grammars: ['のに'],
      keywords: ['noni','particle','despite'],
    ),
    Section(
      heading: 'Expressing contradiction using 「が」 and 「けど」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('デパート – department store'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('何 【なに／なん】 – what'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('欲しい 【ほ・しい】 (i-adj) – desirable'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('今日 【きょう】 – today'),
                  Text('暇 【ひま】 – free　(as in not busy)'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('まだ – yet'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('いい (i-adj) – good'),
                  Text('物 【もの】 – object'),
                  Text('たくさん – a lot (amount)'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('面白い 【おも・しろ・い】(i-adj) – interesting'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Used in the same manner as 「から」 and 「ので」, 「が」 and 「けど」 also connect two sentences together but this time to express a contradiction. Just like 「から」 the declarative 「だ」 is required for nouns and na-adjectives. And just like 「から」 and 「ので」, either part of the contradiction can be left out.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: depaato,
                              inlineSpan: const TextSpan(
                                text: 'デパート',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きました',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: const TextSpan(
                                text: '欲しくなかった',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I went to department store but there was nothing I wanted.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞いた',
                              ),
                            ),
                            TextSpan(
                              text: 'けど',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: shiru,
                              inlineSpan: const TextSpan(
                                text: '知らなかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'I asked (or heard from) a friend but he (or I) didn’t know.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: hima,
                              inlineSpan: const TextSpan(
                                text: '暇',
                              ),
                            ),
                            TextSpan(
                              text: 'だけど',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I’m free today but I will be busy tomorrow.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: hima,
                              inlineSpan: const TextSpan(
                                text: '暇',
                              ),
                            ),
                            TextSpan(
                              text: 'だけど',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I’m free today but I will be busy tomorrow.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'だけど',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: mada,
                              inlineSpan: const TextSpan(
                                text: 'まだ',
                              ),
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(
                              text: 'なの。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'That may be so, but it is that I still like him. [explanation, feminine tone]'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'It may seem odd but 「'),
                  VocabTooltip(
                    message: kiku,
                    inlineSpan: const TextSpan(
                      text: '聞く',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 can either mean “to listen” or “to ask”. You may think this may become confusing but the meaning is usually clear within context. In the second example, we’re assuming that the friend didn’t know, so the speaker was probably asking the friend. Yet again we see the importance of context in Japanese because this sentence can also mean, “I heard from a friend but I didn’t know” since there is neither subject nor topic.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Similar to the difference between 「ので」 and 「から」, 「が」 has a softer tone and is slightly more polite than 「けど」. Though this isn’t a rule as such, it is generally common to see 「が」 attached to a 「～ます」 or 「～です」 ending and 「けど」 attached to a regular, plain ending. A more formal version of 「けど」 is 「けれど」 and even more formal is 「けれども」, which we may see later when we cover formal expressions.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Unlike the English word for contradiction such as “but” or “however”, 「けど」 and 「が」 do not always express a direct contradiction. Often times, especially when introducing a new topic, it is used as a general connector of two separate sentences. For example, in the following sentences, there is no actual contradiction but 「が」 and 「けど」 are used simply to connect the sentences. Sometimes, the English “and” becomes a closer translation than “but”.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: depaato,
                              inlineSpan: const TextSpan(
                                text: 'デパート',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きました',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: '物',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ありました',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'I went to the department store and there was a lot of good stuff.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: 'マトリックス',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            TextSpan(
                              text: 'けど',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: omoshiroi,
                              inlineSpan: const TextSpan(
                                text: '面白かった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'I watched the “Matrix” and it was interesting.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary デパート – department store 行く 【い・く】 (u-verb) – to go 何 【なに／なん】 – what 全然 【ぜん・ぜん】 – not at all (when used with negative) 欲しい 【ほ・しい】 (i-adj) – desirable 友達 【とも・だち】 – friend 聞く 【き・く】 (u-verb) – to ask; to listen 知る 【し・る】 (u-verb) – to know 今日 【きょう】 – today 暇 【ひま】 – free　(as in not busy) 明日 【あした】 – tomorrow 忙しい 【いそが・しい】 (i-adj) – busy 彼 【かれ】 – he; boyfriend まだ – yet 好き 【す・き】 (na-adj) – likable; desirable いい (i-adj) – good 物 【もの】 – object たくさん – a lot (amount) ある (u-verb) – to exist (inanimate) 見る 【み・る】 (ru-verb) – to see 面白い 【おも・しろ・い】(i-adj) – interesting Used in the same manner as 「から」 and 「ので」, 「が」 and 「けど」 also connect two sentences together but this time to express a contradiction. Just like 「から」 the declarative 「だ」 is required for nouns and na-adjectives. And just like 「から」 and 「ので」, either part of the contradiction can be left out. Examples デパートに行きましたが、何も欲しくなかったです。 I went to department store but there was nothing I wanted. 友達に聞いたけど、知らなかった。 I asked (or heard from) a friend but he (or I) didn’t know. 今日は暇だけど、明日は忙しい。 I’m free today but I will be busy tomorrow. 今日は暇だけど、明日は忙しい。 I’m free today but I will be busy tomorrow. だけど、彼がまだ好きなの。 That may be so, but it is that I still like him. [explanation, feminine tone] It may seem odd but 「聞く」 can either mean “to listen” or “to ask”. You may think this may become confusing but the meaning is usually clear within context. In the second example, we’re assuming that the friend didn’t know, so the speaker was probably asking the friend. Yet again we see the importance of context in Japanese because this sentence can also mean, “I heard from a friend but I didn’t know” since there is neither subject nor topic. Similar to the difference between 「ので」 and 「から」, 「が」 has a softer tone and is slightly more polite than 「けど」. Though this isn’t a rule as such, it is generally common to see 「が」 attached to a 「～ます」 or 「～です」 ending and 「けど」 attached to a regular, plain ending. A more formal version of 「けど」 is 「けれど」 and even more formal is 「けれども」, which we may see later when we cover formal expressions. Unlike the English word for contradiction such as “but” or “however”, 「けど」 and 「が」 do not always express a direct contradiction. Often times, especially when introducing a new topic, it is used as a general connector of two separate sentences. For example, in the following sentences, there is no actual contradiction but 「が」 and 「けど」 are used simply to connect the sentences. Sometimes, the English “and” becomes a closer translation than “but”. デパートに行きましたが、いい物がたくさんありました。 I went to the department store and there was a lot of good stuff. マトリックスを見たけど、面白かった。 I watched the “Matrix” and it was interesting.',
      grammars: ['が','けど'],
      keywords: ['but','however','ga','kedo','particle'],
    ),
    Section(
      heading: 'Expressing multiple reasons using 「し」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('どうして – why'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('年上 【とし・うえ】 – older'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('好き 【す・き】 (na-adj) – likable'),
                  Text('優しい 【やさ・しい】 (i-adj) – gentle; kind'),
                  Text('かっこいい (i-adj) – cool; handsome'),
                  Text('面白い 【おも・し・ろい】 (i-adj) – interesting'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'When you want to list reasons for multiple states or actions you can do so by adding 「し」 to the end of each relative clause. It is very similar to the 「や」 particle except that it lists reasons for verbs and state-of-being. Again, for states of being, 「だ」 must be used to explicitly declare the state-of-being for any non-conjugated noun or na-adjective. Let’s look at some examples.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: doushite,
                            inlineSpan: const TextSpan(text: 'どうして'),
                          ),
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'じゃないんですか？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text(
                        'Why isn’t (he/she) friend [seeking explanation]?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sensei,
                            inlineSpan: const TextSpan(text: '先生'),
                          ),
                          TextSpan(
                            text: 'だし',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: toshiue,
                            inlineSpan: const TextSpan(text: '年上'),
                          ),
                          TextSpan(
                            text: 'だし',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '・・・。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Well, he’s/she’s the teacher, and older…'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: doushite,
                            inlineSpan: const TextSpan(text: 'どうして'),
                          ),
                          VocabTooltip(
                            message: kare,
                            inlineSpan: const TextSpan(text: '彼'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: suki,
                            inlineSpan: const TextSpan(text: '好き'),
                          ),
                          const TextSpan(
                            text: 'なの？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Why (do you) like him?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yasashii,
                            inlineSpan: const TextSpan(text: '優しい'),
                          ),
                          TextSpan(
                            text: 'し',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kakkoii,
                            inlineSpan: const TextSpan(text: 'かっこいい'),
                          ),
                          TextSpan(
                            text: 'し',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: omoshiroi,
                            inlineSpan: const TextSpan(text: '面白い'),
                          ),
                          const TextSpan(
                            text: 'から。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'Because he’s kind, attractive, and interesting (among other things).'),
                    english: true,
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'Notice that 「'),
                  VocabTooltip(
                    message: yasashii,
                    inlineSpan: const TextSpan(text: '優しくて'),
                  ),
                  const TextSpan(text: '、'),
                  VocabTooltip(
                    message: kakkoii,
                    inlineSpan: const TextSpan(text: 'かっこよくて'),
                  ),
                  VocabTooltip(
                    message: omoshiroi,
                    inlineSpan: const TextSpan(text: '面白い'),
                  ),
                  const TextSpan(
                    text:
                        'から。」 could also have worked but much like the difference between the 「と」 and 「や」 particle, 「し」 implies that there may be other reasons.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary どうして – why 友達 【とも・だち】 – friend 先生 【せん・せい】 – teacher 年上 【とし・うえ】 – older 彼 【かれ】 – he; boyfriend 好き 【す・き】 (na-adj) – likable 優しい 【やさ・しい】 (i-adj) – gentle; kind かっこいい (i-adj) – cool; handsome 面白い 【おも・し・ろい】 (i-adj) – interesting When you want to list reasons for multiple states or actions you can do so by adding 「し」 to the end of each relative clause. It is very similar to the 「や」 particle except that it lists reasons for verbs and state-of-being. Again, for states of being, 「だ」 must be used to explicitly declare the state-of-being for any non-conjugated noun or na-adjective. Let’s look at some examples. Example 1 Ａ：どうして友達じゃないんですか？ A: Why isn’t (he/she) friend [seeking explanation]? Ｂ：先生だし、年上だし・・・。 B: Well, he’s/she’s the teacher, and older… Example 2 Ａ：どうして彼が好きなの？ A: Why (do you) like him? Ｂ：優しいし、かっこいいし面白いから。 B: Because he’s kind, attractive, and interesting (among other things). Notice that 「優しくて、かっこよくて面白いから。」 could also have worked but much like the difference between the 「と」 and 「や」 particle, 「し」 implies that there may be other reasons.',
      grammars: ['し'],
      keywords: ['shi','reason','list'],
    ),
    Section(
      heading: 'Expressing multiple actions or states using 「～たりする」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('難しい 【むずか・しい】 (i-adj) – difficult'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('本 【ほん】 – book'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('昼寝 【ひる・ね】 – afternoon nap'),
                  Text('この – this （abbr. of これの）'),
                  Text('大学 【だい・がく】 – college'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This is the verb version of the 「や」 particle. You can make an example list of verbs among a possible larger list by conjugating each verb into the past tense and adding 「り」. At the end, you need to attach the verb 「',
                  ),
                  VocabTooltip(message: suru, inlineSpan: const TextSpan(text: 'する'),),
                  const TextSpan(
                    text:
                        '」. Just like the 「や」 particle, the tense is determined by the last verb, which in this case will always be 「',
                  ),
                  VocabTooltip(message: suru, inlineSpan: const TextSpan(text: 'する'),),
                  const TextSpan(
                    text:
                        '」 (since you have to attach it at the end).',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'You can also use this with the state-of-being to say that you are a number of things at various random times among a larger list. Similar to regular verbs, you just take the noun or adjective for each state-of-being and conjugate it to the past state-of-being and then attach 「り」. Then finally, attach 「',
                  ),
                  VocabTooltip(message: suru, inlineSpan: const TextSpan(text: 'する'),),
                  const TextSpan(
                    text:
                        '」 at the end.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「で」 to the noun or na-adjective.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '飲',
                                        ),
                                        TextSpan(
                                          text: 'む',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '飲',
                                        ),
                                        TextSpan(
                                          text: 'んだ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        const TextSpan(
                                          text: 'た',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '飲',
                                        ),
                                        const TextSpan(
                                          text: 'んだ',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'たり',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '飲',
                                        ),
                                        TextSpan(
                                          text: 'んだり',
                                        ),
                                      ],
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For state-of-being:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Conjugate the noun or adjective for each state-of-being to the past tense and add 「り」. Finally, add 「する」 at the very end. ',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kantan,
                                    inlineSpan: const TextSpan(
                                      text: '簡単',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: yasashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '難し',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kantan,
                                    inlineSpan: const TextSpan(
                                      text: '簡単',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: yasashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '難し',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kantan,
                                    inlineSpan: const TextSpan(
                                      text: '簡単',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'だった',
                                  ),
                                  TextSpan(
                                    text: 'り',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: yasashii,
                                    inlineSpan: const TextSpan(
                                      text: '難しかった',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'り',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kantan,
                                    inlineSpan: const TextSpan(
                                      text: '簡単',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'だった',
                                  ),
                                  const TextSpan(
                                    text: 'り',
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: yasashii,
                                    inlineSpan: const TextSpan(
                                      text: '難しかった',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'り',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading:
                    'Rules for stating a list of verbs among a larger list using 「～たりする」',
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見たり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hon,
                              inlineSpan: const TextSpan(
                                text: '本',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読んだり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hirune,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'したり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I do things like (among other things) watch movies, read books, and take naps.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: const TextSpan(
                                text: '簡単',
                              ),
                            ),
                            TextSpan(
                              text: 'だったり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: muzukashii,
                              inlineSpan: const TextSpan(
                                text: '難しかったり',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Class of this college is sometimes easy, sometimes difficult (and other times something else maybe).',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'As you can see, the tense and negative/positive state is controlled by the last 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見たり',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hon,
                              inlineSpan: const TextSpan(
                                text: '本',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読んだり',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'した',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'I ',
                            ),
                            TextSpan(
                              text: 'did',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text:
                                  ' things like (among other things) watch movies, and read books.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見たり',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hon,
                              inlineSpan: const TextSpan(
                                text: '本',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読んだり',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'I ',
                            ),
                            TextSpan(
                              text: 'don’t do',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text:
                                  ' things like (among other things) watch movies, and read books.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見たり',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hon,
                              inlineSpan: const TextSpan(
                                text: '本',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読んだり',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'I ',
                            ),
                            TextSpan(
                              text: 'didn’t do',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text:
                                  ' things like (among other things) watch movies, and read books.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do 食べる 【た・べる】 (ru-verb) – to eat 飲む 【の・む】 (u-verb) – to drink 簡単 【かん・たん】 (na-adj) – simple 難しい 【むずか・しい】 (i-adj) – difficult 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see 本 【ほん】 – book 読む 【よ・む】 (u-verb) – to read 昼寝 【ひる・ね】 – afternoon nap この – this （abbr. of これの） 大学 【だい・がく】 – college 授業 【じゅ・ぎょう】 – class This is the verb version of the 「や」 particle. You can make an example list of verbs among a possible larger list by conjugating each verb into the past tense and adding 「り」. At the end, you need to attach the verb 「する」. Just like the 「や」 particle, the tense is determined by the last verb, which in this case will always be 「する」 (since you have to attach it at the end). You can also use this with the state-of-being to say that you are a number of things at various random times among a larger list. Similar to regular verbs, you just take the noun or adjective for each state-of-being and conjugate it to the past state-of-being and then attach 「り」. Then finally, attach 「する」 at the end. Rules for stating a list of verbs among a larger list using 「～たりする」 For verbs: Attach 「で」 to the noun or na-adjective. Example: 食べる、飲む → 食べた、飲んだ → 食べたり、飲んだり → 食べたり、飲んだりする For state-of-being: Conjugate the noun or adjective for each state-of-being to the past tense and add 「り」. Finally, add 「する」 at the very end. Example: 簡単、難しい → 簡単だった、難しかった → 簡単だったり、難しかったり → 簡単だったり、難しかったりする 映画を見たり、本を読んだり、静かしたりする。 I do things like (among other things) watch movies, read books, and take naps. この大学の授業は簡単だったり、難しかったりする。 Class of this college is sometimes easy, sometimes difficult (and other times something else maybe). As you can see, the tense and negative/positive state is controlled by the last 「する」. 映画を見たり、本を読んだりした。 I did things like (among other things) watch movies, and read books. 映画を見たり、本を読んだりしない。 I don’t do things like (among other things) watch movies, and read books. 映画を見たり、本を読んだりしない。 I didn’t do things like (among other things) watch movies, and read books.',
      grammars: ['～たりする'],
      keywords: ['tarisuru','list'],
    ),
  ],
);