import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson conditionals = Lesson(
  title: 'Conditionals',
  sections: [
    Section(
      heading: 'How to say “if” in Japanese',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'This whole section is dedicated to learning how to say “if” in Japanese. Oh, if only it was as simple as English. In Japanese, there’s four (count them, four) ways to say “if”! Thankfully, the conjugations are sparse and easy especially since you don’t have to deal with tenses.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This whole section is dedicated to learning how to say “if” in Japanese. Oh, if only it was as simple as English. In Japanese, there’s four (count them, four) ways to say “if”! Thankfully, the conjugations are sparse and easy especially since you don’t have to deal with tenses.',
      grammars: [],
      keywords: ['if','conditional'],
    ),
    Section(
      heading: 'Expressing natural consequence using 「と」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('ボール – ball'),
                  Text('落とす 【お・とす】 (u-verb) – to drop'),
                  Text('落ちる 【お・ちる】 (ru-verb) – to fall'),
                  Text('電気 【でん・き】 – electricity; (electric) light'),
                  Text('消す 【け・す】 (u-verb) – to erase'),
                  Text('暗い 【くら・い】 (i-adj) – dark'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('する (exception) – to do'),
                  Text('たくさん – a lot (amount)'),
                  Text('太る 【ふと・る】 (u-verb) – to become fatter'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('きっと – for sure'),
                  Text('年上 【とし・うえ】 – older'),
                ],
              ),
              const NotesSection(
                content: NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Attach 「と」 to the condition followed by the result that would occur should the condition be satisfied.'),
                        Text(
                          '[Condition] + と + [Result]',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('State-of-being must be made explicit.'),
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: '[State-of-being] + ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: 'だ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              ),
                              TextSpan(
                                text: 'と + [Result]',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Rules for using the conditional 「と」',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: booru,
                              inlineSpan: const TextSpan(
                                text: 'ボール',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: otosu,
                              inlineSpan: const TextSpan(
                                text: '落す',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ochiru,
                              inlineSpan: const TextSpan(
                                text: '落ちる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you drop the ball, it will fall.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: konshuumatsu,
                              inlineSpan: const TextSpan(
                                text: '消す',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kurai,
                              inlineSpan: const TextSpan(
                                text: '暗く',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you turn off the lights, it will get dark.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'These examples are designed to show how 「と」 is used to express natural consequence. However, even if the statement isn’t a natural consequence in itself, the 「と」 will tell the audience that it is nevertheless expected to be a natural consequence.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(
                                text: '会えない',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you don’t go to school, you can’t meet your friends.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: futoru,
                              inlineSpan: const TextSpan(
                                text: '太る',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you eat a lot, you will get fat, for sure.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            TextSpan(
                              text: 'だと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kitto,
                              inlineSpan: const TextSpan(
                                text: 'きっと',
                              ),
                            ),
                            VocabTooltip(
                              message: toshiue,
                              inlineSpan: const TextSpan(
                                text: '年上',
                              ),
                            ),
                            const TextSpan(
                              text: 'なんじゃないですか？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If he’s a teacher, he must be older for sure, right?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The “for sure” part is the implied meaning supplied by the 「と」. The speaker is saying that the following condition will occur in that situation, no matter what. As you can see from the last example, if the condition is a state-of-being, it must be expressed so explicitly using 「だ」. This applies to all non-conjugated nouns and na-adjectives as I’m sure you’re used to by now. This will also help prevent confusion with other types of 「と」.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ボール – ball 落とす 【お・とす】 (u-verb) – to drop 落ちる 【お・ちる】 (ru-verb) – to fall 電気 【でん・き】 – electricity; (electric) light 消す 【け・す】 (u-verb) – to erase 暗い 【くら・い】 (i-adj) – dark 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 友達 【とも・だち】 – friend 会う 【あ・う】 (u-verb) – to meet する (exception) – to do たくさん – a lot (amount) 太る 【ふと・る】 (u-verb) – to become fatter 先生 【せん・せい】 – teacher きっと – for sure 年上 【とし・うえ】 – older Rules for using the conditional 「と」 Attach 「と」 to the condition followed by the result that would occur should the condition be satisfied.[Condition] + と + [Result] State-of-being must be made explicit.[State-of-being] + だと + [Result] Examples ボールを落すと落ちる。 If you drop the ball, it will fall. 電気を消すと暗くなる。 If you turn off the lights, it will get dark. These examples are designed to show how 「と」 is used to express natural consequence. However, even if the statement isn’t a natural consequence in itself, the 「と」 will tell the audience that it is nevertheless expected to be a natural consequence. 学校に行かないと友達と会えないよ。 If you don’t go to school, you can’t meet your friends. たくさん食べると太るよ。 If you eat a lot, you will get fat, for sure. 先生だと、きっと年上なんじゃないですか？ If he’s a teacher, he must be older for sure, right? The “for sure” part is the implied meaning supplied by the 「と」. The speaker is saying that the following condition will occur in that situation, no matter what. As you can see from the last example, if the condition is a state-of-being, it must be expressed so explicitly using 「だ」. This applies to all non-conjugated nouns and na-adjectives as I’m sure you’re used to by now. This will also help prevent confusion with other types of 「と」.',
      grammars: ['と'],
      keywords: ['to','conditional','if'],
    ),
    Section(
      heading: 'Contextual conditionals using 「なら（ば）」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('皆 【みんな】 – everybody'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('問題 【もん・だい】 – problem'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('図書館 【と・しょ・かん】 – library'),
                  Text('あそこ – over there'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Another relatively easy to understand type of “if” is the contextual conditional. You can use this particle to express what will happen given a certain context. For example, if you wanted to say, “Well, if everybody’s going, I’m going too” you would use the 「なら」 conditional because you are saying that you will go in the context of everybody else going. The contextual conditional always requires a context in which the conditional occurs. For instance, you would use it for saying things like, “If that’s what you are talking about…” or “If that’s the case, then…”'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In a sense, you are explaining what would occur if you assume a certain condition is satisfied. In other words, you are saying “if given a certain context, here is what will happen.” You will see this reflected in the English translations as the phrase “if given” in the examples.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The 「なら」 is attached to the context in which the conditional occurs. The format is the same as the 「と」 conditional, however, you must not attach the declarative 「だ」.'),
                ],
              ),
              const NotesSection(
                content: NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Attach 「なら」 to the context in which the conditional would occur.'),
                        Text(
                          ' [Assumed Context] + なら + [Result]',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: '[You ',
                          ),
                          TextSpan(
                            text: 'must not',
                          ),
                          TextSpan(
                            text: ' attach the declarative 「だ」.',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                heading: 'Rules for using the contextual conditional 「なら」',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            TextSpan(
                              text: 'なら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If given that everybody is going, then I’ll go too.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリス',
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: const TextSpan(
                                text: 'さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言う',
                              ),
                            ),
                            TextSpan(
                              text: 'なら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: mondai,
                              inlineSpan: const TextSpan(
                                text: '問題',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If given that Alice-san says so, there’s no problem.',
                      ),
                    ],
                  ),
                ],
              ),
              const Heading(text: 'Example Dialogue', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text(
                      'アリス',
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: toshokan,
                            inlineSpan: const TextSpan(text: '図書館'),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: doko,
                            inlineSpan: const TextSpan(text: 'どこ'),
                          ),
                          const TextSpan(
                            text: 'ですか。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Where is the library?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ichirou,
                            inlineSpan: const TextSpan(text: 'ボブ'),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: toshokan,
                            inlineSpan: const TextSpan(text: '図書館'),
                          ),
                          TextSpan(
                            text: 'なら',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: asoko,
                            inlineSpan: const TextSpan(text: 'あそこ'),
                          ),
                          const TextSpan(
                            text: 'です。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text(
                        'If given that you’re talking about the library, then it’s over there.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'The following is incorrect.'),
                ],
              ),
              BulletedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: toshokan,
                          inlineSpan: const TextSpan(text: '図書館'),
                        ),
                        TextSpan(
                          text: 'だ',
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              decoration: TextDecoration.lineThrough),
                        ),
                        const TextSpan(
                          text: 'なら',
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: asoko,
                          inlineSpan: const TextSpan(text: 'あそこ'),
                        ),
                        const TextSpan(
                          text: 'です。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'You can also decide to use 「なら'),
                  TextSpan(
                    text: 'ば',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 instead of just 「なら」. This means exactly the same thing except that it has a more formal nuance.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 皆 【みんな】 – everybody 行く 【い・く】 (u-verb) – to go 私 【わたし】 – me, myself, I 言う 【い・う】 (u-verb) – to say 問題 【もん・だい】 – problem ある (u-verb) – to exist (inanimate) 図書館 【と・しょ・かん】 – library あそこ – over there Another relatively easy to understand type of “if” is the contextual conditional. You can use this particle to express what will happen given a certain context. For example, if you wanted to say, “Well, if everybody’s going, I’m going too” you would use the 「なら」 conditional because you are saying that you will go in the context of everybody else going. The contextual conditional always requires a context in which the conditional occurs. For instance, you would use it for saying things like, “If that’s what you are talking about…” or “If that’s the case, then…”In a sense, you are explaining what would occur if you assume a certain condition is satisfied. In other words, you are saying “if given a certain context, here is what will happen.” You will see this reflected in the English translations as the phrase “if given” in the examples. The 「なら」 is attached to the context in which the conditional occurs. The format is the same as the 「と」 conditional, however, you must not attach the declarative 「だ」. Rules for using the contextual conditional 「なら」 Attach 「なら」 to the context in which the conditional would occur. [Assumed Context] + なら + [Result][You must not attach the declarative 「だ」. Examples みんなが行くなら私も行く。 If given that everybody is going, then I’ll go too. アリスさんが言うなら問題ないよ。 If given that Alice-san says so, there’s no problem. Example Dialogue アリス：図書館はどこですか。 Alice: Where is the library? ボブ：図書館なら、あそこです。 Bob: If given that you’re talking about the library, then it’s over there. The following is incorrect. 図書館だなら、あそこです。 You can also decide to use 「ならば」 instead of just 「なら」. This means exactly the same thing except that it has a more formal nuance.',
      grammars: ['なら（ば）'],
      keywords: ['nara','naraba','if','conditional'],
    ),
    Section(
      heading: 'General conditionals using 「ば」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('おかしい (i-adj) – funny'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('学生 【がく・せい】 – student'),
                  Text('暇 【ひま】 – free　(as in not busy)'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('買い物 【か・い・もの】 – shopping'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('お金 【お・かね】 – money'),
                  Text('いい (i-adj) – good'),
                  Text('楽しい 【たの・しい】 (i-adj) – fun'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('病気 【びょう・き】 – disease; sickness'),
                  Text('なる (u-verb) – to become'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The next type of conditional just expresses a regular “if” condition without any assumptions or embedded meanings. The conjugation rules for the 「ば」 conditional is below. Note, the conjugation rule for nouns and na-adjectives is actually using the verb 「ある」 in 「である」, a formal expression we’ll learn much later.'),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the last /u/ vowel sound to the equivalent /e/ vowel sound and attach 「ば」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'れ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べれ',
                                        ),
                                        TextSpan(
                                          text: 'ば',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'つ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'て',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待て',
                                        ),
                                        TextSpan(
                                          text: 'ば',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    'For i-adjectives or negatives ending in 「ない」:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Drop the last 「い」 and attach 「ければ」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: okashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'おかし',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: okashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'おかし',
                                        ),
                                        TextSpan(
                                          text: 'ければ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'ければ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For nouns and na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach 「であれば」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'であれば',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hima,
                                    inlineSpan: const TextSpan(
                                      text: '暇',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hima,
                                    inlineSpan: const TextSpan(
                                      text: '暇',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'であれば',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Rules for creating potential form',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '会えれ',
                                  ),
                                  TextSpan(
                                    text: 'ば',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kaimono,
                              inlineSpan: const TextSpan(
                                text: '買い物',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きます',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If I can meet with my friend, we will go shopping.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'あれ',
                                  ),
                                  TextSpan(
                                    text: 'ば',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'ね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If I had money, it would be good, huh?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tanoshii,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '楽し',
                                  ),
                                  TextSpan(
                                    text: 'ければ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If it’s fun, I’ll go too.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tanoshii,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '楽しくな',
                                  ),
                                  TextSpan(
                                    text: 'ければ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If it’s not fun, I’ll also not go.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '食べな',
                                  ),
                                  TextSpan(
                                    text: 'ければ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: byouki,
                              inlineSpan: const TextSpan(
                                text: '病気',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you don’t eat, you will become sick.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 待つ 【ま・つ】 (u-verb) – to wait おかしい (i-adj) – funny ある (u-verb) – to exist (inanimate) 学生 【がく・せい】 – student 暇 【ひま】 – free　(as in not busy) 友達 【とも・だち】 – friend 会う 【あ・う】 (u-verb) – to meet 買い物 【か・い・もの】 – shopping 行く 【い・く】 (u-verb) – to go お金 【お・かね】 – money いい (i-adj) – good 楽しい 【たの・しい】 (i-adj) – fun 私 【わたし】 – me; myself; I 病気 【びょう・き】 – disease; sickness なる (u-verb) – to become The next type of conditional just expresses a regular “if” condition without any assumptions or embedded meanings. The conjugation rules for the 「ば」 conditional is below. Note, the conjugation rule for nouns and na-adjectives is actually using the verb 「ある」 in 「である」, a formal expression we’ll learn much later. Rules for creating potential form For verbs: Change the last /u/ vowel sound to the equivalent /e/ vowel sound and attach 「ば」. Examples: 食べる → 食べれ → 食べれば待つ → 待て → 待てば For i-adjectives or negatives ending in 「ない」: Drop the last 「い」 and attach 「ければ」. Examples: おかしい → おかしければない → なければ For nouns and na-adjectives: Attach 「であれば」. Examples: 学生 → 学生であれば暇 → 暇であれば Examples 友達に会えれば、買い物に行きます。 If I can meet with my friend, we will go shopping. お金があればいいね。 If I had money, it would be good, huh? 楽しければ、私も行く。 If it’s fun, I’ll go too. 楽しくなければ、私も行かない。 If it’s not fun, I’ll also not go. 食べなければ病気になるよ。 If you don’t eat, you will become sick.',
      grammars: ['ば'],
      keywords: ['ba','eba','if','conditional'],
    ),
    Section(
      heading: 'Past conditional using 「たら（ば）」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('自動 【じ・どう】 – automatic'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('暇 【ひま】 – free　(as in not busy)'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('学生 【がく・せい】 – student'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('買い物 【か・い・もの】 – shopping'),
                  Text('お金 【お・かね】 – money'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('いい (i-adj) – good'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('誰 【だれ】 – who'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('アメリカ – America'),
                  Text('たくさん – a lot (amount)'),
                  Text('太る 【ふと・る】 (u-verb) – to become fatter'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'I call this next conditional the past conditional because it is produced by taking the past tense and just adding 「ら」. It is commonly called the 「たら」 conditional because all past-tense ends with 「た／だ」 and so it always becomes 「たら／だら」. Like the 「ば」 conditional, it is also a general conditional.'),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'First change the noun, adjective, or verb to its past tense and attach 「ら」.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: jidou,
                                    inlineSpan: const TextSpan(
                                      text: '自動',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: jidou,
                                    inlineSpan: const TextSpan(
                                      text: '自動',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だった',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: jidou,
                                    inlineSpan: const TextSpan(
                                      text: '自動',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'だった',
                                  ),
                                  TextSpan(
                                    text: 'ら',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'つ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待',
                                        ),
                                        TextSpan(
                                          text: 'った',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: matsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '待った',
                                        ),
                                        TextSpan(
                                          text: 'ら',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読',
                                        ),
                                        TextSpan(
                                          text: 'む',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読',
                                        ),
                                        TextSpan(
                                          text: 'んだ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読んだ',
                                        ),
                                        TextSpan(
                                          text: 'ら',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: isogashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '忙し',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: isogashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '忙し',
                                        ),
                                        TextSpan(
                                          text: 'かった',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: isogashii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '忙しかった',
                                        ),
                                        TextSpan(
                                          text: 'ら',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Conjugation Rule for 「たら（ば）」',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: himaFree,
                              inlineSpan: const TextSpan(
                                text: '暇',
                              ),
                            ),
                            const TextSpan(
                              text: 'だっ',
                            ),
                            TextSpan(
                              text: 'たら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: const TextSpan(
                                text: '遊び',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If I am free, I will go play.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'だっ',
                            ),
                            TextSpan(
                              text: 'たら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            VocabTooltip(
                              message: waribiki,
                              inlineSpan: const TextSpan(
                                text: '割引',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買えます',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you’re a student, you can buy with a student discount.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'For i-adjectives and verbs, it is very difficult to differentiate between the two types of conditionals, and you can make life easier for yourself by considering them to be the same. However there is a small difference in that the 「たら」 conditional focuses on what happens after the condition. This is another reason why I call this the past conditional because the condition is “in the past” (not literally) and we’re interested in the result not the condition. The 「ば」 conditional, on the other hand, focuses on the conditional part.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'Let’s compare the difference in nuance.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: au,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '会えれ',
                                ),
                                TextSpan(
                                  text: 'ば',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kaimono,
                            inlineSpan: const TextSpan(text: '買い物'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行きます'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('A'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'We will go shopping, ',
                          ),
                          TextSpan(
                            text: 'if',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: ' I can meet with my friend.',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: au,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '会え',
                                ),
                                TextSpan(
                                  text: 'たら',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kaimono,
                            inlineSpan: const TextSpan(text: '買い物'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行きます'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('B'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'If I can meet with my friend, ',
                          ),
                          TextSpan(
                            text: 'we will go shopping',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '.',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: au,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '会えれ',
                                ),
                                TextSpan(
                                  text: 'ば',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kaimono,
                            inlineSpan: const TextSpan(text: '買い物'),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(text: '行きます'),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('A'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'It would be good, ',
                          ),
                          TextSpan(
                            text: 'if',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: ' I had money, huh?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: okane,
                            inlineSpan: const TextSpan(text: 'お金'),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: 'あっ',
                                ),
                                TextSpan(
                                  text: 'たら',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(text: 'いい'),
                          ),
                          const TextSpan(
                            text: 'ね。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('B'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'If I had money, ',
                          ),
                          TextSpan(
                            text: 'it would be good, huh',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '?',
                          ),
                        ],
                      ),
                    ),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Going by the context, the 「～たら」 form sounds more natural for both examples because it doesn’t seem like we’re really focusing on the condition itself. We’re probably more interested in what’s going to happen once we meet the friend or how nice it would be if we had money.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The past conditional is the only type of conditional where the result can be in the past. It may seem strange to have an “if” when the result has already taken place. Indeed, in this usage, there really is no “if”, it’s just a way of expressing surprise at the result of the condition. This has little to do with conditionals but it is explained here because the grammatical structure is the same.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰ったら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: daremo,
                              inlineSpan: const TextSpan(
                                text: '誰も',
                              ),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いなかった',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'When I went home, there was no one there. (unexpected result)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: 'アメリカ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行ったら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: futoru,
                              inlineSpan: const TextSpan(
                                text: '太りました',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'As a result of going to America, I got really fat. (unexpected result)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'You can also use 「たら'),
                  TextSpan(
                    text: 'ば',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 instead of 「たら」. Similar to 「ならば」, this means exactly the same thing except that it has a more formal nuance.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 自動 【じ・どう】 – automatic 待つ 【ま・つ】 (u-verb) – to wait 読む 【よ・む】 (u-verb) – to read 忙しい 【いそが・しい】 (i-adj) – busy 暇 【ひま】 – free　(as in not busy) 遊ぶ 【あそ・ぶ】 (u-verb) – to play 行く 【い・く】 (u-verb) – to go 学生 【がく・せい】 – student 買う 【か・う】 (u-verb) – to buy 友達 【とも・だち】 – friend 会う 【あ・う】 (u-verb) – to meet 買い物 【か・い・もの】 – shopping お金 【お・かね】 – money ある (u-verb) – to exist (inanimate) いい (i-adj) – good 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home 誰 【だれ】 – who いる (ru-verb) – to exist (animate) アメリカ – America たくさん – a lot (amount) 太る 【ふと・る】 (u-verb) – to become fatter I call this next conditional the past conditional because it is produced by taking the past tense and just adding 「ら」. It is commonly called the 「たら」 conditional because all past-tense ends with 「た／だ」 and so it always becomes 「たら／だら」. Like the 「ば」 conditional, it is also a general conditional. Conjugation Rule for 「たら（ば）」 First change the noun, adjective, or verb to its past tense and attach 「ら」. Examples: 自動 → 自動だった → 自動だったら待つ → 待った → 待ったら読む → 読んだ → 読んだら 忙しい → 忙しかった → 忙しかったら Examples 暇だったら、遊びに行くよ。 If I am free, I will go play. 学生だったら、学生割引で買えます。 If you’re a student, you can buy with a student discount. For i-adjectives and verbs, it is very difficult to differentiate between the two types of conditionals, and you can make life easier for yourself by considering them to be the same. However there is a small difference in that the 「たら」 conditional focuses on what happens after the condition. This is another reason why I call this the past conditional because the condition is “in the past” (not literally) and we’re interested in the result not the condition. The 「ば」 conditional, on the other hand, focuses on the conditional part. Let’s compare the difference in nuance. Example 1 Ａ：友達に会えれば、買い物に行きます。 A: We will go shopping, if I can meet with my friend. Ｂ：友達に会えたら、買い物に行きます。  B: If I can meet with my friend, we will go shopping. Example 2 Ａ：友達に会えれば、買い物に行きます。 A: It would be good, if I had money, huh? Ｂ：お金があったらいいね。 B: If I had money, it would be good, huh? Going by the context, the 「～たら」 form sounds more natural for both examples because it doesn’t seem like we’re really focusing on the condition itself. We’re probably more interested in what’s going to happen once we meet the friend or how nice it would be if we had money. The past conditional is the only type of conditional where the result can be in the past. It may seem strange to have an “if” when the result has already taken place. Indeed, in this usage, there really is no “if”, it’s just a way of expressing surprise at the result of the condition. This has little to do with conditionals but it is explained here because the grammatical structure is the same. 家に帰ったら、誰もいなかったよ。 When I went home, there was no one there. (unexpected result) アメリカに行ったら、たくさん太りました。 As a result of going to America, I got really fat. (unexpected result) You can also use 「たらば」 instead of 「たら」. Similar to 「ならば」, this means exactly the same thing except that it has a more formal nuance.',
      grammars: ['たら（ば）'],
      keywords: ['taraba','if','conditional'],
    ),
    Section(
      heading: 'How does 「もし」 fit into all of this?',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('もし – if by any chance'),
                  Text('いい (i-adj) – good'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('明日 【あした】 – tomorrow'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'Some of you may be aware of the word 「'),
                  VocabTooltip(
                    message: moshi,
                    inlineSpan: const TextSpan(
                      text: 'もし',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 which means “if” and may be wondering how it fits into all of this. Well, if you want to say a conditional, you need to use one of the conditionals discussed above. 「'),
                  VocabTooltip(
                    message: moshi,
                    inlineSpan: const TextSpan(
                      text: 'もし',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is really a supplement to add a sense of uncertainty on whether the condition is true. For instance, you might use it when you want to make an invitation and you don’t want to presume like the following example.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: TextSpan(
                                text: 'もし',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よかったら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: const TextSpan(
                                text: '観',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きます',
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'If ',
                            ),
                            TextSpan(
                              text: 'by any chance',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: ' it’s ok with you, go to watch movie?',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: TextSpan(
                                text: 'もし',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'なら、',
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: 'でも',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If given that there’s no time, tomorrow is fine as well. (Not certain whether there is no time)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary もし – if by any chance いい (i-adj) – good 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch 行く 【い・く】 (u-verb) – to go 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 明日 【あした】 – tomorrow Some of you may be aware of the word 「もし」 which means “if” and may be wondering how it fits into all of this. Well, if you want to say a conditional, you need to use one of the conditionals discussed above. 「もし」 is really a supplement to add a sense of uncertainty on whether the condition is true. For instance, you might use it when you want to make an invitation and you don’t want to presume like the following example. もしよかったら、映画を観に行きますか？ If by any chance it’s ok with you, go to watch movie? もし時間がないなら、明日でもいいよ。 If given that there’s no time, tomorrow is fine as well. (Not certain whether there is no time)',
      grammars: ['もし'],
      keywords: ['moshi','if'],
    ),
  ],
);
