import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson define = Lesson(
  title: 'Defining and Describing',
  sections: [
    Section(
      heading: 'The various uses of 「いう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'In the previous lesson, we learned how to quote a relative clause by encasing it with 「と」. This allowed us to talk about things that people have said, heard, thought, and more. We also took a look at some examples sentences that used 「と」 and 「'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 to describe how to say something in Japanese and even what to call oneself. In this section, we will learn that with 「と」, we can use 「'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 in a similar fashion to define, describe, and generally just talk about the thing itself. We’ll also see how to do the same thing with the casual 「って」 version we first learned about in the last lesson.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In the previous lesson, we learned how to quote a relative clause by encasing it with 「と」. This allowed us to talk about things that people have said, heard, thought, and more. We also took a look at some examples sentences that used 「と」 and 「言う」 to describe how to say something in Japanese and even what to call oneself. In this section, we will learn that with 「と」, we can use 「いう」 in a similar fashion to define, describe, and generally just talk about the thing itself. We’ll also see how to do the same thing with the casual 「って」 version we first learned about in the last lesson.',
      grammars: [],
      keywords: ['iu','to iu','define','describe'],
    ),
    Section(
      heading: 'Using 「いう」 to define',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('これ – this'),
                  Text('何 【なに／なん】 – what'),
                  Text('魚 【さかな】 – fish'),
                  Text('この – this （abbr. of これの）'),
                  Text('鯛 【たい】 – tai (type of fish)'),
                  Text('デパート – department store'),
                  Text('どこ – where'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('英語 【えい・ご】 – English (language)'),
                  Text('意味 【い・み】 – meaning'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In the last lesson, we briefly looked at how to introduce ourselves by using 「と」 and 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. For instance, we had the following example, which Alice used to introduce herself.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            const TextSpan(
                              text: 'アリス',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いいます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I am called Alice. (lit: As for me, you say Alice.)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This sentence pattern is probably one of the first things beginner Japanese students learn in the classroom. In this case, the verb 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 doesn’t mean that somebody actually said something. Rather, Alice is saying that people in general say “Alice” when referring to her. While using kanji for 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is perfectly acceptable, in this case, since nothing is actually being said, using hiragana is also common.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This idea of describing what a person is known or referred to as can also be extended to objects and places. We can essentially define and identify anything we want by using 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in this manner. As you can imagine, this is particularly useful for us because it allows us to ask what things are called in Japanese and for the definition of words we don’t know yet.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: TextSpan(
                                text: 'なん',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: TextSpan(
                                text: '魚',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What is this fish referred to as?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: sakana,
                              inlineSpan: const TextSpan(
                                text: '魚',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: tai,
                              inlineSpan: TextSpan(
                                text: '鯛',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いいます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'This fish is known as “Tai“.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'ルミネと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: depaato,
                              inlineSpan: TextSpan(
                                text: 'デパート',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(
                                text: 'どこ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(
                              text: 'か、',
                            ),
                            VocabTooltip(
                              message: shiru,
                              inlineSpan: const TextSpan(
                                text: '知って',
                              ),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'います',
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Do you know where the department store called “Lumine” is?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '「',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: '」は、',
                            ),
                            VocabTooltip(
                              message: eigo,
                              inlineSpan: const TextSpan(
                                text: '英語',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            TextSpan(
                              text: '「friend」と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: imi,
                              inlineSpan: const TextSpan(
                                text: '意味',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The meaning of “tomodachi” in English is “friend”.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 言う 【い・う】 (u-verb) – to say これ – this 何 【なに／なん】 – what 魚 【さかな】 – fish この – this （abbr. of これの） 鯛 【たい】 – tai (type of fish) デパート – department store どこ – where ある (u-verb) – to exist (inanimate) 知る 【し・る】 (u-verb) – to know 友達 【とも・だち】 – friend 英語 【えい・ご】 – English (language) 意味 【い・み】 – meaning In the last lesson, we briefly looked at how to introduce ourselves by using 「と」 and 「いう」. For instance, we had the following example, which Alice used to introduce herself. 私はアリスといいます。 I am called Alice. (lit: As for me, you say Alice.) This sentence pattern is probably one of the first things beginner Japanese students learn in the classroom. In this case, the verb 「いう」 doesn’t mean that somebody actually said something. Rather, Alice is saying that people in general say “Alice” when referring to her. While using kanji for 「いう」 is perfectly acceptable, in this case, since nothing is actually being said, using hiragana is also common. This idea of describing what a person is known or referred to as can also be extended to objects and places. We can essentially define and identify anything we want by using 「という」 in this manner. As you can imagine, this is particularly useful for us because it allows us to ask what things are called in Japanese and for the definition of words we don’t know yet. Examples これは、なんという魚ですか。 What is this fish referred to as? この魚は、鯛といいます。 This fish is known as “Tai“. ルミネというデパートはどこにあるか、知っていますか？ Do you know where the department store called “Lumine” is? 「友達」は、英語で「friend」という意味です。 The meaning of “tomodachi” in English is “friend”.',
      grammars: ['という'],
      keywords: ['iu','to iu','to','define'],
    ),
    Section(
      heading: 'Using 「いう」 to describe anything',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('主人公 【しゅ・じん・こう】 – main character'),
                  Text('犯人 【はん・にん】 – criminal'),
                  Text('一番 【いち・ばん】 – best; first'),
                  Text('面白い 【おも・しろ・い】(i-adj) – interesting'),
                  Text('日本人 【に・ほん・じん】 – Japanese person'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('弱い 【よわ・い】(i-adj) – weak'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('本当 【ほん・とう】 – real'),
                  Text('独身 【どく・しん】 – single; unmarried'),
                  Text('嘘 【うそ】 – lie'),
                  Text('リブート – reboot'),
                  Text('パソコン – computer, PC'),
                  Text('こう – (things are) this way'),
                  Text('そう – (things are) that way'),
                  Text('ああ – (things are) that way'),
                  Text('どう – how'),
                  Text('再起動 【さい・き・どう】 – reboot'),
                  Text('あんた – you (slang)'),
                  Text('いつも – always'),
                  Text('時 【とき】 – time'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('困る 【こま・る】 (u-verb) – to be bothered, troubled'),
                  Text('人 【ひと】 – person'),
                  Text('結婚 【けっ・こん】 – marriage'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('幸せ 【しあわ・せ】 – happiness'),
                  Text('なる (u-verb) – to become'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('大学 【だい・がく】 – college'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('意味　【い・み】 – meaning'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We learned how to use 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to describe what something is known or referred to as. However, we can take this idea even further by attaching two relative clauses. At this point, 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is so abstract that it doesn’t even really have a meaning. When a relative clause is encapsulated with 「と」, you must have a verb to go along with it and 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is simply being used as a generic verb to enable us to talk about any relative clause. This allows us to describe and explain just about anything ranging from a single word to complete sentences. As you can imagine, this construction is quite useful and employed quite often in Japanese.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shujinkou,
                              inlineSpan: const TextSpan(
                                text: '主人公',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hannin,
                              inlineSpan: const TextSpan(
                                text: '犯人',
                              ),
                            ),
                            const TextSpan(
                              text: 'だった',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'のが',
                            ),
                            VocabTooltip(
                              message: ichiban,
                              inlineSpan: const TextSpan(
                                text: '一番',
                              ),
                            ),
                            VocabTooltip(
                              message: omoshiroi,
                              inlineSpan: const TextSpan(
                                text: '面白かった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The most interesting thing was that the main character was the criminal.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihonjin,
                              inlineSpan: const TextSpan(
                                text: '日本人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: yowai,
                              inlineSpan: const TextSpan(
                                text: '弱い',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'のは',
                            ),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Is it true that Japanese people are weak to alcohol?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: dokushin,
                              inlineSpan: const TextSpan(
                                text: '独身',
                              ),
                            ),
                            const TextSpan(
                              text: 'だ',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'のは、',
                            ),
                            VocabTooltip(
                              message: uso,
                              inlineSpan: const TextSpan(
                                text: '嘘',
                              ),
                            ),
                            const TextSpan(
                              text: 'だったの？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It was a lie that you were single?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ribuuto,
                              inlineSpan: const TextSpan(
                                text: 'リブート',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'のは、',
                            ),
                            VocabTooltip(
                              message: pasokon,
                              inlineSpan: const TextSpan(
                                text: 'パソコン',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: saikidou,
                              inlineSpan: const TextSpan(
                                text: '再起動',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Reboot means to restart your computer.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We can abstract it even further by replacing the relative clause with a generic way of doing something. In this case, we use 「',
                  ),
                  VocabTooltip(
                    message: kou,
                    inlineSpan: const TextSpan(
                      text: 'こう',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: souLikeThat,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: aa,
                    inlineSpan: const TextSpan(
                      text: 'ああ',
                    ),
                  ),
                  const TextSpan(
                    text: '」、and 「',
                  ),
                  VocabTooltip(
                    message: dou,
                    inlineSpan: const TextSpan(
                      text: 'どう',
                    ),
                  ),
                  const TextSpan(
                    text: '」, which when combined with 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 means “this way, “that way”, “that way (far away in an abstract sense)” and “what way” respectively.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: anta,
                              inlineSpan: const TextSpan(
                                text: 'あんた',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: itsumo,
                              inlineSpan: const TextSpan(
                                text: 'いつも',
                              ),
                            ),
                            VocabTooltip(
                              message: kou,
                              inlineSpan: TextSpan(
                                text: 'こう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: toki,
                              inlineSpan: const TextSpan(
                                text: '時',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: '来る',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだから、',
                            ),
                            VocabTooltip(
                              message: komaru,
                              inlineSpan: const TextSpan(
                                text: '困る',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s because you always come at times like these that I’m troubled.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: souLikeThat,
                              inlineSpan: TextSpan(
                                text: 'そう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: issho,
                              inlineSpan: const TextSpan(
                                text: '一緒',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: 'のは、',
                            ),
                            VocabTooltip(
                              message: iya,
                              inlineSpan: const TextSpan(
                                text: '嫌',
                              ),
                            ),
                            const TextSpan(
                              text: 'だよね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(Anybody would) dislike doing work together with that type of person, huh?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aa,
                              inlineSpan: TextSpan(
                                text: 'ああ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: kekkon,
                              inlineSpan: const TextSpan(
                                text: '結婚',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できたら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: shiawase,
                              inlineSpan: const TextSpan(
                                text: '幸せ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なれる',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思います',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I think you can become happy if you could marry that type of person.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            const TextSpan(
                              text: 'って、',
                            ),
                            VocabTooltip(
                              message: dou,
                              inlineSpan: TextSpan(
                                text: 'どう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: imi,
                              inlineSpan: const TextSpan(
                                text: '意味',
                              ),
                            ),
                            const TextSpan(
                              text: 'なの？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What do you mean, “You’re not going to go to college?”',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 主人公 【しゅ・じん・こう】 – main character 犯人 【はん・にん】 – criminal 一番 【いち・ばん】 – best; first 面白い 【おも・しろ・い】(i-adj) – interesting 日本人 【に・ほん・じん】 – Japanese person お酒 【お・さけ】 – alcohol 弱い 【よわ・い】(i-adj) – weak 言う 【い・う】 (u-verb) – to say 本当 【ほん・とう】 – real 独身 【どく・しん】 – single; unmarried 嘘 【うそ】 – lie リブート – reboot パソコン – computer, PC こう – (things are) this way そう – (things are) that way ああ – (things are) that way どう – how 再起動 【さい・き・どう】 – reboot あんた – you (slang) いつも – always 時 【とき】 – time 来る 【く・る】 (exception) – to come 困る 【こま・る】 (u-verb) – to be bothered, troubled 人 【ひと】 – person 結婚 【けっ・こん】 – marriage 出来る 【で・き・る】 (ru-verb) – to be able to do 幸せ 【しあわ・せ】 – happiness なる (u-verb) – to become 思う 【おも・う】 (u-verb) – to think 大学 【だい・がく】 – college 行く 【い・く】 (u-verb) – to go 意味　【い・み】 – meaning We learned how to use 「という」 to describe what something is known or referred to as. However, we can take this idea even further by attaching two relative clauses. At this point, 「いう」 is so abstract that it doesn’t even really have a meaning. When a relative clause is encapsulated with 「と」, you must have a verb to go along with it and 「いう」 is simply being used as a generic verb to enable us to talk about any relative clause. This allows us to describe and explain just about anything ranging from a single word to complete sentences. As you can imagine, this construction is quite useful and employed quite often in Japanese. Examples 主人公が犯人だったというのが一番面白かった。 The most interesting thing was that the main character was the criminal. 日本人はお酒に弱いというのは本当？ Is it true that Japanese people are weak to alcohol? 独身だというのは、嘘だったの？ It was a lie that you were single? リブートというのは、パソコンを再起動するということです。 Reboot means to restart your computer. We can abstract it even further by replacing the relative clause with a generic way of doing something. In this case, we use 「こう」、「そう」、「ああ」、and 「どう」, which when combined with 「いう」 means “this way, “that way”, “that way (far away in an abstract sense)” and “what way” respectively. Examples あんたは、いつもこういう時に来るんだから、困るんだよ。 It’s because you always come at times like these that I’m troubled. そういう人と一緒に仕事をするのは、嫌だよね。(Anybody would) dislike doing work together with that type of person, huh? ああいう人と結婚できたら、幸せになれると思います。 I think you can become happy if you could marry that type of person. 大学に行かないって、どういう意味なの？ What do you mean, “You’re not going to go to college?”',
      grammars: ['というの'],
      keywords: ['iu','to iu','to iu no','to','describe'],
    ),
    Section(
      heading: 'Rephrasing and making conclusions with 「という」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('あんた – you (slang)'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('何 【なに／なん】 – what'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('好き 【す・き】 (na-adj) – likable'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('生きる 【い・きる】 (ru-verb) – to live'),
                  Text('多分 【た・ぶん】 – maybe'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('お金 【お・かね】 – money'),
                  Text('もう – already'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('駄目 【だめ】 – no good'),
                  Text('洋介 【よう・すけ】 – Yousuke (first name)'),
                  Text('別れる 【わか・れる】 (ru-verb) – to separate; to break up'),
                  Text('こと – event, matter'),
                  Text('今 【いま】 – now'),
                  Text('彼氏【かれ・し】 – boyfriend'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('そう – (things are) that way'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We can attach the question marker 「か」 to 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in order to add a questioning element. This construction is used when you want to rephrase or redefine something such as the following dialogue.',
                  ),
                ],
              ),
              const Heading(text: 'Example Dialogue', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'みきちゃんは、',
                          ),
                          VocabTooltip(
                            message: anta,
                            inlineSpan: const TextSpan(
                              text: 'あんた',
                            ),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: kanojo,
                            inlineSpan: const TextSpan(
                              text: '彼女',
                            ),
                          ),
                          const TextSpan(
                            text: 'でしょう？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Miki-chan is your girlfriend, right?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'う～ん、',
                          ),
                          VocabTooltip(
                            message: kanojo,
                            inlineSpan: const TextSpan(
                              text: '彼女',
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          TextSpan(
                            text: 'か',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(
                              text: '友達',
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          TextSpan(
                            text: 'か',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: nan,
                            inlineSpan: const TextSpan(
                              text: 'なん',
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          TextSpan(
                            text: 'か',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '・・・',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'Um, you might say girlfriend, or friend, or something…'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This construction is used all the time, particularly in casual conversations. It can be used to correct something, come to a different conclusion, or even as an interjection.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: ikiru,
                              inlineSpan: const TextSpan(
                                text: '生きて',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: 'いけない',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I like alcohol or rather, can’t live on without it.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tabun,
                              inlineSpan: const TextSpan(
                                text: '多分',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '思う',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行けない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Don’t think I’ll go. Or rather, can’t because there’s no money.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰らない',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'ですけど。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Rather than that, I have to go home already.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Rather than using 「か」 to rephrase a conclusion, we can also simply use 「',
                  ),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to sum up something without rephrasing anything.',
                  ),
                ],
              ),
              const Heading(text: 'Example Dialogue', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'みきちゃんが',
                          ),
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                          const TextSpan(
                            text: 'と',
                          ),
                          VocabTooltip(
                            message: wakareru,
                            inlineSpan: const TextSpan(
                              text: '別れた',
                            ),
                          ),
                          const TextSpan(
                            text: 'んだって。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('I heard that Miki-chan broke up with Yousuke.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'う～ん、',
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          VocabTooltip(
                            message: koto,
                            inlineSpan: TextSpan(
                              text: 'こと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: 'は、みきちゃんは、',
                          ),
                          VocabTooltip(
                            message: ima,
                            inlineSpan: const TextSpan(
                              text: '今',
                            ),
                          ),
                          VocabTooltip(
                            message: kareshi,
                            inlineSpan: const TextSpan(
                              text: '彼氏',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: iru,
                            inlineSpan: const TextSpan(
                              text: 'いない',
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          VocabTooltip(
                            message: koto,
                            inlineSpan: TextSpan(
                              text: 'こと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'Does that mean Miki-chan doesn’t have a boyfriend now?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                          VocabTooltip(
                            message: souThatWay,
                            inlineSpan: TextSpan(
                              text: 'そう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: 'いう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          VocabTooltip(
                            message: koto,
                            inlineSpan: TextSpan(
                              text: 'こと',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('That’s right. That’s what it means.'),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あんた – you (slang) 彼女 【かの・じょ】 – she; girlfriend 友達 【とも・だち】 – friend 言う 【い・う】 (u-verb) – to say 何 【なに／なん】 – what お酒 【お・さけ】 – alcohol 好き 【す・き】 (na-adj) – likable ある (u-verb) – to exist (inanimate) 生きる 【い・きる】 (ru-verb) – to live 多分 【た・ぶん】 – maybe 行く 【い・く】 (u-verb) – to go 思う 【おも・う】 (u-verb) – to think お金 【お・かね】 – money もう – already 帰る 【かえ・る】 (u-verb) – to go home 駄目 【だめ】 – no good 洋介 【よう・すけ】 – Yousuke (first name) 別れる 【わか・れる】 (ru-verb) – to separate; to break up こと – event, matter 今 【いま】 – now 彼氏【かれ・し】 – boyfriend いる (ru-verb) – to exist (animate) そう – (things are) that way We can attach the question marker 「か」 to 「という」 in order to add a questioning element. This construction is used when you want to rephrase or redefine something such as the following dialogue. Example Dialogue Ａ：みきちゃんは、あんたの彼女でしょう？ A: Miki-chan is your girlfriend, right? Ｂ：う～ん、彼女というか、友達というか、なんというか・・・ B: Um, you might say girlfriend, or friend, or something… This construction is used all the time, particularly in casual conversations. It can be used to correct something, come to a different conclusion, or even as an interjection. Examples お酒は好きというか、ないと生きていけないんだよ。 I like alcohol or rather, can’t live on without it. 多分行かないと思う。 というか、お金がないから、行けない。 Don’t think I’ll go. Or rather, can’t because there’s no money. というか、もう帰らないとだめですけど。 Rather than that, I have to go home already. Rather than using 「か」 to rephrase a conclusion, we can also simply use 「こと」 to sum up something without rephrasing anything. Example Dialogue Ａ：みきちゃんが洋介と別れたんだって。 A: I heard that Miki-chan broke up with Yousuke. Ｂ：う～ん、ということは、みきちゃんは、今彼氏がいないということ？ B: Does that mean Miki-chan doesn’t have a boyfriend now? Ａ：そう。そういうこと。 A: That’s right. That’s what it means.',
      grammars: ['というか','ということ'],
      keywords: ['iu','to iu','to iu ka','to iu koto','to','conclude','rephrase'],
    ),
    Section(
      heading: 'Using 「って」 or 「て」 for 「という」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('来年 【らい・ねん】 – next year'),
                  Text('留学 【りゅう・がく】 – study abroad'),
                  Text('する (exception) – to do'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('智子 【とも・こ】 – Tomoko (first name)'),
                  Text('こと – event, matter'),
                  Text('駄目 【だめ】 – no good'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('いい (i-adj) – good'),
                  Text('皆 【みんな】 – everybody'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('今 【いま】 – now'),
                  Text('彼氏【かれ・し】 – boyfriend'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('もう – already'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'As mentioned in the previous lesson, 「って」 is very often used in causal slang in place of 「と」, because it allows us to leave out the rest of the sentence and assume context (or just plain assumption) will take care of the rest. We already saw that we can use 「って」 to replace 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 as well. However, since we just learned how to use 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to do much more than just simply say something, there is a limit to just how much you can leave out. In any case, 「って」 will allow us to leave out not only 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 but also any accompanying particles as you can see in the following example.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: rainen,
                              inlineSpan: const TextSpan(
                                text: '来年',
                              ),
                            ),
                            VocabTooltip(
                              message: ryuugaku,
                              inlineSpan: const TextSpan(
                                text: '留学',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: 'いう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'のは',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: tomoko,
                              inlineSpan: const TextSpan(
                                text: '智子',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The studying abroad next year thing, is that Tomoko?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: rainen,
                              inlineSpan: const TextSpan(
                                text: '来年',
                              ),
                            ),
                            VocabTooltip(
                              message: ryuugaku,
                              inlineSpan: const TextSpan(
                                text: '留学',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: tomoko,
                              inlineSpan: const TextSpan(
                                text: '智子',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The studying abroad next year thing, is that Tomoko?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「だって」 is also another phrase that leaves out just about everything. By convention, it is used to express disagreement or dissatisfaction usually to whine, complain, or to make an excuse but you can’t tell what it means just from looking at it. It is an abbreviation of something along the lines of 「とは',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いって',
                    ),
                  ),
                  const TextSpan(
                    text: 'も」 meaning, “even if that was the case”.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: suru,
                            inlineSpan: const TextSpan(
                              text: 'しない',
                            ),
                          ),
                          const TextSpan(
                            text: 'と',
                          ),
                          VocabTooltip(
                            message: dame,
                            inlineSpan: const TextSpan(
                              text: 'だめ',
                            ),
                          ),
                          const TextSpan(
                            text: 'だよ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Have to do it, you know.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'だって',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(
                              text: '時間',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ない',
                            ),
                          ),
                          const TextSpan(
                            text: 'から',
                          ),
                          VocabTooltip(
                            message: dekiru,
                            inlineSpan: const TextSpan(
                              text: 'できない',
                            ),
                          ),
                          const TextSpan(
                            text: 'よ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'But (even so), can’t do it because there is no time.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行かなくて',
                            ),
                          ),
                          const TextSpan(
                            text: 'も',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(
                            text: 'よ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Don’t have to go, you know.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'だって',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: minna,
                            inlineSpan: const TextSpan(
                              text: 'みんな',
                            ),
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行く',
                            ),
                          ),
                          const TextSpan(
                            text: 'って。',
                          ),
                          VocabTooltip(
                            message: watashi,
                            inlineSpan: const TextSpan(
                              text: '私',
                            ),
                          ),
                          const TextSpan(
                            text: 'も。',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行かない',
                            ),
                          ),
                          const TextSpan(
                            text: 'と。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'But (even so), everybody said they’re going. I have to go too.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In some cases, the small 「つ」 is left out and just 「て」 is used instead of 「って」. This is done (as is usually the case for slang) in order to make things easier to say. In general, this is when there is nothing before the 「て」 or when the sound that comes before it doesn’t require the explicit separation the 「っ」 gives us in order to be understood.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'て',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、みきちゃんは、',
                            ),
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            VocabTooltip(
                              message: kareshi,
                              inlineSpan: const TextSpan(
                                text: '彼氏',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いない',
                              ),
                            ),
                            TextSpan(
                              text: 'て',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Does that mean Miki-chan doesn’t have a boyfriend now?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'て',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: 'いう',
                              ),
                            ),
                            const TextSpan(
                              text: 'か、',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰らない',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'ですけど。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Rather than that, I have to go home already.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Since slang tends to be used in whichever way the person feels like, there are no definite rules defining whether you should use 「って」 or 「て」. However, 「て」 is generally not used to express what people have actually said or heard, which is why it wasn’t covered in the last lesson.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'みきちゃんが、',
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'こない',
                              ),
                            ),
                            TextSpan(
                              text: 'て',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(Can’t use 「て」 for something actually said)',
                      ),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'みきちゃんが、',
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'こない',
                              ),
                            ),
                            TextSpan(
                              text: 'って',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Miki-chan says she isn’t coming tomorrow.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 来年 【らい・ねん】 – next year 留学 【りゅう・がく】 – study abroad する (exception) – to do 言う 【い・う】 (u-verb) – to say 智子 【とも・こ】 – Tomoko (first name) こと – event, matter 駄目 【だめ】 – no good 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 出来る 【で・き・る】 (ru-verb) – to be able to do 行く 【い・く】 (u-verb) – to go いい (i-adj) – good 皆 【みんな】 – everybody 私 【わたし】 – me; myself; I 今 【いま】 – now 彼氏【かれ・し】 – boyfriend いる (ru-verb) – to exist (animate) もう – already 帰る 【かえ・る】 (u-verb) – to go home As mentioned in the previous lesson, 「って」 is very often used in causal slang in place of 「と」, because it allows us to leave out the rest of the sentence and assume context (or just plain assumption) will take care of the rest. We already saw that we can use 「って」 to replace 「という」 as well. However, since we just learned how to use 「という」 to do much more than just simply say something, there is a limit to just how much you can leave out. In any case, 「って」 will allow us to leave out not only 「いう」 but also any accompanying particles as you can see in the following example. Examples 来年留学するというのは、智子のこと？ The studying abroad next year thing, is that Tomoko? 来年留学するって智子のこと？ The studying abroad next year thing, is that Tomoko? 「だって」 is also another phrase that leaves out just about everything. By convention, it is used to express disagreement or dissatisfaction usually to whine, complain, or to make an excuse but you can’t tell what it means just from looking at it. It is an abbreviation of something along the lines of 「とはいっても」 meaning, “even if that was the case”. Example 1 Ａ：しないとだめだよ。 A: Have to do it, you know. Ｂ：だって、時間がないからできないよ。 B: But (even so), can’t do it because there is no time. Example 2 Ａ：行かなくてもいいよ。 A: Don’t have to go, you know. Ｂ：だって、みんな行くって。 私も。 行かないと。 B: But (even so), everybody said they’re going. I have to go too. In some cases, the small 「つ」 is left out and just 「て」 is used instead of 「って」. This is done (as is usually the case for slang) in order to make things easier to say. In general, this is when there is nothing before the 「て」 or when the sound that comes before it doesn’t require the explicit separation the 「っ」 gives us in order to be understood. Examples てことは、みきちゃんは、今彼氏がいないてこと？ Does that mean Miki-chan doesn’t have a boyfriend now? ていうか、もう帰らないとだめですけど。 Rather than that, I have to go home already. Since slang tends to be used in whichever way the person feels like, there are no definite rules defining whether you should use 「って」 or 「て」. However, 「て」 is generally not used to express what people have actually said or heard, which is why it wasn’t covered in the last lesson. みきちゃんが、明日こないて。(Can’t use 「て」 for something actually said) みきちゃんが、明日こないって。 Miki-chan says she isn’t coming tomorrow.',
      grammars: ['って','て'],
      keywords: ['tte','te'],
    ),
    Section(
      heading: 'Saying 「ゆう」 instead of 「いう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('もう – already'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('そう – (things are) that way'),
                  Text('こと – event, matter'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Because the 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 construction is used so often, there are a lot of different variations and slang based on it. While I do not plan on covering all of them here, you can check out casual patterns and slang in the miscellaneous section for yet even more slang derived from 「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The last thing I’m am going to briefly mention here is the use of 「',
                  ),
                  VocabTooltip(
                    message: yuu,
                    inlineSpan: const TextSpan(
                      text: 'ゆう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 instead of 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text: '」. In conversations, it is quite normal to say 「',
                  ),
                  VocabTooltip(
                    message: yuu,
                    inlineSpan: const TextSpan(
                      text: 'ゆう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 instead of 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text: '」. 「',
                  ),
                  VocabTooltip(
                    message: yuu,
                    inlineSpan: const TextSpan(
                      text: 'ゆう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is easier to say because it is simply one letter with a long vowel sound instead of the two different vowel sounds of 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                    text: '」. ',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'て',
                            ),
                            VocabTooltip(
                              message: yuu,
                              inlineSpan: TextSpan(
                                text: 'ゆう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か、',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰らない',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'ですけど。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Rather than that, I have to go home already.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: souLikeThat,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            VocabTooltip(
                              message: yuu,
                              inlineSpan: TextSpan(
                                text: 'ゆう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'じゃないって！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I said it’s not like that (lit: it’s not that type of thing)!',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary もう – already 帰る 【かえ・る】 (u-verb) – to go home そう – (things are) that way こと – event, matter Because the 「という」 construction is used so often, there are a lot of different variations and slang based on it. While I do not plan on covering all of them here, you can check out casual patterns and slang in the miscellaneous section for yet even more slang derived from 「という」. The last thing I’m am going to briefly mention here is the use of 「ゆう」 instead of 「いう」. In conversations, it is quite normal to say 「ゆう」 instead of 「いう」. 「ゆう」 is easier to say because it is simply one letter with a long vowel sound instead of the two different vowel sounds of 「いう」. Examples てゆうか、もう帰らないとだめですけど。 Rather than that, I have to go home already. そうゆうことじゃないって！ I said it’s not like that (lit: it’s not that type of thing)!',
      grammars: ['ゆう'],
      keywords: ['yuu'],
    ),
  ],
);
