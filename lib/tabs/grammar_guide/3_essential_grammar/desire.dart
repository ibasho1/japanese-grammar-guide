import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson desire = Lesson(
  title: 'Desire and Suggestions',
  sections: [
    Section(
      heading: 'How to get your way in Japan',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We will now learn how to say what you want either by just coming out and saying it or by making discreet suggestions. The major topics we will cover will be the 「たい」 conjugation and the volitional form. We will also learn specialized uses of the 「たら」 and 「ば」 conditionals to offer advice.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We will now learn how to say what you want either by just coming out and saying it or by making discreet suggestions. The major topics we will cover will be the 「たい」 conjugation and the volitional form. We will also learn specialized uses of the 「たら」 and 「ば」 conditionals to offer advice.',
      grammars: [],
      keywords: ['want','desire','suggestion','suggest'],
    ),
    Section(
      heading: 'Verbs you want to do with 「たい」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('何 【なに】 – what'),
                  Text('する (exception) – to do'),
                  Text('温泉 【おん・せん】 – hotspring'),
                  Text('ケーキ – cake'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('ずっと – long; far'),
                  Text('一緒 【いっ・しょ】 – together'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('犬 【いぬ】 – dog'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can express verbs that you want to perform with the 「たい」 form. All you need to do is add 「たい」 to the stem of the verb. However, unlike most conjugations we learned where the verb turns into a ru-verb, this form actually transforms the verb into an i-adjective (notice how 「たい」 conveniently ends in 「い」). This makes sense because the conjugated form is a description of something that you want to do. Once you have the 「たい」 form, you can then conjugate it the same as you would any other i-adjective. However, the 「たい」 form is different from regular i-adjectives because it is derived from a verb. Particles we normally associate with verbs such as 「を」、「に」、「へ」、or 「で」 can all be used with the 「たい」 form in addition to the particles commonly used with regular adjectives such as 「は」 and 「が」.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: '「たい」 conjugations'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: 'Past Tense'),
                            TableHeading(text: 'Te-form'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: 'Non-Past'),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行きたい',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行きたくない',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: 'Past'),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行きたかった',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行きたくなかった',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'したい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What do you want to do?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: onsen,
                              inlineSpan: const TextSpan(
                                text: '温泉',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行きたい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I want to go to hot spring.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: keeki,
                              inlineSpan: const TextSpan(
                                text: 'ケーキ',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べたくない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'の？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You don’t want to eat cake?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べたくなかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'けど',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べたく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I didn’t want to eat it but I became wanting to eat.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Example 4 was very awkward to translate but is quite simple in Japanese if you refer to the section about using 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with i-adjectives”. The past tense of the verb 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 was used to create “became want to eat”. Here’s a tongue twister using the negative 「～たくない」 and past-tense of 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(text: '」： 「'),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べたくなくなった',
                    ),
                  ),
                  const TextSpan(
                      text: '」 meaning “became not wanting to eat”.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'This may seem obvious but 「'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 cannot have a 「たい」 form because inanimate objects cannot want anything. However, 「'),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 can be used with the 「たい」 form in examples like the one below.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zutto,
                              inlineSpan: const TextSpan(
                                text: 'ずっと',
                              ),
                            ),
                            VocabTooltip(
                              message: issho,
                              inlineSpan: const TextSpan(
                                text: '一緒',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: TextSpan(
                                text: 'いたい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I want to be together forever. (lit: Want to exist together for long time.)',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Also, you can only use the 「たい」 form for the first-person because you cannot read other people’s mind to see what they want to do. For referring to anyone beside yourself, it is normal to use expressions such as, “I think he wants to…” or “She said that she wants to…” We will learn how to say such expressions in a later lesson. Of course, if you’re asking a question, you can just use the 「たい」 form because you’re not presuming to know anything.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: TextSpan(
                                text: '遊びたい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Do you want to play with dog?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 行く 【い・く】 (u-verb) – to go 何 【なに】 – what する (exception) – to do 温泉 【おん・せん】 – hotspring ケーキ – cake 食べる 【た・べる】 (ru-verb) – to eat ずっと – long; far 一緒 【いっ・しょ】 – together いる (ru-verb) – to exist (animate) 犬 【いぬ】 – dog 遊ぶ 【あそ・ぶ】 (u-verb) – to play You can express verbs that you want to perform with the 「たい」 form. All you need to do is add 「たい」 to the stem of the verb. However, unlike most conjugations we learned where the verb turns into a ru-verb, this form actually transforms the verb into an i-adjective (notice how 「たい」 conveniently ends in 「い」). This makes sense because the conjugated form is a description of something that you want to do. Once you have the 「たい」 form, you can then conjugate it the same as you would any other i-adjective. However, the 「たい」 form is different from regular i-adjectives because it is derived from a verb. Particles we normally associate with verbs such as 「を」、「に」、「へ」、or 「で」 can all be used with the 「たい」 form in addition to the particles commonly used with regular adjectives such as 「は」 and 「が」. Examples 何をしたいですか。 What do you want to do? 温泉に行きたい。 I want to go to hot spring. ケーキ、食べたくないの？ You don’t want to eat cake? 食べたくなかったけど食べたくなった。 I didn’t want to eat it but I became wanting to eat. Example 4 was very awkward to translate but is quite simple in Japanese if you refer to the section about using 「なる」 with i-adjectives”. The past tense of the verb 「なる」 was used to create “became want to eat”. Here’s a tongue twister using the negative 「～たくない」 and past-tense of 「なる」： 「食べたくなくなった」 meaning “became not wanting to eat”. This may seem obvious but 「ある」 cannot have a 「たい」 form because inanimate objects cannot want anything. However, 「いる」 can be used with the 「たい」 form in examples like the one below. ずっと一緒にいたい。 I want to be together forever. (lit: Want to exist together for long time.) Also, you can only use the 「たい」 form for the first-person because you cannot read other people’s mind to see what they want to do. For referring to anyone beside yourself, it is normal to use expressions such as, “I think he wants to…” or “She said that she wants to…” We will learn how to say such expressions in a later lesson. Of course, if you’re asking a question, you can just use the 「たい」 form because you’re not presuming to know anything. 犬と遊びたいですか。 Do you want to play with dog?',
      grammars: ['～たい'],
      keywords: ['want', 'tai'],
    ),
    Section(
      heading: 'Indicating things you want or want done using 「欲しい」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('欲しい 【ほ・しい】 (i-adj) – wanted; desirable'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('大きい 【おお・きい】(i-adj) – big'),
                  Text('縫いぐるみ 【ぬ・いぐるみ】 – stuffed doll'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('部屋 【へ・や】 – room'),
                  Text('きれい (na-adj) – pretty; clean'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'In English, we employ a verb to say that we want something. In Japanese, “to want” is actually an i-adjective and not a verb. We saw something similar with 「'),
                  VocabTooltip(
                    message: suki,
                    inlineSpan: const TextSpan(
                      text: '好き',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 which is an adjective while “to like” in English is a verb. While I didn’t get too much into the workings of 「'),
                  VocabTooltip(
                    message: suki,
                    inlineSpan: const TextSpan(
                      text: '好き',
                    ),
                  ),
                  const TextSpan(
                      text: '」, I have dedicated a whole section to 「'),
                  VocabTooltip(
                    message: hoshii,
                    inlineSpan: const TextSpan(
                      text: '欲しい',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 because it means, “to want something done” when combined with the te-form of a verb. We will learn a more polite and appropriate way to make requests in the “Making Requests” lesson instead of saying, “I want this done.”'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Though not a set rule, whenever words come attached to the te-form of a verb to serve a special grammatical function, it is customary to write it in hiragana. This is because kanji is already used for the verb and the attached word becomes part of that verb.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: const TextSpan(
                                text: '大きい',
                              ),
                            ),
                            VocabTooltip(
                              message: nuigurumi,
                              inlineSpan: const TextSpan(
                                text: '縫いぐるみ',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: TextSpan(
                                text: '欲しい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I want a big stuffed doll!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: TextSpan(
                                text: '欲しい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'んだけど・・・。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I want it all eaten but…',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kirei,
                              inlineSpan: const TextSpan(
                                text: 'きれい',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: TextSpan(
                                text: '欲しい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'のよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It is that I want the room cleaned up, you know.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Like I mentioned, there are more appropriate ways to ask for things which we won’t go into until later. This grammar is not used too often but is included for completeness.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 欲しい 【ほ・しい】 (i-adj) – wanted; desirable 好き 【す・き】 (na-adj) – likable; desirable 大きい 【おお・きい】(i-adj) – big 縫いぐるみ 【ぬ・いぐるみ】 – stuffed doll 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 部屋 【へ・や】 – room きれい (na-adj) – pretty; clean In English, we employ a verb to say that we want something. In Japanese, “to want” is actually an i-adjective and not a verb. We saw something similar with 「好き」 which is an adjective while “to like” in English is a verb. While I didn’t get too much into the workings of 「好き」, I have dedicated a whole section to 「欲しい」 because it means, “to want something done” when combined with the te-form of a verb. We will learn a more polite and appropriate way to make requests in the “Making Requests” lesson instead of saying, “I want this done.” Though not a set rule, whenever words come attached to the te-form of a verb to serve a special grammatical function, it is customary to write it in hiragana. This is because kanji is already used for the verb and the attached word becomes part of that verb. Examples 大きい縫いぐるみが欲しい！ I want a big stuffed doll! 全部食べて欲しいんだけど・・・。 I want it all eaten but… 部屋をきれいにして欲しいのよ。 It is that I want the room cleaned up, you know. Like I mentioned, there are more appropriate ways to ask for things which we won’t go into until later. This grammar is not used too often but is included for completeness.',
      grammars: ['欲しい','～て欲しい'],
      keywords: ['hoshii','te hoshii','want'],
    ),
    Section(
      heading:
          'Making a motion to do something using the volitional form (casual)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('調べる 【しら・べる】 (ru-verb) – to investigate'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('取る 【と・る】 (u-verb) – to take'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('直る 【なお・る】 (u-verb) – to be fixed'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('今日 【きょう】 – today'),
                  Text('何 【なに】 – what'),
                  Text('テーマパーク – theme park'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('カレー – curry'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The term volitional here means a will to do something. In other words, the volitional form indicates that someone is setting out to do something. In the most common example, this simply translates into the English “let’s” or “shall we?” but we’ll also see how this form can be used to express an effort to do something in a lesson further along.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'To conjugate verbs into the volitional form for casual speech, there are two different rules for ru-verbs and u-verbs. For ru-verbs, you simply remove the 「る」 and add 「よう」. For u-verbs, you replace the / u / vowel sound with the / o / vowel sound and add 「う」.'),
                ],
              ),
              NotesSection(
                heading: 'Conjugations rules for the casual volitional form',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Remove the 「る」 and add 「よう」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + ',
                                  ),
                                  TextSpan(
                                    text: 'よう',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べよう',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    'Replace the / u / vowel sound with the / o / vowel sound and add 「う」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '入',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '入',
                                        ),
                                        TextSpan(
                                          text: 'ろ',
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + ',
                                  ),
                                  TextSpan(
                                    text: 'う',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入ろう',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here is a list of verbs you should be used to seeing by now.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample ru-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Volitional'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'よう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Volitional'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'す',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'そう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'く',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'こう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'ぐ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'ごう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぶ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぼう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'つ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'とう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'む',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'もう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'ろう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ぬ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'のう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'う',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'おう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exception Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Volitional'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'しよう',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'こよう',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'I doubt you will ever use 「'),
                  VocabTooltip(
                    message: shinu,
                    inlineSpan: const TextSpan(
                      text: '死のう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 (let’s die) but I left it in for completeness. Here are some more realistic examples.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What shall (we) do today?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: teemapaaku,
                              inlineSpan: const TextSpan(
                                text: 'テーマパーク',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行こう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Let’s go to theme park!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What shall (we) eat tomorrow?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: karee,
                              inlineSpan: const TextSpan(
                                text: 'カレー',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Let’s eat curry!',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Remember, since you’re setting out to do something, it doesn’t make sense to have this verb in the past tense. Therefore, there is only one tense and if you were to replace 「'),
                  VocabTooltip(
                    message: ashita,
                    inlineSpan: const TextSpan(
                      text: '明日',
                    ),
                  ),
                  const TextSpan(
                      text: '」 in the third example with, let’s say, 「'),
                  VocabTooltip(
                    message: kinou,
                    inlineSpan: const TextSpan(
                      text: '昨日',
                    ),
                  ),
                  const TextSpan(
                      text: '」 then the sentence would make no sense.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 入る 【はい・る】 (u-verb) – to enter 着る 【き・る】 (ru-verb) – to wear 信じる 【しん・じる】 (ru-verb) – to believe 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 出る 【で・る】 (ru-verb) – to come out 掛ける 【か・ける】 (ru-verb) – to hang 捨てる 【す・てる】 (ru-verb) – to throw away 調べる 【しら・べる】 (ru-verb) – to investigate 話す 【はな・す】 (u-verb) – to speak 書く 【か・く】 (u-verb) – to write 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 取る 【と・る】 (u-verb) – to take 聞く 【き・く】 (u-verb) – to ask; to listen 泳ぐ 【およ・ぐ】 (u-verb) – to swim 遊ぶ 【あそ・ぶ】 (u-verb) – to play 直る 【なお・る】 (u-verb) – to be fixed 死ぬ 【し・ぬ】 (u-verb) – to die 買う 【か・う】 (u-verb) – to buy する (exception) – to do 来る 【く・る】 (exception) – to come 今日 【きょう】 – today 何 【なに】 – what テーマパーク – theme park 行く 【い・く】 (u-verb) – to go 明日 【あした】 – tomorrow カレー – curry The term volitional here means a will to do something. In other words, the volitional form indicates that someone is setting out to do something. In the most common example, this simply translates into the English “let’s” or “shall we?” but we’ll also see how this form can be used to express an effort to do something in a lesson further along. To conjugate verbs into the volitional form for casual speech, there are two different rules for ru-verbs and u-verbs. For ru-verbs, you simply remove the 「る」 and add 「よう」. For u-verbs, you replace the / u / vowel sound with the / o / vowel sound and add 「う」. Conjugations rules for the casual volitional form For ru-verbs: Remove the 「る」 and add 「よう」 Example: 食べる → 食べ + よう → 食べよう For u-verbs: Replace the / u / vowel sound with the / o / vowel sound and add 「う」 Example: 入る → 入ろ + う → 入ろう Here is a list of verbs you should be used to seeing by now. Examples I doubt you will ever use 「死のう」 (let’s die) but I left it in for completeness. Here are some more realistic examples. 今日は何をしようか？ What shall (we) do today? テーマパークに行こう！ Let’s go to theme park! 明日は何を食べようか？ What shall (we) eat tomorrow? カレーを食べよう！ Let’s eat curry! Remember, since you’re setting out to do something, it doesn’t make sense to have this verb in the past tense. Therefore, there is only one tense and if you were to replace 「明日」 in the third example with, let’s say, 「昨日」 then the sentence would make no sense.',
      grammars: ['～よう'],
      keywords: ['volitional', 'let’s', 'casual'],
    ),
    Section(
      heading:
          'Making a motion to do something using the volitional form (polite)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('今日 【きょう】 – today'),
                  Text('何 【なに】 – what'),
                  Text('テーマパーク – theme park'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('カレー – curry'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The conjugation for the polite form is even simpler. All you have to do is add 「～ましょう」 to the stem of the verb. Similar to the masu-form, verbs in this form must always come at the end of the sentence. In fact, all polite endings must always come at the end and nowhere else as we’ve already seen.'),
                ],
              ),
              NotesSection(
                heading: 'Conjugations rules for the polite volitional form',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For all verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Add 「～ましょう」 to the stem of the verb',
                              ),
                            ],
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + ',
                                  ),
                                  TextSpan(
                                    text: 'ましょう',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べましょう',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '入',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '入',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + ',
                                  ),
                                  TextSpan(
                                    text: 'ましょう',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入りましょう',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Volitional'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'し',
                                      ),
                                      TextSpan(
                                        text: 'ましょう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'き',
                                      ),
                                      TextSpan(
                                        text: 'ましょう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: const TextSpan(
                                    text: '寝る',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'ましょう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行く',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '行き',
                                      ),
                                      TextSpan(
                                        text: 'ましょう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: const TextSpan(
                                    text: '遊ぶ',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊び',
                                      ),
                                      TextSpan(
                                        text: 'ましょう',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Again, there’s nothing new here, just the polite version of the volitional form.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しましょう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What shall (we) do today?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: teemapaaku,
                              inlineSpan: const TextSpan(
                                text: 'テーマパーク',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行きましょう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Let’s go to theme park!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べましょう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'What shall (we) eat tomorrow?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: karee,
                              inlineSpan: const TextSpan(
                                text: 'カレー',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べましょう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Let’s eat curry!',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 入る 【はい・る】 (u-verb) – to enter する (exception) – to do 来る 【く・る】 (exception) – to come 寝る 【ね・る】 (ru-verb) – to sleep 行く 【い・く】 (u-verb) – to go 遊ぶ 【あそ・ぶ】 (u-verb) – to play 今日 【きょう】 – today 何 【なに】 – what テーマパーク – theme park 明日 【あした】 – tomorrow カレー – curry The conjugation for the polite form is even simpler. All you have to do is add 「～ましょう」 to the stem of the verb. Similar to the masu-form, verbs in this form must always come at the end of the sentence. In fact, all polite endings must always come at the end and nowhere else as we’ve already seen. Conjugations rules for the polite volitional form For all verbs: Add 「～ましょう」 to the stem of the verb 食べる → 食べ + ましょう → 食べましょう入る → 入り + ましょう → 入りましょう Examples Again, there’s nothing new here, just the polite version of the volitional form. 今日は何をしましょうか？ What shall (we) do today? テーマパークに行きましょう！ Let’s go to theme park! 明日は何を食べましょうか？ What shall (we) eat tomorrow? カレーを食べましょう！ Let’s eat curry!',
      grammars: ['～ましょう'],
      keywords: ['volitional', 'let’s', 'casual'],
    ),
    Section(
      heading: 'Making suggestions using the 「ば」 or 「たら」 conditional',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('銀行 【ぎん・こう】 – bank'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('たまに – once in a while'),
                  Text('両親【りょう・しん】 – parents'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You can make suggestions by using the 「ば」 or 「たら」 conditional and adding 「'),
                  VocabTooltip(
                    message: dou,
                    inlineSpan: const TextSpan(
                      text: 'どう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. This literally means, “If you do [X], how is it?” In English, this would become, “How about doing [X]?” Grammatically, there’s nothing new here but it is a commonly used set phrase.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ginkou,
                              inlineSpan: const TextSpan(
                                text: '銀行',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行ったら',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: dou,
                              inlineSpan: TextSpan(
                                text: 'どう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'How about going to bank?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tamani,
                              inlineSpan: const TextSpan(
                                text: 'たまに',
                              ),
                            ),
                            const TextSpan(
                              text: 'ご',
                            ),
                            VocabTooltip(
                              message: ryoushin,
                              inlineSpan: const TextSpan(
                                text: '両親',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: hanasu,
                              inlineSpan: TextSpan(
                                text: '話せば',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: dou,
                              inlineSpan: TextSpan(
                                text: 'どう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'How about talking with your parents once in a while?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 銀行 【ぎん・こう】 – bank 行く 【い・く】 (u-verb) – to go たまに – once in a while 両親【りょう・しん】 – parents 話す 【はな・す】 (u-verb) – to speak You can make suggestions by using the 「ば」 or 「たら」 conditional and adding 「どう」. This literally means, “If you do [X], how is it?” In English, this would become, “How about doing [X]?” Grammatically, there’s nothing new here but it is a commonly used set phrase. 銀行に行ったらどうですか。 How about going to bank? たまにご両親と話せばどう？ How about talking with your parents once in a while?',
      grammars: ['～ばどう','～たらどう'],
      keywords: ['suggestion', 'suggest', 'contional', 'ba', 'tara', 'dou'],
    ),
  ],
);
