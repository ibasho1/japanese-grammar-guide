import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson favors = Lesson(
  title: 'Giving and Receiving',
  sections: [
    Section(
      heading: 'Japanese people like gifts',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('お歳暮 【お・せい・ぼ】 – year-end presents'),
                  Text('お中元 【お・ちゅう・げん】 – Bon festival gifts'),
                  Text('あげる (ru-verb) – to give; to raise'),
                  Text('くれる (ru-verb) – to give'),
                  Text('もらう (u-verb) – to receive'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'One thing about Japanese culture is that they’re big on giving gifts. There are many different customs involving giving and receiving gifts (',
                  ),
                  VocabTooltip(
                    message: oseibo,
                    inlineSpan: const TextSpan(
                      text: 'お歳暮',
                    ),
                  ),
                  const TextSpan(
                    text: '、',
                  ),
                  VocabTooltip(
                    message: ochuugen,
                    inlineSpan: const TextSpan(
                      text: 'お中元',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '、etc.) and when Japanese people go traveling, you can be sure that they’re going to be picking up souvenirs to take back as gifts. Even when attending marriages or funerals, people are expected to give a certain amount of money as a gift to help fund the ceremony. You can see why properly learning how to express the giving and receiving of favors and items is a very important and useful skill. For some reason, the proper use of 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」、and 「',
                  ),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 has always haunted people studying Japanese as being horribly complex and intractable. I hope to prove in this section that it is conceptually quite straightforward and simple.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary お歳暮 【お・せい・ぼ】 – year-end presents お中元 【お・ちゅう・げん】 – Bon festival gifts あげる (ru-verb) – to give; to raise くれる (ru-verb) – to give もらう (u-verb) – to receive One thing about Japanese culture is that they’re big on giving gifts. There are many different customs involving giving and receiving gifts (お歳暮、お中元、etc.) and when Japanese people go traveling, you can be sure that they’re going to be picking up souvenirs to take back as gifts. Even when attending marriages or funerals, people are expected to give a certain amount of money as a gift to help fund the ceremony. You can see why properly learning how to express the giving and receiving of favors and items is a very important and useful skill. For some reason, the proper use of 「あげる」、「くれる」、and 「もらう」 has always haunted people studying Japanese as being horribly complex and intractable. I hope to prove in this section that it is conceptually quite straightforward and simple.',
      grammars: [],
      keywords: ['gift', 'giving','give','receiving','receive','get'],
    ),
    Section(
      heading: 'When to use 「あげる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('あげる (ru-verb) – to give; to raise'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('プレゼント – present'),
                  Text('これ – this'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('車 【くるま】 – car'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('代わり 【か・わり】 – substitute'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('学生 【がく・せい】 – student'),
                  Text('父【ちち】 – father'),
                  Text('いい (i-adj) – good'),
                  Text('こと – event, matter'),
                  Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is the Japanese word for “to give” seen from the speaker’s point of view. You must use this verb when you are giving something or doing something for someone else.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I gave present to friend.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            TextSpan(
                              text: 'あげる',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll give this to teacher.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'In order to express the giving of a favor (verb) you must use the ever useful te-form and then attach 「'),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. This applies to all the other sections in this lesson as well.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kuruma,
                              inlineSpan: const TextSpan(
                                text: '車',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll give you the favor of buying a car.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kawari,
                              inlineSpan: const TextSpan(
                                text: '代わり',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll give you the favor of going in your place.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'For third-person, this verb is used when the speaker is looking at it from the giver’s point of view. We’ll see the significance of this when we examine the verb 「'),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(text: '」 next.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The student give this to teacher. (looking at it from the student’s point of view)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: chichi,
                              inlineSpan: const TextSpan(
                                text: '父',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: oshieruInform,
                              inlineSpan: TextSpan(
                                text: '教えて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Friend gave the favor of teaching something good to my dad. (looking at it from the friend’s point of view)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あげる (ru-verb) – to give; to raise 私 【わたし】 – me; myself; I 友達 【とも・だち】 – friend プレゼント – present これ – this 先生 【せん・せい】 – teacher 車 【くるま】 – car 買う 【か・う】 (u-verb) – to buy 代わり 【か・わり】 – substitute 行く 【い・く】 (u-verb) – to go 学生 【がく・せい】 – student 父【ちち】 – father いい (i-adj) – good こと – event, matter 教える 【おし・える】 (ru-verb) – to teach; to inform 「あげる」 is the Japanese word for “to give” seen from the speaker’s point of view. You must use this verb when you are giving something or doing something for someone else. Examples 私が友達にプレゼントをあげた。 I gave present to friend. これは先生にあげる。 I’ll give this to teacher. In order to express the giving of a favor (verb) you must use the ever useful te-form and then attach 「あげる」. This applies to all the other sections in this lesson as well. 車を買ってあげるよ。 I’ll give you the favor of buying a car. 代わりに行ってあげる。 I’ll give you the favor of going in your place. For third-person, this verb is used when the speaker is looking at it from the giver’s point of view. We’ll see the significance of this when we examine the verb 「くれる」 next. 学生がこれを先生にあげる。 The student give this to teacher. (looking at it from the student’s point of view) 友達が父にいいことを教えてあげた。 Friend gave the favor of teaching something good to my dad. (looking at it from the friend’s point of view)',
      grammars: ['あげる'],
      keywords: ['ageru', 'give'],
    ),
    Section(
      heading: 'Using 「やる」 to mean 「あげる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('犬 【いぬ】 – dog'),
                  Text('餌 【えさ】 – food for animals'),
                  Text('やる (u-verb) – to do'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Usually used for pets, animals, and such, you can substitute 「'),
                  VocabTooltip(
                    message: yaru,
                    inlineSpan: const TextSpan(
                      text: 'やる',
                    ),
                  ),
                  const TextSpan(
                      text: '」, which normally means “to do”, for 「'),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. You shouldn’t use this type of 「やる」 for people because it is used when looking down on someone and can be offensive.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: esa,
                              inlineSpan: const TextSpan(
                                text: '餌',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yaru,
                              inlineSpan: TextSpan(
                                text: 'やった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Did you give the dog food?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'Here, 「'),
                  VocabTooltip(
                    message: yaru,
                    inlineSpan: const TextSpan(
                      text: 'やる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 does not mean “to do” but “to give”. You can tell because “doing food to dog” doesn’t make any sense.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 犬 【いぬ】 – dog 餌 【えさ】 – food for animals やる (u-verb) – to do Usually used for pets, animals, and such, you can substitute 「やる」, which normally means “to do”, for 「あげる」. You shouldn’t use this type of 「やる」 for people because it is used when looking down on someone and can be offensive. 犬に餌をやった？ Did you give the dog food? Here, 「やる」 does not mean “to do” but “to give”. You can tell because “doing food to dog” doesn’t make any sense.',
      grammars: ['やる'],
      keywords: ['yaru', 'give'],
    ),
    Section(
      heading: 'When to use 「くれる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('くれる (ru-verb) – to give'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('プレゼント – present'),
                  Text('これ – this'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('車 【くるま】 – car'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('代わり 【か・わり】 – substitute'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('学生 【がく・せい】 – student'),
                  Text('父【ちち】 – father'),
                  Text('いい (i-adj) – good'),
                  Text('こと – event, matter'),
                  Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                  Text('あげる (ru-verb) – to give; to raise'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is also a verb meaning “to give” but unlike 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, it is from the receiver’s point of view. You must use this verb when someone ',
                  ),
                  const TextSpan(
                    text: 'else',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' is giving something or doing something for you (effectively the opposite of 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                    text: '」).',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Friend gave present to me.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Teacher gave this to me.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kuruma,
                              inlineSpan: const TextSpan(
                                text: '車',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'の？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'You’ll give me the favor of buying a car for me?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kawari,
                              inlineSpan: const TextSpan(
                                text: '代わり',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Will you give me the favor of going in my place?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Similarly, when used in the third-person, the speaker is speaking from the receiver’s point of view and not the giver.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The teacher give this to student. (looking at it from the student’s point of view)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: chichi,
                              inlineSpan: const TextSpan(
                                text: '父',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: oshieru,
                              inlineSpan: TextSpan(
                                text: '教えて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Friend gave favor of teaching something good to my dad. (looking at it from the dad’s point of view)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The following diagram illustrates the direction of giving from the point of view of the speaker.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Center(child: Image.asset('assets/images/favordiag.gif')),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'From the speaker’s point of view, all the giving done to others “go up” to everybody else while the giving done by everybody else “goes down” to the speaker. This is probably related to the fact that there is an identical verb 「',
                  ),
                  VocabTooltip(
                    message: ageruRaise,
                    inlineSpan: const TextSpan(
                      text: '上げる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 meaning “to raise” that contains the character for “above” （',
                  ),
                  VocabTooltip(
                    message: ue,
                    inlineSpan: const TextSpan(
                      text: '上',
                    ),
                  ),
                  const TextSpan(
                    text: '） and that the honorific version of 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(
                      text: '下さる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 with the character for down （',
                  ),
                  VocabTooltip(
                    message: shita,
                    inlineSpan: const TextSpan(
                      text: '下',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '）. This restriction allows us to make certain deductions from vague sentences like the following:',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: oshieru,
                              inlineSpan: const TextSpan(
                                text: '教えて',
                              ),
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'んですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Teacher, will you be the one to give favor of teaching to… [anybody other than the speaker]?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Because all giving done to the speaker must always use 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, we know that the teacher must be doing it for someone else and ',
                  ),
                  const TextSpan(
                    text: 'not the speaker',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        '. The speaker is also looking at it from the teacher’s point of view as doing a favor for someone else.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: oshieru,
                              inlineSpan: const TextSpan(
                                text: '教えて',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'んですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Teacher, will you be the one to give favor of teaching to… [anybody including the speaker]?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Because the giver is not the speaker, the teacher is either giving to the speaker or anyone else. The speaker is viewing it from the receiver’s point of view as receiving a favor done by the teacher.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Let’s see some mistakes to watch out for.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '「',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれる',
                              ),
                            ),
                            const TextSpan(
                              text:
                                  '」 is being used as giving done by the speaker. (Wrong)',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげました',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I gave favor of eating it all. (Correct)',
                      ),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: TextSpan(
                                text: 'あげた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '「',
                            ),
                            VocabTooltip(
                              message: ageru,
                              inlineSpan: const TextSpan(
                                text: 'あげる',
                              ),
                            ),
                            const TextSpan(
                              text:
                                  '」 is being used as giving to the speaker. (Wrong)',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Friend gave present to me. (Correct)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary くれる (ru-verb) – to give 友達 【とも・だち】 – friend 私 【わたし】 – me; myself; I プレゼント – present これ – this 先生 【せん・せい】 – teacher 車 【くるま】 – car 買う 【か・う】 (u-verb) – to buy 代わり 【か・わり】 – substitute 行く 【い・く】 (u-verb) – to go 学生 【がく・せい】 – student 父【ちち】 – father いい (i-adj) – good こと – event, matter 教える 【おし・える】 (ru-verb) – to teach; to inform あげる (ru-verb) – to give; to raise 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 「くれる」 is also a verb meaning “to give” but unlike 「あげる」, it is from the receiver’s point of view. You must use this verb when someone else is giving something or doing something for you (effectively the opposite of 「あげる」). Examples 友達が私にプレゼントをくれた。 Friend gave present to me. これは、先生がくれた。 Teacher gave this to me. 車を先生買ってくれるの？ You’ll give me the favor of buying a car for me? 代わりに行ってくれる？ Will you give me the favor of going in my place? Similarly, when used in the third-person, the speaker is speaking from the receiver’s point of view and not the giver. 先生がこれを学生にくれる。 The teacher give this to student. (looking at it from the student’s point of view) 友達が父にいいことを教えてくれた。 Friend gave favor of teaching something good to my dad. (looking at it from the dad’s point of view) The following diagram illustrates the direction of giving from the point of view of the speaker. From the speaker’s point of view, all the giving done to others “go up” to everybody else while the giving done by everybody else “goes down” to the speaker. This is probably related to the fact that there is an identical verb 「上げる」 meaning “to raise” that contains the character for “above” （上） and that the honorific version of 「くれる」 is 「下さる」 with the character for down （下）. This restriction allows us to make certain deductions from vague sentences like the following: 先生が教えてあげるんですか。 Teacher, will you be the one to give favor of teaching to… [anybody other than the speaker]? Because all giving done to the speaker must always use 「くれる」, we know that the teacher must be doing it for someone else and not the speaker. The speaker is also looking at it from the teacher’s point of view as doing a favor for someone else. 先生が教えてくれるんですか。 Teacher, will you be the one to give favor of teaching to… [anybody including the speaker]? Because the giver is not the speaker, the teacher is either giving to the speaker or anyone else. The speaker is viewing it from the receiver’s point of view as receiving a favor done by the teacher. Let’s see some mistakes to watch out for. 私が全部食べてくれました。「くれる」 is being used as giving done by the speaker. (Wrong) 私が全部食べてあげました。 I gave favor of eating it all. (Correct) 友達がプレゼントを私にあげた。「あげる」 is being used as giving to the speaker. (Wrong) 私が全部食べてくれた。 Friend gave present to me. (Correct)',
      grammars: ['くれる'],
      keywords: ['kureru', 'give'],
    ),
    Section(
      heading: 'When to use 「もらう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('私 【わたし】 – me; myself; I'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('プレゼント – present'),
                  Text('もらう (u-verb) – to receive'),
                  Text('これ – this'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('チェック – check'),
                  Text('する (exception) – to do'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('無理 【む・り】 – impossible'),
                  Text('その – that （abbr. of それの）'),
                  Text('時計 【と・けい】 – watch; clock'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: '「'),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 meaning, “to receive” has only one version unlike 「'),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 so there’s very little to explain. One thing to point out is that since you receive '),
                  const TextSpan(
                    text: 'from',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' someone, 「から」 is also appropriate in addition to the 「に」 target particle.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: TextSpan(
                                text: 'もらった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I received present from friend.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            TextSpan(
                              text: 'から',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: purezento,
                              inlineSpan: const TextSpan(
                                text: 'プレゼント',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: TextSpan(
                                text: 'もらった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I received present from friend.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: TextSpan(
                                text: 'もらった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'About this, received the favor of buying it from friend.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: chekku,
                              inlineSpan: const TextSpan(
                                text: 'チェック',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: TextSpan(
                                text: 'もらいたかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なくて',
                              ),
                            ),
                            VocabTooltip(
                              message: muri,
                              inlineSpan: const TextSpan(
                                text: '無理',
                              ),
                            ),
                            const TextSpan(
                              text: 'だった。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'I wanted to receive the favor of checking homework but there was no time and it was impossible.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: '「'),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is seen from the perspective of the receiver, so in the case of first-person, others usually don’t receive things from you. However, you might want to use 「'),
                  VocabTooltip(
                    message: watashi,
                    inlineSpan: const TextSpan(
                      text: '私',
                    ),
                  ),
                  const TextSpan(text: 'から'),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 when you want to emphasize that fact that the other person received it from you. For instance, if you wanted to say, “Hey, I '),
                  TextSpan(
                    text: 'gave',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(text: ' you that!” you would use 「'),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(text: '」. However, you would use 「'),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(text: '」 if you wanted to say, “Hey, you '),
                  TextSpan(
                    text: 'got',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(text: ' that from me!”'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: tokei,
                              inlineSpan: const TextSpan(
                                text: '時計',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらった',
                              ),
                            ),
                            const TextSpan(
                              text: 'のよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(He) received that watch from me.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 私 【わたし】 – me; myself; I 友達 【とも・だち】 – friend プレゼント – present もらう (u-verb) – to receive これ – this 買う 【か・う】 (u-verb) – to buy 宿題 【しゅく・だい】 – homework チェック – check する (exception) – to do 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 無理 【む・り】 – impossible その – that （abbr. of それの） 時計 【と・けい】 – watch; clock 「もらう」 meaning, “to receive” has only one version unlike 「あげる／くれる」 so there’s very little to explain. One thing to point out is that since you receive from someone, 「から」 is also appropriate in addition to the 「に」 target particle. Examples 私が友達にプレゼントをもらった。 I received present from friend. 友達からプレゼントをもらった。 I received present from friend. これは友達に買ってもらった。 About this, received the favor of buying it from friend. 宿題をチェックしてもらいたかったけど、時間がなくて無理だった。 I wanted to receive the favor of checking homework but there was no time and it was impossible. 「もらう」 is seen from the perspective of the receiver, so in the case of first-person, others usually don’t receive things from you. However, you might want to use 「私からもらう」 when you want to emphasize that fact that the other person received it from you. For instance, if you wanted to say, “Hey, I gave you that!” you would use 「あげる」. However, you would use 「もらう」 if you wanted to say, “Hey, you got that from me!” その時計は私からもらったのよ。 (He) received that watch from me.',
      grammars: ['もらう'],
      keywords: ['morau', 'receive'],
    ),
    Section(
      heading: 'Asking favors with 「くれる」 or 「もらえる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('千円 【せん・えん】 – 1,000 yen'),
                  Text('貸す 【か・す】 (u-verb) – lend'),
                  Text('する (exception) – to do'),
                  Text('くれる (ru-verb) – to give'),
                  Text('もらう (u-verb) – to receive'),
                  Text('あなた – you'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('ちょっと – a little'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can make requests by using 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and the potential form of 「',
                  ),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 (can I receive the favor of…). We’ve already seen an example of this in example 4 of the 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 section. Because requests are favors done for the speaker, you cannot use 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(
                      text: 'あげる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 in this situation.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senen,
                              inlineSpan: const TextSpan(
                                text: '千円',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: const TextSpan(
                                text: '貸して',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれる',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Will you give me the favor of lending 1000 yen?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senen,
                              inlineSpan: const TextSpan(
                                text: '千円',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: const TextSpan(
                                text: '貸して',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらえる',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can I receive the favor of you lending 1000 yen?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Notice that the two sentences essentially mean the same thing. This is because the giver and receiver has been omitted because it is obvious from the context. If we were to write out the full sentence, it would look like this:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: anata,
                              inlineSpan: TextSpan(
                                text: 'あなた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: TextSpan(
                                text: '私',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: senen,
                              inlineSpan: const TextSpan(
                                text: '千円',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: const TextSpan(
                                text: '貸して',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれる',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Will you give me the favor of lending 1000 yen?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: TextSpan(
                                text: '私',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: anata,
                              inlineSpan: TextSpan(
                                text: 'あなた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: senen,
                              inlineSpan: const TextSpan(
                                text: '千円',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: const TextSpan(
                                text: '貸して',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらえる',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can I receive the favor of you lending 1000 yen?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'It is not normal to explicitly include the subject and target like this when directly addressing someone but is provided here to illustrate the change of subject and target depending on the verb 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらえる',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can use the negative to make the request a little softer. You’ll see that this is true in many other types of grammar.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Won’t you be a little quieter?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: const TextSpan(
                                text: '書いて',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: TextSpan(
                                text: 'もらえません',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can you write this in kanji for me?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 千円 【せん・えん】 – 1,000 yen 貸す 【か・す】 (u-verb) – lend する (exception) – to do くれる (ru-verb) – to give もらう (u-verb) – to receive あなた – you 私 【わたし】 – me; myself; I ちょっと – a little 静か 【しず・か】 (na-adj) – quiet 漢字 【かん・じ】 – Kanji 書く 【か・く】 (u-verb) – to write You can make requests by using 「くれる」 and the potential form of 「もらう」 (can I receive the favor of…). We’ve already seen an example of this in example 4 of the 「くれる」 section. Because requests are favors done for the speaker, you cannot use 「あげる」 in this situation. Examples 千円を貸してくれる？ Will you give me the favor of lending 1000 yen? 千円を貸してもらえる？ Can I receive the favor of you lending 1000 yen? Notice that the two sentences essentially mean the same thing. This is because the giver and receiver has been omitted because it is obvious from the context. If we were to write out the full sentence, it would look like this: あなたが、私に千円を貸してくれる？ Will you give me the favor of lending 1000 yen? 私が、あなたに千円を貸してもらえる？ Can I receive the favor of you lending 1000 yen? It is not normal to explicitly include the subject and target like this when directly addressing someone but is provided here to illustrate the change of subject and target depending on the verb 「くれる」 and 「もらえる」. You can use the negative to make the request a little softer. You’ll see that this is true in many other types of grammar. ちょっと静かにしてくれない？ Won’t you be a little quieter? 漢字で書いてもらえませんか。 Can you write this in kanji for me?',
      grammars: [],
      keywords: ['kureru','moraeru','potential','favor'],
    ),
    Section(
      heading: 'Asking someone to not do something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('くれる (ru-verb) – to give'),
                  Text('高い 【たか・い】 (i-adj) – high; tall; expensive'),
                  Text('物 【もの】 – object'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In order to request that someone not do something, you simply attach 「で」 to the negative form of the verb and proceed as before.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれます',
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can you not eat it all?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: '高い',
                              ),
                            ),
                            const TextSpan(
                              text: '物',
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買わない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれる',
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can you not buy expensive thing(s)?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat くれる (ru-verb) – to give 高い 【たか・い】 (i-adj) – high; tall; expensive 物 【もの】 – object 買う 【か・う】 (u-verb) – to buy In order to request that someone not do something, you simply attach 「で」 to the negative form of the verb and proceed as before. 全部食べないでくれますか。 Can you not eat it all? 高い物を買わないでくれる？ Can you not buy expensive thing(s)?',
      grammars: ['～ないで'],
      keywords: ['negative','request'],
    ),
  ],
);
