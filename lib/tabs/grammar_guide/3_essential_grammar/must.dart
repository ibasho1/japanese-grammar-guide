import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson must = Lesson(
  title: 'Expressing “Must” or “Have To”',
  sections: [
    Section(
      heading: 'When there’s something that must or must not be done',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In life, there are things that we must or must not do whether it’s taking out the trash or doing our homework. We will cover how to say this in Japanese because it is a useful expression and it also ties in well with the previous section. We will also learn how to the say the expression, “You don’t have to…” to finish off this section.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In life, there are things that we must or must not do whether it’s taking out the trash or doing our homework. We will cover how to say this in Japanese because it is a useful expression and it also ties in well with the previous section. We will also learn how to the say the expression, “You don’t have to…” to finish off this section.',
      grammars: [],
      keywords: ['must','have to','expressing'],
    ),
    Section(
      heading:
          'Using 「だめ」, 「いけない」, and 「ならない」 for things that must not be done',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('駄目 【だめ】 – no good'),
                  Text('ここ – here'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('それ – that'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('夜 【よる】 – evening'),
                  Text('遅い 【おそ・い】 (i-adj) – late'),
                  Text('～まで (particle) – until ～'),
                  Text('電話 【でん・わ】 – phone'),
                  Text('する (exception) – to do'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'If you’re not familiar with the word 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '」（'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: '駄目',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '）, though it can be used in many different ways it essentially means “no good”. The other two key words in this section are 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 and 「ならない」 and they have essentially the same basic meaning as 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '」. However, while 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 can be used by itself, 「ならない」 must only be used in the grammar presented here. In addition, while 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 and 「ならない」 conjugate like i-adjectives they are not actual adjectives. Let’s learn how to use these words to express things that must not be done.'),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Take the te-form of the verb, add the 「は」 (wa) particle and finally attach either 「',
                              ),
                              VocabTooltip(
                                message: dame,
                                inlineSpan: const TextSpan(
                                  text: 'だめ',
                                ),
                              ),
                              const TextSpan(
                                text: '」、「',
                              ),
                              VocabTooltip(
                                message: dame,
                                inlineSpan: const TextSpan(
                                  text: 'いけない',
                                ),
                              ),
                              const TextSpan(
                                text: '」、or 「ならない」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + は + ',
                                  ),
                                  VocabTooltip(
                                    message: dame,
                                    inlineSpan: const TextSpan(
                                      text: 'だめ = ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'は',
                                  ),
                                  VocabTooltip(
                                    message: dame,
                                    inlineSpan: const TextSpan(
                                      text: 'だめ',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + は + ',
                                  ),
                                  VocabTooltip(
                                    message: ikenai,
                                    inlineSpan: const TextSpan(
                                      text: 'いけない = ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'は',
                                  ),
                                  VocabTooltip(
                                    message: ikenai,
                                    inlineSpan: const TextSpan(
                                      text: 'いけない',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' + は + ',
                                  ),
                                  const TextSpan(
                                    text: 'ならない = ',
                                  ),
                                  VocabTooltip(
                                    message: hairu,
                                    inlineSpan: const TextSpan(
                                      text: '入って',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'はならない',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'How to say: Must not [verb]  ',
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ikenai,
                              inlineSpan: const TextSpan(
                                text: 'いけません',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You must not enter here.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You can’t (must not) eat that!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yoru,
                              inlineSpan: const TextSpan(
                                text: '夜',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: osoi,
                              inlineSpan: const TextSpan(
                                text: '遅く',
                              ),
                            ),
                            VocabTooltip(
                              message: made,
                              inlineSpan: const TextSpan(
                                text: 'まで',
                              ),
                            ),
                            VocabTooltip(
                              message: denwa,
                              inlineSpan: const TextSpan(
                                text: '電話',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'ならない。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You must not use the phone until late at night.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: neru,
                              inlineSpan: TextSpan(
                                text: '寝て',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'なりませんでした。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Wasn’t allowed to sleep early.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'The difference between 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '」、「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(text: '」、and 「ならない」 is that, first of all, 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '」 is casual. While 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 and 「ならない」 are basically identical, 「ならない」 is generally more for things that apply to more than one person like rules and policies.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 駄目 【だめ】 – no good ここ – here 入る 【はい・る】 (u-verb) – to enter それ – that 食べる 【たべ・る】 (ru-verb) – to eat 夜 【よる】 – evening 遅い 【おそ・い】 (i-adj) – late～まで (particle) – until ～電話 【でん・わ】 – phone する (exception) – to do 早い 【はや・い】 (i-adj) – fast; early 寝る 【ね・る】 (ru-verb) – to sleep If you’re not familiar with the word 「だめ」（駄目）, though it can be used in many different ways it essentially means “no good”. The other two key words in this section are 「いけない」 and 「ならない」 and they have essentially the same basic meaning as 「だめ」. However, while 「いけない」 can be used by itself, 「ならない」 must only be used in the grammar presented here. In addition, while 「いけない」 and 「ならない」 conjugate like i-adjectives they are not actual adjectives. Let’s learn how to use these words to express things that must not be done. How to say: Must not [verb] Take the te-form of the verb, add the 「は」 (wa) particle and finally attach either 「だめ」、「いけない」、or 「ならない」. Examples: 入る → 入って + は + だめ = 入ってはだめ入る → 入って + は + いけない = 入ってはいけない入る → 入って + は + ならない = 入ってはならない ここに入ってはいけません。 You must not enter here. それを食べてはだめ！ You can’t (must not) eat that! 夜、遅くまで電話してはならない。 You must not use the phone until late at night. 早く寝てはなりませんでした。 Wasn’t allowed to sleep early. The difference between 「だめ」、「いけない」、and 「ならない」 is that, first of all, 「だめ」 is casual. While 「いけない」 and 「ならない」 are basically identical, 「ならない」 is generally more for things that apply to more than one person like rules and policies.',
      grammars: ['だめ','いけない','ならない'],
      keywords: ['dame','ikenai','naranai','forbidden','not allowed','must not'],
    ),
    Section(
      heading: 'Expressing things that must be done',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('する (exception) – to do'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You may have predicted that the opposite of “You must not do” would use 「'),
                  VocabTooltip(
                    message: ikeru,
                    inlineSpan: const TextSpan(
                      text: 'いける',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 because they look like the positive version of 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(text: '」 and 「ならない」. However, 「'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 and 「ならない」 must always be negative, so this is not correct. In actuality, we still use the same 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '／ならない」 and use the opposite of the verb that goes in front of it instead. This double negative can be kind of confusing at first but you will get used to it with practice. There are three ways to conjugate the verb before adding 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '／ならない」 and two of them involve conditionals so aren’t you glad that you just learned conditionals in the previous section?'),
                ],
              ),
              NotesSection(
                content: NumberedList(
                  items: [
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                              text: 'Negative te-form + 「は」 (wa) particle + '),
                          VocabTooltip(
                            message: dame,
                            inlineSpan: const TextSpan(
                              text: 'だめ',
                            ),
                          ),
                          const TextSpan(text: '／'),
                          VocabTooltip(
                            message: ikenai,
                            inlineSpan: const TextSpan(
                              text: 'いけない',
                            ),
                          ),
                          const TextSpan(text: '／ならない.'),
                        ],
                      ),
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                              text: 'Negative verb + 「と」 conditional + '),
                          VocabTooltip(
                            message: dame,
                            inlineSpan: const TextSpan(
                              text: 'だめ',
                            ),
                          ),
                          const TextSpan(text: '／'),
                          VocabTooltip(
                            message: ikenai,
                            inlineSpan: const TextSpan(
                              text: 'いけない',
                            ),
                          ),
                          const TextSpan(text: '／ならない.'),
                        ],
                      ),
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                              text: 'Negative verb + 「ば」 conditional + '),
                          VocabTooltip(
                            message: dame,
                            inlineSpan: const TextSpan(
                              text: 'だめ',
                            ),
                          ),
                          const TextSpan(text: '／'),
                          VocabTooltip(
                            message: ikenai,
                            inlineSpan: const TextSpan(
                              text: 'いけない',
                            ),
                          ),
                          const TextSpan(text: '／ならない.'),
                        ],
                      ),
                    ),
                  ],
                ),
                heading: 'How to say: Must [verb]',
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The first method is the same as the “must not do” grammar form except that we simply negated the verb.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かなくて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'なりません。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Must go to school everyday.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しなくて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ikenai,
                              inlineSpan: const TextSpan(
                                text: 'いけなかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Had to do homework.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The second method uses the natural conditional that we learned in the last lesson. Literally, it means if you don’t do something, then it automatically leads to the fact that it is no good. (In other words, you must do it.) However, people tend to use it for situations beyond the natural consequence characterization that we learned from the last section because it’s shorter and easier to use than the other two types of grammar.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Must go to school everyday.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ikenai,
                              inlineSpan: const TextSpan(
                                text: 'いけない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Have to do homework.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The third method is similar to the second except that it uses a different type of conditional as explained in the last lesson. With the 「ば」 conditional, it can be used for a wider range of situations. Note that since the verb is always negative, for the 「ば」 conditional, we will always be removing the last 「い」 and adding 「ければ」.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かなければ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: ikenai,
                              inlineSpan: const TextSpan(
                                text: 'いけません',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Must go to school everyday.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しなければ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'だった。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Had to do homework.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'It may seem like I just breezed through a whole lot of material because there are three grammar forms and 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '／ならない」 adding up to nine possible combinations (3×3). However, some combinations are more common than others but I did not explicitly point out which were more common because any combination is technically correct and going over style would merely confuse at this point. Also, keep in mind that there is nothing essentially new in terms of conjugation rules. We already covered conditionals in the last lesson and adding the wa particle to the te-form in the beginning of this section.'),
                ],
              ),
              const Heading(
                text: '※ Reality Check',
                level: 2,
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Although we spent the last section explaining 「～なければ」 and 「～なくては」, the reality is that because they are so long, they are practically never used in real conversations. While they are often used in a written context, in actual speech, people usually use the 「と」 conditional or the various shortcuts described below. In casual speech, the 「と」 conditional is the most prevalent type of conditional. Though I explained in depth the meaning associated with the 「と」 conditional, you have to take it with a grain of salt here because people are inherently lazy.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 毎日 【まい・にち】 – everyday 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 宿題 【しゅく・だい】 – homework する (exception) – to do You may have predicted that the opposite of “You must not do” would use 「いける」 or 「なる」 because they look like the positive version of 「いけない」 and 「ならない」. However, 「いけない」 and 「ならない」 must always be negative, so this is not correct. In actuality, we still use the same 「だめ／いけない／ならない」 and use the opposite of the verb that goes in front of it instead. This double negative can be kind of confusing at first but you will get used to it with practice. There are three ways to conjugate the verb before adding 「だめ／いけない／ならない」 and two of them involve conditionals so aren’t you glad that you just learned conditionals in the previous section? How to say: Must [verb] Negative te-form + 「は」 (wa) particle + だめ／いけない／ならない. Negative verb + 「と」 conditional + だめ／いけない／ならない. Negative verb + 「ば」 conditional + だめ／いけない／ならない. The first method is the same as the “must not do” grammar form except that we simply negated the verb. 毎日学校に行かなくてはなりません。 Must go to school everyday. 宿題をしなくてはいけなかった。 Had to do homework. The second method uses the natural conditional that we learned in the last lesson. Literally, it means if you don’t do something, then it automatically leads to the fact that it is no good. (In other words, you must do it.) However, people tend to use it for situations beyond the natural consequence characterization that we learned from the last section because it’s shorter and easier to use than the other two types of grammar. 毎日学校に行かないとだめです。 Must go to school everyday. 宿題をしないといけない。 Have to do homework. The third method is similar to the second except that it uses a different type of conditional as explained in the last lesson. With the 「ば」 conditional, it can be used for a wider range of situations. Note that since the verb is always negative, for the 「ば」 conditional, we will always be removing the last 「い」 and adding 「ければ」. 毎日学校に行かなければいけません。 Must go to school everyday. 宿題をしなければだめだった。 Had to do homework. It may seem like I just breezed through a whole lot of material because there are three grammar forms and 「だめ／いけない／ならない」 adding up to nine possible combinations (3×3). However, some combinations are more common than others but I did not explicitly point out which were more common because any combination is technically correct and going over style would merely confuse at this point. Also, keep in mind that there is nothing essentially new in terms of conjugation rules. We already covered conditionals in the last lesson and adding the wa particle to the te-form in the beginning of this section. ※ Reality Check Although we spent the last section explaining 「～なければ」 and 「～なくては」, the reality is that because they are so long, they are practically never used in real conversations. While they are often used in a written context, in actual speech, people usually use the 「と」 conditional or the various shortcuts described below. In casual speech, the 「と」 conditional is the most prevalent type of conditional. Though I explained in depth the meaning associated with the 「と」 conditional, you have to take it with a grain of salt here because people are inherently lazy.',
      grammars: ['は／と／ば だめ／いけない／ならない'],
      keywords: ['dame','ikenai','naranai','must','have to'],
    ),
    Section(
      heading: 'Various short-cuts for the lazy',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('ご飯 【ご・はん】 – rice; meal'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('ここ – here'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('駄目 【だめ】 – no good'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You may have been grumbling and complaining about how long most of the expressions are just to say you must do something. You can end up with up to eight additional syllables just to say “I have to…”!',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Well, others have thought the same before and people usually use short abbreviated versions of 「なくては」 and 「なければ」 in casual speech. Teachers are often reluctant to teach these overly familiar expressions because they are so much easier to use which is bad for times when they might not be appropriate. But, on the other hand, if you don’t learn casual expressions, it makes it difficult to understand your friends (or would-be friends if you only knew how to speak less stiffly!). So here they are but take care to properly practice the longer forms so that you will be able to use them for the appropriate occasions.',
                  ),
                ],
              ),
              const NotesSection(
                content: NumberedList(
                  items: [
                    Text('Simply replace 「なくて」 with 「なくちゃ」.'),
                    Text('Simply replace 「なければ」 with 「なきゃ」.'),
                  ],
                ),
                heading: 'Casual abbreviations for things that must be done',
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Right now, you may be saying, “What the?” because the “abbreviations” are about the same length as what we’ve already covered. The secret here is that, unlike the expressions we learned so far, you can just leave the 「',
                  ),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(text: '／ならない」 part out altogether!'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'し',
                                  ),
                                  TextSpan(
                                    text: 'なくちゃ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Gotta study.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gohan,
                              inlineSpan: const TextSpan(
                                text: 'ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '食べ',
                                  ),
                                  TextSpan(
                                    text: 'なきゃ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Gotta eat.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The 「と」 conditional is also used by itself to imply 「',
                  ),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(text: '／ならない」.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行かない',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Gotta go to school.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'There is another 「ちゃ」 abbreviation for things that you must ',
                  ),
                  const TextSpan(
                    text: 'not do',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text: '. However, in this case, you cannot leave out 「',
                  ),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '／'),
                  VocabTooltip(
                    message: ikenai,
                    inlineSpan: const TextSpan(
                      text: 'いけない',
                    ),
                  ),
                  const TextSpan(
                      text: '／ならない」. Since this is a casual abbreviation, 「'),
                  VocabTooltip(
                    message: dame,
                    inlineSpan: const TextSpan(
                      text: 'だめ',
                    ),
                  ),
                  const TextSpan(text: '」 is used in most cases.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'One very important difference for this casual form is that verbs that end in 「む」、「ぶ」、「ぬ」 use 「じゃ」 instead of 「ちゃ」. Essentially, all the verbs that end in 「んだ」 for past tense fall in this category.'),
                ],
              ),
              const NotesSection(
                content: NumberedList(
                  items: [
                    Text('Replace 「ては」 with 「ちゃ」.'),
                    Text('Replace 「では」 with 「じゃ」.'),
                  ],
                ),
                heading: 'Casual abbreviations for things that must be done',
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入っちゃ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'だよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Gotta study.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shinu,
                              inlineSpan: TextSpan(
                                text: '死んじゃ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            const TextSpan(
                              text: 'だよ！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You can’t die!',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'On a final note, in general, 「ちゃ」 sounds a bit cutesy or girly. You’ve already seen an example of this with the 「ちゃん」 suffix. Similarly, 「なくちゃ」 also sounds a bit cutesy or childish.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 勉強 【べん・きょう】 – study する (exception) – to do ご飯 【ご・はん】 – rice; meal 食べる 【た・べる】 (ru-verb) – to eat 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go ここ – here 入る 【はい・る】 (u-verb) – to enter 駄目 【だめ】 – no good 死ぬ 【し・ぬ】 (u-verb) – to die You may have been grumbling and complaining about how long most of the expressions are just to say you must do something. You can end up with up to eight additional syllables just to say “I have to…”! Well, others have thought the same before and people usually use short abbreviated versions of 「なくては」 and 「なければ」 in casual speech. Teachers are often reluctant to teach these overly familiar expressions because they are so much easier to use which is bad for times when they might not be appropriate. But, on the other hand, if you don’t learn casual expressions, it makes it difficult to understand your friends (or would-be friends if you only knew how to speak less stiffly!). So here they are but take care to properly practice the longer forms so that you will be able to use them for the appropriate occasions. Casual abbreviations for things that must be done Simply replace 「なくて」 with 「なくちゃ」. Simply replace 「なければ」 with 「なきゃ」. Right now, you may be saying, “What the?” because the “abbreviations” are about the same length as what we’ve already covered. The secret here is that, unlike the expressions we learned so far, you can just leave the 「だめ／いけない／ならない」 part out altogether! 勉強しなくちゃ。 Gotta study. ご飯を食べなきゃ。 Gotta eat. The 「と」 conditional is also used by itself to imply 「だめ／いけない／ならない」. 学校に行かないと。 Gotta go to school. There is another 「ちゃ」 abbreviation for things that you must not do. However, in this case, you cannot leave out 「だめ／いけない／ならない」. Since this is a casual abbreviation, 「だめ」 is used in most cases. One very important difference for this casual form is that verbs that end in 「む」、「ぶ」、「ぬ」 use 「じゃ」 instead of 「ちゃ」. Essentially, all the verbs that end in 「んだ」 for past tense fall in this category. Casual abbreviations for things that must be done Replace 「ては」 with 「ちゃ」. Replace 「では」 with 「じゃ」. ここに入っちゃだめだよ。 Gotta study. 死んじゃだめだよ！ You can’t die! On a final note, in general, 「ちゃ」 sounds a bit cutesy or girly. You’ve already seen an example of this with the 「ちゃん」 suffix. Similarly, 「なくちゃ」 also sounds a bit cutesy or childish.',
      grammars: ['なくちゃ','なきゃ','ちゃ','じゃ'],
      keywords: ['nakucha','nakya','cha','ja','abbreviation'],
    ),
    Section(
      heading: 'Saying something is ok to do or not do',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('いい (i-adj) – good'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('大丈夫 【だい・じょう・ぶ】 (na-adj) – ok'),
                  Text('構う 【かま・う】 (u-verb) – to mind; to be concerned about'),
                  Text('もう – already'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('これ – this'),
                  Text('ちょっと – just a little'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Now let’s learn how to say either that it’s ok to do or not do something. I decided to shove this section in here because in Japanese, this is essential how to say that you don’t have to something (by saying it’s ok to not do it). The grammar itself is also relatively easy to pick up and makes for a short section.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'By simply using the te-form and the 「も」 particle, you are essentially saying, “even if you do X…” Common words that come after this include 「'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」, 「'),
                  VocabTooltip(
                    message: daijoubu,
                    inlineSpan: const TextSpan(
                      text: '大丈夫',
                    ),
                  ),
                  const TextSpan(text: '」, or 「'),
                  VocabTooltip(
                    message: kamau,
                    inlineSpan: const TextSpan(
                      text: '構わない',
                    ),
                  ),
                  const TextSpan(text: '」. Some examples will come in handy.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '食べ',
                                  ),
                                  TextSpan(
                                    text: 'ても',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You can go ahead and eat it all. (lit: Even if you eat it all, it’s good, you know.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '食べなく',
                                  ),
                                  TextSpan(
                                    text: 'ても',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You don’t have to eat it all. (lit: Even if you don’t eat it all, it’s good, you know.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '飲ん',
                                  ),
                                  TextSpan(
                                    text: 'でも',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: daijoubu,
                              inlineSpan: const TextSpan(
                                text: '大丈夫',
                              ),
                            ),
                            const TextSpan(
                              text: 'だよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s ok if you drink it all. (lit: Even if you drink it all, it’s OK, you know.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '飲ん',
                                  ),
                                  TextSpan(
                                    text: 'でも',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: kamau,
                              inlineSpan: const TextSpan(
                                text: '構わない',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I don’t mind if you drink it all. (lit: Even if you drink it all, I don’t mind, you know.)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'In casual speech, 「～ても'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」 sometimes get shortened to just 「～て'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」 （or 「～で'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」 instead of 「～でも'),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(text: '」 ）.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '帰っ',
                                  ),
                                  TextSpan(
                                    text: 'て',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'いい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can I go home already?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '見',
                                  ),
                                  TextSpan(
                                    text: 'て',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'いい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can I take a quick look at this?',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat いい (i-adj) – good 飲む 【の・む】 (u-verb) – to drink 大丈夫 【だい・じょう・ぶ】 (na-adj) – ok 構う 【かま・う】 (u-verb) – to mind; to be concerned about もう – already 帰る 【かえ・る】 (u-verb) – to go home これ – this ちょっと – just a little 見る 【み・る】 (ru-verb) – to see Now let’s learn how to say either that it’s ok to do or not do something. I decided to shove this section in here because in Japanese, this is essential how to say that you don’t have to something (by saying it’s ok to not do it). The grammar itself is also relatively easy to pick up and makes for a short section. By simply using the te-form and the 「も」 particle, you are essentially saying, “even if you do X…” Common words that come after this include 「いい」, 「大丈夫」, or 「構わない」. Some examples will come in handy. 全部食べてもいいよ。 You can go ahead and eat it all. (lit: Even if you eat it all, it’s good, you know.) 全部食べなくてもいいよ。 You don’t have to eat it all. (lit: Even if you don’t eat it all, it’s good, you know.) 全部飲んでも大丈夫だよ。 It’s ok if you drink it all. (lit: Even if you drink it all, it’s OK, you know.) 全部飲んでも構わないよ。 I don’t mind if you drink it all. (lit: Even if you drink it all, I don’t mind, you know.) In casual speech, 「～てもいい」 sometimes get shortened to just 「～ていい」 （or 「～でいい」 instead of 「～でもいい」 ）. もう帰っていい？ Can I go home already? これ、ちょっと見ていい？ Can I take a quick look at this?',
      grammars: ['～ても','～ていい'],
      keywords: ['temo','te ii'],
    ),
  ],
);
