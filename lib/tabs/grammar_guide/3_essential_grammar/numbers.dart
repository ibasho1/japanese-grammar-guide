import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson numbers = Lesson(
  title: 'Numbers and Counting',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Numbers and counting in Japanese are difficult enough to require its own section. First of all, the number system is in units of four instead of three, which can make converting into English quite difficult. Also, there are things called counters, which are required to count different types of objects, animals, or people. We will learn the most generic and widely used counters to get you started so that you can learn more on your own. To be honest, counters might be the only thing that’ll make you want to quit learning Japanese, it’s that bad. I recommend you digest only a little bit of this section at a time because it’s an awful lot of things to memorize.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Numbers and counting in Japanese are difficult enough to require its own section. First of all, the number system is in units of four instead of three, which can make converting into English quite difficult. Also, there are things called counters, which are required to count different types of objects, animals, or people. We will learn the most generic and widely used counters to get you started so that you can learn more on your own. To be honest, counters might be the only thing that’ll make you want to quit learning Japanese, it’s that bad. I recommend you digest only a little bit of this section at a time because it’s an awful lot of things to memorize.',
      grammars: ['数'],
      keywords: ['numbers','counting'],
    ),
    Section(
      heading: 'The number system',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The Japanese number system is spread into units of four. So a number such as 10,000,000 is actually split up as 1000,0000. However, thanks to the strong influence of the Western world and the standardization of numbers, when numbers are actually written, the split-off is three digits. Here are the first ten numbers.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(
                      caption: 'Kanji and readings for numbers 1 to 10',
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: '1',
                            ),
                            TableHeading(
                              text: '2',
                            ),
                            TableHeading(
                              text: '3',
                            ),
                            TableHeading(
                              text: '4',
                            ),
                            TableHeading(
                              text: '5',
                            ),
                            TableHeading(
                              text: '6',
                            ),
                            TableHeading(
                              text: '7',
                            ),
                            TableHeading(
                              text: '8',
                            ),
                            TableHeading(
                              text: '9',
                            ),
                            TableHeading(
                              text: '10',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ichi,
                                  inlineSpan: const TextSpan(
                                    text: '一',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ni,
                                  inlineSpan: const TextSpan(
                                    text: '	二',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: sanThree,
                                  inlineSpan: const TextSpan(
                                    text: '三',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiYon,
                                  inlineSpan: const TextSpan(
                                    text: '四',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: go,
                                  inlineSpan: const TextSpan(
                                    text: '五',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: roku,
                                  inlineSpan: const TextSpan(
                                    text: '六',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shichiNana,
                                  inlineSpan: const TextSpan(
                                    text: '七',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hachi,
                                  inlineSpan: const TextSpan(
                                    text: '八',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kyuu,
                                  inlineSpan: const TextSpan(
                                    text: '九',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: juu,
                                  inlineSpan: const TextSpan(
                                    text: '十',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ichi,
                                  inlineSpan: const TextSpan(
                                    text: 'いち',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ni,
                                  inlineSpan: const TextSpan(
                                    text: 'に',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: sanThree,
                                  inlineSpan: const TextSpan(
                                    text: 'さん',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Column(
                                children: [
                                  Text.rich(
                                    VocabTooltip(
                                      message: shi,
                                      inlineSpan: const TextSpan(
                                        text: 'し',
                                      ),
                                    ),
                                  ),
                                  Text.rich(
                                    VocabTooltip(
                                      message: yon,
                                      inlineSpan: const TextSpan(
                                        text: 'よん',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: go,
                                  inlineSpan: const TextSpan(
                                    text: 'ご',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: roku,
                                  inlineSpan: const TextSpan(
                                    text: 'ろく',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Column(
                                children: [
                                  Text.rich(
                                    VocabTooltip(
                                      message: shichi,
                                      inlineSpan: const TextSpan(
                                        text: 'しち',
                                      ),
                                    ),
                                  ),
                                  Text.rich(
                                    VocabTooltip(
                                      message: nana,
                                      inlineSpan: const TextSpan(
                                        text: 'なな',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hachi,
                                  inlineSpan: const TextSpan(
                                    text: 'はち',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kyuu,
                                  inlineSpan: const TextSpan(
                                    text: 'きゅう',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: juu,
                                  inlineSpan: const TextSpan(
                                    text: '	じゅう',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'As the chart indicates, 4 can either be 「',
                  ),
                  VocabTooltip(
                    message: shi,
                    inlineSpan: const TextSpan(
                      text: 'し',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: yon,
                    inlineSpan: const TextSpan(
                      text: 'よん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 7 can either be 「',
                  ),
                  VocabTooltip(
                    message: shichi,
                    inlineSpan: const TextSpan(
                      text: 'しち',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: nana,
                    inlineSpan: const TextSpan(
                      text: 'なな',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Basically, both are acceptable up to 10. However, past ten, the reading is almost always 「',
                  ),
                  VocabTooltip(
                    message: yon,
                    inlineSpan: const TextSpan(
                      text: 'よん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: nana,
                    inlineSpan: const TextSpan(
                      text: 'なな',
                    ),
                  ),
                  const TextSpan(
                    text: '」. In general, 「',
                  ),
                  VocabTooltip(
                    message: yon,
                    inlineSpan: const TextSpan(
                      text: 'よん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: nana,
                    inlineSpan: const TextSpan(
                      text: 'なな',
                    ),
                  ),
                  const TextSpan(
                    text: '」 are preferred over 「',
                  ),
                  VocabTooltip(
                    message: shi,
                    inlineSpan: const TextSpan(
                      text: 'し',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: shichi,
                    inlineSpan: const TextSpan(
                      text: 'しち',
                    ),
                  ),
                  const TextSpan(
                    text: '」 in most circumstances.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can simply count from 1 to 99 with just these ten numbers. Japanese is easier than English in this respect because you do not have to memorize separate words such as “twenty” or “fifty”. In Japanese, it’s simply just “two ten” and “five ten”.'),
                ],
              ),
              const NumberedList(
                items: [
                  Text('三十一 （さんじゅういち） = 31'),
                  Text('五十四 （ごじゅうよん）= 54'),
                  Text('七十七 （ななじゅうなな）= 77'),
                  Text('二十 （にじゅう） = 20'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Notice that numbers are either always written in kanji or numerals because hiragana can get rather long and hard to decipher.'),
                ],
              ),
              const Heading(
                text: 'Numbers past 99',
                level: 2,
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'Here are the higher numbers:'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'Numerals'),
                        TableData(
                          content: Text('100'),
                        ),
                        TableData(
                          content: Text('1,000'),
                        ),
                        TableData(
                          content: Text('10,000'),
                        ),
                        TableData(
                          content: Text('10^8'),
                        ),
                        TableData(
                          content: Text('	10^12'),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Kanji'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: hyaku,
                              inlineSpan: const TextSpan(
                                text: '百',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sen,
                              inlineSpan: const TextSpan(
                                text: '千',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: man,
                              inlineSpan: const TextSpan(
                                text: '万',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: okuHundredMillion,
                              inlineSpan: const TextSpan(
                                text: '億',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: chou,
                              inlineSpan: const TextSpan(
                                text: '兆',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Notice how the numbers jumped four digits from 10^4 to 10^8 between '),
                  VocabTooltip(
                    message: man,
                    inlineSpan: const TextSpan(
                      text: '万',
                    ),
                  ),
                  const TextSpan(text: ' and '),
                  VocabTooltip(
                    message: okuHundredMillion,
                    inlineSpan: const TextSpan(
                      text: '億',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '. That’s because Japanese is divided into units of four. Once you get past 1'),
                  VocabTooltip(
                    message: man,
                    inlineSpan: const TextSpan(
                      text: '万',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' (10,000), you start all over until you reach 9,999'),
                  VocabTooltip(
                    message: man,
                    inlineSpan: const TextSpan(
                      text: '万',
                    ),
                  ),
                  const TextSpan(text: ', then it rotates to 1'),
                  VocabTooltip(
                    message: okuHundredMillion,
                    inlineSpan: const TextSpan(
                      text: '億',
                    ),
                  ),
                  const TextSpan(text: ' (100,000,000). By the way, '),
                  VocabTooltip(
                    message: hyakuOneHundred,
                    inlineSpan: const TextSpan(
                      text: '百',
                    ),
                  ),
                  const TextSpan(text: ' is 100 and '),
                  VocabTooltip(
                    message: senOneThousand,
                    inlineSpan: const TextSpan(
                      text: '千',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' is 1,000, but anything past that, and you need to attach a 1 so the rest of the units become '),
                  VocabTooltip(
                    message: ichiman,
                    inlineSpan: const TextSpan(
                      text: '一万',
                    ),
                  ),
                  const TextSpan(text: ' (10^4)、'),
                  VocabTooltip(
                    message: ichioku,
                    inlineSpan: const TextSpan(
                      text: '一億',
                    ),
                  ),
                  const TextSpan(text: ' (10^8)、'),
                  VocabTooltip(
                    message: icchou,
                    inlineSpan: const TextSpan(
                      text: '一兆',
                    ),
                  ),
                  const TextSpan(text: ' (10^12).'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Now you can count up to 9,999,999,999,999,999 just by chaining the numbers same as before. This is where the problems start, however. Try saying 「いちちょう」 、「ろくひゃく」、or 「さんせん」 really quickly, you’ll notice it’s difficult because of the repetition of similar consonant sounds. Therefore, Japanese people have decided to make it easier on themselves by pronouncing them as 「'),
                  VocabTooltip(
                    message: icchou,
                    inlineSpan: const TextSpan(
                      text: 'いっちょう',
                    ),
                  ),
                  const TextSpan(text: '」、 「'),
                  VocabTooltip(
                    message: roppyaku,
                    inlineSpan: const TextSpan(
                      text: 'ろっぴゃく',
                    ),
                  ),
                  const TextSpan(text: '」、and 「'),
                  VocabTooltip(
                    message: sanzen,
                    inlineSpan: const TextSpan(
                      text: 'さんぜん',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. Unfortunately, it makes it all the harder for you to remember how to pronounce everything. Here are all the slight sound changes.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'Numerals'),
                        TableHeading(text: 'Kanji'),
                        TableHeading(text: 'Reading'),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('300'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanbyaku,
                              inlineSpan: const TextSpan(
                                text: '三百',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanbyaku,
                              inlineSpan: const TextSpan(
                                text: 'さんびゃく',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('600'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: roppyaku,
                              inlineSpan: const TextSpan(
                                text: '六百',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: roppyaku,
                              inlineSpan: const TextSpan(
                                text: 'ろっぴゃく',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('800'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: happyaku,
                              inlineSpan: const TextSpan(
                                text: '八百',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: happyaku,
                              inlineSpan: const TextSpan(
                                text: 'はっぴゃく',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('3000'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanzen,
                              inlineSpan: const TextSpan(
                                text: '三千',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanzen,
                              inlineSpan: const TextSpan(
                                text: 'さんぜん',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('8000'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: hassen,
                              inlineSpan: const TextSpan(
                                text: '八千',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: hassen,
                              inlineSpan: const TextSpan(
                                text: 'はっせん',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableData(
                          content: Text('10^12'),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: icchou,
                              inlineSpan: const TextSpan(
                                text: '一兆',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: icchou,
                              inlineSpan: const TextSpan(
                                text: 'いっちょう',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('四万三千七十六'),
                      Text('（よんまんさんぜんななじゅうろく）'),
                      Text('43,076'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('七億六百二十四万九千二百二十二'),
                      Text('（ななおくろっぴゃくにじゅうよんまんきゅうせんにひゃくにじゅうに）'),
                      Text('706,249,222'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('五百兆二万一'),
                      Text('（ごひゃくちょうにまんいち）'),
                      Text('500,000,000,020,001'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Notice that it is customary to write large numbers only in numerals as even kanji can become difficult to decipher.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'The Japanese number system is spread into units of four. So a number such as 10,000,000 is actually split up as 1000,0000. However, thanks to the strong influence of the Western world and the standardization of numbers, when numbers are actually written, the split-off is three digits. Here are the first ten numbers. As the chart indicates, 4 can either be 「し」 or 「よん」 and 7 can either be 「しち」 or 「なな」. Basically, both are acceptable up to 10. However, past ten, the reading is almost always 「よん」 and 「なな」. In general, 「よん」 and 「なな」 are preferred over 「し」 and 「しち」 in most circumstances. You can simply count from 1 to 99 with just these ten numbers. Japanese is easier than English in this respect because you do not have to memorize separate words such as “twenty” or “fifty”. In Japanese, it’s simply just “two ten” and “five ten”. 三十一 （さんじゅういち） = 31 五十四 （ごじゅうよん）= 54 七十七 （ななじゅうなな）= 77 二十 （にじゅう） = 20 Notice that numbers are either always written in kanji or numerals because hiragana can get rather long and hard to decipher. Numbers past 99 Here are the higher numbers: Notice how the numbers jumped four digits from 10^4 to 10^8 between 万 and 億. That’s because Japanese is divided into units of four. Once you get past 1万 (10,000), you start all over until you reach 9,999万, then it rotates to 1億 (100,000,000). By the way, 百 is 100 and 千 is 1,000, but anything past that, and you need to attach a 1 so the rest of the units become 一万 (10^4)、一億 (10^8)、一兆 (10^12). Now you can count up to 9,999,999,999,999,999 just by chaining the numbers same as before. This is where the problems start, however. Try saying 「いちちょう」 、「ろくひゃく」、or 「さんせん」 really quickly, you’ll notice it’s difficult because of the repetition of similar consonant sounds. Therefore, Japanese people have decided to make it easier on themselves by pronouncing them as 「いっちょう」、 「ろっぴゃく」、and 「さんぜん」. Unfortunately, it makes it all the harder for you to remember how to pronounce everything. Here are all the slight sound changes. 四万三千七十六（よんまんさんぜんななじゅうろく） 43,076 七億六百二十四万九千二百二十二 （ななおくろっぴゃくにじゅうよんまんきゅうせんにひゃくにじゅうに） 706,249,222五百兆二万一 （ごひゃくちょうにまんいち） 500,000,000,020,001 Notice that it is customary to write large numbers only in numerals as even kanji can become difficult to decipher.',
      grammars: [],
      keywords: ['1','2','3','4','5','6','7','8','9','10','100','1000','10000','100000000','numbers'],
    ),
    // TODO: Addendum fractions?
    Section(
      heading: 'Numbers smaller or less than 1',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('零 【れい】 – zero'),
                  Text('ゼロ – zero'),
                  Text('マル – circle; zero'),
                  Text('点 【てん】 – period; point'),
                  Text('マイナス – minus'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'Zero in Japanese is 「'),
                  VocabTooltip(
                    message: rei,
                    inlineSpan: const TextSpan(
                      text: '零',
                    ),
                  ),
                  const TextSpan(text: '」 but 「'),
                  VocabTooltip(
                    message: zero,
                    inlineSpan: const TextSpan(
                      text: 'ゼロ',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: zero,
                    inlineSpan: const TextSpan(
                      text: 'マル',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is more common in modern Japanese. There is no special method for reading decimals, you simply say 「'),
                  VocabTooltip(
                    message: ten,
                    inlineSpan: const TextSpan(
                      text: '点',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 for the dot and read each individual number after the decimal point. Here’s an example:'),
                ],
              ),
              BulletedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(text: '0.0021 = '),
                        VocabTooltip(
                          message: zero,
                          inlineSpan: const TextSpan(
                            text: 'ゼロ',
                          ),
                        ),
                        const TextSpan(text: '、'),
                        VocabTooltip(
                          message: ten,
                          inlineSpan: const TextSpan(
                            text: '点',
                          ),
                        ),
                        const TextSpan(text: '、'),
                        VocabTooltip(
                          message: zero,
                          inlineSpan: const TextSpan(
                            text: 'ゼロ',
                          ),
                        ),
                        const TextSpan(text: '、'),
                        VocabTooltip(
                          message: zero,
                          inlineSpan: const TextSpan(
                            text: 'ゼロ',
                          ),
                        ),
                        const TextSpan(text: '、'),
                        VocabTooltip(
                          message: ni,
                          inlineSpan: const TextSpan(
                            text: '二',
                          ),
                        ),
                        const TextSpan(text: '、'),
                        VocabTooltip(
                          message: ichi,
                          inlineSpan: const TextSpan(
                            text: '一',
                          ),
                        ),
                        const TextSpan(text: '。'),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'For negative numbers, everything is the same as positive numbers except that you say 「'),
                  VocabTooltip(
                    message: mainasu,
                    inlineSpan: const TextSpan(
                      text: 'マイナス',
                    ),
                  ),
                  const TextSpan(text: '」 first.'),
                ],
              ),
              BulletedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: mainasu,
                          inlineSpan: const TextSpan(
                            text: 'マイナス',
                          ),
                        ),
                        const TextSpan(text: '二十九 = -29'),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 零 【れい】 – zero ゼロ – zero マル – circle; zero 点 【てん】 – period; point マイナス – minus Zero in Japanese is 「零」 but 「ゼロ」 or 「マル」 is more common in modern Japanese. There is no special method for reading decimals, you simply say 「点」 for the dot and read each individual number after the decimal point. Here’s an example: 0.0021 = ゼロ、点、ゼロ、ゼロ、二、一。 For negative numbers, everything is the same as positive numbers except that you say 「マイナス」 first. マイナス二十九 = -29',
      grammars: [],
      keywords: ['decimal','point','zero','negative numbers','maths'],
    ),
    Section(
      heading: 'Counting and counters',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Ah, and now we come to the fun part. In Japanese, when you are simply counting numbers, everything is just as you would expect, '),
                  VocabTooltip(
                    message: ichi,
                    inlineSpan: const TextSpan(
                      text: '一',
                    ),
                  ),
                  const TextSpan(text: '、'),
                  VocabTooltip(
                    message: ni,
                    inlineSpan: const TextSpan(
                      text: '二',
                    ),
                  ),
                  const TextSpan(text: '、'),
                  VocabTooltip(
                    message: san,
                    inlineSpan: const TextSpan(
                      text: '三',
                    ),
                  ),
                  const TextSpan(text: '、'),
                  const TextSpan(
                      text:
                          ' and so on. However, if you want to count any type of object, you have to use something called a counter which depends on what type of object you are counting and on top of this, there are various sound changes similar to the ones we saw with '),
                  VocabTooltip(
                    message: roppyaku,
                    inlineSpan: const TextSpan(
                      text: '六百',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ', etc. The counters themselves are usually single kanji characters that often have a special reading just for the counter. First, let’s learn the counters for dates.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Ah, and now we come to the fun part. In Japanese, when you are simply counting numbers, everything is just as you would expect, 一、二、三、 and so on. However, if you want to count any type of object, you have to use something called a counter which depends on what type of object you are counting and on top of this, there are various sound changes similar to the ones we saw with 六百, etc. The counters themselves are usually single kanji characters that often have a special reading just for the counter. First, let’s learn the counters for dates.',
      grammars: ['助数詞'],
      keywords: ['counters','counting'],
    ),
    Section(
      heading: 'Dates',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('平成 【へい・せい】 – Heisei era'),
                  Text('昭和 【しょう・わ】 – Showa era'),
                  Text('和暦 【わ・れき】 – Japanese calendar'),
                  Text('一日 【いち・にち】 – one day'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'The year is very easy. All you have to do is say the number and add 「'),
                  VocabTooltip(
                    message: nen,
                    inlineSpan: const TextSpan(
                      text: '年',
                    ),
                  ),
                  const TextSpan(text: '」 which is pronounced here as 「'),
                  VocabTooltip(
                    message: nen,
                    inlineSpan: const TextSpan(
                      text: 'ねん',
                    ),
                  ),
                  const TextSpan(
                      text: '」. For example, Year 2003 becomes 2003'),
                  VocabTooltip(
                    message: nen,
                    inlineSpan: const TextSpan(
                      text: '年',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' (にせんさんねん）. The catch is that there is another calendar which starts over every time a new emperor ascends the throne. The year is preceded by the era, for example the year 2000 is: '),
                  VocabTooltip(
                    message: heisei,
                    inlineSpan: const TextSpan(
                      text: '平成',
                    ),
                  ),
                  VocabTooltip(
                    message: juuninen,
                    inlineSpan: const TextSpan(
                      text: '12年',
                    ),
                  ),
                  const TextSpan(text: '. My birthday, 1981 is '),
                  VocabTooltip(
                    message: shouwa,
                    inlineSpan: const TextSpan(
                      text: '昭和',
                    ),
                  ),
                  VocabTooltip(
                    message: gojuurokunen,
                    inlineSpan: const TextSpan(
                      text: '56年',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' (The Showa era lasted from 1926 to 1989). You may think that you don’t need to know this but if you’re going to be filling out forms in Japan, they often ask you for your birthday or the current date in the Japanese calendar （'),
                  VocabTooltip(
                    message: wareki,
                    inlineSpan: const TextSpan(
                      text: '和暦',
                    ),
                  ),
                  const TextSpan(text: '）. So here’s a '),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'neat converter',
                    ),
                    uri: 'https://www.japan-guide.com/e/e2272.html',
                  ),
                  const TextSpan(
                      text:
                          ' you can use to convert to the Japanese calendar.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Saying the months is actually easier than English because all you have to do is write the number (either in numerals or kanji) of the month and add 「'),
                  VocabTooltip(
                    message: gatsu,
                    inlineSpan: const TextSpan(
                      text: '月',
                    ),
                  ),
                  const TextSpan(text: '」 which is read as 「'),
                  VocabTooltip(
                    message: gatsu,
                    inlineSpan: const TextSpan(
                      text: 'がつ',
                    ),
                  ),
                  const TextSpan(
                      text: '」. However, you need to pay attention to April （'),
                  VocabTooltip(
                    message: shigatsu,
                    inlineSpan: const TextSpan(
                      text: '４月',
                    ),
                  ),
                  const TextSpan(text: '）, July （'),
                  VocabTooltip(
                    message: shichigatsu,
                    inlineSpan: const TextSpan(
                      text: '７月',
                    ),
                  ),
                  const TextSpan(text: '）, and September （'),
                  VocabTooltip(
                    message: kugatsu,
                    inlineSpan: const TextSpan(
                      text: '９月',
                    ),
                  ),
                  const TextSpan(text: '） which are pronounced 「'),
                  VocabTooltip(
                    message: shigatsu,
                    inlineSpan: const TextSpan(
                      text: 'しがつ',
                    ),
                  ),
                  const TextSpan(text: '」、 「'),
                  VocabTooltip(
                    message: shichigatsu,
                    inlineSpan: const TextSpan(
                      text: 'しちがつ',
                    ),
                  ),
                  const TextSpan(text: '」、and 「'),
                  VocabTooltip(
                    message: kugatsu,
                    inlineSpan: const TextSpan(
                      text: 'くがつ',
                    ),
                  ),
                  const TextSpan(text: '」 respectively.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Finally, we get to the days of the month, which is where the headache starts. The first day of the month is 「'),
                  VocabTooltip(
                    message: tsuitachi,
                    inlineSpan: const TextSpan(
                      text: 'ついたち',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: tsuitachi,
                    inlineSpan: const TextSpan(
                      text: '一日',
                    ),
                  ),
                  const TextSpan(text: '）; '),
                  const TextSpan(
                    text: 'different',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(text: ' from 「'),
                  VocabTooltip(
                    message: ichinichi,
                    inlineSpan: const TextSpan(
                      text: 'いちにち',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: ichinichi,
                    inlineSpan: const TextSpan(
                      text: '一日',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '）, which means “one day”. Besides this and some other exceptions we’ll soon cover, you can simply say the number and add 「'),
                  VocabTooltip(
                    message: nichi,
                    inlineSpan: const TextSpan(
                      text: '日',
                    ),
                  ),
                  const TextSpan(text: '」 which is pronounced here as 「'),
                  VocabTooltip(
                    message: nichi,
                    inlineSpan: const TextSpan(
                      text: 'にち',
                    ),
                  ),
                  const TextSpan(text: '） which are pronounced 「'),
                  VocabTooltip(
                    message: shigatsu,
                    inlineSpan: const TextSpan(
                      text: 'しがつ',
                    ),
                  ),
                  const TextSpan(text: '」. For example, the 26th becomes '),
                  VocabTooltip(
                    message: nijuurokunichi,
                    inlineSpan: const TextSpan(
                      text: '26日',
                    ),
                  ),
                  const TextSpan(text: '（'),
                  VocabTooltip(
                    message: nijuurokunichi,
                    inlineSpan: const TextSpan(
                      text: 'にじゅうろくにち',
                    ),
                  ),
                  const TextSpan(text: '）. Pretty simple, '),
                  const TextSpan(
                    text: 'however',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                      text:
                          ', the first 10 days, the 14th, 19th, 20th, 29th have special readings that you must separately memorize. If you like memorizing things, you’ll have a ball here. Notice that the kanji doesn’t change but the reading does.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Days of the month'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Day'),
                            TableHeading(text: 'Kanji'),
                            TableHeading(text: 'Reading'),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('What day'),
                            ),
                            TableData(
                              content: Text('何日'),
                            ),
                            TableData(
                              content: Text('なん・にち'),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('1st'),
                            ),
                            const TableData(
                              content: Text('一日'),
                            ),
                            TableData(
                              content: Text(
                                'ついたち',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('2nd'),
                            ),
                            const TableData(
                              content: Text('二日'),
                            ),
                            TableData(
                              content: Text(
                                'ふつ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('3rd'),
                            ),
                            const TableData(
                              content: Text('三日'),
                            ),
                            TableData(
                              content: Text(
                                'みっ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('4th'),
                            ),
                            const TableData(
                              content: Text('四日'),
                            ),
                            TableData(
                              content: Text(
                                'よっ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('5th'),
                            ),
                            const TableData(
                              content: Text('五日'),
                            ),
                            TableData(
                              content: Text(
                                'いつ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('6th'),
                            ),
                            const TableData(
                              content: Text('六日'),
                            ),
                            TableData(
                              content: Text(
                                'むい・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('7th'),
                            ),
                            const TableData(
                              content: Text('七日'),
                            ),
                            TableData(
                              content: Text(
                                'なの・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('8th'),
                            ),
                            const TableData(
                              content: Text('八日'),
                            ),
                            TableData(
                              content: Text(
                                'よう・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('9th'),
                            ),
                            const TableData(
                              content: Text('九日'),
                            ),
                            TableData(
                              content: Text(
                                'ここの・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('10th'),
                            ),
                            const TableData(
                              content: Text('十日'),
                            ),
                            TableData(
                              content: Text(
                                'とお・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('11th'),
                            ),
                            TableData(
                              content: Text('十一日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・いち・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('12th'),
                            ),
                            TableData(
                              content: Text('十二日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・に・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('13th'),
                            ),
                            TableData(
                              content: Text('十三日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・さん・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('14th'),
                            ),
                            const TableData(
                              content: Text('十四日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・よっ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('15th'),
                            ),
                            TableData(
                              content: Text('十五日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・ご・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('16th'),
                            ),
                            TableData(
                              content: Text('十六日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・ろく・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('17th'),
                            ),
                            const TableData(
                              content: Text('十七日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・しち・にち',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('18th'),
                            ),
                            TableData(
                              content: Text('十八日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・はち・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('19th'),
                            ),
                            const TableData(
                              content: Text('十九日'),
                            ),
                            TableData(
                              content: Text(
                                'じゅう・く・にち',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('20th'),
                            ),
                            const TableData(
                              content: Text('二十日'),
                            ),
                            TableData(
                              content: Text(
                                'はつ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('21st'),
                            ),
                            TableData(
                              content: Text('二十一日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・いち・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('22nd'),
                            ),
                            TableData(
                              content: Text('二十二日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・に・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('23rd'),
                            ),
                            TableData(
                              content: Text('二十三日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・さん・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('24th'),
                            ),
                            const TableData(
                              content: Text('二十四日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・よっ・か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('25th'),
                            ),
                            TableData(
                              content: Text('二十五日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・ご・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('26th'),
                            ),
                            TableData(
                              content: Text('二十六日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・ろく・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('27th'),
                            ),
                            const TableData(
                              content: Text('二十七日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・しち・にち',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('28th'),
                            ),
                            TableData(
                              content: Text('二十八日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・はち・にち',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableData(
                              content: Text('29th'),
                            ),
                            const TableData(
                              content: Text('二十九日'),
                            ),
                            TableData(
                              content: Text(
                                'に・じゅう・く・にち',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('30th'),
                            ),
                            TableData(
                              content: Text('三十日'),
                            ),
                            TableData(
                              content: Text(
                                'さん・じゅう・にち',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableData(
                              content: Text('31st'),
                            ),
                            TableData(
                              content: Text('	三十一日'),
                            ),
                            TableData(
                              content: Text(
                                'さん・じゅう・いち・にち',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'In Japan, the full format for dates follows the international date format and looks like: XXXX年YY月ZZ日. For example, today’s date would be: '),
                  VocabTooltip(
                    message: nisensannen,
                    inlineSpan: const TextSpan(
                      text: '2003年',
                    ),
                  ),
                  VocabTooltip(
                    message: juunigatsu,
                    inlineSpan: const TextSpan(
                      text: '12月',
                    ),
                  ),
                  VocabTooltip(
                    message: futsuka,
                    inlineSpan: const TextSpan(
                      text: '2日',
                    ),
                  ),
                  const TextSpan(text: '.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 平成 【へい・せい】 – Heisei era 昭和 【しょう・わ】 – Showa era 和暦 【わ・れき】 – Japanese calendar 一日 【いち・にち】 – one day The year is very easy. All you have to do is say the number and add 「年」 which is pronounced here as 「ねん」. For example, Year 2003 becomes 2003年 (にせんさんねん）. The catch is that there is another calendar which starts over every time a new emperor ascends the throne. The year is preceded by the era, for example the year 2000 is: 平成12年. My birthday, 1981 is 昭和56年 (The Showa era lasted from 1926 to 1989). You may think that you don’t need to know this but if you’re going to be filling out forms in Japan, they often ask you for your birthday or the current date in the Japanese calendar （和暦）. So here’s a neat converter you can use to convert to the Japanese calendar. Saying the months is actually easier than English because all you have to do is write the number (either in numerals or kanji) of the month and add 「月」 which is read as 「がつ」. However, you need to pay attention to April （４月）, July （７月）, and September （９月） which are pronounced 「しがつ」、 「しちがつ」、and 「くがつ」 respectively. Finally, we get to the days of the month, which is where the headache starts. The first day of the month is 「ついたち」 （一日）; different from 「いちにち」 （一日）, which means “one day”. Besides this and some other exceptions we’ll soon cover, you can simply say the number and add 「日」 which is pronounced here as 「にち」. For example, the 26th becomes 26日（にじゅうろくにち）. Pretty simple, however, the first 10 days, the 14th, 19th, 20th, 29th have special readings that you must separately memorize. If you like memorizing things, you’ll have a ball here. Notice that the kanji doesn’t change but the reading does. In Japan, the full format for dates follows the international date format and looks like: XXXX年YY月ZZ日. For example, today’s date would be: 2003年12月2日.',
      grammars: ['日付'],
      keywords: ['dates','year','month','day'],
    ),
    Section(
      heading: 'Time',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Now, we’ll learn how to tell time. The hour is given by saying the number and adding 「'),
                  VocabTooltip(
                    message: ji,
                    inlineSpan: const TextSpan(
                      text: '時',
                    ),
                  ),
                  const TextSpan(text: '」 which is pronounced here as 「'),
                  VocabTooltip(
                    message: ji,
                    inlineSpan: const TextSpan(
                      text: 'じ',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. Here is a chart of exceptions to look out for.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'English'),
                        TableData(
                          content: Text('4 o’clock'),
                        ),
                        TableData(
                          content: Text('7 o’clock'),
                        ),
                        TableData(
                          content: Text('9 o’clock'),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Kanji'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: yoji,
                              inlineSpan: const TextSpan(
                                text: '四時',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: shichiji,
                              inlineSpan: const TextSpan(
                                text: '七時',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: kuji,
                              inlineSpan: const TextSpan(
                                text: '九時',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Reading'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: yoji,
                              inlineSpan: const TextSpan(
                                text: 'よじ',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: shichiji,
                              inlineSpan: const TextSpan(
                                text: 'しちじ',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: kuji,
                              inlineSpan: const TextSpan(
                                text: 'くじ',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Notice how the numbers 4, 7, and 9 keep coming up to be a pain in the butt? Well, those and sometimes 1, 6 and 8 are the numbers to watch out for.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'The minutes are given by adding 「'),
                  VocabTooltip(
                    message: fun,
                    inlineSpan: const TextSpan(
                      text: '分',
                    ),
                  ),
                  const TextSpan(text: '」 which usually read as 「'),
                  VocabTooltip(
                    message: fun,
                    inlineSpan: const TextSpan(
                      text: 'ふん',
                    ),
                  ),
                  const TextSpan(text: '」 with the following exceptions:'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'English'),
                        TableData(
                          content: Text('1 min'),
                        ),
                        TableData(
                          content: Text('3 min'),
                        ),
                        TableData(
                          content: Text('4 min'),
                        ),
                        TableData(
                          content: Text('6 min'),
                        ),
                        TableData(
                          content: Text('8 min'),
                        ),
                        TableData(
                          content: Text('10 min'),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Kanji'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: ippun,
                              inlineSpan: const TextSpan(
                                text: '一分',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanpun,
                              inlineSpan: const TextSpan(
                                text: '三分',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: yonpun,
                              inlineSpan: const TextSpan(
                                text: '四分',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: roppun,
                              inlineSpan: const TextSpan(
                                text: '六分',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: happun,
                              inlineSpan: const TextSpan(
                                text: '八分',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: juppun,
                              inlineSpan: const TextSpan(
                                text: '十分',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Reading'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: ippun,
                              inlineSpan: const TextSpan(
                                text: 'いっぷん',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sanpun,
                              inlineSpan: const TextSpan(
                                text: 'さんぷん',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: yonpun,
                              inlineSpan: const TextSpan(
                                text: 'よんぷん',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: roppun,
                              inlineSpan: const TextSpan(
                                text: 'ろっぷん',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: happun,
                              inlineSpan: const TextSpan(
                                text: 'はっぷん',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: juppun,
                              inlineSpan: const TextSpan(
                                text: 'じゅっぷん',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'For higher number, you use the normal pronunciation for the higher digits and rotate around the same readings for 1 to 10. For instance, 24 minutes is 「'),
                  VocabTooltip(
                    message: nijuuyonpun,
                    inlineSpan: const TextSpan(
                      text: 'にじゅうよんぷん',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: nijuuyonpun,
                    inlineSpan: const TextSpan(
                      text: '二十四分',
                    ),
                  ),
                  const TextSpan(text: '） while 30 minutes is 「'),
                  VocabTooltip(
                    message: sanjuppun,
                    inlineSpan: const TextSpan(
                      text: 'さんじゅっぷん',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: sanjuppun,
                    inlineSpan: const TextSpan(
                      text: '三十分',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '）. There are also other less common but still correct pronunciations such as 「'),
                  VocabTooltip(
                    message: hachifun,
                    inlineSpan: const TextSpan(
                      text: 'はちふん',
                    ),
                  ),
                  const TextSpan(text: '」 for 「'),
                  VocabTooltip(
                    message: hachifun,
                    inlineSpan: const TextSpan(
                      text: '八分',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: jippun,
                    inlineSpan: const TextSpan(
                      text: 'じっぷん',
                    ),
                  ),
                  const TextSpan(text: '」 for 「'),
                  VocabTooltip(
                    message: jippun,
                    inlineSpan: const TextSpan(
                      text: '十分',
                    ),
                  ),
                  const TextSpan(text: '」 (this one is almost never used).'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'All readings for seconds consists of the number plus 「'),
                  VocabTooltip(
                    message: byou,
                    inlineSpan: const TextSpan(
                      text: '秒',
                    ),
                  ),
                  const TextSpan(text: '」, which is read as 「'),
                  VocabTooltip(
                    message: byou,
                    inlineSpan: const TextSpan(
                      text: 'びょう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. There are no exceptions for seconds and all the readings are the same.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'Some examples of time.'),
                ],
              ),
              const NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('1時24分'),
                      Text('（いちじ・にじゅうよんぷん）'),
                      Text('1:24'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('午後4時10分'),
                      Text('（ごご・よじ・じゅっぷん）'),
                      Text('4:10 PM'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('午前9時16分'),
                      Text('（ごぜん・くじ・じゅうろっぷん）'),
                      Text('9:16 AM'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('13時16分'),
                      Text('（じゅうさんじ・じゅうろっぷん）'),
                      Text('13:16'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('2時18分13秒'),
                      Text('（にじ・じゅうはっぷん・じゅうさんびょう）'),
                      Text('2:18:13'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Now, we’ll learn how to tell time. The hour is given by saying the number and adding 「時」 which is pronounced here as 「じ」. Here is a chart of exceptions to look out for. Notice how the numbers 4, 7, and 9 keep coming up to be a pain in the butt? Well, those and sometimes 1, 6 and 8 are the numbers to watch out for. The minutes are given by adding 「分」 which usually read as 「ふん」 with the following exceptions: For higher number, you use the normal pronunciation for the higher digits and rotate around the same readings for 1 to 10. For instance, 24 minutes is 「にじゅうよんぷん」 （二十四分） while 30 minutes is 「さんじゅっぷん」 （三十分）. There are also other less common but still correct pronunciations such as 「はちふん」 for 「八分」 and 「じっぷん」 for 「十分」 (this one is almost never used). All readings for seconds consists of the number plus 「秒」, which is read as 「びょう」. There are no exceptions for seconds and all the readings are the same. Some examples of time. 1時24分 （いちじ・にじゅうよんぷん） 1:24 午後4時10分 （ごご・よじ・じゅっぷん） 4:10 PM 午前9時16分 （ごぜん・くじ・じゅうろっぷん） 9:16 AM 13時16分 （じゅうさんじ・じゅうろっぷん） 13:16 2時18分13秒 （にじ・じゅうはっぷん・じゅうさんびょう） 2:18:13',
      grammars: ['時間'],
      keywords: ['time','hour','minute','clock'],
    ),
    Section(
      heading: 'A span of time',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Ha! I bet you thought you were done with dates and time, well guess again. This time we will learn counters for counting spans of time, days, months, and years. The basic counter for a span of time is 「'),
                  VocabTooltip(
                    message: kan,
                    inlineSpan: const TextSpan(
                      text: '間',
                    ),
                  ),
                  const TextSpan(text: '」, which is read as 「'),
                  VocabTooltip(
                    message: kan,
                    inlineSpan: const TextSpan(
                      text: 'かん',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. You can attach it to the end of hours, days, weeks, and years. Minutes (in general) and seconds do not need this counter and months have a separate counter, which we will cover next.'),
                ],
              ),
              const NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('二時間四十分'),
                      Text('（にじかん・よんじゅっぷん）'),
                      Text('2 hours and 40 minutes'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('二十日間'),
                      Text('（はつかかん）'),
                      Text('20 days'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('十五日間'),
                      Text('（じゅうごにちかん）'),
                      Text('15 days'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('二年間'),
                      Text('（にねんかん）'),
                      Text('two years'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('三週間'),
                      Text('（さんしゅうかん）'),
                      Text('three weeks'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('一日'),
                      Text('（いちにち）'),
                      Text('1 day'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'As mentioned before, a period of one day is 「'),
                  VocabTooltip(
                    message: ichinichi,
                    inlineSpan: const TextSpan(
                      text: '一日',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: ichinichi,
                    inlineSpan: const TextSpan(
                      text: 'いちにち',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '） which is different from the 1st of the month: 「'),
                  VocabTooltip(
                    message: tsuitachi,
                    inlineSpan: const TextSpan(
                      text: 'ついたち',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Pronunciations to watch out for when counting weeks is one week: 「'),
                  VocabTooltip(
                    message: isshuukan,
                    inlineSpan: const TextSpan(
                      text: '一週間',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: isshuukan,
                    inlineSpan: const TextSpan(
                      text: 'いっしゅうかん',
                    ),
                  ),
                  const TextSpan(text: '） and 8 weeks: 「'),
                  VocabTooltip(
                    message: hasshuukan,
                    inlineSpan: const TextSpan(
                      text: '八週間',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: hasshuukan,
                    inlineSpan: const TextSpan(
                      text: 'はっしゅうかん',
                    ),
                  ),
                  const TextSpan(text: '）.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'To count the number of months, you simple take a regular number and add 「か」 and 「'),
                  VocabTooltip(
                    message: getsu,
                    inlineSpan: const TextSpan(
                      text: '月',
                    ),
                  ),
                  const TextSpan(text: '」 which is pronounced here as 「'),
                  VocabTooltip(
                    message: getsu,
                    inlineSpan: const TextSpan(
                      text: 'げつ',
                    ),
                  ),
                  const TextSpan(text: '」 and '),
                  const TextSpan(
                    text: 'not',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(text: ' 「'),
                  VocabTooltip(
                    message: gatsu,
                    inlineSpan: const TextSpan(
                      text: 'がつ',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. The 「か」 used in this counter is usually written as a small katakana 「ヶ」 which is confusing because it’s still pronounced as 「か」 and not 「け」. The small 「ヶ」 is actually totally different from the katakana 「ケ」 and is really an abbreviation for the kanji 「箇」, the original kanji for the counter. This small 「ヶ」 is also used in some place names such as 「'),
                  VocabTooltip(
                    message: sendagaya,
                    inlineSpan: TextSpan(
                      children: [
                        const TextSpan(text: '千駄'),
                        TextSpan(
                          text: 'ヶ',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(text: '谷'),
                      ],
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 and other counters, such as the counter for location described in the “Other Counters” section below.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In counting months, you should watch out for the following sound changes:'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'English'),
                        TableData(
                          content: Text('1 month'),
                        ),
                        TableData(
                          content: Text('6 months'),
                        ),
                        TableData(
                          content: Text('10 months'),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Kanji'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: ikkagetsu,
                              inlineSpan: const TextSpan(
                                text: '一ヶ月',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: rokkagetsu,
                              inlineSpan: const TextSpan(
                                text: '六ヶ月',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: jukkagetsu,
                              inlineSpan: const TextSpan(
                                text: '十ヶ月',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        const TableHeading(text: 'Reading'),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: ikkagetsu,
                              inlineSpan: const TextSpan(
                                text: 'いっかげつ',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: rokkagetsu,
                              inlineSpan: const TextSpan(
                                text: 'ろっかげつ',
                              ),
                            ),
                          ),
                        ),
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: jukkagetsu,
                              inlineSpan: const TextSpan(
                                text: 'じゅっかげつ',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Just like minutes, the high numbers rotate back using the same sounds for 1 to 10.'),
                ],
              ),
              const NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('十一ヶ月'),
                      Text('（じゅういっかげつ）'),
                      Text('Eleven months'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('二十ヶ月'),
                      Text('（にじゅっかげつ）'),
                      Text('Twenty months'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('三十三ヶ月'),
                      Text('（さんじゅうさんかげつ）'),
                      Text('Thirty three months'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Ha! I bet you thought you were done with dates and time, well guess again. This time we will learn counters for counting spans of time, days, months, and years. The basic counter for a span of time is 「間」, which is read as 「かん」. You can attach it to the end of hours, days, weeks, and years. Minutes (in general) and seconds do not need this counter and months have a separate counter, which we will cover next. 二時間四十分 （にじかん・よんじゅっぷん） 2 hours and 40 minutes 二十日間（はつかかん） 20 days 十五日間 （じゅうごにちかん） 15 days 二年間 （にねんかん） two years 三週間 （さんしゅうかん） three weeks 一日 （いちにち） 1 day As mentioned before, a period of one day is 「一日」 （いちにち） which is different from the 1st of the month: 「ついたち」. Pronunciations to watch out for when counting weeks is one week: 「一週間」 （いっしゅうかん） and 8 weeks: 「八週間」 （はっしゅうかん）. To count the number of months, you simple take a regular number and add 「か」 and 「月」 which is pronounced here as 「げつ」 and not 「がつ」. The 「か」 used in this counter is usually written as a small katakana 「ヶ」 which is confusing because it’s still pronounced as 「か」 and not 「け」. The small 「ヶ」 is actually totally different from the katakana 「ケ」 and is really an abbreviation for the kanji 「箇」, the original kanji for the counter. This small 「ヶ」 is also used in some place names such as 「千駄ヶ谷」 and other counters, such as the counter for location described in the “Other Counters” section below. In counting months, you should watch out for the following sound changes: Just like minutes, the high numbers rotate back using the same sounds for 1 to 10. 十一ヶ月 （じゅういっかげつ） Eleven months 二十ヶ月（にじゅっかげつ） Twenty months 三十三ヶ月 （さんじゅうさんかげつ） Thirty three months',
      grammars: [],
      keywords: ['span','time','hour','day','week','year'],
    ),
    Section(
      heading: 'Other counters',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We’ll cover some of the most common counters so that you’ll be familiar with how counters work. This will hopefully allow you to learn other counters on your own because there are too many to even consider covering them all. The important thing to remember is that using the wrong counter is grammatically incorrect. If you are counting people, you '),
                  TextSpan(
                    text: 'must',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' use the people counter, etc. Sometimes, it is acceptable to use a more generic counter when a less commonly used counter applies. Here are some counters.'),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Table(
                  border: TableBorder.all(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  children: [
                    const TableRow(
                      children: [
                        TableHeading(text: 'Counter'),
                        TableHeading(text: 'When to Use'),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: nin,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count the number of people',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: honCounter,
                              inlineSpan: const TextSpan(
                                text: '本',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count long, cylindrical objects such as bottles or chopsticks',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: mai,
                              inlineSpan: const TextSpan(
                                text: '枚',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count thin objects such as paper or shirts',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: satsu,
                              inlineSpan: const TextSpan(
                                text: '冊',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count bound objects usually books',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: hiki,
                              inlineSpan: const TextSpan(
                                text: '匹',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count small animals like cats or dogs',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: sai,
                              inlineSpan: const TextSpan(
                                text: '歳',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count the age of a living creatures such as people',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: ko,
                              inlineSpan: const TextSpan(
                                text: '個',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count small (often round) objects',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: kai,
                              inlineSpan: const TextSpan(
                                text: '回',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count number of times',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: kasho,
                              inlineSpan: const TextSpan(
                                text: 'ヶ所（箇所）',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count number of locations',
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableData(
                          content: Text.rich(
                            VocabTooltip(
                              message: tsu,
                              inlineSpan: const TextSpan(
                                text: 'つ',
                              ),
                            ),
                          ),
                        ),
                        const TableData(
                          content: Text(
                            'To count any generic object that has a rare or no counter',
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption:
                            'Counting 1 to 10 (some variations might exist)'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '人'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'ひとり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'ふたり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんにん',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よにん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごにん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろくにん',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'しちにん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はちにん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうにん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅうにん',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '本'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっぽん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にほん',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんぼん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんほん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごほん',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろっぽん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななほん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はちほん',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうほん',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっぽん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: const [
                        TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '枚'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いちまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろくまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はちまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうまい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅうまい',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '冊'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっさつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にさつ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんさつ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんさつ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごさつ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろくさつ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななさつ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はっさつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうさつ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっさつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '匹'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっぴき',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にひき',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんびき',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんひき',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごひき',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろっぴき',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななひき',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はっぴき',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうひき',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっぴき',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '歳'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっさい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にさい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんさい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんさい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごさい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろくさい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななさい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はっさい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうさい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっさい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '個'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっこ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にこ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんこ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんこ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごこ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろっこ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななこ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はっこ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうこ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっこ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: '回'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっかい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にかい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんかい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんかい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごかい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろっかい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななかい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はちかい',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうかい',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっかい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: 'ヶ所（箇所）'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'いっかしょ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'にかしょ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'さんかしょ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よんかしょ',
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'ごかしょ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'ろっかしょ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななかしょ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'はっかしょ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'きゅうかしょ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'じゅっかしょ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: ' '),
                            TableHeading(text: 'つ'),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '1'),
                            TableData(
                              content: Text(
                                'ひとつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '2'),
                            TableData(
                              content: Text(
                                'ふたつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '3'),
                            TableData(
                              content: Text(
                                'みっつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '4'),
                            TableData(
                              content: Text(
                                'よっつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '5'),
                            TableData(
                              content: Text(
                                'いつつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '6'),
                            TableData(
                              content: Text(
                                'むっつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const TableRow(
                          children: [
                            TableHeading(text: '7'),
                            TableData(
                              content: Text(
                                'ななつ',
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '8'),
                            TableData(
                              content: Text(
                                'やっつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '9'),
                            TableData(
                              content: Text(
                                'ここのつ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(text: '10'),
                            TableData(
                              content: Text(
                                'とお',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'The changed sounds have been highlighted.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You don’t count 0 because there is nothing to count. You can simply use 「'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いない',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. The chart has hiragana for pronunciation but, as before, it is usually written with either numbers or kanji plus the counter with the single exception of 「'),
                  VocabTooltip(
                    message: too,
                    inlineSpan: const TextSpan(
                      text: 'とお',
                    ),
                  ),
                  const TextSpan(text: '」 which is simply written as 「'),
                  VocabTooltip(
                    message: too,
                    inlineSpan: const TextSpan(
                      text: '十',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'For higher numbers, it’s the same as before, you use the normal pronunciation for the higher digits and rotate around the same readings for 1 to 10 except for 「'),
                  VocabTooltip(
                    message: hitori,
                    inlineSpan: const TextSpan(
                      text: '一人',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: futari,
                    inlineSpan: const TextSpan(
                      text: '二人',
                    ),
                  ),
                  const TextSpan(text: '」 which transforms to the normal 「'),
                  VocabTooltip(
                    message: ichi,
                    inlineSpan: const TextSpan(
                      text: 'いち',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: ni,
                    inlineSpan: const TextSpan(
                      text: 'に',
                    ),
                  ),
                  const TextSpan(
                      text: '」 once you get past the first two. So 「'),
                  VocabTooltip(
                    message: hitori,
                    inlineSpan: const TextSpan(
                      text: '一人',
                    ),
                  ),
                  const TextSpan(text: '」 is 「'),
                  VocabTooltip(
                    message: hitori,
                    inlineSpan: const TextSpan(
                      text: 'ひとり',
                    ),
                  ),
                  const TextSpan(text: '」 while 「'),
                  VocabTooltip(
                    message: juuichinin,
                    inlineSpan: const TextSpan(
                      text: '11人',
                    ),
                  ),
                  const TextSpan(text: '」 is 「'),
                  VocabTooltip(
                    message: juuichinin,
                    inlineSpan: const TextSpan(
                      text: 'じゅういちにん',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. Also, the generic counter 「～つ」 only applies up to exactly ten items. Past that, you can just use regular plain numbers.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Note: The counter for age is often sometimes written as 「'),
                  VocabTooltip(
                    message: saiSimplified,
                    inlineSpan: const TextSpan(
                      text: '才',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 for those who don’t have the time to write out the more complex kanji. Plus, age 20 is usually read as 「'),
                  VocabTooltip(
                    message: hatachi,
                    inlineSpan: const TextSpan(
                      text: 'はたち',
                    ),
                  ),
                  const TextSpan(text: '」 and not 「にじゅっさい」.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We’ll cover some of the most common counters so that you’ll be familiar with how counters work. This will hopefully allow you to learn other counters on your own because there are too many to even consider covering them all. The important thing to remember is that using the wrong counter is grammatically incorrect. If you are counting people, you must use the people counter, etc. Sometimes, it is acceptable to use a more generic counter when a less commonly used counter applies. Here are some counters. 人 To count the number of people 本 To count long, cylindrical objects such as bottles or chopsticks 枚 To count thin objects such as paper or shirts 冊 To count bound objects usually books 匹 To count small animals like cats or dogs 歳 To count the age of a living creatures such as people 個 To count small (often round) objects 回 To count number of times ヶ所（箇所） To count number of locations つ To count any generic object that has a rare or no counter The changed sounds have been highlighted. You don’t count 0 because there is nothing to count. You can simply use 「ない」 or 「いない」. The chart has hiragana for pronunciation but, as before, it is usually written with either numbers or kanji plus the counter with the single exception of 「とお」 which is simply written as 「十」. For higher numbers, it’s the same as before, you use the normal pronunciation for the higher digits and rotate around the same readings for 1 to 10 except for 「一人」 and 「二人」 which transforms to the normal 「いち」 and 「に」 once you get past the first two. So 「一人」 is 「ひとり」 while 「11人」 is 「じゅういちにん」. Also, the generic counter 「～つ」 only applies up to exactly ten items. Past that, you can just use regular plain numbers. Note: The counter for age is often sometimes written as 「才」 for those who don’t have the time to write out the more complex kanji. Plus, age 20 is usually read as 「はたち」 and not 「にじゅっさい」.',
      grammars: [],
      keywords: ['counters','age','sai','animals','hiki','people','nin','tsu','books','satsu','ko','sheets','mai','cylinders','hon','times','kai'],
    ),
    Section(
      heading: 'Using 「目」 to show order',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(text: 'You can attach 「'),
                  VocabTooltip(
                    message: me,
                    inlineSpan: const TextSpan(
                      text: '目',
                    ),
                  ),
                  const TextSpan(text: '」 (read as 「'),
                  VocabTooltip(
                    message: me,
                    inlineSpan: const TextSpan(
                      text: 'め',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」) to various counters to indicate the order. The most common example is the 「'),
                  VocabTooltip(
                    message: ban,
                    inlineSpan: const TextSpan(
                      text: '番',
                    ),
                  ),
                  const TextSpan(text: '」 counter. For example, 「'),
                  VocabTooltip(
                    message: ichiban,
                    inlineSpan: const TextSpan(
                      text: '一番',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」　which means “number one” becomes “the first” when you add 「'),
                  VocabTooltip(
                    message: me,
                    inlineSpan: const TextSpan(
                      text: '目',
                    ),
                  ),
                  const TextSpan(text: '」 （'),
                  VocabTooltip(
                    message: ichibanme,
                    inlineSpan: const TextSpan(
                      text: '一番目',
                    ),
                  ),
                  const TextSpan(text: '）. Similarly, 「'),
                  VocabTooltip(
                    message: ikkaime,
                    inlineSpan: const TextSpan(
                      text: '一回目',
                    ),
                  ),
                  const TextSpan(text: '」 is the first time, 「'),
                  VocabTooltip(
                    message: nikaime,
                    inlineSpan: const TextSpan(
                      text: '二回目',
                    ),
                  ),
                  const TextSpan(text: '」 is the second time, 「'),
                  VocabTooltip(
                    message: yoninme,
                    inlineSpan: const TextSpan(
                      text: '四人目',
                    ),
                  ),
                  const TextSpan(text: '」 is the fourth person, and so on.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'You can attach 「目」 (read as 「め」) to various counters to indicate the order. The most common example is the 「番」 counter. For example, 「一番」　which means “number one” becomes “the first” when you add 「目」 （一番目）. Similarly, 「一回目」 is the first time, 「二回目」 is the second time, 「四人目」 is the fourth person, and so on.',
      grammars: ['～目'],
      keywords: ['me','order','first','second','third'],
    ),
  ],
);
