import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/customs/youtube_link.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson people = Lesson(
  title: 'Addressing people',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              YouTubeLink(title: 'Learn Japanese from Scratch 2.1.3 - Addressing People', uri: 'https://youtu.be/QK7HCBqc_-c'),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Not only is it important to use the right type of language with the right people, it is also important to address them by the right name. It is also important to address yourself with the proper level of politeness. Japanese is special in that there are so many ways of saying the simple words, “I” and “you”. We will go over some of ways to refer to yourself and others.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Not only is it important to use the right type of language with the right people, it is also important to address them by the right name. It is also important to address yourself with the proper level of politeness. Japanese is special in that there are so many ways of saying the simple words, “I” and “you”. We will go over some of ways to refer to yourself and others.',
      grammars: [],
      keywords: ['people','addressing'],
    ),
    Section(
      heading: 'Referring to yourself',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const BulletedList(
                items: [
                  Text('名前 【な・まえ】 – name'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'There are many ways to say “I” in Japanese. Some of these words are not as common and others are hopelessly outdated. We will go over the most common ones that are in use today. The usages of all the different words for “I” is separated into two categories: gender and politeness. In other words, there are words that are usually used by males and words that are usually only used by females and they all depend on the social context.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text: 'Before going into this: a note about the word 「'),
                  VocabTooltip(
                    message: watashi,
                    inlineSpan: const TextSpan(
                      text: '私',
                    ),
                  ),
                  const TextSpan(
                    text: '」. The official reading of the kanji is 「',
                  ),
                  VocabTooltip(
                    message: watakushi,
                    inlineSpan: const TextSpan(
                      text: 'わたくし',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. This is the reading you use in a formal context (for example, a speech by the president of a company). This reading will probably be accompanied with honorific and humble forms, which we will cover later. In all other situations, it is usually read as 「',
                  ),
                  VocabTooltip(
                    message: watashi,
                    inlineSpan: const TextSpan(
                      text: 'わたし',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. This is the most generic reference to “I” in terms of politeness and gender; therefore it is usually one of the first words taught to students of Japanese. Here is a list of the most common words for “I” and how they are used:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: watakushi,
                          inlineSpan: const TextSpan(
                            text: '私',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' 【わたくし】 – Used by both males and females for formal situations.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: watashi,
                          inlineSpan: const TextSpan(
                            text: '私',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' 【わたし】 – Used by both males and females for normal polite situations.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: boku,
                          inlineSpan: const TextSpan(
                            text: '僕',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' 【ぼく】 – Used primarily by males from fairly polite to fairly casual situations.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ore,
                          inlineSpan: const TextSpan(
                            text: '俺',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' 【おれ】 – A very rough version of “I” used almost exclusively by males in very casual situations.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: atashi,
                          inlineSpan: const TextSpan(
                            text: 'あたし',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' – A very feminine and casual way to refer to oneself. Many girls have decided to opt for 「わたし」 instead because 「あたし」 has a cutesy and girly sound.',
                        ),
                      ],
                    ),
                  ),
                  const Text(
                    'One’s own name – Also a very feminine and kind of childish way to refer to oneself.',
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: washi,
                          inlineSpan: const TextSpan(
                            text: 'わし',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' – Usually used by older men well in their middle-ages.',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Let’s see how different types of sentences use the appropriate version of “I”. 「',
                  ),
                  VocabTooltip(
                    message: watakushi,
                    inlineSpan: const TextSpan(
                      text: 'わたくし',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is left out because we have yet to go over very formal grammatical expressions.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'はキムです。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My name is Kim. (Neutral, polite)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: boku,
                              inlineSpan: const TextSpan(
                                text: '僕',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'はキムです。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My name is Kim. (Masculine, polite)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: boku,
                              inlineSpan: const TextSpan(
                                text: '僕',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'はボブだ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My name is Bob. (Masculine, casual)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ore,
                              inlineSpan: const TextSpan(
                                text: '俺',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'はボブだ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My name is Bob. (Masculine, casual)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: atashi,
                              inlineSpan: const TextSpan(
                                text: 'あたし',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'はアリス。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My name is Alice. (Feminine, casual)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 名前 【な・まえ】 – name There are many ways to say “I” in Japanese. Some of these words are not as common and others are hopelessly outdated. We will go over the most common ones that are in use today. The usages of all the different words for “I” is separated into two categories: gender and politeness. In other words, there are words that are usually used by males and words that are usually only used by females and they all depend on the social context. Before going into this: a note about the word 「私」. The official reading of the kanji is 「わたくし」. This is the reading you use in a formal context (for example, a speech by the president of a company). This reading will probably be accompanied with honorific and humble forms, which we will cover later. In all other situations, it is usually read as 「わたし」. This is the most generic reference to “I” in terms of politeness and gender; therefore it is usually one of the first words taught to students of Japanese. Here is a list of the most common words for “I” and how they are used: 私 【わたくし】 – Used by both males and females for formal situations. 私 【わたし】 – Used by both males and females for normal polite situations. 僕 【ぼく】 – Used primarily by males from fairly polite to fairly casual situations. 俺 【おれ】 – A very rough version of “I” used almost exclusively by males in very casual situations. あたし – A very feminine and casual way to refer to oneself. Many girls have decided to opt for 「わたし」 instead because 「あたし」 has a cutesy and girly sound. One’s own name – Also a very feminine and kind of childish way to refer to oneself. わし – Usually used by older men well in their middle-ages. Let’s see how different types of sentences use the appropriate version of “I”. 「わたくし」 is left out because we have yet to go over very formal grammatical expressions. 私の名前はキムです。 My name is Kim. (Neutral, polite) 僕の名前はキムです。 My name is Kim. (Masculine, polite) 僕の名前はボブだ。 My name is Bob. (Masculine, casual) 俺の名前はボブだ。 My name is Bob. (Masculine, casual) あたしの名前はアリス。 My name is Alice. (Feminine, casual)',
      grammars: [],
      keywords: ['I','me','oneself','pronoun','first-person','watashi','watakushi','boku','ore','atashi','washi'],
    ),
    Section(
      heading: 'Referring to others by name',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('社長 【しゃ・ちょう】 – company president'),
                  Text('課長 【か・ちょう】 – section manager'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Japanese does not require the use of “you” nearly as much as English does. I hope that the examples with Bob, Alice, and Jim have shown that people refer to other people by their names even when they are directly addressing that person. Another common way to address people is by their title such as 「',
                  ),
                  VocabTooltip(
                    message: shachou,
                    inlineSpan: const TextSpan(
                      text: '社長',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: kachou,
                    inlineSpan: const TextSpan(
                      text: '課長',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: sensei,
                    inlineSpan: const TextSpan(
                      text: '先生',
                    ),
                  ),
                  const TextSpan(
                    text: '」, etc. The word 「',
                  ),
                  VocabTooltip(
                    message: sensei,
                    inlineSpan: const TextSpan(
                      text: '先生',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is used to generally mean any person who has significant knowledge and expertise in something. For example, people usually use 「',
                  ),
                  VocabTooltip(
                    message: sensei,
                    inlineSpan: const TextSpan(
                      text: '先生',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 when directly addressing doctors or teachers (obviously). You can also include the person’s last name such as 「田中',
                  ),
                  VocabTooltip(
                    message: sensei,
                    inlineSpan: const TextSpan(
                      text: '先生',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 (teacher Tanaka). In the case where your relationship with the person doesn’t involve any title, you can use their name (usually their last name) attached with 「さん」 to show politeness. If calling them by their last name seems a little too polite and distant, the practice of attaching 「さん」 to their first name also exists. More endearing and colloquial versions of 「さん」 include 「くん」 and 「ちゃん」. 「くん」 is usually attached to the name of males who are of equal or lower social position. (For example, my boss sometimes calls me 「キムくん」). 「ちゃん」 is a very endearing way to refer to usually females of equal or lower social position.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 社長 【しゃ・ちょう】 – company president 課長 【か・ちょう】 – section manager 先生 【せん・せい】 – teacher 田中 【た・なか】 – Tanaka (last name) Japanese does not require the use of “you” nearly as much as English does. I hope that the examples with Bob, Alice, and Jim have shown that people refer to other people by their names even when they are directly addressing that person. Another common way to address people is by their title such as 「社長」、「課長」、「先生」, etc. The word 「先生」 is used to generally mean any person who has significant knowledge and expertise in something. For example, people usually use 「先生」 when directly addressing doctors or teachers (obviously). You can also include the person’s last name such as 「田中先生」 (teacher Tanaka). In the case where your relationship with the person doesn’t involve any title, you can use their name (usually their last name) attached with 「さん」 to show politeness. If calling them by their last name seems a little too polite and distant, the practice of attaching 「さん」 to their first name also exists. More endearing and colloquial versions of 「さん」 include 「くん」 and 「ちゃん」. 「くん」 is usually attached to the name of males who are of equal or lower social position. (For example, my boss sometimes calls me 「キムくん」). 「ちゃん」 is a very endearing way to refer to usually females of equal or lower social position.',
      grammars: [],
      keywords: ['you','second-person','honorific','title'],
    ),
    Section(
      heading: 'Referring to others with “you”',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Please do not use 「',
                  ),
                  VocabTooltip(
                    message: anata,
                    inlineSpan: const TextSpan(
                      text: 'あなた',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 just like you would use the word “you” in English. In directly addressing people, there are three levels of politeness: 1) Using the person’s name with the appropriate suffix, 2) Not using anything at all, 3) Using 「',
                  ),
                  VocabTooltip(
                    message: anata,
                    inlineSpan: const TextSpan(
                      text: 'あなた',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. In fact, by the time you get to three, you’re dangerously in the area of being rude. Most of the time, you do not need to use anything at all because you are directly addressing the person. Constantly pounding the listener with “you” every sentence sounds like you are accusing the person of something.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: anata,
                    inlineSpan: const TextSpan(
                      text: 'あなた',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is also an old-fashioned way for women to refer to their husband or lover. Unless you are a middle-aged women with a Japanese husband, I doubt you will be using 「',
                  ),
                  VocabTooltip(
                    message: anata,
                    inlineSpan: const TextSpan(
                      text: 'あなた',
                    ),
                  ),
                  const TextSpan(
                    text: '」 in this fashion as well.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here is a list of some words meaning “you” in English. You will rarely need to use any of these words, especially the ones in the second half of the list.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: anata,
                          inlineSpan: const TextSpan(
                            text: 'あなた',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' – Generally only used when there is no way to physically address the person or know the person’s name. For example, direct questions to the reader on a form that the reader must fill out would use 「',
                        ),
                        VocabTooltip(
                          message: anata,
                          inlineSpan: const TextSpan(
                            text: 'あなた',
                          ),
                        ),
                        const TextSpan(
                          text: '」.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kimi,
                          inlineSpan: const TextSpan(
                            text: '君',
                          ),
                        ),
                        const TextSpan(
                          text:
                              '【きみ】 – Can be a very close and assuming way to address girls (especially by guys). Can also be kind of rude.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: omae,
                          inlineSpan: const TextSpan(
                            text: 'お前',
                          ),
                        ),
                        const TextSpan(
                          text:
                              '【お・まえ】 – A very rough and coarse way to address someone. Usually used by guys and often changed to 「',
                        ),
                        VocabTooltip(
                          message: omee,
                          inlineSpan: const TextSpan(
                            text: 'おめえ',
                          ),
                        ),
                        const TextSpan(
                          text: '」.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: anta,
                          inlineSpan: const TextSpan(
                            text: 'あんた',
                          ),
                        ),
                        const TextSpan(
                          text:
                              ' – A very assuming and familiar way to address someone. The person using this is maybe miffed off about something.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: temae,
                          inlineSpan: const TextSpan(
                            text: '手前',
                          ),
                        ),
                        const TextSpan(
                          text: '【て・まえ】 – Very rude. Like 「',
                        ),
                        VocabTooltip(
                          message: omae,
                          inlineSpan: const TextSpan(
                            text: 'お前',
                          ),
                        ),
                        const TextSpan(
                          text:
                              '」, to add extra punch, people will usually say it like, 「',
                        ),
                        VocabTooltip(
                          message: temee,
                          inlineSpan: const TextSpan(
                            text: 'てめ～～',
                          ),
                        ),
                        const TextSpan(
                          text:
                              '」. Sounds like you want to beat someone up. I’ve only seen this one used in movies and comic books. In fact, if you try this on your friends, they will probably laugh at you and tell you that you’ve probably been reading too many comic books.',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kisama,
                          inlineSpan: const TextSpan(
                            text: '貴様',
                          ),
                        ),
                        const TextSpan(
                          text:
                              '【き・さま】 – Very, very rude. Sounds like you want to take someone out. I’ve also only seen this one used in comic books. I only go over it so you can understand and enjoy comic books yourself!',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Please do not use 「あなた」 just like you would use the word “you” in English. In directly addressing people, there are three levels of politeness: 1) Using the person’s name with the appropriate suffix, 2) Not using anything at all, 3) Using 「あなた」. In fact, by the time you get to three, you’re dangerously in the area of being rude. Most of the time, you do not need to use anything at all because you are directly addressing the person. Constantly pounding the listener with “you” every sentence sounds like you are accusing the person of something. 「あなた」 is also an old-fashioned way for women to refer to their husband or lover. Unless you are a middle-aged women with a Japanese husband, I doubt you will be using 「あなた」 in this fashion as well. Here is a list of some words meaning “you” in English. You will rarely need to use any of these words, especially the ones in the second half of the list. あなた – Generally only used when there is no way to physically address the person or know the person’s name. For example, direct questions to the reader on a form that the reader must fill out would use 「あなた」. 君【きみ】 – Can be a very close and assuming way to address girls (especially by guys). Can also be kind of rude. お前【お・まえ】 – A very rough and coarse way to address someone. Usually used by guys and often changed to 「おめえ」. あんた – A very assuming and familiar way to address someone. The person using this is maybe miffed off about something. 手前【て・まえ】 – Very rude. Like 「お前」, to add extra punch, people will usually say it like, 「てめ～～」. Sounds like you want to beat someone up. I’ve only seen this one used in movies and comic books. In fact, if you try this on your friends, they will probably laugh at you and tell you that you’ve probably been reading too many comic books. 貴様【き・さま】 – Very, very rude. Sounds like you want to take someone out. I’ve also only seen this one used in comic books. I only go over it so you can understand and enjoy comic books yourself!',
      grammars: [],
      keywords: ['you','second-person','pronoun','anata','anta','kimi','omae','temae','omee','temee','kisama'],
    ),
    Section(
      heading: 'Referring to others in third person',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('ガールフレンド – girlfriend'),
                  Text('ボーイフレンド – boyfriend'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can use 「',
                  ),
                  VocabTooltip(
                    message: kare,
                    inlineSpan: const TextSpan(
                      text: '彼',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kanojo,
                    inlineSpan: const TextSpan(
                      text: '彼女',
                    ),
                  ),
                  const TextSpan(
                    text: '」 for “he” and “she” respectively. Notice that 「',
                  ),
                  VocabTooltip(
                    message: kare,
                    inlineSpan: const TextSpan(
                      text: '彼',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kanojo,
                    inlineSpan: const TextSpan(
                      text: '彼女',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 can also mean “boyfriend” and “girlfriend”. So how can you tell which meaning is being used? Context, of course. For example, if someone asks, 「',
                  ),
                  VocabTooltip(
                    message: kanojo,
                    inlineSpan: const TextSpan(
                      text: '彼女',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'ですか？」 the person is obviously asking if she is your girlfriend because the question, “Is she she?” doesn’t make any sense. Another less commonly used alternative is to say 「',
                  ),
                  VocabTooltip(
                    message: gaarufurendo,
                    inlineSpan: const TextSpan(
                      text: 'ガールフレンド',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: booifurendo,
                    inlineSpan: const TextSpan(
                      text: 'ボーイフレンド',
                    ),
                  ),
                  const TextSpan(
                    text: '」 for, well, I’m sure you can guess what they mean.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 彼 【かれ】 – he; boyfriend 彼女 【かの・じょ】 – she; girlfriend ガールフレンド – girlfriend ボーイフレンド – boyfriend You can use 「彼」 and 「彼女」 for “he” and “she” respectively. Notice that 「彼」 and 「彼女」 can also mean “boyfriend” and “girlfriend”. So how can you tell which meaning is being used? Context, of course. For example, if someone asks, 「彼女ですか？」 the person is obviously asking if she is your girlfriend because the question, “Is she she?” doesn’t make any sense. Another less commonly used alternative is to say 「ガールフレンド」 and 「ボーイフレンド」 for, well, I’m sure you can guess what they mean.',
      grammars: [],
      keywords: ['he','she','they','pronoun','third-person','kare','kanojo','boyfriend','girlfriend'],
    ),
    Section(
      heading: 'Referring to family members',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('母 【はは】 – mother'),
                  Text('お母さん 【お・かあ・さん】 – mother (polite)'),
                  Text('両親 【りょう・しん】 – parents'),
                  Text('父 【ちち】 – father'),
                  Text('お父さん 【お・とう・さん】 – father (polite)'),
                  Text('妻 【つま】 – wife'),
                  Text('奥さん 【おく・さん】 – wife (polite)'),
                  Text('夫 【おっと】 – husband'),
                  Text('主人 【しゅ・じん】 – husband'),
                  Text('姉 【あね】 – older sister'),
                  Text('お姉さん 【お・ねえ・さん】 – older sister (polite)'),
                  Text('兄 【あに】 – older brother'),
                  Text('お兄さん 【お・にい・さん】 – older brother (polite)'),
                  Text('妹 【いもうと】 – younger sister'),
                  Text('弟 【おとうと】 – younger brother'),
                  Text('息子 【むす・こ】 – son'),
                  Text('娘 【むすめ】 – daughter'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Referring to family members is a little more complicated than English. (It could be worse, try learning Korean!) For the purpose of brevity, (since this ',
                  ),
                  const TextSpan(
                    text: 'is',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' a grammar guide and not a vocabulary guide) we will only go over the immediate family. In Japanese, you refer to members of other people’s family more politely than your own. This is only when you are talking about members of your own family to others ',
                  ),
                  const TextSpan(
                    text: 'outside the family',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  const TextSpan(
                    text:
                        '. For example, you would refer to your own mother as 「',
                  ),
                  VocabTooltip(
                    message: haha,
                    inlineSpan: const TextSpan(
                      text: '母',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to people outside your family but you might very well call her 「',
                  ),
                  VocabTooltip(
                    message: okaasan,
                    inlineSpan: const TextSpan(
                      text: 'お母さん',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 at home within your own family. There is also a distinction between older and younger siblings. The following chart list some of the most common terms for family members. There may also be other possibilities not covered in this chart.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableCaption(caption: 'Family member chart'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'One’s own family',
                            ),
                            TableHeading(
                              text: 'Someone else’s family',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Parents',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: ryoushin,
                                      inlineSpan: const TextSpan(
                                        text: '両親',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: goryoushin,
                                          inlineSpan: const TextSpan(
                                            text: 'ご両親',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Mother',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: haha,
                                      inlineSpan: const TextSpan(
                                        text: '母',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: okaasan,
                                          inlineSpan: const TextSpan(
                                            text: 'お母さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Father',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: chichi,
                                      inlineSpan: const TextSpan(
                                        text: '父',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: otousan,
                                          inlineSpan: const TextSpan(
                                            text: 'お父さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Wife',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: tsuma,
                                      inlineSpan: const TextSpan(
                                        text: '妻',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: okusan,
                                          inlineSpan: const TextSpan(
                                            text: '奥さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Husband',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: otto,
                                      inlineSpan: const TextSpan(
                                        text: '夫',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: goshujin,
                                          inlineSpan: const TextSpan(
                                            text: 'ご主人',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Older Sister',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: ane,
                                      inlineSpan: const TextSpan(
                                        text: '姉',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: oneesan,
                                          inlineSpan: const TextSpan(
                                            text: 'お姉さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Older Brother',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: ani,
                                      inlineSpan: const TextSpan(
                                        text: '兄',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: oniisan,
                                          inlineSpan: const TextSpan(
                                            text: 'お兄さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Younger Sister',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: imouto,
                                      inlineSpan: const TextSpan(
                                        text: '妹',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: imoutosan,
                                          inlineSpan: const TextSpan(
                                            text: '妹さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Younger Brother',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: otouto,
                                      inlineSpan: const TextSpan(
                                        text: '弟',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: otoutosan,
                                          inlineSpan: const TextSpan(
                                            text: '弟さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Son',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: musuko,
                                      inlineSpan: const TextSpan(
                                        text: '息子',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: musukosan,
                                          inlineSpan: const TextSpan(
                                            text: '息子さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Daughter',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: musume,
                                      inlineSpan: const TextSpan(
                                        text: '娘',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: musumesan,
                                          inlineSpan: const TextSpan(
                                            text: '娘さん',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Another word for wife, 「',
                  ),
                  VocabTooltip(
                    message: kanai,
                    inlineSpan: const TextSpan(
                      text: '家内',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is often considered politically incorrect because the kanji used are “house” and “inside” which implies that wives belong in the home. Amen. (Just kidding)',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 母 【はは】 – mother お母さん 【お・かあ・さん】 – mother (polite) 両親 【りょう・しん】 – parents 父 【ちち】 – father お父さん 【お・とう・さん】 – father (polite) 妻 【つま】 – wife 奥さん 【おく・さん】 – wife (polite) 夫 【おっと】 – husband 主人 【しゅ・じん】 – husband 姉 【あね】 – older sister お姉さん 【お・ねえ・さん】 – older sister (polite) 兄 【あに】 – older brother お兄さん 【お・にい・さん】 – older brother (polite) 妹 【いもうと】 – younger sister 弟 【おとうと】 – younger brother 息子 【むす・こ】 – son 娘 【むすめ】 – daughte Referring to family members is a little more complicated than English. (It could be worse, try learning Korean!) For the purpose of brevity, (since this is a grammar guide and not a vocabulary guide) we will only go over the immediate family. In Japanese, you refer to members of other people’s family more politely than your own. This is only when you are talking about members of your own family to others outside the family. For example, you would refer to your own mother as 「母」 to people outside your family but you might very well call her 「お母さん」 at home within your own family. There is also a distinction between older and younger siblings. The following chart list some of the most common terms for family members. There may also be other possibilities not covered in this chart. Another word for wife, 「家内」 is often considered politically incorrect because the kanji used are “house” and “inside” which implies that wives belong in the home. Amen. (Just kidding)',
      grammars: [],
      keywords: ['family','mother','father','sister','brother','wife','husband','son','daughter','haha','chichi','ane','ani','tsuma','otto','okaasan','otousan','oneesan','oniisan','imouto','otouto','musume','musuko','okusan','shujin','ryoushin','vocabulary'],
    ),
  ],
);
