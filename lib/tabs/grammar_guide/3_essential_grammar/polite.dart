import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson polite = Lesson(
  title: 'Polite Form and Verb Stems',
  sections: [
    Section(
      heading: 'Not being rude in Japan',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('丁寧語 【てい・ねい・ご】 – polite language'),
                  Text('尊敬語 【そん・けい・ご】 – honorific language'),
                  Text('謙譲語 【けん・じょう・ご】 – humble language'),
                  Text('はい – yes (polite)'),
                  Text('いいえ – no (polite)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The Japanese we have learned so far is all well and good if you’re 5-years old. Unfortunately, adults are expected to use a politer version of the language (called ',
                  ),
                  VocabTooltip(
                    message: teineigo,
                    inlineSpan: const TextSpan(
                      text: '丁寧語',
                    ),
                  ),
                  const TextSpan(
                    text:
                        ') when addressing certain people. People you will probably use ',
                  ),
                  VocabTooltip(
                    message: teineigo,
                    inlineSpan: const TextSpan(
                      text: '丁寧語',
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' with are: 1) people of higher social rank, and 2) people you are not familiar with. Deciding when to use which language is pretty much a matter of “feel”. However, it is a good idea to stick with one form for each person.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Later (probably much later), we will learn an even politer version of the language called honorific （',
                  ),
                  VocabTooltip(
                    message: sonkeigo,
                    inlineSpan: const TextSpan(
                      text: '尊敬語',
                    ),
                  ),
                  const TextSpan(
                    text: '） and humble （',
                  ),
                  VocabTooltip(
                    message: kenjougo,
                    inlineSpan: const TextSpan(
                      text: '謙譲語',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '） form. It will be more useful than you may think because store clerks, receptionists, and such will speak to you in those forms. But for now, let’s concentrate on just ',
                  ),
                  VocabTooltip(
                    message: teineigo,
                    inlineSpan: const TextSpan(
                      text: '丁寧語',
                    ),
                  ),
                  const TextSpan(
                    text: ', which is the base for ',
                  ),
                  VocabTooltip(
                    message: sonkeigo,
                    inlineSpan: const TextSpan(
                      text: '尊敬語',
                    ),
                  ),
                  const TextSpan(
                    text: ' and ',
                  ),
                  VocabTooltip(
                    message: kenjougo,
                    inlineSpan: const TextSpan(
                      text: '謙譲語',
                    ),
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Fortunately, it is not difficult to change casual speech to polite speech. There may be some slight changes to the vocabulary (for example, “yes” and “no” become 「',
                  ),
                  VocabTooltip(
                    message: hai,
                    inlineSpan: const TextSpan(
                      text: 'はい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: iie,
                    inlineSpan: const TextSpan(
                      text: 'いいえ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 respectively in polite speech), and very colloquial types of sentence endings are not used in polite speech. (We will learn about sentence endings in a later section.) Essentially, the only main difference between polite and casual speech comes at the very end of the sentence. You cannot even tell whether a person is speaking in polite or casual speech until the sentence is finished.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 丁寧語 【てい・ねい・ご】 – polite language 尊敬語 【そん・けい・ご】 – honorific language 謙譲語 【けん・じょう・ご】 – humble language はい – yes (polite) いいえ – no (polite) The Japanese we have learned so far is all well and good if you’re 5-years old. Unfortunately, adults are expected to use a politer version of the language (called 丁寧語) when addressing certain people. People you will probably use 丁寧語 with are: 1) people of higher social rank, and 2) people you are not familiar with. Deciding when to use which language is pretty much a matter of “feel”. However, it is a good idea to stick with one form for each person. Later (probably much later), we will learn an even politer version of the language called honorific （尊敬語） and humble （謙譲語） form. It will be more useful than you may think because store clerks, receptionists, and such will speak to you in those forms. But for now, let’s concentrate on just 丁寧語, which is the base for 尊敬語 and 謙譲語. Fortunately, it is not difficult to change casual speech to polite speech. There may be some slight changes to the vocabulary (for example, “yes” and “no” become 「はい」 and 「いいえ」 respectively in polite speech), and very colloquial types of sentence endings are not used in polite speech. (We will learn about sentence endings in a later section.) Essentially, the only main difference between polite and casual speech comes at the very end of the sentence. You cannot even tell whether a person is speaking in polite or casual speech until the sentence is finished.',
      grammars: ['丁寧語'],
      keywords: ['teineigo','polite','stem','verb stem','polite form'],
    ),
    Section(
      heading: 'The stem of verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('怒る 【おこ・る】 (u-verb) – to get angry'),
                  Text('鉄拳 【てっ・けん】 – fist'),
                  Text('休み 【やす・み】 – rest; vacation'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('楽しむ 【たの・しむ】 (u-verb) – to enjoy'),
                  Text('出す 【だ・す】 (u-verb) – to bring out'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('走り出す 【はし・り・だ・す】 (u-verb) – to break into a run'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('替える 【か・える】 (ru-verb) – to switch'),
                  Text('着替える 【き・が・える】 (ru-verb) – to change (clothes)'),
                  Text('付ける 【つ・ける】 (ru-verb) – to attach'),
                  Text('加える 【くわ・える】 (ru-verb) – to add'),
                  Text(
                      '付け加える 【つ・け・くわ・える】 (ru-verb) – to add one thing to another'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('言い出す 【い・い・だ・す】 (u-verb) – to start talking'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In order to conjugate all u-verbs and ru-verbs into their respective polite forms, we will first learn about the stem of verbs. This is often called the ',
                  ),
                  TextSpan(
                    text: 'masu-stem',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' in Japanese textbooks but we will call it just the ',
                  ),
                  TextSpan(
                    text: 'stem',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' because it is used in many more conjugations than just its masu-form. The stem is really great because it’s very easy to produce and is useful in many different types of grammar.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Remove the 「る」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '食べ',
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' The last vowel sound changes from an / u / vowel sound to an / i / vowel sound.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: oyogu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '泳',
                                        ),
                                        TextSpan(
                                          text: 'ぐ ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: oyogu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '泳',
                                        ),
                                        TextSpan(
                                          text: 'ぎ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'くる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'き',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Rules for extracting the stem of verbs',
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The stem when used by itself can be a very specialized and limited way of creating nouns from verbs. While the 「の」 particle allows you to talk about verbs as if they were nouns, the stem actually turns verbs into nouns. In fact, in very rare cases, the stem is used more often than the verb itself. For example, the stem of 「',
                  ),
                  VocabTooltip(
                    message: ikaru,
                    inlineSpan: const TextSpan(
                      text: '怒る',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」（いかる） is used more often than the verb itself. The movie, “Fists of Fury” is translated as 「',
                  ),
                  VocabTooltip(
                    message: ikaru,
                    inlineSpan: const TextSpan(
                      text: '怒り',
                    ),
                  ),
                  const TextSpan(
                    text: 'の',
                  ),
                  VocabTooltip(
                    message: tekken,
                    inlineSpan: const TextSpan(
                      text: '鉄拳',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and not 「',
                  ),
                  VocabTooltip(
                    message: ikaru,
                    inlineSpan: const TextSpan(
                      text: '怒る',
                    ),
                  ),
                  VocabTooltip(
                    message: tekken,
                    inlineSpan: const TextSpan(
                      text: '鉄拳',
                    ),
                  ),
                  const TextSpan(
                    text: '」. In fact, 「',
                  ),
                  VocabTooltip(
                    message: okoru,
                    inlineSpan: const TextSpan(
                      text: '怒る',
                    ),
                  ),
                  const TextSpan(
                    text: '」 will most likely be read as 「',
                  ),
                  VocabTooltip(
                    message: okoru,
                    inlineSpan: const TextSpan(
                      text: 'おこる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, a completely different verb with the same meaning and kanji! There are a number of specific nouns (such as 「',
                  ),
                  VocabTooltip(
                    message: yasumi,
                    inlineSpan: const TextSpan(
                      text: '休み',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」) that are really verb stems that are used like regular nouns. However, in general we cannot take any verb and make it into a noun. For example, the following sentence is wrong.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: const TextSpan(
                                text: '飲み',
                                style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                              style: TextStyle(
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                                style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(This sentence makes sense but no one talks like this)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'However, a useful grammar that works in general for stems of all verbs is using the stem as a target with a motion verb (almost always 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来る',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in this case). This grammar means, “to go or to come to do [some verb]”. Here’s an example.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Tomorrow, go to see movie.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(
                      text: '見',
                    ),
                  ),
                  const TextSpan(
                    text: 'に」 is the stem of 「',
                  ),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(
                      text: '見る',
                    ),
                  ),
                  const TextSpan(
                    text: '」 （which is ',
                  ),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(
                      text: '見',
                    ),
                  ),
                  const TextSpan(
                    text: '） combined with the target particle 「に」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The motion target particle 「へ」 sounds like you’re literally going or coming to something while the 「に」 particle implies that you are going or coming for the purpose of doing something.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: TextSpan(
                                text: '遊び',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'へ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Yesterday, friend came to a playing activity. (Sounds a bit strange)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: TextSpan(
                                text: '遊び',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Yesterday, friend came to play.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The expression 「',
                  ),
                  VocabTooltip(
                    message: tanoshimu,
                    inlineSpan: const TextSpan(
                      text: '楽しみ',
                    ),
                  ),
                  const TextSpan(
                    text: 'に',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 meaning “to look forward to” is formed from grammar similar to this but is a special case and should be considered a set expression.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Other verbs are also sometimes attached to the stem to create new verbs. For example, when 「',
                  ),
                  VocabTooltip(
                    message: dasu,
                    inlineSpan: const TextSpan(
                      text: '出す',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is attached to the stem of 「',
                  ),
                  VocabTooltip(
                    message: hashiru,
                    inlineSpan: const TextSpan(
                      text: '走る',
                    ),
                  ),
                  const TextSpan(
                    text: '」, which is 「',
                  ),
                  VocabTooltip(
                    message: hashiru,
                    inlineSpan: const TextSpan(
                      text: '走り',
                    ),
                  ),
                  const TextSpan(
                    text: '」, you get 「',
                  ),
                  VocabTooltip(
                    message: hashiridasu,
                    inlineSpan: const TextSpan(
                      text: '走り出す',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 meaning “to break out into a run”. Other examples include 「',
                  ),
                  VocabTooltip(
                    message: kirikaeru,
                    inlineSpan: const TextSpan(
                      text: '切り替える',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, which means “to switch over to something else”, and 「',
                  ),
                  VocabTooltip(
                    message: tsukekuwaeru,
                    inlineSpan: const TextSpan(
                      text: '付け加える',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, which means “to add something by attaching it”. You can see how the separate meanings of the two verbs are combined to create the new combined verb. For example, 「',
                  ),
                  VocabTooltip(
                    message: iidasu,
                    inlineSpan: const TextSpan(
                      text: '言い出す',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 means “to start talking”, combining the meaning, “to speak” and “to bring out”. There are no general rules here, you need to just memorize these combined verbs as separate verbs in their own right.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Things that are written in a formal context such as newspaper articles also use the stem as a conjunctive verb. We will come back to this later in the formal expression lesson.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 泳ぐ 【およ・ぐ】 (u-verb) – to swim する (exception) – to do 来る 【く・る】 (exception) – to come 怒る 【おこ・る】 (u-verb) – to get angry 鉄拳 【てっ・けん】 – fist 休み 【やす・み】 – rest; vacation 飲む 【の・む】 (u-verb) – to drink 明日 【あした】 – tomorrow 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see 行く 【い・く】 (u-verb) – to go 友達 【とも・だち】 – friend 遊ぶ 【あそ・ぶ】 (u-verb) – to play 楽しむ 【たの・しむ】 (u-verb) – to enjoy 出す 【だ・す】 (u-verb) – to bring out 走る 【はし・る】 (u-verb) – to run 走り出す 【はし・り・だ・す】 (u-verb) – to break into a run 着る 【き・る】 (ru-verb) – to wear 替える 【か・える】 (ru-verb) – to switch 着替える 【き・が・える】 (ru-verb) – to change (clothes) 付ける 【つ・ける】 (ru-verb) – to attach 加える 【くわ・える】 (ru-verb) – to add 付け加える 【つ・け・くわ・える】 (ru-verb) – to add one thing to another 言う 【い・う】 (u-verb) – to say 言い出す 【い・い・だ・す】 (u-verb) – to start talking In order to conjugate all u-verbs and ru-verbs into their respective polite forms, we will first learn about the stem of verbs. This is often called the masu-stem in Japanese textbooks but we will call it just the stem because it is used in many more conjugations than just its masu-form. The stem is really great because it’s very easy to produce and is useful in many different types of grammar. Rules for extracting the stem of verbs For ru-verbs: Remove the 「る」. Example: 食べい → 食べ For u-verbs: The last vowel sound changes from an / u / vowel sound to an / i / vowel sound. Example: 泳ぐ  → 泳ぎ Exceptions: 「する」 becomes 「し」「くる」 becomes 「き」 The stem when used by itself can be a very specialized and limited way of creating nouns from verbs. While the 「の」 particle allows you to talk about verbs as if they were nouns, the stem actually turns verbs into nouns. In fact, in very rare cases, the stem is used more often than the verb itself. For example, the stem of 「怒る」（いかる） is used more often than the verb itself. The movie, “Fists of Fury” is translated as 「怒りの鉄拳」 and not 「怒る鉄拳」. In fact, 「怒る」 will most likely be read as 「おこる」, a completely different verb with the same meaning and kanji! There are a number of specific nouns (such as 「休み」) that are really verb stems that are used like regular nouns. However, in general we cannot take any verb and make it into a noun. For example, the following sentence is wrong. 飲みをする。(This sentence makes sense but no one talks like this) However, a useful grammar that works in general for stems of all verbs is using the stem as a target with a motion verb (almost always 「行く」 and 「来る」 in this case). This grammar means, “to go or to come to do [some verb]”. Here’s an example. 明日、映画を見に行く。 Tomorrow, go to see movie. 「見に」 is the stem of 「見る」 （which is 見） combined with the target particle 「に」. The motion target particle 「へ」 sounds like you’re literally going or coming to something while the 「に」 particle implies that you are going or coming for the purpose of doing something. 昨日、友達が遊びへきた。 Yesterday, friend came to a playing activity. (Sounds a bit strange) 昨日、友達が遊びにきた。 Yesterday, friend came to play. The expression 「楽しみにする」 meaning “to look forward to” is formed from grammar similar to this but is a special case and should be considered a set expression. Other verbs are also sometimes attached to the stem to create new verbs. For example, when 「出す」 is attached to the stem of 「走る」, which is 「走り」, you get 「走り出す」 meaning “to break out into a run”. Other examples include 「切り替える」, which means “to switch over to something else”, and 「付け加える」, which means “to add something by attaching it”. You can see how the separate meanings of the two verbs are combined to create the new combined verb. For example, 「言い出す」 means “to start talking”, combining the meaning, “to speak” and “to bring out”. There are no general rules here, you need to just memorize these combined verbs as separate verbs in their own right. Things that are written in a formal context such as newspaper articles also use the stem as a conjunctive verb. We will come back to this later in the formal expression lesson.',
      grammars: ['連用形'],
      keywords: ['stem','verb'],
    ),
    Section(
      heading: 'Using 「～ます」 to make verbs polite',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('明日 【あした】 – tomorrow'),
                  Text('大学 【だい・がく】 – college'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('先週 【せん・しゅう】 – last week'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('晩ご飯 【ばん・ご・はん】 – dinner'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('面白い 【おも・しろ・い】(i-adj) – interesting'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Of course, the reason I introduced the verb stem is to learn how to conjugate verbs into their polite form… the masu-form! The masu-form must always come at the end of a complete sentence and never inside a modifying relative clause. When we learn compound sentences, we will see that each sub-sentence of the compound sentence can end in masu-form as well.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'To conjugate verbs into the masu-form, you attach different conjugations of 「ます」 to the stem depending on the tense. Here is a chart.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'A conjugation chart with sample stem 「遊び」'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: '	ます conjugations',
                            ),
                            TableHeading(
                              text: 'Stem+ます',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Plain',
                            ),
                            const TableData(
                              centered: false,
                              content: Text('ます'),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: const TextSpan(
                                            text: '遊び',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ます',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Negative',
                            ),
                            const TableData(
                              centered: false,
                              content: Text('ません'),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: const TextSpan(
                                            text: '遊び',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ません',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            const TableData(
                              centered: false,
                              content: Text('ました'),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: const TextSpan(
                                            text: '遊び',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ました',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past-Neg',
                            ),
                            const TableData(
                              centered: false,
                              content: Text('ませんでした'),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: asobu,
                                          inlineSpan: const TextSpan(
                                            text: '遊び',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ませんでした',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行きます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Tomorrow, go to college.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senshuu,
                              inlineSpan: const TextSpan(
                                text: '先週',
                              ),
                            ),
                            const TextSpan(
                              text: '、ボブに',
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(
                                text: '会いました',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You know, met Bob last week.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bangohan,
                              inlineSpan: const TextSpan(
                                text: '晩ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べませんでした',
                              ),
                            ),
                            const TextSpan(
                              text: 'ね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Didn’t eat dinner, huh?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: omoshiroi,
                              inlineSpan: const TextSpan(
                                text: '面白くない',
                              ),
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見ません',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'About not interesting movies, do not see (them).',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 明日 【あした】 – tomorrow 大学 【だい・がく】 – college 行く 【い・く】 (u-verb) – to go 先週 【せん・しゅう】 – last week 会う 【あ・う】 (u-verb) – to meet 晩ご飯 【ばん・ご・はん】 – dinner 食べる 【た・べる】 (ru-verb) – to eat 面白い 【おも・しろ・い】(i-adj) – interesting 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see Of course, the reason I introduced the verb stem is to learn how to conjugate verbs into their polite form… the masu-form! The masu-form must always come at the end of a complete sentence and never inside a modifying relative clause. When we learn compound sentences, we will see that each sub-sentence of the compound sentence can end in masu-form as well. To conjugate verbs into the masu-form, you attach different conjugations of 「ます」 to the stem depending on the tense. Here is a chart. 明日、大学に行きます。 Tomorrow, go to college. 先週、ボブに会いましたよ。 You know, met Bob last week. 晩ご飯を食べませんでしたね。 Didn’t eat dinner, huh? 面白くない映画は見ません。 About not interesting movies, do not see (them).',
      grammars: ['～ます'],
      keywords: ['masu','polite'],
    ),
    Section(
      heading: 'Using 「です」 for everything else',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('かわいい (i-adj) – cute'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('子犬 【こ・いぬ】 – puppy'),
                  Text('とても – very'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('昨日【きのう】 – yesterday'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('その – that （abbr of 「それの」）'),
                  Text('部屋 【へ・や】 – room'),
                  Text('先週 【せん・しゅう】 – last week'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('映画 【えい・が】 – movie'),
                  Text('面白い 【おも・しろ・い】(i-adj) – interesting'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For any sentence that does not end in a ru-verb or u-verb, the only thing that needs to be done is to add 「です」 or 「でした」. You can also do this for substituted nouns (both 「の」 and 「ん」) by just treating them like regular nouns. Another important thing to remember is that if there is a declarative 「だ」, it must be removed. In being polite, I guess you can’t be so bold as to forwardly declare things the way 「だ」 does. Just like the masu-form, this must also go at the end of a complete sentence. Here is a chart illustrating the conjugations.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'i-adjective (だ cannot be used)'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Casual',
                            ),
                            TableHeading(
                              text: 'Polite',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Plain',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいい',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいい',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Negative',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいくない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいくない',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいかった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいかった',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past-Neg',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいくなかった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいくなかった',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'na-adjective/noun (might have to remove だ)'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Casual',
                            ),
                            TableHeading(
                              text: 'Polite',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Plain',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静か',
                                      ),
                                    ),
                                    const TextSpan(
                                      text: '（だ）',
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静か',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Negative',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静かじゃない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静かじゃない',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静かだった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '※',
                                        ),
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静か',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'でした',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past-Neg',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静かじゃなかった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静かじゃなかった',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'です',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '※ Notice in the case of noun/na-adjective only, the past tense becomes 「でした」. A very common mistake is to do the same for i-adjectives. Remember 「',
                  ),
                  VocabTooltip(
                    message: kawaii,
                    inlineSpan: const TextSpan(
                      text: '静かじゃなかった',
                    ),
                  ),
                  TextSpan(
                    text: 'でした',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                      decoration: TextDecoration.lineThrough,
                    ),
                  ),
                  const TextSpan(
                    text: '」 is wrong!',
                  ),
                ],
              ),
              const Heading(
                text: 'Examples',
                level: 2,
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koinu,
                              inlineSpan: const TextSpan(
                                text: '子犬',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好き',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'About puppies, like very much. (The most natural translation is that someone likes puppies very much but there is not enough context to rule out that the puppies like something very much.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'なかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'んです',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It was that there was no time yesterday.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: amari,
                              inlineSpan: const TextSpan(
                                text: 'あまり',
                              ),
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: TextSpan(
                                text: '静かじゃない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That room is not very quiet.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senshuu,
                              inlineSpan: const TextSpan(
                                text: '先週',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: omoshiroi,
                              inlineSpan: TextSpan(
                                text: '面白かった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Movie saw last week was very interesting.',
                      ),
                    ],
                  ),
                ],
              ),
              const Heading(
                text: '※ Reality Check',
                level: 2,
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'I have heard on a number of occasions that the negative non-past conjugation as given here is not an “officially” correct conjugation. Instead what’s considered to be a more “correct” conjugation is to actually replace the 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text: 'です」 part with 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ありません',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. The reasoning is that the polite negative form of the verb 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is not 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text: 'です」 but 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ありません',
                    ),
                  ),
                  const TextSpan(
                    text: '」. Therefore, 「',
                  ),
                  VocabTooltip(
                    message: kawaii,
                    inlineSpan: const TextSpan(
                      text: 'かわいくない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 actually becomes 「',
                  ),
                  VocabTooltip(
                    message: kawaii,
                    inlineSpan: const TextSpan(
                      text: 'かわいくありません',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: shizuka,
                    inlineSpan: const TextSpan(
                      text: '静かじゃない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 becomes 「',
                  ),
                  VocabTooltip(
                    message: shizuka,
                    inlineSpan: const TextSpan(
                      text: '静かじゃありません',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The reality of today’s Japanese is that what’s supposed to be the “official” conjugation sounds rather stiff and formal. In normal everyday conversations, the conjugation presented here will be used almost every time. While you should use the more formal conjugations for written works using the polite form, you’ll rarely hear it in actual speech. In conclusion, I recommend studying and becoming familiar with ',
                  ),
                  TextSpan(
                    text: 'both',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: ' types of conjugations.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(
                        caption: 'A more formal negative conjugation'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: ' ',
                            ),
                            TableHeading(
                              text: 'Casual',
                            ),
                            TableHeading(
                              text: 'Polite',
                            )
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Negative',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいくない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいく',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ありません',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past-Neg',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: kawaii,
                                      inlineSpan: const TextSpan(
                                        text: 'かわいくなかった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: kawaii,
                                          inlineSpan: const TextSpan(
                                            text: 'かわいく',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ありませんでした',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Negative',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静かじゃない',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静かじゃ',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ありません',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            const TableHeading(
                              text: 'Past-Neg',
                            ),
                            TableData(
                              centered: false,
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: shizuka,
                                      inlineSpan: const TextSpan(
                                        text: '静かじゃなかった',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              centered: false,
                              content: Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        VocabTooltip(
                                          message: shizuka,
                                          inlineSpan: const TextSpan(
                                            text: '静かじゃ',
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'ありませんでした',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: amari,
                              inlineSpan: const TextSpan(
                                text: 'あまり',
                              ),
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静かじゃない',
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You know, that room is not very quiet.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: amari,
                              inlineSpan: const TextSpan(
                                text: 'あまり',
                              ),
                            ),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'じゃありません',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You know, that room is not very quiet.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary かわいい (i-adj) – cute 静か 【しず・か】 (na-adj) – quiet 子犬 【こ・いぬ】 – puppy とても – very 好き 【す・き】 (na-adj) – likable; desirable 昨日【きのう】 – yesterday 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) その – that （abbr of 「それの」） 部屋 【へ・や】 – room 先週 【せん・しゅう】 – last week 見る 【み・る】 (ru-verb) – to see 映画 【えい・が】 – movie 面白い 【おも・しろ・い】(i-adj) – interesting For any sentence that does not end in a ru-verb or u-verb, the only thing that needs to be done is to add 「です」 or 「でした」. You can also do this for substituted nouns (both 「の」 and 「ん」) by just treating them like regular nouns. Another important thing to remember is that if there is a declarative 「だ」, it must be removed. In being polite, I guess you can’t be so bold as to forwardly declare things the way 「だ」 does. Just like the masu-form, this must also go at the end of a complete sentence. Here is a chart illustrating the conjugations. ※ Notice in the case of noun/na-adjective only, the past tense becomes 「でした」. A very common mistake is to do the same for i-adjectives. Remember 「静かじゃなかったでした」 is wrong! Examples 子犬はとても好きです。 About puppies, like very much. (The most natural translation is that someone likes puppies very much but there is not enough context to rule out that the puppies like something very much.) 昨日、時間がなかったんです。 It was that there was no time yesterday. その部屋はあまり静かじゃないです。 That room is not very quiet. 先週に見た映画は、とても面白かったです。 Movie saw last week was very interesting. ※ Reality Check I have heard on a number of occasions that the negative non-past conjugation as given here is not an “officially” correct conjugation. Instead what’s considered to be a more “correct” conjugation is to actually replace the 「ないです」 part with 「ありませんです」 but 「」. The reasoning is that the polite negative form of the verb 「ある」 is not 「ないありません」. Therefore, 「かわいくない」 actually becomes 「かわいくありません」 and 「静かじゃない」 becomes 「静かじゃありません」. The reality of today’s Japanese is that what’s supposed to be the “official” conjugation sounds rather stiff and formal. In normal everyday conversations, the conjugation presented here will be used almost every time. While you should use the more formal conjugations for written works using the polite form, you’ll rarely hear it in actual speech. In conclusion, I recommend studying and becoming familiar with both types of conjugations. その部屋はあまり静かじゃないですよ。 You know, that room is not very quiet. その部屋はあまり静かじゃありませんよ。 You know, that room is not very quiet.',
      grammars: ['です'],
      keywords: ['desu','polite'],
    ),
    Section(
      heading: '「です」 is NOT the same as 「だ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('そう – so'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('はい – yes (polite)'),
                  Text('答える 【こた・える】 (ru-verb) – to answer'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Many of you who have taken Japanese classes have probably been taught that 「です」 is the polite version of 「だ」. However, I want to point some several key differences here and the reasons why they are in fact completely different things. It is impossible to fully explain the reasons why they are fundamentally different without discussing grammar that have yet to be covered so I would like to target this toward those who have already started learning Japanese and have been incorrectly misinformed that 「だ」 is the casual version of 「です」. For the rest of you new to this, you can easily skip this part.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'I’m sure most of you have learned the expression 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 by now. Now, there are four ways to make a complete sentence using the state-of-being with 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to produce a sentence that says, “That is so.”',
                  ),
                ],
              ),
              const Heading(
                text: 'Different ways to say, “That is so.”',
                level: 2,
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'だ。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'です。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'でございます。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The first 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is the implied state-of-being and 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だ」 is the declarative. As I’ve stated before, the non-assuming soft spoken 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is often used by females while the more confident 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: 'だ」 is often used by males.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: 'です」 is the polite version of 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: '」, created by attaching 「です」 to the noun. 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: 'です」 is ',
                  ),
                  const TextSpan(
                    text: 'not',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const TextSpan(
                    text: ' the polite version of 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だ」 where the 「だ」 is replaced by 「です」 and I’ll explain why.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Perhaps we wanted to make that sentence into a question instead to ask, “Is that so?” There are several ways to do this but some possibilities are given in the following. (This grammar is covered in a later section.)',
                  ),
                ],
              ),
              const Heading(
                text: 'Different ways to ask, “Is that so?”',
                level: 2,
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'か？',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'ですか？',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'As I’ve explained before, the 「だ」 is used to declare what one believes to be a fact. Therefore, 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だか？」 is not a valid way to ask a question because it is declaring a fact and asking a question at the same time. But the fact that 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'ですか」 is a valid question shows that 「です」 and 「だ」 are essentially different. 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'です」, in showing respect and humbleness, is not as assertive and is merely the polite version of 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Besides the difference in nuance between 「だ」 and 「です」, another key difference is that 「だ」 is used in many different types of grammar to delineate a relative clause. 「です」, on the other hand, is only used at the end of a sentence to designate a polite state-of-being. For instance, consider the two following sentences. (This grammar is covered in a later section.)',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sou,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思います',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I think that is so.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sou,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思います',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(Incorrect sentence)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  TextSpan(
                    text: 'だ',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思います',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is valid while 「',
                  ),
                  VocabTooltip(
                    message: sou,
                    inlineSpan: const TextSpan(
                      text: 'そう',
                    ),
                  ),
                  TextSpan(
                    text: 'です',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思います',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is not because 「です」 can only go at the end of the sentence. 「です」 can only be in a relative clause when it is a direct quote of what someone said such as the following.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: '「はい、',
                        ),
                        VocabTooltip(
                          message: sou,
                          inlineSpan: const TextSpan(
                            text: 'そう',
                          ),
                        ),
                        const TextSpan(
                          text: 'です」と',
                        ),
                        VocabTooltip(
                          message: kotaeru,
                          inlineSpan: const TextSpan(
                            text: '答えた',
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In conclusion, replacing 「です」 with 「だ」, thinking one is the polite equivalent of the other or vice-versa will potentially result in grammatically incorrect sentences. It is best to think of them as totally separate things (because they are).',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary そう – so 思う 【おも・う】 (u-verb) – to think はい – yes (polite) 答える 【こた・える】 (ru-verb) – to answer Many of you who have taken Japanese classes have probably been taught that 「です」 is the polite version of 「だ」. However, I want to point some several key differences here and the reasons why they are in fact completely different things. It is impossible to fully explain the reasons why they are fundamentally different without discussing grammar that have yet to be covered so I would like to target this toward those who have already started learning Japanese and have been incorrectly misinformed that 「だ」 is the casual version of 「です」. For the rest of you new to this, you can easily skip this part. I’m sure most of you have learned the expression 「そう」 by now. Now, there are four ways to make a complete sentence using the state-of-being with 「そう」 to produce a sentence that says, “That is so.” Different ways to say, “That is so.” そう。そうだ。そうです。そうでございます。 The first 「そう」 is the implied state-of-being and 「そうだ」 is the declarative. As I’ve stated before, the non-assuming soft spoken 「そう」 is often used by females while the more confident 「そうだ」 is often used by males. 「そうです」 is the polite version of 「そう」, created by attaching 「です」 to the noun. 「そうです」 is not the polite version of 「そうだ」 where the 「だ」 is replaced by 「です」 and I’ll explain why. Perhaps we wanted to make that sentence into a question instead to ask, “Is that so?” There are several ways to do this but some possibilities are given in the following. (This grammar is covered in a later section.) Different ways to ask, “Is that so?” そう？ そうか？ そうですか？ As I’ve explained before, the 「だ」 is used to declare what one believes to be a fact. Therefore, 「そうだか？」 is not a valid way to ask a question because it is declaring a fact and asking a question at the same time. But the fact that 「そうですか」 is a valid question shows that 「です」 and 「だ」 are essentially different. 「そうです」, in showing respect and humbleness, is not as assertive and is merely the polite version of 「そう」. Besides the difference in nuance between 「だ」 and 「です」, another key difference is that 「だ」 is used in many different types of grammar to delineate a relative clause. 「です」, on the other hand, is only used at the end of a sentence to designate a polite state-of-being. For instance, consider the two following sentences. (This grammar is covered in a later section.) そうだと思います。 I think that is so. そうですと思います。(Incorrect sentence) 「そうだと思います」 is valid while 「そうですと思います」 is not because 「です」 can only go at the end of the sentence. 「です」 can only be in a relative clause when it is a direct quote of what someone said such as the following. 「はい、そうです」と答えた。 In conclusion, replacing 「です」 with 「だ」, thinking one is the polite equivalent of the other or vice-versa will potentially result in grammatically incorrect sentences. It is best to think of them as totally separate things (because they are).',
      grammars: [],
      keywords: ['declarative','da','desu','polite','casual'],
    ),
  ],
);
