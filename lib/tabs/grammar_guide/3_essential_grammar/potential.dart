import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson potential = Lesson(
  title: 'Potential Form',
  sections: [
    Section(
      heading: 'Expressing the ability to do something',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In Japanese, the ability to do a certain action is expressed by conjugating the verb rather than adding a word such as the words “can” or “able to” in the case of English. All verbs conjugated into the potential form become a ru-verb.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In Japanese, the ability to do a certain action is expressed by conjugating the verb rather than adding a word such as the words “can” or “able to” in the case of English. All verbs conjugated into the potential form become a ru-verb.',
      grammars: [],
      keywords: ['potential','can','able to'],
    ),
    Section(
      heading: 'The potential form',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang'),
                  Text('調べる 【しら・べる】 (ru-verb) – to investigate'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('取る 【と・る】 (u-verb) – to take'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('残念 【ざん・ねん】 (na-adj) – unfortunate'),
                  Text('今週末 【こん・しゅう・まつ】 – this weekend'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('もう – already'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Once again, the conjugation rules can be split into three major groups: ru-verbs, u-verbs, and exception verbs. However, the potential form of the verb 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 (meaning “to do”) is a special exception because it becomes a completely different verb: 「',
                  ),
                  VocabTooltip(
                    message: dekiru,
                    inlineSpan: const TextSpan(
                      text: 'できる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 （',
                  ),
                  VocabTooltip(
                    message: dekiru,
                    inlineSpan: const TextSpan(
                      text: '出来る',
                    ),
                  ),
                  const TextSpan(
                    text: '）',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace the 「る」 with 「られる」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'られる',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the last character from a / u / vowel sound to the equivalent / e / vowel sound and add 「る」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '遊',
                                        ),
                                        TextSpan(
                                          text: 'ぶ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '遊',
                                        ),
                                        TextSpan(
                                          text: 'べ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: asobu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '遊べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'する',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: dekiru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'できる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'くる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'こられる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const Text(
                            '※Remember that all potential verbs become ru-verbs.'),
                      ],
                    ),
                  ],
                ),
                heading: 'Rules for creating potential form',
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample ru-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Potential'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Potential'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'す',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'せる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kaku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '書',
                                      ),
                                      TextSpan(
                                        text: 'く',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kaku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '書',
                                      ),
                                      TextSpan(
                                        text: 'ける',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぶ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'べる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'つ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'てる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'む',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'める',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: toru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '取',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: toru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '取',
                                      ),
                                      TextSpan(
                                        text: 'れる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ぬ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ねる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'う',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'える',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exception Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Potential'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: dekiru,
                                  inlineSpan: const TextSpan(
                                    text: 'できる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'こられる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'It is also possible to just add 「れる」 instead of the full 「られる」 for ru-verbs. For example, 「'),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べる',
                    ),
                  ),
                  const TextSpan(text: '」 becomes 「'),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べれる',
                    ),
                  ),
                  const TextSpan(text: '」 instead of 「'),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べられる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. I suggest learning the official 「られる」 conjugation first because laziness can be a hard habit to break and the shorter version, though common, is considered to be slang.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書けます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can you write kanji?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zannen,
                              inlineSpan: const TextSpan(
                                text: '残念',
                              ),
                            ),
                            const TextSpan(
                              text: 'だが、',
                            ),
                            VocabTooltip(
                              message: konshuumatsu,
                              inlineSpan: const TextSpan(
                                text: '今週末',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行けない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s unfortunate, but can’t go this weekend.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: shinjiru,
                              inlineSpan: TextSpan(
                                text: '信じられない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I can’t believe it already.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見る 【み・る】 (ru-verb) – to see 遊ぶ 【あそ・ぶ】 (u-verb) – to play する (exception) – to do 来る 【く・る】 (exception) – to come 出来る 【で・き・る】 (ru-verb) – to be able to do 食べる 【た・べる】 (ru-verb) – to eat 着る 【き・る】 (ru-verb) – to wear 信じる 【しん・じる】 (ru-verb) – to believe 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 出る 【で・る】 (ru-verb) – to come out 掛ける 【か・ける】 (ru-verb) – to hang 調べる 【しら・べる】 (ru-verb) – to investigate 話す 【はな・す】 (u-verb) – to speak 書く 【か・く】 (u-verb) – to write 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 取る 【と・る】 (u-verb) – to take 死ぬ 【し・ぬ】 (u-verb) – to die 買う 【か・う】 (u-verb) – to buy 漢字 【かん・じ】 – Kanji 残念 【ざん・ねん】 (na-adj) – unfortunate 今週末 【こん・しゅう・まつ】 – this weekend 行く 【い・く】 (u-verb) – to go もう – already Once again, the conjugation rules can be split into three major groups: ru-verbs, u-verbs, and exception verbs. However, the potential form of the verb 「する」 (meaning “to do”) is a special exception because it becomes a completely different verb: 「できる」 （出来る） Rules for creating potential form For ru-verbs: Replace the 「る」 with 「られる」. Example: 見る → 見られる For u-verbs: Change the last character from a / u / vowel sound to the equivalent / e / vowel sound and add 「る」. Example: 遊ぶ → 遊べ → 遊べる Exceptions: 「する」 becomes 「できる」「くる」 becomes 「こられる」※Remember that all potential verbs become ru-verbs. It is also possible to just add 「れる」 instead of the full 「られる」 for ru-verbs. For example, 「食べる」 becomes 「食べれる」 instead of 「食べられる」. I suggest learning the official 「られる」 conjugation first because laziness can be a hard habit to break and the shorter version, though common, is considered to be slang. Examples 漢字は書けますか？ Can you write kanji? 残念だが、今週末は行けない。 It’s unfortunate, but can’t go this weekend. もう信じられない。 I can’t believe it already.',
      grammars: ['～える'],
      keywords: ['potential','verb'],
    ),
    Section(
      heading: 'Potential forms do not have direct objects',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('富士山 【ふ・じ・さん】 – Mt. Fuji'),
                  Text('登る 【のぼ・る】 (u-verb) – to climb'),
                  Text('重い 【おも・い】 (i-adj) – heavy'),
                  Text('荷物 【に・もつ】 – baggage'),
                  Text('持つ 【も・つ】 (u-verb) – to hold'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The potential form indicates that something is possible but no actual action is actually taken. While the potential form is still a verb, because it is describing the state of feasibility, in general, you don’t want to use the direct object 「を」 as you would with the non-potential form of the verb. For example the following sentences sound unnatural.'),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: fujisan,
                          inlineSpan: const TextSpan(
                            text: '富士山',
                          ),
                        ),
                        TextSpan(
                          text: 'を',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                        VocabTooltip(
                          message: noboru,
                          inlineSpan: const TextSpan(
                            text: '登れた',
                          ),
                        ),
                        const TextSpan(text: '。'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: omoi,
                          inlineSpan: const TextSpan(
                            text: '重い',
                          ),
                        ),
                        VocabTooltip(
                          message: nimotsu,
                          inlineSpan: const TextSpan(
                            text: '荷物',
                          ),
                        ),
                        TextSpan(
                          text: 'を',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                        VocabTooltip(
                          message: motsu,
                          inlineSpan: const TextSpan(
                            text: '持てます',
                          ),
                        ),
                        const TextSpan(text: '。'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Here are the versions using either 「が」 or 「は」 instead:'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: fujisan,
                              inlineSpan: const TextSpan(
                                text: '富士山',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: noboru,
                              inlineSpan: const TextSpan(
                                text: '登れた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Was able to climb Fuji-san.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: omoi,
                              inlineSpan: const TextSpan(
                                text: '重い',
                              ),
                            ),
                            VocabTooltip(
                              message: nimotsu,
                              inlineSpan: const TextSpan(
                                text: '荷物',
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: motsu,
                              inlineSpan: const TextSpan(
                                text: '持てます',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Am able to hold heavy baggage.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 富士山 【ふ・じ・さん】 – Mt. Fuji 登る 【のぼ・る】 (u-verb) – to climb 重い 【おも・い】 (i-adj) – heavy 荷物 【に・もつ】 – baggage 持つ 【も・つ】 (u-verb) – to hold The potential form indicates that something is possible but no actual action is actually taken. While the potential form is still a verb, because it is describing the state of feasibility, in general, you don’t want to use the direct object 「を」 as you would with the non-potential form of the verb. For example the following sentences sound unnatural. 富士山を登れた。重い荷物を持てます。 Here are the versions using either 「が」 or 「は」 instead: 富士山が登れた。 Was able to climb Fuji-san. 重い荷物が持てます。 Am able to hold heavy baggage.',
      grammars: [],
      keywords: ['direct object','potential','verb'],
    ),
    Section(
      heading: 'Are 「見える」 and 「聞こえる」 exceptions?',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('見える 【み・える】 (ru-verb) – to be visible'),
                  Text('聞こえる 【き・こえる】 (ru-verb) – to be audible'),
                  Text('今日 【きょう】 – today'),
                  Text('晴れる 【は・れる】 (ru-verb) – to be sunny'),
                  Text('富士山 【ふ・じ・さん】 – Mt. Fuji'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('おかげ – thanks to'),
                  Text('映画 【えい・が】 – movie'),
                  Text('ただ – free of charge; only'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('こと – event, matter'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('久しぶり 【ひさ・しぶり】 – after a long time'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('声 【こえ】 – voice'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('周り 【まわ・り】 – surroundings'),
                  Text('うるさい (i-adj) – noisy'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('あまり／あんまり – not very (when used with negative)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'There are two verbs 「'),
                  VocabTooltip(
                    message: mieru,
                    inlineSpan: const TextSpan(
                      text: '見える',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: kikoeru,
                    inlineSpan: const TextSpan(
                      text: '聞こえる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 that mean that something is visible and audible, respectively. When you want to say that you can see or hear something, you’ll want to use these verbs. If however, you wanted to say that you were given the opportunity to see or hear something, you would use the regular potential form. However, in this case, it is more common to use the type of expression as seen in example 3.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: hareru,
                              inlineSpan: const TextSpan(
                                text: '晴れて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: fujisan,
                              inlineSpan: const TextSpan(
                                text: '富士山',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hareru,
                              inlineSpan: const TextSpan(
                                text: '見える',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          '(It) cleared up today and Fuji-san is visible.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: okage,
                              inlineSpan: const TextSpan(
                                text: 'おかげ',
                              ),
                            ),
                            const TextSpan(
                              text: 'で、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: tada,
                              inlineSpan: const TextSpan(
                                text: 'ただ',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見られた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Thanks to (my) friend, (I) was able to watch the movie for free.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: okage,
                              inlineSpan: const TextSpan(
                                text: 'おかげ',
                              ),
                            ),
                            const TextSpan(
                              text: 'で、',
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: tada,
                              inlineSpan: const TextSpan(
                                text: 'ただ',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見る',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Thanks to (my) friend, (I) was able to watch the movie for free.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You can see that example 3 uses the generic noun for an event to say literally, “The event of seeing movie was able to be done.” which essentially means the same thing as 「'),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(
                      text: '見られる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. You can also just use generic noun substitution to substitute for 「'),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: tomodachi,
                          inlineSpan: const TextSpan(
                            text: '友達',
                          ),
                        ),
                        const TextSpan(text: 'の'),
                        VocabTooltip(
                          message: okage,
                          inlineSpan: const TextSpan(
                            text: 'おかげ',
                          ),
                        ),
                        const TextSpan(
                          text: 'で、',
                        ),
                        VocabTooltip(
                          message: eiga,
                          inlineSpan: const TextSpan(
                            text: '映画',
                          ),
                        ),
                        const TextSpan(text: 'を'),
                        VocabTooltip(
                          message: tada,
                          inlineSpan: const TextSpan(
                            text: 'ただ',
                          ),
                        ),
                        const TextSpan(text: 'で'),
                        VocabTooltip(
                          message: miru,
                          inlineSpan: const TextSpan(
                            text: '見る',
                          ),
                        ),
                        TextSpan(
                          text: 'の',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(text: 'が'),
                        VocabTooltip(
                          message: dekiru,
                          inlineSpan: const TextSpan(
                            text: 'できた',
                          ),
                        ),
                        const TextSpan(text: '。'),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'Here’s some more examples using 「'),
                  VocabTooltip(
                    message: kiku,
                    inlineSpan: const TextSpan(
                      text: '聞く',
                    ),
                  ),
                  const TextSpan(
                      text: '」, can you tell the difference? Notice that 「'),
                  VocabTooltip(
                    message: kikoeru,
                    inlineSpan: const TextSpan(
                      text: '聞こえる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 always means “audible” and never “able to ask”.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hisashiburi,
                              inlineSpan: const TextSpan(
                                text: '久しぶり',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: koe,
                              inlineSpan: const TextSpan(
                                text: '声',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: TextSpan(
                                text: '聞けた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'I was able to hear his voice for the first time in a long time.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mawari,
                              inlineSpan: const TextSpan(
                                text: '周り',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: urusai,
                              inlineSpan: const TextSpan(
                                text: 'うるさくて',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言っている',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: anmari,
                              inlineSpan: const TextSpan(
                                text: 'あんまり',
                              ),
                            ),
                            VocabTooltip(
                              message: kikoeru,
                              inlineSpan: TextSpan(
                                text: '聞こえなかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The surroundings were noisy and I couldn’t hear what he was saying very well.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見える 【み・える】 (ru-verb) – to be visible 聞こえる 【き・こえる】 (ru-verb) – to be audible 今日 【きょう】 – today 晴れる 【は・れる】 (ru-verb) – to be sunny 富士山 【ふ・じ・さん】 – Mt. Fuji 友達 【とも・だち】 – friend おかげ – thanks to 映画 【えい・が】 – movie ただ – free of charge; only 見る 【み・る】 (ru-verb) – to see こと – event, matter 出来る 【で・き・る】 (ru-verb) – to be able to do 久しぶり 【ひさ・しぶり】 – after a long time 彼 【かれ】 – he; boyfriend 声 【こえ】 – voice 聞く 【き・く】 (u-verb) – to ask; to listen 周り 【まわ・り】 – surroundings うるさい (i-adj) – noisy 言う 【い・う】 (u-verb) – to say あまり／あんまり – not very (when used with negative) There are two verbs 「見える」 and 「聞こえる」 that mean that something is visible and audible, respectively. When you want to say that you can see or hear something, you’ll want to use these verbs. If however, you wanted to say that you were given the opportunity to see or hear something, you would use the regular potential form. However, in this case, it is more common to use the type of expression as seen in example 3. 今日は晴れて、富士山が見える。(It) cleared up today and Fuji-san is visible. 友達のおかげで、映画はただで見られた。 Thanks to (my) friend, (I) was able to watch the movie for free. 友達のおかげで、映画はただで見ることができた。 Thanks to (my) friend, (I) was able to watch the movie for free. You can see that example 3 uses the generic noun for an event to say literally, “The event of seeing movie was able to be done.” which essentially means the same thing as 「見られる」. You can also just use generic noun substitution to substitute for 「こと」. 友達のおかげで、映画をただで見るのができた。 Here’s some more examples using 「聞く」, can you tell the difference? Notice that 「聞こえる」 always means “audible” and never “able to ask”. 久しぶりに彼の声が聞けた。 I was able to hear his voice for the first time in a long time. 周りがうるさくて、彼が言っていることがあんまり聞こえなかった。 The surroundings were noisy and I couldn’t hear what he was saying very well.',
      grammars: ['見える','聞こえる'],
      keywords: ['mieru','kikoeru','audible','visible','potential','verb'],
    ),
    Section(
      heading: '「ある」, yet another exception',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('そんな – that sort of'),
                  Text('こと – event, matter'),
                  Text('有り得る 【あ・り・え・る／あ・り・う・る】 (ru-verb) – to possibly exist'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('寝坊【ね・ぼう】 – oversleep'),
                  Text('する (exception) – to do'),
                  Text('それ – that'),
                  Text('話 【はなし】 – story'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'You can say that something has a possibility of existing by combining 「'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(text: '」 and the verb 「'),
                  VocabTooltip(
                    message: eru,
                    inlineSpan: const TextSpan(
                      text: '得る',
                    ),
                  ),
                  const TextSpan(text: '」 to produce 「'),
                  VocabTooltip(
                    message: arieruAriuru,
                    inlineSpan: const TextSpan(
                      text: 'あり得る',
                    ),
                  ),
                  const TextSpan(text: '」. This essentially means 「'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(text: 'が'),
                  VocabTooltip(
                    message: dekiru,
                    inlineSpan: const TextSpan(
                      text: 'できる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 except that nobody actually says that, they just use 「'),
                  VocabTooltip(
                    message: arieruAriuru,
                    inlineSpan: const TextSpan(
                      text: 'あり得る',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. This verb is very curious in that it can be read as either 「'),
                  VocabTooltip(
                    message: ariuru,
                    inlineSpan: const TextSpan(
                      text: 'ありうる',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: arieru,
                    inlineSpan: const TextSpan(
                      text: 'ありえる',
                    ),
                  ),
                  const TextSpan(text: '」, '),
                  const TextSpan(
                    text: 'however',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                      text: '; all the other conjugations such as 「'),
                  VocabTooltip(
                    message: arieru,
                    inlineSpan: const TextSpan(
                      text: 'ありえない',
                    ),
                  ),
                  const TextSpan(text: '」、「'),
                  VocabTooltip(
                    message: arieru,
                    inlineSpan: const TextSpan(
                      text: 'ありえた',
                    ),
                  ),
                  const TextSpan(text: '」、and 「'),
                  VocabTooltip(
                    message: arieru,
                    inlineSpan: const TextSpan(
                      text: 'ありえなかった',
                    ),
                  ),
                  const TextSpan(
                      text: '」 only have one possible reading using 「え」.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: ariuru,
                              inlineSpan: TextSpan(
                                text: 'ありうる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That kind of situation/event is possible (lit: can exist).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: arieru,
                              inlineSpan: TextSpan(
                                text: 'ありえる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That kind of situation/event is possible (lit: can exist).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: arieru,
                              inlineSpan: TextSpan(
                                text: 'ありえない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That kind of situation/event is not possible (lit: cannot exist).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: nebou,
                              inlineSpan: const TextSpan(
                                text: '寝坊',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: ariuru,
                              inlineSpan: TextSpan(
                                text: 'ありうる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s also possible that he overslept. (lit: The event that he overslept also possibly exists.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: arieru,
                              inlineSpan: TextSpan(
                                text: 'ありえない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: hanashi,
                              inlineSpan: const TextSpan(
                                text: '話',
                              ),
                            ),
                            const TextSpan(
                              text: 'だよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'That’s an impossible story/scenario. (lit: That story/scenario cannot exist.)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary そんな – that sort of こと – event, matter 有り得る 【あ・り・え・る／あ・り・う・る】 (ru-verb) – to possibly exist 彼 【かれ】 – he; boyfriend 寝坊【ね・ぼう】 – oversleep する (exception) – to do それ – that 話 【はなし】 – story You can say that something has a possibility of existing by combining 「ある」 and the verb 「得る」 to produce 「あり得る」. This essentially means 「あることができる」 except that nobody actually says that, they just use 「あり得る」. This verb is very curious in that it can be read as either 「ありうる」 or 「ありえる」, however; all the other conjugations such as 「ありえない」、「ありえた」、and 「ありえなかった」 only have one possible reading using 「え」. そんなことはありうる。 That kind of situation/event is possible (lit: can exist). そんなことはありえる。 That kind of situation/event is possible (lit: can exist). そんなことはありえない。 That kind of situation/event is not possible (lit: cannot exist). 彼が寝坊したこともありうるね。 It’s also possible that he overslept. (lit: The event that he overslept also possibly exists.) それは、ありえない話だよ。 That’s an impossible story/scenario. (lit: That story/scenario cannot exist.)',
      grammars: ['あり得る'],
      keywords: ['arieru','ariuru','arienai'],
    ),
  ],
);
