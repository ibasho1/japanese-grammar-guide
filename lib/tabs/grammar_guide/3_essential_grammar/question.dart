import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson question = Lesson(
  title: 'The Question Marker',
  sections: [
    Section(
      heading: 'Questions in polite form',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('田中 【た・なか】 – Tanaka (last name)'),
                    Text('お母さん【お・かあ・さん】 – mother (polite)'),
                    Text('どこ – where'),
                    Text('鈴木 【すず・き】 – Suzuki (last name)'),
                    Text('母 【はは】 – mother'),
                    Text('買い物 【か・い・もの】 – shopping'),
                    Text('行く 【い・く】 (u-verb) – to go'),
                    Text('イタリア – Italy'),
                    Text('料理 【りょう・り】 – cooking; cuisine; dish'),
                    Text('食べる 【た・べる】 (ru-verb) – to eat'),
                    Text('ちょっと – a little'),
                    Text('お腹 【お・なか】 – stomach'),
                    Text('いっぱい – full'),
                    Text('ごめんなさい – sorry (polite)'),
                    Text('ごめん – sorry'),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'The question marker is covered here because it is primarily used to clearly indicate a question in polite sentences. While it is entirely possible to express a question even in polite form using just intonation, the question marker is often attached to the very end of the sentence to indicate a question. The question marker is simply the hiragana character 「か」 and you don’t need to add a question mark. For previously explained reasons, you must not use the declarative 「だ」 with the question marker.'),
                  ],
                ),
                const Heading(text: 'Example 1', level: 2),
                Dialogue(
                  items: [
                    DialogueLine(
                      speaker: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tanaka,
                              inlineSpan: const TextSpan(text: '田中'),
                            ),
                            const TextSpan(
                              text: 'さん',
                            ),
                          ],
                        ),
                      ),
                      text: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okaasan,
                              inlineSpan: const TextSpan(text: 'お母さん'),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(text: 'どこ'),
                            ),
                            const TextSpan(
                              text: 'です',
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ),
                    const DialogueLine(
                      speaker: Text('Tanaka-san'),
                      text: Text('Where is (your) mother?'),
                      english: true,
                    ),
                  ],
                ),
                Dialogue(
                  items: [
                    DialogueLine(
                      speaker: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suzuki,
                              inlineSpan: const TextSpan(text: '鈴木'),
                            ),
                            const TextSpan(
                              text: 'さん',
                            ),
                          ],
                        ),
                      ),
                      text: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: haha,
                              inlineSpan: const TextSpan(text: '母'),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kaimono,
                              inlineSpan: const TextSpan(text: '買い物'),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(text: '行きました'),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ),
                    const DialogueLine(
                      speaker: Text('Suzuki-san'),
                      text: Text('(My) mother went shopping.'),
                      english: true,
                    ),
                  ],
                ),
                const Heading(text: 'Example 2', level: 2),
                Dialogue(
                  items: [
                    DialogueLine(
                      speaker: const Text('キムさん'),
                      text: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: itaria,
                              inlineSpan: const TextSpan(text: 'イタリア'),
                            ),
                            VocabTooltip(
                              message: ryouri,
                              inlineSpan: const TextSpan(text: '料理'),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(text: '食べ'),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(text: '行きません'),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ),
                    const DialogueLine(
                      speaker: Text('Kim-san'),
                      text: Text('Go to eat Italian food?'),
                      english: true,
                    ),
                  ],
                ),
                Dialogue(
                  items: [
                    DialogueLine(
                      speaker: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suzuki,
                              inlineSpan: const TextSpan(text: '鈴木'),
                            ),
                            const TextSpan(
                              text: 'さん',
                            ),
                          ],
                        ),
                      ),
                      text: Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sumimasen,
                              inlineSpan: const TextSpan(text: 'すみません'),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(text: 'ちょっと'),
                            ),
                            VocabTooltip(
                              message: onaka,
                              inlineSpan: const TextSpan(text: 'お腹'),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: ippai,
                              inlineSpan: const TextSpan(text: 'いっぱい'),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                    ),
                    const DialogueLine(
                      speaker: Text('Suzuki-san'),
                      text: Text('Sorry. (My) stomach is a little full.'),
                      english: true,
                    ),
                  ],
                ),
                Paragraph(
                  texts: [
                    const TextSpan(
                      text:
                          'Here the question is actually being used as an invitation just like how in English we say, “Won’t you come in for a drink?” 「',
                    ),
                    VocabTooltip(
                      message: sumimasen,
                      inlineSpan: const TextSpan(text: 'すみません'),
                    ),
                    const TextSpan(
                      text:
                          '」 is a polite way of apologizing. Slightly less formal is 「',
                    ),
                    VocabTooltip(
                      message: gomennasai,
                      inlineSpan: const TextSpan(text: 'ごめんなさい'),
                    ),
                    const TextSpan(
                      text: '」 while the casual version is simply 「',
                    ),
                    VocabTooltip(
                      message: gomen,
                      inlineSpan: const TextSpan(text: 'ごめん'),
                    ),
                    const TextSpan(
                      text: '」.',
                    ),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary 田中 【た・なか】 – Tanaka (last name) お母さん【お・かあ・さん】 – mother (polite) どこ – where 鈴木 【すず・き】 – Suzuki (last name) 母 【はは】 – mother 買い物 【か・い・もの】 – shopping 行く 【い・く】 (u-verb) – to go イタリア – Italy 料理 【りょう・り】 – cooking; cuisine; dish 食べる 【た・べる】 (ru-verb) – to eat ちょっと – a little お腹 【お・なか】 – stomach いっぱい – full ごめんなさい – sorry (polite) ごめん – sorry The question marker is covered here because it is primarily used to clearly indicate a question in polite sentences. While it is entirely possible to express a question even in polite form using just intonation, the question marker is often attached to the very end of the sentence to indicate a question. The question marker is simply the hiragana character 「か」 and you don’t need to add a question mark. For previously explained reasons, you must not use the declarative 「だ」 with the question marker. Example 1 田中さん：お母さんはどこですか。 Tanaka-san: Where is (your) mother? 鈴木さん：母は買い物に行きました。 Suzuki-san: (My) mother went shopping. Example 2 キムさん：イタリア料理を食べに行きませんか。 Kim-san: Go to eat Italian food? 鈴木さん：すみません。ちょっとお腹がいっぱいです。 Suzuki-san: Sorry. (My) stomach is a little full. Here the question is actually being used as an invitation just like how in English we say, “Won’t you come in for a drink?” 「すみません」 is a polite way of apologizing. Slightly less formal is 「ごめんなさい」 while the casual version is simply 「ごめん」.',
      grammars: ['か'],
      keywords: ['ka','particle','question','question particle','sorry','apologize','apology','gomen','gomennasai','sumimasen','polite'],
    ),
Section(
      heading: 'The question marker in casual speech',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('こんな – this sort of'),
                    Text('本当 【ほん・とう】 – real'),
                    Text('食べる 【た・べる】 (ru-verb) – to eat'),
                    Text('そんな – that sort of'),
                    Text('ある (u-verb) – to exist (inanimate)'),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'It makes sense to conclude that the question marker would work in exactly the same way in casual speech as it does in polite speech. However, this is not the case. The question marker 「か」 is usually not used with casual speech to make actual questions. It is often used to consider whether something is true or not. Depending on the context and intonation, it can also be used to make rhetorical questions or to express sarcasm. It can sound quite rough so you might want to be careful about using 「か」 for questions in the plain casual form.'),
                  ],
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: konna,
                                inlineSpan: const TextSpan(
                                  text: 'こんな',
                                ),
                              ),
                              const TextSpan(
                                text: 'のを',
                              ),
                              VocabTooltip(
                                message: hontou,
                                inlineSpan: const TextSpan(
                                  text: '本当',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: const TextSpan(
                                  text: '食べる',
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              const TextSpan(
                                text: '？',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Do you think [he/she] will really eat this type of thing?',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: sonna,
                                inlineSpan: const TextSpan(
                                  text: 'そんな',
                                ),
                              ),
                              const TextSpan(
                                text: 'のは、',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              const TextSpan(
                                text: 'よ！',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Do I look like I would have something like that?!',
                        ),
                      ],
                    ),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'Instead of 「か」, real questions in casual speech are usually asked with the explanatory の particle or nothing at all except for a rise in intonation, as we have already seen in previous sections.'),
                  ],
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: konna,
                                inlineSpan: const TextSpan(
                                  text: 'こんな',
                                ),
                              ),
                              const TextSpan(
                                text: 'のを',
                              ),
                              VocabTooltip(
                                message: hontou,
                                inlineSpan: const TextSpan(
                                  text: '本当',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: const TextSpan(
                                  text: '食べる',
                                ),
                              ),
                              const TextSpan(
                                text: '？',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Are you really going to eat something like this?',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: sonna,
                                inlineSpan: const TextSpan(
                                  text: 'そんな',
                                ),
                              ),
                              const TextSpan(
                                text: 'のは、',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              TextSpan(
                                text: 'の',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              const TextSpan(
                                text: '？',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Do you have something like that?',
                        ),
                      ],
                    ),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary こんな – this sort of 本当 【ほん・とう】 – real 食べる 【た・べる】 (ru-verb) – to eat そんな – that sort of ある (u-verb) – to exist (inanimate) It makes sense to conclude that the question marker would work in exactly the same way in casual speech as it does in polite speech. However, this is not the case. The question marker 「か」 is usually not used with casual speech to make actual questions. It is often used to consider whether something is true or not. Depending on the context and intonation, it can also be used to make rhetorical questions or to express sarcasm. It can sound quite rough so you might want to be careful about using 「か」 for questions in the plain casual form. こんなのを本当に食べるか？ Do you think [he/she] will really eat this type of thing? そんなのは、あるかよ！ Do I look like I would have something like that?! Instead of 「か」, real questions in casual speech are usually asked with the explanatory の particle or nothing at all except for a rise in intonation, as we have already seen in previous sections. こんなのを本当に食べる？ Are you really going to eat something like this? そんなのは、あるの？ Do you have something like that?',
      grammars: [],
      keywords: ['casual','ka','particle','question particle','question'],
    ),
Section(
      heading: '「か」 used in relative clauses',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('昨日【きのう】 – yesterday'),
                    Text('何【なに】 – what'),
                    Text('食べる 【た・べる】 (ru-verb) – to eat'),
                    Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                    Text('彼 【かれ】 – he; boyfriend'),
                    Text('言う 【い・う】 (u-verb) – to say'),
                    Text('分かる 【わ・かる】 (u-verb) – to understand'),
                    Text('先生 【せん・せい】 – teacher'),
                    Text('学校 【がっ・こう】 – school'),
                    Text('行く 【い・く】 (u-verb) – to go'),
                    Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                    Text('どう – how'),
                    Text('知る 【し・る】 (u-verb) – to know'),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'Another use of the question marker is simply grammatical and has nothing to do with the politeness. A question marker attached to the end of a relative clause makes a mini-question inside a larger sentence. This allows the speaker to talk about the question. For example, you can talk about the question, “What did I eat today?” In the following examples, the question that is being considered is in red.'),
                  ],
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kinou,
                                inlineSpan: TextSpan(
                                  text: '昨日',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: nani,
                                inlineSpan: TextSpan(
                                  text: '何',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'を',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: TextSpan(
                                  text: '食べた',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: wasureru,
                                inlineSpan: const TextSpan(
                                  text: '忘れた',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Forgot what I ate yesterday.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kare,
                                inlineSpan: const TextSpan(
                                  text: '彼',
                                ),
                              ),
                              const TextSpan(
                                text: 'は',
                              ),
                              VocabTooltip(
                                message: nani,
                                inlineSpan: TextSpan(
                                  text: '何',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'を',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: iu,
                                inlineSpan: TextSpan(
                                  text: '言った',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: wakaru,
                                inlineSpan: const TextSpan(
                                  text: '分からない',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Don’t understand what he said.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kare,
                                inlineSpan: TextSpan(
                                  text: '先生',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'が',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: gakkou,
                                inlineSpan: TextSpan(
                                  text: '学校',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'に',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: iku,
                                inlineSpan: TextSpan(
                                  text: '行った',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: oshieru,
                                inlineSpan: const TextSpan(
                                  text: '教えない',
                                ),
                              ),
                              const TextSpan(
                                text: '？',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Won’t you inform me whether teacher went to school?',
                        ),
                      ],
                    ),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'In sentences like example 3 where the question being considered has a yes/no answer, it is common (but not necessary) to attach 「どうか」. This is roughly equivalent to saying, “whether or not” in English. You can also include the alternative as well to mean the same thing.'),
                  ],
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kare,
                                inlineSpan: const TextSpan(
                                  text: '先生',
                                ),
                              ),
                              const TextSpan(
                                text: 'が',
                              ),
                              VocabTooltip(
                                message: gakkou,
                                inlineSpan: const TextSpan(
                                  text: '学校',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: iku,
                                inlineSpan: const TextSpan(
                                  text: '行った',
                                ),
                              ),
                              const TextSpan(
                                text: 'か',
                              ),
                              VocabTooltip(
                                message: dou,
                                inlineSpan: TextSpan(
                                  text: 'どう',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: shiru,
                                inlineSpan: const TextSpan(
                                  text: '知らない',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Don’t know whether or not teacher went to school.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kare,
                                inlineSpan: const TextSpan(
                                  text: '先生',
                                ),
                              ),
                              const TextSpan(
                                text: 'が',
                              ),
                              VocabTooltip(
                                message: gakkou,
                                inlineSpan: const TextSpan(
                                  text: '学校',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: iku,
                                inlineSpan: const TextSpan(
                                  text: '行った',
                                ),
                              ),
                              const TextSpan(
                                text: 'か',
                              ),
                              VocabTooltip(
                                message: iku,
                                inlineSpan: TextSpan(
                                  text: '行かなかった',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              TextSpan(
                                text: 'か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                              VocabTooltip(
                                message: shiru,
                                inlineSpan: const TextSpan(
                                  text: '知らない',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Don’t know whether teacher went to school or didn’t.',
                        ),
                      ],
                    ),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary 昨日【きのう】 – yesterday 何【なに】 – what 食べる 【た・べる】 (ru-verb) – to eat 忘れる 【わす・れる】 (ru-verb) – to forget 彼 【かれ】 – he; boyfriend 言う 【い・う】 (u-verb) – to say 分かる 【わ・かる】 (u-verb) – to understand 先生 【せん・せい】 – teacher 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 教える 【おし・える】 (ru-verb) – to teach; to inform どう – how 知る 【し・る】 (u-verb) – to know Another use of the question marker is simply grammatical and has nothing to do with the politeness. A question marker attached to the end of a relative clause makes a mini-question inside a larger sentence. This allows the speaker to talk about the question. For example, you can talk about the question, “What did I eat today?” In the following examples, the question that is being considered is in red. 昨日何を食べたか忘れた。 Forgot what I ate yesterday. 彼は何を言ったか分からない。 Don’t understand what he said. 先生が学校に行ったか教えない？ Won’t you inform me whether teacher went to school? In sentences like example 3 where the question being considered has a yes/no answer, it is common (but not necessary) to attach 「どうか」. This is roughly equivalent to saying, “whether or not” in English. You can also include the alternative as well to mean the same thing. 先生が学校に行ったかどうか知らない。 Don’t know whether or not teacher went to school. 先生が学校に行ったか行かなかったか知らない。 Don’t know whether teacher went to school or didn’t.',
      grammars: ['かどうか'],
      keywords: ['ka','particle','question particle','relative clause'],
    ),
// TODO: add/append nazeka?
Section(
      heading: 'Using question words',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('おいしい (i-adj) – tasty'),
                    Text('クッキー – cookie'),
                    Text('全部 【ぜん・ぶ】 – everything'),
                    Text('食べる 【た・べる】 (ru-verb) – to eat'),
                    Text('誰 【だれ】 – who'),
                    Text('盗む 【ぬす・む】 (u-verb) – to steal'),
                    Text('知る 【し・る】 (u-verb) – to know'),
                    Text('犯人 【はん・にん】 – criminal'),
                    Text('見る 【み・る】 (ru-verb) – to see'),
                    Text('この – this （abbr. of これの）'),
                    Text('中 【なか】 – inside'),
                    Text('～から (particle) – from ～'),
                    Text('選ぶ 【えら・ぶ】 (u-verb) – to select'),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'While we’re on the topic of questions, this is a good time to go over question words (where, who, what, etc.) and what they mean in various contexts. Take a look at what adding the question marker does to the meaning of the words.'),
                  ],
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      const TableCaption(caption: 'Question Words'),
                      Table(
                        border: TableBorder.all(
                          color: Theme.of(context).colorScheme.outline,
                        ),
                        children: [
                          const TableRow(
                            children: [
                              TableHeading(
                                text: 'Word+Question Marker',
                              ),
                              TableHeading(
                                text: 'Meaning',
                              )
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: dare,
                                            inlineSpan: const TextSpan(
                                              text: '誰',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'か',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Someone'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: nani,
                                            inlineSpan: const TextSpan(
                                              text: '何',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'か',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Something'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'いつ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'か',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Sometime'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: doko,
                                            inlineSpan: const TextSpan(
                                              text: 'どこ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'か',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Somewhere'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'どれ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'か',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('A certain one from many'),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Heading(text: 'Examples', level: 2),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            'As you can see by the following examples, you can treat these words just like any regular nouns.'),
                  ],
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: dareka,
                                inlineSpan: TextSpan(
                                  text: '誰か',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              const TextSpan(
                                text: 'が',
                              ),
                              VocabTooltip(
                                message: oishii,
                                inlineSpan: const TextSpan(
                                  text: 'おいしい',
                                ),
                              ),
                              VocabTooltip(
                                message: kukkii,
                                inlineSpan: const TextSpan(
                                  text: 'クッキー',
                                ),
                              ),
                              const TextSpan(
                                text: 'を',
                              ),
                              VocabTooltip(
                                message: zenbu,
                                inlineSpan: const TextSpan(
                                  text: '全部',
                                ),
                              ),
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: const TextSpan(
                                  text: '食べた',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Someone ate all the delicious cookies.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: dare,
                                inlineSpan: const TextSpan(
                                  text: '誰',
                                ),
                              ),
                              const TextSpan(
                                text: 'が、',
                              ),
                              VocabTooltip(
                                message: nusumu,
                                inlineSpan: const TextSpan(
                                  text: '盗んだ',
                                ),
                              ),
                              const TextSpan(
                                text: 'のか、',
                              ),
                              VocabTooltip(
                                message: dareka,
                                inlineSpan: TextSpan(
                                  text: '誰か',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: shiru,
                                inlineSpan: const TextSpan(
                                  text: '知りません',
                                ),
                              ),
                              const TextSpan(
                                text: 'か。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Doesn’t anybody know who stole it?',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: hannin,
                                inlineSpan: const TextSpan(
                                  text: '犯人',
                                ),
                              ),
                              const TextSpan(
                                text: 'を',
                              ),
                              VocabTooltip(
                                message: dokoka,
                                inlineSpan: TextSpan(
                                  text: 'どこか',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              const TextSpan(
                                text: 'で',
                              ),
                              VocabTooltip(
                                message: miru,
                                inlineSpan: const TextSpan(
                                  text: '見ました',
                                ),
                              ),
                              const TextSpan(
                                text: 'か。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Did you see the criminal somewhere?',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kono,
                                inlineSpan: const TextSpan(
                                  text: 'この',
                                ),
                              ),
                              VocabTooltip(
                                message: naka,
                                inlineSpan: const TextSpan(
                                  text: '中',
                                ),
                              ),
                              const TextSpan(
                                text: 'から',
                              ),
                              VocabTooltip(
                                message: doreka,
                                inlineSpan: TextSpan(
                                  text: 'どれか',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              const TextSpan(
                                text: 'を',
                              ),
                              VocabTooltip(
                                message: erabu,
                                inlineSpan: const TextSpan(
                                  text: '選ぶ',
                                ),
                              ),
                              const TextSpan(
                                text: 'の。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          '(Explaining) You are to select a certain one from inside this (selection).',
                        ),
                      ],
                    ),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary おいしい (i-adj) – tasty クッキー – cookie 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 誰 【だれ】 – who 盗む 【ぬす・む】 (u-verb) – to steal 知る 【し・る】 (u-verb) – to know 犯人 【はん・にん】 – criminal 見る 【み・る】 (ru-verb) – to see この – this （abbr. of これの） 中 【なか】 – inside～から (particle) – from ～選ぶ 【えら・ぶ】 (u-verb) – to select While we’re on the topic of questions, this is a good time to go over question words (where, who, what, etc.) and what they mean in various contexts. Take a look at what adding the question marker does to the meaning of the words. 誰か Someone 何か Something いつか Sometime どこか Somewhere どれか A certain one from many Examples As you can see by the following examples, you can treat these words just like any regular nouns. 誰かがおいしいクッキーを全部食べた。 Someone ate all the delicious cookies. 誰が、盗んだのか、誰か知りませんか。 Doesn’t anybody know who stole it? 犯人をどこかで見ましたか。 Did you see the criminal somewhere? この中からどれかを選ぶの。(Explaining) You are to select a certain one from inside this (selection).',
      grammars: [],
      keywords: ['dareka','nanika','dokoka','doreka','itsuka','someone','something','sometime','somewhere','ka','particle','question word'],
    ),
Section(
      heading: 'Question words with inclusive meaning',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('全部 【ぜん・ぶ】 – everything'),
                    Text('皆 【みんな】 – everybody'),
                    Text('皆さん 【みな・さん】 – everybody (polite)'),
                    Text('この – this （abbr. of これの）'),
                    Text('質問 【しつ・もん】 – question'),
                    Text('答え 【こた・え】 – answer'),
                    Text('知る 【し・る】 (u-verb) – to know'),
                    Text('友達 【とも・だち】 – friend'),
                    Text('遅れる 【おく・れる】 (ru-verb) – to be late'),
                    Text('ここ – here'),
                    Text('ある (u-verb) – to exist (inanimate)'),
                    Text('レストラン – restaurant'),
                    Text('おいしい (i-adj) – tasty'),
                    Text('今週末 【こん・しゅう・まつ】 – this weekend'),
                    Text('行く 【い・く】 (u-verb) – to go'),
                  ],
                ),
                Paragraph(
                  texts: [
                    const TextSpan(
                        text:
                            'The same question words in the chart above can be combined with 「も」 in a negative sentence to mean “nobody” （'),
                    VocabTooltip(
                      message: daremo,
                      inlineSpan: const TextSpan(
                        text: '誰も',
                      ),
                    ),
                    const TextSpan(text: '）, “nothing” （'),
                    VocabTooltip(
                      message: nanimo,
                      inlineSpan: const TextSpan(
                        text: '何も',
                      ),
                    ),
                    const TextSpan(text: '）, “nowhere” （'),
                    VocabTooltip(
                      message: dokomo,
                      inlineSpan: const TextSpan(
                        text: 'どこも',
                      ),
                    ),
                    const TextSpan(text: '）, etc.'),
                  ],
                ),
                Paragraph(
                  texts: [
                    const TextSpan(text: '「'),
                    VocabTooltip(
                      message: daremo,
                      inlineSpan: const TextSpan(
                        text: '誰も',
                      ),
                    ),
                    const TextSpan(text: '」 and 「'),
                    VocabTooltip(
                      message: nanimo,
                      inlineSpan: const TextSpan(
                        text: '何も',
                      ),
                    ),
                    const TextSpan(
                        text:
                            '」 are primarily used only for negative sentences. Curiously, there is no way to say “everything” with question words. Instead, it is conventional to use other words like 「'),
                    VocabTooltip(
                      message: zenbu,
                      inlineSpan: const TextSpan(
                        text: '全部',
                      ),
                    ),
                    const TextSpan(text: '」. And although 「'),
                    VocabTooltip(
                      message: daremo,
                      inlineSpan: const TextSpan(
                        text: '誰も',
                      ),
                    ),
                    const TextSpan(
                        text:
                            '」 can sometimes be used to mean “everybody”, it is customary to use 「'),
                    VocabTooltip(
                      message: daremo,
                      inlineSpan: const TextSpan(
                        text: '皆',
                      ),
                    ),
                    const TextSpan(text: '」 or 「'),
                    VocabTooltip(
                      message: minasan,
                      inlineSpan: const TextSpan(
                        text: '皆さん',
                      ),
                    ),
                    const TextSpan(text: '」.'),
                  ],
                ),
                Paragraph(
                  texts: [
                    const TextSpan(text: 'The remaining three words 「'),
                    VocabTooltip(
                      message: itsumo,
                      inlineSpan: const TextSpan(
                        text: 'いつも',
                      ),
                    ),
                    const TextSpan(text: '」 (meaning “always”) and 「'),
                    VocabTooltip(
                      message: doremo,
                      inlineSpan: const TextSpan(
                        text: 'どれも',
                      ),
                    ),
                    const TextSpan(text: '」 (meaning “any and all”), and 「'),
                    VocabTooltip(
                      message: dokomo,
                      inlineSpan: const TextSpan(
                        text: 'どこも',
                      ),
                    ),
                    const TextSpan(
                        text:
                            '」 (meaning everywhere) can be used in both negative and positive sentences.'),
                  ],
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      const TableCaption(caption: 'Inclusive Words'),
                      Table(
                        border: TableBorder.all(
                          color: Theme.of(context).colorScheme.outline,
                        ),
                        children: [
                          const TableRow(
                            children: [
                              TableHeading(
                                text: 'Word+Question Marker',
                              ),
                              TableHeading(
                                text: 'Meaning',
                              )
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: dare,
                                            inlineSpan: const TextSpan(
                                              text: '誰',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'も',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Everybody/Nobody'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: nani,
                                            inlineSpan: const TextSpan(
                                              text: '何',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'も',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Nothing (negative only)'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'いつ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'も',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Always'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: doko,
                                            inlineSpan: const TextSpan(
                                              text: 'どこ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'も',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Everywhere'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'どれ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'も',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Any and all'),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Heading(text: 'Examples', level: 2),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kono,
                                inlineSpan: const TextSpan(
                                  text: 'この',
                                ),
                              ),
                              VocabTooltip(
                                message: shitsumon,
                                inlineSpan: const TextSpan(
                                  text: '質問',
                                ),
                              ),
                              const TextSpan(
                                text: 'の',
                              ),
                              VocabTooltip(
                                message: kotae,
                                inlineSpan: const TextSpan(
                                  text: '答え',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: daremo,
                                inlineSpan: TextSpan(
                                  text: '誰も',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: shiru,
                                inlineSpan: const TextSpan(
                                  text: '知らない',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Nobody knows the answer of this question.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: tomodachi,
                                inlineSpan: const TextSpan(
                                  text: '友達',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: itsumo,
                                inlineSpan: TextSpan(
                                  text: 'いつも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: okureru,
                                inlineSpan: const TextSpan(
                                  text: '遅れる',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Friend is always late.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: koko,
                                inlineSpan: const TextSpan(
                                  text: 'ここ',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              VocabTooltip(
                                message: resutoran,
                                inlineSpan: const TextSpan(
                                  text: 'レストラン',
                                ),
                              ),
                              const TextSpan(
                                text: 'は',
                              ),
                              VocabTooltip(
                                message: doremo,
                                inlineSpan: TextSpan(
                                  text: 'どれも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: oishii,
                                inlineSpan: const TextSpan(
                                  text: 'おいしくない',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Any and all restaurants that are here are not tasty.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: konshuumatsu,
                                inlineSpan: const TextSpan(
                                  text: '今週末',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: naka,
                                inlineSpan: TextSpan(
                                  text: 'どこにも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: iku,
                                inlineSpan: const TextSpan(
                                  text: '行かなかった',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Went nowhere this weekend.',
                        ),
                      ],
                    ),
                  ],
                ),
                const Paragraph(
                  texts: [
                    TextSpan(
                        text:
                            '(Grammatically, this 「も」 is the same as the topic particle 「も」 so the target particle 「に」 must go before the topic particle 「も」 in ordering.)'),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary 全部 【ぜん・ぶ】 – everything 皆 【みんな】 – everybody 皆さん 【みな・さん】 – everybody (polite) この – this （abbr. of これの） 質問 【しつ・もん】 – question 答え 【こた・え】 – answer 知る 【し・る】 (u-verb) – to know 友達 【とも・だち】 – friend 遅れる 【おく・れる】 (ru-verb) – to be late ここ – here ある (u-verb) – to exist (inanimate) レストラン – restaurant おいしい (i-adj) – tasty 今週末 【こん・しゅう・まつ】 – this weekend 行く 【い・く】 (u-verb) – to go The same question words in the chart above can be combined with 「も」 in a negative sentence to mean “nobody” （誰も）, “nothing” （何も）, “nowhere” （どこも）, etc. 「誰も」 and 「何も」 are primarily used only for negative sentences. Curiously, there is no way to say “everything” with question words. Instead, it is conventional to use other words like 「全部」. And although 「誰も」 can sometimes be used to mean “everybody”, it is customary to use 「皆」 or 「皆さん」. The remaining three words 「いつも」 (meaning “always”) and 「どれも」 (meaning “any and all”), and 「どこも」 (meaning everywhere) can be used in both negative and positive sentences. 誰も Everybody/Nobody 何も Nothing (negative only) いつも Always どこも Everywhere どれも Any and all Examples この質問の答えは、誰も知らない。 Nobody knows the answer of this question. 友達は、いつも遅れる。 Friend is always late. ここにあるレストランはどれもおいしくない。 Any and all restaurants that are here are not tasty. 今週末は、どこにも行かなかった。 Went nowhere this weekend.(Grammatically, this 「も」 is the same as the topic particle 「も」 so the target particle 「に」 must go before the topic particle 「も」 in ordering.)',
      grammars: [],
      keywords: ['daremo','nanimo','dokomo','doremo','itsumo','nobody','no-one','everybody','everyone','nothing','everything','nowhere','everywhere','always','any and all','mo','particle','question word'],
    ),
Section(
      heading: 'Question words to mean “any”',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [const Heading(text: 'Vocabulary', level: 2),
                const NumberedList(
                  items: [
                    Text('この – this （abbr. of これの）'),
                    Text('質問 【しつ・もん】 – question'),
                    Text('答え 【こた・え】 – answer'),
                    Text('分かる 【わ・かる】 (u-verb) – to understand'),
                    Text('昼ご飯 【ひる・ご・はん】 – lunch'),
                    Text('いい (i-adj) – good'),
                    Text('あの – that (over there) （abbr. of あれの）'),
                    Text('人 【ひと】 – person'),
                    Text('本当 【ほん・とう】 – real'),
                    Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  ],
                ),
                Paragraph(
                  texts: [
                    const TextSpan(
                        text:
                            'The same question words combined with 「でも」 can be used to mean “any”. One thing to be careful about is that 「'),
                    VocabTooltip(
                      message: daremo,
                      inlineSpan: const TextSpan(
                        text: '何でも',
                      ),
                    ),
                    const TextSpan(text: '」 is read as 「なんでも」 and not 「なにでも」.'),
                  ],
                ),
                Padding(
                  padding: kDefaultParagraphPadding,
                  child: Column(
                    children: [
                      const TableCaption(caption: 'Words for “Any”'),
                      Table(
                        border: TableBorder.all(
                          color: Theme.of(context).colorScheme.outline,
                        ),
                        children: [
                          const TableRow(
                            children: [
                              TableHeading(
                                text: 'Word+Question Marker',
                              ),
                              TableHeading(
                                text: 'Meaning',
                              )
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: dare,
                                            inlineSpan: const TextSpan(
                                              text: '誰',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'でも',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Anybody'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: nani,
                                            inlineSpan: const TextSpan(
                                              text: '何',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'でも',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Anything'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'いつ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'でも',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Anytime'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: doko,
                                            inlineSpan: const TextSpan(
                                              text: 'どこ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'でも',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Anywhere'),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              TableData(
                                centered: false,
                                content: Column(
                                  children: [
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          VocabTooltip(
                                            message: itsu,
                                            inlineSpan: const TextSpan(
                                              text: 'どれ',
                                            ),
                                          ),
                                          TextSpan(
                                            text: 'でも',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TableData(
                                centered: false,
                                content: Text('Whichever'),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                NumberedList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: kono,
                                inlineSpan: const TextSpan(
                                  text: 'この',
                                ),
                              ),
                              VocabTooltip(
                                message: shitsumon,
                                inlineSpan: const TextSpan(
                                  text: '質問',
                                ),
                              ),
                              const TextSpan(
                                text: 'の',
                              ),
                              VocabTooltip(
                                message: kotae,
                                inlineSpan: const TextSpan(
                                  text: '答え',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: daredemo,
                                inlineSpan: TextSpan(
                                  text: '誰でも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: wakaru,
                                inlineSpan: const TextSpan(
                                  text: '分かる',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Anybody understands the answer of this question.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: hirugohan,
                                inlineSpan: const TextSpan(
                                  text: '昼ご飯',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: dokodemo,
                                inlineSpan: TextSpan(
                                  text: 'どこでも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: ii,
                                inlineSpan: const TextSpan(
                                  text: 'いい',
                                ),
                              ),
                              const TextSpan(
                                text: 'です。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'About lunch, anywhere is good.',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              VocabTooltip(
                                message: koko,
                                inlineSpan: const TextSpan(
                                  text: 'あの',
                                ),
                              ),
                              VocabTooltip(
                                message: hito,
                                inlineSpan: const TextSpan(
                                  text: '人',
                                ),
                              ),
                              const TextSpan(
                                text: 'は、',
                              ),
                              VocabTooltip(
                                message: hontou,
                                inlineSpan: const TextSpan(
                                  text: '本当',
                                ),
                              ),
                              const TextSpan(
                                text: 'に',
                              ),
                              VocabTooltip(
                                message: nandemo,
                                inlineSpan: TextSpan(
                                  text: '何でも',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                ),
                              ),
                              VocabTooltip(
                                message: taberu,
                                inlineSpan: const TextSpan(
                                  text: '食べる',
                                ),
                              ),
                              const TextSpan(
                                text: '。',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'That person really eats anything.',
                        ),
                      ],
                    ),
                  ],
                ),],
          );
        },
      ),
      plaintext: 'Vocabulary この – this （abbr. of これの） 質問 【しつ・もん】 – question 答え 【こた・え】 – answer 分かる 【わ・かる】 (u-verb) – to understand 昼ご飯 【ひる・ご・はん】 – lunch いい (i-adj) – good あの – that (over there) （abbr. of あれの） 人 【ひと】 – person 本当 【ほん・とう】 – real 食べる 【た・べる】 (ru-verb) – to eat The same question words combined with 「でも」 can be used to mean “any”. One thing to be careful about is that 「何でも」 is read as 「なんでも」 and not 「なにでも」. 誰でも Anybody 何でも Anything いつでも Anytime どこでも Anywhere どれでも Whichever この質問の答えは、誰でも分かる。 Anybody understands the answer of this question. 昼ご飯は、どこでもいいです。 About lunch, anywhere is good. あの人は、本当に何でも食べる。 That person really eats anything.',
      grammars: [],
      keywords: ['daredemo','dokodemo','nandemo','doredemo','itsudemo','anybody','anyone','anywhere','anything','whichever','anytime','demo','particle','question word'],
    ),
  ],
);
