import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson requests = Lesson(
  title: 'Making Requests',
  sections: [
    Section(
      heading: 'Politely (and not so politely) making requests',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Similar to asking for favors, which we learned in the last lesson, there are also various ways to make requests in Japanese. This is effectively the Japanese way of saying, “please do X”. We’ll first learn the most common way to make requests using a special conjugation of the verb 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(text: 'くださる'),
                  ),
                  const TextSpan(
                    text: '」 and the firmer 「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(text: 'なさる'),
                  ),
                  const TextSpan(
                    text:
                        '」. Finally, we’ll learn the rarely used excessively strong command form for the sake of completeness. You can safely skip the last part unless you’re an avid reader of manga.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Similar to asking for favors, which we learned in the last lesson, there are also various ways to make requests in Japanese. This is effectively the Japanese way of saying, “please do X”. We’ll first learn the most common way to make requests using a special conjugation of the verb 「くださる」 and the firmer 「なさる」. Finally, we’ll learn the rarely used excessively strong command form for the sake of completeness. You can safely skip the last part unless you’re an avid reader of manga.',
      grammars: [],
      keywords: ['please','request','ask'],
    ),
    Section(
      heading: '「～ください」－ a special conjugation of 「くださる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('それ – that'),
                  Text('くれる (ru-verb) – to give'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('ここ – here'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('消しゴム 【け・し・ごむ】 – eraser'),
                  Text('貸す 【か・す】 (u-verb) – lend'),
                  Text('遠い 【とお・い】 (i-adj) – far'),
                  Text('所 【ところ】 – place'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('お父さん【お・とう・さん】 – father (polite)'),
                  Text('時計 【と・けい】 – watch; clock'),
                  Text('壊れる 【こわ・れる】 (ru-verb) – to break'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 is a special conjugation of 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(text: 'くださる'),
                  ),
                  const TextSpan(
                    text: '」, which is the honorific form of 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text:
                        '」. We will learn more about honorific and humble forms in the beginning of the next major section. We are going over 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text:
                        '」 here because it has a slight difference in meaning from the normal 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text: '」 and the honorific 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(text: 'くださる'),
                  ),
                  const TextSpan(
                    text: '」. 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 is different from 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text: '」 in the following fashion:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: TextSpan(
                                text: 'ください',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please give me that.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Can you give me that?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'As you can see 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 is a direct request for something while 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text:
                        '」 is used as a question asking for someone to give something. However, it is similar to 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text:
                        '」 in that you can make a request for an action by simply attaching it to the te-form of the verb.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書いて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please write it in kanji.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yukkuri,
                              inlineSpan: const TextSpan(
                                text: 'ゆっくり',
                              ),
                            ),
                            VocabTooltip(
                              message: hanasu,
                              inlineSpan: TextSpan(
                                text: '話して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please speak slowly.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The rules for negative requests are same as the rules for 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text: '」 as well.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: rakugaki,
                              inlineSpan: const TextSpan(
                                text: '落書き',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書かない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please don’t write graffiti.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                text: 'こないで',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please don’t come here.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In casual speech, it is often common to simply drop the 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 part.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: hanasu,
                              inlineSpan: TextSpan(
                                text: '話して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please speak in Japanese.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: keshigomu,
                              inlineSpan: const TextSpan(
                                text: '消しゴム',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: TextSpan(
                                text: '貸して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please lend me the eraser.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tooi,
                              inlineSpan: const TextSpan(
                                text: '遠い',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: const TextSpan(
                                text: '所',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please don’t go to a far place.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For those who want to sound particularly commanding and manly, it is also possible to use 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text: '」 with the 「る」 removed.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: hanasu,
                              inlineSpan: TextSpan(
                                text: '話して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Speak in Japanese.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: keshigomu,
                              inlineSpan: const TextSpan(
                                text: '消しゴム',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kasu,
                              inlineSpan: TextSpan(
                                text: '貸して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Lend me the eraser.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tooi,
                              inlineSpan: const TextSpan(
                                text: '遠い',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: const TextSpan(
                                text: '所',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Don’t go to a far place.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Because 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 like the masu-form must always come at the end sentence or a relative clause, you cannot use it to directly modify a noun. For example, the following is not possible with 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: otousan,
                              inlineSpan: const TextSpan(
                                text: 'お父さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: tokei,
                              inlineSpan: const TextSpan(
                                text: '時計',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kowareru,
                              inlineSpan: const TextSpan(
                                text: '壊れた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The clock that father gave broke.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Of course, since direct quotes is merely repeating something someone said in verbatim, you can put practically anything in a direct quote.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '「',
                            ),
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(
                              text: '」',
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: otousan,
                              inlineSpan: const TextSpan(
                                text: 'お父さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言った',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Father said, “Please give me that.”',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary それ – that くれる (ru-verb) – to give 漢字 【かん・じ】 – Kanji 書く 【か・く】 (u-verb) – to write ここ – here 来る 【く・る】 (exception) – to come 日本語 【に・ほん・ご】 – Japanese (language) 話す 【はな・す】 (u-verb) – to speak 消しゴム 【け・し・ごむ】 – eraser 貸す 【か・す】 (u-verb) – lend 遠い 【とお・い】 (i-adj) – far 所 【ところ】 – place 行く 【い・く】 (u-verb) – to go お父さん【お・とう・さん】 – father (polite) 時計 【と・けい】 – watch; clock 壊れる 【こわ・れる】 (ru-verb) – to break 言う 【い・う】 (u-verb) – to say 「ください」 is a special conjugation of 「くださる」, which is the honorific form of 「くれる」. We will learn more about honorific and humble forms in the beginning of the next major section. We are going over 「ください」 here because it has a slight difference in meaning from the normal 「くれる」 and the honorific 「くださる」. 「ください」 is different from 「くれる」 in the following fashion: それをください。 Please give me that. それをくれる？ Can you give me that? As you can see 「ください」 is a direct request for something while 「くれる」 is used as a question asking for someone to give something. However, it is similar to 「くれる」 in that you can make a request for an action by simply attaching it to the te-form of the verb. 漢字で書いてください。 Please write it in kanji. ゆっくり話してください。 Please speak slowly. The rules for negative requests are same as the rules for 「くれる」 as well. 落書きを書かないでください。 Please don’t write graffiti. ここにこないでください。 Please don’t come here. In casual speech, it is often common to simply drop the 「ください」 part. 日本語で話して。 Please speak in Japanese. 消しゴムを貸して。 Please lend me the eraser. 遠い所に行かないで。 Please don’t go to a far place. For those who want to sound particularly commanding and manly, it is also possible to use 「くれる」 with the 「る」 removed. 日本語で話してくれ。 Speak in Japanese. 消しゴムを貸してくれ。 Lend me the eraser. 遠い所に行かないでくれ。 Don’t go to a far place. Because 「ください」 like the masu-form must always come at the end sentence or a relative clause, you cannot use it to directly modify a noun. For example, the following is not possible with 「ください」. お父さんがくれた時計が壊れた。 The clock that father gave broke. Of course, since direct quotes is merely repeating something someone said in verbatim, you can put practically anything in a direct quote. 「それをください」とお父さんが言った。 Father said, “Please give me that.”',
      grammars: ['～ください'],
      keywords: ['kudasai','please'],
    ),
    Section(
      heading: 'Using 「～ちょうだい」 as a casual request',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('頂戴 【ちょうだい】 – receiving (humble)'),
                  Text('致す 【いたす】 (u-verb) – to do (humble)'),
                  Text('スプーン – spoon'),
                  Text('ここ – here'),
                  Text('名前 【な・まえ】 – name'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'A casual alternative of 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(
                      text: 'ください',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is 「',
                  ),
                  VocabTooltip(
                    message: choudai,
                    inlineSpan: const TextSpan(
                      text: 'ちょうだい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. While it can be used by anyone, it has a slightly feminine and childish nuance and is always written in Hiragana. Written in Kanji, it is usually used in a very formal expression such as 「',
                  ),
                  VocabTooltip(
                    message: choudai,
                    inlineSpan: const TextSpan(
                      text: '頂戴',
                    ),
                  ),
                  VocabTooltip(
                    message: itasu,
                    inlineSpan: const TextSpan(
                      text: '致します',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Grammatically, it’s used exactly the same way as 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(
                      text: 'ください',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: supuun,
                              inlineSpan: const TextSpan(
                                text: 'スプーン',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: choudai,
                              inlineSpan: TextSpan(
                                text: 'ちょうだい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please give me the spoon.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書いて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: choudai,
                              inlineSpan: TextSpan(
                                text: 'ちょうだい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Please write your name here.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 頂戴 【ちょうだい】 – receiving (humble) 致す 【いたす】 (u-verb) – to do (humble) スプーン – spoon ここ – here 名前 【な・まえ】 – name 書く 【か・く】 (u-verb) – to write A casual alternative of 「ください」 is 「ちょうだい」. While it can be used by anyone, it has a slightly feminine and childish nuance and is always written in Hiragana. Written in Kanji, it is usually used in a very formal expression such as 「頂戴致します」. Grammatically, it’s used exactly the same way as 「ください」. Examples スプーンをちょうだい。 Please give me the spoon. ここに名前を書いてちょうだい？ Please write your name here.',
      grammars: ['～ちょうだい'],
      keywords: ['choudai','casual','please'],
    ),
    Section(
      heading: 'Using 「～なさい」 to make firm but polite requests',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('する (exception) – to do'),
                  Text('いい (i-adj) – good'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('ここ – here'),
                  Text('座る 【すわ・る】 (ru-verb) – to sit'),
                  Text('まだ – yet'),
                  Text('いっぱい – full'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('たくさん – a lot (amount)'),
                  Text('それ – that'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('そう – (things are) that way'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(
                      text: 'なさい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is a special honorific conjugation of 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. It is a soft yet firm way of issuing a command. It is used, for example, when a mother is scolding her child or when a teacher wants a delinquent student to pay attention. Unlike 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(
                      text: 'ください',
                    ),
                  ),
                  const TextSpan(
                    text: '」, 「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(
                      text: 'なさい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 only applies to positive verbs and uses the stem of the verb instead of the te-form. It also cannot be used by itself but must be attached to another verb.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「なさい」 to make firm but polite requests',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Conjugate the verb to its stem and attach 「',
                              ),
                              VocabTooltip(
                                message: nasaru,
                                inlineSpan: const TextSpan(
                                  text: 'なさい',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nasaru,
                                    inlineSpan: TextSpan(
                                      text: 'なさい',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '飲',
                                        ),
                                        TextSpan(
                                          text: 'む',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '飲',
                                        ),
                                        TextSpan(
                                          text: 'み',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: nomu,
                                    inlineSpan: const TextSpan(
                                      text: '飲み',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nasaru,
                                    inlineSpan: TextSpan(
                                      text: 'なさい',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'し',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nasaru,
                                    inlineSpan: TextSpan(
                                      text: 'なさい',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よく',
                              ),
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: TextSpan(
                                text: '聞き',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: nasaru,
                              inlineSpan: const TextSpan(
                                text: 'なさい',
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Listen well!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suwaru,
                              inlineSpan: TextSpan(
                                text: '座り',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: nasaru,
                              inlineSpan: const TextSpan(
                                text: 'なさい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Sit here.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also drop the 「さい」 portion of the 「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(
                      text: 'なさい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to make a casual version of this grammar.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mada,
                              inlineSpan: const TextSpan(
                                text: 'まだ',
                              ),
                            ),
                            VocabTooltip(
                              message: ippai,
                              inlineSpan: const TextSpan(
                                text: 'いっぱい',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べな',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'There’s still a lot, so eat a lot.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思う',
                              ),
                            ),
                            const TextSpan(
                              text: 'なら、',
                            ),
                            VocabTooltip(
                              message: souLikeThat,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しな',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you think that’s fine, then go ahead and do it.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【たべ・る】 (ru-verb) – to eat 飲む 【の・む】 (u-verb) – to drink する (exception) – to do いい (i-adj) – good 聞く 【き・く】 (u-verb) – to ask; to listen ここ – here 座る 【すわ・る】 (ru-verb) – to sit まだ – yet いっぱい – full ある (u-verb) – to exist (inanimate) たくさん – a lot (amount) それ – that 思う 【おも・う】 (u-verb) – to think そう – (things are) that way 「なさい」 is a special honorific conjugation of 「する」. It is a soft yet firm way of issuing a command. It is used, for example, when a mother is scolding her child or when a teacher wants a delinquent student to pay attention. Unlike 「ください」, 「なさい」 only applies to positive verbs and uses the stem of the verb instead of the te-form. It also cannot be used by itself but must be attached to another verb. Using 「なさい」 to make firm but polite requests Conjugate the verb to its stem and attach 「なさい」. Examples: 食べる → 食べなさい 飲む → 飲み → 飲みなさいする → し → しなさい Examples よく聞きなさい！ Listen well! ここに座りなさい。 Sit here. You can also drop the 「さい」 portion of the 「なさい」 to make a casual version of this grammar. まだいっぱいあるから、たくさん食べな。 There’s still a lot, so eat a lot. それでいいと思うなら、そうしなよ。 If you think that’s fine, then go ahead and do it.',
      grammars: ['～なさい'],
      keywords: ['polite','casual','firm','verb'],
    ),
    Section(
      heading: 'The command form',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('くれる (ru-verb) – to give'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('直る 【なお・る】 (u-verb) – to be fixed'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('好き 【す・き】 (na-adj) – likable'),
                  Text('あっち – that way (over there) （abbr of あちら）'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('酒 【さけ】 – alcohol'),
                  Text('持つ 【も・つ】 (u-verb) – to hold'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We will go over the command form in the interest of covering all the possible verb conjugations. In reality, the command form is rarely used as Japanese people tend to be too polite to use imperatives. Also, this coarse type of speech is rarely, if indeed at all, used by females who tend to use 「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(
                      text: 'なさい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or an exasperated 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 when angry or irritated. This form is only really useful for reading or watching fictional works. You may often see or hear 「',
                  ),
                  VocabTooltip(
                    message: shinu,
                    inlineSpan: const TextSpan(
                      text: '死ね',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '！」 (“Die!”) in fiction which, of course, you’ll never hear in real life. (I hope!)',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Be sure to note that, in addition to the familiar 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」, 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'くる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 exception verbs, 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is also an exception for the command form.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'For ru-verbs:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text: ' Replace the 「る」 with 「ろ」.',
                          ),
                        ],
                      ),
                    ),
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'For u-verbs:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' Change the last character from an / u / vowel to an / e / vowel.',
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'しろ',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'くる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'こい',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kureru,
                                    inlineSpan: const TextSpan(
                                      text: 'くれる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kureru,
                                    inlineSpan: const TextSpan(
                                      text: 'くれ',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Rules for creating command form',
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample ru-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Command'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'ろ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Command'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'す',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'せ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'け',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'け',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぶ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'べ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'つ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'て',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'む',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'め',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'め',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ぬ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ね',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'う',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'え',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exception Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Command'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'しろ',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'こい',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kureru,
                                  inlineSpan: const TextSpan(
                                    text: 'くれる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kureru,
                                  inlineSpan: const TextSpan(
                                    text: 'くれ',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary くれる (ru-verb) – to give 死ぬ 【し・ぬ】 (u-verb) – to die する (exception) – to do 来る 【く・る】 (exception) – to come 食べる 【た・べる】 (ru-verb) – to eat 着る 【き・る】 (ru-verb) – to wear 信じる 【しん・じる】 (ru-verb) – to believe 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 出る 【で・る】 (ru-verb) – to come out 掛ける 【か・ける】 (ru-verb) – to hang 捨てる 【す・てる】 (ru-verb) – to throw away 話す 【はな・す】 (u-verb) – to speak 聞く 【き・く】 (u-verb) – to ask; to listen 遊ぶ 【あそ・ぶ】 (u-verb) – to play 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 直る 【なお・る】 (u-verb) – to be fixed 買う 【か・う】 (u-verb) – to buy 好き 【す・き】 (na-adj) – likable あっち – that way (over there) （abbr of あちら） 行く 【い・く】 (u-verb) – to go 早い 【はや・い】 (i-adj) – fast; early 酒 【さけ】 – alcohol 持つ 【も・つ】 (u-verb) – to hold We will go over the command form in the interest of covering all the possible verb conjugations. In reality, the command form is rarely used as Japanese people tend to be too polite to use imperatives. Also, this coarse type of speech is rarely, if indeed at all, used by females who tend to use 「なさい」 or an exasperated 「くれる」 when angry or irritated. This form is only really useful for reading or watching fictional works. You may often see or hear 「死ね！」 (“Die!”) in fiction which, of course, you’ll never hear in real life. (I hope!) Be sure to note that, in addition to the familiar 「する」, 「くる」 exception verbs, 「くれる」 is also an exception for the command form. Rules for creating command form For ru-verbs: Replace the 「る」 with 「ろ」. For u-verbs: Change the last character from an / u / vowel to an / e / vowel. Exceptions: する → しろ くる → こい くれる → くれ',
      grammars: ['～ろ','～え'],
      keywords: ['command','demand','imperative','verb'],
    ),
    Section(
      heading: 'Negative command',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('する (exception) – to do'),
                  Text('それ – that'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('変 【へん】 (na-adj) – strange'),
                  Text('こと – event, matter'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The negative command form is very simple: simply attach 「な」 to either ru-verbs or u-verbs. Don’t confuse this with the 「な」 sentence-ending particle we will be learning at the end of this section. The intonation is totally different.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「な」 to the verb.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  TextSpan(
                                      text: 'な',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary)),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                      text: 'な',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Using the negative command form',
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            TextSpan(
                                text: 'な',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary)),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Don’t eat that!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hen,
                              inlineSpan: const TextSpan(
                                text: '変',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言う',
                              ),
                            ),
                            TextSpan(
                                text: 'な',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary)),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Don’t say such weird things!',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This is not to be confused with the shortened version of 「～なさい」 we just learned in the last section. The most obvious difference (besides the clear difference in tone) is that in 「～なさい」, the verb is first converted to the stem while the negative command has no conjugation. For example, for 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」, 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'しな',
                    ),
                  ),
                  const TextSpan(
                    text: '」 would be the short version of 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'しなさい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 while 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'するな',
                    ),
                  ),
                  const TextSpan(
                    text: '」 would be a negative command.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 行く 【い・く】 (u-verb) – to go する (exception) – to do それ – that 食べる 【た・べる】 (ru-verb) – to eat 変 【へん】 (na-adj) – strange こと – event, matter 言う 【い・う】 (u-verb) – to say The negative command form is very simple: simply attach 「な」 to either ru-verbs or u-verbs. Don’t confuse this with the 「な」 sentence-ending particle we will be learning at the end of this section. The intonation is totally different. Using the negative command form Attach 「な」 to the verb. Examples: 行く → 行くな する → するな Examples それを食べるな！ Don’t eat that! 変なことを言うな！ Don’t say such weird things! This is not to be confused with the shortened version of 「～なさい」 we just learned in the last section. The most obvious difference (besides the clear difference in tone) is that in 「～なさい」, the verb is first converted to the stem while the negative command has no conjugation. For example, for 「する」, 「しな」 would be the short version of 「しなさい」 while 「するな」 would be a negative command.',
      grammars: ['～な'],
      keywords: ['command','demand','imperative','negative','verb'],
    ),
  ],
);
