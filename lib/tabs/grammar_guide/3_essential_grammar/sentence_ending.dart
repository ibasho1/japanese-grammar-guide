import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson sentenceEnding = Lesson(
  title: 'Review and More Sentence-Ending Particles',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We are coming to the end of the fourth major section of the guide. Do you feel like your Japanese has improved? We’ve come to the point where we’ve learned enough conjugations to be able to start mixing them together in various useful combinations. Of course this can be a little difficult to do without some practice, which is the reason for this lesson. But first, since we’ve come to the end of yet another section, let’s learn some more sentence-endings particles.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We are coming to the end of the fourth major section of the guide. Do you feel like your Japanese has improved? We’ve come to the point where we’ve learned enough conjugations to be able to start mixing them together in various useful combinations. Of course this can be a little difficult to do without some practice, which is the reason for this lesson. But first, since we’ve come to the end of yet another section, let’s learn some more sentence-endings particles.',
      grammars: [],
      keywords: ['review','sentence-ending particle','sentence-ending','particle'],
    ),
    Section(
      heading: '「な」 and 「さ」 sentence-ending particles',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('あのう／あの – say; well; errr'),
                  Text('うん – yes (casual)'),
                  Text('この – this （abbr. of これの）'),
                  Text('間 【あいだ】 – space (between); time (between); period'),
                  Text('ディズニーランド – Disney Land'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('すごい (i-adj) – to a great extent'),
                  Text('込む 【こ・む】 (u-verb) – to become crowded'),
                  Text('何 【なに／なん】 – what'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('図書館 【と・しょ・かん】 – library'),
                  Text('何で 【なん・で】 – why; how'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('たくさん – a lot (amount)'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('まだ – yet'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('大丈夫 【だい・じょう・ぶ】 (na-adj) – ok'),
                  Text('なる (u-verb) – to become'),
                  Text('いい (i-adj) – good'),
                  Text('今日 【きょう】 – today'),
                  Text('雨 【あめ】 – rain'),
                  Text('降る 【ふ・る】(u-verb) – to precipitate'),
                  Text('大学 【だい・がく】 – college'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'After the 「よ」 and 「ね」, 「さ」 and 「な」 are the next most commonly used sentence-ending particles.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「さ」, which is basically a very casual form of 「よ」, is similar to the English “like” in that some people throw it in at the end of almost every single phrase. Of course, that doesn’t mean it’s necessarily a very sophisticated manner of speech but just like using “like” all the time, I cannot deny that it is an easy habit to fall into. In that sense, due to its over-use, it has almost lost any specific meaning. You may overhear a conversation like the following:'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: anoHey,
                            inlineSpan: const TextSpan(
                              text: 'あの',
                            ),
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '・・・',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Hey…'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Yeah.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: konoaida,
                            inlineSpan: const TextSpan(
                              text: 'この間',
                            ),
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '・・・',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('This one time…'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Yeah.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: dizuniirando,
                            inlineSpan: const TextSpan(
                              text: 'ディズニーランド',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行った',
                            ),
                          ),
                          const TextSpan(
                            text: 'んだけど',
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: nanka,
                            inlineSpan: const TextSpan(
                              text: 'なんか',
                            ),
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: sugoi,
                            inlineSpan: const TextSpan(
                              text: 'すごい',
                            ),
                          ),
                          VocabTooltip(
                            message: komu,
                            inlineSpan: const TextSpan(
                              text: '込んで',
                            ),
                          ),
                          const TextSpan(
                            text: 'て・・・',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text(
                        'I went to Disney Land and it was really crowded…'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Uh huh.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: nanimo,
                            inlineSpan: const TextSpan(
                              text: '何も',
                            ),
                          ),
                          VocabTooltip(
                            message: dekiru,
                            inlineSpan: const TextSpan(
                              text: 'できなくて',
                            ),
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '・・・',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Couldn’t do anything, you know…'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'And it goes on like this, sometimes the other person might break in to say something related to the topic.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can use 「な」 in place of 「ね」 when it sounds too soft and reserved for what you want to say or for the audience you are speaking to. Its rough sound generally applies to the male gender but is not necessarily restricted to only males.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ima,
                            inlineSpan: const TextSpan(
                              text: '今',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: toshokan,
                            inlineSpan: const TextSpan(
                              text: '図書館',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: const TextSpan(
                              text: '行く',
                            ),
                          ),
                          const TextSpan(
                            text: 'んだよ',
                          ),
                          TextSpan(
                            text: 'な',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'You are going to the library now huh? (seeking explanation)'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomoko,
                            inlineSpan: const TextSpan(
                              text: '智子',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: nande,
                            inlineSpan: const TextSpan(
                              text: 'なんで',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Tomoko'),
                    text: Text('Yeah, why?'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text(
                      'ボブ',
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: nihongo,
                            inlineSpan: const TextSpan(
                              text: '日本語',
                            ),
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          VocabTooltip(
                            message: takusan,
                            inlineSpan: const TextSpan(
                              text: 'たくさん',
                            ),
                          ),
                          VocabTooltip(
                            message: benkyou,
                            inlineSpan: const TextSpan(
                              text: '勉強',
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: const TextSpan(
                              text: 'した',
                            ),
                          ),
                          const TextSpan(
                            text: 'けど',
                          ),
                          TextSpan(
                            text: 'な',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                          VocabTooltip(
                            message: mada,
                            inlineSpan: const TextSpan(
                              text: 'まだ',
                            ),
                          ),
                          VocabTooltip(
                            message: zenzen,
                            inlineSpan: const TextSpan(
                              text: '全然',
                            ),
                          ),
                          VocabTooltip(
                            message: wakaru,
                            inlineSpan: const TextSpan(
                              text: 'わからない',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text(
                        'I studied Japanese a lot, right? But, I still don’t get it at all.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: daijoubu,
                            inlineSpan: const TextSpan(
                              text: '大丈夫',
                            ),
                          ),
                          const TextSpan(
                            text: 'よ。',
                          ),
                          VocabTooltip(
                            message: kitto,
                            inlineSpan: const TextSpan(
                              text: 'きっと',
                            ),
                          ),
                          VocabTooltip(
                            message: wakaru,
                            inlineSpan: const TextSpan(
                              text: 'わかる',
                            ),
                          ),
                          VocabTooltip(
                            message: you,
                            inlineSpan: const TextSpan(
                              text: 'よう',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: naru,
                            inlineSpan: const TextSpan(
                              text: 'なる',
                            ),
                          ),
                          const TextSpan(
                            text: 'から',
                          ),
                          TextSpan(
                            text: 'さ',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'No problem. You’ll become able to understand for sure, you know?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text(
                      'ボブ',
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'なら',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(
                            text: 'けど',
                          ),
                          TextSpan(
                            text: 'な',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Bob'),
                    text: Text('If so, it would be good.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The 「な」 sentence-ending particle is often used with the question marker 「か」 to indicate that the speaker is considering something.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: furu,
                              inlineSpan: const TextSpan(
                                text: '降る',
                              ),
                            ),
                            TextSpan(
                              text: 'かな',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I wonder if it’ll rain today.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行ける',
                              ),
                            ),
                            TextSpan(
                              text: 'かな',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I wonder if I can go to a good college.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あのう／あの – say; well; errr うん – yes (casual) この – this （abbr. of これの） 間 【あいだ】 – space (between); time (between); period ディズニーランド – Disney Land 行く 【い・く】 (u-verb) – to go すごい (i-adj) – to a great extent 込む 【こ・む】 (u-verb) – to become crowded 何 【なに／なん】 – what 出来る 【で・き・る】 (ru-verb) – to be able to do 図書館 【と・しょ・かん】 – library 何で 【なん・で】 – why; how 日本語 【に・ほん・ご】 – Japanese (language) たくさん – a lot (amount) 勉強 【べん・きょう】 – study する (exception) – to do まだ – yet 全然 【ぜん・ぜん】 – not at all (when used with negative) 分かる 【わ・かる】 (u-verb) – to understand 大丈夫 【だい・じょう・ぶ】 (na-adj) – ok なる (u-verb) – to become いい (i-adj) – good 今日 【きょう】 – today 雨 【あめ】 – rain 降る 【ふ・る】(u-verb) – to precipitate 大学 【だい・がく】 – college After the 「よ」 and 「ね」, 「さ」 and 「な」 are the next most commonly used sentence-ending particles. 「さ」, which is basically a very casual form of 「よ」, is similar to the English “like” in that some people throw it in at the end of almost every single phrase. Of course, that doesn’t mean it’s necessarily a very sophisticated manner of speech but just like using “like” all the time, I cannot deny that it is an easy habit to fall into. In that sense, due to its over-use, it has almost lost any specific meaning. You may overhear a conversation like the following: Ａ：あのさ・・・ A: Hey… Ｂ：うん。 B: Yeah. Ａ：この間さ・・・ A: This one time… Ｂ：うん。 B: Yeah. Ａ：ディズニーランドに行ったんだけどさ、なんかさ、すごい込んでて・・・ A: I went to Disney Land and it was really crowded… Ｂ：うん。 B: Uh huh. Ａ：何もできなくてさ・・・ A: Couldn’t do anything, you know… And it goes on like this, sometimes the other person might break in to say something related to the topic. You can use 「な」 in place of 「ね」 when it sounds too soft and reserved for what you want to say or for the audience you are speaking to. Its rough sound generally applies to the male gender but is not necessarily restricted to only males. Example 1 洋介：今、図書館に行くんだよな。 Yousuke: You are going to the library now huh? (seeking explanation) 智子：うん、なんで？ Tomoko: Yeah, why? Example 2 ボブ：日本語は、たくさん勉強したけどな。まだ全然わからない。 Bob: I studied Japanese a lot, right? But, I still don’t get it at all. アリス：大丈夫よ。きっとわかるようになるからさ。 Alice: No problem. You’ll become able to understand for sure, you know? ボブ：ならいいけどな。 Bob: If so, it would be good. The 「な」 sentence-ending particle is often used with the question marker 「か」 to indicate that the speaker is considering something. 今日は雨が降るかな？ I wonder if it’ll rain today. いい大学に行けるかな？ I wonder if I can go to a good college.',
      grammars: ['な','さ','かな'],
      keywords: ['na','sa','kana','particle','sentence-ending particle'],
    ),
    Section(
      heading: '「かい」 and 「だい」 sentence-ending particles',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('おい – hey'),
                  Text('どこ – where'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('呼ぶ 【よ・ぶ】 (u-verb) – to call'),
                  Text('いい (i-adj) – good'),
                  Text(
                      '一体 【いったい】 – forms an emphatic question (e.g. “why on earth?”)'),
                  Text('何時 【なん・じ】 – what time'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('つもり – intention, plan'),
                  Text('俺 【おれ】 – me; myself; I (masculine)'),
                  Text('土曜日 【ど・よう・び】 – Saturday'),
                  Text('映画 【えい・が】 – movie'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('一緒 【いっ・しょ】 – together'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「かい」 and 「だい」 are strongly masculine sentence endings for asking questions. 「かい」 is used for yes/no questions while 「だい」 is used for open-ended questions.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: oi,
                              inlineSpan: const TextSpan(
                                text: 'おい',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(
                                text: 'どこ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'ん'),
                            TextSpan(
                              text: 'だい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Hey, where are (you) going?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'さきちゃんって'),
                            VocabTooltip(
                              message: yobu,
                              inlineSpan: const TextSpan(
                                text: '呼んで',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            TextSpan(
                              text: 'かい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Can (I) call you Saki-chan?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ittai,
                              inlineSpan: const TextSpan(
                                text: '一体',
                              ),
                            ),
                            VocabTooltip(
                              message: nanji,
                              inlineSpan: const TextSpan(
                                text: '何時',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰って',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'くる',
                              ),
                            ),
                            VocabTooltip(
                              message: tsumori,
                              inlineSpan: const TextSpan(
                                text: 'つもり',
                              ),
                            ),
                            const TextSpan(text: 'だったん'),
                            TextSpan(
                              text: 'だい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text(
                          'What time were (you) planning on coming home exactly?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ore,
                              inlineSpan: const TextSpan(
                                text: '俺',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: doyoubi,
                              inlineSpan: const TextSpan(
                                text: '土曜日',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'けど、'),
                            VocabTooltip(
                              message: issho,
                              inlineSpan: const TextSpan(
                                text: '一緒',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            TextSpan(
                              text: 'かい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text(
                          'I’m going to see a movie Saturday, go together?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary おい – hey どこ – where 行く 【い・く】 (u-verb) – to go 呼ぶ 【よ・ぶ】 (u-verb) – to call いい (i-adj) – good 一体 【いったい】 – forms an emphatic question (e.g. “why on earth?”) 何時 【なん・じ】 – what time 帰る 【かえ・る】 (u-verb) – to go home つもり – intention, plan 俺 【おれ】 – me; myself; I (masculine) 土曜日 【ど・よう・び】 – Saturday 映画 【えい・が】 – movie 見る 【み・る】 (ru-verb) – to see 一緒 【いっ・しょ】 – together 「かい」 and 「だい」 are strongly masculine sentence endings for asking questions. 「かい」 is used for yes/no questions while 「だい」 is used for open-ended questions. Examples おい、どこに行くんだい？ Hey, where are (you) going? さきちゃんって呼んでもいいかい？ Can (I) call you Saki-chan? 一体何時に帰ってくるつもりだったんだい？ What time were (you) planning on coming home exactly? 俺は土曜日、映画を見に行くけど、一緒に行くかい？ I’m going to see a movie Saturday, go together?',
      grammars: ['かい','だい'],
      keywords: ['kai','dai','particle','question','question particle','sentence-ending particle','casual','masculine'],
    ),
    Section(
      heading: 'Gender-specific sentence-ending particles',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('もう – already'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('おい – hey'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('これ – this'),
                  Text('終わり 【お・わり】 – end'),
                  Text('いい (i-adj) – good'),
                  Text('大学 【だい・がく】 – college'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'These sentence-ending particles are primarily used just to emphasize something and doesn’t really have a meaning per se. However, they can make your statements sound much stronger and/or very gender-specific. Using 「わ」 is just like 「よ」 except it will make you sound very feminine (this is a different sound from the 「わ」 used in Kansai dialect). 「かしら」 is also a very feminine version of 「かな」, which we just went over. 「ぞ」 and 「ぜ」 are identical to 「よ」 except that it makes you sound “cool” and manly, or at least, that is the intent. These examples may not be very helpful without actually hearing what they sound like.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'わ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('There is no more time.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: oi,
                              inlineSpan: const TextSpan(
                                text: 'おい',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            TextSpan(
                              text: 'ぞ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('Hey, we’re going!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: owari,
                              inlineSpan: const TextSpan(
                                text: '終わり',
                              ),
                            ),
                            const TextSpan(text: 'だ'),
                            TextSpan(
                              text: 'ぜ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('With this, it’s over already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ireru,
                              inlineSpan: const TextSpan(
                                text: '入れる',
                              ),
                            ),
                            TextSpan(
                              text: 'かしら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('I wonder if I can enter a good college.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary もう – already 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) おい – hey 行く 【い・く】 (u-verb) – to go これ – this 終わり 【お・わり】 – end いい (i-adj) – good 大学 【だい・がく】 – college 入る 【はい・る】 (u-verb) – to enter These sentence-ending particles are primarily used just to emphasize something and doesn’t really have a meaning per se. However, they can make your statements sound much stronger and/or very gender-specific. Using 「わ」 is just like 「よ」 except it will make you sound very feminine (this is a different sound from the 「わ」 used in Kansai dialect). 「かしら」 is also a very feminine version of 「かな」, which we just went over. 「ぞ」 and 「ぜ」 are identical to 「よ」 except that it makes you sound “cool” and manly, or at least, that is the intent. These examples may not be very helpful without actually hearing what they sound like. もう時間がないわ。 There is no more time. おい、行くぞ！ Hey, we’re going! これで、もう終わりだぜ。 With this, it’s over already. いい大学に入れるかしら？ I wonder if I can enter a good college.',
      grammars: ['わ','ぞ','ぜ','かしら'],
      keywords: ['wa','so','ze','kashira','sentence-ending particle','masculine','feminine'],
    ),
    Section(
      heading: 'That’s a wrap!',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('加賀 【か・が】 – Kaga (last name)'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('ちょっと – a little'),
                  Text('質問 【しつ・もん】 – question'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('いい (i-adj) – good'),
                  Text('はい – yes (polite)'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('何 【なに／なん】 – what'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('そう – (things are) that way'),
                  Text('大体 【だい・たい】 – mostly'),
                  Text('こんにちは – good day'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('ただし – however'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('時 【とき】 – time'),
                  Text('他 【ほか】 – other'),
                  Text('表現 【ひょう・げん】 – expression'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('これ – this'),
                  Text('覚える 【おぼ・える】 (ru-verb) – to memorize'),
                  Text('朝 【あさ】 – morning'),
                  Text('おはよう – good morning'),
                  Text('でも – but'),
                  Text('上 【うえ】 – above'),
                  Text('人 【ひと】 – person'),
                  Text('おはようございます – good morning (polite)'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('間違える 【ま・ちが・える】 (ru-verb) – to make a mistake'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('なる (u-verb) – to become'),
                  Text('洋介 【よう・すけ】 – Yousuke (first name)'),
                  Text('あのう／あの – say; well; errr'),
                  Text('英語 【えい・ご】 – English (language)'),
                  Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                  Text('もらう (u-verb) – to receive'),
                  Text('もし – if by any chance'),
                  Text('時間 【じ・かん】 – time'),
                  Text('うん – yes (casual)'),
                  Text('アメリカ – America'),
                  Text('留学 【りゅう・がく】 – study abroad'),
                  Text('する (exception) – to do'),
                  Text('去年 【きょ・ねん】 – last year'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('お金 【お・かね】 – money'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('いつ – when'),
                  Text('欲しい 【ほ・しい】 (i-adj) – wanted; desirable'),
                  Text('来週 【らい・しゅう】 – next week'),
                  Text('木曜日 【もく・よう・び】 – Thursday'),
                  Text('ありがとう – thank you'),
                  Text('怠ける 【なま・ける】 (ru-verb) – to neglect, to be lazy about'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('そんな – that sort of'),
                  Text('こと – event, matter'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We learned quite a lot of things in this section. Let’s try to put it all together by seeing how different kinds of conjugations are used in different combinations. This is of course by no means an exhaustive list but merely an illustration of how we can use what we learned in various combinations to create a lot of useful expressions.'),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kaga,
                            inlineSpan: const TextSpan(
                              text: '加賀',
                            ),
                          ),
                          VocabTooltip(
                            message: sensei,
                            inlineSpan: const TextSpan(
                              text: '先生',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: chotto,
                            inlineSpan: const TextSpan(
                              text: 'ちょっと',
                            ),
                          ),
                          VocabTooltip(
                            message: shitsumon,
                            inlineSpan: const TextSpan(
                              text: '質問',
                            ),
                          ),
                          const TextSpan(text: 'を'),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: const TextSpan(
                              text: '聞いて',
                            ),
                          ),
                          const TextSpan(text: 'も'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'ですか？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kaga,
                            inlineSpan: const TextSpan(
                              text: '加賀',
                            ),
                          ),
                          VocabTooltip(
                            message: sensei,
                            inlineSpan: const TextSpan(
                              text: '先生',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hai,
                            inlineSpan: const TextSpan(
                              text: 'はい',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'ですよ。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(text: '「Hello」を'),
                          VocabTooltip(
                            message: nihongo,
                            inlineSpan: const TextSpan(
                              text: '日本語',
                            ),
                          ),
                          const TextSpan(text: 'で'),
                          VocabTooltip(
                            message: nan,
                            inlineSpan: TextSpan(
                              text: '何',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: '言えば',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'ですか。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: nan,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言えば',
                    ),
                  ),
                  const TextSpan(
                      text: '」 = quoted sub-clause + if conditional of '),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kaga,
                            inlineSpan: const TextSpan(
                              text: '加賀',
                            ),
                          ),
                          VocabTooltip(
                            message: sensei,
                            inlineSpan: const TextSpan(
                              text: '先生',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(text: 'ね。'),
                          VocabTooltip(
                            message: daitai,
                            inlineSpan: const TextSpan(
                              text: '大体',
                            ),
                          ),
                          const TextSpan(text: '、「'),
                          VocabTooltip(
                            message: konnichiha,
                            inlineSpan: const TextSpan(
                              text: 'こんにちは',
                            ),
                          ),
                          const TextSpan(text: '」'),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: TextSpan(
                              text: '言う',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: omou,
                            inlineSpan: TextSpan(
                              text: '思います',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(text: 'よ。'),
                          VocabTooltip(
                            message: tadashi,
                            inlineSpan: const TextSpan(
                              text: 'ただし',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: kaku,
                            inlineSpan: const TextSpan(
                              text: '書く',
                            ),
                          ),
                          VocabTooltip(
                            message: toki,
                            inlineSpan: const TextSpan(
                              text: '時',
                            ),
                          ),
                          const TextSpan(text: 'は「'),
                          VocabTooltip(
                            message: konnichiwa,
                            inlineSpan: const TextSpan(
                              text: 'こんにちわ',
                            ),
                          ),
                          const TextSpan(text: '」'),
                          TextSpan(
                            text: 'じゃなくて',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(text: '、「'),
                          VocabTooltip(
                            message: konnichiha,
                            inlineSpan: const TextSpan(
                              text: 'こんにちは',
                            ),
                          ),
                          const TextSpan(text: '」と'),
                          VocabTooltip(
                            message: kaku,
                            inlineSpan: const TextSpan(
                              text: '書かなくて',
                            ),
                          ),
                          const TextSpan(text: 'は'),
                          VocabTooltip(
                            message: naru,
                            inlineSpan: const TextSpan(
                              text: 'なりません',
                            ),
                          ),
                          const TextSpan(text: '。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「と',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思います',
                    ),
                  ),
                  const TextSpan(
                      text: '」 = quoted sub-clause + quoted sub-clause'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: '「じゃなくて」 = negative sequence of states'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(text: 'ですか。'),
                          VocabTooltip(
                            message: hoka,
                            inlineSpan: const TextSpan(
                              text: '他',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: nanika,
                            inlineSpan: const TextSpan(
                              text: '何か',
                            ),
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          VocabTooltip(
                            message: hyougen,
                            inlineSpan: const TextSpan(
                              text: '表現',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'あります',
                            ),
                          ),
                          const TextSpan(text: 'か。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kaga,
                            inlineSpan: const TextSpan(
                              text: '加賀',
                            ),
                          ),
                          VocabTooltip(
                            message: sensei,
                            inlineSpan: const TextSpan(
                              text: '先生',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kore,
                            inlineSpan: const TextSpan(
                              text: 'これ',
                            ),
                          ),
                          const TextSpan(text: 'も'),
                          VocabTooltip(
                            message: oboeru,
                            inlineSpan: const TextSpan(
                              text: '覚え',
                            ),
                          ),
                          TextSpan(
                            text: 'といて',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(text: 'ね。'),
                          VocabTooltip(
                            message: asa,
                            inlineSpan: const TextSpan(
                              text: '朝',
                            ),
                          ),
                          const TextSpan(text: 'は、「'),
                          VocabTooltip(
                            message: ohayou,
                            inlineSpan: const TextSpan(
                              text: 'おはよう',
                            ),
                          ),
                          const TextSpan(text: '」と'),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: const TextSpan(
                              text: '言う',
                            ),
                          ),
                          const TextSpan(text: 'の。'),
                          VocabTooltip(
                            message: demo,
                            inlineSpan: const TextSpan(
                              text: 'でも',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: ue,
                            inlineSpan: const TextSpan(
                              text: '上',
                            ),
                          ),
                          const TextSpan(text: 'の'),
                          VocabTooltip(
                            message: hito,
                            inlineSpan: const TextSpan(
                              text: '人',
                            ),
                          ),
                          const TextSpan(text: 'には「'),
                          VocabTooltip(
                            message: ohayougozaimasu,
                            inlineSpan: const TextSpan(
                              text: 'おはようございます',
                            ),
                          ),
                          const TextSpan(text: '」と'),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: const TextSpan(
                              text: '言って',
                            ),
                          ),
                          const TextSpan(text: 'ください。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: oboeru,
                    inlineSpan: const TextSpan(
                      text: '覚え',
                    ),
                  ),
                  const TextSpan(
                    text: 'といて」 – ',
                  ),
                  VocabTooltip(
                    message: oboeru,
                    inlineSpan: const TextSpan(
                      text: '覚える',
                    ),
                  ),
                  const TextSpan(
                      text:
                          ' + abbreviated form of ～ておく + casual ～てください with ください dropped'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hai,
                            inlineSpan: const TextSpan(
                              text: 'はい',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: wakaru,
                            inlineSpan: const TextSpan(
                              text: '分かりました',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                          VocabTooltip(
                            message: machigaeru,
                            inlineSpan: const TextSpan(
                              text: '間違えない',
                            ),
                          ),
                          VocabTooltip(
                            message: you,
                            inlineSpan: const TextSpan(
                              text: 'よう',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: const TextSpan(
                              text: 'します',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          VocabTooltip(
                            message: benkyou,
                            inlineSpan: const TextSpan(
                              text: '勉強',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: naru,
                            inlineSpan: const TextSpan(
                              text: 'なりました',
                            ),
                          ),
                          const TextSpan(text: '！'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const Heading(text: 'Literal translation of Example 1', level: 2),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Kaga-sensei, is it ok to ask you a question?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text('Yes, it’s ok.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'If you say what for “hello” in Japanese, is it ok?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text(
                        'Well, mostly, I think people say “konnichiwa”. Only, when you write it, you must write “konnichiha” and not “konnichiwa”.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Is that so? Are there any other good expressions?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text(
                        'Please memorize this too (in preparation for the future). In the morning, everybody says, “ohayou”. But, please say, “ohayou-gozaimasu” to a higher person.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Yes, I understood. I’ll do in the manner of not making mistake. It became good study!'),
                    english: true,
                  ),
                ],
              ),
              const Heading(
                  text: 'Interpretative translation of Example 1', level: 2),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Kaga-sensei, is it ok to ask you a question?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text('Sure.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('How do you say “Hello” in Japanese?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text(
                        'Well, most of the time, I think people say “konnichiwa”. Only, when you write it, you must write “konnichiha” and not “konnichiwa”.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Is that so? Are there any other good expressions?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Kaga-sensei'),
                    text: Text(
                        'You should know this too. In the morning, everybody says, “ohayou”. But, please say, “ohayou-gozaimasu” to a higher person.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Ok, I got it. I’ll try not to make that mistake. That was very informative!'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(text: 'お！アリスだ。'),
                          VocabTooltip(
                            message: anoHey,
                            inlineSpan: const TextSpan(
                              text: 'あの',
                            ),
                          ),
                          const TextSpan(text: 'ね、'),
                          VocabTooltip(
                            message: shitsumon,
                            inlineSpan: const TextSpan(
                              text: '質問',
                            ),
                          ),
                          const TextSpan(text: 'を'),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: const TextSpan(
                              text: '聞いて',
                            ),
                          ),
                          const TextSpan(text: 'も'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: '？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: nani,
                            inlineSpan: const TextSpan(
                              text: '何',
                            ),
                          ),
                          const TextSpan(text: '？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: chotto,
                            inlineSpan: const TextSpan(
                              text: 'ちょっと',
                            ),
                          ),
                          VocabTooltip(
                            message: eigo,
                            inlineSpan: const TextSpan(
                              text: '英語',
                            ),
                          ),
                          const TextSpan(text: 'を'),
                          VocabTooltip(
                            message: oshieru,
                            inlineSpan: TextSpan(
                              text: '教えて',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: morau,
                            inlineSpan: TextSpan(
                              text: 'もらいたい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(text: 'んだけどさ、'),
                          VocabTooltip(
                            message: moshi,
                            inlineSpan: const TextSpan(
                              text: 'もし',
                            ),
                          ),
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(
                              text: '時間',
                            ),
                          ),
                          const TextSpan(text: 'が'),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'あれば',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: oshieru,
                            inlineSpan: const TextSpan(
                              text: '教えて',
                            ),
                          ),
                          VocabTooltip(
                            message: kureru,
                            inlineSpan: const TextSpan(
                              text: 'くれない',
                            ),
                          ),
                          const TextSpan(text: '？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: oshieru,
                    inlineSpan: const TextSpan(
                      text: '教えて',
                    ),
                  ),
                  VocabTooltip(
                    message: morau,
                    inlineSpan: const TextSpan(
                      text: 'もらいたい',
                    ),
                  ),
                  const TextSpan(text: '」 = receiving favor + to want （たい）'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(text: 'え？'),
                          VocabTooltip(
                            message: eigo,
                            inlineSpan: const TextSpan(
                              text: '英語',
                            ),
                          ),
                          const TextSpan(text: 'を'),
                          VocabTooltip(
                            message: benkyou,
                            inlineSpan: const TextSpan(
                              text: '勉強',
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: const TextSpan(
                              text: 'する',
                            ),
                          ),
                          const TextSpan(text: 'の？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: amerika,
                            inlineSpan: const TextSpan(
                              text: 'アメリカ',
                            ),
                          ),
                          const TextSpan(text: 'で'),
                          VocabTooltip(
                            message: ryuugaku,
                            inlineSpan: const TextSpan(
                              text: '留学',
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'して',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'みたいなと',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: omou,
                            inlineSpan: TextSpan(
                              text: '思って',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(text: 'ね。'),
                          VocabTooltip(
                            message: kyonen,
                            inlineSpan: const TextSpan(
                              text: '去年',
                            ),
                          ),
                          const TextSpan(text: 'も'),
                          VocabTooltip(
                            message: iku,
                            inlineSpan: TextSpan(
                              text: '行こう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'と',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'した',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(text: 'けど、'),
                          VocabTooltip(
                            message: okane,
                            inlineSpan: const TextSpan(
                              text: 'お金',
                            ),
                          ),
                          const TextSpan(text: 'が'),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'なくて',
                            ),
                          ),
                          const TextSpan(text: '・・・'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'して',
                    ),
                  ),
                  const TextSpan(
                    text: 'みたいなと',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思って',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 = to try something out （～てみる） + want to （たい） + な sentence-ending particle + quoted subquote + te-form of '),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行こう',
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'した',
                    ),
                  ),
                  const TextSpan(text: '」 = volitional of '),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                    text: ' + to attempt （と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '）',
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(text: 'なの？'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'よ。'),
                          VocabTooltip(
                            message: itsu,
                            inlineSpan: const TextSpan(
                              text: 'いつ',
                            ),
                          ),
                          VocabTooltip(
                            message: oshieru,
                            inlineSpan: const TextSpan(
                              text: '教えて',
                            ),
                          ),
                          VocabTooltip(
                            message: hoshii,
                            inlineSpan: const TextSpan(
                              text: 'ほしい',
                            ),
                          ),
                          const TextSpan(text: 'の？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: itsu,
                            inlineSpan: const TextSpan(
                              text: 'いつ',
                            ),
                          ),
                          const TextSpan(text: 'でも'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'よ。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(text: 'じゃ、'),
                          VocabTooltip(
                            message: raishuu,
                            inlineSpan: const TextSpan(
                              text: '来週',
                            ),
                          ),
                          const TextSpan(text: 'の'),
                          VocabTooltip(
                            message: mokuyoubi,
                            inlineSpan: const TextSpan(
                              text: '木曜日',
                            ),
                          ),
                          const TextSpan(text: 'からは'),
                          VocabTooltip(
                            message: dou,
                            inlineSpan: const TextSpan(
                              text: 'どう',
                            ),
                          ),
                          const TextSpan(text: '？'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(text: '、'),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(text: 'よ。'),
                          VocabTooltip(
                            message: arigatou,
                            inlineSpan: const TextSpan(
                              text: 'ありがとう',
                            ),
                          ),
                          const TextSpan(text: '！'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: benkyou,
                            inlineSpan: const TextSpan(
                              text: '勉強',
                            ),
                          ),
                          const TextSpan(text: 'を'),
                          VocabTooltip(
                            message: namakeru,
                            inlineSpan: TextSpan(
                              text: '怠けた',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'り、',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: kuru,
                            inlineSpan: TextSpan(
                              text: '来なかった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'り、',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'しない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          TextSpan(
                            text: 'で',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(text: 'ね。'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: namakeru,
                    inlineSpan: const TextSpan(
                      text: '怠けた',
                    ),
                  ),
                  const TextSpan(
                    text: 'り',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来なかった',
                    ),
                  ),
                  const TextSpan(
                    text: 'り',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'しない',
                    ),
                  ),
                  const TextSpan(text: '」 = List of actions （～たり'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '） + negative request of ',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: yousuke,
                            inlineSpan: const TextSpan(
                              text: '洋介',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sonna,
                            inlineSpan: const TextSpan(
                              text: 'そんな',
                            ),
                          ),
                          VocabTooltip(
                            message: koto,
                            inlineSpan: const TextSpan(
                              text: 'こと',
                            ),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: const TextSpan(
                              text: 'しない',
                            ),
                          ),
                          const TextSpan(text: 'よ！'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const Heading(text: 'Literal translation of Example 2', level: 2),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'Oh! It’s Alice. Hey, is it ok to ask a question?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('What?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'I want to receive the favor of you teaching English and if, by any chance, you have time, will you give the favor of teaching?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Huh? You are going to study English?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'Yeah, I was thinking that I want to try studying abroad in America. I tried to make motion toward going last year too but, without money…'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Is that so? It’s good. When do you want me to teach you?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('Anytime is good.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Then what about from next week Thursday?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('Yeah, ok. Thanks!'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Don’t do things like shirk on your studies or not come, ok?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('I won’t do anything like that!'),
                    english: true,
                  ),
                ],
              ),
              const Heading(
                  text: 'Interpretative translation of Example 2', level: 2),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text:
                        Text('Oh! It’s Alice. Hey, can I ask you a question?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('What’s up?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'I want to learn English so if you have time, can you teach me?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Huh? You’re going to study English?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text(
                        'Yeah, I was thinking about studying abroad in America. I tried going last year too but I didn’t have the money.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'Really? No problem. When do you want me to teach you?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('Anytime is fine.'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('What about from next week Thursday then?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('OK, thanks!'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Alice'),
                    text: Text(
                        'You’re not going to shirk on your studies or not come or anything right?'),
                    english: true,
                  ),
                ],
              ),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Yousuke'),
                    text: Text('I won’t do anything like that!'),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 加賀 【か・が】 – Kaga (last name) 先生 【せん・せい】 – teacher ちょっと – a little 質問 【しつ・もん】 – question 聞く 【き・く】 (u-verb) – to ask; to listen いい (i-adj) – good はい – yes (polite) 日本語 【に・ほん・ご】 – Japanese (language) 何 【なに／なん】 – what 言う 【い・う】 (u-verb) – to say そう – (things are) that way 大体 【だい・たい】 – mostly こんにちは – good day 思う 【おも・う】 (u-verb) – to think ただし – however 書く 【か・く】 (u-verb) – to write 時 【とき】 – time 他 【ほか】 – other 表現 【ひょう・げん】 – expression ある (u-verb) – to exist (inanimate) これ – this 覚える 【おぼ・える】 (ru-verb) – to memorize 朝 【あさ】 – morning おはよう – good morning でも – but 上 【うえ】 – above 人 【ひと】 – person おはようございます – good morning (polite) 分かる 【わ・かる】 (u-verb) – to understand 間違える 【ま・ちが・える】 (ru-verb) – to make a mistake 勉強 【べん・きょう】 – study なる (u-verb) – to become 洋介 【よう・すけ】 – Yousuke (first name) あのう／あの – say; well; errr 英語 【えい・ご】 – English (language) 教える 【おし・える】 (ru-verb) – to teach; to inform もらう (u-verb) – to receive もし – if by any chance 時間 【じ・かん】 – time うん – yes (casual) アメリカ – America 留学 【りゅう・がく】 – study abroad する (exception) – to do 去年 【きょ・ねん】 – last year 行く 【い・く】 (u-verb) – to go お金 【お・かね】 – money ある (u-verb) – to exist (inanimate) いつ – when 欲しい 【ほ・しい】 (i-adj) – wanted; desirable 来週 【らい・しゅう】 – next week 木曜日 【もく・よう・び】 – Thursday ありがとう – thank you 怠ける 【なま・ける】 (ru-verb) – to neglect, to be lazy about 来る 【く・る】 (exception) – to come そんな – that sort of こと – event, matter We learned quite a lot of things in this section. Let’s try to put it all together by seeing how different kinds of conjugations are used in different combinations. This is of course by no means an exhaustive list but merely an illustration of how we can use what we learned in various combinations to create a lot of useful expressions. Example 1 アリス：加賀先生、ちょっと質問を聞いてもいいですか？ 加賀先生：はい、いいですよ。 アリス：「Hello」を日本語で何と言えばいいですか。 「何と言えば」 = quoted sub-clause + if conditional of 言う 加賀先生：そうね。大体、「こんにちは」と言うと思いますよ。ただし、書く時は「こんにちわ」じゃなくて、「こんにちは」と書かなくてはなりません。 「と言うと思います」 = quoted sub-clause + quoted sub-clause 「じゃなくて」 = negative sequence of states アリス：そうですか。他に何かいい表現はありますか。 加賀先生：これも覚えといてね。朝は、「おはよう」と言うの。でも、上の人には「おはようございます」と言ってください。 「覚えといて」 – 覚える + abbreviated form of ～ておく + casual ～てください with ください dropped アリス：はい、分かりました。間違えないようにします。いい勉強になりました！ Literal translation of Example 1 Alice: Kaga-sensei, is it ok to ask you a question? Kaga-sensei: Yes, it’s ok. Alice: If you say what for “hello” in Japanese, is it ok? Kaga-sensei: Well, mostly, I think people say “konnichiwa”. Only, when you write it, you must write “konnichiha” and not “konnichiwa”. Alice: Is that so? Are there any other good expressions? Kaga-sensei: Please memorize this too (in preparation for the future). In the morning, everybody says, “ohayou”. But, please say, “ohayou-gozaimasu” to a higher person. Alice: Yes, I understood. I’ll do in the manner of not making mistake. It became good study! Interpretative translation of Example 1 Alice: Kaga-sensei, is it ok to ask you a question? Kaga-sensei: Sure. Alice: How do you say “Hello” in Japanese? Kaga-sensei: Well, most of the time, I think people say “konnichiwa”. Only, when you write it, you must write “konnichiha” and not “konnichiwa”. Alice: Is that so? Are there any other good expressions? Kaga-sensei: You should know this too. In the morning, everybody says, “ohayou”. But, please say, “ohayou-gozaimasu” to a higher person. Alice: Ok, I got it. I’ll try not to make that mistake. That was very informative! Example 2 洋介：お！ アリスだ。あのね、質問を聞いてもいい？ アリス：何？ 洋介：ちょっと英語を教えてもらいたいんだけどさ、もし時間があれば、教えてくれない？ 「教えてもらいたい」 = receiving favor + to want （たい） アリス：え？ 英語を勉強するの？ 洋介：うん、アメリカで留学してみたいなと思ってね。去年も行こうとしたけど、お金がなくて・・・ 「してみたいなと思って」 = to try something out （～てみる） + want to （たい） + な sentence-ending particle + quoted subquote + te-form of 思う 「行こうとした」 = volitional of 行く + to attempt （とする） アリス：そうなの？ いいよ。いつ教えてほしいの？ 洋介：いつでもいいよ。アリス：じゃ、来週の木曜日からはどう？ 洋介：うん、いいよ。ありがとう！ アリス：勉強を怠けたり、来なかったり、しないでね。 「怠けたり来なかったりしない」 = List of actions （～たりする） + negative request of する. 洋介：そんなことしないよ！ Literal translation of Example 2 Yousuke: Oh! It’s Alice. Hey, is it ok to ask a question? Alice: What? Yousuke: I want to receive the favor of you teaching English and if, by any chance, you have time, will you give the favor of teaching? Alice: Huh? You are going to study English? Yousuke: Yeah, I was thinking that I want to try studying abroad in America. I tried to make motion toward going last year too but, without money… Alice: Is that so? It’s good. When do you want me to teach you? Yousuke: Anytime is good. Alice: Then what about from next week Thursday? Yousuke: Yeah, ok. Thanks! Alice: Don’t do things like shirk on your studies or not come, ok? Yousuke: I won’t do anything like that! Interpretative translation of Example 2 Yousuke: Oh! It’s Alice. Hey, can I ask you a question? Alice: What’s up? Yousuke: I want to learn English so if you have time, can you teach me? Alice: Huh? You’re going to study English? Yousuke: Yeah, I was thinking about studying abroad in America. I tried going last year too but I didn’t have the money. Alice: Really? No problem. When do you want me to teach you? Yousuke: Anytime is fine. Alice: What about from next week Thursday then? Yousuke: OK, thanks! Alice: You’re not going to shirk on your studies or not come or anything right? Yousuke: I won’t do anything like that!',
      grammars: [],
      keywords: ['dialog','conversation','practice'],
    ),
  ],
);
