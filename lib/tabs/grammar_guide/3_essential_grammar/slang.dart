import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson slang = Lesson(
  title: 'Casual Patterns and Slang',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'So far, for every grammar we have covered, we also went over all the casual variations as well. However, even though we have already covered all the casual forms, truly mastering casual speech in Japanese requires far more than just learning the various casual forms. There are countless numbers of ways in which wordings and pronunciations change as well as differences between male and female speech. Understanding slang also requires knowing various vocabulary that is also growing with every new generation. Many adults would be hard-pressed to understand the kind of slang being used by kids today.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'While comprehensively covering slang and relevant vocabulary would require a book in and of itself (a book that would soon become out of date), I’ll instead cover some broad patterns and common phenomenon which will at least help you get started in understanding the most common aspects of Japanese slang. There is no particular order in the material presented here and I expect this page to grow continuously as I find different things to cover.'),
                ],
              ),
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Please note that slang is also heavily influenced by local dialects. Although all the material presented here is valid for the greater Tokyo area, your mileage may vary depending on where you are located.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'So far, for every grammar we have covered, we also went over all the casual variations as well. However, even though we have already covered all the casual forms, truly mastering casual speech in Japanese requires far more than just learning the various casual forms. There are countless numbers of ways in which wordings and pronunciations change as well as differences between male and female speech. Understanding slang also requires knowing various vocabulary that is also growing with every new generation. Many adults would be hard-pressed to understand the kind of slang being used by kids today. While comprehensively covering slang and relevant vocabulary would require a book in and of itself (a book that would soon become out of date), I’ll instead cover some broad patterns and common phenomenon which will at least help you get started in understanding the most common aspects of Japanese slang. There is no particular order in the material presented here and I expect this page to grow continuously as I find different things to cover. Please note that slang is also heavily influenced by local dialects. Although all the material presented here is valid for the greater Tokyo area, your mileage may vary depending on where you are located.',
      grammars: [],
      keywords: ['slang','casual','casual pattern'],
    ),
    Section(
      heading: 'Basic principles of slang',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ここ – here'),
                  Text('つまらない (i-adj) – boring'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text(
                      'まったく – entirely; indeed; good grief (expression of exasperation)'),
                  Text('いつ – when'),
                  Text('こんな – this sort of'),
                  Text('所 【ところ】 – place'),
                  Text('ぐずぐず – tardily; hesitatingly'),
                  Text('する (exception) – to do'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In the world of slang, anything goes and rules that apply to written Japanese are often broken. The most difficult part is that, of course, you can’t just say whatever you want. When you break the rules, you have to break it the correct way. Taking what you learned from textbooks or Japanese classes and applying it to the real world is not so easy because it is impossible to teach all the possible ways things can get jumbled up in the spoken language. Learning how to speak naturally with all the correct idiosyncrasies and inconsistencies in a language is something that requires practice with real people in real-world situations. In this section, we’ll look at some common patterns and themes that will at least help you get an idea of where the majority of slang originates from.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'One thing you’ll soon realize when you first start talking to Japanese people in real life is that many sounds are slurred together. This is especially true for males. The fact is voices in instructional material such as language tapes often exaggerate the pronunciation of each letter in order to make aural comprehension easier. In reality, not all the sounds are pronounced as clearly as it should be and things end up sounding different from how it’s written on paper.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'There is one major driving factor behind the majority of slang in Japanese. The primary goal of most slang is to make things easier to say. In other words, the goal is to reduce or simplify the movement of your mouth. There are two primary ways in which this is accomplished, 1) By making things shorter or, 2) By slurring the sounds together. We have already seen many examples of the first method such as shortening 「かもしれない」 to 「かも」 or preferring 「と」 to the longer conditional forms. The second method makes things easier to say usually by substituting parts of words with sounds that fit better with the sounds surrounding it or by merging two or more sounds together. For example, the same 「かもしれない」 might be pronounced 「かもしんない」 since 「しん」 requires less movement than 「しれ」.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The fundamental goal of slang is to reduce mouth movement.',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Let’s see some more examples of words that get shortened or slurred. Try saying both versions to get a feel for how the slang saves space and some calories for your mouth.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: tsumaranai,
                              inlineSpan: const TextSpan(
                                text: 'つまらない',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行こう',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            TextSpan(
                              text: 'つまんない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'んち',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行こう',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mattaku,
                              inlineSpan: const TextSpan(
                                text: 'まったく',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: itsu,
                              inlineSpan: const TextSpan(
                                text: 'いつ',
                              ),
                            ),
                            const TextSpan(
                              text: 'まで',
                            ),
                            VocabTooltip(
                              message: konna,
                              inlineSpan: const TextSpan(
                                text: 'こんな',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: const TextSpan(
                                text: 'ところ',
                              ),
                            ),
                            const TextSpan(
                              text: 'で、',
                            ),
                            VocabTooltip(
                              message: guzuguzu,
                              inlineSpan: const TextSpan(
                                text: 'ぐずぐず',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだよ。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'ったく',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: itsu,
                              inlineSpan: const TextSpan(
                                text: 'いつ',
                              ),
                            ),
                            const TextSpan(
                              text: 'まで',
                            ),
                            VocabTooltip(
                              message: konna,
                              inlineSpan: const TextSpan(
                                text: 'こんな',
                              ),
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'とこ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'で、',
                            ),
                            VocabTooltip(
                              message: guzuguzu,
                              inlineSpan: const TextSpan(
                                text: 'ぐずぐず',
                              ),
                            ),
                            TextSpan(
                              text: 'すんだ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You’ll see that a great deal of slang in Japanese stems from this single principle of making things easier to say. It’s very natural because it’s guided by how your mouth moves. With a fair amount of practice, you should be able to naturally pick up shorter, alternative pronunciations and incorporate them into your own speech.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ここ – here つまらない (i-adj) – boring 私 【わたし】 – me; myself; I 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 行く 【い・く】 (u-verb) – to go まったく – entirely; indeed; good grief (expression of exasperation) いつ – when こんな – this sort of 所 【ところ】 – place ぐずぐず – tardily; hesitatingly する (exception) – to do In the world of slang, anything goes and rules that apply to written Japanese are often broken. The most difficult part is that, of course, you can’t just say whatever you want. When you break the rules, you have to break it the correct way. Taking what you learned from textbooks or Japanese classes and applying it to the real world is not so easy because it is impossible to teach all the possible ways things can get jumbled up in the spoken language. Learning how to speak naturally with all the correct idiosyncrasies and inconsistencies in a language is something that requires practice with real people in real-world situations. In this section, we’ll look at some common patterns and themes that will at least help you get an idea of where the majority of slang originates from. One thing you’ll soon realize when you first start talking to Japanese people in real life is that many sounds are slurred together. This is especially true for males. The fact is voices in instructional material such as language tapes often exaggerate the pronunciation of each letter in order to make aural comprehension easier. In reality, not all the sounds are pronounced as clearly as it should be and things end up sounding different from how it’s written on paper. There is one major driving factor behind the majority of slang in Japanese. The primary goal of most slang is to make things easier to say. In other words, the goal is to reduce or simplify the movement of your mouth. There are two primary ways in which this is accomplished, 1) By making things shorter or, 2) By slurring the sounds together. We have already seen many examples of the first method such as shortening 「かもしれない」 to 「かも」 or preferring 「と」 to the longer conditional forms. The second method makes things easier to say usually by substituting parts of words with sounds that fit better with the sounds surrounding it or by merging two or more sounds together. For example, the same 「かもしれない」 might be pronounced 「かもしんない」 since 「しん」 requires less movement than 「しれ」. The fundamental goal of slang is to reduce mouth movement. Let’s see some more examples of words that get shortened or slurred. Try saying both versions to get a feel for how the slang saves space and some calories for your mouth. Examples ここはつまらないから私の家に行こう。 ここつまんないから、私んち行こう。 まったく、いつまでこんなところで、ぐずぐずするんだよ。 ったく、いつまでこんな私とこで、ぐずぐずすんだよ。 You’ll see that a great deal of slang in Japanese stems from this single principle of making things easier to say. It’s very natural because it’s guided by how your mouth moves. With a fair amount of practice, you should be able to naturally pick up shorter, alternative pronunciations and incorporate them into your own speech.',
      grammars: [],
      keywords: ['slang','casual'],
    ),
    Section(
      heading: 'Sentence ordering and particles (or the lack thereof)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('それ – that'),
                  Text('何 【なに／なん】 – what'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('あの – that (over there) （abbr. of あれの）'),
                  Text('人 【ひと】 – person'),
                  Text('もう – already'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('昨日【きのう】 – yesterday'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('アイス – ice (short for ice cream)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'While written Japanese already has fairly loose rules regarding sentence order, casual spoken Japanese takes it one step further. A complete sentence requires a verb at the end of the sentence to complete the thought. However, we’ll see how this rule is bent in casual conversations.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Conversations are sporadic and chaotic in any language and it’s common for people to say the first thing that pops into their head without thinking out the whole proper sentence.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'For example, if you wanted to ask what something was, the normal, proper way would be to ask, 「'),
                  VocabTooltip(
                    message: sore,
                    inlineSpan: const TextSpan(
                      text: 'それ',
                    ),
                  ),
                  const TextSpan(text: 'は'),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '？」 However, if the first thing that popped into your head, “What the?” then it would be more natural to say 「'),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(text: '」 first. However, since 「'),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(text: 'は'),
                  VocabTooltip(
                    message: sore,
                    inlineSpan: const TextSpan(
                      text: 'それ',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '？」 doesn’t make any sense (Is what that?), you can simply break it up into what are essentially two sentence fragments asking “what” first （'),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '？）, and then following it up with the explanation of what you were talking about （「'),
                  VocabTooltip(
                    message: sore,
                    inlineSpan: const TextSpan(
                      text: 'それ',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 in this case）. For the sake of convenience, this is lumped into what looks like one sentence.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('What is that?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('What? That. (Two sentences lumped into one)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Sometimes, the first thing that pops into your head might be main verb. But if the main verb has already slipped out of your mouth, you’re now left with the rest of the sentence without a verb to complete the thought. In conversational Japanese, it’s perfectly acceptable to have the verb come first using the same technique we just saw by breaking them into two sentences. The second sentence is incomplete of course, but that kind of thing is common in the speech of any language.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            const TextSpan(text: '？'),
                            VocabTooltip(
                              message: ano,
                              inlineSpan: const TextSpan(
                                text: 'あの',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Did you see? That guy?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べた',
                              ),
                            ),
                            const TextSpan(text: '？'),
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買った',
                              ),
                            ),
                            VocabTooltip(
                              message: aisu,
                              inlineSpan: const TextSpan(
                                text: 'アイス',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'You ate it already? The ice cream I bought yesterday.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary それ – that 何 【なに／なん】 – what 見る 【み・る】 (ru-verb) – to see あの – that (over there) （abbr. of あれの） 人 【ひと】 – person もう – already 食べる 【たべ・る】 (ru-verb) – to eat 昨日【きのう】 – yesterday 買う 【か・う】 (u-verb) – to buy アイス – ice (short for ice cream) While written Japanese already has fairly loose rules regarding sentence order, casual spoken Japanese takes it one step further. A complete sentence requires a verb at the end of the sentence to complete the thought. However, we’ll see how this rule is bent in casual conversations. Conversations are sporadic and chaotic in any language and it’s common for people to say the first thing that pops into their head without thinking out the whole proper sentence. For example, if you wanted to ask what something was, the normal, proper way would be to ask, 「それは何？」 However, if the first thing that popped into your head, “What the?” then it would be more natural to say 「何」 first. However, since 「何はそれ？」 doesn’t make any sense (Is what that?), you can simply break it up into what are essentially two sentence fragments asking “what” first （何？）, and then following it up with the explanation of what you were talking about （「それ」 in this case）. For the sake of convenience, this is lumped into what looks like one sentence. Examples それは何？ What is that? 何それ？ What? That. (Two sentences lumped into one) Sometimes, the first thing that pops into your head might be main verb. But if the main verb has already slipped out of your mouth, you’re now left with the rest of the sentence without a verb to complete the thought. In conversational Japanese, it’s perfectly acceptable to have the verb come first using the same technique we just saw by breaking them into two sentences. The second sentence is incomplete of course, but that kind of thing is common in the speech of any language. 見た？ あの人？ Did you see? That guy? もう食べた？ 昨日買ったアイス。 You ate it already? The ice cream I bought yesterday.',
      grammars: [],
      keywords: ['particle','slang'],
    ),
    Section(
      heading: 'Using 「じゃん」 instead of 「じゃない」 to confirm',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('サラリーマン – office worker (salary man)'),
                  Text('残業 【ざん・ぎょう】 – overtime'),
                  Text('たくさん – a lot (amount)'),
                  Text('する (exception) – to do'),
                  Text('まあ – well'),
                  Text('いい (i-adj) – good'),
                  Text('ほら – look'),
                  Text('やはり／やっぱり – as I thought'),
                  Text('レポート – report'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('駄目 【だめ】 – no good'),
                  Text('誰 【だれ】 – who'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('ここ – here'),
                  Text('着替える 【きが・える】 (ru-verb) – to change clothes'),
                  Text('～君 【～くん】 – name suffix'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('やはり／やっぱり／やっぱ – as I thought'),
                  Text('駅 【えき】 – station'),
                  Text('近い 【ちか・い】 (i-adj) – close, near'),
                  Text('カラオケ – karaoke'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('うん – yes (casual)'),
                  Text('あそこ – over there'),
                  Text('すぐ – soon; nearby'),
                  Text('隣 【となり】 – next to'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「じゃん」 is an abbreviation of 「じゃない」, the negative conjugation for nouns and na-adjectives. However, this only applies to 「じゃない」 used in the following fashion.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sarariiman,
                              inlineSpan: const TextSpan(
                                text: 'サラリーマン',
                              ),
                            ),
                            const TextSpan(text: 'だから、'),
                            VocabTooltip(
                              message: zangyou,
                              inlineSpan: const TextSpan(
                                text: '残業',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: 'ん'),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text(
                          'Because he’s a salaryman, doesn’t he do a lot of overtime?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The important thing to note about the example above is that 「じゃない」 here is actually confirming the positive. In fact, a closer translation is, “Because he’s a salaryman, he '),
                  TextSpan(
                    text: 'probably does',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' a lot of overtime.” But it’s still a question so there’s a slight nuance that you are seeking confirmation even though you are relatively sure.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「じゃん」 is a shorter slang for expressing the same type of thing except it doesn’t even bother to ask a question to confirm. It’s completely affirmative in tone.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In fact, the closest equivalent to 「じゃん」 is 「じゃない」 used in the following fashion.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: maa,
                              inlineSpan: const TextSpan(
                                text: 'まあ',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(
                              text: 'じゃない',
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Well, it’s probably fine (don’t you think?).'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'This type of expression is the '),
                  TextSpan(
                    text: 'only',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' case where you can attach 「じゃない」 directly to i-adjectives and verbs. Once you actually hear this expression in real life, you’ll see that it has a distinct pronunciation that is different from simply using the negative. Plus, you have to realize that this type of 「じゃない」 sounds rather mature and feminine, unlike 「じゃん」, which is gender-neutral.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Like the above, specialized use of 「じゃない」, you can also attach 「じゃん」 directly to verbs and i-adjectives as well as the usual nouns and na-adjectives. Because slang is usually created to make things easier, it’s not surprising that the rules for using 「じゃん」 are so lax and easy.'),
                ],
              ),
              const NotesSection(
                heading: 'Summary',
                content: NumberedList(
                  items: [
                    Text(
                        'Though derived from 「じゃない」, 「じゃん」 is always used to confirm the positive.'),
                    Text(
                        'It can be attached to the end of any sentence regardless of whether it ends in a noun, adjective, verb, or adverb.'),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Finally, let’s get to the examples. Hopefully, you can see that 「じゃん」 is basically saying something along the lines of, “See, I’m right, aren’t I?”'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hora,
                              inlineSpan: const TextSpan(
                                text: 'ほら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: yappari,
                              inlineSpan: const TextSpan(
                                text: 'やっぱり',
                              ),
                            ),
                            VocabTooltip(
                              message: repooto,
                              inlineSpan: const TextSpan(
                                text: 'レポート',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: const TextSpan(
                                text: '書かない',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'だめ',
                              ),
                            ),
                            TextSpan(
                              text: 'じゃん',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'See, as I thought, you have to write the report.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いない',
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: kigaeru,
                              inlineSpan: const TextSpan(
                                text: '着替えて',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            TextSpan(
                              text: 'じゃん',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Since there’s nobody, it’s probably fine to change here.'),
                    ],
                  ),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'たかし',
                        ),
                        VocabTooltip(
                          message: kun,
                          inlineSpan: const TextSpan(
                            text: '君',
                          ),
                        ),
                        const TextSpan(
                          text: 'は、',
                        ),
                        VocabTooltip(
                          message: koko,
                          inlineSpan: const TextSpan(
                            text: 'ここ',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iru,
                          inlineSpan: const TextSpan(
                            text: 'いる',
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('Is Takashi here?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shiru,
                            inlineSpan: const TextSpan(
                              text: '知らない',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Dunno.'),
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'あっ！',
                          ),
                          VocabTooltip(
                            message: yappa,
                            inlineSpan: const TextSpan(
                              text: 'やっぱ',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: iru,
                            inlineSpan: const TextSpan(
                              text: 'いる',
                            ),
                          ),
                          TextSpan(
                            text: 'じゃん',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(text: '！'),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Ah! See, he is here!'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'There’s also another variation, which attaches the question marker as well. The meaning is mostly the same but it adds more to the questioning, confirming tone.'),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: eki,
                            inlineSpan: const TextSpan(
                              text: '駅',
                            ),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: chikai,
                            inlineSpan: const TextSpan(
                              text: '近く',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: karaoke,
                            inlineSpan: const TextSpan(
                              text: 'カラオケ',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ある',
                            ),
                          ),
                          TextSpan(
                            text: 'じゃんか',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text(
                        'There’s a karaoke place near the station, right?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(
                              text: 'うん',
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Yeah.'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: asoko,
                            inlineSpan: const TextSpan(
                              text: 'あそこ',
                            ),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: sugu,
                            inlineSpan: const TextSpan(
                              text: 'すぐ',
                            ),
                          ),
                          VocabTooltip(
                            message: tonari,
                            inlineSpan: const TextSpan(
                              text: '隣',
                            ),
                          ),
                          const TextSpan(text: 'だ。'),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('It’s right next to there.'),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary サラリーマン – office worker (salary man) 残業 【ざん・ぎょう】 – overtime たくさん – a lot (amount) する (exception) – to do まあ – well いい (i-adj) – good ほら – look やはり／やっぱり – as I thought レポート – report 書く 【か・く】 (u-verb) – to write 駄目 【だめ】 – no good 誰 【だれ】 – who いる (ru-verb) – to exist (animate) ここ – here 着替える 【きが・える】 (ru-verb) – to change clothes～君 【～くん】 – name suffix 知る 【し・る】 (u-verb) – to know やはり／やっぱり／やっぱ – as I thought 駅 【えき】 – station 近い 【ちか・い】 (i-adj) – close, near カラオケ – karaoke ある (u-verb) – to exist (inanimate) うん – yes (casual) あそこ – over there すぐ – soon; nearby 隣 【となり】 – next to 「じゃん」 is an abbreviation of 「じゃない」, the negative conjugation for nouns and na-adjectives. However, this only applies to 「じゃない」 used in the following fashion. サラリーマンだから、残業はたくさんするんじゃない？ Because he’s a salaryman, doesn’t he do a lot of overtime? The important thing to note about the example above is that 「じゃない」 here is actually confirming the positive. In fact, a closer translation is, “Because he’s a salaryman, he probably does a lot of overtime.” But it’s still a question so there’s a slight nuance that you are seeking confirmation even though you are relatively sure. 「じゃん」 is a shorter slang for expressing the same type of thing except it doesn’t even bother to ask a question to confirm. It’s completely affirmative in tone. In fact, the closest equivalent to 「じゃん」 is 「じゃない」 used in the following fashion. まあ、いいじゃない。 Well, it’s probably fine (don’t you think?). This type of expression is the only case where you can attach 「じゃない」 directly to i-adjectives and verbs. Once you actually hear this expression in real life, you’ll see that it has a distinct pronunciation that is different from simply using the negative. Plus, you have to realize that this type of 「じゃない」 sounds rather mature and feminine, unlike 「じゃん」, which is gender-neutral. Like the above, specialized use of 「じゃない」, you can also attach 「じゃん」 directly to verbs and i-adjectives as well as the usual nouns and na-adjectives. Because slang is usually created to make things easier, it’s not surprising that the rules for using 「じゃん」 are so lax and easy. Summary Though derived from 「じゃない」, 「じゃん」 is always used to confirm the positive. It can be attached to the end of any sentence regardless of whether it ends in a noun, adjective, verb, or adverb. Finally, let’s get to the examples. Hopefully, you can see that 「じゃん」 is basically saying something along the lines of, “See, I’m right, aren’t I?” Examples ほら、やっぱりレポートを書かないとだめじゃん。 See, as I thought, you have to write the report. 誰もいないからここで着替えてもいいじゃん。 Since there’s nobody, it’s probably fine to change here. Ａ：たかし君は、ここにいる？ A: Is Takashi here? Ｂ：知らない。 B：Dunno. Ａ：あっ！やっぱ、いるじゃん！ A: Ah! See, he is here! There’s also another variation, which attaches the question marker as well. The meaning is mostly the same but it adds more to the questioning, confirming tone. Ａ：駅の近くにカラオケがあるじゃんか。 A: There’s a karaoke place near the station, right? Ｂ：うん。 B: Yeah. Ａ：あそこのすぐ隣だ。 A: It’s right next to there.',
      grammars: ['じゃん','じゃんか'],
      keywords: ['jan','janka'],
    ),
    Section(
      heading: 'Using 「つ」 for 「という」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('何で 【なん・で】 – why; how'),
                  Text('お前 【お・まえ】 – you (casual)'),
                  Text('ここ – here'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('デート – date'),
                  Text('する (exception) – to do'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('試験 【し・けん】 – exam'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('違う 【ちが・う】 (u-verb) – to be different'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'As we learned in the defining and describing section, 「'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 serves many more functions than the equivalent English verb, “to say”. It is used all the time and therefore, it’s not too surprising that a number of variations and slang have developed. Here’s one more that I felt was too “slangy” to cover so early at that point of the guide.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'This may sound hard to believe but if you really slur 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 together, it becomes something resembling 「つ」. Or least, that’s what somebody thought when he or she began replacing 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(text: '」 with 「つ」 or in some case 「つう」.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Now, in my opinion, 「つ」 is a lot harder to say than 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 so using it like a native might take a bit of practice. Rather than making things easier to say, as is usually the case, the real purpose of this substitution is to sound rougher because 「つ」 has a harder, hissing sound. This is ideal for when you’re pissed or for that young and rough image you’ve always wanted. As you might expect, this type of speech is usually used by males or very tough females.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'つうか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nande,
                              inlineSpan: const TextSpan(
                                text: 'なんで',
                              ),
                            ),
                            VocabTooltip(
                              message: omae,
                              inlineSpan: const TextSpan(
                                text: 'お前',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'にいんのよ！'),
                          ],
                        ),
                      ),
                      const Text('Or rather, why are you here?!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'つって',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'んのに、みきちゃんと'),
                            VocabTooltip(
                              message: deeto,
                              inlineSpan: const TextSpan(
                                text: 'デート',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'し',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: 'いった',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞いた',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Although he’s saying he doesn’t have time due to homework, I heard he went on a date with Miki-chan.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: shiken,
                              inlineSpan: const TextSpan(
                                text: '試験',
                              ),
                            ),
                            const TextSpan(text: 'だぞ。'),
                            TextSpan(
                              text: 'つっても',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'してない',
                              ),
                            ),
                            const TextSpan(text: 'だろうな。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Yo, tomorrow’s the test. Even if I say that, you probably didn’t study anyway, huh?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'だから、'),
                            VocabTooltip(
                              message: chigau,
                              inlineSpan: const TextSpan(
                                text: '違う',
                              ),
                            ),
                            const TextSpan(text: 'んだ'),
                            TextSpan(
                              text: 'つう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'の！'),
                          ],
                        ),
                      ),
                      const Text('Like I said, you’re wrong!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'If you want even more emphasis, you can even add a small 「つ」. This usually means you are really at the brink of your patience.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'だから、'),
                            VocabTooltip(
                              message: chigau,
                              inlineSpan: const TextSpan(
                                text: '違う',
                              ),
                            ),
                            const TextSpan(text: 'んだ'),
                            TextSpan(
                              text: 'っつう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'の！'),
                          ],
                        ),
                      ),
                      const Text('Like I said, you’re wrong!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 言う 【い・う】 (u-verb) – to say 何で 【なん・で】 – why; how お前 【お・まえ】 – you (casual) ここ – here いる (ru-verb) – to exist (animate) 宿題 【しゅく・だい】 – homework 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) デート – date する (exception) – to do 行く 【い・く】 (u-verb) – to go 聞く 【き・く】 (u-verb) – to ask; to listen 明日 【あした】 – tomorrow 試験 【し・けん】 – exam 勉強 【べん・きょう】 – study 違う 【ちが・う】 (u-verb) – to be different As we learned in the defining and describing section, 「いう」 serves many more functions than the equivalent English verb, “to say”. It is used all the time and therefore, it’s not too surprising that a number of variations and slang have developed. Here’s one more that I felt was too “slangy” to cover so early at that point of the guide. This may sound hard to believe but if you really slur 「という」 together, it becomes something resembling 「つ」. Or least, that’s what somebody thought when he or she began replacing 「という」 with 「つ」 or in some case 「つう」. Now, in my opinion, 「つ」 is a lot harder to say than 「という」 so using it like a native might take a bit of practice. Rather than making things easier to say, as is usually the case, the real purpose of this substitution is to sound rougher because 「つ」 has a harder, hissing sound. This is ideal for when you’re pissed or for that young and rough image you’ve always wanted. As you might expect, this type of speech is usually used by males or very tough females. Examples つうか、なんでお前がここにいんのよ！ Or rather, why are you here?! 宿題で時間がないつってんのに、みきちゃんとデートしにいったと聞いたよ。 Although he’s saying he doesn’t have time due to homework, I heard he went on a date with Miki-chan. 明日は試験だぞ。つっても、勉強はしてないだろうな。 Yo, tomorrow’s the test. Even if I say that, you probably didn’t study anyway, huh? だから、違うんだつうの！ Like I said, you’re wrong! If you want even more emphasis, you can even add a small 「つ」. This usually means you are really at the brink of your patience. だから、違うんだっつうの！ Like I said, you’re wrong!',
      grammars: ['つ'],
      keywords: ['tsu','to iu','quote'],
    ),
    Section(
      heading: 'Using 「ってば」 and 「ったら」 to show exasperation',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('もう – already'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('あなた – you'),
                  Text('いつも – always'),
                  Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          '「ってば」 and 「ったら」 is yet another type of abbreviation for 「という」 similar to 「って」 as discussed in the defining and describing section. In this case, it’s an abbreviation of the conditional form of 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いう',
                    ),
                  ),
                  const TextSpan(text: '」, which is 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いえば',
                    ),
                  ),
                  const TextSpan(text: '」 and 「と'),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: 'いったら',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. By using this abbreviation, you are essentially saying something along the lines of, “If I told you once, I told you a million times!” You can use this expression when you tired of repeating yourself or when you are exasperated with somebody for not listening to you.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            TextSpan(
                              text: 'ってば',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('I told you I’m going already!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: anata,
                              inlineSpan: const TextSpan(
                                text: 'あなた',
                              ),
                            ),
                            TextSpan(
                              text: 'ったら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: itsumo,
                              inlineSpan: const TextSpan(
                                text: 'いつも',
                              ),
                            ),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: const TextSpan(
                                text: '忘れる',
                              ),
                            ),
                            const TextSpan(text: 'んだから。'),
                          ],
                        ),
                      ),
                      const Text('You’re always forgetting.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary もう – already 行く 【い・く】 (u-verb) – to go あなた – you いつも – always 忘れる 【わす・れる】 (ru-verb) – to forget 「ってば」 and 「ったら」 is yet another type of abbreviation for 「という」 similar to 「って」 as discussed in the defining and describing section. In this case, it’s an abbreviation of the conditional form of 「という」, which is 「といえば」 and 「といったら」. By using this abbreviation, you are essentially saying something along the lines of, “If I told you once, I told you a million times!” You can use this expression when you tired of repeating yourself or when you are exasperated with somebody for not listening to you. Examples もう行くってば！ I told you I’m going already! あなたったら、いつも忘れるんだから。 You’re always forgetting.',
      grammars: ['ってば','ったら'],
      keywords: ['tteba','ttara'],
    ),
    Section(
      heading: 'Using 「なんか」 just about everywhere',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('何 【なに／なん】 – what'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('今日 【きょう】 – today'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('お風呂 【お・ふ・ろ】 – bath'),
                  Text('超 【ちょう】 – super'),
                  Text('気持ち 【き・も・ち】 – feeling'),
                  Text('いい (i-adj) – good'),
                  Text('お母さん【お・かあ・さん】 – mother (polite)'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('戻る 【もど・る】 (u-verb) – to return'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('こと – event, matter'),
                  Text('本当 【ほん・とう】 – real'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'By now, you’re probably aware that 「'),
                  VocabTooltip(
                    message: naninan,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(text: '」 can be either read as 「'),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: 'なに',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: nan,
                    inlineSpan: const TextSpan(
                      text: 'なん',
                    ),
                  ),
                  const TextSpan(
                      text: '」 depending on what comes after it such as 「'),
                  VocabTooltip(
                    message: naniiro,
                    inlineSpan: const TextSpan(
                      text: '何色',
                    ),
                  ),
                  const TextSpan(text: '」（'),
                  VocabTooltip(
                    message: naniiro,
                    inlineSpan: const TextSpan(
                      text: 'なにいろ',
                    ),
                  ),
                  const TextSpan(text: '） versus 「'),
                  VocabTooltip(
                    message: nannin,
                    inlineSpan: const TextSpan(
                      text: '何人',
                    ),
                  ),
                  const TextSpan(text: '」（'),
                  VocabTooltip(
                    message: nannin,
                    inlineSpan: const TextSpan(
                      text: 'なんにん',
                    ),
                  ),
                  const TextSpan(text: '）. In the case of 「'),
                  VocabTooltip(
                    message: nanika,
                    inlineSpan: const TextSpan(
                      text: '何か',
                    ),
                  ),
                  const TextSpan(text: '」, while 「'),
                  VocabTooltip(
                    message: nanika,
                    inlineSpan: const TextSpan(
                      text: 'なにか',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is the correct reading, it is often contracted to just 「なんか」 in casual speech.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nanika,
                              inlineSpan: TextSpan(
                                text: 'なにか',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Eat something?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Eat something?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'However, 「なんか」 also has a function similar to the word “like” in English. By “like”, I’m not talking about the actual word but the kind that has no meaning and some people use just about anywhere in the sentence. Similarly, 「なんか」 can also be used as a filler without any actual meaning. For instance, take a look at the example below.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            const TextSpan(text: 'みたいよ。'),
                          ],
                        ),
                      ),
                      const Text('I guess he’s like busy today.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'While 「なんか」 is a shorter version of 「'),
                  VocabTooltip(
                    message: nanika,
                    inlineSpan: const TextSpan(
                      text: 'なにか',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」, only 「なんか」 can be used in this way as a filler.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: nanika,
                              inlineSpan: TextSpan(
                                text: 'なにか',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            const TextSpan(text: 'みたいよ。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: '（「'),
                            VocabTooltip(
                              message: nanika,
                              inlineSpan: const TextSpan(
                                text: 'なにか',
                              ),
                            ),
                            const TextSpan(
                                text: '」 cannot be used as a filler word.）'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(text: 'Let’s take a look at a few more examples.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'ね。'),
                            VocabTooltip(
                              message: ofuro,
                              inlineSpan: const TextSpan(
                                text: 'お風呂',
                              ),
                            ),
                            const TextSpan(text: 'って'),
                            VocabTooltip(
                              message: chou,
                              inlineSpan: const TextSpan(
                                text: '超',
                              ),
                            ),
                            VocabTooltip(
                              message: kimochi,
                              inlineSpan: const TextSpan(
                                text: '気持ち',
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'よね！'),
                          ],
                        ),
                      ),
                      const Text('Like, baths feel really good, huh?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okaasan,
                              inlineSpan: const TextSpan(
                                text: 'お母さん',
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'まで'),
                            VocabTooltip(
                              message: modoru,
                              inlineSpan: const TextSpan(
                                text: '戻らない',
                              ),
                            ),
                            const TextSpan(text: 'んだってよ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Mom said she’s not coming back until like tomorrow.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'さ。ボブは、'),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            TextSpan(
                              text: 'なんか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(text: 'かな？'),
                          ],
                        ),
                      ),
                      const Text(
                          'Hey like, do you really think that Bob likes somebody like me?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 何 【なに／なん】 – what 食べる 【たべ・る】 (ru-verb) – to eat 今日 【きょう】 – today 忙しい 【いそが・しい】 (i-adj) – busy お風呂 【お・ふ・ろ】 – bath 超 【ちょう】 – super 気持ち 【き・も・ち】 – feeling いい (i-adj) – good お母さん【お・かあ・さん】 – mother (polite) 明日 【あした】 – tomorrow 戻る 【もど・る】 (u-verb) – to return 私 【わたし】 – me; myself; I こと – event, matter 本当 【ほん・とう】 – real 好き 【す・き】 (na-adj) – likable; desirable By now, you’re probably aware that 「何」 can be either read as 「なに」 or 「なん」 depending on what comes after it such as 「何色」（なにいろ） versus 「何人」（なんにん）. In the case of 「何か」, while 「なにか」 is the correct reading, it is often contracted to just 「なんか」 in casual speech. なにか食べる？ Eat something? なんか食べる？ Eat something? However, 「なんか」 also has a function similar to the word “like” in English. By “like”, I’m not talking about the actual word but the kind that has no meaning and some people use just about anywhere in the sentence. Similarly, 「なんか」 can also be used as a filler without any actual meaning. For instance, take a look at the example below. 今日は、なんか忙しいみたいよ。 I guess he’s like busy today. While 「なんか」 is a shorter version of 「なにか」, only 「なんか」 can be used in this way as a filler. 今日は、なにか忙しいみたいよ。（「なにか」 cannot be used as a filler word.） Let’s take a look at a few more examples. Examples なんかね。お風呂って超気持ちいいよね！ Like, baths feel really good, huh? お母さんが、なんか明日まで戻らないんだってよ。 Mom said she’s not coming back until like tomorrow. なんかさ。ボブは、私のことなんか本当に好きかな？ Hey like, do you really think that Bob likes somebody like me?',
      grammars: ['なんか'],
      keywords: ['nanka','like','something','filler'],
    ),
    Section(
      heading: 'Showing contempt for an action with 「～やがる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('あんな – that sort of'),
                  Text('奴 【やつ】 – guy (derogatory)'),
                  Text('負ける 【ま・ける】 (ru-verb) – to lose'),
                  Text('どう – how'),
                  Text('する (exception) – to do'),
                  Text('やる (u-verb) – to do'),
                  Text('気 【き】 – mood; intent'),
                  Text('さっさと – quickly'),
                  Text('来る 【く・る】 (exception) – to come'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          '「やがる」 is a verb suffix used to indicate hatred or contempt for the person doing the action. Unlike the rest of the slang covered here, this extremely strong language is '),
                  TextSpan(
                    text: 'not',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                      text:
                          ' used in normal, everyday conversations. You will probably never hear this expression outside of movies, comic books, games, and the like. However, it is covered here so that you can understand when it is used in those mediums.'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In order to use 「やがる」, you simply attach it to the stem of the verb. After that, 「やがる」 is conjugated just like a regular u-verb.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: anna,
                              inlineSpan: const TextSpan(
                                text: 'あんな',
                              ),
                            ),
                            VocabTooltip(
                              message: yatsu,
                              inlineSpan: const TextSpan(
                                text: 'やつ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: makeru,
                              inlineSpan: TextSpan(
                                text: '負け',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'やがって',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。じゃ、'),
                            VocabTooltip(
                              message: dou,
                              inlineSpan: const TextSpan(
                                text: 'どう',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'す',
                              ),
                            ),
                            const TextSpan(text: 'んだよ？'),
                          ],
                        ),
                      ),
                      const Text(
                          'Losing to a guy like that. Well, what are you going to do?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yaru,
                              inlineSpan: const TextSpan(
                                text: 'やる',
                              ),
                            ),
                            VocabTooltip(
                              message: ki,
                              inlineSpan: const TextSpan(
                                text: '気',
                              ),
                            ),
                            const TextSpan(text: 'か？だったら'),
                            VocabTooltip(
                              message: sassato,
                              inlineSpan: const TextSpan(
                                text: 'さっさと',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                text: '来',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'やがれ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text(
                          'You want to fight? If so, then hurry up and come on!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あんな – that sort of 奴 【やつ】 – guy (derogatory) 負ける 【ま・ける】 (ru-verb) – to lose どう – how する (exception) – to do やる (u-verb) – to do 気 【き】 – mood; intent さっさと – quickly 来る 【く・る】 (exception) – to come 「やがる」 is a verb suffix used to indicate hatred or contempt for the person doing the action. Unlike the rest of the slang covered here, this extremely strong language is not used in normal, everyday conversations. You will probably never hear this expression outside of movies, comic books, games, and the like. However, it is covered here so that you can understand when it is used in those mediums. In order to use 「やがる」, you simply attach it to the stem of the verb. After that, 「やがる」 is conjugated just like a regular u-verb. Examples あんなやつに負けやがって。じゃ、どうすんだよ？ Losing to a guy like that. Well, what are you going to do? やる気か？だったらさっさと来やがれ！ You want to fight? If so, then hurry up and come on!',
      grammars: ['～やがる'],
      keywords: ['verb','yagaru'],
    ),
  ],
);
