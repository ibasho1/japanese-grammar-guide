import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson suruNaru = Lesson(
  title: 'Using する and なる With the に Particle',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We can use the verbs 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text:
                        '」 in conjunction with the 「に」 particle to make various useful expressions. We are used to using the object particle with 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text: '」 because something is usually done ',
                  ),
                  const TextSpan(
                    text: 'to',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text:
                        ' something else. We will see how the meaning changes when we change the particle to 「に」. As for 「',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text:
                        '」, it is always used with the 「に」 particle because “becoming” is not an action done to something else but rather a target of change. The only grammatical point of interest here is using 「',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text: '」 with i-adjectives and verbs.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We can use the verbs 「する」 and 「なる」 in conjunction with the 「に」 particle to make various useful expressions. We are used to using the object particle with 「する」 because something is usually done to something else. We will see how the meaning changes when we change the particle to 「に」. As for 「なる」, it is always used with the 「に」 particle because “becoming” is not an action done to something else but rather a target of change. The only grammatical point of interest here is using 「なる」 with i-adjectives and verbs.',
      grammars: [],
      keywords: ['suru','naru','ni suru','ni naru','become'],
    ),
    Section(
      heading: 'Using 「なる」 and 「する」 for nouns and na-adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('上手 【じょう・ず】 (na-adj) – skillful'),
                  Text('なる (u-verb) – to become'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('医者 【い・しゃ】 – doctor'),
                  Text('有名 【ゆう・めい】 (na-adj) – famous'),
                  Text('人 【ひと】 – person'),
                  Text('ハンバーガー – hamburger'),
                  Text('サラダ – salad'),
                  Text('する (exception) – to do'),
                  Text('他 【ほか】 – other'),
                  Text('いい (i-adj) – good'),
                  Text('物 【もの】 – object'),
                  Text('たくさん – a lot (amount)'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('やはり／やっぱり – as I thought'),
                  Text('これ – this'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'As already explained, using 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                      text:
                          '」 with nouns and na-adjectives presents nothing new and acts pretty much the way you’d expect.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: jouzu,
                              inlineSpan: const TextSpan(
                                text: '上手',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'His Japanese has become skillful.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: isha,
                              inlineSpan: const TextSpan(
                                text: '医者',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I became a doctor.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: yuumei,
                              inlineSpan: const TextSpan(
                                text: '有名',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I will become a famous person.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'For adjectives, using the verb 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with the 「に」 particle is just a review back to the lesson on adverbs. However, for nouns, when you use the verb 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with the 「に」 particle, it means that you are going to do things toward something. This changes the meaning of 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 to mean, “to decide on [X]”. This is a common expression to use, for instance, when you are ordering items on a menu.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: hanbaagaa,
                              inlineSpan: const TextSpan(
                                text: 'ハンバーガー',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: sarada,
                              inlineSpan: const TextSpan(
                                text: 'サラダ',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'します',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll have the hamburger and salad. (lit: I’ll do toward hamburger and salad.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hoka,
                              inlineSpan: const TextSpan(
                                text: '他',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: 'もの',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: yappari,
                              inlineSpan: const TextSpan(
                                text: 'やっぱり',
                              ),
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'There are a lot of other good things, but as I thought, I’ll go with this one.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'If you think this expression is strange, think about the English expression, “I’ll go with the hamburger.” Exactly where are you going with the hamburger?'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 彼 【かれ】 – he; boyfriend 日本語 【に・ほん・ご】 – Japanese (language) 上手 【じょう・ず】 (na-adj) – skillful なる (u-verb) – to become 私 【わたし】 – me, myself, I 医者 【い・しゃ】 – doctor 有名 【ゆう・めい】 (na-adj) – famous 人 【ひと】 – person ハンバーガー – hamburger サラダ – salad する (exception) – to do 他 【ほか】 – other いい (i-adj) – good 物 【もの】 – object たくさん – a lot (amount) ある (u-verb) – to exist (inanimate) やはり／やっぱり – as I thought これ – this As already explained, using 「なる」 with nouns and na-adjectives presents nothing new and acts pretty much the way you’d expect. 彼の日本語が上手になった。 His Japanese has become skillful. 私は医者になった。 I became a doctor. 私は有名な人になる。 I will become a famous person. For adjectives, using the verb 「する」 with the 「に」 particle is just a review back to the lesson on adverbs. However, for nouns, when you use the verb 「する」 with the 「に」 particle, it means that you are going to do things toward something. This changes the meaning of 「する」 to mean, “to decide on [X]”. This is a common expression to use, for instance, when you are ordering items on a menu. 私は、ハンバーガーとサラダにします。 I’ll have the hamburger and salad. (lit: I’ll do toward hamburger and salad.) 他にいいものがたくさんあるけど、やっぱりこれにする。 There are a lot of other good things, but as I thought, I’ll go with this one. If you think this expression is strange, think about the English expression, “I’ll go with the hamburger.” Exactly where are you going with the hamburger?',
      grammars: ['にする', 'になる'],
      keywords: ['suru','naru','ni suru','ni naru','become','go with','noun','na-adjective'],
    ),
    Section(
      heading: 'Using 「なる」 with i-adjectives',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('去年 【きょ・ねん】 – last year'),
                  Text('～から (particle) – from ～'),
                  Text('背 【せ】 – height'),
                  Text('高い 【たか・い】 (i-adj) – high; tall; expensive'),
                  Text('なる (u-verb) – to become'),
                  Text('運動 【うん・どう】 – exercise'),
                  Text('する (exception) – to do'),
                  Text('人～から (particle) – ～ so'),
                  Text('強い 【つよ・い】 (i-adj) – strong'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('たくさん – a lot (amount)'),
                  Text('頭 【あたま】 – head'),
                  Text('いい (i-adj) – good'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Because the 「に」 particle is a target particle that is used for nouns and by extension na-adjectives, we need to use something else to show that something is becoming an i-adjective. Since “becoming” expresses a change in state, it makes sense to describe this process using an adverb. In fact, you’ll notice that we were already using adverbs (of a sort) in the previous section by using 「に」 with na-adjectives.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyonen,
                              inlineSpan: const TextSpan(
                                text: '去年',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: se,
                              inlineSpan: const TextSpan(
                                text: '背',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高く',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Your height has gotten taller from last year, huh?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: undou,
                              inlineSpan: const TextSpan(
                                text: '運動',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'している',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: tsuyoi,
                              inlineSpan: TextSpan(
                                text: '強く',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I will become stronger because I am exercising.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: atama,
                              inlineSpan: const TextSpan(
                                text: '頭',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'よく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Since I studied a lot, I became smarter. (lit: head became better)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 去年 【きょ・ねん】 – last year～から (particle) – from ～背 【せ】 – height 高い 【たか・い】 (i-adj) – high; tall; expensive なる (u-verb) – to become 運動 【うん・どう】 – exercise する (exception) – to do 人～から (particle) – ～ so 強い 【つよ・い】 (i-adj) – strong 勉強 【べん・きょう】 – study たくさん – a lot (amount) 頭 【あたま】 – head いい (i-adj) – good Because the 「に」 particle is a target particle that is used for nouns and by extension na-adjectives, we need to use something else to show that something is becoming an i-adjective. Since “becoming” expresses a change in state, it makes sense to describe this process using an adverb. In fact, you’ll notice that we were already using adverbs (of a sort) in the previous section by using 「に」 with na-adjectives. 去年から背が高くなったね。 Your height has gotten taller from last year, huh? 運動しているから、強くなる。 I will become stronger because I am exercising. 勉強をたくさんしたから、頭がよくなった。 Since I studied a lot, I became smarter. (lit: head became better)',
      grammars: ['～くなる'],
      keywords: ['naru','become','i-adjective'],
    ),
    Section(
      heading: 'Using 「なる」 and 「する」 with verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('海外 【かい・がい】 – overseas'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('こと – event, matter'),
                  Text('なる (u-verb) – to become'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('肉 【にく】 – meat'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('する (exception) – to do'),
                  Text('日本 【に・ほん】 – Japan'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('一年間 【いち・ねん・かん】 – span of 1 year'),
                  Text('練習 【れん・しゅう】 – practice'),
                  Text('ピアノ – piano'),
                  Text('弾く 【ひ・く】 (u-verb) – to play (piano, guitar)'),
                  Text('地下 【ち・か】 – underground'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('富士山 【ふ・じ・さん】 – Mt. Fuji'),
                  Text('見える 【み・える】 (ru-verb) – to be visible'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'You may be wondering how to use 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with verbs since there’s no way to directly modify a verb with another verb. The simple solution is to add a generic noun such as a generic event: '),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(text: '（'),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: '事',
                    ),
                  ),
                  const TextSpan(text: '） or an appearance/manner: '),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(text: '（'),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: '様',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '）. These nouns don’t refer to anything specific and are used to describe something else. In this case, they allow us to describe verbs in the same manner as nouns. Here are some examples of how to use these generic nouns with 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(text: '」 and 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(text: '」.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kaigai,
                              inlineSpan: const TextSpan(
                                text: '海外',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s been decided that I will go abroad. (lit: It became the event of going abroad.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It became so that I eat meat everyday. (lit: It became the appearance of eating meat everyday.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kaigai,
                              inlineSpan: const TextSpan(
                                text: '海外',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I decided I will go abroad. (lit: I did toward the event of going abroad.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べる',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I will try to eat meat everyday. (lit: I will do toward the manner of eating meat everyday.)',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'You can modify a verb with 「'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(text: '」 or 「'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 by first making it into a noun clause and then treating it just like a regular noun. Pretty clever, huh? I hope the literal translations give you a sense of why the example sentences mean what they do. For instance, in the fourth example, 「～'),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 translates into “to make an effort toward…” but in Japanese, it’s really only a target towards acting in a certain manner.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'Since potential verbs describe a state of feasibility rather than an action (remember, that’s why the 「を」 particle couldn’t be used), it is often used in conjunction with 「～'),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(
                      text: 'なる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 to describe a change in manner to a state of feasibility. Let’s take this opportunity to get some potential conjugation practice in.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: '来て',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sushi,
                              inlineSpan: const TextSpan(
                                text: '寿司',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べられる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'After coming to Japan, I became able to eat sushi.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ichinenkan,
                              inlineSpan: const TextSpan(
                                text: '一年間',
                              ),
                            ),
                            VocabTooltip(
                              message: renshuu,
                              inlineSpan: const TextSpan(
                                text: '練習',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: piano,
                              inlineSpan: const TextSpan(
                                text: 'ピアノ',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hiku,
                              inlineSpan: TextSpan(
                                text: '弾ける',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Because I practiced for one year, I became able to play the piano.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chika,
                              inlineSpan: const TextSpan(
                                text: '地下',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入って',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: fujisan,
                              inlineSpan: TextSpan(
                                text: '富士山',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: mieru,
                              inlineSpan: TextSpan(
                                text: '見えなく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'After going underground, Fuji-san became not visible.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 海外 【かい・がい】 – overseas 行く 【い・く】 (u-verb) – to go こと – event, matter なる (u-verb) – to become 毎日 【まい・にち】 – everyday 肉 【にく】 – meat 食べる 【た・べる】 (ru-verb) – to eat する (exception) – to do 日本 【に・ほん】 – Japan 来る 【く・る】 (exception) – to come 一年間 【いち・ねん・かん】 – span of 1 year 練習 【れん・しゅう】 – practice ピアノ – piano 弾く 【ひ・く】 (u-verb) – to play (piano, guitar) 地下 【ち・か】 – underground 入る 【はい・る】 (u-verb) – to enter 富士山 【ふ・じ・さん】 – Mt. Fuji 見える 【み・える】 (ru-verb) – to be visible You may be wondering how to use 「なる」 and 「する」 with verbs since there’s no way to directly modify a verb with another verb. The simple solution is to add a generic noun such as a generic event: こと（事） or an appearance/manner: よう（様）. These nouns don’t refer to anything specific and are used to describe something else. In this case, they allow us to describe verbs in the same manner as nouns. Here are some examples of how to use these generic nouns with 「する」 and 「なる」. 海外に行くことになった。 It’s been decided that I will go abroad. (lit: It became the event of going abroad.) 毎日、肉を食べるようになった。 It became so that I eat meat everyday. (lit: It became the appearance of eating meat everyday.) 海外に行くことにした。 I decided I will go abroad. (lit: I did toward the event of going abroad.) 毎日、肉を食べるようにする。 I will try to eat meat everyday. (lit: I will do toward the manner of eating meat everyday.) You can modify a verb with 「なる」 or 「する」 by first making it into a noun clause and then treating it just like a regular noun. Pretty clever, huh? I hope the literal translations give you a sense of why the example sentences mean what they do. For instance, in the fourth example, 「～ようにする」 translates into “to make an effort toward…” but in Japanese, it’s really only a target towards acting in a certain manner. Since potential verbs describe a state of feasibility rather than an action (remember, that’s why the 「を」 particle couldn’t be used), it is often used in conjunction with 「～ようになる」 to describe a change in manner to a state of feasibility. Let’s take this opportunity to get some potential conjugation practice in. 日本に来て、寿司が食べられるようになった。 After coming to Japan, I became able to eat sushi. 一年間練習したから、ピアノが弾けるようになった。 Because I practiced for one year, I became able to play the piano. 地下に入って、富士山が見えなくなった。 After going underground, Fuji-san became not visible.',
      grammars: ['～ことになる','～ことにする','～ようになる','～ようにする'],
      keywords: ['naru','suru','become','verb'],
    ),
  ],
);
