import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson teForm = Lesson(
  title: 'Other Uses of the Te-Form',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The te-form is incredibly useful as it is used widely in many different types of grammatical expressions. We will learn about enduring states with the 「～ている」 and 「～てある」 form. Even though we have learned various conjugations for verbs, they have all been one-time actions. We will now go over how one would say, for example, “I '),
                  TextSpan(
                    text: 'am',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                      text:
                          ' running.” We will also learn how to perform an action for the future using the 「～ておく」 expression and to express directions of actions using 「～ていく」 and 「～てくる」.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'The te-form is incredibly useful as it is used widely in many different types of grammatical expressions. We will learn about enduring states with the 「～ている」 and 「～てある」 form. Even though we have learned various conjugations for verbs, they have all been one-time actions. We will now go over how one would say, for example, “I am running.” We will also learn how to perform an action for the future using the 「～ておく」 expression and to express directions of actions using 「～ていく」 and 「～てくる」.',
      grammars: [],
      keywords: ['te-form','te','other uses'],
    ),
    Section(
      heading: 'Using 「～ている」 for enduring states',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('何【なに】 – what'),
                  Text('する (exception) – to do'),
                  Text('昼ご飯 【ひる・ご・はん】 – lunch'),
                  Text('教科書 【きょう・か・しょ】 – textbook'),
                  Text('話 【はなし】 – story'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('ううん – casual word for “no” (nah, uh-uh)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'We already know how to express a state-of-being using 「です」, 「だ」, etc. However, it only indicates a one-time thing; you are something or not. This grammar, however, describes a continuing state of an action verb. This usually translates to the progressive form in English except for a few exceptions, which we will examine later. We can make good use of the te-form we learned in the last section because the only thing left to do is add 「'),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                      text:
                          '」! You can then treat the result as a regular ru-verb.'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'This 「'),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                      text:
                          '」 is the same ru-verb describing existence, first described in the negative verb section. However, in this case, you don’t have to worry about whether the subject is animate or inanimate.'),
                ],
              ),
              NotesSection(
                heading: 'Using 「～ている」 for enduring states',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'To describe a continuing action, first conjugate the verb to the te-form and then attach the verb 「',
                              ),
                              VocabTooltip(
                                message: iru,
                                inlineSpan: const TextSpan(text: 'いる'),
                              ),
                              const TextSpan(
                                text:
                                    '」. The entire result conjugates as a ru-verb.',
                              ),
                            ],
                          ),
                        ),
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Examples:',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'て',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '読',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: iru,
                                    inlineSpan: TextSpan(
                                      text: 'いる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読',
                                        ),
                                        TextSpan(
                                          text: 'む',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読ん',
                                        ),
                                        TextSpan(
                                          text: 'で',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: const TextSpan(
                                      text: '読んで',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: iru,
                                    inlineSpan: TextSpan(
                                      text: 'いる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption:
                                      'The result conjugates as a ru-verb regardless of what the original verb is'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: ' ',
                                      ),
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Non-Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yomu,
                                                    inlineSpan: const TextSpan(
                                                      text: '読んで',
                                                    ),
                                                  ),
                                                  VocabTooltip(
                                                    message: iru,
                                                    inlineSpan: TextSpan(
                                                      text: 'いる',
                                                      style: TextStyle(
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .primary,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('reading'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yomu,
                                                    inlineSpan: const TextSpan(
                                                      text: '読んで',
                                                    ),
                                                  ),
                                                  VocabTooltip(
                                                    message: iru,
                                                    inlineSpan: TextSpan(
                                                      text: 'いない',
                                                      style: TextStyle(
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .primary,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('is not reading'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yomu,
                                                    inlineSpan: const TextSpan(
                                                      text: '読んで',
                                                    ),
                                                  ),
                                                  VocabTooltip(
                                                    message: iru,
                                                    inlineSpan: TextSpan(
                                                      text: 'いた',
                                                      style: TextStyle(
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .primary,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('was reading'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yomu,
                                                    inlineSpan: const TextSpan(
                                                      text: '読んで',
                                                    ),
                                                  ),
                                                  VocabTooltip(
                                                    message: iru,
                                                    inlineSpan: TextSpan(
                                                      text: 'いなかった',
                                                      style: TextStyle(
                                                        color: Theme.of(context)
                                                            .colorScheme
                                                            .primary,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('was not reading'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: nani,
                            inlineSpan: const TextSpan(text: '何'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: 'し',
                                ),
                                TextSpan(
                                  text: 'ている',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: 'の？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('What is friend doing?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hirugohan,
                            inlineSpan: const TextSpan(text: '昼ご飯'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: taberu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '食べ',
                                ),
                                TextSpan(
                                  text: 'ている',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('(Friend) is eating lunch.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Note that once you’ve changed it into a regular ru-verb, you can do all the normal conjugations. The examples below show the masu-form and plain negative conjugations.',
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: nani,
                            inlineSpan: const TextSpan(text: '何'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: yomu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '読ん',
                                ),
                                TextSpan(
                                  text: 'でいる',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('What are you reading?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kyoukasho,
                            inlineSpan: const TextSpan(text: '教科書'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: yomu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '読ん',
                                ),
                                TextSpan(
                                  text: 'でいます',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('I am reading textbook.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hanashi,
                            inlineSpan: const TextSpan(text: '話'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '聞い',
                                ),
                                TextSpan(
                                  text: 'ていますか',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text(
                        'Are you listening to me? (lit: Are you listening to story?)'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: uun,
                            inlineSpan: const TextSpan(text: 'ううん'),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '聞い',
                                ),
                                TextSpan(
                                  text: 'ていない',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('No, I’m not listening.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Since people are usually too lazy to roll their tongues to properly pronounce the 「い」, it is often omitted in conversational Japanese. If you are writing an essay or paper, you should always include the 「い」. Here are the abbreviated versions of the previous examples.',
                  ),
                ],
              ),
              const Heading(text: 'Example 4', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(text: '友達'),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: nani,
                            inlineSpan: const TextSpan(text: '何'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: 'し',
                                ),
                                TextSpan(
                                  text: 'てる',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: 'の？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('What is friend doing?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hirugohan,
                            inlineSpan: const TextSpan(text: '昼ご飯'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: taberu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '食べ',
                                ),
                                TextSpan(
                                  text: 'てる',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('(Friend) is eating lunch.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 5', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: nani,
                            inlineSpan: const TextSpan(text: '何'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: yomu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '読ん',
                                ),
                                TextSpan(
                                  text: 'でる',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('What are you reading?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kyoukasho,
                            inlineSpan: const TextSpan(text: '教科書'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: yomu,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '読ん',
                                ),
                                TextSpan(
                                  text: 'でいます',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('I am reading textbook.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 6', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: hanashi,
                            inlineSpan: const TextSpan(text: '話'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '聞い',
                                ),
                                TextSpan(
                                  text: 'ていますか',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text(
                        'Are you listening to me? (lit: Are you listening to story?)'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: uun,
                            inlineSpan: const TextSpan(text: 'ううん'),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kiku,
                            inlineSpan: TextSpan(
                              children: [
                                const TextSpan(
                                  text: '聞い',
                                ),
                                TextSpan(
                                  text: 'てない',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('No, I’m not listening.'),
                    english: true,
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Notice how I left the 「い」 alone for the polite forms. Though people certainly omit the 「い」 even in polite form, you might want to get used to the proper way of saying things first before getting carried away with casual abbreviations. You will be amazed at the extensive types of abbreviations that exist in casual speech. (You may also be amazed at how long everything gets in super polite speech.) Basically, you will get the abbreviations if you just act lazy and slur everything together. Particles also get punted off left and right.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'For example:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: nani,
                          inlineSpan: const TextSpan(text: '何'),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: const TextSpan(text: 'している'),
                        ),
                        const TextSpan(
                          text:
                              'の？(Those particles are such a pain to say all the time…)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: nani,
                          inlineSpan: const TextSpan(text: '何'),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: const TextSpan(text: 'している'),
                        ),
                        const TextSpan(
                          text:
                              'の？ (Ugh, I hate having to spell out all the vowels.)',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: nani,
                          inlineSpan: const TextSpan(text: '何'),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: const TextSpan(text: 'してん'),
                        ),
                        const TextSpan(
                          text: 'の？ (Ah, perfect.)',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 読む 【よ・む】 (u-verb) – to read 友達 【とも・だち】 – friend 何【なに】 – what する (exception) – to do 昼ご飯 【ひる・ご・はん】 – lunch 教科書 【きょう・か・しょ】 – textbook 話 【はなし】 – story 聞く 【き・く】 (u-verb) – to ask; to listen ううん – casual word for “no” (nah, uh-uh) We already know how to express a state-of-being using 「です」, 「だ」, etc. However, it only indicates a one-time thing; you are something or not. This grammar, however, describes a continuing state of an action verb. This usually translates to the progressive form in English except for a few exceptions, which we will examine later. We can make good use of the te-form we learned in the last section because the only thing left to do is add 「いる」! You can then treat the result as a regular ru-verb. This 「いる」 is the same ru-verb describing existence, first described in the negative verb section. However, in this case, you don’t have to worry about whether the subject is animate or inanimate. Using 「～ている」 for enduring states To describe a continuing action, first conjugate the verb to the te-form and then attach the verb 「いる」. The entire result conjugates as a ru-verb. Examples: 食べる → 食べて → 読いる 読む → 読んで → 読んでいる Example 1 Ａ：友達は何をしているの？ A: What is friend doing? Ｂ：昼ご飯を食べている。 B: (Friend) is eating lunch. Note that once you’ve changed it into a regular ru-verb, you can do all the normal conjugations. The examples below show the masu-form and plain negative conjugations. Example 2 Ａ：何を読んでいる？ A: What are you reading? Ｂ：教科書を読んでいます。 B: I am reading textbook. Example 3 Ａ：話を聞いていますか。 A: Are you listening to me? (lit: Are you listening to story?) Ｂ：ううん、聞いていない。 B: No, I’m not listening. Since people are usually too lazy to roll their tongues to properly pronounce the 「い」, it is often omitted in conversational Japanese. If you are writing an essay or paper, you should always include the 「い」. Here are the abbreviated versions of the previous examples. Example 4 Ａ：友達は何をしてるの？ A: What is friend doing? Ｂ：昼ご飯を食べてる。 B: (Friend) is eating lunch. Example 5 Ａ：何を読んでる？ A: What are you reading? Ｂ：教科書を読んでいます。 B: I am reading textbook. Example 6 Ａ：話を聞いていますか。 A: Are you listening to me? (lit: Are you listening to story?) Ｂ：ううん、聞いてない。 B: No, I’m not listening. Notice how I left the 「い」 alone for the polite forms. Though people certainly omit the 「い」 even in polite form, you might want to get used to the proper way of saying things first before getting carried away with casual abbreviations. You will be amazed at the extensive types of abbreviations that exist in casual speech. (You may also be amazed at how long everything gets in super polite speech.) Basically, you will get the abbreviations if you just act lazy and slur everything together. Particles also get punted off left and right. For example: 何をしているの？(Those particles are such a pain to say all the time…) 何しているの？ (Ugh, I hate having to spell out all the vowels.) 何してんの？ (Ah, perfect.)',
      grammars: ['～ている'],
      keywords: ['enduring states','teiru','continuing action','progressive form'],
    ),
    Section(
      heading: 'Enduring state-of-being vs enduring state of action',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('今日 【きょう】 – today'),
                  Text('この – this （abbr. of これの）'),
                  Text('歌 【うた】 – song'),
                  Text('道 【みち】 – road'),
                  Text('はい – yes (polite)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'There are certain cases where an enduring state doesn’t translate into the progressive form. In fact, there is an ambiguity in whether one is in a state of ',
                  ),
                  const TextSpan(
                    text: 'doing',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text: ' an action versus being in a state that ',
                  ),
                  const TextSpan(
                    text: 'resulted',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' from some action. This is usually decided by context and common practices. For example, although 「',
                  ),
                  VocabTooltip(
                    message: kekkon,
                    inlineSpan: const TextSpan(text: '結婚'),
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'している'),
                  ),
                  const TextSpan(
                    text:
                        '」 can technically mean someone is in a chapel currently getting married, it is usually used to refer to someone who is already married and is currently in that married state. We’ll now discuss some common verbs that often cause this type of confusion for learners of Japanese.',
                  ),
                ],
              ),
              const Heading(text: '「知る」', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知る'),
                  ),
                  const TextSpan(
                    text:
                        '」 means “to know”. English is weird in that “know” is supposed to be a verb but is actually describing a state of having knowledge. Japanese is more consistent and 「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知る'),
                  ),
                  const TextSpan(
                    text:
                        '」 is just a regular action verb. In other words, I “knowed” (action) something and so now I know it (state). That’s why the English word “to know” is really a continuing state in Japanese, namely: 「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知っている'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Heading(text: '「知る」 vs 「分かる」', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: wakaru,
                    inlineSpan: const TextSpan(text: '分かる'),
                  ),
                  const TextSpan(
                    text: '」 meaning “to understand” may seem similar to 「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知る'),
                  ),
                  const TextSpan(
                    text:
                        '」 in some cases. However, there is a difference between “knowing” and “understanding”. Try not to confuse 「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知っている'),
                  ),
                  const TextSpan(
                    text: '」 with 「',
                  ),
                  VocabTooltip(
                    message: wakaru,
                    inlineSpan: const TextSpan(text: '分かっている'),
                  ),
                  const TextSpan(
                    text: '」. 「',
                  ),
                  VocabTooltip(
                    message: wakaru,
                    inlineSpan: const TextSpan(text: '分かっている'),
                  ),
                  const TextSpan(
                    text:
                        '」 means that you are already in a state of understanding, in other words, you already get it. If you misuse this, you may sound pompous. (“Yeah, yeah, I got it already.”) On the other hand, 「',
                  ),
                  VocabTooltip(
                    message: shiru,
                    inlineSpan: const TextSpan(text: '知っている'),
                  ),
                  const TextSpan(
                    text: '」 simply means you know something.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: '知りました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I found out about it today. (I did the action of knowing today.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: uta,
                              inlineSpan: const TextSpan(
                                text: '歌',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: '知っています',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Do (you) know this song?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: michi,
                              inlineSpan: const TextSpan(
                                text: '道',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: TextSpan(
                                text: '分かります',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Do you know the way? (lit: Do (you) understand the road?)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hai,
                              inlineSpan: const TextSpan(
                                text: 'はい',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: hai,
                              inlineSpan: const TextSpan(
                                text: 'はい',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: TextSpan(
                                text: '分かった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: TextSpan(
                                text: '分かった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Yes, yes, I got it, I got it.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 知る 【し・る】 (u-verb) – to know 分かる 【わ・かる】 (u-verb) – to understand 今日 【きょう】 – today この – this （abbr. of これの） 歌 【うた】 – song 道 【みち】 – road はい – yes (polite) There are certain cases where an enduring state doesn’t translate into the progressive form. In fact, there is an ambiguity in whether one is in a state of doing an action versus being in a state that resulted from some action. This is usually decided by context and common practices. For example, although 「結婚している」 can technically mean someone is in a chapel currently getting married, it is usually used to refer to someone who is already married and is currently in that married state. We’ll now discuss some common verbs that often cause this type of confusion for learners of Japanese. 「知る」 「知る」 means “to know”. English is weird in that “know” is supposed to be a verb but is actually describing a state of having knowledge. Japanese is more consistent and 「知る」 is just a regular action verb. In other words, I “knowed” (action) something and so now I know it (state). That’s why the English word “to know” is really a continuing state in Japanese, namely: 「知っている」. 「知る」 vs 「分かる」 「分かる」 meaning “to understand” may seem similar to 「知る」 in some cases. However, there is a difference between “knowing” and “understanding”. Try not to confuse 「知っている」 with 「分かっている」. 「分かっている」 means that you are already in a state of understanding, in other words, you already get it. If you misuse this, you may sound pompous. (“Yeah, yeah, I got it already.”) On the other hand, 「知っている」 simply means you know something. Examples 今日、知りました。 I found out about it today. (I did the action of knowing today.) この歌を知っていますか？ Do (you) know this song? 道は分かりますか。 Do you know the way? (lit: Do (you) understand the road?) はい、はい、分かった、分かった。 Yes, yes, I got it, I got it.',
      grammars: [],
      keywords: ['enduring states','teiru','progressive form','know','understand','wakaru','shiru','shitteru'],
    ),
    Section(
      heading: 'Motion verbs (行く、来る、etc.)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('鈴木 【すず・き】 – Suzuki (last name)'),
                  Text('どこ – where'),
                  Text('もう – already'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('先 【さき】 – before'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('美恵 【み・え】 – Mie (first name)'),
                  Text('来る 【く・る】 (exception) – to come'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'It is reasonable to assume the actions 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(text: '行っている'),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(text: '来ている'),
                  ),
                  const TextSpan(
                    text:
                        '」 would mean, “going” and “coming” respectively. But unfortunately, this is not the case. The 「～ている」 form of motion verbs is more like a sequence of actions we saw in the last section. You completed the motion, and now you exist in that state. (Remember, 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                    text:
                        '」 is the verb of existence of animate objects.) It might help to think of it as two separate and successive actions: 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(text: '行って'),
                  ),
                  const TextSpan(
                    text: '」、and then 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suzuki,
                              inlineSpan: const TextSpan(
                                text: '鈴木',
                              ),
                            ),
                            const TextSpan(
                              text: 'さんは',
                            ),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(
                                text: 'どこ',
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Where is Suzuki-san?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: '帰っている',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'He is already at home (went home and is there now).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: saki,
                              inlineSpan: const TextSpan(
                                text: '先',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行っている',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll go on ahead. (I’ll go and be there before you.)',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mie,
                              inlineSpan: const TextSpan(
                                text: '美恵',
                              ),
                            ),
                            const TextSpan(
                              text: 'ちゃんは、',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                text: '来ている',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Mie-chan is already here, you know. (She came and is here.)',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 鈴木 【すず・き】 – Suzuki (last name) どこ – where もう – already 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home 先 【さき】 – before 行く 【い・く】 (u-verb) – to go 美恵 【み・え】 – Mie (first name) 来る 【く・る】 (exception) – to come It is reasonable to assume the actions 「行っている」 and 「来ている」 would mean, “going” and “coming” respectively. But unfortunately, this is not the case. The 「～ている」 form of motion verbs is more like a sequence of actions we saw in the last section. You completed the motion, and now you exist in that state. (Remember, 「いる」 is the verb of existence of animate objects.) It might help to think of it as two separate and successive actions: 「行って」、and then 「いる」. Examples 鈴木さんはどこですか。 Where is Suzuki-san? もう、家に帰っている。 He is already at home (went home and is there now). 先に行っているよ。 I’ll go on ahead. (I’ll go and be there before you.) 美恵ちゃんは、もう来ているよ。 Mie-chan is already here, you know. (She came and is here.)',
      grammars: [],
      keywords: ['iku','kuru','motion verb','verb','come','go'],
    ),
    Section(
      heading: 'Using 「～てある」 for resultant states',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('準備 【じゅん・び】 – preparations'),
                  Text('どう – how'),
                  Text('もう – already'),
                  Text('する (exception) – to do'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('旅行 【りょ・こう】 – travel'),
                  Text('計画 【けい・かく】 – plans'),
                  Text('終わる 【お・わる】 (u-verb) – to end'),
                  Text('うん – casual word for “yes” (yeah, uh-huh)'),
                  Text('切符 【きっ・ぷ】 – ticket'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('ホテル – hotel'),
                  Text('予約 【よ・やく】 – reservation'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Appropriately enough, just like there is an 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text: '」 to go with 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                    text:
                        '」, there is a 「～てある」 form that also has a special meaning. By replacing 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(text: 'いる'),
                  ),
                  const TextSpan(
                    text: '」 with 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text:
                        '」, instead of a continuing action, it becomes a resultant state after the action has already taken place. Usually, this expression is used to explain that something is in a state of completion. The completed action also carries a nuance of being completed in preparation for something else.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Since this grammar describes the state of a completed action, it is common to see the 「は」 and 「も」 particles instead of the 「を」 particle.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: junbi,
                            inlineSpan: const TextSpan(text: '準備'),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: dou,
                            inlineSpan: const TextSpan(text: 'どう'),
                          ),
                          const TextSpan(
                            text: 'ですか。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('How are the preparations?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: junbi,
                            inlineSpan: const TextSpan(text: '準備'),
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          VocabTooltip(
                            message: mou,
                            inlineSpan: const TextSpan(text: 'もう'),
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'してある',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          TextSpan(
                            text: 'よ',
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.primary),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('The preparations are already done.'),
                    english: true,
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: ryokou,
                            inlineSpan: const TextSpan(text: '旅行'),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: keikaku,
                            inlineSpan: const TextSpan(text: '計画'),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: owaru,
                            inlineSpan: const TextSpan(text: '終わった'),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Are the plans for the trip complete?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: un,
                            inlineSpan: const TextSpan(text: 'うん'),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: kippu,
                            inlineSpan: const TextSpan(text: '切符'),
                          ),
                          const TextSpan(
                            text: 'を',
                          ),
                          VocabTooltip(
                            message: kau,
                            inlineSpan: const TextSpan(text: '買った'),
                          ),
                          const TextSpan(
                            text: 'し、',
                          ),
                          VocabTooltip(
                            message: hoteru,
                            inlineSpan: const TextSpan(text: 'ホテル'),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: yoyaku,
                            inlineSpan: const TextSpan(text: '予約'),
                          ),
                          const TextSpan(
                            text: 'も',
                          ),
                          VocabTooltip(
                            message: suru,
                            inlineSpan: TextSpan(
                              text: 'してある',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'Uh huh, not only did I buy the ticket, I also took care of the hotel reservations.'),
                    english: true,
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 準備 【じゅん・び】 – preparations どう – how もう – already する (exception) – to do ある (u-verb) – to exist (inanimate) 旅行 【りょ・こう】 – travel 計画 【けい・かく】 – plans 終わる 【お・わる】 (u-verb) – to end うん – casual word for “yes” (yeah, uh-huh) 切符 【きっ・ぷ】 – ticket 買う 【か・う】 (u-verb) – to buy ホテル – hotel 予約 【よ・やく】 – reservation Appropriately enough, just like there is an 「ある」 to go with 「いる」, there is a 「～てある」 form that also has a special meaning. By replacing 「いる」 with 「ある」, instead of a continuing action, it becomes a resultant state after the action has already taken place. Usually, this expression is used to explain that something is in a state of completion. The completed action also carries a nuance of being completed in preparation for something else. Since this grammar describes the state of a completed action, it is common to see the 「は」 and 「も」 particles instead of the 「を」 particle. Example 1 Ａ：準備はどうですか。 A: How are the preparations? Ｂ：準備は、もうしてあるよ。 B: The preparations are already done. Example 2 Ａ：旅行の計画は終わった？ A: Are the plans for the trip complete? Ｂ：うん、切符を買ったし、ホテルの予約もしてある。 B: Uh huh, not only did I buy the ticket, I also took care of the hotel reservations.',
      grammars: ['～てある'],
      keywords: ['tearu','resultant state','complete'],
    ),
    Section(
      heading: 'Using the 「～ておく」 form as preparation for the future',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('晩ご飯 【ばん・ご・はん】 – dinner'),
                  Text('作る 【つく・る】 (u-verb) – to make'),
                  Text('電池 【でん・ち】 – battery'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'While 「～てある」 carries a nuance of a completed action in preparation for something else, 「～ておく」 explicitly states that the action is done (or will be done) with the future in mind. Imagine this: you have made a delicious pie and you’re going to ',
                  ),
                  const TextSpan(
                    text: 'place',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text:
                        ' it on the window sill for it to cool so that you can eat it later. This image might help explain why the verb 「',
                  ),
                  VocabTooltip(
                    message: oku,
                    inlineSpan: const TextSpan(text: 'おく'),
                  ),
                  const TextSpan(
                    text: '」 （',
                  ),
                  VocabTooltip(
                    message: oku,
                    inlineSpan: const TextSpan(text: '置く'),
                  ),
                  const TextSpan(
                    text:
                        '）, meaning “to place”, can be used to describe a preparation for the future. (It’s just too bad that pies on window sills always seem to go through some kind of mishap especially in cartoons.) While 「',
                  ),
                  VocabTooltip(
                    message: oku,
                    inlineSpan: const TextSpan(text: '置く'),
                  ),
                  const TextSpan(
                    text:
                        '」 by itself is written in kanji, it is customary to use hiragana when it comes attached to a conjugated verb (such as the te-form).',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bangohan,
                              inlineSpan: const TextSpan(
                                text: '晩ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: tsukuru,
                              inlineSpan: TextSpan(
                                text: '作っておく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Make dinner (in advance for the future).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denchi,
                              inlineSpan: const TextSpan(
                                text: '電池',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買っておきます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll buy batteries (in advance for the future).',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「ておく」 is also sometimes abbreviated to 「～とく」 for convenience.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bangohan,
                              inlineSpan: const TextSpan(
                                text: '晩ご飯',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: tsukuru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '作っ',
                                  ),
                                  TextSpan(
                                    text: 'とく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Make dinner (in advance for the future).',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denchi,
                              inlineSpan: const TextSpan(
                                text: '電池',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '買っ',
                                  ),
                                  TextSpan(
                                    text: 'ときます',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’ll buy batteries (in advance for the future).',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 晩ご飯 【ばん・ご・はん】 – dinner 作る 【つく・る】 (u-verb) – to make 電池 【でん・ち】 – battery 買う 【か・う】 (u-verb) – to buy While 「～てある」 carries a nuance of a completed action in preparation for something else, 「～ておく」 explicitly states that the action is done (or will be done) with the future in mind. Imagine this: you have made a delicious pie and you’re going to place it on the window sill for it to cool so that you can eat it later. This image might help explain why the verb 「おく」 （置く）, meaning “to place”, can be used to describe a preparation for the future. (It’s just too bad that pies on window sills always seem to go through some kind of mishap especially in cartoons.) While 「置く」 by itself is written in kanji, it is customary to use hiragana when it comes attached to a conjugated verb (such as the te-form). Examples 晩ご飯を作っておく。 Make dinner (in advance for the future). 電池を買っておきます。 I’ll buy batteries (in advance for the future). 「ておく」 is also sometimes abbreviated to 「～とく」 for convenience. Examples 晩ご飯を作っとく。 Make dinner (in advance for the future). 電池を買っときます。 I’ll buy batteries (in advance for the future).',
      grammars: ['～ておく'],
      keywords: ['prepare','teoku'],
    ),
    Section(
      heading: 'Using motion verbs （行く、来る） with the te-form',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('えんぴつ – pencil'),
                  Text('持つ 【も・つ】 (u-verb) – to hold'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('お父さん【お・とう・さん】 – father (polite)'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('駅 【えき】 – station'),
                  Text('方 【ほう】 – direction, way'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('冬 【ふゆ】 – winter'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('コート – coat'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('増える 【ふ・える】 (ru-verb) – to increase'),
                  Text('一生懸命 【いっ・しょう・けん・めい】 – with all one’s might'),
                  Text('頑張る 【がん・ば・る】 (u-verb) – to try one’s best'),
                  Text('色々 【いろ・いろ】 (na-adj) – various'),
                  Text('人 【ひと】 – person'),
                  Text(
                      '付き合う 【つ・き・あ・う】 (u-verb) – to go out with; to keep in company with'),
                  Text('いい (i-adj) – good'),
                  Text('まだ – yet'),
                  Text('見つかる 【み・つかる】 (u-verb) – to be found'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('ずっと – long; far'),
                  Text('前 【まえ】 – front; before'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('結局 【けっ・きょく】 – eventually'),
                  Text('やめる (ru-verb) – to stop; to quit'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also use the motion verbs “to go” （',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                    text: '）and “to come” （',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来る',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '） with the te-form, to show that an action is oriented toward or from someplace. The most common and useful example of this is the verb 「',
                  ),
                  VocabTooltip(
                    message: motsu,
                    inlineSpan: const TextSpan(
                      text: '持つ',
                    ),
                  ),
                  const TextSpan(
                    text: '」 (to hold). While 「',
                  ),
                  VocabTooltip(
                    message: motsu,
                    inlineSpan: const TextSpan(
                      text: '持っている',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 means you are in a state of holding something (in possession of), when the 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is replaced with 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: 'いく',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'くる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, it means you are taking or bringing something. Of course, the conjugation is the same as the regular 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来る',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: enpitsu,
                              inlineSpan: const TextSpan(
                                text: 'えんぴつ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: motsu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '持って',
                                  ),
                                  TextSpan(
                                    text: 'いる',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Do (you) have a pencil?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: enpitsu,
                              inlineSpan: const TextSpan(
                                text: '鉛筆',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(
                              text: 'へ',
                            ),
                            VocabTooltip(
                              message: motsu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '持って',
                                  ),
                                  TextSpan(
                                    text: 'いく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Are (you) taking pencil to school?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: enpitsu,
                              inlineSpan: const TextSpan(
                                text: '鉛筆',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: motsu,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '持って',
                                  ),
                                  TextSpan(
                                    text: 'くる',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Are (you) bringing pencil to home?',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For these examples, it may make more sense to think of them as a sequence of actions: hold and go, or hold and come. Here are a couple more examples.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: otousan,
                              inlineSpan: const TextSpan(
                                text: 'お父さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '帰って',
                                  ),
                                  TextSpan(
                                    text: 'きました',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Father came back home early.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eki,
                              inlineSpan: const TextSpan(
                                text: '駅',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(
                              text: 'へ',
                            ),
                            VocabTooltip(
                              message: hashiru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '走って',
                                  ),
                                  TextSpan(
                                    text: 'いった',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Went running toward the direction of station.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The motion verbs can also be used in time expressions to move forward or come up to the present.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: isshoukenmei,
                              inlineSpan: const TextSpan(
                                text: '一生懸命',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: ganbaru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '頑張って',
                                  ),
                                  TextSpan(
                                    text: 'いく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Will try my hardest (toward the future) with all my might!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: iroiro,
                              inlineSpan: const TextSpan(
                                text: '色々',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: tsukiau,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '付き合って',
                                  ),
                                  TextSpan(
                                    text: 'きた',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: mada,
                              inlineSpan: const TextSpan(
                                text: 'まだ',
                              ),
                            ),
                            VocabTooltip(
                              message: mitsukaru,
                              inlineSpan: const TextSpan(
                                text: '見つからない',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Went out (up to the present) with various types of people but a good person hasn’t been found yet.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: zutto,
                              inlineSpan: const TextSpan(
                                text: 'ずっと',
                              ),
                            ),
                            VocabTooltip(
                              message: mae,
                              inlineSpan: const TextSpan(
                                text: '前',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'して',
                                  ),
                                  TextSpan(
                                    text: 'きて',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kekkyoku,
                              inlineSpan: const TextSpan(
                                text: '結局',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: yameruStop,
                              inlineSpan: const TextSpan(
                                text: 'やめた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Studied Japanese from way back before and eventually quit.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary えんぴつ – pencil 持つ 【も・つ】 (u-verb) – to hold いる (ru-verb) – to exist (animate) 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 来る 【く・る】 (exception) – to come お父さん【お・とう・さん】 – father (polite) 早い 【はや・い】 (i-adj) – fast; early 帰る 【かえ・る】 (u-verb) – to go home 駅 【えき】 – station 方 【ほう】 – direction, way 走る 【はし・る】 (u-verb) – to run 冬 【ふゆ】 – winter 入る 【はい・る】 (u-verb) – to enter コート – coat 着る 【き・る】 (ru-verb) – to wear 増える 【ふ・える】 (ru-verb) – to increase 一生懸命 【いっ・しょう・けん・めい】 – with all one’s might 頑張る 【がん・ば・る】 (u-verb) – to try one’s best 色々 【いろ・いろ】 (na-adj) – various 人 【ひと】 – person 付き合う 【つ・き・あ・う】 (u-verb) – to go out with; to keep in company with いい (i-adj) – good まだ – yet 見つかる 【み・つかる】 (u-verb) – to be found 日本語 【に・ほん・ご】 – Japanese (language) ずっと – long; far 前 【まえ】 – front; before 勉強 【べん・きょう】 – study する (exception) – to do 結局 【けっ・きょく】 – eventually やめる (ru-verb) – to stop; to quit You can also use the motion verbs “to go” （行く）and “to come” （来る） with the te-form, to show that an action is oriented toward or from someplace. The most common and useful example of this is the verb 「持つ」 (to hold). While 「持っている」 means you are in a state of holding something (in possession of), when the 「いる」 is replaced with 「いく」 or 「くる」, it means you are taking or bringing something. Of course, the conjugation is the same as the regular 「行く」 and 「来る」. Examples えんぴつを持っている？ Do (you) have a pencil? 鉛筆を学校へ持っていく？ Are (you) taking pencil to school? 鉛筆を家に持ってくる？ Are (you) bringing pencil to home? For these examples, it may make more sense to think of them as a sequence of actions: hold and go, or hold and come. Here are a couple more examples. お父さんは、早く帰ってきました。 Father came back home early. 駅の方へ走っていった。 Went running toward the direction of station. The motion verbs can also be used in time expressions to move forward or come up to the present. 一生懸命、頑張っていく！ Will try my hardest (toward the future) with all my might! 色々な人と付き合ってきたけど、いい人はまだ見つからない。 Went out (up to the present) with various types of people but a good person hasn’t been found yet. 日本語をずっと前から勉強してきて、結局はやめた。 Studied Japanese from way back before and eventually quit.',
      grammars: ['～て行く','～て来る'],
      keywords: ['bring','take','iku','kuru','motion verb','verb','come','go'],
    ),
  ],
);
