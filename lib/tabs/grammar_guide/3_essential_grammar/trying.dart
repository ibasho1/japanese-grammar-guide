import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson trying = Lesson(
  title: 'Trying or Attempting Something',
  sections: [
    Section(
      heading: 'Let’s try some stuff',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In English, we use the word, “try” to mean both “to try something out” and “to make an effort to do something”. In Japanese, these are separate grammatical expressions. For instance, “I tried the cherry flavor” and “I tried to do homework” mean quite different things and though English does not make a distinction, Japanese does.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In English, we use the word, “try” to mean both “to try something out” and “to make an effort to do something”. In Japanese, these are separate grammatical expressions. For instance, “I tried the cherry flavor” and “I tried to do homework” mean quite different things and though English does not make a distinction, Japanese does.',
      grammars: [],
      keywords: ['try','attempt'],
    ),
    Section(
      heading: 'To try something out',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('見る 【み・る】 – to see; to watch'),
                  Text('切る 【き・る】 (u-verb) – to cut'),
                  Text(
                      'お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake)'),
                  Text('初めて 【はじ・めて】 – for the first time'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('とても – very'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('すごい (i-adj) – to a great extent'),
                  Text('眠い 【ねむ・い】(i-adj) – sleepy'),
                  Text('なる (u-verb) – to become'),
                  Text('新しい 【あたら・しい】(i-adj) – new'),
                  Text('デパート – department store'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('広島 【ひろ・しま】 – Hiroshima'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'To try something out, you simply need to change the verb to the te-form and add 「'),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(text: 'みる'),
                  ),
                  const TextSpan(
                      text:
                          '」. If it helps you to remember, you can think of it as a sequence of an action and then seeing the result. In fact 「'),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(text: 'みる'),
                  ),
                  const TextSpan(text: '」 conjugates just like 「'),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(text: '見る'),
                  ),
                  const TextSpan(text: '」. However, just like the 「～て'),
                  VocabTooltip(
                    message: hoshii,
                    inlineSpan: const TextSpan(text: 'ほしい'),
                  ),
                  const TextSpan(
                      text: '」 grammar we learned, this is a set phrase and 「'),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(text: '見る'),
                  ),
                  const TextSpan(text: '」 is usually written in hiragana.'),
                ],
              ),
              NotesSection(
                heading: 'To try something out',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Conjugate the verb to the te-form and add 「',
                              ),
                              VocabTooltip(
                                message: miru,
                                inlineSpan: const TextSpan(
                                  text: 'みる',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '切',
                                        ),
                                        TextSpan(
                                          text: 'って',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切って',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      text: 'みる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'You can treat the whole result as a regular verb just as you would with 「',
                              ),
                              VocabTooltip(
                                message: miru,
                                inlineSpan: const TextSpan(
                                  text: '見る',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切って',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      text: 'みる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切って',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      text: 'みた',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切って',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      text: 'みない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: kiru,
                                    inlineSpan: const TextSpan(
                                      text: '切って',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      text: 'みなかった',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okonomiyaki,
                              inlineSpan: const TextSpan(
                                text: 'お好み焼き',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: hajimete,
                              inlineSpan: const TextSpan(
                                text: '初めて',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: 'みた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしかった',
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I tried eating okonomiyaki for the first time and it was very tasty!',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲んで',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: 'みました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'が、',
                            ),
                            VocabTooltip(
                              message: sugoi,
                              inlineSpan: const TextSpan(
                                text: 'すごく',
                              ),
                            ),
                            VocabTooltip(
                              message: nemui,
                              inlineSpan: const TextSpan(
                                text: '眠く',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なりました',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I tried drinking alcohol and I became extremely sleepy.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: atarashii,
                              inlineSpan: const TextSpan(
                                text: '新しい',
                              ),
                            ),
                            VocabTooltip(
                              message: depaato,
                              inlineSpan: const TextSpan(
                                text: 'デパート',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: 'みる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’m going to check out the new department store.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hiroshima,
                              inlineSpan: const TextSpan(
                                text: '広島',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: okonomiyaki,
                              inlineSpan: const TextSpan(
                                text: 'お好み焼き',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: 'みたい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I want to try eating Hiroshima okonomiyaki!',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見る 【み・る】 – to see; to watch 切る 【き・る】 (u-verb) – to cut お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake) 初めて 【はじ・めて】 – for the first time 食べる 【た・べる】 (ru-verb) – to eat とても – very おいしい (i-adj) – tasty お酒 【お・さけ】 – alcohol 飲む 【の・む】 (u-verb) – to drink すごい (i-adj) – to a great extent 眠い 【ねむ・い】(i-adj) – sleepy なる (u-verb) – to become 新しい 【あたら・しい】(i-adj) – new デパート – department store 行く 【い・く】 (u-verb) – to go 広島 【ひろ・しま】 – Hiroshima To try something out, you simply need to change the verb to the te-form and add 「みる」. If it helps you to remember, you can think of it as a sequence of an action and then seeing the result. In fact 「みる」 conjugates just like 「見る」. However, just like the 「～てほしい」 grammar we learned, this is a set phrase and 「見る」 is usually written in hiragana. To try something out Conjugate the verb to the te-form and add 「みる」. Example: 切る → 切って → 切ってみる You can treat the whole result as a regular verb just as you would with 「見る」. Example: 切ってみる、切ってみた、切ってみない、切ってみなかった Examples お好み焼きを初めて食べてみたけど、とてもおいしかった！ I tried eating okonomiyaki for the first time and it was very tasty! お酒を飲んでみましたが、すごく眠くなりました。 I tried drinking alcohol and I became extremely sleepy. 新しいデパートに行ってみる。 I’m going to check out the new department store. 広島のお好み焼きを食べてみたい！ I want to try eating Hiroshima okonomiyaki!',
      grammars: ['～てみる'],
      keywords: ['temiru','try','try out'],
    ),
    Section(
      heading: 'To attempt to do something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('避ける 【さ・ける】 (ru-verb) – to avoid'),
                  Text('無理矢理 【む・り・や・り】 – forcibly'),
                  Text('部屋 【へ・や】 – room'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('結局 【けっ・きょく】 – eventually'),
                  Text('徹夜 【てつ・や】 – staying up all night'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('奥さん【おく・さん】 – wife (polite)'),
                  Text('止める 【と・める】 (ru-verb) – to stop'),
                  Text('なるべく – as much as possible'),
                  Text('ジム – gym'),
                  Text('決める 【き・める】 (ru-verb) – to decide'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We already learned that the volitional form was used to indicate a will to set out to do something. If you guessed that this next grammar for attempting to do something would involve the volitional form, you were right. To say that you tried (as in attempted) to do something, you need to conjugate the verb into the volitional, enclose it in a quotation (so that we can perform an action on the clause) and finally add the verb 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」. Or put more simply, you just add 「と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to the volitional form of the verb. This is simply an extension of the quoted relative clause from the last section. Instead of saying the quote （',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                    text: '） or treating it as a thought （',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                  const TextSpan(
                    text: '、',
                  ),
                  VocabTooltip(
                    message: kangaeru,
                    inlineSpan: const TextSpan(
                      text: '考える',
                    ),
                  ),
                  const TextSpan(
                    text: '）, we are simply doing it with 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Attempting a certain action',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    'Change the verb to the volitional form and add 「とする」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'よう',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見よう',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'と',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'こう',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行こう',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'と',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: sakeru,
                              inlineSpan: TextSpan(
                                text: '避けよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Everyday, she attempts to avoid study.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: muriyari,
                              inlineSpan: const TextSpan(
                                text: '無理矢理',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入ろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'している',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'He is attempting to force his way into the room.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: neruSleep,
                              inlineSpan: TextSpan(
                                text: '寝よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'した',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'けど、',
                            ),
                            VocabTooltip(
                              message: kekkyoku,
                              inlineSpan: const TextSpan(
                                text: '結局',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: tetsuya,
                              inlineSpan: const TextSpan(
                                text: '徹夜',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I attempted to sleep early but ended up staying up all night.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲もう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'した',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'が、',
                            ),
                            VocabTooltip(
                              message: okusan,
                              inlineSpan: const TextSpan(
                                text: '奥さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: tomeru,
                              inlineSpan: const TextSpan(
                                text: '止めた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'He tried to drink alcohol but his wife stopped him.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Though we use the verb 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to say, “to do attempt”, we can use different verbs to do other things with the attempt. For instance, we can use the verb 「',
                  ),
                  VocabTooltip(
                    message: kimeru,
                    inlineSpan: const TextSpan(
                      text: '決める',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to say, “decide to attempt to do [X]”. Here are some examples of other actions carried out on the attempt.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: narubeku,
                              inlineSpan: const TextSpan(
                                text: 'なるべく',
                              ),
                            ),
                            VocabTooltip(
                              message: sakeru,
                              inlineSpan: const TextSpan(
                                text: '避けよう',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思った',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I thought I would attempt to avoid studying as much as possible.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: jimu,
                              inlineSpan: const TextSpan(
                                text: 'ジム',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行こう',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: kimeru,
                              inlineSpan: TextSpan(
                                text: '決めた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Decided to attempt to go to gym everyday.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do 言う 【い・う】 (u-verb) – to say 思う 【おも・う】 (u-verb) – to think 考える 【かんが・える】 (ru-verb) – to think 見る 【み・る】 (ru-verb) – to see 行く 【い・く】 (u-verb) – to go 毎日 【まい・にち】 – everyday 勉強 【べん・きょう】 – study 避ける 【さ・ける】 (ru-verb) – to avoid 無理矢理 【む・り・や・り】 – forcibly 部屋 【へ・や】 – room 入る 【はい・る】 (u-verb) – to enter 早い 【はや・い】 (i-adj) – fast; early 寝る 【ね・る】 (ru-verb) – to sleep 結局 【けっ・きょく】 – eventually 徹夜 【てつ・や】 – staying up all night お酒 【お・さけ】 – alcohol 飲む 【の・む】 (u-verb) – to drink 奥さん【おく・さん】 – wife (polite) 止める 【と・める】 (ru-verb) – to stop なるべく – as much as possible ジム – gym 決める 【き・める】 (ru-verb) – to decide We already learned that the volitional form was used to indicate a will to set out to do something. If you guessed that this next grammar for attempting to do something would involve the volitional form, you were right. To say that you tried (as in attempted) to do something, you need to conjugate the verb into the volitional, enclose it in a quotation (so that we can perform an action on the clause) and finally add the verb 「する」. Or put more simply, you just add 「とする」 to the volitional form of the verb. This is simply an extension of the quoted relative clause from the last section. Instead of saying the quote （言う） or treating it as a thought （思う、考える）, we are simply doing it with 「する」. Attempting a certain action Change the verb to the volitional form and add 「とする」. Examples: 見る → 見よう → 見ようとする 行く → 行こう → 行こうとする Examples 毎日、勉強を避けようとする。 Everyday, she attempts to avoid study. 無理矢理に部屋に入ろうとしている。 He is attempting to force his way into the room. 早く寝ようとしたけど、結局は徹夜した。 I attempted to sleep early but ended up staying up all night. お酒を飲もうとしたが、奥さんが止めた。 He tried to drink alcohol but his wife stopped him. Though we use the verb 「する」 to say, “to do attempt”, we can use different verbs to do other things with the attempt. For instance, we can use the verb 「決める」 to say, “decide to attempt to do [X]”. Here are some examples of other actions carried out on the attempt. 勉強をなるべく避けようと思った。 I thought I would attempt to avoid studying as much as possible. 毎日ジムに行こうと決めた。 Decided to attempt to go to gym everyday.',
      grammars: ['～ようとする'],
      keywords: ['you ni suru','attempt','try'],
    ),
  ],
);
