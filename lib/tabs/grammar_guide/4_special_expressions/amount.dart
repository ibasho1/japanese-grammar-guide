import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson amount = Lesson(
  title: 'Expressing Amounts',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This lesson will cover various expressions used to express various ',
                  ),
                  TextSpan(
                    text: 'degrees',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                    text: ' of amounts. For example, sentences like, “I ',
                  ),
                  TextSpan(
                    text: 'only',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text: ' ate one”, “That was ',
                  ),
                  TextSpan(
                    text: 'all',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text: ' that was left”, “There’s ',
                  ),
                  TextSpan(
                    text: 'just',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text: ' old people here”, or “I ate ',
                  ),
                  TextSpan(
                    text: 'too much',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text:
                        '” all indicate whether there’s a lot or little of something. Most of these expressions are made with particles and not as separate words as you see in English.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This lesson will cover various expressions used to express various degrees of amounts. For example, sentences like, “I only ate one”, “That was all that was left”, “There’s just old people here”, or “I ate too much” all indicate whether there’s a lot or little of something. Most of these expressions are made with particles and not as separate words as you see in English.',
      grammars: [],
      keywords: ['amount'],
    ),
    Section(
      heading: 'Indicating that’s all there is using 「だけ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('りんご – apple'),
                  Text('これ – this'),
                  Text('それ – that'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('この – this （abbr. of これの）'),
                  Text('歌 【うた】 – song'),
                  Text('歌う 【うた・う】 (u-verb) – to sing'),
                  Text('その – that （abbr. of それの）'),
                  Text('人 【ひと】 – person'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('販売機 【はん・ばい・き】 – vending machine'),
                  Text('五百円玉 【ご・ひゃく・えん・だま】 – 500 yen coin'),
                  Text('小林 【こ・ばやし】 – Kobayashi (last name)'),
                  Text('返事 【へん・じ】 – reply'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('準備 【じゅん・び】 – preparations'),
                  Text('終わる 【お・わる】 (u-verb) – to end'),
                  Text('ここ – here'),
                  Text('名前 【な・まえ】 – name'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('いい (i-adj) – good'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The particle 「だけ」 is used to express that that’s all there is. Just like the other particles we have already learned, it is directly attached to the end of whichever word that it applies to.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ringo,
                              inlineSpan: const TextSpan(
                                text: 'りんご',
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Just apple(s) (and nothing else).'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Just that and this (and nothing else).'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When one of the major particles are also applied to a word, these particles must come after 「だけ」. In fact, the ordering of multiple particles usually start from the most specific to the most general.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            TextSpan(
                              text: 'だけは',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べないで',
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: 'ください',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Just don’t eat that. (Anything else is assumed to be OK).'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: uta,
                              inlineSpan: const TextSpan(
                                text: '歌',
                              ),
                            ),
                            TextSpan(
                              text: 'だけを',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: utau,
                              inlineSpan: const TextSpan(
                                text: '歌わなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Didn’t sing just this song.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            TextSpan(
                              text: 'だけが',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(text: 'だったんだ。'),
                          ],
                        ),
                      ),
                      const Text('That person was the only person I liked.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The same goes for double particles. Again 「だけ」 must come first.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: hanbaiki,
                              inlineSpan: const TextSpan(
                                text: '販売機',
                              ),
                            ),
                            TextSpan(
                              text: 'だけでは',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: gohyakuendama,
                              inlineSpan: const TextSpan(
                                text: '五百円玉',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: tsukau,
                              inlineSpan: const TextSpan(
                                text: '使えない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Cannot use 500 yen coin in just this vending machine.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'With minor particles such as 「から」 or 「まで」, it is difficult to tell which should come first. When in doubt, try googling to see the level of popularity of each combination. It turns out that 「からだけ」 is almost twice as popular as 「だけから」 with a hit number of 90,000 vs. 50,000.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kobayashi,
                              inlineSpan: const TextSpan(
                                text: '小林',
                              ),
                            ),
                            const TextSpan(text: 'さん'),
                            TextSpan(
                              text: 'からだけは',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: henji,
                              inlineSpan: const TextSpan(
                                text: '返事',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: '来なかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'A reply has not come '),
                            TextSpan(
                              text: 'from only',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' Kobayashi-san.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Unlike some particles, you can directly attach 「だけ」 to verbs as well.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: junbi,
                              inlineSpan: const TextSpan(
                                text: '準備',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: owaru,
                              inlineSpan: const TextSpan(
                                text: '終わった',
                              ),
                            ),
                            const TextSpan(text: 'から、'),
                            VocabTooltip(
                              message: korekara,
                              inlineSpan: const TextSpan(
                                text: 'これから',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Since the preparations are done, from here we just have to eat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: namae,
                              inlineSpan: const TextSpan(
                                text: '名前',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書く',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'ですか？'),
                          ],
                        ),
                      ),
                      const Text('Is it ok to just write [my] name here?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary りんご – apple これ – this それ – that 食べる 【たべ・る】 (ru-verb) – to eat この – this （abbr. of これの） 歌 【うた】 – song 歌う 【うた・う】 (u-verb) – to sing その – that （abbr. of それの） 人 【ひと】 – person 好き 【す・き】 (na-adj) – likable; desirable 販売機 【はん・ばい・き】 – vending machine 五百円玉 【ご・ひゃく・えん・だま】 – 500 yen coin 小林 【こ・ばやし】 – Kobayashi (last name) 返事 【へん・じ】 – reply 来る 【く・る】 (exception) – to come 準備 【じゅん・び】 – preparations 終わる 【お・わる】 (u-verb) – to end ここ – here 名前 【な・まえ】 – name 書く 【か・く】 (u-verb) – to write いい (i-adj) – good The particle 「だけ」 is used to express that that’s all there is. Just like the other particles we have already learned, it is directly attached to the end of whichever word that it applies to. Examples りんごだけ。 Just apple(s) (and nothing else). これとそれだけ。 Just that and this (and nothing else). When one of the major particles are also applied to a word, these particles must come after 「だけ」. In fact, the ordering of multiple particles usually start from the most specific to the most general. それだけは、食べないでください。 Just don’t eat that. (Anything else is assumed to be OK). この歌だけを歌わなかった。 Didn’t sing just this song. その人だけが好きだったんだ。 That person was the only person I liked. The same goes for double particles. Again 「だけ」 must come first. この販売機だけでは、五百円玉が使えない。 Cannot use 500 yen coin in just this vending machine. With minor particles such as 「から」 or 「まで」, it is difficult to tell which should come first. When in doubt, try googling to see the level of popularity of each combination. It turns out that 「からだけ」 is almost twice as popular as 「だけから」 with a hit number of 90,000 vs. 50,000. 小林さんからだけは、返事が来なかった。 A reply has not come from only Kobayashi-san. Unlike some particles, you can directly attach 「だけ」 to verbs as well. 準備が終わったから、これからは食べるだけだ。 Since the preparations are done, from here we just have to eat. ここに名前を書くだけでいいですか？ Is it ok to just write [my] name here?',
      grammars: ['だけ'],
      keywords: ['dake','just','only'],
    ),
    Section(
      heading: 'Using 「のみ」 as a formal version of 「だけ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('この – this （abbr. of これの）'),
                  Text('乗車券 【じょう・しゃ・けん】 – passenger ticket'),
                  Text('発売 【はつ・ばい】 – sale'),
                  Text('当日 【とう・じつ】 – that very day'),
                  Text('有効 【ゆう・こう】 – effective'),
                  Text('アンケート – survey'),
                  Text('対象 【たい・しょう】 – target'),
                  Text('大学生 【だい・がく・せい】 – college student'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'A particle that is essentially identical both grammatically and in meaning to 「だけ」 is 「のみ」. However, unlike 「だけ」, which is used in regular conversations, 「のみ」 is usually only used in a written context. It is often used for explaining policies, in manuals, and other things of that nature. This grammar really belongs in the advanced section since formal language has a different flavor and tone from what we have seen so far. However, it is covered here because it is essentially identical to 「だけ」. Just googling for 「のみ」 will quickly show the difference in the type of language that is used with 「のみ」 as opposed to 「だけ」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: joushaken,
                              inlineSpan: const TextSpan(
                                text: '乗車券',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: hatsubai,
                              inlineSpan: const TextSpan(
                                text: '発売',
                              ),
                            ),
                            VocabTooltip(
                              message: toujitsu,
                              inlineSpan: const TextSpan(
                                text: '当日',
                              ),
                            ),
                            TextSpan(
                              text: 'のみ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: yuukou,
                              inlineSpan: const TextSpan(
                                text: '有効',
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'This boarding ticket is '),
                            TextSpan(
                              text: 'only',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                                text:
                                    ' valid on the date on which it was purchased.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ankeeto,
                              inlineSpan: const TextSpan(
                                text: 'アンケート',
                              ),
                            ),
                            VocabTooltip(
                              message: taishou,
                              inlineSpan: const TextSpan(
                                text: '対象',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: daigakusei,
                              inlineSpan: const TextSpan(
                                text: '大学生',
                              ),
                            ),
                            TextSpan(
                              text: 'のみ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text: 'The targets of this survey are '),
                            TextSpan(
                              text: 'only',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' college students.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary この – this （abbr. of これの） 乗車券 【じょう・しゃ・けん】 – passenger ticket 発売 【はつ・ばい】 – sale 当日 【とう・じつ】 – that very day 有効 【ゆう・こう】 – effective アンケート – survey 対象 【たい・しょう】 – target 大学生 【だい・がく・せい】 – college student A particle that is essentially identical both grammatically and in meaning to 「だけ」 is 「のみ」. However, unlike 「だけ」, which is used in regular conversations, 「のみ」 is usually only used in a written context. It is often used for explaining policies, in manuals, and other things of that nature. This grammar really belongs in the advanced section since formal language has a different flavor and tone from what we have seen so far. However, it is covered here because it is essentially identical to 「だけ」. Just googling for 「のみ」 will quickly show the difference in the type of language that is used with 「のみ」 as opposed to 「だけ」. この乗車券は発売当日のみ有効です。 This boarding ticket is only valid on the date on which it was purchased. アンケート対象は大学生のみです。 The targets of this survey are only college students.',
      grammars: ['のみ'],
      keywords: ['nomi','formal','just','only'],
    ),
    Section(
      heading: 'Indicating that there’s nothing else using 「しか」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('これ – this'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('今日 【きょう】 – today'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('朝ご飯 【あさ・ご・はん】 – breakfast'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('ううん – no (casual)'),
                  Text('何【なに】 – what'),
                  Text('もらう – to receive'),
                  Text('頑張る 【がん・ば・る】 (u-verb) – to try one’s best'),
                  Text('こう – (things are) this way'),
                  Text('なる (u-verb) – to become'),
                  Text('逃げる 【に・げる】 (ru-verb) – to escape; to run away'),
                  Text('もう – already'),
                  Text('腐る 【くさ・る】 (u-verb) – to rot; to spoil'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'I carefully phrased the title of this section to show that 「しか」 must be used to indicate the ',
                  ),
                  TextSpan(
                    text: 'lack',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' of everything else. In other words, the rest of the sentence must always be negative.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('There’s nothing but this.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'The following is incorrect.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('(Should be using 「だけ」 instead)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'As you can see, 「しか」 has an embedded negative meaning while 「だけ」 doesn’t have any particular nuance.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見る',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('See just this.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'だけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Don’t see just this.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Don’t see anything else but this.'),
                    ],
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しくて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '朝ご飯',
                              ),
                            ),
                            TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べられなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Today was busy and couldn’t eat anything but breakfast.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Notice that unlike 「だけ」, it is necessary to finish off the sentence.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買う',
                              ),
                            ),
                            const TextSpan(text: 'の？'),
                          ],
                        ),
                      ),
                      const Text('You’re buying everything?)'),
                    ],
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: uun,
                              inlineSpan: const TextSpan(
                                text: 'ううん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'だけ。'),
                          ],
                        ),
                      ),
                      const Text('Nah, just this.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: uun,
                              inlineSpan: const TextSpan(
                                text: 'ううん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'しか'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買わない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Nah, won’t buy anything else but this.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: uun,
                              inlineSpan: const TextSpan(
                                text: 'ううん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                                style: TextStyle(
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            const TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                  decoration: TextDecoration.lineThrough),
                            ),
                            const TextSpan(text: 'だけ。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(Wrong, the sentence must explicitly indicate the negative.)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'While the major particles always come last, it turns out that 「しか」 must come after 「から」 and 「まで」. A google search of 「からしか」 beats 「しかから」 by an overwhelming 60,000 to 600.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'アリス'),
                            TextSpan(
                              text: 'からしか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらってない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I didn’t receive anything except from Alice.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'You can also use this grammar with verbs.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: korekara,
                              inlineSpan: const TextSpan(
                                text: 'これから',
                              ),
                            ),
                            VocabTooltip(
                              message: ganbaru,
                              inlineSpan: const TextSpan(
                                text: '頑張る',
                              ),
                            ),
                            TextSpan(
                              text: 'しか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('There’s nothing to do but try our best!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kou,
                              inlineSpan: const TextSpan(
                                text: 'こう',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なったら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nigeru,
                              inlineSpan: TextSpan(
                                text: '逃げる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'しか'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'There no choice but to run away once it turns out like this.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kusaru,
                              inlineSpan: const TextSpan(
                                text: '腐っている',
                              ),
                            ),
                            const TextSpan(text: 'から、'),
                            VocabTooltip(
                              message: suteru,
                              inlineSpan: TextSpan(
                                text: '捨てる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'しか'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s rotten already so there’s nothing to do but throw it out.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary これ – this ある (u-verb) – to exist (inanimate) 見る 【み・る】 (ru-verb) – to see 今日 【きょう】 – today 忙しい 【いそが・しい】 (i-adj) – busy 朝ご飯 【あさ・ご・はん】 – breakfast 食べる 【た・べる】 (ru-verb) – to eat 全部 【ぜん・ぶ】 – everything 買う 【か・う】 (u-verb) – to buy ううん – no (casual) 何【なに】 – what もらう – to receive 頑張る 【がん・ば・る】 (u-verb) – to try one’s best こう – (things are) this way なる (u-verb) – to become 逃げる 【に・げる】 (ru-verb) – to escape; to run away もう – already 腐る 【くさ・る】 (u-verb) – to rot; to spoil 捨てる 【す・てる】 (ru-verb) – to throw away I carefully phrased the title of this section to show that 「しか」 must be used to indicate the lack of everything else. In other words, the rest of the sentence must always be negative. これしかない。 There’s nothing but this. The following is incorrect. これしかない。(Should be using 「だけ」 instead) As you can see, 「しか」 has an embedded negative meaning while 「だけ」 doesn’t have any particular nuance. これだけ見る。 See just this. これだけ見ない。 Don’t see just this. これしか見ない。 Don’t see anything else but this. Examples 今日は忙しくて、朝ご飯しか食べられなかった。 Today was busy and couldn’t eat anything but breakfast. Notice that unlike 「だけ」, it is necessary to finish off the sentence. 全部買うの？ You’re buying everything?) ううん、これだけ。 Nah, just this. ううん、これしか買わない。 Nah, won’t buy anything else but this. ううん、これしかだけ。(Wrong, the sentence must explicitly indicate the negative.) While the major particles always come last, it turns out that 「しか」 must come after 「から」 and 「まで」. A google search of 「からしか」 beats 「しかから」 by an overwhelming 60,000 to 600. アリスからしか何ももらってない。 I didn’t receive anything except from Alice. You can also use this grammar with verbs. これから頑張るしかない！ There’s nothing to do but try our best! こうなったら、逃げるしかない。 There no choice but to run away once it turns out like this. もう腐っているから、捨てるしかないよ。 It’s rotten already so there’s nothing to do but throw it out.',
      grammars: ['しか'],
      keywords: ['nothing but','just','only','shika'],
    ),
    Section(
      heading: '「っきゃ」, an alternative to 「しか」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('これ – this'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('こう – (things are) this way'),
                  Text('なる (u-verb) – to become'),
                  Text('もう – already'),
                  Text('やる (u-verb) – to do'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「っきゃ」 is another version of 「しか」 that means essentially the same thing and works exactly the same way. Just substitute 「しか」 with 「っきゃ」 and you’re good to go. This version is a bit stronger than 「しか」 in emphasis but it’s not used nearly as often so I wouldn’t worry about it too much. I briefly cover it here just in case you do run into this expression.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: korekara,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買う',
                              ),
                            ),
                            TextSpan(
                              text: 'っきゃ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('There’s nothing but to buy this!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kou,
                              inlineSpan: const TextSpan(
                                text: 'こう',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なったら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: yaru,
                              inlineSpan: const TextSpan(
                                text: 'やる',
                              ),
                            ),
                            TextSpan(
                              text: 'っきゃ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text(
                          'If things turn out like this, there nothing to do but to just do it!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary これ – this 買う 【か・う】 (u-verb) – to buy ある (u-verb) – to exist (inanimate) こう – (things are) this way なる (u-verb) – to become もう – already やる (u-verb) – to do 「っきゃ」 is another version of 「しか」 that means essentially the same thing and works exactly the same way. Just substitute 「しか」 with 「っきゃ」 and you’re good to go. This version is a bit stronger than 「しか」 in emphasis but it’s not used nearly as often so I wouldn’t worry about it too much. I briefly cover it here just in case you do run into this expression. Examples これは買うっきゃない！ There’s nothing but to buy this! こうなったら、もうやるっきゃない！ If things turn out like this, there nothing to do but to just do it!',
      grammars: ['っきゃ'],
      keywords: ['kkya','just','only'],
    ),
    Section(
      heading: 'Expressing the opposite of 「だけ」 with 「ばかり」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('何 【なに／なん】 – what'),
                  Text('おばさん – middle-aged lady'),
                  Text('嫌 【いや】 (na-adj) disagreeable; unpleasant'),
                  Text('崇 【たかし】 – Takashi (first name)'),
                  Text('～君 【～くん】 – name suffix'),
                  Text('漫画 【まん・が】 – comic book'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('かっこ悪い 【かっこ・わる・い】 (i-adj) – unattractive; uncool'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('麻雀【マー・ジャン】 – mahjong'),
                  Text('直美 【なお・み】 – Naomi (first name)'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('仕事 【し・ごと】 – job'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「ばかり」 is used to express the condition where there’s so much of something to the point where there’s nothing else. Notice this is fundamentally different from 「しか」 which expresses a ',
                  ),
                  TextSpan(
                    text: 'lack',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' of everything else but the item in question. In more casual situations, 「ばかり」 is usually pronounced 「ばっかり」 or just 「ばっか」. For example, let’s say you went to a party to find, much to your dismay, the whole room filled with middle-aged women. You might say the following.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(text: 'だよ！'),
                            VocabTooltip(
                              message: obasan,
                              inlineSpan: const TextSpan(
                                text: 'おばさん',
                              ),
                            ),
                            TextSpan(
                              text: 'ばっかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'じゃないか？'),
                          ],
                        ),
                      ),
                      const Text('What the? Isn’t it nothing but obasan?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Or perhaps a little more girly:',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: iya,
                              inlineSpan: const TextSpan(
                                text: 'いや',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                            VocabTooltip(
                              message: obasan,
                              inlineSpan: const TextSpan(
                                text: 'おばさん',
                              ),
                            ),
                            TextSpan(
                              text: 'ばっかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Eww. It’s nothing but obasan.'),
                    ],
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: takashi,
                              inlineSpan: const TextSpan(
                                text: '崇',
                              ),
                            ),
                            VocabTooltip(
                              message: kun,
                              inlineSpan: const TextSpan(
                                text: '君',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: manga,
                              inlineSpan: const TextSpan(
                                text: '漫画',
                              ),
                            ),
                            TextSpan(
                              text: 'ばっかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読んでて',
                              ),
                            ),
                            const TextSpan(text: 'さ。'),
                            VocabTooltip(
                              message: kakkowarui,
                              inlineSpan: const TextSpan(
                                text: 'かっこ悪い',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Takashi-kun is reading nothing but comic books… He’s so uncool.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'It is quite common in casual speech to end midsentence like this. Notice 「'),
                  VocabTooltip(
                    message: yomu,
                    inlineSpan: const TextSpan(
                      text: '読んでて',
                    ),
                  ),
                  const TextSpan(text: '」 is the te-form of 「'),
                  VocabTooltip(
                    message: yomu,
                    inlineSpan: const TextSpan(
                      text: '読んでいる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with the 「い」 dropped. We assume that the conclusion will come somewhere later in the story.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: maajan,
                              inlineSpan: const TextSpan(
                                text: '麻雀',
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text(
                          'He’s nothing but mahjong. (He does nothing but play mahjong.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: naomi,
                              inlineSpan: const TextSpan(
                                text: '直美',
                              ),
                            ),
                            VocabTooltip(
                              message: chan,
                              inlineSpan: const TextSpan(
                                text: 'ちゃん',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: asobu,
                              inlineSpan: const TextSpan(
                                text: '遊ぶ',
                              ),
                            ),
                            TextSpan(
                              text: 'ばっかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'でしょう！'),
                          ],
                        ),
                      ),
                      const Text(
                          'You’re hanging out with Naomi-chan all the time, aren’t you!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            TextSpan(
                              text: 'ばっか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'だよ。'),
                          ],
                        ),
                      ),
                      const Text('Lately, it’s nothing but work.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 何 【なに／なん】 – what おばさん – middle-aged lady 嫌 【いや】 (na-adj) disagreeable; unpleasant 崇 【たかし】 – Takashi (first name) ～君 【～くん】 – name suffix 漫画 【まん・が】 – comic book 読む 【よ・む】 (u-verb) – to read かっこ悪い 【かっこ・わる・い】 (i-adj) – unattractive; uncool 彼 【かれ】 – he; boyfriend 麻雀【マー・ジャン】 – mahjong 直美 【なお・み】 – Naomi (first name) 遊ぶ 【あそ・ぶ】 (u-verb) – to play 最近 【さい・きん】 – recent; lately 仕事 【し・ごと】 – job 「ばかり」 is used to express the condition where there’s so much of something to the point where there’s nothing else. Notice this is fundamentally different from 「しか」 which expresses a lack of everything else but the item in question. In more casual situations, 「ばかり」 is usually pronounced 「ばっかり」 or just 「ばっか」. For example, let’s say you went to a party to find, much to your dismay, the whole room filled with middle-aged women. You might say the following. 何だよ！おばさんばっかりじゃないか？ What the? Isn’t it nothing but obasan? Or perhaps a little more girly: いやだ。おばさんばっかり。 Eww. It’s nothing but obasan. Examples 崇君は漫画ばっかり読んでてさ。かっこ悪い。 Takashi-kun is reading nothing but comic books… He’s so uncool. It is quite common in casual speech to end midsentence like this. Notice 「読んでて」 is the te-form of 「読んでいる」 with the 「い」 dropped. We assume that the conclusion will come somewhere later in the story. 彼は麻雀ばかりです。 He’s nothing but mahjong. (He does nothing but play mahjong.) 直美ちゃんと遊ぶばっかりでしょう！ You’re hanging out with Naomi-chan all the time, aren’t you! 最近は仕事ばっかだよ。 Lately, it’s nothing but work.',
      grammars: ['ばかり'],
      keywords: ['nothing but','bakari'],
    ),
    Section(
      heading: 'Saying there’s too much of something using 「すぎる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('過ぎる 【す・ぎる】 (ru-verb) – to exceed; to pass'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('太る 【ふと・る】 (u-verb) – to become fatter'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('大きい 【おお・きい】 (i-adj) – big'),
                  Text('面白い 【おも・しろ・い】 (i-adj) – interesting'),
                  Text('もったいない (i-adj) – wasteful'),
                  Text('情けない 【なさ・けない】 (i-adj) – pitiable'),
                  Text('危ない 【あぶ・ない】 (i-adj) – dangerous'),
                  Text('少ない 【すく・ない】 (i-adj) – few'),
                  Text('佐藤 【さ・とう】 – Satou (last name)'),
                  Text('料理 【りょう・り】 – cooking; cuisine; dish'),
                  Text('上手 【じょう・ず】 (na-adj) – skillful'),
                  Text('また – again'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('気 【き】 – mood; intent'),
                  Text('つける – to attach'),
                  Text('気をつける – (expression) to be careful'),
                  Text('トランク – trunk'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('罠 【わな】 – trap'),
                  Text('時間 【じ・かん】 – time'),
                  Text('足りる 【た・りる】 (ru-verb) – to be sufficient'),
                  Text('何【なに】 – what'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('昨晩 【さく・ばん】 – last night'),
                  Text('こと – event, matter'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('覚える 【おぼ・える】 (ru-verb) – to memorize'),
                  Text('それ – that'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: '「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(text: '」 is a regular ru-verb written 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: '過ぎる',
                    ),
                  ),
                  const TextSpan(text: '」 meaning, “to exceed”. When 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is attached to the end of other verbs and adjectives, it means that it is too much or that it has exceeded the normal levels. For verbs, you must directly attach 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                      text: '」 to the stem of the verb. For example, 「'),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べ',
                    ),
                  ),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(text: '」 means “to eat too much” and 「'),
                  VocabTooltip(
                    message: nomu,
                    inlineSpan: const TextSpan(
                      text: '飲み',
                    ),
                  ),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 means “to drink too much”. For adjectives, you just attach it to the end after you remove the last 「い」 from the i-adjectives (as usual). One more rule is that for both negative verbs and adjectives, one must remove the 「い」 from 「ない」 and replace with 「さ」 before attaching 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」. There is no tense (past or non-past) associated with this grammar. Since 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 is a regular ru-verb, this grammar always results in a regular ru-verb.'),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「すぎる」 to indicate there’s too much of something',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const TextSpan(
                                text: ' First change the verb to the ',
                              ),
                              const TextSpan(
                                text: 'stem',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                              const TextSpan(
                                text: ' and attach 「',
                              ),
                              VocabTooltip(
                                message: sugiru,
                                inlineSpan: const TextSpan(
                                  text: 'すぎる',
                                ),
                              ),
                              const TextSpan(text: '」.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: futoru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '太',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: futoru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '太',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: futoru,
                                    inlineSpan: const TextSpan(
                                      text: '太り',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const TextSpan(
                                text: ' Attach 「',
                              ),
                              VocabTooltip(
                                message: sugiru,
                                inlineSpan: const TextSpan(
                                  text: 'すぎる',
                                ),
                              ),
                              const TextSpan(
                                  text:
                                      '」.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shizuka,
                                    inlineSpan: const TextSpan(
                                      text: '静か',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shizuka,
                                    inlineSpan: const TextSpan(
                                      text: '静か',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For i-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const TextSpan(
                                  text:
                                      ' Remove the last 「い」 first before attaching 「'),
                              VocabTooltip(
                                message: sugiru,
                                inlineSpan: const TextSpan(
                                  text: 'すぎる',
                                ),
                              ),
                              const TextSpan(text: '」.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: ookii,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '大き',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: ookii,
                                    inlineSpan: const TextSpan(
                                      text: '大き',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For negative verbs and adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const TextSpan(
                                text:
                                    ' Replace the last 「い」 from 「ない」 with 「さ」 and then attach 「',
                              ),
                              VocabTooltip(
                                message: sugiru,
                                inlineSpan: const TextSpan(
                                  text: 'すぎる',
                                ),
                              ),
                              const TextSpan(text: '」.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べな',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べなさ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '面白くな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '面白くな',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: const TextSpan(
                                      text: '面白くなさ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'I-adjectives that end in 「ない」 which incorporate the negative 「',
                              ),
                              VocabTooltip(
                                message: nai,
                                inlineSpan: const TextSpan(
                                  text: '無い',
                                ),
                              ),
                              const TextSpan(text: '」 such as 「'),
                              VocabTooltip(
                                message: mottainai,
                                inlineSpan: const TextSpan(
                                  text: 'もったいない',
                                ),
                              ),
                              const TextSpan(text: '」（'),
                              VocabTooltip(
                                message: mottainai,
                                inlineSpan: TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: '勿体',
                                    ),
                                    TextSpan(
                                      text: '無い',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TextSpan(text: '） or 「'),
                              VocabTooltip(
                                message: nasakenai,
                                inlineSpan: const TextSpan(
                                  text: '情けない',
                                ),
                              ),
                              const TextSpan(text: '」（'),
                              VocabTooltip(
                                message: nasakenai,
                                inlineSpan: TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: '情け',
                                    ),
                                    TextSpan(
                                      text: '無い',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const TextSpan(text: '） follow the above rule.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: mottainai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'もったいな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: mottainai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'もったいな',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: mottainai,
                                    inlineSpan: const TextSpan(
                                      text: '食べなさ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nasakenai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '情けな',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: nasakenai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '情けな',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: nasakenai,
                                    inlineSpan: const TextSpan(
                                      text: '情けなさ',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Most regular i-adjectives such as 「',
                              ),
                              VocabTooltip(
                                message: abunai,
                                inlineSpan: const TextSpan(
                                  text: '危ない',
                                ),
                              ),
                              const TextSpan(
                                text: '」 or 「',
                              ),
                              VocabTooltip(
                                message: sukunai,
                                inlineSpan: const TextSpan(
                                  text: '少ない',
                                ),
                              ),
                              const TextSpan(
                                  text: '」 follow the regular rule for i-adjectives (rule 3).'),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: abunai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '危な',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: abunai,
                                    inlineSpan: const TextSpan(
                                      text: '危な',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: sukunai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '少な',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: sukunai,
                                    inlineSpan: const TextSpan(
                                      text: '少な',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: sugiru,
                                    inlineSpan: TextSpan(
                                      text: 'すぎる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: satou,
                              inlineSpan: const TextSpan(
                                text: '佐藤',
                              ),
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: const TextSpan(
                                text: 'さん',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ryouri,
                              inlineSpan: const TextSpan(
                                text: '料理',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ryouri,
                              inlineSpan: const TextSpan(
                                text: '上手',
                              ),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: mata,
                              inlineSpan: const TextSpan(
                                text: 'また',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: '過ぎました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Satou-san is good at cooking and I ate too much again.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲み',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: 'すぎない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: kiwotsukeru,
                              inlineSpan: const TextSpan(
                                text: '気をつけて',
                              ),
                            ),
                            const TextSpan(text: 'ね。'),
                          ],
                        ),
                      ),
                      const Text('Be careful to not drink too much, ok?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: TextSpan(
                                text: '大き',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: 'すぎる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: toranku,
                              inlineSpan: const TextSpan(
                                text: 'トランク',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入らない',
                              ),
                            ),
                            const TextSpan(text: 'ぞ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It won’t fit in the trunk cause it’s too big, man.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: TextSpan(
                                text: '静か',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: 'すぎる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                            VocabTooltip(
                              message: wana,
                              inlineSpan: const TextSpan(
                                text: '罠',
                              ),
                            ),
                            const TextSpan(text: 'かもしれないよ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s too quiet. It might be a trap, you know.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: tariru,
                              inlineSpan: TextSpan(
                                text: '足りなさ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: 'すぎて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Due to too much of a lack of time, I couldn’t do anything.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'には、'),
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: mottainai,
                              inlineSpan: TextSpan(
                                text: 'もったいなさ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: TextSpan(
                                text: 'すぎる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'She is totally wasted on him (too good for him).'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'It is also common to change 「'),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(text: '」 into its stem and use it as a noun.'),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sakuban,
                          inlineSpan: const TextSpan(
                            text: '昨晩',
                          ),
                        ),
                        const TextSpan(
                          text: 'の',
                        ),
                        VocabTooltip(
                          message: koto,
                          inlineSpan: const TextSpan(
                            text: 'こと',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: zenzen,
                          inlineSpan: const TextSpan(
                            text: '全然',
                          ),
                        ),
                        VocabTooltip(
                          message: oboeru,
                          inlineSpan: const TextSpan(
                            text: '覚えてない',
                          ),
                        ),
                        const TextSpan(
                          text: 'な。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text(
                      'A: Man, I don’t remember anything about last night.'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sore,
                            inlineSpan: const TextSpan(
                              text: 'それ',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: nomu,
                            inlineSpan: TextSpan(
                              text: '飲み',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: sugiru,
                            inlineSpan: TextSpan(
                              text: 'すぎ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'だよ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('That’s drinking too much.'),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 過ぎる 【す・ぎる】 (ru-verb) – to exceed; to pass 食べる 【た・べる】 (ru-verb) – to eat 飲む 【の・む】 (u-verb) – to drink 太る 【ふと・る】 (u-verb) – to become fatter 静か 【しず・か】 (na-adj) – quiet 大きい 【おお・きい】 (i-adj) – big 面白い 【おも・しろ・い】 (i-adj) – interesting もったいない (i-adj) – wasteful 情けない 【なさ・けない】 (i-adj) – pitiable 危ない 【あぶ・ない】 (i-adj) – dangerous 少ない 【すく・ない】 (i-adj) – few 佐藤 【さ・とう】 – Satou (last name) 料理 【りょう・り】 – cooking; cuisine; dish 上手 【じょう・ず】 (na-adj) – skillful また – again お酒 【お・さけ】 – alcohol 気 【き】 – mood; intent つける – to attach 気をつける – (expression) to be careful トランク – trunk 入る 【はい・る】 (u-verb) – to enter 罠 【わな】 – trap 時間 【じ・かん】 – time 足りる 【た・りる】 (ru-verb) – to be sufficient 何【なに】 – what 出来る 【で・き・る】 (ru-verb) – to be able to do 彼 【かれ】 – he; boyfriend 彼女 【かの・じょ】 – she; girlfriend 昨晩 【さく・ばん】 – last night こと – event, matter 全然 【ぜん・ぜん】 – not at all (when used with negative) 覚える 【おぼ・える】 (ru-verb) – to memorize それ – that 「すぎる」 is a regular ru-verb written 「過ぎる」 meaning, “to exceed”. When 「すぎる」 is attached to the end of other verbs and adjectives, it means that it is too much or that it has exceeded the normal levels. For verbs, you must directly attach 「すぎる」 to the stem of the verb. For example, 「食べすぎる」 means “to eat too much” and 「飲みすぎる」 means “to drink too much”. For adjectives, you just attach it to the end after you remove the last 「い」 from the i-adjectives (as usual). One more rule is that for both negative verbs and adjectives, one must remove the 「い」 from 「ない」 and replace with 「さ」 before attaching 「すぎる」. There is no tense (past or non-past) associated with this grammar. Since 「すぎる」 is a regular ru-verb, this grammar always results in a regular ru-verb. Using 「すぎる」 to indicate there’s too much of something For u-verbs: First change the verb to the stem and attach 「すぎる」. Examples: 食べる → 食べすぎる 太る → 太り → 太りすぎる For na-adjectives: Attach 「すぎる」. Example: 静か → 静かすぎる For i-adjectives: Remove the last 「い」 first before attaching 「すぎる」. Example: 大きい → 大きすぎる For negative verbs and adjectives: Replace the last 「い」 from 「ない」 with 「さ」 and then attach 「すぎる」. Examples: 食べない → 食べなさ → 食べなさすぎる 面白くない → 面白くなさ → 面白くなさすぎる I-adjectives that end in 「ない」 which incorporate the negative 「無い」 such as 「もったいない」（勿体無い） or 「情けない」（情け無い） follow the above rule. Examples: もったいない → もったいなさ → 食べなさすぎる 情けない → 情けなさ → 情けなさすぎる Most regular i-adjectives such as 「危ない」 or 「少ない」 follow the regular rule for i-adjectives (rule 3). Examples: 危ない → 危なすぎる少ない → 少なすぎる Examples 佐藤さんは料理が上手で、また食べ過ぎました。 Satou-san is good at cooking and I ate too much again. お酒を飲みすぎないように気をつけてね。 Be careful to not drink too much, ok? 大きすぎるからトランクに入らないぞ。 It won’t fit in the trunk cause it’s too big, man. 静かすぎる。罠かもしれないよ。 It’s too quiet. It might be a trap, you know. 時間が足りなさすぎて、何もできなかった。 Due to too much of a lack of time, I couldn’t do anything. 彼には、彼女がもったいなさすぎるよ。 She is totally wasted on him (too good for him). It is also common to change 「すぎる」 into its stem and use it as a noun. Ａ：昨晩のこと、全然覚えてないな。 A: A: Man, I don’t remember anything about last night. Ｂ：それは飲みすぎだよ。 B：That’s drinking too much.',
      grammars: ['すぎる'],
      keywords: ['too','too much','exceed','excessive'],
    ),
    Section(
      heading: 'Adding the 「も」 particle to express excessive amounts',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('昨日【きのう】 – yesterday'),
                  Text('電話 【でん・わ】 – phone'),
                  Text('～回 【～かい】 – counter for number of times'),
                  Text('する (exception) – to do'),
                  Text('試験 【し・けん】 – exam'),
                  Text('ため – for the sake/benefit of'),
                  Text('～時間 【～じ・かん】 – counter for span of hour(s)'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('今年 【こ・とし】 – this year'),
                  Text('キロ – kilo'),
                  Text('太る 【ふと・る】 (u-verb) – to become fatter'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'When the 「も」 particle comes after some type of amount, it means that the amount indicated is way too much. For instance, let’s look at the next example.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: denwa,
                              inlineSpan: const TextSpan(
                                text: '電話',
                              ),
                            ),
                            VocabTooltip(
                              message: sankai,
                              inlineSpan: TextSpan(
                                text: '三回',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'も',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(text: 'よ！'),
                          ],
                        ),
                      ),
                      const Text('I called you like three times yesterday!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Notice that the 「も」 particle is attached to the amount “three times”. This sentence implies that the speaker called even three times and still the person didn’t pick up the phone. We understand this to mean that three times are a lot of times to call someone.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shiken,
                              inlineSpan: const TextSpan(
                                text: '試験',
                              ),
                            ),
                            const TextSpan(text: 'のために'),
                            VocabTooltip(
                              message: sanjikan,
                              inlineSpan: TextSpan(
                                text: '三時間',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'も',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I studied three whole hours for the exam.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kotoshi,
                              inlineSpan: const TextSpan(
                                text: '今年',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: juukiro,
                              inlineSpan: TextSpan(
                                text: '十キロ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'も',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: futoru,
                              inlineSpan: const TextSpan(
                                text: '太っちゃった',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('I gained 10 whole kilograms this year!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 昨日【きのう】 – yesterday 電話 【でん・わ】 – phone ～回 【～かい】 – counter for number of times する (exception) – to do 試験 【し・けん】 – exam ため – for the sake/benefit of ～時間 【～じ・かん】 – counter for span of hour(s) 勉強 【べん・きょう】 – study 今年 【こ・とし】 – this year キロ – kilo 太る 【ふと・る】 (u-verb) – to become fatter When the 「も」 particle comes after some type of amount, it means that the amount indicated is way too much. For instance, let’s look at the next example. 昨日、電話三回もしたよ！ I called you like three times yesterday! Notice that the 「も」 particle is attached to the amount “three times”. This sentence implies that the speaker called even three times and still the person didn’t pick up the phone. We understand this to mean that three times are a lot of times to call someone. 試験のために三時間も勉強した。 I studied three whole hours for the exam. 今年、十キロも太っちゃった！ I gained 10 whole kilograms this year!',
      grammars: ['も'],
      keywords: ['mo','excessive','particle'],
    ),
    Section(
      heading: 'Using 「ほど」 to express the extent of something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('程 【ほど】 – degree, extent'),
                  Text('今日 【きょう】 – today'),
                  Text('天気 【てん・き】 – weather'),
                  Text('それ – that'),
                  Text('暑い 【あつ・い】 (i-adj) – hot'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('忙しい 【いそが・しい】 (i-adj) – busy'),
                  Text('韓国 【かん・こく】 – Korea'),
                  Text('料理 【りょう・り】 – cooking; cuisine; dish'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('なる (u-verb) – to become'),
                  Text('歩く 【ある・く】 (u-verb) – to walk'),
                  Text('迷う 【まよ・う】 (u-verb) – to get lost'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('頭 【あたま】 – head'),
                  Text('いい (i-adj) – good'),
                  Text('ハードディスク – hard disk'),
                  Text('容量 【よう・りょう】 – capacity'),
                  Text('大きい 【おお・きい】(i-adj) – big'),
                  Text('もっと – more'),
                  Text('たくさん – a lot (amount)'),
                  Text('曲 【きょく】 – tune'),
                  Text('保存 【ほ・ぞん】 – save'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('航空券 【こう・くう・けん】 – plane ticket'),
                  Text('安い 【やす・い】 (i-adj) – cheap'),
                  Text('限る 【かぎ・る】 (u-verb) – to limit'),
                  Text('文章 【ぶん・しょう】 – sentence; writing'),
                  Text('短い 【みじか・い】 (i-adj) – short'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('良い 【よ・い】 (i-adj) – good'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The noun 「ほど」（程） is attached to a word in a sentence to express the extent of something. It can modify nouns as well as verbs as seen in the next example.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '天気',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: atsui,
                              inlineSpan: const TextSpan(
                                text: '暑くない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Today’s weather is not hot to that extent.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: neruSleep,
                              inlineSpan: const TextSpan(
                                text: '寝る',
                              ),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: isogashii,
                              inlineSpan: const TextSpan(
                                text: '忙しい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Busy to the extent that there’s no time to sleep.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'When you use this with conditionals, you can express something that translates into English as, “The more you [verb], the more…” The grammar is always formed in the following sequence: [conditional of verb] followed immediately by [same verb+ ほど]'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kankoku,
                              inlineSpan: const TextSpan(
                                text: '韓国',
                              ),
                            ),
                            VocabTooltip(
                              message: ryouri,
                              inlineSpan: const TextSpan(
                                text: '料理',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べれば',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'About Korean food, the more you eat the tastier it becomes.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The literal translation is, “About Korean food, if you eat, to the extent that you eat, it becomes tasty.” which essentially means the same thing. The example uses the 「ば」 conditional form, but the 「たら」 conditional will work as well. Since this is a general statement, the contextual 「なら」 conditional will never work. The decided 「と」 conditional won’t work very well here either since it may not always be true depending on the extent of the action.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aruku,
                              inlineSpan: TextSpan(
                                text: '歩いたら',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: aruku,
                              inlineSpan: TextSpan(
                                text: '歩く',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: mayou,
                              inlineSpan: const TextSpan(
                                text: '迷って',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('The more I walked, the more I got lost.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'すれば',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: atama,
                              inlineSpan: const TextSpan(
                                text: '頭',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The more you study, the more you will become smarter.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'You can also use this grammar with i-adjectives by using the 「ば」 conditional.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'iPodは、'),
                            VocabTooltip(
                              message: haadodisuku,
                              inlineSpan: const TextSpan(
                                text: 'ハードディスク',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: youryou,
                              inlineSpan: const TextSpan(
                                text: '容量',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: TextSpan(
                                text: '大きければ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: TextSpan(
                                text: '大きい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: motto,
                              inlineSpan: const TextSpan(
                                text: 'もっと',
                              ),
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kyoku,
                              inlineSpan: const TextSpan(
                                text: '曲',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: hozon,
                              inlineSpan: const TextSpan(
                                text: '保存',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できます',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'About iPod, the larger the hard disk capacity, the more songs you can save.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koukuuken,
                              inlineSpan: const TextSpan(
                                text: '航空券',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yasui,
                              inlineSpan: TextSpan(
                                text: '安ければ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: yasui,
                              inlineSpan: TextSpan(
                                text: '安い',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'とは'),
                            VocabTooltip(
                              message: kagiru,
                              inlineSpan: const TextSpan(
                                text: '限らない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s not necessarily the case that the cheaper the ticket, the better it is.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'For na-adjectives, since you can’t use the 「ば」 conditional you have to resort to the 「なら」 conditional. Because it sounds strange to use the 「なら」 conditional in this fashion, you will hardly ever see this grammar used with na-adjectives. Since 「ほど」 is treated as a noun, make sure you don’t forget to use 「な」 to attach the noun to the na-adjective.'),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bunshou,
                              inlineSpan: const TextSpan(
                                text: '文章',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: mijikai,
                              inlineSpan: const TextSpan(
                                text: '短ければ',
                              ),
                            ),
                            VocabTooltip(
                              message: mijikai,
                              inlineSpan: const TextSpan(
                                text: '短い',
                              ),
                            ),
                            const TextSpan(
                              text: 'ほど',
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: TextSpan(
                                text: '簡単',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'なら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: TextSpan(
                                text: '簡単',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ほど',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: yoi,
                              inlineSpan: const TextSpan(
                                text: 'よい',
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The shorter and simpler the sentences, the better it is.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 程 【ほど】 – degree, extent 今日 【きょう】 – today 天気 【てん・き】 – weather それ – that 暑い 【あつ・い】 (i-adj) – hot 寝る 【ね・る】 (ru-verb) – to sleep 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 忙しい 【いそが・しい】 (i-adj) – busy 韓国 【かん・こく】 – Korea 料理 【りょう・り】 – cooking; cuisine; dish 食べる 【たべ・る】 (ru-verb) – to eat おいしい (i-adj) – tasty なる (u-verb) – to become 歩く 【ある・く】 (u-verb) – to walk 迷う 【まよ・う】 (u-verb) – to get lost 勉強 【べん・きょう】 – study 頭 【あたま】 – head いい (i-adj) – good ハードディスク – hard disk 容量 【よう・りょう】 – capacity 大きい 【おお・きい】(i-adj) – big もっと – more たくさん – a lot (amount) 曲 【きょく】 – tune 保存 【ほ・ぞん】 – save 出来る 【で・き・る】 (ru-verb) – to be able to do 航空券 【こう・くう・けん】 – plane ticket 安い 【やす・い】 (i-adj) – cheap 限る 【かぎ・る】 (u-verb) – to limit 文章 【ぶん・しょう】 – sentence; writing 短い 【みじか・い】 (i-adj) – short 簡単 【かん・たん】 (na-adj) – simple 良い 【よ・い】 (i-adj) – good The noun 「ほど」（程） is attached to a word in a sentence to express the extent of something. It can modify nouns as well as verbs as seen in the next example. 今日の天気はそれほど暑くない。 Today’s weather is not hot to that extent. 寝る時間がないほど忙しい。 Busy to the extent that there’s no time to sleep. When you use this with conditionals, you can express something that translates into English as, “The more you [verb], the more…” The grammar is always formed in the following sequence: [conditional of verb] followed immediately by [same verb+ ほど]韓国料理は食べれば食べるほど、おいしくなる。 About Korean food, the more you eat the tastier it becomes. The literal translation is, “About Korean food, if you eat, to the extent that you eat, it becomes tasty.” which essentially means the same thing. The example uses the 「ば」 conditional form, but the 「たら」 conditional will work as well. Since this is a general statement, the contextual 「なら」 conditional will never work. The decided 「と」 conditional won’t work very well here either since it may not always be true depending on the extent of the action. 歩いたら歩くほど、迷ってしまった。 The more I walked, the more I got lost. 勉強をすればするほど、頭がよくなるよ。 The more you study, the more you will become smarter. You can also use this grammar with i-adjectives by using the 「ば」 conditional. iPod は、ハードディスクの容量が大きければ大きいほどもっとたくさんの曲が保存できます。 About iPod, the larger the hard disk capacity, the more songs you can save. 航空券は安ければ安いほどいいとは限らない。 It’s not necessarily the case that the cheaper the ticket, the better it is. For na-adjectives, since you can’t use the 「ば」 conditional you have to resort to the 「なら」 conditional. Because it sounds strange to use the 「なら」 conditional in this fashion, you will hardly ever see this grammar used with na-adjectives. Since 「ほど」 is treated as a noun, make sure you don’t forget to use 「な」 to attach the noun to the na-adjective. 文章は、短ければ短いほど、簡単なら簡単ほどよいです。 The shorter and simpler the sentences, the better it is.',
      grammars: ['ほど'],
      keywords: ['extent','hodo','conditional'],
    ),
    Section(
      heading: 'Using 「～さ」 with adjectives to indicate an amount',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('高い 【たか・い】 (i-adj) – high; tall; expensive'),
                  Text('低い 【ひく・い】 (i-adj) – short'),
                  Text('穏やか 【おだ・やか】 (na-adj) – calm, peaceful'),
                  Text('この – this （abbr. of これの）'),
                  Text('ビル – building'),
                  Text('何 【なに／なん】 – what'),
                  Text('犬 【いぬ】 – dog'),
                  Text('聴覚 【ちょう・かく】 – sense of hearing'),
                  Text('敏感 【びん・かん】 (na-adj) – sensitive'),
                  Text('人間 【にん・げん】 – human'),
                  Text('比べる 【くら・べる】 (ru-verb) – to compare'),
                  Text('はるか – far more'),
                  Text('上 【うえ】 – above'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We will now learn how to add 「さ」 to adjectives to indicate an amount of that adjective. For example, we can attach 「さ」 to the adjective for “high” in order to get “height”. Instead of looking at the height, we can even attach 「さ」 to the adjective for “low” to focus on the amount of lowness as opposed to the amount of highness. In fact, there is nothing to stop us from using this with any adjective to indicate an amount of that adjective. The result becomes a regular noun indicating the amount of that adjective.'),
                ],
              ),
              NotesSection(
                heading: 'Adding 「～さ」 to adjectives to indicate an amount',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For i-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' First remove the trailing 「い」 from the i-adjective and then attach 「さ」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: takai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '高',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hikui,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '低',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hikui,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '低',
                                        ),
                                        TextSpan(
                                          text: 'さ',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Just attach 「さ」 to the end of the na-adjective.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: odayaka,
                                    inlineSpan: const TextSpan(
                                      text: '穏やか',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: odayaka,
                                    inlineSpan: const TextSpan(
                                      text: '穏やか',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const Text(
                          'The result becomes a regular noun.',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: biru,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: TextSpan(
                                text: '高さ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(text: 'ですか？'),
                          ],
                        ),
                      ),
                      const Text('What is the height of this building?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: choukaku,
                              inlineSpan: const TextSpan(
                                text: '聴覚',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: binkan,
                              inlineSpan: TextSpan(
                                text: '敏感',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'さ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: ningen,
                              inlineSpan: const TextSpan(
                                text: '人間',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kuraberu,
                              inlineSpan: const TextSpan(
                                text: '比べる',
                              ),
                            ),
                            const TextSpan(text: 'と、'),
                            VocabTooltip(
                              message: haruka,
                              inlineSpan: const TextSpan(
                                text: 'はるか',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ue,
                              inlineSpan: const TextSpan(
                                text: '上',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'If you compare the level of sensitivity of hearing of dogs to humans, it is far above.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 高い 【たか・い】 (i-adj) – high; tall; expensive 低い 【ひく・い】 (i-adj) – short 穏やか 【おだ・やか】 (na-adj) – calm, peaceful この – this （abbr. of これの） ビル – building 何 【なに／なん】 – what 犬 【いぬ】 – dog 聴覚 【ちょう・かく】 – sense of hearing 敏感 【びん・かん】 (na-adj) – sensitive 人間 【にん・げん】 – human 比べる 【くら・べる】 (ru-verb) – to compare はるか – far more 上 【うえ】 – above We will now learn how to add 「さ」 to adjectives to indicate an amount of that adjective. For example, we can attach 「さ」 to the adjective for “high” in order to get “height”. Instead of looking at the height, we can even attach 「さ」 to the adjective for “low” to focus on the amount of lowness as opposed to the amount of highness. In fact, there is nothing to stop us from using this with any adjective to indicate an amount of that adjective. The result becomes a regular noun indicating the amount of that adjective. Adding 「～さ」 to adjectives to indicate an amount For i-adjectives: First remove the trailing 「い」 from the i-adjective and then attach 「さ」. Examples: 高い → 高さ低い → 低さ For na-adjectives: Just attach 「さ」 to the end of the na-adjective. Example: 穏やか → 穏やかさ The result becomes a regular noun. Examples このビルの高さは何ですか？ What is the height of this building? 犬の聴覚の敏感さを人間と比べると、はるかに上だ。 If you compare the level of sensitivity of hearing of dogs to humans, it is far above.',
      grammars: ['～さ'],
      keywords: ['sa','adjective',],
    ),
  ],
);
