import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson causePass = Lesson(
  title: 'Causative and Passive Verbs',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We will now learn the last two major types of verb conjugations: causative and passive forms. These two verb conjugations are traditionally covered together because of the notorious causative-passive combination. We will now go over what all these things are and how they are used.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We will now learn the last two major types of verb conjugations: causative and passive forms. These two verb conjugations are traditionally covered together because of the notorious causative-passive combination. We will now go over what all these things are and how they are used.',
      grammars: [],
      keywords: ['causative','passive','causative verb','passive verb'],
    ),
    Section(
      heading: 'Causative verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('あげる (ru-verb) – to give; to raise'),
                  Text('くれる (ru-verb) – to give'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('調べる 【しら・べる】 (ru-verb) – to investigate'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('直る 【なお・る】 (u-verb) – to be fixed'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('学生 【がく・せい】 – student'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('たくさん – a lot (amount)'),
                  Text('質問 【しつ・もん】 – question'),
                  Text('今日 【きょう】 – today'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('休む 【やす・む】 (u-verb) – to rest'),
                  Text('その – abbreviation of 「それの」'),
                  Text('部長 【ぶ・ちょう】 – section manager'),
                  Text('いい (i-adj) – good'),
                  Text('長時間 【ちょう・じ・かん】 – long period of time'),
                  Text('働く 【はたら・く】 (u-verb) – to work'),
                  Text('トイレ – bathroom; toilet'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Verbs conjugated into the causative form are used to indicate an action that someone makes happen. Like Captain Picard so succinctly puts it, the causative verb means to “make it so”. This verb is usually used in the context of making somebody do something. The really confusing thing about the causative verb is that it can also mean to let someone do something. Or maybe this is a different type of verb with the exact same conjugation rules. Whichever the case may be, a verb in the causative form can mean either making or letting someone do something. The only good news is that when the causative form is used with 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(text: 'あげる'),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text:
                        '」, it almost always means to “let someone do”. Once you get used to it, surprisingly, it becomes quite clear which meaning is being used when.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べさせた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Made/Let',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' (someone) eat it all.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べさせて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Let',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' (someone) eat it all.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Here are the conjugation rules for the causative form. All causative verbs become ru-verbs. '),
                      ],
                    ),
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace the last 「る」 with 「させる」.',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the last character as you would for negative verbs but attach 「せる」 instead of 「ない」.',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'する',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'させる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'くる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'こさせる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Causative Conjugation Rules',
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample ru-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Causative'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Causative'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'す',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'させる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kaku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '書',
                                      ),
                                      TextSpan(
                                        text: 'く',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kaku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '書',
                                      ),
                                      TextSpan(
                                        text: 'かせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'ぐ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'がせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぶ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ばせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'つ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'たせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'む',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'ませる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'らせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ぬ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'なせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'う',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'わせる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exception Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Causative'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'させる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'こさせる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Here are some examples using the causative verb. Context will usually tell you which is being meant, but for our purposes we will assume that when the verb is used with 「',
                  ),
                  VocabTooltip(
                    message: ageru,
                    inlineSpan: const TextSpan(text: 'あげる'),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text: '」（',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '） it means “to ',
                  ),
                  const TextSpan(
                    text: 'let',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text: ' someone do” while it means, “to ',
                  ),
                  const TextSpan(
                    text: 'make',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text: ' someone do” when used without it.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'させた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Teacher made students do lots of homework.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: shitsumon,
                              inlineSpan: const TextSpan(
                                text: '質問',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: takusan,
                              inlineSpan: const TextSpan(
                                text: 'たくさん',
                              ),
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: TextSpan(
                                text: '聞かせて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Teacher let (someone) ask lots of questions.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: yasumu,
                              inlineSpan: const TextSpan(
                                text: '休ませて',
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: TextSpan(
                                text: 'ください',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Please let me rest from work today. (Please let me take the day off today.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: buchou,
                              inlineSpan: const TextSpan(
                                text: '部長',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よく',
                              ),
                            ),
                            VocabTooltip(
                              message: choujikan,
                              inlineSpan: const TextSpan(
                                text: '長時間',
                              ),
                            ),
                            VocabTooltip(
                              message: hataraku,
                              inlineSpan: TextSpan(
                                text: '働かせる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'That manager often makes (people) work long hours.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'When asking for permission to let someone do something, it is more common to use the 「～ても',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(text: 'いい'),
                  ),
                  const TextSpan(
                    text: '」 grammar.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: toire,
                              inlineSpan: const TextSpan(
                                text: 'トイレ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かせて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: TextSpan(
                                text: 'くれます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Can you let me go to the bathroom? (Sounds like a prisoner, even in English)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: toire,
                              inlineSpan: const TextSpan(
                                text: 'トイレ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'も',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'いい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Is it ok to go to the bathroom? (No problem here)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あげる (ru-verb) – to give; to raise くれる (ru-verb) – to give 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 着る 【き・る】 (ru-verb) – to wear 信じる 【しん・じる】 (ru-verb) – to believe 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 出る 【で・る】 (ru-verb) – to come out 掛ける 【か・ける】 (ru-verb) – to hang 捨てる 【す・てる】 (ru-verb) – to throw away 調べる 【しら・べる】 (ru-verb) – to investigate 話す 【はな・す】 (u-verb) – to speak 聞く 【き・く】 (u-verb) – to ask; to listen 泳ぐ 【およ・ぐ】 (u-verb) – to swim 遊ぶ 【あそ・ぶ】 (u-verb) – to play 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 直る 【なお・る】 (u-verb) – to be fixed 死ぬ 【し・ぬ】 (u-verb) – to die 買う 【か・う】 (u-verb) – to buy する (exception) – to do 来る 【く・る】 (exception) – to come 先生 【せん・せい】 – teacher 学生 【がく・せい】 – student 宿題 【しゅく・だい】 – homework たくさん – a lot (amount) 質問 【しつ・もん】 – question 今日 【きょう】 – today 仕事 【し・ごと】 – job 休む 【やす・む】 (u-verb) – to rest その – abbreviation of 「それの」部長 【ぶ・ちょう】 – section manager いい (i-adj) – good 長時間 【ちょう・じ・かん】 – long period of time 働く 【はたら・く】 (u-verb) – to work トイレ – bathroom; toilet 行く 【い・く】 (u-verb) – to go Verbs conjugated into the causative form are used to indicate an action that someone makes happen. Like Captain Picard so succinctly puts it, the causative verb means to “make it so”. This verb is usually used in the context of making somebody do something. The really confusing thing about the causative verb is that it can also mean to let someone do something. Or maybe this is a different type of verb with the exact same conjugation rules. Whichever the case may be, a verb in the causative form can mean either making or letting someone do something. The only good news is that when the causative form is used with 「あげる」 and 「くれる」, it almost always means to “let someone do”. Once you get used to it, surprisingly, it becomes quite clear which meaning is being used when. 全部食べさせた。 Made/Let (someone) eat it all. 全部食べさせてくれた。 Let (someone) eat it all. Causative Conjugation Rules Here are the conjugation rules for the causative form. All causative verbs become ru-verbs. For ru-verbs: Replace the last 「る」 with 「させる」. For u-verbs: Change the last character as you would for negative verbs but attach 「せる」 instead of 「ない」. Exceptions: 「する」 becomes 「させる」「くる」 becomes 「こさせる」 Examples Here are some examples using the causative verb. Context will usually tell you which is being meant, but for our purposes we will assume that when the verb is used with 「あげる」 and 「くれる」（ください） it means “to let someone do” while it means, “to make someone do” when used without it. 先生が学生に宿題をたくさんさせた。 Teacher made students do lots of homework. 先生が質問をたくさん聞かせてくれた。 Teacher let (someone) ask lots of questions. 今日は仕事を休ませてください。 Please let me rest from work today. (Please let me take the day off today.) その部長は、よく長時間働かせる。 That manager often makes (people) work long hours. When asking for permission to let someone do something, it is more common to use the 「～てもいい」 grammar. トイレに行かせてくれますか。 Can you let me go to the bathroom? (Sounds like a prisoner, even in English) トイレに行ってもいいですか。 Is it ok to go to the bathroom? (No problem here)',
      grammars: ['～せる'],
      keywords: ['causative','verb','make','let','allow'],
    ),
    Section(
      heading: 'A shorter alternative',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('同じ 【おな・じ】 – same'),
                  Text('こと – event, matter'),
                  Text('何回 【なん・かい】 – how many times'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('お腹 【お・なか】 – stomach'),
                  Text('空く 【あ・く】 (u-verb) – to become empty'),
                  Text('何 【なに／なん】 – what'),
                  Text('くれる (ru-verb) – to give'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'There is a shorter version of the causative conjugation, which I will go over for completeness. However, since this version is mostly used in very rough slang, you are free to skip this section until you’ve had time to get used to the regular form. Also, textbooks usually don’t cover this version of the causative verb.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The key difference in this version is that all verbs become an u-verbs with a 「す」 ending. Therefore, the resulting verb would conjugate just like any other u-verb ending in 「す」 such as 「',
                  ),
                  VocabTooltip(
                    message: hanasu,
                    inlineSpan: const TextSpan(text: '話す'),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: sasu,
                    inlineSpan: const TextSpan(text: '指す'),
                  ),
                  const TextSpan(
                    text:
                        '」. The first part of the conjugation is the same as the original causative form. However, for ru-verbs, instead of attaching 「させる」, you attach 「さす」 and for u-verbs, you attach 「す」 instead of 「せる」. As a result, all the verbs become an u-verb ending in 「す」.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'This form is rarely used so you may just want to stick with the more traditional version of the causative form.'),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace the last 「る」 with 「さす」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'さす',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the last character as you would for negative verbs but attach 「す」 instead of 「ない」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example: ',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'か',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'する',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'さす',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'くる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'こさす',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Shortened Causative Form ',
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: onaji,
                              inlineSpan: const TextSpan(
                                text: '同じ',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: nankai,
                              inlineSpan: const TextSpan(
                                text: '何回',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言わす',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Don’t make me say the same thing again and again!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: onaka,
                              inlineSpan: const TextSpan(
                                text: 'お腹',
                              ),
                            ),
                            VocabTooltip(
                              message: aku,
                              inlineSpan: const TextSpan(
                                text: '空いている',
                              ),
                            ),
                            const TextSpan(
                              text: 'んだから、',
                            ),
                            VocabTooltip(
                              message: nanka,
                              inlineSpan: const TextSpan(
                                text: 'なんか',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べさして',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれ',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I’m hungry so let me eat something.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go する (exception) – to do 来る 【く・る】 (exception) – to come 同じ 【おな・じ】 – same こと – event, matter 何回 【なん・かい】 – how many times 言う 【い・う】 (u-verb) – to say お腹 【お・なか】 – stomach 空く 【あ・く】 (u-verb) – to become empty 何 【なに／なん】 – what くれる (ru-verb) – to give There is a shorter version of the causative conjugation, which I will go over for completeness. However, since this version is mostly used in very rough slang, you are free to skip this section until you’ve had time to get used to the regular form. Also, textbooks usually don’t cover this version of the causative verb. The key difference in this version is that all verbs become an u-verbs with a 「す」 ending. Therefore, the resulting verb would conjugate just like any other u-verb ending in 「す」 such as 「話す」 or 「指す」. The first part of the conjugation is the same as the original causative form. However, for ru-verbs, instead of attaching 「させる」, you attach 「さす」 and for u-verbs, you attach 「す」 instead of 「せる」. As a result, all the verbs become an u-verb ending in 「す」. Shortened Causative Form This form is rarely used so you may just want to stick with the more traditional version of the causative form. For ru-verbs: Replace the last 「る」 with 「さす」. Example: 食べる → 食べさす For u-verbs: Change the last character as you would for negative verbs but attach 「す」 instead of 「ない」. Example: 行く → 行か → 行かす Exceptions: 「する」 becomes 「さす」「くる」 becomes 「こさす」同じことを何回も言わすな！ Don’t make me say the same thing again and again! お腹空いているんだから、なんか食べさしてくれよ。 I’m hungry so let me eat something.',
      grammars: ['～さす'],
      keywords: ['sasu','abbreviation','causative','verb','make','let','allow'],
    ),
    Section(
      heading: 'Passive verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('調べる 【しら・べる】 (ru-verb) – to investigate'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('泳ぐ 【およ・ぐ】 (u-verb) – to swim'),
                  Text('遊ぶ 【あそ・ぶ】 (u-verb) – to play'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('直る 【なお・る】 (u-verb) – to be fixed'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('ポリッジ – porridge'),
                  Text('誰 【だれ】 – who'),
                  Text('皆 【みんな】 – everybody'),
                  Text('変 【へん】 (na-adj) – strange'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('光 【ひかり】 – light'),
                  Text('速い 【はや・い】 (i-adj) – fast'),
                  Text('超える 【こ・える】 (ru-verb) – to exceed'),
                  Text('不可能 【ふ・か・のう】 – impossible'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('この – this （abbr. of これの）'),
                  Text('教科書 【きょう・か・しょ】 – textbook'),
                  Text('多い 【おお・い】 (i-adj) – numerous'),
                  Text('人 【ひと】 – person'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('外国人 【がい・こく・じん】 – foreigner'),
                  Text('質問 【しつ・もん】 – question'),
                  Text('答える 【こた・える】 (ru-verb) – to answer'),
                  Text('パッケージ – package'),
                  Text('あらゆる – all'),
                  Text('含む 【ふく・む】 (u-verb) – to include'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Passive verbs are verbs that are done to the (passive) subject. Unlike English style of writing which discourages the use of the passive form, passive verbs in Japanese are often used in essays and articles.',
                  ),
                ],
              ),
              NotesSection(
                content: BlankList(
                  items: [
                    const Text('All passive verbs become ru-verbs. '),
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Replace the last 「る」 with 「られる」.',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the last character as you would for negative verbs but attach 「れる」 instead of 「ない」.',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'する',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'される',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'くる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」 becomes 「',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'こられる',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '」',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                heading: 'Passive Conjugation Rules',
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample ru-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Passive'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '食べ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '着',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinjiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '信じ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: neruSleep,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '寝',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: okiru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '起き',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: deru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '出',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kakeru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '掛け',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suteru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '捨て',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiraberu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '調べ',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Sample u-verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Passive'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'す',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: hanasu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '話',
                                      ),
                                      TextSpan(
                                        text: 'される',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'く',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '聞',
                                      ),
                                      TextSpan(
                                        text: 'かれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'ぐ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oyogu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '泳',
                                      ),
                                      TextSpan(
                                        text: 'がれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ぶ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: asobu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '遊',
                                      ),
                                      TextSpan(
                                        text: 'ばれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'つ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: matsu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '待',
                                      ),
                                      TextSpan(
                                        text: 'たれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'む',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '飲',
                                      ),
                                      TextSpan(
                                        text: 'まれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'る',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: naoru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '直',
                                      ),
                                      TextSpan(
                                        text: 'られる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'ぬ',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shinu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '死',
                                      ),
                                      TextSpan(
                                        text: 'なれる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'う',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kau,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '買',
                                      ),
                                      TextSpan(
                                        text: 'われる',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  children: [
                    const TableCaption(caption: 'Exception Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(text: 'Plain'),
                            TableHeading(text: 'Passive'),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'される',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'くる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: 'こられる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: porijji,
                              inlineSpan: const TextSpan(
                                text: 'ポリッジ',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: dareka,
                              inlineSpan: const TextSpan(
                                text: '誰か',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べられた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('The porridge was eaten by somebody!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hen,
                              inlineSpan: const TextSpan(
                                text: '変',
                              ),
                            ),
                            const TextSpan(text: 'だと'),
                            VocabTooltip(
                              message: ippun,
                              inlineSpan: TextSpan(
                                text: '言われます',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I am told by everybody that (I’m) strange.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hikari,
                              inlineSpan: const TextSpan(
                                text: '光',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hayasa,
                              inlineSpan: const TextSpan(
                                text: '速さ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: koeru,
                              inlineSpan: const TextSpan(
                                text: '超える',
                              ),
                            ),
                            const TextSpan(text: 'のは、'),
                            VocabTooltip(
                              message: fukanou,
                              inlineSpan: const TextSpan(
                                text: '不可能',
                              ),
                            ),
                            const TextSpan(text: 'だと'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思われる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Exceeding the speed of light is thought to be impossible.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: kyoukasho,
                              inlineSpan: const TextSpan(
                                text: '教科書',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ooku,
                              inlineSpan: const TextSpan(
                                text: '多く',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読まれている',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'This textbook is being read by a large number of people.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gaikokujin,
                              inlineSpan: const TextSpan(
                                text: '外国人',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: shitsumon,
                              inlineSpan: const TextSpan(
                                text: '質問',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: TextSpan(
                                text: '聞かれた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: kotaeru,
                              inlineSpan: const TextSpan(
                                text: '答えられなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I was asked a question by a foreigner but I couldn’t answer.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: pakkeeji,
                              inlineSpan: const TextSpan(
                                text: 'パッケージ',
                              ),
                            ),
                            const TextSpan(text: 'には、'),
                            VocabTooltip(
                              message: arayuru,
                              inlineSpan: const TextSpan(
                                text: 'あらゆる',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: 'もの',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: fukumu,
                              inlineSpan: TextSpan(
                                text: '含まれている',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Everything is included in this package.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 着る 【き・る】 (ru-verb) – to wear 信じる 【しん・じる】 (ru-verb) – to believe 寝る 【ね・る】 (ru-verb) – to sleep 起きる 【お・きる】 (ru-verb) – to wake; to occur 出る 【で・る】 (ru-verb) – to come out 掛ける 【か・ける】 (ru-verb) – to hang 捨てる 【す・てる】 (ru-verb) – to throw away 調べる 【しら・べる】 (ru-verb) – to investigate 話す 【はな・す】 (u-verb) – to speak 聞く 【き・く】 (u-verb) – to ask; to listen 泳ぐ 【およ・ぐ】 (u-verb) – to swim 遊ぶ 【あそ・ぶ】 (u-verb) – to play 待つ 【ま・つ】 (u-verb) – to wait 飲む 【の・む】 (u-verb) – to drink 直る 【なお・る】 (u-verb) – to be fixed 死ぬ 【し・ぬ】 (u-verb) – to die 買う 【か・う】 (u-verb) – to buy する (exception) – to do 来る 【く・る】 (exception) – to come ポリッジ – porridge 誰 【だれ】 – who 皆 【みんな】 – everybody 変 【へん】 (na-adj) – strange 言う 【い・う】 (u-verb) – to say 光 【ひかり】 – light 速い 【はや・い】 (i-adj) – fast 超える 【こ・える】 (ru-verb) – to exceed 不可能 【ふ・か・のう】 – impossible 思う 【おも・う】 (u-verb) – to think この – this （abbr. of これの） 教科書 【きょう・か・しょ】 – textbook 多い 【おお・い】 (i-adj) – numerous 人 【ひと】 – person 読む 【よ・む】 (u-verb) – to read 外国人 【がい・こく・じん】 – foreigner 質問 【しつ・もん】 – question 答える 【こた・える】 (ru-verb) – to answer パッケージ – package あらゆる – all 含む 【ふく・む】 (u-verb) – to include Passive verbs are verbs that are done to the (passive) subject. Unlike English style of writing which discourages the use of the passive form, passive verbs in Japanese are often used in essays and articles. Passive Conjugation Rules All passive verbs become ru-verbs. For ru-verbs: Replace the last 「る」 with 「られる」. For u-verbs: Change the last character as you would for negative verbs but attach 「れる」 instead of 「ない」. Exceptions: 「する」 becomes 「される」「くる」 becomes 「こられる」 Examples ポリッジが誰かに食べられた！ The porridge was eaten by somebody! みんなに変だと言われます。 I am told by everybody that (I’m) strange. 光の速さを超えるのは、不可能だと思われる。 Exceeding the speed of light is thought to be impossible. この教科書は多くの人に読まれている。 This textbook is being read by a large number of people. 外国人に質問を聞かれたが、答えられなかった。 I was asked a question by a foreigner but I couldn’t answer. このパッケージには、あらゆるものが含まれている。 Everything is included in this package.',
      grammars: ['～れる'],
      keywords: ['passive','verb'],
    ),
    Section(
      heading: 'Using passive form to show politeness',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('どう – how'),
                  Text('する (exception) – to do'),
                  Text('領収証 【りょう・しゅう・しょう】 – receipt'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('会議 【かい・ぎ】 – meeting'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'While we will go over various types of grammar that express a politeness level above the normal -masu/-desu forms in the next lesson, it is useful to know that using passive form is another more polite way to express an action. In Japanese, a sentence is usually more polite when it is less direct. For example, it is more polite to refer to someone by his or her name and not by the direct pronoun “you”. It is also more polite to ask a negative question than a positive one. (For example, 「',
                  ),
                  VocabTooltip(
                    message: mono,
                    inlineSpan: const TextSpan(
                      text: 'します',
                    ),
                  ),
                  const TextSpan(
                    text: 'か？」 vs. 「 ',
                  ),
                  VocabTooltip(
                    message: mono,
                    inlineSpan: const TextSpan(
                      text: 'しません',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'か？」) In a similar sense, using the passive form makes the sentence less direct because the subject does not directly perform the action. This makes it sound more polite. Here is the same sentence in increasing degrees of politeness. ',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: dou,
                          inlineSpan: const TextSpan(
                            text: 'どう',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: TextSpan(
                            text: 'する',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        const TextSpan(
                            text: '？ – What will you do? (lit: How do?)'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: dou,
                          inlineSpan: const TextSpan(
                            text: 'どう',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: TextSpan(
                            text: 'します',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        TextSpan(
                          text: 'か',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(text: '？ – Regular polite.'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: dou,
                          inlineSpan: const TextSpan(
                            text: 'どう',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: TextSpan(
                            text: 'されます',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        TextSpan(
                          text: 'か',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(text: '？- Passive polite.'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: dou,
                          inlineSpan: const TextSpan(
                            text: 'どう',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: TextSpan(
                            text: 'なさいます',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        TextSpan(
                          text: 'か',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                            text: '？- Honorific (to be covered next lesson)'),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: dou,
                          inlineSpan: const TextSpan(
                            text: 'どう',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: TextSpan(
                            text: 'なさいます',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                        ),
                        TextSpan(
                          text: 'でしょうか',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                            text:
                                '？- Honorific + a lesser degree of certainty.'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Notice how the same sentence grows longer and longer as you get more and more indirect.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ryoushuushou,
                              inlineSpan: const TextSpan(
                                text: '領収証',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: dou,
                              inlineSpan: const TextSpan(
                                text: 'どう',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'されます',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'か？'),
                          ],
                        ),
                      ),
                      const Text(
                          'What about your receipt? (lit: How will you do receipt?)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kaigi,
                              inlineSpan: const TextSpan(
                                text: '会議',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かれる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'んですか？'),
                          ],
                        ),
                      ),
                      const Text('Are you going to tomorrow’s meeting?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary どう – how する (exception) – to do 領収証 【りょう・しゅう・しょう】 – receipt 明日 【あした】 – tomorrow 会議 【かい・ぎ】 – meeting 行く 【い・く】 (u-verb) – to go While we will go over various types of grammar that express a politeness level above the normal -masu/-desu forms in the next lesson, it is useful to know that using passive form is another more polite way to express an action. In Japanese, a sentence is usually more polite when it is less direct. For example, it is more polite to refer to someone by his or her name and not by the direct pronoun “you”. It is also more polite to ask a negative question than a positive one. (For example, 「しますか？」 vs. 「 しませんか？」) In a similar sense, using the passive form makes the sentence less direct because the subject does not directly perform the action. This makes it sound more polite. Here is the same sentence in increasing degrees of politeness. どうする？ – What will you do? (lit: How do?) どうしますか？ – Regular polite. どうされますか？- Passive polite. どうなさいますか？- Honorific (to be covered next lesson) どうなさいますでしょうか？- Honorific + a lesser degree of certainty. Notice how the same sentence grows longer and longer as you get more and more indirect. Examples 領収証はどうされますか？ What about your receipt? (lit: How will you do receipt?) 明日の会議に行かれるんですか？ Are you going to tomorrow’s meeting?',
      grammars: [],
      keywords: ['passive','verb','polite'],
    ),
    Section(
      heading: 'Causative-passive forms',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('朝ご飯 【あさ・ご・はん】 – breakfast'),
                  Text('日本 【に・ほん】 – Japan'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('こと – event, matter'),
                  Text('多い 【おお・い】 (i-adj) – numerous'),
                  Text('あいつ – that guy (derogatory)'),
                  Text('～時間 【～じ・かん】 – counter for span of hour(s)'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('親 【おや】 – parent'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('する (exception) – to do'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The causative-passive form is simply the combination of causative and passive conjugations to mean that the action of making someone do something was done to that person. This would effectively translate into, “[someone] is made to do [something]”. The important thing to remember is the order of conjugation. The verb is first conjugated to the causative and then passive, never the other way around.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Causative-Passive Conjugation Form',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'The causative-passive verb is formed by first conjugating to the causative form and then by conjugating the result to the passive form.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'させる',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べさせ',
                                        ),
                                        TextSpan(
                                          text: 'られる',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'かせ',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行かせ',
                                        ),
                                        TextSpan(
                                          text: 'られる',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: asagohan,
                              inlineSpan: const TextSpan(
                                text: '朝ご飯',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べたくなかった',
                              ),
                            ),
                            const TextSpan(text: 'のに、'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べさせられた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text:
                                    'Despite not wanting to eat breakfast, I '),
                            TextSpan(
                              text: 'was made to eat',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' it.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(text: 'では、'),
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲ませられる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ooi,
                              inlineSpan: const TextSpan(
                                text: '多い',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'In Japan, the event of '),
                            TextSpan(
                              text: 'being made to drink',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' is numerous.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aitsu,
                              inlineSpan: const TextSpan(
                                text: 'あいつ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: nijikan,
                              inlineSpan: const TextSpan(
                                text: '二時間',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: matsu,
                              inlineSpan: TextSpan(
                                text: '待たせられた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'I '),
                            TextSpan(
                              text: 'was made to wait',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' 2 hours by that guy.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: oya,
                              inlineSpan: const TextSpan(
                                text: '親',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'させられる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'I '),
                            TextSpan(
                              text: 'am made to do homework',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' everyday by my parent(s).'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go 朝ご飯 【あさ・ご・はん】 – breakfast 日本 【に・ほん】 – Japan お酒 【お・さけ】 – alcohol 飲む 【の・む】 (u-verb) – to drink こと – event, matter 多い 【おお・い】 (i-adj) – numerous あいつ – that guy (derogatory)～時間 【～じ・かん】 – counter for span of hour(s) 待つ 【ま・つ】 (u-verb) – to wait 親 【おや】 – parent 宿題 【しゅく・だい】 – homework する (exception) – to do The causative-passive form is simply the combination of causative and passive conjugations to mean that the action of making someone do something was done to that person. This would effectively translate into, “[someone] is made to do [something]”. The important thing to remember is the order of conjugation. The verb is first conjugated to the causative and then passive, never the other way around. Causative-Passive Conjugation Form The causative-passive verb is formed by first conjugating to the causative form and then by conjugating the result to the passive form. Examples: 食べる → 食べさせる → 食べさせられる行く → 行かせる → 行かせられる Examples 朝ご飯は食べたくなかったのに、食べさせられた。 Despite not wanting to eat breakfast, I was made to eat it. 日本では、お酒を飲ませられることが多い。 In Japan, the event of being made to drink is numerous. あいつに二時間も待たせられた。 I was made to wait 2 hours by that guy. 親に毎日宿題をさせられる。 I am made to do homework everyday by my parent(s).',
      grammars: ['～せられる'],
      keywords: ['causative-passive','verb','made to'],
    ),
    Section(
      heading: 'A shorter alternative',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('立つ 【た・つ】 (u-verb) – to stand'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('話す 【はな・す】 (u-verb) – to speak'),
                  Text('学生 【がく・せい】 – student'),
                  Text('廊下 【ろう・か】 – hall, corridor'),
                  Text('日本 【に・ほん】 – Japan'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('こと – event, matter'),
                  Text('多い 【おお・い】 (i-adj) – numerous'),
                  Text('あいつ – that guy (derogatory)'),
                  Text('～時間 【～じ・かん】 – counter for span of hour(s)'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Going along with the shorter causative alternative, you can also use the same conjugation for the causative-passive form. I won’t cover it in too much detail because the usefulness of this form is rather limited just like the shorter causative form itself. The idea is to simply used the shortened causative form instead of using the regular causative conjugation. The rest is the same as before.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Shortened causative-passive form examples',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'First conjugate to the shortened causative form. Then conjugate to the passive form.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'か',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'される',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: tatsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '立',
                                        ),
                                        TextSpan(
                                          text: 'つ',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tatsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '立',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tatsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '立た',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tatsu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '立た',
                                        ),
                                        TextSpan(
                                          text: 'される',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This form cannot be used in cases where the shorter causative form ends in 「さす」, in other words, you can’t have a 「さされる」 ending.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Verbs that cannot be used in this form',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Examples of verbs you ',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              TextSpan(
                                text: 'can’t',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(
                                text: ' use in this form:',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'さす',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'さされる',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hanasu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '話',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hanasu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '話',
                                        ),
                                        TextSpan(
                                          text: 'さす',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hanasu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '話',
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                        TextSpan(
                                          text: 'さされる',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: rouka,
                              inlineSpan: const TextSpan(
                                text: '廊下',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: tatsu,
                              inlineSpan: TextSpan(
                                text: '立たされた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'The student '),
                            TextSpan(
                              text: 'was made to stand',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' in the hall.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(text: 'では、'),
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲まされる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ooi,
                              inlineSpan: const TextSpan(
                                text: '多い',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'In Japan, the event of '),
                            TextSpan(
                              text: 'being made to drink',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' is numerous.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aitsu,
                              inlineSpan: const TextSpan(
                                text: 'あいつ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: nijikan,
                              inlineSpan: const TextSpan(
                                text: '二時間',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: matsu,
                              inlineSpan: TextSpan(
                                text: '待たされた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'I '),
                            TextSpan(
                              text: 'was made to wait',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' 2 hours by that guy.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 行く 【い・く】 (u-verb) – to go 立つ 【た・つ】 (u-verb) – to stand 食べる 【た・べる】 (ru-verb) – to eat 話す 【はな・す】 (u-verb) – to speak 学生 【がく・せい】 – student 廊下 【ろう・か】 – hall, corridor 日本 【に・ほん】 – Japan お酒 【お・さけ】 – alcohol 飲む 【の・む】 (u-verb) – to drink こと – event, matter 多い 【おお・い】 (i-adj) – numerous あいつ – that guy (derogatory)～時間 【～じ・かん】 – counter for span of hour(s) 待つ 【ま・つ】 (u-verb) – to wait Going along with the shorter causative alternative, you can also use the same conjugation for the causative-passive form. I won’t cover it in too much detail because the usefulness of this form is rather limited just like the shorter causative form itself. The idea is to simply used the shortened causative form instead of using the regular causative conjugation. The rest is the same as before. Shortened causative-passive form examples First conjugate to the shortened causative form. Then conjugate to the passive form. Examples: 行く → 行か → 行かす → 行かされる立つ → 立た → 立たす → 立たされる This form cannot be used in cases where the shorter causative form ends in 「さす」, in other words, you can’t have a 「さされる」 ending. Verbs that cannot be used in this form Examples of verbs you can’t use in this form: 食べる → 食べさす → 食べさされる話す → 話さす → 話さされる Examples 学生が廊下に立たされた。 The student was made to stand in the hall. 日本では、お酒を飲まされることが多い。 In Japan, the event of being made to drink is numerous. あいつに二時間も待たされた。 I was made to wait 2 hours by that guy.',
      grammars: ['～される'],
      keywords: ['abbreviation','causative-passive','verb','made to'],
    ),
  ],
);
