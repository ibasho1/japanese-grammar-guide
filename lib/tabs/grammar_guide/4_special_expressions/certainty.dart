import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson certainty = Lesson(
  title: 'Various Degrees of Certainty',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In general, Japanese people don’t assert themselves of something unless they are absolutely sure that it is correct. This accounts for the incredibly frequent use of 「～と',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 and the various grammatical expressions used to express specific levels of certainty. We will go over these expressions starting from the less certain to the most certain.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In general, Japanese people don’t assert themselves of something unless they are absolutely sure that it is correct. This accounts for the incredibly frequent use of 「～と思う」 and the various grammatical expressions used to express specific levels of certainty. We will go over these expressions starting from the less certain to the most certain.',
      grammars: [],
      keywords: ['certainty','various degrees'],
    ),
    Section(
      heading: 'Using 「かもしれない」 to express uncertainty',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('多分 【た・ぶん】 – perhaps; probably'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('学生 【がく・せい】 – student'),
                  Text('それ – that'),
                  Text('面白い 【おも・し・ろい】 (i-adj) – interesting'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('退屈 【たい・くつ】 – boredom'),
                  Text('食堂 【しょく・どう】 – cafeteria'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('雨 【あめ】 – rain'),
                  Text('試合 【し・あい】 – match, game'),
                  Text('中止 【ちゅう・し】 – cancellation'),
                  Text('なる (u-verb) – to become'),
                  Text('この – this （abbr. of これの）'),
                  Text('映画 【えい・が】 – movie'),
                  Text('～回 【～かい】 – counter for number of times'),
                  Text('こと – event, matter'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('あそこ – over there'),
                  Text('代々木公園 【よ・よ・ぎ・こう・えん】 – Yoyogi park'),
                  Text('もう – already'),
                  Text('逃げる 【に・げる】 (ru-verb) – to escape; to run away'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「かもしれない」 is used to mean “maybe” or “possibly” and is less certain than the word 「',
                  ),
                  VocabTooltip(
                    message: tabun,
                    inlineSpan: const TextSpan(
                      text: '多分',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. It attaches to the end of a complete clause. For noun and na-adjective clauses, the declarative 「だ」 must be removed. It can also be written in kanji as 「かも知れない」 and you can treat it the same as a negative ru-verb (there is no positive equivalent) so the masu-form would become 「かもしれません」. In casual speech, it can be abbreviated to just 「かも」. There is also a very masculine version 「かもしれん」, which is simply a different type of negative verb.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Expressing uncertainty with 「かもしれない」',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'Simply attach 「かもしれない」 or 「かも知れない」 to the clause'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: eiga,
                                    inlineSpan: const TextSpan(
                                      text: '映画',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'を',
                                  ),
                                  VocabTooltip(
                                    message: miruWatch,
                                    inlineSpan: const TextSpan(
                                      text: '観た',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'かもしれない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kare,
                                    inlineSpan: const TextSpan(
                                      text: '彼',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'は',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'かもしれない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: sore,
                                    inlineSpan: const TextSpan(
                                      text: 'それ',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'は',
                                  ),
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: const TextSpan(
                                      text: '面白い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'かもしれない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Noun and na-adjective clauses must ',
                              ),
                              TextSpan(
                                text: 'not',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: ' use the declarative 「だ」',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: sensei,
                                    inlineSpan: const TextSpan(
                                      text: '先生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const TextSpan(
                                    text: 'かもしれない',
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: sensei,
                                    inlineSpan: const TextSpan(
                                      text: '先生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'かもしれない',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taikutsu,
                                    inlineSpan: const TextSpan(
                                      text: '退屈',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const TextSpan(
                                    text: 'かもしれない',
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taikutsu,
                                    inlineSpan: const TextSpan(
                                      text: '退屈',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'かもしれない',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'It can be abbreviated to just 「かも」 in casual speech'),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: const TextSpan(
                                      text: '面白い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'かもしれない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: omoshiroi,
                                    inlineSpan: const TextSpan(
                                      text: '面白い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'かも',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'スミスさんは'),
                            VocabTooltip(
                              message: shokudou,
                              inlineSpan: const TextSpan(
                                text: '食堂',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            TextSpan(
                              text: 'かもしれません',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Smith-san may have gone to the cafeteria.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: shiai,
                              inlineSpan: const TextSpan(
                                text: '試合',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: chuushi,
                              inlineSpan: const TextSpan(
                                text: '中止',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            TextSpan(
                              text: 'かもしれない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'ね。'),
                          ],
                        ),
                      ),
                      const Text('The game may become canceled by rain, huh?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ikkai,
                              inlineSpan: const TextSpan(
                                text: '一回',
                              ),
                            ),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: const TextSpan(
                                text: '観た',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            TextSpan(
                              text: 'かも',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('I might have already seen this movie once.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: asoko,
                              inlineSpan: const TextSpan(
                                text: 'あそこ',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: yoyogi,
                              inlineSpan: const TextSpan(
                                text: '代々木',
                              ),
                            ),
                            VocabTooltip(
                              message: kouen,
                              inlineSpan: const TextSpan(
                                text: '公園',
                              ),
                            ),
                            TextSpan(
                              text: 'かもしれない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('That might be Yoyogi park over there.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: nigeru,
                              inlineSpan: const TextSpan(
                                text: '逃げられない',
                              ),
                            ),
                            TextSpan(
                              text: 'かもしれん',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'ぞ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Might not be able to escape anymore, you know.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 多分 【た・ぶん】 – perhaps; probably 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch 彼 【かれ】 – he; boyfriend 学生 【がく・せい】 – student それ – that 面白い 【おも・し・ろい】 (i-adj) – interesting 先生 【せん・せい】 – teacher 退屈 【たい・くつ】 – boredom 食堂 【しょく・どう】 – cafeteria 行く 【い・く】 (u-verb) – to go 雨 【あめ】 – rain 試合 【し・あい】 – match, game 中止 【ちゅう・し】 – cancellation なる (u-verb) – to become この – this （abbr. of これの） 映画 【えい・が】 – movie～回 【～かい】 – counter for number of times こと – event, matter ある (u-verb) – to exist (inanimate) あそこ – over there 代々木公園 【よ・よ・ぎ・こう・えん】 – Yoyogi park もう – already 逃げる 【に・げる】 (ru-verb) – to escape; to run away 「かもしれない」 is used to mean “maybe” or “possibly” and is less certain than the word 「多分」. It attaches to the end of a complete clause. For noun and na-adjective clauses, the declarative 「だ」 must be removed. It can also be written in kanji as 「かも知れない」 and you can treat it the same as a negative ru-verb (there is no positive equivalent) so the masu-form would become 「かもしれません」. In casual speech, it can be abbreviated to just 「かも」. There is also a very masculine version 「かもしれん」, which is simply a different type of negative verb. Expressing uncertainty with 「かもしれない」 Simply attach 「かもしれない」 or 「かも知れない」 to the clause Examples: 映画を観たかもしれない 彼は学生かもしれない それは面白いかもしれない Noun and na-adjective clauses must not use the declarative 「だ」 Examples: 先生だかもしれない → 先生かもしれない退屈だかもしれない → 退屈かもしれない It can be abbreviated to just 「かも」 in casual speech Example: 面白いかもしれない → 面白いかも Examples スミスさんは食堂に行ったかもしれません。 Smith-san may have gone to the cafeteria. 雨で試合は中止になるかもしれないね。 The game may become canceled by rain, huh? この映画は一回観たことあるかも！ I might have already seen this movie once. あそこが代々木公園かもしれない。 That might be Yoyogi park over there. もう逃げられないかもしれんぞ。 Might not be able to escape anymore, you know.',
      grammars: ['かもしれない'],
      keywords: ['kamoshirenai','maybe'],
    ),
    Section(
      heading: 'Using 「でしょう」 to express a fair amount of certainty (polite)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('多分 【た・ぶん】 – perhaps; probably'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('雨 【あめ】 – rain'),
                  Text('学生 【がく・せい】 – student'),
                  Text('これ – this'),
                  Text('どこ – where'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('休む 【やす・む】 (u-verb) – to rest'),
                  Text('いただく (u-verb) – to receive; to eat; to drink (humble)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「でしょう」 is used to express a level of some certainty and is close in meaning to 「',
                  ),
                  VocabTooltip(
                    message: tabun,
                    inlineSpan: const TextSpan(
                      text: '多分',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Just like 「～です／～ます」, it must come at the end of a complete sentence. It does not have any other conjugations. You can also replace 「～ですか」 with 「～でしょうか」 to make the question sound slightly more polite and less assuming by adding a slight level of uncertainty.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            TextSpan(
                              text: 'でしょう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Probably rain tomorrow too.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(text: 'さん'),
                            TextSpan(
                              text: 'でしょう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'か。'),
                          ],
                        ),
                      ),
                      const Text('Are (you) student?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: korekara,
                              inlineSpan: const TextSpan(
                                text: 'これから',
                              ),
                            ),
                            VocabTooltip(
                              message: doko,
                              inlineSpan: const TextSpan(
                                text: 'どこ',
                              ),
                            ),
                            const TextSpan(text: 'へ'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'ん'),
                            TextSpan(
                              text: 'でしょう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'か？'),
                          ],
                        ),
                      ),
                      const Text('Where (are you) going from here?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'If you want to sound really, really polite, you can even add 「～でしょうか」 to the end of a 「～ます」 ending.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yasumu,
                              inlineSpan: const TextSpan(
                                text: '休ませて',
                              ),
                            ),
                            VocabTooltip(
                              message: itadaku,
                              inlineSpan: const TextSpan(
                                text: 'いただけます',
                              ),
                            ),
                            TextSpan(
                              text: 'でしょうか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'May I receive the favor of resting, possibly?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 多分 【た・ぶん】 – perhaps; probably 明日 【あした】 – tomorrow 雨 【あめ】 – rain 学生 【がく・せい】 – student これ – this どこ – where 行く 【い・く】 (u-verb) – to go 休む 【やす・む】 (u-verb) – to rest いただく (u-verb) – to receive; to eat; to drink (humble) 「でしょう」 is used to express a level of some certainty and is close in meaning to 「多分」. Just like 「～です／～ます」, it must come at the end of a complete sentence. It does not have any other conjugations. You can also replace 「～ですか」 with 「～でしょうか」 to make the question sound slightly more polite and less assuming by adding a slight level of uncertainty. Examples 明日も雨でしょう。 Probably rain tomorrow too. 学生さんでしょうか。 Are (you) student? これからどこへ行くんでしょうか？ Where (are you) going from here? If you want to sound really, really polite, you can even add 「～でしょうか」 to the end of a 「～ます」 ending. 休ませていただけますでしょうか。 May I receive the favor of resting, possibly?',
      grammars: ['でしょう'],
      keywords: ['deshou','probably'],
    ),
    Section(
      heading:
          'Using 「でしょう」 and 「だろう」 to express strong amount of certainty (casual)',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('遅刻 【ち・こく】 – tardiness'),
                  Text('する (exception) – to do'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('これ – this'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('掃除 【そう・じ】 – cleaning'),
                  Text('手伝う 【て・つだ・う】 (u-verb) – to help, to assist'),
                  Text('くれる (ru-verb) – to give'),
                  Text('そう – (things are) that way'),
                  Text('どこ – where'),
                  Text('もう – already'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The casual equivalent of 「でしょう」 is surprisingly enough 「でしょう」. However, when you are speaking in a polite manner, the 「でしょう」 is enunciated flatly while in casual speech, it has a rising intonation and can be shortened to 「でしょ」. In addition, since people tend to be more assertive in casual situations, the casual version has a much stronger flavor often sounding more like, “See, I told you so!”',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'あっ！',
                        ),
                        VocabTooltip(
                          message: chikoku,
                          inlineSpan: const TextSpan(
                            text: '遅刻',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: const TextSpan(
                            text: 'しちゃう',
                          ),
                        ),
                        const TextSpan(
                          text: '！',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('Ah! We’re going to be late!'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'だから、',
                          ),
                          VocabTooltip(
                            message: jikan,
                            inlineSpan: const TextSpan(
                              text: '時間',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ない',
                            ),
                          ),
                          const TextSpan(
                            text: 'って',
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: const TextSpan(
                              text: '言った',
                            ),
                          ),
                          TextSpan(
                            text: 'でしょう',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '！',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('That’s why I told you there was no time!'),
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: korekara,
                          inlineSpan: const TextSpan(
                            text: 'これから',
                          ),
                        ),
                        VocabTooltip(
                          message: taberu,
                          inlineSpan: const TextSpan(
                            text: '食べ',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iku,
                          inlineSpan: const TextSpan(
                            text: '行く',
                          ),
                        ),
                        const TextSpan(
                          text: 'ん',
                        ),
                        TextSpan(
                          text: 'でしょ',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('You’re going to eat from now aren’t you?'),
                  english: true,
                ),
              ]),
              const Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text('Ｂ'),
                    text: Text('だったら？'),
                  ),
                  DialogueLine(
                    speaker: Text('B'),
                    text: Text('So what if I am?'),
                  ),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: souji,
                          inlineSpan: const TextSpan(
                            text: '掃除',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: tetsudau,
                          inlineSpan: const TextSpan(
                            text: '手伝って',
                          ),
                        ),
                        VocabTooltip(
                          message: kureru,
                          inlineSpan: const TextSpan(
                            text: 'くれる',
                          ),
                        ),
                        TextSpan(
                          text: 'でしょう',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('You’re going to help me clean, right?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'え？',
                          ),
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(
                            text: 'なの？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Huh? Is that so?'),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「だろう」 means essentially the same thing as 「でしょう」 except that it sounds more masculine and is used mostly by males.',
                  ),
                ],
              ),
              const Heading(text: 'Example 4', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text: 'アリスは',
                        ),
                        VocabTooltip(
                          message: doko,
                          inlineSpan: const TextSpan(
                            text: 'どこ',
                          ),
                        ),
                        const TextSpan(
                          text: 'だ？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('Where is Alice?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: mou,
                            inlineSpan: const TextSpan(
                              text: 'もう',
                            ),
                          ),
                          VocabTooltip(
                            message: neruSleep,
                            inlineSpan: const TextSpan(
                              text: '寝ている',
                            ),
                          ),
                          TextSpan(
                            text: 'だろう',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Probably sleeping already.'),
                  ),
                ],
              ),
              const Heading(text: 'Example 5', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: mou,
                          inlineSpan: const TextSpan(
                            text: 'もう',
                          ),
                        ),
                        VocabTooltip(
                          message: ie,
                          inlineSpan: const TextSpan(
                            text: '家',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: kaeru,
                          inlineSpan: const TextSpan(
                            text: '帰る',
                          ),
                        ),
                        const TextSpan(
                          text: 'ん',
                        ),
                        TextSpan(
                          text: 'だろう',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('You’re going home already, right?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sou,
                            inlineSpan: const TextSpan(
                              text: 'そう',
                            ),
                          ),
                          const TextSpan(
                            text: 'よ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('That’s right.'),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 遅刻 【ち・こく】 – tardiness する (exception) – to do 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) 言う 【い・う】 (u-verb) – to say これ – this 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go 掃除 【そう・じ】 – cleaning 手伝う 【て・つだ・う】 (u-verb) – to help, to assist くれる (ru-verb) – to give そう – (things are) that way どこ – where もう – already 寝る 【ね・る】 (ru-verb) – to sleep 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home The casual equivalent of 「でしょう」 is surprisingly enough 「でしょう」. However, when you are speaking in a polite manner, the 「でしょう」 is enunciated flatly while in casual speech, it has a rising intonation and can be shortened to 「でしょ」. In addition, since people tend to be more assertive in casual situations, the casual version has a much stronger flavor often sounding more like, “See, I told you so!” Example 1 Ａ：あっ！遅刻しちゃう！ A: Ah! We’re going to be late! Ｂ：だから、時間がないって言ったでしょう！ B：That’s why I told you there was no time! Example 2 Ａ：これから食べに行くんでしょ。 A: You’re going to eat from now aren’t you? Ｂ：だったら？ B：So what if I am? Example 3 Ａ：掃除、手伝ってくれるでしょう。 A: You’re going to help me clean, right? Ｂ：え？そうなの？ B：Huh? Is that so? 「だろう」 means essentially the same thing as 「でしょう」 except that it sounds more masculine and is used mostly by males. Example 4 Ａ：アリスはどこだ？ A: Where is Alice? Ｂ：もう寝ているだろう。 B：Probably sleeping already. Example 5 Ａ：もう家に帰るんだろう。 A: You’re going home already, right? Ｂ：そうよ。 B：That’s right.',
      grammars: ['でしょ', 'だろう'],
      keywords: ['deshou','darou','certainly'],
    ),
  ],
);
