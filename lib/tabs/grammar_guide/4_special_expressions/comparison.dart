import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson comparison = Lesson(
  title: 'Using 「方」 and 「よる」',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'If you were wondering how to make comparison in Japanese, well wonder no more. We will learn how to use 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 and 「より」 to make comparisons between two things. We will also learn other uses of 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「よる」 along the way.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'If you were wondering how to make comparison in Japanese, well wonder no more. We will learn how to use 「方」 and 「より」 to make comparisons between two things. We will also learn other uses of 「方」 and 「よる」 along the way.',
      grammars: [],
      keywords: ['comparison','hou','yoru','compare'],
    ),
    Section(
      heading: 'Using 「方」 for comparisons',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      '方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing'),
                  Text('ご飯 【ご・はん】 – rice; meal'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('鈴木 【すず・き】 – Suzuki (last name)'),
                  Text('若い 【わか・い】 (i-adj) – young'),
                  Text('学生 【がく・せい】 – student'),
                  Text('いい (i-adj) – good'),
                  Text('赤ちゃん 【あか・ちゃん】 – baby'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('ゆっくり – slowly'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('健康 【けん・こう】 – health'),
                  Text('こちら – this way'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('怖い 【こわ・い】 (i-adj) – scary'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('そんな – that sort of'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The noun 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is read as 「',
                  ),
                  VocabTooltip(
                    message: hou,
                    inlineSpan: const TextSpan(
                      text: 'ほう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 when it is used to mean a direction or orientation. As an aside, it can also be read as 「',
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: 'かた',
                    ),
                  ),
                  const TextSpan(
                    text: '」 when it is used as a politer version of 「',
                  ),
                  VocabTooltip(
                    message: hito,
                    inlineSpan: const TextSpan(
                      text: '人',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'When we use 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to mean direction, we can use it for comparison by saying one way of things is better, worse, etc., than the other way. Grammatically, it works just like any other regular nouns.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Use it with nouns by utilizing the 「の」 particle.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gohan,
                              inlineSpan: const TextSpan(
                                text: 'ご飯',
                              ),
                            ),
                            TextSpan(
                              text: 'の',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Rice is tastier. (lit: The way of rice is tasty.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suzuki,
                              inlineSpan: const TextSpan(
                                text: '鈴木',
                              ),
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: const TextSpan(
                                text: 'さん',
                              ),
                            ),
                            TextSpan(
                              text: 'の',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: wakai,
                              inlineSpan: const TextSpan(
                                text: '若い',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Suzuki-san is younger. (lit: The way of Suzuki is young.)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Grammatically, it’s no different from a regular noun.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: TextSpan(
                                text: '学生',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s better to not be a student. (lit: The way of not being student is good.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: akachan,
                              inlineSpan: const TextSpan(
                                text: '赤ちゃん',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: TextSpan(
                                text: '静か',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Like quiet babies more. (lit: About babies, the quiet way is desirable.)'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For non-negative verbs, you can also use the past tense to add more certainty and confidence, particularly when making suggestions.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yukkuri,
                              inlineSpan: const TextSpan(
                                text: 'ゆっくり',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kenkou,
                              inlineSpan: const TextSpan(
                                text: '健康',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('It’s better for your health to eat slowly.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kochira,
                              inlineSpan: const TextSpan(
                                text: 'こちら',
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早かった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('It was faster to go from this way.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'The same thing does ',
                  ),
                  TextSpan(
                    text: 'not',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: ' apply for negative verbs.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kowai,
                              inlineSpan: const TextSpan(
                                text: '怖い',
                              ),
                            ),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: TextSpan(
                                text: '観ない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('It’s better not to watch scary movie(s).'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The negative verb is only in the past tense when the comparison is of something that happened in the past.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲まなかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'よかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('It was better not to have drunk that much.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing ご飯 【ご・はん】 – rice; meal おいしい (i-adj) – tasty 鈴木 【すず・き】 – Suzuki (last name) 若い 【わか・い】 (i-adj) – young 学生 【がく・せい】 – student いい (i-adj) – good 赤ちゃん 【あか・ちゃん】 – baby 静か 【しず・か】 (na-adj) – quiet 好き 【す・き】 (na-adj) – likable; desirable ゆっくり – slowly 食べる 【た・べる】 (ru-verb) – to eat 健康 【けん・こう】 – health こちら – this way 行く 【い・く】 (u-verb) – to go 早い 【はや・い】 (i-adj) – fast; early 怖い 【こわ・い】 (i-adj) – scary 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch そんな – that sort of 飲む 【の・む】 (u-verb) – to drink The noun 「方」 is read as 「ほう」 when it is used to mean a direction or orientation. As an aside, it can also be read as 「かた」 when it is used as a politer version of 「人」. When we use 「方」 to mean direction, we can use it for comparison by saying one way of things is better, worse, etc., than the other way. Grammatically, it works just like any other regular nouns. Examples Use it with nouns by utilizing the 「の」 particle. ご飯の方がおいしい。 Rice is tastier. (lit: The way of rice is tasty.) 鈴木さんの方が若い。 Suzuki-san is younger. (lit: The way of Suzuki is young.) Grammatically, it’s no different from a regular noun. 学生じゃない方がいいよ。 It’s better to not be a student. (lit: The way of not being student is good.) 赤ちゃんは、静かな方が好き。 Like quiet babies more. (lit: About babies, the quiet way is desirable.) For non-negative verbs, you can also use the past tense to add more certainty and confidence, particularly when making suggestions. ゆっくり食べた方が健康にいいよ。 It’s better for your health to eat slowly. こちらから行った方が早かった。 It was faster to go from this way. The same thing does not apply for negative verbs. 怖い映画は観ない方がいいよ。 It’s better not to watch scary movie(s). The negative verb is only in the past tense when the comparison is of something that happened in the past. そんなに飲まなかった方がよかった。 It was better not to have drunk that much.',
      grammars: ['方'],
      keywords: ['hou','more'],
    ),
    Section(
      heading: 'Using 「より」 for comparisons',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      '方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing'),
                  Text('花 【はな】 – flower'),
                  Text('ご飯 【ご・はん】 – rice; meal'),
                  Text('パン – bread'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('若い 【わか・い】 (i-adj) – young'),
                  Text('鈴木 【すず・き】 – Suzuki (last name)'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('嫌 【いや】 (na-adj) disagreeable; unpleasant'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('まし – not as bad'),
                  Text('ゆっくり – slowly'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('いい (i-adj) – good'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can think of 「より」 as being the opposite of 「',
                  ),
                  VocabTooltip(
                    message: hou,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. It means, “rather than” or “as opposed to”. It attaches directly to the back of any word. It is usually used in conjunction with 「',
                  ),
                  VocabTooltip(
                    message: hou,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to say something like, “This way is better as opposed to that way.”',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hana,
                              inlineSpan: const TextSpan(
                                text: '花',
                              ),
                            ),
                            const TextSpan(text: 'より'),
                            VocabTooltip(
                              message: dango,
                              inlineSpan: const TextSpan(
                                text: '団子',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Dango rather than flowers. (This is a very famous saying.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gohan,
                              inlineSpan: const TextSpan(
                                text: 'ご飯',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: pan,
                              inlineSpan: TextSpan(
                                text: 'パン',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'より',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Rice tastes better than bread. (lit: The rice way is tasty as opposed to bread.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: pan,
                              inlineSpan: TextSpan(
                                text: 'パン',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: TextSpan(
                                text: 'さん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'より',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suzuki,
                              inlineSpan: const TextSpan(
                                text: '鈴木',
                              ),
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: const TextSpan(
                                text: 'さん',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: wakai,
                              inlineSpan: const TextSpan(
                                text: '若い',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Suzuki-san is younger than Kim-san. (lit: The way of Suzuki is young as opposed to Kim-san.)'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For those curious about the meaning of the proverb, dango is a sweet doughy treat usually sold at festivals. The proverb is saying that people prefer this treat to watching the flowers, referring to the 「',
                  ),
                  VocabTooltip(
                    message: hanami,
                    inlineSpan: const TextSpan(
                      text: '花見',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 event where people go out to see the cherry blossoms (and get smashed). The deeper meaning of the proverb, like all good proverbs, depends on how you apply it.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Of course, there is no rule that 「より」 must be used with 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. The other way of things can be gleaned from context.',
                  ),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: suzuki,
                          inlineSpan: const TextSpan(
                            text: '鈴木',
                          ),
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: mainichi,
                          inlineSpan: const TextSpan(
                            text: '毎日',
                          ),
                        ),
                        VocabTooltip(
                          message: shigoto,
                          inlineSpan: const TextSpan(
                            text: '仕事',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iku,
                          inlineSpan: const TextSpan(
                            text: '行く',
                          ),
                        ),
                        const TextSpan(
                          text: 'のが',
                        ),
                        VocabTooltip(
                          message: iya,
                          inlineSpan: const TextSpan(
                            text: '嫌',
                          ),
                        ),
                        const TextSpan(
                          text: 'だ。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Suzuki'),
                  text: Text('I don’t like going to work everyday.'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('スミス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shigoto,
                            inlineSpan: const TextSpan(
                              text: '仕事',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ない',
                            ),
                          ),
                          TextSpan(
                            text: 'より',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          VocabTooltip(
                            message: mashi,
                            inlineSpan: const TextSpan(
                              text: 'まし',
                            ),
                          ),
                          const TextSpan(
                            text: 'だよ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Smith'),
                    text:
                        Text('It’s not as bad as opposed to not having a job.'),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Words associated with 「より」 do not need any tense. Notice in the following sentence that 「',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 in front of 「より」 is present tense even though 「',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 in front of 「',
                  ),
                  VocabTooltip(
                    message: hou,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is past tense.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yukkuri,
                              inlineSpan: const TextSpan(
                                text: 'ゆっくり',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            const TextSpan(text: 'みた'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'より'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It is better to eat slowly as opposed to eating quickly.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing 花 【はな】 – flower ご飯 【ご・はん】 – rice; meal パン – bread おいしい (i-adj) – tasty 若い 【わか・い】 (i-adj) – young 鈴木 【すず・き】 – Suzuki (last name) 毎日 【まい・にち】 – everyday 仕事 【し・ごと】 – job 嫌 【いや】 (na-adj) disagreeable; unpleasant ある (u-verb) – to exist (inanimate) まし – not as bad ゆっくり – slowly 食べる 【た・べる】 (ru-verb) – to eat 早い 【はや・い】 (i-adj) – fast; early いい (i-adj) – good You can think of 「より」 as being the opposite of 「方」. It means, “rather than” or “as opposed to”. It attaches directly to the back of any word. It is usually used in conjunction with 「方」 to say something like, “This way is better as opposed to that way.” Examples 花より団子。 Dango rather than flowers. (This is a very famous saying.) ご飯の方が、パンよりおいしい。 Rice tastes better than bread. (lit: The rice way is tasty as opposed to bread.) パンさんより鈴木さんの方が若い。 Suzuki-san is younger than Kim-san. (lit: The way of Suzuki is young as opposed to Kim-san.) For those curious about the meaning of the proverb, dango is a sweet doughy treat usually sold at festivals. The proverb is saying that people prefer this treat to watching the flowers, referring to the 「花見」 event where people go out to see the cherry blossoms (and get smashed). The deeper meaning of the proverb, like all good proverbs, depends on how you apply it. Of course, there is no rule that 「より」 must be used with 「方」. The other way of things can be gleaned from context. 鈴木：毎日仕事に行くのが嫌だ。 Suzuki: I don’t like going to work everyday. スミス：仕事がないよりましだよ。 Smith：It’s not as bad as opposed to not having a job. Words associated with 「より」 do not need any tense. Notice in the following sentence that 「食べる」 in front of 「より」 is present tense even though 「食べる」 in front of 「方」 is past tense. ゆっくり食べた方が早くみた食べるよりいい。 It is better to eat slowly as opposed to eating quickly.',
      grammars: ['より'],
      keywords: ['yori','than'],
    ),
    Section(
      heading: 'Using 「より」 as a superlative',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('誰 【だれ】 – who'),
                  Text('何【なに】 – what'),
                  Text('どこ – where'),
                  Text('商品 【しょう・ひん】 – product'),
                  Text('品質 【ひん・しつ】 – quality of a good'),
                  Text('大切 【たい・せつ】 (na-adj) – important'),
                  Text('する (exception) – to do'),
                  Text('この – this （abbr. of これの）'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also use 「より」 with question words such as 「',
                  ),
                  VocabTooltip(
                    message: dare,
                    inlineSpan: const TextSpan(
                      text: '誰',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: nani,
                    inlineSpan: const TextSpan(
                      text: '何',
                    ),
                  ),
                  const TextSpan(
                    text: '」、or 「',
                  ),
                  VocabTooltip(
                    message: doko,
                    inlineSpan: const TextSpan(
                      text: 'どこ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to make a superlative by comparing with everything or everybody else. In this case, though not required, it is common to include the 「も」 particle.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shouhin,
                              inlineSpan: const TextSpan(
                                text: '商品',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hinshitsu,
                              inlineSpan: const TextSpan(
                                text: '品質',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(text: 'より'),
                            VocabTooltip(
                              message: taisetsu,
                              inlineSpan: const TextSpan(
                                text: '大切',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しています',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'We place value in product’s quality over anything else.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: dare,
                              inlineSpan: const TextSpan(
                                text: '誰',
                              ),
                            ),
                            const TextSpan(text: 'よりも'),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できます',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Can do this job more quickly than anyone else.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 誰 【だれ】 – who 何【なに】 – what どこ – where 商品 【しょう・ひん】 – product 品質 【ひん・しつ】 – quality of a good 大切 【たい・せつ】 (na-adj) – important する (exception) – to do この – this （abbr. of これの） 仕事 【し・ごと】 – job 早い 【はや・い】 (i-adj) – fast; early 出来る 【で・き・る】 (ru-verb) – to be able to do You can also use 「より」 with question words such as 「誰」、「何」、or 「どこ」 to make a superlative by comparing with everything or everybody else. In this case, though not required, it is common to include the 「も」 particle. Examples 商品の品質を何より大切にしています。 We place value in product’s quality over anything else. この仕事は誰よりも早くできます。 Can do this job more quickly than anyone else.',
      grammars: ['よりも'],
      keywords: ['yorimo','more than anything'],
    ),
    Section(
      heading: 'Using 「方」 to express a way to do something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      '方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('新宿 【しん・じゅく】 – Shinjuku'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('そう – (things are) that way'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('体 【からだ】 – body'),
                  Text('いい (i-adj) – good'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('教える 【おし・える】 (ru-verb) – to teach; to inform'),
                  Text('くれる (ru-verb) – to give'),
                  Text('パソコン – computer, PC'),
                  Text('使う 【つか・う】 (u-verb) – to use'),
                  Text('皆 【みんな】 – everybody'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also attach 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to the stem of verbs to express a way to do that verb. In this usage, 「',
                  ),
                  VocabTooltip(
                    message: houKata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is read as 「',
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: 'かた',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and the result becomes a noun. For example, 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行き',
                    ),
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」（いきかた） means, “the way to go” or 「',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べ',
                    ),
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」（たべかた）means, “the way to eat”. This expression is probably what you want to use when you want to ask how to do something.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shinjuku,
                              inlineSpan: const TextSpan(
                                text: '新宿',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行き',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kata,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: const TextSpan(
                                text: '分かります',
                              ),
                            ),
                            const TextSpan(text: 'か。'),
                          ],
                        ),
                      ),
                      const Text('Do you know the way to go to Shinjuku?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: souiu,
                              inlineSpan: const TextSpan(
                                text: 'そういう',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kata,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: karada,
                              inlineSpan: const TextSpan(
                                text: '体',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よくない',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Eating in that way is not good for your body.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書き',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kata,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: oshieru,
                              inlineSpan: const TextSpan(
                                text: '教えて',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれます',
                              ),
                            ),
                            const TextSpan(text: 'か？'),
                          ],
                        ),
                      ),
                      const Text('Can you teach me the way of writing kanji?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: pasokon,
                              inlineSpan: const TextSpan(
                                text: 'パソコン',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: tsukau,
                              inlineSpan: TextSpan(
                                text: '使い',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kata,
                              inlineSpan: TextSpan(
                                text: '方',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            VocabTooltip(
                              message: shiru,
                              inlineSpan: const TextSpan(
                                text: '知っている',
                              ),
                            ),
                            const TextSpan(text: 'でしょう。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Probably everybody knows the way to use PC’s.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'When verbs are transformed to this form, the result becomes a noun clause. Sometimes, this requires a change of particles. For instance, while 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行く',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 usually involves a target (the 「に」 or 「へ」 particle), since 「',
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(
                      text: '行き',
                    ),
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is a noun clause, example 1 becomes 「',
                  ),
                  VocabTooltip(
                    message: shinjuku,
                    inlineSpan: const TextSpan(
                      text: '新宿',
                    ),
                  ),
                  TextSpan(
                    text: 'の',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(text: '行き'),
                  ),
                  VocabTooltip(
                    message: kata,
                    inlineSpan: const TextSpan(
                      text: '方',
                    ),
                  ),
                  const TextSpan(
                    text: '」 instead of the familiar 「',
                  ),
                  VocabTooltip(
                    message: shinjuku,
                    inlineSpan: const TextSpan(
                      text: '新宿',
                    ),
                  ),
                  TextSpan(
                    text: 'に',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  VocabTooltip(
                    message: iku,
                    inlineSpan: const TextSpan(text: '行く'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing 行く 【い・く】 (u-verb) – to go 食べる 【た・べる】 (ru-verb) – to eat 新宿 【しん・じゅく】 – Shinjuku 分かる 【わ・かる】 (u-verb) – to understand そう – (things are) that way 言う 【い・う】 (u-verb) – to say 体 【からだ】 – body いい (i-adj) – good 漢字 【かん・じ】 – Kanji 書く 【か・く】 (u-verb) – to write 教える 【おし・える】 (ru-verb) – to teach; to inform くれる (ru-verb) – to give パソコン – computer, PC 使う 【つか・う】 (u-verb) – to use 皆 【みんな】 – everybody 知る 【し・る】 (u-verb) – to know You can also attach 「方」 to the stem of verbs to express a way to do that verb. In this usage, 「方」 is read as 「かた」 and the result becomes a noun. For example, 「行き方」（いきかた） means, “the way to go” or 「食べ方」（たべかた）means, “the way to eat”. This expression is probably what you want to use when you want to ask how to do something. Examples 新宿の行き方は分かりますか。 Do you know the way to go to Shinjuku? そういう食べ方は体によくないよ。 Eating in that way is not good for your body. 漢字の書き方を教えてくれますか？ Can you teach me the way of writing kanji? パソコンの使い方は、みんな知っているでしょう。 Probably everybody knows the way to use PC’s. When verbs are transformed to this form, the result becomes a noun clause. Sometimes, this requires a change of particles. For instance, while 「行く」 usually involves a target (the 「に」 or 「へ」 particle), since 「行き方」 is a noun clause, example 1 becomes 「新宿の行き方」 instead of the familiar 「新宿に行く」.',
      grammars: ['～方'],
      keywords: ['kata','way','how to'],
    ),
    Section(
      heading: 'Using 「によって」 to express dependency',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('人 【ひと】 – person'),
                  Text('話 【はなし】 – story'),
                  Text('違う 【ちが・う】 (u-verb) – to be different'),
                  Text('季節 【き・せつ】 – season'),
                  Text('果物 【くだ・もの】 – fruit'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('なる (u-verb) – to become'),
                  Text('まずい (i-adj) – unpleasant'),
                  Text('和子 【かず・こ】 – Kazuko (first name)'),
                  Text('今日 【きょう】 – today'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('大樹 【だい・き】 – Daiki (first name)'),
                  Text('それ – that'),
                  Text('裕子 【ゆう・こ】 – Yuuko (first name)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When you want to say, “depending on [X]”, you can do this in Japanese by simply attaching 「によって」 to [X].',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hito,
                              inlineSpan: TextSpan(
                                text: '人',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'によって',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hanashi,
                              inlineSpan: const TextSpan(
                                text: '話',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: chigau,
                              inlineSpan: const TextSpan(
                                text: '違う',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The story is different depending on the person.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kisetsu,
                              inlineSpan: TextSpan(
                                text: '季節',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'によって',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: kudamono,
                              inlineSpan: const TextSpan(
                                text: '果物',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なったり',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: mazui,
                              inlineSpan: const TextSpan(
                                text: 'まずく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なったり',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Fruit becomes tasty or nasty depending on the season.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is simply the te-form of 「よる」 as seen by the following simple exchange.',
                  ),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kazuko,
                          inlineSpan: const TextSpan(
                            text: '和子',
                          ),
                        ),
                      ],
                    ),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kyou,
                          inlineSpan: const TextSpan(
                            text: '今日',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: nomu,
                          inlineSpan: const TextSpan(
                            text: '飲み',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: iku,
                          inlineSpan: const TextSpan(
                            text: '行こう',
                          ),
                        ),
                        const TextSpan(
                          text: 'か？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Kazuko'),
                  text: Text('Shall we go drinking today?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: daiki,
                            inlineSpan: const TextSpan(
                              text: '大樹',
                            ),
                          ),
                        ],
                      ),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: sore,
                            inlineSpan: const TextSpan(
                              text: 'それ',
                            ),
                          ),
                          const TextSpan(
                            text: 'は、',
                          ),
                          VocabTooltip(
                            message: yuuko,
                            inlineSpan: const TextSpan(
                              text: '裕子',
                            ),
                          ),
                          const TextSpan(
                            text: 'に',
                          ),
                          VocabTooltip(
                            message: yoru,
                            inlineSpan: TextSpan(
                              text: 'よる',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'ね。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Daiki'),
                    text: Text('That depends on Yuuko.'),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 人 【ひと】 – person 話 【はなし】 – story 違う 【ちが・う】 (u-verb) – to be different 季節 【き・せつ】 – season 果物 【くだ・もの】 – fruit おいしい (i-adj) – tasty なる (u-verb) – to become まずい (i-adj) – unpleasant 和子 【かず・こ】 – Kazuko (first name) 今日 【きょう】 – today 飲む 【の・む】 (u-verb) – to drink 行く 【い・く】 (u-verb) – to go 大樹 【だい・き】 – Daiki (first name) それ – that 裕子 【ゆう・こ】 – Yuuko (first name) When you want to say, “depending on [X]”, you can do this in Japanese by simply attaching 「によって」 to [X]. Examples 人によって話が違う。 The story is different depending on the person. 季節によって果物はおいしくなったり、まずくなったりする。 Fruit becomes tasty or nasty depending on the season. This is simply the te-form of 「よる」 as seen by the following simple exchange. 和子：今日は飲みに行こうか？ Kazuko: Shall we go drinking today? 大樹：それは、裕子によるね。 Daiki：That depends on Yuuko.',
      grammars: ['によって'],
      keywords: ['niyotte','depending on'],
    ),
    Section(
      heading: 'Indicating a source of information using 「によると」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('天気 【てん・き】 – weather'),
                  Text('予報 【よ・ほう】 – forecast'),
                  Text('今日 【きょう】 – today'),
                  Text('雨 【あめ】 – rain'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('話 【はなし】 – story'),
                  Text('朋子 【とも・こ】 – Tomoko (first name)'),
                  Text('やっと – finally'),
                  Text('ボーイフレンド – boyfriend'),
                  Text('見つける 【み・つける】 (ru-verb) – to find'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Another expression using 「よる」 is by using it with the target and the decided conditional 「と」 to indicate a source of information. In English, this would translate to “according to [X]” where 「によると」 is attached to [X].',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tenki,
                              inlineSpan: TextSpan(
                                text: '天気',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: yohou,
                              inlineSpan: TextSpan(
                                text: '予報',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'によると',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            const TextSpan(text: 'だそうだ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'According to the weather forecast, I hear today is rain.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tomodachi,
                              inlineSpan: const TextSpan(
                                text: '友達',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hanashi,
                              inlineSpan: TextSpan(
                                text: '話',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'によると',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: tomoko,
                              inlineSpan: const TextSpan(
                                text: '朋子',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yatto,
                              inlineSpan: const TextSpan(
                                text: 'やっと',
                              ),
                            ),
                            VocabTooltip(
                              message: booifurendo,
                              inlineSpan: const TextSpan(
                                text: 'ボーイフレンド',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: mitsukeru,
                              inlineSpan: const TextSpan(
                                text: '見つけた',
                              ),
                            ),
                            const TextSpan(text: 'らしい。'),
                          ],
                        ),
                      ),
                      const Text(
                          'According to a friend’s story, it appears that Tomoko finally found a boyfriend.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 天気 【てん・き】 – weather 予報 【よ・ほう】 – forecast 今日 【きょう】 – today 雨 【あめ】 – rain 友達 【とも・だち】 – friend 話 【はなし】 – story 朋子 【とも・こ】 – Tomoko (first name) やっと – finally ボーイフレンド – boyfriend 見つける 【み・つける】 (ru-verb) – to find Another expression using 「よる」 is by using it with the target and the decided conditional 「と」 to indicate a source of information. In English, this would translate to “according to [X]” where 「によると」 is attached to [X]. Examples 天気予報によると、今日は雨だそうだ。 According to the weather forecast, I hear today is rain. 友達の話によると、朋子はやっとボーイフレンドを見つけたらしい。 According to a friend’s story, it appears that Tomoko finally found a boyfriend.',
      grammars: ['によると'],
      keywords: ['niyoruto','according to'],
    ),
  ],
);
