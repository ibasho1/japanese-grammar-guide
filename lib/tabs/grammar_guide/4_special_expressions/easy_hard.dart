import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson easyHard = Lesson(
  title: 'Actions That Are Easy or Hard To Do',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(
                text: 'Vocabulary',
                level: 2,
              ),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('しゃべる (u-verb) – to talk'),
                  Text('この – this （abbr. of これの）'),
                  Text('字 【じ】 – character; hand-writing'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('カクテル – cocktail'),
                  Text('ビール – beer'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('部屋 【へ・や】 – room'),
                  Text('暗い 【くら・い】 (i-adj) – dark'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('難しい 【むずか・しい】 (i-adj) – difficult'),
                  Text('易しい 【やさ・しい】 (i-adj) – easy'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('容易 【よう・い】 (na-adj) – simple'),
                  Text('その – that （abbr. of それの）'),
                  Text('肉 【にく】 – meat'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is a short easy lesson on how to transform verbs into adjectives describing whether that action is easy or difficult to do. Basically, it consists of changing the verb into the stem and adding 「やすい」 for easy and 「にくい」 for hard. The result then becomes a regular i-adjective. Pretty easy, huh?',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「～やすい、～にくい」 to describe easy and difficult actions',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'To describe an action as being easy, change the verb to the stem and add 「やすい」. To describe an action as being difficult, attach 「にくい」 to the stem.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'やすい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shaberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'しゃべ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shaberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'しゃべ',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shaberu,
                                    inlineSpan: const TextSpan(
                                      text: 'しゃべり',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'にくい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption:
                                      'The result becomes a regular i-adjective.'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: ' ',
                                      ),
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Non-Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: taberu,
                                                inlineSpan: const TextSpan(
                                                  text: '食べ',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'にく',
                                              ),
                                              TextSpan(
                                                text: 'い',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: taberu,
                                                inlineSpan: const TextSpan(
                                                  text: '食べ',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'にく',
                                              ),
                                              TextSpan(
                                                text: 'くない',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: taberu,
                                                inlineSpan: const TextSpan(
                                                  text: '食べ',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'にく',
                                              ),
                                              TextSpan(
                                                text: 'かった',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: taberu,
                                                inlineSpan: const TextSpan(
                                                  text: '食べ',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'にく',
                                              ),
                                              TextSpan(
                                                text: 'くなかった',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: ji,
                              inlineSpan: const TextSpan(
                                text: '字',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読み',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'にくい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('This hand-writing is hard to read.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kakuteru,
                              inlineSpan: const TextSpan(
                                text: 'カクテル',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: biiru,
                              inlineSpan: const TextSpan(
                                text: 'ビール',
                              ),
                            ),
                            const TextSpan(text: 'より'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲み',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'やすい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Cocktails are easier to drink than beer.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kurai,
                              inlineSpan: const TextSpan(
                                text: '暗かった',
                              ),
                            ),
                            const TextSpan(text: 'ので、'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'にくかった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Since the room was dark, it was hard to see.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'As an aside: Be careful with 「',
                  ),
                  VocabTooltip(
                    message: yomu,
                    inlineSpan: const TextSpan(
                      text: '見',
                    ),
                  ),
                  const TextSpan(
                    text: 'にくい」 because 「',
                  ),
                  VocabTooltip(
                    message: minikui,
                    inlineSpan: const TextSpan(
                      text: '醜い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a rarely used adjective meaning, “ugly”. I wonder if it’s just coincidence that “difficult to see” and “ugly” sound exactly the same?',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Of course, you can always use some other grammatical structure that we have already learned to express the same thing using appropriate adjectives such as 「',
                  ),
                  VocabTooltip(
                    message: muzukashii,
                    inlineSpan: const TextSpan(
                      text: '難しい',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: yasashiiEasy,
                    inlineSpan: const TextSpan(
                      text: '易しい',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: kantan,
                    inlineSpan: const TextSpan(
                      text: '簡単',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: youi,
                    inlineSpan: const TextSpan(
                      text: '容易',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」、etc. The following two sentences are essentially identical in meaning.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'にくい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('That meat is hard to eat.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べる',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'のは',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: muzukashii,
                              inlineSpan: TextSpan(
                                text: '難しい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('The thing of eating that meat is difficult.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat しゃべる (u-verb) – to talk この – this （abbr. of これの） 字 【じ】 – character; hand-writing 読む 【よ・む】 (u-verb) – to read カクテル – cocktail ビール – beer 飲む 【の・む】 (u-verb) – to drink 部屋 【へ・や】 – room 暗い 【くら・い】 (i-adj) – dark 見る 【み・る】 (ru-verb) – to see 難しい 【むずか・しい】 (i-adj) – difficult 易しい 【やさ・しい】 (i-adj) – easy 簡単 【かん・たん】 (na-adj) – simple 容易 【よう・い】 (na-adj) – simple その – that （abbr. of それの） 肉 【にく】 – meat This is a short easy lesson on how to transform verbs into adjectives describing whether that action is easy or difficult to do. Basically, it consists of changing the verb into the stem and adding 「やすい」 for easy and 「にくい」 for hard. The result then becomes a regular i-adjective. Pretty easy, huh? Using 「～やすい、～にくい」 to describe easy and difficult actions To describe an action as being easy, change the verb to the stem and add 「やすい」. To describe an action as being difficult, attach 「にくい」 to the stem. Examples: 食べる → 食べやすい しゃべる → しゃべり → しゃべりにくい Examples この字は読みにくい。 This hand-writing is hard to read. カクテルはビールより飲みやすい。 Cocktails are easier to drink than beer. 部屋が暗かったので、見にくかった。 Since the room was dark, it was hard to see. As an aside: Be careful with 「見にくい」 because 「醜い」 is a rarely used adjective meaning, “ugly”. I wonder if it’s just coincidence that “difficult to see” and “ugly” sound exactly the same? Of course, you can always use some other grammatical structure that we have already learned to express the same thing using appropriate adjectives such as 「難しい」、「易しい」、「簡単」、「容易」、etc. The following two sentences are essentially identical in meaning. その肉は食べにくい。 That meat is hard to eat. その肉を食べるのは難しい。 The thing of eating that meat is difficult.',
      grammars: ['～やすい','～にくい'],
      keywords: ['yasui','nikui','easy','hard','actions'],
    ),
    Section(
      heading: 'Variations of 「～にくい」 with 「～がたい」 and 「～づらい」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                  Text('思い出 【おも・い・で】 – memories'),
                  Text('大切 【たい・せつ】 (na-adj) – important'),
                  Text('する (exception) – to do'),
                  Text('とても – very'),
                  Text('信じる 【しん・じる】 (ru-verb) – to believe'),
                  Text('話 【はなし】 – story'),
                  Text('本当 【ほん・とう】 – real'),
                  Text('起こる 【おこ・る】 (u-verb) – to happen'),
                  Text('辛い【1) から・い; 2) つら・い】 (i-adj) – 1) spicy; 2) painful'),
                  Text('日本語 【に・ほん・ご】 – Japanese (language)'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('待ち合わせ 【ま・ち・あわ・せ】 – meeting arrangement'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('場所 【ば・しょ】 – location'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The kanji for 「にくい」 actually comes from 「',
                  ),
                  VocabTooltip(
                    message: nikuiKatai,
                    inlineSpan: const TextSpan(
                      text: '難い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 which can also be read as 「かたい」. As a result, you can also add a voiced version 「～がたい」 as a verb suffix to express the same thing as 「にくい」. 「にくい」 is more common for speaking while 「がたい」 is more suited for the written medium. 「にくい」 tends to be used for physical actions while 「がたい」 is usually reserved for less physical actions that don’t actually require movement. However, there seems to be no hard rule on which is more appropriate for a given verb so I suggest searching for both versions in google to ascertain the popularity of a given combination. You should also always write the suffix in hiragana to prevent ambiguities in the reading.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'との'),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: TextSpan(
                                text: '忘れ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'がたい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: omoide,
                              inlineSpan: const TextSpan(
                                text: '思い出',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taisetsu,
                              inlineSpan: const TextSpan(
                                text: '大切',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'している',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I am treating importantly the hard to forget memories of and with him.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: shinjiru,
                              inlineSpan: TextSpan(
                                text: '信じ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'がたい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: hanashi,
                              inlineSpan: const TextSpan(
                                text: '話',
                              ),
                            ),
                            const TextSpan(text: 'だが、'),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: okoru,
                              inlineSpan: const TextSpan(
                                text: '起こった',
                              ),
                            ),
                            const TextSpan(text: 'らしい。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s a very difficult to believe story but it seems (from hearsay) that it really happened.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Yet another, more coarse variation of stem + 「にくい」 is to use 「づらい」 instead which is a slightly transformed version of 「',
                  ),
                  VocabTooltip(
                    message: tsurai,
                    inlineSpan: const TextSpan(
                      text: '辛い',
                    ),
                  ),
                  const TextSpan(
                    text: '」（つらい）. This is not to be confused with the same 「',
                  ),
                  VocabTooltip(
                    message: karai,
                    inlineSpan: const TextSpan(
                      text: '辛い',
                    ),
                  ),
                  const TextSpan(
                    text: '」（からい）, which means spicy!',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihongo,
                              inlineSpan: const TextSpan(
                                text: '日本語',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読み',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'づらい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'な。'),
                          ],
                        ),
                      ),
                      const Text('Man, Japanese is hard to read.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: machiawase,
                              inlineSpan: const TextSpan(
                                text: '待ち合わせ',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: TextSpan(
                                text: '分かり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'づらい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: basho,
                              inlineSpan: const TextSpan(
                                text: '場所',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            const TextSpan(text: 'でね。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Please don’t pick a difficult to understand location for the meeting arrangement.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 彼 【かれ】 – he; boyfriend 忘れる 【わす・れる】 (ru-verb) – to forget 思い出 【おも・い・で】 – memories 大切 【たい・せつ】 (na-adj) – important する (exception) – to do とても – very 信じる 【しん・じる】 (ru-verb) – to believe 話 【はなし】 – story 本当 【ほん・とう】 – real 起こる 【おこ・る】 (u-verb) – to happen 辛い【1) から・い; 2) つら・い】 (i-adj) – 1) spicy; 2) painful 日本語 【に・ほん・ご】 – Japanese (language) 読む 【よ・む】 (u-verb) – to read 待ち合わせ 【ま・ち・あわ・せ】 – meeting arrangement 分かる 【わ・かる】 (u-verb) – to understand 場所 【ば・しょ】 – location The kanji for 「にくい」 actually comes from 「難い」 which can also be read as 「かたい」. As a result, you can also add a voiced version 「～がたい」 as a verb suffix to express the same thing as 「にくい」. 「にくい」 is more common for speaking while 「がたい」 is more suited for the written medium. 「にくい」 tends to be used for physical actions while 「がたい」 is usually reserved for less physical actions that don’t actually require movement. However, there seems to be no hard rule on which is more appropriate for a given verb so I suggest searching for both versions in google to ascertain the popularity of a given combination. You should also always write the suffix in hiragana to prevent ambiguities in the reading. Examples 彼との忘れがたい思い出を大切にしている。 I am treating importantly the hard to forget memories of and with him. とても信じがたい話だが、本当に起こったらしい。 It’s a very difficult to believe story but it seems (from hearsay) that it really happened. Yet another, more coarse variation of stem + 「にくい」 is to use 「づらい」 instead which is a slightly transformed version of 「辛い」（つらい）. This is not to be confused with the same 「辛い」（からい）, which means spicy! Examples 日本語は読みづらいな。 Man, Japanese is hard to read. 待ち合わせは、分かりづらい場所にしないでね。 Please don’t pick a difficult to understand location for the meeting arrangement.',
      grammars: ['～がたい','～づらい'],
      keywords: ['gatai','tsurai','hard'],
    ),
  ],
);
