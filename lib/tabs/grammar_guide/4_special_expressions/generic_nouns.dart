import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson genericNoun = Lesson(
  title: 'Special Expressions With Generic Nouns',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We’ve already learned how to use generic nouns in order to modify nouns. Now we will go over some special expression used with generic nouns.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We’ve already learned how to use generic nouns in order to modify nouns. Now we will go over some special expression used with generic nouns.',
      grammars: [],
      keywords: ['generic nouns','special expressions'],
    ),
    Section(
      heading: 'Using 「こと」 to say whether something has happened',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('こと – event, matter'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('徹夜 【てつ・や】 – staying up all night'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('する (exception) – to do'),
                  Text('一人 【ひとり】 – 1 person; alone'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('パリ – Paris'),
                  Text('お寿司 【お・す・し】- sushi'),
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('日本 【に・ほん】 – Japan'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('ヨーロッパ – Europe'),
                  Text('いい (i-adj) – good'),
                  Text('そう – (things are) that way'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('～度 【～ど】 – counter for number of times'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(text: 'When you combine 「'),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(
                      text: '」, the generic word for an event with 「'),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」, you can talk about whether an event exists or not.'),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tetsuya,
                              inlineSpan: const TextSpan(
                                text: '徹夜',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'There are times when I do homework while staying up all night.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hitori,
                              inlineSpan: const TextSpan(
                                text: '一人',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行く',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ありません',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I never go by myself.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Using the past tense of the verb with 「',
                  ),
                  VocabTooltip(
                    message: koto,
                    inlineSpan: const TextSpan(
                      text: 'こと',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, you can talk about whether an event has ever taken place. This is essentially the only way you can say “have done” in Japanese so this is a very useful expression. You need to use this grammar any time you want to talk about whether someone has ever done something.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: pari,
                              inlineSpan: const TextSpan(
                                text: 'パリ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あります',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'か。'),
                          ],
                        ),
                      ),
                      const Text('Have you ever gone to Paris?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: osushi,
                              inlineSpan: const TextSpan(
                                text: 'お寿司',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I’ve had sushi before.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '観た',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'の？'),
                          ],
                        ),
                      ),
                      const Text('You’ve never seen a Japanese movie?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yooroppa,
                              inlineSpan: const TextSpan(
                                text: 'ヨーロッパ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あったら',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'な。'),
                          ],
                        ),
                      ),
                      const Text('It would be nice if I ever go to Europe.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: souiu,
                              inlineSpan: const TextSpan(
                                text: 'そういう',
                              ),
                            ),
                            const TextSpan(text: 'のを'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見た',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'なかった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I had never seen anything like that.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ichido,
                              inlineSpan: const TextSpan(
                                text: '一度',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'んです。'),
                          ],
                        ),
                      ),
                      const Text('I’ve never gone, not even once.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary こと – event, matter ある (u-verb) – to exist (inanimate) 徹夜 【てつ・や】 – staying up all night 宿題 【しゅく・だい】 – homework する (exception) – to do 一人 【ひとり】 – 1 person; alone 行く 【い・く】 (u-verb) – to go パリ – Paris お寿司 【お・す・し】- sushi 食べる 【たべ・る】 (ru-verb) – to eat 日本 【に・ほん】 – Japan 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch ヨーロッパ – Europe いい (i-adj) – good そう – (things are) that way 言う 【い・う】 (u-verb) – to say 見る 【み・る】 (ru-verb) – to see～度 【～ど】 – counter for number of times When you combine 「こと」, the generic word for an event with 「ある」, you can talk about whether an event exists or not. Examples 徹夜して、宿題することはある。 There are times when I do homework while staying up all night. 一人で行くことはありません。 I never go by myself. Using the past tense of the verb with 「こと」, you can talk about whether an event has ever taken place. This is essentially the only way you can say “have done” in Japanese so this is a very useful expression. You need to use this grammar any time you want to talk about whether someone has ever done something. Examples パリに行ったことはありますか。 Have you ever gone to Paris? お寿司を食べたことがある。 I’ve had sushi before. 日本の映画を観たことないの？ You’ve never seen a Japanese movie? ヨーロッパに行ったことがあったらいいな。 It would be nice if I ever go to Europe. そういうのを見たことがなかった。 I had never seen anything like that. 一度行ったこともないんです。 I’ve never gone, not even once.',
      grammars: ['こと'],
      keywords: ['koto','happen'],
    ),
    Section(
      heading: 'Using 「ところ」 as an abstract place',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('所 【ところ】 – place'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('映画 【えい・が】 – movie'),
                  Text('今 【いま】 – now'),
                  Text('ちょうど – just right; exactly'),
                  Text('いい – good'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('優しい 【やさ・しい】 (i-adj) – gentle; kind'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                  Text('終わる 【お・わる】 (u-verb) – to end'),
                  Text('これ – this'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: tokoro,
                    inlineSpan: const TextSpan(
                      text: 'ところ',
                    ),
                  ),
                  const TextSpan(
                    text: '」（',
                  ),
                  VocabTooltip(
                    message: tokoro,
                    inlineSpan: const TextSpan(
                      text: '所',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '） is usually used to indicate a generic physical location. However, it can also hold a much broader meaning ranging from a characteristic to a place in time.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きて',
                              ),
                            ),
                            const TextSpan(text: '。'),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            VocabTooltip(
                              message: choudo,
                              inlineSpan: const TextSpan(
                                text: 'ちょうど',
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: TextSpan(
                                text: 'ところ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'だよ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Come quickly. We’re at the good part of the movie.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yasashii,
                              inlineSpan: const TextSpan(
                                text: '優しい',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: TextSpan(
                                text: 'ところ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('His personality has some gentle parts too.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yasashii,
                              inlineSpan: const TextSpan(
                                text: '優しい',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: TextSpan(
                                text: 'ところ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('His personality has some gentle parts too.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: owaru,
                              inlineSpan: const TextSpan(
                                text: '終った',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: TextSpan(
                                text: 'ところ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text('Class has ended just now.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: korekara,
                              inlineSpan: const TextSpan(
                                text: 'これから',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            VocabTooltip(
                              message: tokoro,
                              inlineSpan: TextSpan(
                                text: 'ところ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'でした。'),
                          ],
                        ),
                      ),
                      const Text('I was just about to go from now.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 所 【ところ】 – place 早い 【はや・い】 (i-adj) – fast; early 来る 【く・る】 (exception) – to come 映画 【えい・が】 – movie 今 【いま】 – now ちょうど – just right; exactly いい – good 彼 【かれ】 – he; boyfriend 優しい 【やさ・しい】 (i-adj) – gentle; kind ある (u-verb) – to exist (inanimate) 授業 【じゅ・ぎょう】 – class 終わる 【お・わる】 (u-verb) – to end これ – this 行く 【い・く】 (u-verb) – to go 「ところ」（所） is usually used to indicate a generic physical location. However, it can also hold a much broader meaning ranging from a characteristic to a place in time. Examples 早くきて。映画は今ちょうどいいところだよ。 Come quickly. We’re at the good part of the movie. 彼は優しいところもあるよ。 His personality has some gentle parts too. 彼は優しいところもあるよ。 His personality has some gentle parts too. 今は授業が終ったところです。 Class has ended just now. これから行くところでした。 I was just about to go from now.',
      grammars: ['ところ'],
      keywords: ['tokoro','place'],
    ),
    Section(
      heading: 'Using 「もの」 as a casual feminine way to emphasize',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('物 【もの】 – object'),
                  Text('どうして – why'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The generic object noun 「',
                  ),
                  VocabTooltip(
                    message: mono,
                    inlineSpan: const TextSpan(
                      text: 'もの',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 can be used as a casual and feminine way of emphasizing something. This is identical to the explanatory feminine emphasis expressed by the 「の」 particle. Just like the explanatory 「の」 particle, the 「の」 is often changed into 「ん」 resulting in 「もん」. Using 「もん」 sounds very feminine and a little cheeky (in a cute way).',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: doushite,
                              inlineSpan: const TextSpan(
                                text: 'どうして',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'こなかった',
                              ),
                            ),
                            const TextSpan(text: 'の？'),
                          ],
                        ),
                      ),
                      const Text('Why didn’t (you) come?'),
                    ],
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あった',
                              ),
                            ),
                            TextSpan(
                              text: 'の',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('(I) had class. [feminine explanatory]'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あった',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: TextSpan(
                                text: 'もの',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('(I) had class. [feminine explanatory]'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あった',
                              ),
                            ),
                            TextSpan(
                              text: 'もん',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(I) had class, so there. [feminine explanatory]'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 物 【もの】 – object どうして – why 来る 【く・る】 (exception) – to come 授業 【じゅ・ぎょう】 – class ある (u-verb) – to exist (inanimate) The generic object noun 「もの」 can be used as a casual and feminine way of emphasizing something. This is identical to the explanatory feminine emphasis expressed by the 「の」 particle. Just like the explanatory 「の」 particle, the 「の」 is often changed into 「ん」 resulting in 「もん」. Using 「もん」 sounds very feminine and a little cheeky (in a cute way). Examples どうしてこなかったの？ Why didn’t (you) come? 授業があったの。(I) had class. [feminine explanatory]授業があったもの。(I) had class. [feminine explanatory]授業があったもん。(I) had class, so there. [feminine explanatory]',
      grammars: ['もの','もん'],
      keywords: ['mono','mon','feminine','emphasis','explanatory'],
    ),
  ],
);
