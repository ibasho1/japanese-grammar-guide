import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson honorific = Lesson(
  title: 'Honorific and Humble Forms',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Japanese can be roughly separated into three levels of politeness: casual, polite, and honorific/humble. So far, we have already gone over the polite forms using 「～です」 and 「～ます」. We will now cover the next level of politeness using honorific and humble forms. You will often hear this type of language in any customer/consumer type situations such as fast food counters, restaurants, etc. For now, the first thing to remember is that the speaker always considers himself/herself to be at the lowest level. So any actions performed by oneself are in humble form while actions performed by anyone else seen from the view of the speaker uses the honorific form.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Japanese can be roughly separated into three levels of politeness: casual, polite, and honorific/humble. So far, we have already gone over the polite forms using 「～です」 and 「～ます」. We will now cover the next level of politeness using honorific and humble forms. You will often hear this type of language in any customer/consumer type situations such as fast food counters, restaurants, etc. For now, the first thing to remember is that the speaker always considers himself/herself to be at the lowest level. So any actions performed by oneself are in humble form while actions performed by anyone else seen from the view of the speaker uses the honorific form.',
      grammars: ['尊敬語','謙譲語'],
      keywords: ['honorific','humble','humble form','honorific form'],
    ),
    Section(
      heading: 'Set expressions',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('なさる – to do (honorific)'),
                  Text('致す 【いた・す】 (u-verb) – to do (humble)'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('いらっしゃる – to be; to go; to come (honorific)'),
                  Text('おいでになる – to be; to go; to come (honorific)'),
                  Text('参る 【まい・る】 (u-verb) – to go; to come (humble)'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('おる (ru-verb) – to exist (animate) (humble)'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('ご覧になる 【ご・らん・になる】 – to see (honorific)'),
                  Text('拝見する 【はい・けん・する】 – to see (humble)'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('伺う 【うかが・う】 (u-verb) – to ask; to listen (humble)'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('おっしゃる – to say (honorific)'),
                  Text('申す 【もう・す】 (u-verb) – to say (humble)'),
                  Text('申し上げる 【もう・し・あ・げる】 (u-verb) – to say (humble)'),
                  Text('あげる (ru-verb) – to give; to raise'),
                  Text(
                      '差し上げる 【さ・し・あ・げる】 (ru-verb) – to give; to raise (humble)'),
                  Text('くれる (ru-verb) – to give'),
                  Text('下さる 【くだ・さる】 – to give (honorific)'),
                  Text('もらう (u-verb) – to receive'),
                  Text('いただく (u-verb) – to receive; to eat; to drink (humble)'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text(
                      '召し上がる 【め・し・あ・がる】 (u-verb) – to eat; to drink (honorific)'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('ご存じ 【ご・ぞん・じ】 – knowing (honorific)'),
                  Text('存じる 【ぞん・じる】 (ru-verb) – to know (humble)'),
                  Text('ござる – to be (formal)'),
                  Text('もう – already'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('何 【なに／なん】 – what'),
                  Text('推薦状 【すい・せん・じょう】 – letter of recommendation'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('どちら – which way'),
                  Text('今日 【きょう】 – today'),
                  Text('私 【わたし】 – me; myself; I'),
                  Text('レポート – report'),
                  Text('失礼 【しつ・れい】 – discourtesy'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The difficult part of learning honorific and humble language is that there are a number of words that have separate verbs for honorific and humble forms. Anything that does not have its own special expression fall under the general rules of humble and honorific conjugations that we will cover next.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableHeading(text: 'Honorific and Humble Verbs'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'Plain',
                            ),
                            TableHeading(
                              text: 'Honorific',
                            ),
                            TableHeading(
                              text: 'Humble',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: suru,
                                  inlineSpan: const TextSpan(
                                    text: 'する',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: const TextSpan(
                                    text: 'なさる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: itasu,
                                  inlineSpan: const TextSpan(
                                    text: '致す',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iku,
                                  inlineSpan: const TextSpan(
                                    text: '行く',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: irassharu,
                                      inlineSpan: const TextSpan(
                                        text: 'いらっしゃる',
                                      ),
                                    ),
                                    const TextSpan(text: '／'),
                                    VocabTooltip(
                                      message: oideninaru,
                                      inlineSpan: const TextSpan(
                                        text: 'おいでになる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: mairu,
                                  inlineSpan: const TextSpan(
                                    text: '参る',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kuru,
                                  inlineSpan: const TextSpan(
                                    text: '来る',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: irassharu,
                                      inlineSpan: const TextSpan(
                                        text: 'いらっしゃる',
                                      ),
                                    ),
                                    const TextSpan(text: '／'),
                                    VocabTooltip(
                                      message: oideninaru,
                                      inlineSpan: const TextSpan(
                                        text: 'おいでになる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: mairu,
                                  inlineSpan: const TextSpan(
                                    text: '参る',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iru,
                                  inlineSpan: const TextSpan(
                                    text: 'いる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: irassharu,
                                      inlineSpan: const TextSpan(
                                        text: 'いらっしゃる',
                                      ),
                                    ),
                                    const TextSpan(text: '／'),
                                    VocabTooltip(
                                      message: oideninaru,
                                      inlineSpan: const TextSpan(
                                        text: 'おいでになる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: oru,
                                  inlineSpan: const TextSpan(
                                    text: 'おる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: miru,
                                  inlineSpan: const TextSpan(
                                    text: '見る',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: goran,
                                      inlineSpan: const TextSpan(
                                        text: 'ご覧',
                                      ),
                                    ),
                                    const TextSpan(text: 'に'),
                                    VocabTooltip(
                                      message: naru,
                                      inlineSpan: const TextSpan(
                                        text: 'なる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: haiken,
                                      inlineSpan: const TextSpan(
                                        text: '拝見',
                                      ),
                                    ),
                                    VocabTooltip(
                                      message: suru,
                                      inlineSpan: const TextSpan(
                                        text: 'する',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kiku,
                                  inlineSpan: const TextSpan(
                                    text: '聞く',
                                  ),
                                ),
                              ),
                            ),
                            const TableData(
                              content: Text.rich(
                                TextSpan(
                                  text: '－',
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ukagau,
                                  inlineSpan: const TextSpan(
                                    text: '伺う',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: iu,
                                  inlineSpan: const TextSpan(
                                    text: '言う',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: const TextSpan(
                                    text: 'おっしゃる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: mousu,
                                      inlineSpan: const TextSpan(
                                        text: '申す',
                                      ),
                                    ),
                                    const TextSpan(text: '／'),
                                    VocabTooltip(
                                      message: moushiageru,
                                      inlineSpan: const TextSpan(
                                        text: '申し上げる',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ageru,
                                  inlineSpan: const TextSpan(
                                    text: 'あげる',
                                  ),
                                ),
                              ),
                            ),
                            const TableData(
                              content: Text.rich(
                                TextSpan(
                                  text: '－',
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: sashiageru,
                                  inlineSpan: const TextSpan(
                                    text: '差し上げる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kureru,
                                  inlineSpan: const TextSpan(
                                    text: 'くれる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: const TextSpan(
                                    text: '下さる',
                                  ),
                                ),
                              ),
                            ),
                            const TableData(
                              content: Text.rich(
                                TextSpan(
                                  text: '－',
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: morau,
                                  inlineSpan: const TextSpan(
                                    text: 'もらう',
                                  ),
                                ),
                              ),
                            ),
                            const TableData(
                              content: Text.rich(
                                TextSpan(
                                  text: '－',
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: itadaku,
                                  inlineSpan: const TextSpan(
                                    text: 'いただく',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: taberu,
                                  inlineSpan: const TextSpan(
                                    text: '食べる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: meshiagaru,
                                  inlineSpan: const TextSpan(
                                    text: '召し上がる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: itadaku,
                                  inlineSpan: const TextSpan(
                                    text: 'いただく',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nomu,
                                  inlineSpan: const TextSpan(
                                    text: '飲む',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: meshiagaru,
                                  inlineSpan: const TextSpan(
                                    text: '召し上がる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: itadaku,
                                  inlineSpan: const TextSpan(
                                    text: 'いただく',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: shiru,
                                  inlineSpan: const TextSpan(
                                    text: '知っている',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                TextSpan(
                                  children: [
                                    VocabTooltip(
                                      message: gosonji,
                                      inlineSpan: const TextSpan(
                                        text: 'ご存知',
                                      ),
                                    ),
                                    const TextSpan(text: '（です）'),
                                  ],
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: sonjiru,
                                  inlineSpan: const TextSpan(
                                    text: '存じる',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(
                  text: 'Honorific verbs with special conjugations', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'A number of these verbs ',
                  ),
                  const TextSpan(
                    text: 'do not',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text:
                        ' follow the normal masu-conjugation rules and they include: 「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(text: 'なさる'),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: irassharu,
                    inlineSpan: const TextSpan(text: 'いらっしゃる'),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: ossharu,
                    inlineSpan: const TextSpan(text: 'おっしゃる'),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(text: '下さる'),
                  ),
                  const TextSpan(
                    text: '」、 and 「',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ござる'),
                  ),
                  const TextSpan(
                    text:
                        '」 (which we will soon cover). For all masu-form tenses of these verbs, instead of the 「る」 becoming a 「り」 as it does with normal u-verbs, it instead becomes an 「い」. All other conjugations besides the masu-form do not change from regular u-verbs.',
                  ),
                ],
              ),
              Padding(
                padding: kDefaultParagraphPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TableHeading(text: 'ます-conjugations'),
                    Table(
                      border: TableBorder.all(
                        color: Theme.of(context).colorScheme.outline,
                      ),
                      children: [
                        const TableRow(
                          children: [
                            TableHeading(
                              text: 'Plain',
                            ),
                            TableHeading(
                              text: 'ます-form',
                            ),
                            TableHeading(
                              text: 'Past ます-form',
                            ),
                            TableHeading(
                              text: 'Negative ます-form',
                            ),
                            TableHeading(
                              text: 'Past-negative ます-form',
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: const TextSpan(
                                    text: 'なさる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'なさ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ます',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'なさ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ました',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'なさ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ません',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: nasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'なさ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ませんでした',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: irassharu,
                                  inlineSpan: const TextSpan(
                                    text: 'いらっしゃる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: irassharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'いらっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ます',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: irassharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'いらっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ました',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: irassharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'いらっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ません',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: irassharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'いらっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ませんでした',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: const TextSpan(
                                    text: 'おっしゃる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'おっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ます',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'おっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ました',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'おっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ません',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: ossharu,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'おっしゃ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ませんでした',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: const TextSpan(
                                    text: '下さる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '下さ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ます',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '下さ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ました',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '下さ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ません',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: kudasaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: '下さ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ませんでした',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: gozaru,
                                  inlineSpan: const TextSpan(
                                    text: 'ござる',
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: gozaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'ござ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ます',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: gozaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'ござ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ました',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: gozaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'ござ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ません',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            TableData(
                              content: Text.rich(
                                VocabTooltip(
                                  message: gozaru,
                                  inlineSpan: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'ござ',
                                      ),
                                      TextSpan(
                                        text: 'い',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                        ),
                                      ),
                                      const TextSpan(
                                        text: 'ませんでした',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples of honorific form', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We can now begin to see that 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」 is just a special conjugation of 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(text: '下さる'),
                  ),
                  const TextSpan(
                    text: '」 which is the honorific version of 「',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(text: 'くれる'),
                  ),
                  const TextSpan(
                    text:
                        '」. Let’s look at some actual examples. Since these examples are all questions directed directly to someone (second person), they all use the honorific form.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'アリスさん、',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: meshiagaru,
                              inlineSpan: TextSpan(
                                text: '召し上がりました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Alice-san, did (you) eat already?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: nasaru,
                              inlineSpan: TextSpan(
                                text: 'なさっている',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'んですか。',
                            ),
                          ],
                        ),
                      ),
                      const Text('What are you doing at work?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suisenjou,
                              inlineSpan: const TextSpan(
                                text: '推薦状',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: const TextSpan(
                                text: '書いて',
                              ),
                            ),
                            VocabTooltip(
                              message: kudasaru,
                              inlineSpan: TextSpan(
                                text: 'くださる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'んです',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'You’re going to give me the favor of writing a recommendation letter?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: dochira,
                              inlineSpan: const TextSpan(
                                text: 'どちら',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: irassharu,
                              inlineSpan: TextSpan(
                                text: 'いらっしゃいました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Where did you come from?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: dochira,
                              inlineSpan: const TextSpan(
                                text: 'どちら',
                              ),
                            ),
                            const TextSpan(
                              text: 'へ',
                            ),
                            VocabTooltip(
                              message: irassharu,
                              inlineSpan: TextSpan(
                                text: 'いらっしゃいます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Where are you going today?'),
                    ],
                  ),
                ],
              ),
              const Heading(text: 'Examples of humble form', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The following examples are all actions done by the speaker so they all use the humble form.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'はキムと',
                            ),
                            VocabTooltip(
                              message: mousu,
                              inlineSpan: TextSpan(
                                text: '申します',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'As for me, (people) say Kim. (I am called Kim.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: const TextSpan(
                                text: '書いた',
                              ),
                            ),
                            VocabTooltip(
                              message: repooto,
                              inlineSpan: const TextSpan(
                                text: 'レポート',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見て',
                              ),
                            ),
                            VocabTooltip(
                              message: itadaku,
                              inlineSpan: TextSpan(
                                text: 'いただけます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Will I be able to receive the favor of getting my report looked at?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shitsurei,
                              inlineSpan: const TextSpan(
                                text: '失礼',
                              ),
                            ),
                            VocabTooltip(
                              message: itasu,
                              inlineSpan: TextSpan(
                                text: '致します',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Excuse me. (lit: I am doing a discourtesy.)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do なさる – to do (honorific) 致す 【いた・す】 (u-verb) – to do (humble) 行く 【い・く】 (u-verb) – to go いらっしゃる – to be; to go; to come (honorific) おいでになる – to be; to go; to come (honorific) 参る 【まい・る】 (u-verb) – to go; to come (humble) いる (ru-verb) – to exist (animate) おる (ru-verb) – to exist (animate) (humble) 見る 【み・る】 (ru-verb) – to see ご覧になる 【ご・らん・になる】 – to see (honorific) 拝見する 【はい・けん・する】 – to see (humble) 聞く 【き・く】 (u-verb) – to ask; to listen 伺う 【うかが・う】 (u-verb) – to ask; to listen (humble) 言う 【い・う】 (u-verb) – to say おっしゃる – to say (honorific) 申す 【もう・す】 (u-verb) – to say (humble) 申し上げる 【もう・し・あ・げる】 (u-verb) – to say (humble) あげる (ru-verb) – to give; to raise 差し上げる 【さ・し・あ・げる】 (ru-verb) – to give; to raise (humble) くれる (ru-verb) – to give 下さる 【くだ・さる】 – to give (honorific) もらう (u-verb) – to receive いただく (u-verb) – to receive; to eat; to drink (humble) 食べる 【た・べる】 (ru-verb) – to eat 召し上がる 【め・し・あ・がる】 (u-verb) – to eat; to drink (honorific) 飲む 【の・む】 (u-verb) – to drink 知る 【し・る】 (u-verb) – to know ご存じ 【ご・ぞん・じ】 – knowing (honorific) 存じる 【ぞん・じる】 (ru-verb) – to know (humble) ござる – to be (formal) もう – already 仕事 【し・ごと】 – job 何 【なに／なん】 – what 推薦状 【すい・せん・じょう】 – letter of recommendation 書く 【か・く】 (u-verb) – to write どちら – which way 今日 【きょう】 – today 私 【わたし】 – me; myself; I レポート – report 失礼 【しつ・れい】 – discourtesy The difficult part of learning honorific and humble language is that there are a number of words that have separate verbs for honorific and humble forms. Anything that does not have its own special expression fall under the general rules of humble and honorific conjugations that we will cover next. Honorific verbs with special conjugations A number of these verbs do not follow the normal masu-conjugation rules and they include: 「なさる」、「いらっしゃる」、「おっしゃる」、「下さる」、 and 「ござる」 (which we will soon cover). For all masu-form tenses of these verbs, instead of the 「る」 becoming a 「り」 as it does with normal u-verbs, it instead becomes an 「い」. All other conjugations besides the masu-form do not change from regular u-verbs. Examples of honorific form We can now begin to see that 「ください」 is just a special conjugation of 「下さる」 which is the honorific version of 「くれる」. Let’s look at some actual examples. Since these examples are all questions directed directly to someone (second person), they all use the honorific form. アリスさん、もう召し上がりましたか。 Alice-san, did (you) eat already? 仕事で何をなさっているんですか。 What are you doing at work? 推薦状を書いてくださるんですか。 You’re going to give me the favor of writing a recommendation letter? どちらからいらっしゃいましたか。 Where did you come from? 今日は、どちらへいらっしゃいますか。 Where are you going today? Examples of humble form The following examples are all actions done by the speaker so they all use the humble form. 私はキムと申します。 As for me, (people) say Kim. (I am called Kim.) 私が書いたレポートを見ていただけますか。 Will I be able to receive the favor of getting my report looked at? 失礼致します。 Excuse me. (lit: I am doing a discourtesy.)',
      grammars: [],
      keywords: ['verb','vocabulary','conjugation'],
    ),
    Section(
      heading: 'Other substitutions',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('こちら – this way'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('部屋 【へ・や】 – room'),
                  Text('ござる – to be (formal)'),
                  Text('お手洗い 【お・て・あら・い】 – bathroom'),
                  Text('この – this （abbr. of これの）'),
                  Text('ビル – building'),
                  Text('～階 【～かい】 – counter for story/floor'),
                  Text('いい (i-adj) – good'),
                  Text('よろしい (i-adj) – good (formal)'),
                  Text('悪い 【わる・い】 (i-adj) – bad'),
                  Text('すいません – sorry (polite)'),
                  Text('ごめん – sorry (casual)'),
                  Text('ごめんなさい – sorry (polite)'),
                  Text('すみません – sorry (polite)'),
                  Text('申し訳ありません 【もう・し・わけ・ありません】 – sorry (formal)'),
                  Text('言い訳 【い・い・わけ】 – excuse'),
                  Text('恐れ入ります 【おそ・れ・い・ります】 – sorry (formal)'),
                  Text('恐縮です 【きょう・しゅく・です】 – sorry (formal)'),
                  Text('～様 【～さま】 – honorific name suffix'),
                  Text('～さん – polite name suffix'),
                  Text('お客様 【お・きゃく・さま】 – customer (formal)'),
                  Text('神様 【かみ・さま】 – god (formal)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In addition to these set expressions, there are some words that also have more polite counterparts. Probably the most important is the politer version of 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text: '」, which is 「',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ござる'),
                  ),
                  const TextSpan(
                    text:
                        '」. This verb can be used for both inanimate and animate objects. It is neither honorific nor humble but it is a step above 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text:
                        '」 in politeness. However, unless you want to sound like a samurai, 「',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ござる'),
                  ),
                  const TextSpan(
                    text: '」 is always used in the polite form: 「',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ございます'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'By extension, the politer version of 「です」 is 「で',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ございます'),
                  ),
                  const TextSpan(
                    text:
                        '」. This is essentially the masu-form conjugation of 「で',
                  ),
                  VocabTooltip(
                    message: gozaru,
                    inlineSpan: const TextSpan(text: 'ござる'),
                  ),
                  const TextSpan(
                    text: '」, which comes from 「で',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text:
                        '」 literally meaning, “to exist as” (to be covered much later).',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kochira,
                              inlineSpan: const TextSpan(
                                text: 'こちら',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Over here is my room.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kochira,
                              inlineSpan: const TextSpan(
                                text: 'こちら',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: heya,
                              inlineSpan: const TextSpan(
                                text: '部屋',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: gozaru,
                              inlineSpan: TextSpan(
                                text: 'ございます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('This way is my room.'),
                    ],
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: otearai,
                              inlineSpan: const TextSpan(
                                text: 'お手洗い',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: biru,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: nikai,
                              inlineSpan: const TextSpan(
                                text: '二階',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あります',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'The bathroom is on the second floor of this building.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: otearai,
                              inlineSpan: const TextSpan(
                                text: 'お手洗い',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: biru,
                              inlineSpan: const TextSpan(
                                text: 'ビル',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: nikai,
                              inlineSpan: const TextSpan(
                                text: '二階',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: gozaru,
                              inlineSpan: TextSpan(
                                text: 'ございます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'The bathroom is on the second floor of this building.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Other examples include 「',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(text: 'いい'),
                  ),
                  const TextSpan(
                    text: '」, which is more formally expressed as 「',
                  ),
                  VocabTooltip(
                    message: yoroshii,
                    inlineSpan: const TextSpan(text: 'よろしい'),
                  ),
                  const TextSpan(
                    text:
                        '」. There are also six different ways to say, “I’m sorry” (not counting 「',
                  ),
                  VocabTooltip(
                    message: warui,
                    inlineSpan: const TextSpan(text: '悪い'),
                  ),
                  const TextSpan(
                    text: 'ね」 or slight inflection changes like 「',
                  ),
                  VocabTooltip(
                    message: suimasen,
                    inlineSpan: const TextSpan(text: 'すいません'),
                  ),
                  const TextSpan(
                    text: '」).',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Successively politer expressions for apologizing:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: gomen,
                          inlineSpan: const TextSpan(text: 'ごめん'),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: gomennasai,
                          inlineSpan: const TextSpan(text: 'ごめんなさい'),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sumimasen,
                          inlineSpan: const TextSpan(text: 'すみません'),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: moushiwake,
                          inlineSpan: const TextSpan(text: '申し訳'),
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(text: 'ありません'),
                        ),
                        const TextSpan(
                          text: '。 (',
                        ),
                        VocabTooltip(
                          message: moushiwake,
                          inlineSpan: const TextSpan(text: '申し訳'),
                        ),
                        const TextSpan(
                          text: ' is the humble form of ',
                        ),
                        VocabTooltip(
                          message: iiwake,
                          inlineSpan: const TextSpan(text: '言い訳'),
                        ),
                        const TextSpan(
                          text: ')',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: osoreiru,
                          inlineSpan: const TextSpan(text: '恐れ入ります'),
                        ),
                        const TextSpan(
                          text: '。',
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kyoushuku,
                          inlineSpan: const TextSpan(text: '恐縮'),
                        ),
                        const TextSpan(
                          text: 'です。',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'In addition, the politest suffix for names is 「',
                  ),
                  VocabTooltip(
                    message: sama,
                    inlineSpan: const TextSpan(text: '様'),
                  ),
                  const TextSpan(
                    text:
                        '」, one level above 「さん」. You won’t be using this suffix too often in actual speech even if you speak to that person in honorific/humble speech. However, expect to use it when writing letters even to people you are somewhat familiar with. Also, service people such as cashiers or waitresses/waiters will normally refer to the customer as 「',
                  ),
                  VocabTooltip(
                    message: okyakusama,
                    inlineSpan: const TextSpan(text: 'お客様'),
                  ),
                  const TextSpan(
                    text:
                        '」. Of course, royalty and deities are always accompanied by 「',
                  ),
                  VocabTooltip(
                    message: sama,
                    inlineSpan: const TextSpan(text: '様'),
                  ),
                  const TextSpan(
                    text: '」 such as 「',
                  ),
                  VocabTooltip(
                    message: kamisama,
                    inlineSpan: const TextSpan(text: '神様'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary こちら – this way 私 【わたし】 – me, myself, I 部屋 【へ・や】 – room ござる – to be (formal) お手洗い 【お・て・あら・い】 – bathroom この – this （abbr. of これの） ビル – building～階 【～かい】 – counter for story/floor いい (i-adj) – good よろしい (i-adj) – good (formal) 悪い 【わる・い】 (i-adj) – bad すいません – sorry (polite) ごめん – sorry (casual) ごめんなさい – sorry (polite) すみません – sorry (polite) 申し訳ありません 【もう・し・わけ・ありません】 – sorry (formal) 言い訳 【い・い・わけ】 – excuse 恐れ入ります 【おそ・れ・い・ります】 – sorry (formal) 恐縮です 【きょう・しゅく・です】 – sorry (formal)～様 【～さま】 – honorific name suffix～さん – polite name suffix お客様 【お・きゃく・さま】 – customer (formal) 神様 【かみ・さま】 – god (formal) In addition to these set expressions, there are some words that also have more polite counterparts. Probably the most important is the politer version of 「ある」, which is 「ござる」. This verb can be used for both inanimate and animate objects. It is neither honorific nor humble but it is a step above 「ある」 in politeness. However, unless you want to sound like a samurai, 「ござる」 is always used in the polite form: 「ございます」. By extension, the politer version of 「です」 is 「でございます」. This is essentially the masu-form conjugation of 「でござる」, which comes from 「である」 literally meaning, “to exist as” (to be covered much later). Examples こちらは、私の部屋です。 Over here is my room. こちらは、私の部屋でございます。 This way is my room. お手洗いはこのビルの二階にあります。 The bathroom is on the second floor of this building. お手洗いはこのビルの二階にございます。 The bathroom is on the second floor of this building. Other examples include 「いい」, which is more formally expressed as 「よろしい」. There are also six different ways to say, “I’m sorry” (not counting 「悪いね」 or slight inflection changes like 「すいません」). Successively politer expressions for apologizing: ごめん。 ごめんなさい。 すみません。 申し訳ありません。(申し訳 is the humble form of 言い訳) 恐れ入ります。 恐縮です。 In addition, the politest suffix for names is 「様」, one level above 「さん」. You won’t be using this suffix too often in actual speech even if you speak to that person in honorific/humble speech. However, expect to use it when writing letters even to people you are somewhat familiar with. Also, service people such as cashiers or waitresses/waiters will normally refer to the customer as 「お客様」. Of course, royalty and deities are always accompanied by 「様」 such as 「神様」.',
      grammars: ['ございます','でございます'],
      keywords: ['sorry','apologize','verb','vocabulary','honorific','humble'],
    ),
    Section(
      heading: 'Honorific and humble conjugations',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('お茶 【お・ちゃ】 – tea'),
                  Text('お金 【お・かね】 – money'),
                  Text('音読み 【おん・よ・み】 – Chinese reading'),
                  Text('意見 【い・けん】 – opinion'),
                  Text('ご飯 【ご・はん】 – rice; meal'),
                  Text('訓読み 【くん・よ・み】 – Japanese reading'),
                  Text('仕事 【し・ごと】 – job'),
                  Text(
                      'お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake)'),
                  Text('お土産 【お・みやげ】 – souvenir'),
                  Text('返事 【へん・じ】 – reply'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('見える 【み・える】 (ru-verb) – to be visible'),
                  Text('なる (u-verb) – to become'),
                  Text('もう – already'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('店内 【てん・ない】 – store interior'),
                  Text(
                      '召し上がる 【め・し・あ・がる】 (ru-verb) – to eat; to drink (honorific)'),
                  Text('二重敬語 【に・じゅう・けい・ご】 – redundant honorific'),
                  Text('下さる 【くだ・さる】 – to give (honorific)'),
                  Text('少々 【しょう・しょう】 – just a minute; small quantity'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('こちら – this way'),
                  Text('ご覧下さい 【ご・らん・くだ・さい】 – please look (honorific)'),
                  Text('閉まる 【し・まる】 (u-verb) – to close'),
                  Text('ドア – door'),
                  Text('注意 【ちゅう・い】 – caution'),
                  Text('よろしい (i-adj) – good (formal)'),
                  Text('願う 【ねが・う】 (u-verb) – to wish; to request'),
                  Text('する (exception) – to do'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('こと – event, matter'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('すみません – sorry (polite)'),
                  Text('千円 【せん・えん】 – 1,000 yen'),
                  Text('預かる 【あず・かる】 – to look after; to hold on to'),
                  Text('致す 【いた・す】 (u-verb) – to do (humble)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For all other verbs without set expressions, there are conjugation rules to change them into honorific and humble forms. They both involve a common practice of attaching a polite prefix 「',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: '御'),
                  ),
                  const TextSpan(
                    text:
                        '」. In Japanese, there is an practice of attaching an honorific prefix 「',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: '御'),
                  ),
                  const TextSpan(
                    text:
                        '」 to certain (not all) nouns to show politeness. In fact, some words like 「',
                  ),
                  VocabTooltip(
                    message: osake,
                    inlineSpan: const TextSpan(text: 'お酒'),
                  ),
                  const TextSpan(
                    text: '」、 「',
                  ),
                  VocabTooltip(
                    message: ocha,
                    inlineSpan: const TextSpan(text: 'お茶'),
                  ),
                  const TextSpan(
                    text: '」、or 「',
                  ),
                  VocabTooltip(
                    message: okane,
                    inlineSpan: const TextSpan(text: 'お金'),
                  ),
                  const TextSpan(
                    text:
                        '」 come with this prefix so often that it’s become practically the word itself. In general, 「',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: '御'),
                  ),
                  const TextSpan(
                    text: '」 is written in hiragana as either 「',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: 'ご'),
                  ),
                  const TextSpan(
                    text: '」 for words read as ',
                  ),
                  VocabTooltip(
                    message: onyomi,
                    inlineSpan: const TextSpan(text: '音読み'),
                  ),
                  const TextSpan(
                    text: '（e.g. ',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: 'ご'),
                  ),
                  VocabTooltip(
                    message: iken,
                    inlineSpan: const TextSpan(text: '意見'),
                  ),
                  const TextSpan(
                    text: '、',
                  ),
                  VocabTooltip(
                    message: gohan,
                    inlineSpan: const TextSpan(text: 'ご飯'),
                  ),
                  const TextSpan(
                    text: '） or 「お」 for words read as ',
                  ),
                  VocabTooltip(
                    message: kunyomi,
                    inlineSpan: const TextSpan(text: '訓読み'),
                  ),
                  const TextSpan(
                    text: '（e.g. ',
                  ),
                  VocabTooltip(
                    message: okane,
                    inlineSpan: const TextSpan(text: 'お金'),
                  ),
                  const TextSpan(
                    text: '、 お',
                  ),
                  VocabTooltip(
                    message: shigoto,
                    inlineSpan: const TextSpan(text: '仕事'),
                  ),
                  const TextSpan(
                    text:
                        '）. In fact, you may have been using this prefix already without realizing it like 「',
                  ),
                  VocabTooltip(
                    message: okonomiyaki,
                    inlineSpan: const TextSpan(text: 'お好み焼き'),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: omiyage,
                    inlineSpan: const TextSpan(text: 'お土産'),
                  ),
                  const TextSpan(
                    text:
                        '」. There are some exceptions to this rule such as 「お',
                  ),
                  VocabTooltip(
                    message: henji,
                    inlineSpan: const TextSpan(text: '返事'),
                  ),
                  const TextSpan(
                    text: '」. Luckily since 「',
                  ),
                  VocabTooltip(
                    message: go,
                    inlineSpan: const TextSpan(text: '御'),
                  ),
                  const TextSpan(
                    text:
                        '」 is rarely written in kanji, identifying the exceptions should not really be a problem.',
                  ),
                ],
              ),
              const Heading(text: 'Honorific Form', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'The honorific form of verbs ',
                  ),
                  TextSpan(
                    text:
                        'that are not among the set honorific expressions given above',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text: ' can be formed in two different ways.',
                  ),
                ],
              ),
              const Heading(
                  text: 'Honorific Conjugation 1: お + stem + に + なる', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This kind of makes sense if you think of it as a person becoming the honorific state of a verb. All subsequent conjugations follow the normal rules of conjugating the u-verb 「',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text:
                        '」. To be honest, this type of sentence formulation is rarely used.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: mieru,
                              inlineSpan: TextSpan(
                                text: '見え',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なります',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Have you seen the teacher?'),
                    ],
                  ),
                ],
              ),
              const Heading(
                  text: 'Honorific Conjugation 2: お + stem + です', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                text: '帰り',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('You’re going home already?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tennai,
                              inlineSpan: const TextSpan(
                                text: '店内',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: meshiagaru,
                              inlineSpan: TextSpan(
                                text: '召し上がり',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'か。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Will you be dining in?'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Service people want to be extra polite so they will often use this type of “double honorific” conjugation or ',
                  ),
                  VocabTooltip(
                    message: nijuukeigo,
                    inlineSpan: const TextSpan(text: '二重敬語'),
                  ),
                  const TextSpan(
                    text: ' (in this case, the honorific 「',
                  ),
                  VocabTooltip(
                    message: meshiagaru,
                    inlineSpan: const TextSpan(text: '召し上がる'),
                  ),
                  const TextSpan(
                    text:
                        '」 combined with the honorific conjugation). Whether it’s necessary or grammatically proper is another story.',
                  ),
                ],
              ),
              const Heading(text: 'Using 「ください」 with honorifics', level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also use 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: '下さい'),
                  ),
                  const TextSpan(
                    text: '」 with a honorific verb by replacing 「に',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text: '」 with 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text:
                        '」. This is useful for when you want to ask somebody to do something but still use a honorific verb. ',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Yet another often-used expression.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shoushou,
                              inlineSpan: const TextSpan(
                                text: '少々',
                              ),
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: matsu,
                              inlineSpan: TextSpan(
                                text: '待ち',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: TextSpan(
                                text: 'ください',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Please wait a moment.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Similarly, with 「',
                  ),
                  VocabTooltip(
                    message: goran,
                    inlineSpan: const TextSpan(text: 'ご覧'),
                  ),
                  const TextSpan(
                    text: 'に',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text: '」, you simply replace 「に',
                  ),
                  VocabTooltip(
                    message: naru,
                    inlineSpan: const TextSpan(text: 'なる'),
                  ),
                  const TextSpan(
                    text: '」 with 「',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(text: 'ください'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kochira,
                              inlineSpan: const TextSpan(
                                text: 'こちら',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: goran,
                              inlineSpan: TextSpan(
                                text: 'ご覧',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: TextSpan(
                                text: '下さい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Please look this way.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This works for other nouns as well. For example, riding the trains…',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shimaru,
                              inlineSpan: const TextSpan(
                                text: '閉まる',
                              ),
                            ),
                            VocabTooltip(
                              message: doa,
                              inlineSpan: const TextSpan(
                                text: 'ドア',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            TextSpan(
                              text: 'ご',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: chuui,
                              inlineSpan: TextSpan(
                                text: '注意',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: TextSpan(
                                text: '下さい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Please be careful of the closing doors.'),
                    ],
                  ),
                ],
              ),
              const Heading(text: 'Humble Form', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Humble verbs are formed in the following fashion.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Humble Conjugation: お + stem + ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You’ve probably already heard the first example many times before but now you know exactly where it comes from.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yoroshii,
                              inlineSpan: const TextSpan(
                                text: 'よろしく',
                              ),
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: negau,
                              inlineSpan: TextSpan(
                                text: '願いします',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('I properly make request.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: TextSpan(
                                text: '聞き',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'したい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あります',
                              ),
                            ),
                            const TextSpan(
                              text: 'が。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Teacher, there’s something I want to ask you.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sumimasen,
                              inlineSpan: const TextSpan(
                                text: 'すみません',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: matsu,
                              inlineSpan: TextSpan(
                                text: '待たせ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('Sorry, I made you wait (causative form).'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: senen,
                              inlineSpan: const TextSpan(
                                text: '千円',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            TextSpan(
                              text: 'お',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: azukaru,
                              inlineSpan: TextSpan(
                                text: '預かり',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: itasu,
                              inlineSpan: TextSpan(
                                text: 'いたします',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('We’ll be holding on [from?] your 1000 yen.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'You’ll hear something like example 4 when, for example, you need to get change after paying 1000 yen. Again, the ',
                  ),
                  VocabTooltip(
                    message: nijuukeigo,
                    inlineSpan: const TextSpan(
                      text: '二重敬語',
                    ),
                  ),
                  const TextSpan(
                    text: ' where 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 has been converted to the humble 「',
                  ),
                  VocabTooltip(
                    message: itasu,
                    inlineSpan: const TextSpan(
                      text: '致す',
                    ),
                  ),
                  const TextSpan(
                    text: '」 form when it’s already in the お+stem+',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' humble form. Some Japanese people complain that this makes no sense and that 「から」 should really be 「を」.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary お酒 【お・さけ】 – alcohol お茶 【お・ちゃ】 – tea お金 【お・かね】 – money 音読み 【おん・よ・み】 – Chinese reading 意見 【い・けん】 – opinion ご飯 【ご・はん】 – rice; meal 訓読み 【くん・よ・み】 – Japanese reading 仕事 【し・ごと】 – job お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake) お土産 【お・みやげ】 – souvenir 返事 【へん・じ】 – reply 先生 【せん・せい】 – teacher 見える 【み・える】 (ru-verb) – to be visible なる (u-verb) – to become もう – already 帰る 【かえ・る】 (u-verb) – to go home 店内 【てん・ない】 – store interior 召し上がる 【め・し・あ・がる】 (ru-verb) – to eat; to drink (honorific) 二重敬語 【に・じゅう・けい・ご】 – redundant honorific 下さる 【くだ・さる】 – to give (honorific) 少々 【しょう・しょう】 – just a minute; small quantity 待つ 【ま・つ】 (u-verb) – to wait こちら – this way ご覧下さい 【ご・らん・くだ・さい】 – please look (honorific) 閉まる 【し・まる】 (u-verb) – to close ドア – door 注意 【ちゅう・い】 – caution よろしい (i-adj) – good (formal) 願う 【ねが・う】 (u-verb) – to wish; to request する (exception) – to do 聞く 【き・く】 (u-verb) – to ask; to listen こと – event, matter ある (u-verb) – to exist (inanimate) すみません – sorry (polite) 千円 【せん・えん】 – 1,000 yen 預かる 【あず・かる】 – to look after; to hold on to 致す 【いた・す】 (u-verb) – to do (humble) For all other verbs without set expressions, there are conjugation rules to change them into honorific and humble forms. They both involve a common practice of attaching a polite prefix 「御」. In Japanese, there is an practice of attaching an honorific prefix 「御」 to certain (not all) nouns to show politeness. In fact, some words like 「お酒」、 「お茶」、or 「お金」 come with this prefix so often that it’s become practically the word itself. In general, 「御」 is written in hiragana as either 「ご」 for words read as 音読み（e.g. ご意見、ご飯） or 「お」 for words read as 訓読み（e.g. お金、 お仕事）. In fact, you may have been using this prefix already without realizing it like 「お好み焼き」 or 「お土産」. There are some exceptions to this rule such as 「お返事」. Luckily since 「御」 is rarely written in kanji, identifying the exceptions should not really be a problem. Honorific Form The honorific form of verbs that are not among the set honorific expressions given above can be formed in two different ways. Honorific Conjugation 1: お + stem + に + なる This kind of makes sense if you think of it as a person becoming the honorific state of a verb. All subsequent conjugations follow the normal rules of conjugating the u-verb 「なる」. To be honest, this type of sentence formulation is rarely used. 先生はお見えになりますか。 Have you seen the teacher? Honorific Conjugation 2: お + stem + ですもうお帰りですか。 You’re going home already? 店内でお召し上がりですか。 Will you be dining in? Service people want to be extra polite so they will often use this type of “double honorific” conjugation or 二重敬語 (in this case, the honorific 「召し上がる」 combined with the honorific conjugation). Whether it’s necessary or grammatically proper is another story. Using 「ください」 with honorifics You can also use 「 下さい」 with a honorific verb by replacing 「になる」 with 「 ください」. This is useful for when you want to ask somebody to do something but still use a honorific verb. Yet another often-used expression. 少々お待ちください。 Please wait a moment. Similarly, with 「ご覧になる」, you simply replace 「になる」 with 「ください」. こちらにご覧下さい。 Please look this way. This works for other nouns as well. For example, riding the trains… 閉まるドアにご注意下さい。 Please be careful of the closing doors. Humble Form Humble verbs are formed in the following fashion. Humble Conjugation: お + stem + する You’ve probably already heard the first example many times before but now you know exactly where it comes from. よろしくお願いします。 I properly make request. 先生、お聞きしたいことがありますが。 Teacher, there’s something I want to ask you. すみません、お待たせしました。 Sorry, I made you wait (causative form). 千円からお預かりいたします。 We’ll be holding on [from?] your 1000 yen. You’ll hear something like example 4 when, for example, you need to get change after paying 1000 yen. Again, the 二重敬語 where 「する」 has been converted to the humble 「致す」 form when it’s already in the お+stem+する humble form. Some Japanese people complain that this makes no sense and that 「から」 should really be 「を」.',
      grammars: ['御','御～になる','御～です','御～下さい','御～する'],
      keywords: ['prefix','o','go','verb','honorific','humble'],
    ),
    Section(
      heading: 'Making honorific requests',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('下さる 【くだ・さる】 – to give (honorific)'),
                  Text('いらっしゃる – to be; to go; to come (honorific)'),
                  Text('なさる – to do (honorific)'),
                  Text('おっしゃる – to say (honorific)'),
                  Text('する (exception) – to do'),
                  Text('いらっしゃいませ – please come in (formal)'),
                  Text('いらっしゃい – please come in'),
                  Text('ありがとうございました – thank you (polite)'),
                  Text('また – again'),
                  Text('越す 【こ・す】 – to go over'),
                  Text('どうぞ – please'),
                  Text('ゆっくり – slowly'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We learned how to make polite requests using 「～',
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: const TextSpan(
                      text: 'ください',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in a previous section and we just looked at how to use honorific verbs with requests as well. However, there is yet another way to make requests using honorific verbs. This grammar only applies to the honorific verbs with special 「～ます」 conjugations that we just covered. This includes 「',
                  ),
                  VocabTooltip(
                    message: kudasaru,
                    inlineSpan: const TextSpan(
                      text: '下さる',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: irassharu,
                    inlineSpan: const TextSpan(
                      text: 'いらっしゃる',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: nasaru,
                    inlineSpan: const TextSpan(
                      text: 'なさる',
                    ),
                  ),
                  const TextSpan(
                    text: '」、and 「',
                  ),
                  VocabTooltip(
                    message: ossharu,
                    inlineSpan: const TextSpan(
                      text: 'おっしゃる',
                    ),
                  ),
                  const TextSpan(
                    text: '」. I’ve never actually seen this used with 「',
                  ),
                  VocabTooltip(
                    message: ossharu,
                    inlineSpan: const TextSpan(
                      text: 'おっしゃる',
                    ),
                  ),
                  const TextSpan(
                    text: '」, but it is grammatically possible.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Making requests for honorific actions',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'Conjugate the honorific verb to the special masu-conjugation and replace the last 「す」 with 「せ」.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '下さ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '下さいま',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '下さいま',
                                        ),
                                        TextSpan(
                                          text: 'せ',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'いらっしゃ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'いらっしゃいま',
                                        ),
                                        TextSpan(
                                          text: 'す',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'いらっしゃいま',
                                        ),
                                        TextSpan(
                                          text: 'せ',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'An abbreviated and less formal version of this is to simply remove the 「ます」 after conjugating to the special masu-form.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '下さ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '下さい',
                                        ),
                                        TextSpan(
                                          text: 'ます',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kudasaru,
                                    inlineSpan: const TextSpan(
                                      text: '下さい',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'いらっしゃ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'いらっしゃい',
                                        ),
                                        TextSpan(
                                          text: 'ます',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: irassharu,
                                    inlineSpan: const TextSpan(
                                      text: 'いらっしゃい',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Now you finally know where grammar such as 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'し',
                    ),
                  ),
                  TextSpan(
                    text: 'なさい ',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'して',
                    ),
                  ),
                  VocabTooltip(
                    message: kudasai,
                    inlineSpan: TextSpan(
                      text: 'ください',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 actually came from. Let’s look at a few quick examples.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You’ll probably hear this one a million times every time you enter some kind of store in Japan.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: irassharu,
                              inlineSpan: TextSpan(
                                text: 'いらっしゃいませ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Please come in!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'However, a middle-aged sushi chef will probably use the abbreviated version.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: irassharu,
                              inlineSpan: TextSpan(
                                text: 'いらっしゃい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('Please come in!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Some more examples…',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: arigatougozaimasu,
                              inlineSpan: const TextSpan(
                                text: 'ありがとうございました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                            VocabTooltip(
                              message: mata,
                              inlineSpan: const TextSpan(
                                text: 'また',
                              ),
                            ),
                            const TextSpan(text: 'お'),
                            VocabTooltip(
                              message: kosu,
                              inlineSpan: const TextSpan(
                                text: '越し',
                              ),
                            ),
                            VocabTooltip(
                              message: kudasaru,
                              inlineSpan: TextSpan(
                                text: 'くださいませ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Thank you very much. Please come again.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: douzo,
                              inlineSpan: const TextSpan(
                                text: 'どうぞ',
                              ),
                            ),
                            const TextSpan(text: '、ご'),
                            VocabTooltip(
                              message: yukkuri,
                              inlineSpan: const TextSpan(
                                text: 'ゆっくり',
                              ),
                            ),
                            VocabTooltip(
                              message: nasaru,
                              inlineSpan: TextSpan(
                                text: 'なさいませ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Please take your time and relax.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 下さる 【くだ・さる】 – to give (honorific) いらっしゃる – to be; to go; to come (honorific) なさる – to do (honorific) おっしゃる – to say (honorific) する (exception) – to do いらっしゃいませ – please come in (formal) いらっしゃい – please come in ありがとうございました – thank you (polite) また – again 越す 【こ・す】 – to go over どうぞ – please ゆっくり – slowly We learned how to make polite requests using 「～ください」 in a previous section and we just looked at how to use honorific verbs with requests as well. However, there is yet another way to make requests using honorific verbs. This grammar only applies to the honorific verbs with special 「～ます」 conjugations that we just covered. This includes 「下さる」、「いらっしゃる」、「なさる」、and 「おっしゃる」. I’ve never actually seen this used with 「おっしゃる」, but it is grammatically possible. Making requests for honorific actions Conjugate the honorific verb to the special masu-conjugation and replace the last 「す」 with 「せ」. Examples: 下さる → 下さいます → 下さいませ いらっしゃる → いらっしゃいます → いらっしゃいませ An abbreviated and less formal version of this is to simply remove the 「ます」 after conjugating to the special masu-form. Examples: 下さる → 下さいます → 下さい いらっしゃる → いらっしゃいます → いらっしゃい Now you finally know where grammar such as 「しなさい 」 and 「してください」 actually came from. Let’s look at a few quick examples. Examples You’ll probably hear this one a million times every time you enter some kind of store in Japan. いらっしゃいませ。 Please come in! However, a middle-aged sushi chef will probably use the abbreviated version. いらっしゃい！ Please come in! Some more examples… ありがとうございました。またお越しくださいませ。 Thank you very much. Please come again. どうぞ、ごゆっくりなさいませ。 Please take your time and relax.',
      grammars: ['～ませ',],
      keywords: ['request','honorific','verb'],
    ),
  ],
);
