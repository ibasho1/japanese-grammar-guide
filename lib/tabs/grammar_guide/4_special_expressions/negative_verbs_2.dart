import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson negativeVerbs2 = Lesson(
  title: 'More Negative Verbs',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We already learned the most common type of negative verbs; the ones that end in 「ない」. However, there are couple more different types of negatives verbs. The ones you will find most useful are the first two, which expresses an action that was done without having done another action. The others are fairly obscure or useful only for very casual expressions. However, you ',
                  ),
                  TextSpan(
                    text: 'will',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text:
                        ' run into them if you learn Japanese for a fair amount of time.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We already learned the most common type of negative verbs; the ones that end in 「ない」. However, there are couple more different types of negatives verbs. The ones you will find most useful are the first two, which expresses an action that was done without having done another action. The others are fairly obscure or useful only for very casual expressions. However, you will run into them if you learn Japanese for a fair amount of time.',
      grammars: [],
      keywords: ['negative'],
    ),
    Section(
      heading: 'Doing something without doing something else',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('何 【なに／なん】 – what'),
                  Text('歯 【は】 – tooth'),
                  Text('磨く 【みが・く】 (u-verb) – to brush; to polish'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('する (exception) – to do'),
                  Text('授業 【じゅ・ぎょう】 – class'),
                  Text('止める 【や・める】 (ru-verb) – to stop'),
                  Text(
                      '方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing'),
                  Text('いい (i-adj) – good'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('相談 【そう・だん】 – consultation'),
                  Text('この – this （abbr. of これの）'),
                  Text('取る 【と・る】 (u-verb) – to take'),
                  Text('こと – event, matter'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('そんな – that sort of'),
                  Text('お酒 【お・さけ】 – alcohol'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                  Text('当然 【とう・ぜん】 – naturally'),
                  Text('酔っ払う 【よ・っ・ぱ・らう】 (u-verb) – to get drunk'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('東大 【とう・だい】 – Tokyo University （abbr. for 「東京大学」）'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Way back when, we learned how to express a sequence of actions and this worked fine for both positive and negative verbs. For instance, the sentence “I didn’t eat, and then I went to sleep” would become 「',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(
                      text: '食べなくて',
                    ),
                  ),
                  VocabTooltip(
                    message: neru,
                    inlineSpan: const TextSpan(
                      text: '寝た',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '。」 However, this sentence sounds a bit strange because eating doesn’t have much to do with sleeping. What we probably ',
                  ),
                  const TextSpan(
                    text: 'really',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text: ' want to say is that we went to sleep ',
                  ),
                  const TextSpan(
                    text: 'without',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' eating. To express this, we need to use a more generalized form of the negative request we covered at the very end of the giving and receiving lesson. In other words, instead of substituting the last 「い」 with 「くて」, we need only append 「で」 instead.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Doing something without doing something else',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'To indicate an action that was done ',
                              ),
                              TextSpan(
                                text: 'without',
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' doing another action, add 「で」 to the negative of the action that was ',
                              ),
                              TextSpan(
                                text: 'not',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                              TextSpan(
                                text: ' done.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べない',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'で',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Use it with nouns by utilizing the 「の」 particle.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: neru,
                              inlineSpan: const TextSpan(
                                text: '寝ました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Went to sleep without eating anything.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ha,
                              inlineSpan: const TextSpan(
                                text: '歯',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: migaku,
                              inlineSpan: const TextSpan(
                                text: '磨かない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行っちゃいました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Went to school without brushing teeth (by accident).'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'のは、'),
                            VocabTooltip(
                              message: yameruStopQuit,
                              inlineSpan: const TextSpan(
                                text: 'やめた',
                              ),
                            ),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s better to stop going to class without doing homework.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: soudan,
                              inlineSpan: const TextSpan(
                                text: '相談',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            TextSpan(
                              text: 'で',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: jugyou,
                              inlineSpan: const TextSpan(
                                text: '授業',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: toru,
                              inlineSpan: const TextSpan(
                                text: '取る',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: '出来ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'You cannot take this class without consulting with teacher.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Hopefully not too difficult. Another way to express the exact same thing is to replace the last 「ない」 part with 「ず」. However, the two exception verbs 「する」 and 「くる」 become 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'せず',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こず',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 respectively. It is also common to see this grammar combined with the target 「に」 particle. This version is more formal than 「ないで」 and is not used as much in regular conversations.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Doing something without doing something else',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'Another way to indicate an action that was done without doing another action is to replace the 「ない」 part of the negative action that was not done with 「ず」.'),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'ず',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'ず',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'せず',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'くる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'こず',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言わず',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰って',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('He went home without saying anything.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べず',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: osake,
                              inlineSpan: const TextSpan(
                                text: 'お酒',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: const TextSpan(
                                text: '飲む',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: touzen,
                              inlineSpan: const TextSpan(
                                text: '当然',
                              ),
                            ),
                            VocabTooltip(
                              message: yopparau,
                              inlineSpan: const TextSpan(
                                text: '酔っ払います',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Obviously, you’re going to get drunk if you drink that much without eating anything.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'せず',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: toudai,
                              inlineSpan: const TextSpan(
                                text: '東大',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ireru,
                              inlineSpan: const TextSpan(
                                text: '入れる',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思わない',
                              ),
                            ),
                            const TextSpan(text: 'な。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I don’t think you can get in Tokyo University without studying.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 寝る 【ね・る】 (ru-verb) – to sleep 何 【なに／なん】 – what 歯 【は】 – tooth 磨く 【みが・く】 (u-verb) – to brush; to polish 学校 【がっ・こう】 – school 行く 【い・く】 (u-verb) – to go 宿題 【しゅく・だい】 – homework する (exception) – to do 授業 【じゅ・ぎょう】 – class 止める 【や・める】 (ru-verb) – to stop 方 【1) ほう; 2) かた】 – 1) direction; side; 2) person; way of doing いい (i-adj) – good 先生 【せん・せい】 – teacher 相談 【そう・だん】 – consultation この – this （abbr. of これの） 取る 【と・る】 (u-verb) – to take こと – event, matter 出来る 【で・き・る】 (ru-verb) – to be able to do 彼 【かれ】 – he; boyfriend 言う 【い・う】 (u-verb) – to say 帰る 【かえ・る】 (u-verb) – to go home そんな – that sort of お酒 【お・さけ】 – alcohol 飲む 【の・む】 (u-verb) – to drink 当然 【とう・ぜん】 – naturally 酔っ払う 【よ・っ・ぱ・らう】 (u-verb) – to get drunk 勉強 【べん・きょう】 – study 東大 【とう・だい】 – Tokyo University （abbr. for 「東京大学」） 入る 【はい・る】 (u-verb) – to enter 思う 【おも・う】 (u-verb) – to think Way back when, we learned how to express a sequence of actions and this worked fine for both positive and negative verbs. For instance, the sentence “I didn’t eat, and then I went to sleep” would become 「食べなくて寝た。」 However, this sentence sounds a bit strange because eating doesn’t have much to do with sleeping. What we probably really want to say is that we went to sleep without eating. To express this, we need to use a more generalized form of the negative request we covered at the very end of the giving and receiving lesson. In other words, instead of substituting the last 「い」 with 「くて」, we need only append 「で」 instead. Doing something without doing something else To indicate an action that was done without doing another action, add 「で」 to the negative of the action that was not done. Example: 食べる → 食べない → 食べないで Examples Use it with nouns by utilizing the 「の」 particle. 何も食べないで寝ました。 Went to sleep without eating anything. 歯を磨かないで、学校に行っちゃいました。 Went to school without brushing teeth (by accident). 宿題をしないで、授業に行くのは、やめた方がいいよ。 It’s better to stop going to class without doing homework. 先生と相談しないで、この授業を取ることは出来ない。 You cannot take this class without consulting with teacher. Hopefully not too difficult. Another way to express the exact same thing is to replace the last 「ない」 part with 「ず」. However, the two exception verbs 「する」 and 「くる」 become 「せず」 and 「こず」 respectively. It is also common to see this grammar combined with the target 「に」 particle. This version is more formal than 「ないで」 and is not used as much in regular conversations. Doing something without doing something else Another way to indicate an action that was done without doing another action is to replace the 「ない」 part of the negative action that was not done with 「ず」. Examples: 食べる → 食べない → 食べず行く → 行かない → 行かず Exceptions: する → せずくる → こず Examples 彼は何も言わず、帰ってしまった。 He went home without saying anything. 何も食べずにそんなにお酒を飲むと当然酔っ払いますよ。 Obviously, you’re going to get drunk if you drink that much without eating anything. 勉強せずに東大に入れると思わないな。 I don’t think you can get in Tokyo University without studying.',
      grammars: ['～ないで', '～ず'],
      keywords: ['without','naide','zu'],
    ),
    Section(
      heading: 'A casual masculine type of negative that ends in 「ん」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('すまん – sorry (masculine)'),
                  Text('すみません – sorry (polite)'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('韓国人 【かん・こく・じん】 – Korean person'),
                  Text('結婚 【けっ・こん】 – marriage'),
                  Text('なる (u-verb) – to become'),
                  Text('そんな – that sort of'),
                  Text('こと – event, matter'),
                  Text('皆 【みんな】 – everybody'),
                  Text('今日 【きょう】 – today'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Finally, we cover another type of negative that is used mostly by older men. Since 「ない」 is so long and difficult to say (sarcasm), you can shorten it to just 「ん」. However, you can’t directly modify other words in this form; in other words, you can’t make it a modifying relative clause. In the same manner as before, 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 becomes 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'せん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'くる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 becomes 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 though I’ve never heard or seen 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こん',
                    ),
                  ),
                  const TextSpan(
                    text: '」 actually being used. If you have ever heard ｢',
                  ),
                  VocabTooltip(
                    message: suman,
                    inlineSpan: const TextSpan(
                      text: 'すまん',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 and wondered what that meant, it’s actually an example of this grammar. Notice that 「',
                  ),
                  VocabTooltip(
                    message: sumimasen,
                    inlineSpan: const TextSpan(
                      text: 'すみません',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is actually in polite negative form. Well, the plain form would be 「',
                  ),
                  VocabTooltip(
                    message: sumanai,
                    inlineSpan: const TextSpan(
                      text: 'すまない',
                    ),
                  ),
                  const TextSpan(
                    text: '」, right? That further transforms to just 「',
                  ),
                  VocabTooltip(
                    message: suman,
                    inlineSpan: const TextSpan(
                      text: 'すまん',
                    ),
                  ),
                  const TextSpan(
                    text: '」. The word brings up an image of ',
                  ),
                  VocabTooltip(
                    message: ojisan,
                    inlineSpan: const TextSpan(
                      text: 'おじさん',
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' but that may be just me. Anyway, it’s a male expression.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'A shorter way to say negative verbs',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'A shorter way to say a negative verb is to use 「ん」 instead of 「ない」.'),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知ら',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知ら',
                                        ),
                                        TextSpan(
                                          text: 'ん',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'せん',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'くる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'こん',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: suman,
                              inlineSpan: const TextSpan(
                                text: 'すまん',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Sorry.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kankokujin,
                              inlineSpan: const TextSpan(
                                text: '韓国人',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kekkon,
                              inlineSpan: const TextSpan(
                                text: '結婚',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しなくて',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'ならん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('You must marry a Korean!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'させん',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('I won’t let you do such a thing!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can even use this slang for past tense verbs by adding 「かった」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: '皆',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'って、'),
                            VocabTooltip(
                              message: shiru,
                              inlineSpan: TextSpan(
                                children: [
                                  const TextSpan(
                                    text: '知らん',
                                  ),
                                  TextSpan(
                                    text: 'かった',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('I didn’t know everybody was going today.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do 来る 【く・る】 (exception) – to come すまん – sorry (masculine) すみません – sorry (polite) 知る 【し・る】 (u-verb) – to know 韓国人 【かん・こく・じん】 – Korean person 結婚 【けっ・こん】 – marriage なる (u-verb) – to become そんな – that sort of こと – event, matter 皆 【みんな】 – everybody 今日 【きょう】 – today 行く 【い・く】 (u-verb) – to go Finally, we cover another type of negative that is used mostly by older men. Since 「ない」 is so long and difficult to say (sarcasm), you can shorten it to just 「ん」. However, you can’t directly modify other words in this form; in other words, you can’t make it a modifying relative clause. In the same manner as before, 「する」 becomes 「せん」 and 「くる」 becomes 「こん」 though I’ve never heard or seen 「こん」 actually being used. If you have ever heard ｢すまん」 and wondered what that meant, it’s actually an example of this grammar. Notice that 「すみません」 is actually in polite negative form. Well, the plain form would be 「すまない」, right? That further transforms to just 「すまん」. The word brings up an image of おじさん but that may be just me. Anyway, it’s a male expression. A shorter way to say negative verbs A shorter way to say a negative verb is to use 「ん」 instead of 「ない」. Example: 知る → 知らない → 知らん Exceptions: する → せんくる → こん Examples すまん。 Sorry. 韓国人と結婚しなくてはならん！ You must marry a Korean! そんなことはさせん！ I won’t let you do such a thing! You can even use this slang for past tense verbs by adding 「かった」. 皆、今日行くって、知らんかったよ。 I didn’t know everybody was going today.',
      grammars: ['～ん'],
      keywords: ['masculine','n','negative','verb'],
    ),
    Section(
      heading: 'A classical negative verb that ends in 「ぬ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('韓国人 【かん・こく・じん】 – Korean person'),
                  Text('結婚 【けっ・こん】 – marriage'),
                  Text('なる (u-verb) – to become'),
                  Text('模擬 【も・ぎ】 – mock'),
                  Text('試験 【し・けん】 – exam'),
                  Text('何回 【なん・かい】 – how many times'),
                  Text('失敗 【しっ・ぱい】 – failure'),
                  Text('実際 【じっ・さい】 – actual'),
                  Text('受ける 【う・ける】 (ru-verb) – to receive'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('結果 【けっ・か】 – result'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'There is yet another version of the negative verb conjugation and it uses 「ぬ」 instead of the 「ない」 that attaches to the end of the verb. While this version of the negative conjugation is old-fashioned and part of classical Japanese, you will still encounter it occasionally. In fact, I just saw this conjugation on a sign at the train station today, so it’s not too uncommon.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For any verb, you can replace 「ない」 with 「ぬ」 to get to an old-fashion sounding version of the negative. Similar to the last section, 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 becomes 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'せぬ',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'くる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 becomes 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こぬ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. You may hear this grammar being used from older people or your friends if they want to bring back ye olde days.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'An old-fashioned way to say negative verbs',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'An old-fashioned way to say a negative verb is to use 「ぬ」 instead of 「ない」.'),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知ら',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '知ら',
                                        ),
                                        TextSpan(
                                          text: 'ぬ',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'せぬ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'くる',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'こぬ',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kankokujin,
                              inlineSpan: const TextSpan(
                                text: '韓国人',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kekkon,
                              inlineSpan: const TextSpan(
                                text: '結婚',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'ならぬ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text('You must not marry a Korean!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mogi,
                              inlineSpan: const TextSpan(
                                text: '模擬',
                              ),
                            ),
                            VocabTooltip(
                              message: shiken,
                              inlineSpan: const TextSpan(
                                text: '試験',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: nankai,
                              inlineSpan: const TextSpan(
                                text: '何回',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: shippai,
                              inlineSpan: const TextSpan(
                                text: '失敗',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: jissai,
                              inlineSpan: const TextSpan(
                                text: '実際',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: ukeru,
                              inlineSpan: const TextSpan(
                                text: '受けて',
                              ),
                            ),
                            const TextSpan(
                              text: 'みたら',
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思わぬ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: kekka,
                              inlineSpan: const TextSpan(
                                text: '結果',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: deru,
                              inlineSpan: const TextSpan(
                                text: '出た',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'After having failed mock examination any number of times, a result I wouldn’t have thought came out when I actually tried taking the test.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do 来る 【く・る】 (exception) – to come 知る 【し・る】 (u-verb) – to know 韓国人 【かん・こく・じん】 – Korean person 結婚 【けっ・こん】 – marriage なる (u-verb) – to become 模擬 【も・ぎ】 – mock 試験 【し・けん】 – exam 何回 【なん・かい】 – how many times 失敗 【しっ・ぱい】 – failure 実際 【じっ・さい】 – actual 受ける 【う・ける】 (ru-verb) – to receive 思う 【おも・う】 (u-verb) – to think 結果 【けっ・か】 – result 出る 【で・る】 (ru-verb) – to come out There is yet another version of the negative verb conjugation and it uses 「ぬ」 instead of the 「ない」 that attaches to the end of the verb. While this version of the negative conjugation is old-fashioned and part of classical Japanese, you will still encounter it occasionally. In fact, I just saw this conjugation on a sign at the train station today, so it’s not too uncommon. For any verb, you can replace 「ない」 with 「ぬ」 to get to an old-fashion sounding version of the negative. Similar to the last section, 「する」 becomes 「せぬ」 and 「くる」 becomes 「こぬ」. You may hear this grammar being used from older people or your friends if they want to bring back ye olde days. An old-fashioned way to say negative verbs An old-fashioned way to say a negative verb is to use 「ぬ」 instead of 「ない」. Example: 知る → 知らない → 知らぬ Exceptions: する → せぬくる → こぬ Examples 韓国人と結婚してはならぬ！ You must not marry a Korean! 模擬試験に何回も失敗して、実際に受けてみたら思わぬ結果が出た。 After having failed mock examination any number of times, a result I wouldn’t have thought came out when I actually tried taking the test.',
      grammars: ['～ぬ'],
      keywords: ['nu','negative','verb','classical'],
    ),
  ],
);
