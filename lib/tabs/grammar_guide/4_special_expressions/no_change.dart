import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson noChange = Lesson(
  title: 'Expressing a Lack of Change',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Up until now, we’ve mostly been talking about things that have happened or changed in the course of events. We will now learn some simple grammar to express a ',
                  ),
                  TextSpan(
                    text: 'lack',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                    text: ' of change.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Up until now, we’ve mostly been talking about things that have happened or changed in the course of events. We will now learn some simple grammar to express a lack of change.',
      grammars: [],
      keywords: ['change','lack of change'],
    ),
    Section(
      heading: 'Using 「まま」 to express a lack of change',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('この – this （abbr. of これの）'),
                  Text('宜しい 【よろ・しい】 (i-adj) – good (formal)'),
                  Text('半分 【はん・ぶん】 – half'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('駄目 【だめ】 – no good'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('今日 【きょう】 – today'),
                  Text('悲しい 【かな・しい】 (i-adj) – sad'),
                  Text('その – that （abbr. of それの）'),
                  Text('格好 【かっ・こう】 – appearance'),
                  Text('クラブ – club; nightclub'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「まま」, not to be confused with the childish expression for “mother” （ママ）, is a grammatical phrase to express a lack of change in something. Grammatically, it is used just like a regular noun. You’ll most likely hear this grammar at a convenience store when you buy a very small item. Since store clerks use super polite expressions and at lightning fast speeds, learning this one expression will help you out a bit in advance. (Of course, upon showing a lack of comprehension, the person usually repeats the exact same phrase… at the exact same speed.)',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            TextSpan(
                              text: 'まま',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: yoroshii,
                              inlineSpan: const TextSpan(
                                text: '宜しい',
                              ),
                            ),
                            const TextSpan(text: 'ですか？'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'Is it ok '),
                            TextSpan(
                              text: 'just like',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' this?'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In other words, the clerk wants to know if you’ll take it just like that or whether you want it in a small bag. 「',
                  ),
                  VocabTooltip(
                    message: yoroshii,
                    inlineSpan: const TextSpan(
                      text: '宜しい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, in case I haven’t gone over it yet, is simply a very polite version of 「',
                  ),
                  VocabTooltip(
                    message: yoroshii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. Notice that 「まま」 grammatically works just like a regular noun which means, as usual, that you can modify it with verb phrases or adjectives.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hanbun,
                              inlineSpan: const TextSpan(
                                text: '半分',
                              ),
                            ),
                            const TextSpan(text: 'しか'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べてない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'まま',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: suteru,
                              inlineSpan: const TextSpan(
                                text: '捨てちゃ',
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'ダメ',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text(
                          'You can’t throw it out leaving it in that half-eaten condition!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Ok, the translation is very loose, but the idea is that it’s in an unchanged state of being half-eaten and you can’t just throw that out.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Here’s a good example I found googling around',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Hint: The 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いさせる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is the causative form of 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 meaning “let/make me exist”.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'だけは'),
                            VocabTooltip(
                              message: kanashii,
                              inlineSpan: TextSpan(
                                text: '悲しい',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'まま',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いさせて',
                              ),
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: const TextSpan(
                                text: 'ほしい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'For only today, I want you to let me stay in this sad condition.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Finally, just in case, here’s an example of direct noun modification.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: kakkou,
                              inlineSpan: TextSpan(
                                text: '格好',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'のまま',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: kurabu,
                              inlineSpan: const TextSpan(
                                text: 'クラブ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ireru,
                              inlineSpan: const TextSpan(
                                text: '入れない',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'You can’t get in the club in that getup (without changing it).'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary この – this （abbr. of これの） 宜しい 【よろ・しい】 (i-adj) – good (formal) 半分 【はん・ぶん】 – half 食べる 【た・べる】 (ru-verb) – to eat 捨てる 【す・てる】 (ru-verb) – to throw away 駄目 【だめ】 – no good いる (ru-verb) – to exist (animate) 今日 【きょう】 – today 悲しい 【かな・しい】 (i-adj) – sad その – that （abbr. of それの） 格好 【かっ・こう】 – appearance クラブ – club; nightclub 入る 【はい・る】 (u-verb) – to enter 「まま」, not to be confused with the childish expression for “mother” （ママ）, is a grammatical phrase to express a lack of change in something. Grammatically, it is used just like a regular noun. You’ll most likely hear this grammar at a convenience store when you buy a very small item. Since store clerks use super polite expressions and at lightning fast speeds, learning this one expression will help you out a bit in advance. (Of course, upon showing a lack of comprehension, the person usually repeats the exact same phrase… at the exact same speed.) Examples このままで宜しいですか？ Is it ok just like this? In other words, the clerk wants to know if you’ll take it just like that or whether you want it in a small bag. 「宜しい」, in case I haven’t gone over it yet, is simply a very polite version of 「いい」. Notice that 「まま」 grammatically works just like a regular noun which means, as usual, that you can modify it with verb phrases or adjectives. 半分しか食べてないままで捨てちゃダメ！ You can’t throw it out leaving it in that half-eaten condition! Ok, the translation is very loose, but the idea is that it’s in an unchanged state of being half-eaten and you can’t just throw that out. Here’s a good example I found googling around Hint: The 「いさせる」 is the causative form of 「いる」 meaning “let/make me exist”. 今日だけは悲しいままでいさせてほしい。 For only today, I want you to let me stay in this sad condition. Finally, just in case, here’s an example of direct noun modification. その格好のままでクラブに入れないよ。 You can’t get in the club in that getup (without changing it).',
      grammars: ['まま'],
      keywords: ['mama','like this','as is'],
    ),
    Section(
      heading: 'Using 「っぱなし」 to leave something the way it is',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('放す 【はな・す】 (u-verb) – to release; to set loose'),
                  Text('くれる (ru-verb) – to give'),
                  Text('ほったらかす (u-verb) – to neglect'),
                  Text('テレビ – TV, television'),
                  Text('開ける 【あ・ける】 (ru-verb) – to open'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                  Text('つける (ru-verb) – to attach; to turn on'),
                  Text('する (exception) – to do'),
                  Text('眠れる 【ねむ・れる】 (ru-verb) – to fall asleep'),
                  Text('人 【ひと】 – person'),
                  Text('結構 【けっ・こう】 – fairly, reasonably'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('窓 【まど】 – window'),
                  Text('蚊 【か】 – mosquito'),
                  Text('いっぱい – full'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text(
                      'しまう (u-verb) – to do something by accident; to finish completely'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The verb 「',
                  ),
                  VocabTooltip(
                    message: hanasuSetLoose,
                    inlineSpan: const TextSpan(
                      text: '放す',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 meaning “to set loose”, can be used in various ways in regards to leaving something the way it is. For instance, a variation 「',
                  ),
                  VocabTooltip(
                    message: hottoku,
                    inlineSpan: const TextSpan(
                      text: '放っとく',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is used when you want to say “Leave me alone”. For instance, you might use the command form of a request （',
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれる',
                    ),
                  ),
                  const TextSpan(
                    text: '） and say, 「',
                  ),
                  VocabTooltip(
                    message: hottoku,
                    inlineSpan: const TextSpan(
                      text: 'ほっといて',
                    ),
                  ),
                  VocabTooltip(
                    message: kureru,
                    inlineSpan: const TextSpan(
                      text: 'くれ',
                    ),
                  ),
                  const TextSpan(
                    text: '！」(Leave me alone!). Yet another variant 「',
                  ),
                  VocabTooltip(
                    message: hottarakasu,
                    inlineSpan: const TextSpan(
                      text: 'ほったらかす',
                    ),
                  ),
                  const TextSpan(
                    text: '」 means “to neglect”.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The grammar I would like to discuss here is the 「っぱなし」 suffix variant. You can attach this suffix to the stem of any verb to describe the act of doing something and leaving it that way without changing it. You can treat the combination like a regular noun.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Here’s a link with more examples of this grammar. As you can see by the examples, this suffix carries a nuance that the thing left alone is due to oversight or neglect. Here are the (simple) conjugation rules for this grammar.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「っぱなし」 to complete an action and leave it that way',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Take the stem of the verb and attach 「っぱなし」.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: akeru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '開け',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: akeru,
                                    inlineSpan: const TextSpan(
                                      text: '開けた',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'っぱなし',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kaku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '書',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kaku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '書',
                                        ),
                                        TextSpan(
                                          text: 'き',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kaku,
                                    inlineSpan: const TextSpan(
                                      text: '書き',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'っぱなし',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: tsukeru,
                              inlineSpan: TextSpan(
                                text: 'つけ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'っぱなし',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しなければ',
                              ),
                            ),
                            VocabTooltip(
                              message: nemureru,
                              inlineSpan: const TextSpan(
                                text: '眠れない',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: kekkou,
                              inlineSpan: const TextSpan(
                                text: '結構',
                              ),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いる',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'There exists a fair number of people who cannot sleep unless they turn on the TV and leave it that way.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mado,
                              inlineSpan: const TextSpan(
                                text: '窓',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: akeru,
                              inlineSpan: TextSpan(
                                text: '開け',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'っ放し',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'だったので、'),
                            VocabTooltip(
                              message: ka,
                              inlineSpan: const TextSpan(
                                text: '蚊',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ippai,
                              inlineSpan: const TextSpan(
                                text: 'いっぱい',
                              ),
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入って',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The window was left wide open so a lot of mosquitoes got in.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 放す 【はな・す】 (u-verb) – to release; to set loose くれる (ru-verb) – to give ほったらかす (u-verb) – to neglect テレビ – TV, television 開ける 【あ・ける】 (ru-verb) – to open 書く 【か・く】 (u-verb) – to write つける (ru-verb) – to attach; to turn on する (exception) – to do 眠れる 【ねむ・れる】 (ru-verb) – to fall asleep 人 【ひと】 – person 結構 【けっ・こう】 – fairly, reasonably いる (ru-verb) – to exist (animate) 窓 【まど】 – window 蚊 【か】 – mosquito いっぱい – full 入る 【はい・る】 (u-verb) – to enter しまう (u-verb) – to do something by accident; to finish completely The verb 「放す」 meaning “to set loose”, can be used in various ways in regards to leaving something the way it is. For instance, a variation 「放っとく」 is used when you want to say “Leave me alone”. For instance, you might use the command form of a request （くれる） and say, 「ほっといてくれ！」(Leave me alone!). Yet another variant 「ほったらかす」 means “to neglect”. The grammar I would like to discuss here is the 「っぱなし」 suffix variant. You can attach this suffix to the stem of any verb to describe the act of doing something and leaving it that way without changing it. You can treat the combination like a regular noun. Here’s a link with more examples of this grammar. As you can see by the examples, this suffix carries a nuance that the thing left alone is due to oversight or neglect. Here are the (simple) conjugation rules for this grammar. Using 「っぱなし」 to complete an action and leave it that way Take the stem of the verb and attach 「っぱなし」. Examples: 開ける → 開けたっぱなし書く → 書き → 書きっぱなし Examples テレビをつけっぱなしにしなければ眠れない人は、結構いる。 There exists a fair number of people who cannot sleep unless they turn on the TV and leave it that way. 窓が開けっ放しだったので、蚊がいっぱい入ってしまった。 The window was left wide open so a lot of mosquitoes got in.',
      grammars: ['っぱなし'],
      keywords: ['left','ppanashi','leave alone'],
    ),
  ],
);
