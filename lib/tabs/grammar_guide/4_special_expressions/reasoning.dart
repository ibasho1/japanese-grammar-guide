import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson reasoning = Lesson(
  title: 'Hypothesizing and Concluding',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In this section, we’re going to learn how to make hypotheses and reach conclusions using: 「と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text: '」（',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: '訳',
                    ),
                  ),
                  const TextSpan(
                    text: '）.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this section, we’re going to learn how to make hypotheses and reach conclusions using: 「とする」 and 「わけ」（訳）.',
      grammars: [],
      keywords: ['hypothesising','concluding','reasoning'],
    ),
    Section(
      heading: 'Coming to a conclusion with 「わけ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('訳 【わけ】 – meaning; reason; can be deduced'),
                  Text('直子 【なお・こ】 – Naoko (first name)'),
                  Text('いくら – how much'),
                  Text('英語 【えい・ご】 – English (language)'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('する (exception) – to do'),
                  Text('うまい (i-adj) – skillful; delicious'),
                  Text('なる (u-verb) – to become'),
                  Text('つまり – in short'),
                  Text('語学 【ご・がく】 – language study'),
                  Text('能力 【のう・りょく】 – ability'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('失礼 【しつ・れい】 – discourtesy'),
                  Text('中国語 【ちゅう・ごく・ご】 – Chinese language'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('広子 【ひろ・こ】 – Hiroko (first name)'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('こと – event, matter'),
                  Text('一郎 【いち・ろう】 – Ichirou (first name)'),
                  Text('微積分 【び・せき・ぶん】 – (differential and integral) calculus'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('ここ – here'),
                  Text('試験 【し・けん】 – exam'),
                  Text('合格 【ごう・かく】 – pass (as in an exam)'),
                  Text('今度 【こん・ど】 – this time; another time'),
                  Text('負ける 【ま・ける】 (ru-verb) – to lose'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('あきらめる (ru-verb) – to give up'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The noun 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text: '」（',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: '訳',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '） is a bit difficult to describe but it’s defined as: “meaning; reason; can be deduced”. You can see how this word is used in the following mini-dialogue.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(children: [
                      VocabTooltip(
                        message: naoko,
                        inlineSpan: const TextSpan(
                          text: '直子',
                        ),
                      ),
                    ]),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ikura,
                          inlineSpan: const TextSpan(
                            text: 'いくら',
                          ),
                        ),
                        VocabTooltip(
                          message: eiga,
                          inlineSpan: const TextSpan(
                            text: '英語',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: benkyou,
                          inlineSpan: const TextSpan(
                            text: '勉強',
                          ),
                        ),
                        VocabTooltip(
                          message: suru,
                          inlineSpan: const TextSpan(
                            text: 'して',
                          ),
                        ),
                        const TextSpan(
                          text: 'も、',
                        ),
                        VocabTooltip(
                          message: umai,
                          inlineSpan: const TextSpan(
                            text: 'うまく',
                          ),
                        ),
                        VocabTooltip(
                          message: naru,
                          inlineSpan: const TextSpan(
                            text: 'ならない',
                          ),
                        ),
                        const TextSpan(
                          text: 'の。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Naoko'),
                  text: Text(
                      'No matter how much I study, I don’t become better at English.'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('ジム'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: tsumari,
                            inlineSpan: const TextSpan(
                              text: 'つまり',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: gogaku,
                            inlineSpan: const TextSpan(
                              text: '語学',
                            ),
                          ),
                          const TextSpan(
                            text: 'には、',
                          ),
                          VocabTooltip(
                            message: nouryoku,
                            inlineSpan: const TextSpan(
                              text: '能力',
                            ),
                          ),
                          const TextSpan(
                            text: 'が',
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ない',
                            ),
                          ),
                          const TextSpan(
                            text: 'と',
                          ),
                          VocabTooltip(
                            message: iu,
                            inlineSpan: const TextSpan(
                              text: 'いう',
                            ),
                          ),
                          VocabTooltip(
                            message: wake,
                            inlineSpan: TextSpan(
                              text: '訳',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: 'か。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  DialogueLine(
                    speaker: const Text('Jim'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'So basically, it ',
                          ),
                          TextSpan(
                            text: 'means',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: ' that you don’t have ability at language.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(children: [
                      VocabTooltip(
                        message: naoko,
                        inlineSpan: const TextSpan(
                          text: '直子',
                        ),
                      ),
                    ]),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: shitsurei,
                          inlineSpan: const TextSpan(
                            text: '失礼',
                          ),
                        ),
                        const TextSpan(
                          text: 'ね。',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Naoko'),
                  text: Text('How rude.'),
                  english: true,
                ),
              ]),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'As you can see, Jim is concluding from what Naoko said that she must not have any skills at learning languages. This is completely different from the explanatory 「の」, which is used to explain something that may or may not be obvious. 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is instead used to draw conclusions that anyone might be able to arrive at given certain information.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'A very useful application of this grammar is to combine it with 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to indicate that there is no reasonable conclusion. This allows some very useful expression like, “How in the world am I supposed to know that?”',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chuugokugo,
                              inlineSpan: const TextSpan(
                                text: '中国語',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読める',
                              ),
                            ),
                            VocabTooltip(
                              message: wake,
                              inlineSpan: TextSpan(
                                text: 'わけ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'There’s no way I can read Chinese. (lit: There is no reasoning for [me] to be able to read Chinese.)'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Under the normal rules of grammar, we must have a particle for the noun 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in order to use it with the verb but since this type of expression is used so often, the particle is often dropped to create just 「～',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: TextSpan(
                      text: 'ない',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(children: [
                      VocabTooltip(
                        message: naoko,
                        inlineSpan: const TextSpan(
                          text: '直子',
                        ),
                      ),
                    ]),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: hiroko,
                          inlineSpan: const TextSpan(
                            text: '広子',
                          ),
                        ),
                        const TextSpan(
                          text: 'の',
                        ),
                        VocabTooltip(
                          message: ie,
                          inlineSpan: const TextSpan(
                            text: '家',
                          ),
                        ),
                        const TextSpan(
                          text: 'に',
                        ),
                        VocabTooltip(
                          message: ikiru,
                          inlineSpan: const TextSpan(
                            text: '行った',
                          ),
                        ),
                        VocabTooltip(
                          message: koto,
                          inlineSpan: const TextSpan(
                            text: 'こと',
                          ),
                        ),
                        VocabTooltip(
                          message: aru,
                          inlineSpan: const TextSpan(
                            text: 'ある',
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Naoko'),
                  text: Text('Have you ever gone to Hiroko’s house?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(children: [
                        VocabTooltip(
                          message: ichirou,
                          inlineSpan: const TextSpan(
                            text: '一郎',
                          ),
                        ),
                      ]),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: aru,
                            inlineSpan: const TextSpan(
                              text: 'ある',
                            ),
                          ),
                          VocabTooltip(
                            message: wake,
                            inlineSpan: TextSpan(
                              text: 'わけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: TextSpan(
                              text: 'ない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: 'でしょう。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Ichirou'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text:
                                'There’s no way I would have ever gone to her house, right?',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const Heading(text: 'Example 3', level: 2),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(children: [
                        VocabTooltip(
                          message: naoko,
                          inlineSpan: const TextSpan(
                            text: '直子',
                          ),
                        ),
                      ]),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: bisekibun,
                            inlineSpan: const TextSpan(
                              text: '微積分',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: wakaru,
                            inlineSpan: const TextSpan(
                              text: '分かる',
                            ),
                          ),
                          const TextSpan(
                            text: '？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Naoko'),
                    text: Text(
                        'Do you understand (differential and integral) calculus?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: Text.rich(
                      TextSpan(children: [
                        VocabTooltip(
                          message: ichirou,
                          inlineSpan: const TextSpan(
                            text: '一郎',
                          ),
                        ),
                      ]),
                    ),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: wakaru,
                            inlineSpan: const TextSpan(
                              text: '分かる',
                            ),
                          ),
                          VocabTooltip(
                            message: wake,
                            inlineSpan: TextSpan(
                              text: 'わけ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          VocabTooltip(
                            message: aru,
                            inlineSpan: TextSpan(
                              text: 'ない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                          const TextSpan(
                            text: 'よ！',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Ichirou'),
                    text: Text('There’s no way I would understand!'),
                    english: true,
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'There is one thing to be careful of because 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 can also mean that something is very easy (lit: requires no explanation). You can easily tell when this meaning is intended however, because it is used in the same manner as an adjective.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: shiken,
                              inlineSpan: const TextSpan(
                                text: '試験',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: goukaku,
                              inlineSpan: const TextSpan(
                                text: '合格',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: 'のは'),
                            VocabTooltip(
                              message: wake,
                              inlineSpan: TextSpan(
                                text: 'わけ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('It’s easy to pass the tests here.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Finally, although not as common, 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 can also be used as a formal expression for saying that something must or must not be done at all costs. This is simply a stronger and more formal version of 「～てはいけない」. This grammar is created by simply attaching 「',
                  ),
                  VocabTooltip(
                    message: wake,
                    inlineSpan: const TextSpan(
                      text: 'わけ',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'にはいかない」. The 「は」 is the topic particle and is pronounced 「わ」. The reason 「いけない」 changes to 「いかない」 is probably related to intransitive and transitive verbs but I don’t want to get too caught up in the logistics of it. Just take note that it’s 「い',
                  ),
                  TextSpan(
                    text: 'か',
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                        decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text: 'ない」 in this case and not 「い',
                  ),
                  TextSpan(
                    text: 'け',
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                        decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text: 'ない」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kondo,
                              inlineSpan: const TextSpan(
                                text: '今度',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: makeru,
                              inlineSpan: const TextSpan(
                                text: '負ける',
                              ),
                            ),
                            VocabTooltip(
                              message: wake,
                              inlineSpan: TextSpan(
                                text: 'わけ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'にはいかない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('This time, I must not lose at all costs.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'まで'),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: akirameru,
                              inlineSpan: const TextSpan(
                                text: 'あきらめる',
                              ),
                            ),
                            VocabTooltip(
                              message: wake,
                              inlineSpan: TextSpan(
                                text: 'わけ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'にはいかない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('After coming this far, I must not give up.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 訳 【わけ】 – meaning; reason; can be deduced 直子 【なお・こ】 – Naoko (first name) いくら – how much 英語 【えい・ご】 – English (language) 勉強 【べん・きょう】 – study する (exception) – to do うまい (i-adj) – skillful; delicious なる (u-verb) – to become つまり – in short 語学 【ご・がく】 – language study 能力 【のう・りょく】 – ability ある (u-verb) – to exist (inanimate) 言う 【い・う】 (u-verb) – to say 失礼 【しつ・れい】 – discourtesy 中国語 【ちゅう・ごく・ご】 – Chinese language 読む 【よ・む】 (u-verb) – to read 広子 【ひろ・こ】 – Hiroko (first name) 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 行く 【い・く】 (u-verb) – to go こと – event, matter 一郎 【いち・ろう】 – Ichirou (first name) 微積分 【び・せき・ぶん】 – (differential and integral) calculus 分かる 【わ・かる】 (u-verb) – to understand ここ – here 試験 【し・けん】 – exam 合格 【ごう・かく】 – pass (as in an exam) 今度 【こん・ど】 – this time; another time 負ける 【ま・ける】 (ru-verb) – to lose 来る 【く・る】 (exception) – to come あきらめる (ru-verb) – to give up The noun 「わけ」（訳） is a bit difficult to describe but it’s defined as: “meaning; reason; can be deduced”. You can see how this word is used in the following mini-dialogue. Example 1 直子：いくら英語を勉強しても、うまくならないの。 Naoko: No matter how much I study, I don’t become better at English. ジム：つまり、語学には、能力がないという訳か。 Jim：So basically, it means that you don’t have ability at language. 直子：失礼ね。 Naoko: How rude. As you can see, Jim is concluding from what Naoko said that she must not have any skills at learning languages. This is completely different from the explanatory 「の」, which is used to explain something that may or may not be obvious. 「わけ」 is instead used to draw conclusions that anyone might be able to arrive at given certain information. A very useful application of this grammar is to combine it with 「ない」 to indicate that there is no reasonable conclusion. This allows some very useful expression like, “How in the world am I supposed to know that?” 中国語が読めるわけがない。 There’s no way I can read Chinese. (lit: There is no reasoning for [me] to be able to read Chinese.) Under the normal rules of grammar, we must have a particle for the noun 「わけ」 in order to use it with the verb but since this type of expression is used so often, the particle is often dropped to create just 「～わけない」. Example 2 直子：広子の家に行ったことある？ Naoko: Have you ever gone to Hiroko’s house? 一郎：あるわけないでしょう。 Ichirou：There’s no way I would have ever gone to her house, right? Example 3 直子：微積分は分かる？ Naoko: Do you understand (differential and integral) calculus? 一郎：分かるわけないよ！ Ichirou: There’s no way I would understand! There is one thing to be careful of because 「わけない」 can also mean that something is very easy (lit: requires no explanation). You can easily tell when this meaning is intended however, because it is used in the same manner as an adjective. ここの試験に合格するのはわけない。 It’s easy to pass the tests here. Finally, although not as common, 「わけ」 can also be used as a formal expression for saying that something must or must not be done at all costs. This is simply a stronger and more formal version of 「～てはいけない」. This grammar is created by simply attaching 「わけにはいかない」. The 「は」 is the topic particle and is pronounced 「わ」. The reason 「いけない」 changes to 「いかない」 is probably related to intransitive and transitive verbs but I don’t want to get too caught up in the logistics of it. Just take note that it’s 「いかない」 in this case and not 「いけない」. 今度は負けるわけにはいかない。 This time, I must not lose at all costs. ここまできて、あきらめるわけにはいかない。 After coming this far, I must not give up.',
      grammars: ['わけ','わけにはいかない'],
      keywords: ['wake','wakeniwaikanai','wakenihaikanai','reason','concluding'],
    ),
    Section(
      heading: 'Making hypotheses with 「とする」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('する (exception) – to do'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('今 【いま】 – now'),
                  Text('～時 【～じ】 – counter for hours'),
                  Text('着く 【つ・く】 (u-verb) – to arrive'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('観客 【かん・きゃく】 – spectator'),
                  Text('参加 【さん・か】 – participation'),
                  Text('もらう – to receive'),
                  Text('被害者 【ひ・がい・しゃ】 – victim'),
                  Text('非常 【ひ・じょう】 – extreme'),
                  Text('幸い 【さいわ・い】 (na-adj) – fortunate'),
                  Text('朝ご飯 【あさ・ご・はん】 – breakfast'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('もう – already'),
                  Text('昼 【ひる】 – afternoon'),
                  Text('お腹 【お・なか】 – stomach'),
                  Text('空く 【す・く】 (u-verb) – to become empty'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'While this next grammar doesn’t necessarily have anything directly related to the previous grammar, I thought it would fit nicely together. In a previous lesson, we learn how to combine the volitional form with 「と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to indicate an attempt to perform an action. We will now learn several other ways 「と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 can be used. It may help to keep in mind that 「と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is really just a combination of the quotation particle 「と」 and the verb 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 meaning “to do”. Let’s say you have a sentence: [verb]と',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '. This means literally that you are doing like “[verb]” (in quotes). As you can see, when used with the volitional, it becomes: “Doing like making motion to do [verb]”. In other words, you are acting as if to make a motion to do [verb]. As we have already seen, this translates to “attempt to do [verb]”. Let’s see what happens when we use it on plain verbs.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Assume we go tomorrow.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The example above is considering what would happen supposing that they should decide to go tomorrow. You can see that the literal translation “do like we go tomorrow” still makes sense. However, in this situation, we are making a hypothesis unlike the grammar we have gone over before with the volitional form of the verb. Since we are considering a hypothesis, it is reasonable to assume that the conditional will be very handy here and indeed, you will often see sentences like the following:',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'したら',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kuji,
                              inlineSpan: const TextSpan(
                                text: '９時',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: tsuku,
                              inlineSpan: const TextSpan(
                                text: '着く',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思います',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'If we suppose that we go from now, I think we will arrive at 9:00.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'As you can see, the verb 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 has been conjugated to the 「たら」 conditional form to consider what would happen ',
                  ),
                  const TextSpan(
                    text: 'if you assume',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text: ' a certain case. You can also change 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to the te-form （',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'して',
                    ),
                  ),
                  const TextSpan(
                    text: '） and use it as a sequence of actions like so:',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kankyaku,
                              inlineSpan: const TextSpan(
                                text: '観客',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: sanka,
                              inlineSpan: const TextSpan(
                                text: '参加',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'させて',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Received favor of allowing to participate as spectator.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: higaisha,
                              inlineSpan: const TextSpan(
                                text: '被害者',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'は',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: hijou,
                              inlineSpan: const TextSpan(
                                text: '非常',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: saiwai,
                              inlineSpan: const TextSpan(
                                text: '幸い',
                              ),
                            ),
                            const TextSpan(text: 'だった。'),
                          ],
                        ),
                      ),
                      const Text('As a victim, was extremely fortunate.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: asagohan,
                              inlineSpan: const TextSpan(
                                text: '朝ご飯',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べた',
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'も',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: hiru,
                              inlineSpan: const TextSpan(
                                text: '昼',
                              ),
                            ),
                            const TextSpan(text: 'だから'),
                            VocabTooltip(
                              message: onaka,
                              inlineSpan: const TextSpan(
                                text: 'お腹',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suku,
                              inlineSpan: const TextSpan(
                                text: '空いた',
                              ),
                            ),
                            const TextSpan(text: 'でしょう。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Even assuming that you ate breakfast, because it’s already noon, you’re probably hungry, right?'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The same idea applies here as well. In example 1, you are doing like a “spectator” and doing like a “victim” in example 2 and finally, doing like you ate breakfast in example 3. So you can see why the same grammar applies for all these types of sentences because they all mean the same thing in Japanese (minus the use of additional particles and various conjugations of 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text: '」).',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary する (exception) – to do 明日 【あした】 – tomorrow 行く 【い・く】 (u-verb) – to go 今 【いま】 – now～時 【～じ】 – counter for hours 着く 【つ・く】 (u-verb) – to arrive 思う 【おも・う】 (u-verb) – to think 観客 【かん・きゃく】 – spectator 参加 【さん・か】 – participation もらう – to receive 被害者 【ひ・がい・しゃ】 – victim 非常 【ひ・じょう】 – extreme 幸い 【さいわ・い】 (na-adj) – fortunate 朝ご飯 【あさ・ご・はん】 – breakfast 食べる 【た・べる】 (ru-verb) – to eat もう – already 昼 【ひる】 – afternoon お腹 【お・なか】 – stomach 空く 【す・く】 (u-verb) – to become empty While this next grammar doesn’t necessarily have anything directly related to the previous grammar, I thought it would fit nicely together. In a previous lesson, we learn how to combine the volitional form with 「とする」 to indicate an attempt to perform an action. We will now learn several other ways 「とする」 is really just a combination of the quotation particle 「と」 and the verb 「する」 can be used. It may help to keep in mind that 「とする」 meaning “to do”. Let’s say you have a sentence: [verb]とする. This means literally that you are doing like “[verb]” (in quotes). As you can see, when used with the volitional, it becomes: “Doing like making motion to do [verb]”. In other words, you are acting as if to make a motion to do [verb]. As we have already seen, this translates to “attempt to do [verb]”. Let’s see what happens when we use it on plain verbs. Examples 明日に行くとする。 Assume we go tomorrow. The example above is considering what would happen supposing that they should decide to go tomorrow. You can see that the literal translation “do like we go tomorrow” still makes sense. However, in this situation, we are making a hypothesis unlike the grammar we have gone over before with the volitional form of the verb. Since we are considering a hypothesis, it is reasonable to assume that the conditional will be very handy here and indeed, you will often see sentences like the following: 今から行くとしたら、９時に着くと思います。 If we suppose that we go from now, I think we will arrive at 9:00. As you can see, the verb 「する」 has been conjugated to the 「たら」 conditional form to consider what would happen if you assume a certain case. You can also change 「する」 to the te-form （して） and use it as a sequence of actions like so: 観客として参加させてもらった。 Received favor of allowing to participate as spectator. 被害者としては、非常に幸いだった。 As a victim, was extremely fortunate. 朝ご飯を食べたとしても、もう昼だからお腹が空いたでしょう。 Even assuming that you ate breakfast, because it’s already noon, you’re probably hungry, right? The same idea applies here as well. In example 1, you are doing like a “spectator” and doing like a “victim” in example 2 and finally, doing like you ate breakfast in example 3. So you can see why the same grammar applies for all these types of sentences because they all mean the same thing in Japanese (minus the use of additional particles and various conjugations of 「する」).',
      grammars: ['とする'],
      keywords: ['tosuru','hypothesizing','if','assuming'],
    ),
  ],
);
