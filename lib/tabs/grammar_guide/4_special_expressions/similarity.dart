import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson similarity = Lesson(
  title: 'Similarity or Hearsay',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In Japanese there are many different ways to express likeness or similarity depending on appearance, behavior, or outcome. When learning these expressions for the first time, it is difficult to understand what the differences are between them because they all translate to the same thing in English. This lesson is designed to study the differences between these expressions so that you can start to get a sense of which is appropriate for what you want to say.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In Japanese there are many different ways to express likeness or similarity depending on appearance, behavior, or outcome. When learning these expressions for the first time, it is difficult to understand what the differences are between them because they all translate to the same thing in English. This lesson is designed to study the differences between these expressions so that you can start to get a sense of which is appropriate for what you want to say.',
      grammars: [],
      keywords: ['similarity','hearsay'],
    ),
    Section(
      heading: 'Expressing similarity with よう （様）',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ここ – here'),
                  Text('誰 【だれ】 – who'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('学生 【がく・せい】 – student'),
                  Text('静か 【しず・か】 (na-adj) – quiet'),
                  Text('あの – that (over there) （abbr. of あれの）'),
                  Text('人 【ひと】 – person'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('気 【き】 – mood; intent'),
                  Text('する (exception) – to do'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('雰囲気【ふん・い・き】 – atmosphere; mood'),
                  Text('ちょっと – a little'),
                  Text('怒る 【おこ・る】 (u-verb) – to get angry'),
                  Text('聞こえる 【き・こえる】 (ru-verb) – to be audible'),
                  Text('何 【なに／なん】 – what'),
                  Text('起こる 【おこ・る】 (u-verb) – to happen'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'We’ve already briefly gone over 「',
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and learned that 「',
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 means an appearance or manner. We can use this definition to say that something has an appearance or manner of a certain state. This word can be used in many ways to express similarity. The simplest example is by directly modifying the relative clause. When the sentence ends in 「',
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, you must explicitly express the state-of-being by adding 「だ」, 「です」, or 「でございます」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: 'ここには、'),
                            VocabTooltip(
                              message: daremo,
                              inlineSpan: const TextSpan(
                                text: '誰も',
                              ),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いない',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Looks like no one is here.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: const TextSpan(
                                text: '観た',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Looks like (he) watched the movie.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When directly modifying nouns or na-adjectives, you must use the 「の」 particle for nouns or attach 「な」 to na-adjectives.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            TextSpan(
                              text: 'の',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text('Looks like it’s a student.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koko,
                              inlineSpan: const TextSpan(
                                text: 'ここ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: shizuka,
                              inlineSpan: const TextSpan(
                                text: '静か',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text('Looks like it’s quiet.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Notice that example 1 does not say that the person looks ',
                  ),
                  const TextSpan(
                    text: 'like a student',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text:
                        '. Rather, the declarative 「だ」 states that the person ',
                  ),
                  const TextSpan(
                    text: 'appears to be a student',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text: '. On a side note, you can’t say 「',
                  ),
                  VocabTooltip(
                    message: oishii,
                    inlineSpan: const TextSpan(
                      text: 'おいしい',
                    ),
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だ」 to say that something looks tasty. This is like saying, “This dish apparently is tasty,” which can actually be kind of rude.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can also use it as a na-adjective to describe something that appears to be something else.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ano,
                              inlineSpan: const TextSpan(
                                text: 'あの',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見た',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: ki,
                              inlineSpan: const TextSpan(
                                text: '気',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Had a feeling like I saw that person before.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            TextSpan(
                              text: 'な',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: funiki,
                              inlineSpan: const TextSpan(
                                text: '雰囲気',
                              ),
                            ),
                            const TextSpan(text: 'ですね。'),
                          ],
                        ),
                      ),
                      const Text('He has a student-like atmosphere.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Finally, we can attach the target particle to say things like, “I heard it like that” or “I said it like…”.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: okoru,
                              inlineSpan: const TextSpan(
                                text: '怒った',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: kikoeru,
                              inlineSpan: const TextSpan(
                                text: '聞こえた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Was able to hear it like (she) was a little mad.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: okoru,
                              inlineSpan: const TextSpan(
                                text: '起こらなかった',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言った',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Said (it) like nothing happened.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ここ – here 誰 【だれ】 – who いる (ru-verb) – to exist (animate) 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch 学生 【がく・せい】 – student 静か 【しず・か】 (na-adj) – quiet あの – that (over there) （abbr. of あれの） 人 【ひと】 – person 見る 【み・る】 (ru-verb) – to see 気 【き】 – mood; intent する (exception) – to do 彼 【かれ】 – he; boyfriend 雰囲気【ふん・い・き】 – atmosphere; mood ちょっと – a little 怒る 【おこ・る】 (u-verb) – to get angry 聞こえる 【き・こえる】 (ru-verb) – to be audible 何 【なに／なん】 – what 起こる 【おこ・る】 (u-verb) – to happen 言う 【い・う】 (u-verb) – to say We’ve already briefly gone over 「よう」 and learned that 「よう」 means an appearance or manner. We can use this definition to say that something has an appearance or manner of a certain state. This word can be used in many ways to express similarity. The simplest example is by directly modifying the relative clause. When the sentence ends in 「よう」, you must explicitly express the state-of-being by adding 「だ」, 「です」, or 「でございます」. ここには、誰もいないようだ。 Looks like no one is here. 映画を観たようです。 Looks like (he) watched the movie. When directly modifying nouns or na-adjectives, you must use the 「の」 particle for nouns or attach 「な」 to na-adjectives. 学生のようだ。 Looks like it’s a student. ここは静かなようだ。 Looks like it’s quiet. Notice that example 1 does not say that the person looks like a student. Rather, the declarative 「だ」 states that the person appears to be a student. On a side note, you can’t say 「おいしいようだ」 to say that something looks tasty. This is like saying, “This dish apparently is tasty,” which can actually be kind of rude. You can also use it as a na-adjective to describe something that appears to be something else. あの人を見たような気がした。 Had a feeling like I saw that person before. 彼は学生のような雰囲気ですね。 He has a student-like atmosphere. Finally, we can attach the target particle to say things like, “I heard it like that” or “I said it like…”. ちょっと怒ったように聞こえた。 Was able to hear it like (she) was a little mad. 何も起こらなかったのように言った。 Said (it) like nothing happened.',
      grammars: ['よう （様）'],
      keywords: ['you','seem'],
    ),
    Section(
      heading: 'Using 「みたい」 to say something looks like something else',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('犬 【いぬ】 – dog'),
                  Text('もう – already'),
                  Text('売り切れ 【う・り・き・れ】 – sold out'),
                  Text('制服 【せい・ふく】 – uniform'),
                  Text('着る 【き・る】 (ru-verb) – to wear'),
                  Text('姿 【すがた】 – figure'),
                  Text('学生 【がく・せい】 – student'),
                  Text('この – this （abbr. of これの）'),
                  Text('ピザ – pizza'),
                  Text(
                      'お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake)'),
                  Text('見える 【み・える】 (ru-verb) – to be visible'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Another way to express similarity which is considered more casual is by using 「みたい」. Do not confuse this with the 「たい」 conjugation of 「',
                  ),
                  VocabTooltip(
                    message: miru,
                    inlineSpan: const TextSpan(
                      text: '見る',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. The main difference is that this 「みたい」 can be attached directly to nouns, adjectives, and verbs just like particles which i-adjectives like 「～たい」 obviously can’t do.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「みたい」 to say something looks like something else',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'Attach 「みたい」 to the noun that bears the resemblance. 「みたい」 conjugates like a noun or na-adjective and not an i-adjective.'),
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption: 'Conjugation Example with 「犬」'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: ' ',
                                      ),
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Non-Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: inu,
                                                    inlineSpan: const TextSpan(
                                                      text: '犬',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'みたい',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('looks like a dog'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: inu,
                                                    inlineSpan: const TextSpan(
                                                      text: '犬',
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: 'じゃない',
                                                    style: TextStyle(
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .primary,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'みたい',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text(
                                                'doesn’t look like a dog'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: inu,
                                                    inlineSpan: const TextSpan(
                                                      text: '犬',
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: 'だった',
                                                    style: TextStyle(
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .primary,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'みたい',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('looked like a dog'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: inu,
                                                    inlineSpan: const TextSpan(
                                                      text: '犬',
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: 'じゃなかった',
                                                    style: TextStyle(
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .primary,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'みたい',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text(
                                                'didn’t look like a dog'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: urikire,
                              inlineSpan: const TextSpan(
                                text: '売り切れ',
                              ),
                            ),
                            TextSpan(
                              text: 'みたい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Looks like it’s sold out already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: seifuku,
                              inlineSpan: const TextSpan(
                                text: '制服',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kiruWear,
                              inlineSpan: const TextSpan(
                                text: '着ている',
                              ),
                            ),
                            VocabTooltip(
                              message: sugata,
                              inlineSpan: const TextSpan(
                                text: '姿',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: 'みる',
                              ),
                            ),
                            const TextSpan(text: 'と、'),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            TextSpan(
                              text: 'みたい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Looking at the uniform-wearing figure, (person) looks like a student.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The implied meaning here is the person wearing the uniform is not really a student because he/she only looks like a student. This is different from example 3 from the previous 「',
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 section which implied that the person appears to be (but might not be) a student. Again, we also can’t say 「',
                  ),
                  VocabTooltip(
                    message: oishii,
                    inlineSpan: const TextSpan(
                      text: 'おいしい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'みたい」 to say that something looks tasty because it implies that, in actuality, the food might not be so good.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Don’t forget that 「みたい」 does not conjugate like the 「～たい」 form or i-adjectives.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: piza,
                              inlineSpan: const TextSpan(
                                text: 'ピザ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: okonomiyaki,
                              inlineSpan: const TextSpan(
                                text: 'お好み焼き',
                              ),
                            ),
                            const TextSpan(text: 'みた'),
                            TextSpan(
                              text: 'くない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('(みたい conjugates like a na-adjective.)'),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: piza,
                              inlineSpan: const TextSpan(
                                text: 'ピザ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: okonomiyaki,
                              inlineSpan: const TextSpan(
                                text: 'お好み焼き',
                              ),
                            ),
                            const TextSpan(text: 'みたい'),
                            TextSpan(
                              text: 'じゃない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Doesn’t this pizza looks like okonomiyaki?'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「みたい」 is a grammar used mostly for conversational Japanese. Do not use it in essays, articles, or anything that needs to sound authoritative. You can use 「',
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 instead in the following fashion.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: urikire,
                              inlineSpan: const TextSpan(
                                text: '売り切れ',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('It appears that it is sold-out already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: piza,
                              inlineSpan: const TextSpan(
                                text: 'ピザ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: okonomiyaki,
                              inlineSpan: const TextSpan(
                                text: 'お好み焼き',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: mieru,
                              inlineSpan: const TextSpan(
                                text: '見える',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('This pizza looks like okonomiyaki.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見る 【み・る】 (ru-verb) – to see 犬 【いぬ】 – dog もう – already 売り切れ 【う・り・き・れ】 – sold out 制服 【せい・ふく】 – uniform 着る 【き・る】 (ru-verb) – to wear 姿 【すがた】 – figure 学生 【がく・せい】 – student この – this （abbr. of これの） ピザ – pizza お好み焼き 【お・この・み・や・き】 – okonomiyaki (Japanese-style pancake) 見える 【み・える】 (ru-verb) – to be visible Another way to express similarity which is considered more casual is by using 「みたい」. Do not confuse this with the 「たい」 conjugation of 「見る」. The main difference is that this 「みたい」 can be attached directly to nouns, adjectives, and verbs just like particles which i-adjectives like 「～たい」 obviously can’t do. Using 「みたい」 to say something looks like something else Attach 「みたい」 to the noun that bears the resemblance. 「みたい」 conjugates like a noun or na-adjective and not an i-adjective. Examples もう売り切れみたい。 Looks like it’s sold out already. 制服を着ている姿をみると、学生みたいです。 Looking at the uniform-wearing figure, (person) looks like a student. The implied meaning here is the person wearing the uniform is not really a student because he/she only looks like a student. This is different from example 3 from the previous 「よう」 section which implied that the person appears to be (but might not be) a student. Again, we also can’t say 「おいしいみたい」 to say that something looks tasty because it implies that, in actuality, the food might not be so good. Don’t forget that 「みたい」 does not conjugate like the 「～たい」 form or i-adjectives. このピザはお好み焼きみたくない？(みたい conjugates like a na-adjective.) このピザはお好み焼きみたいじゃない？ Doesn’t this pizza looks like okonomiyaki? 「みたい」 is a grammar used mostly for conversational Japanese. Do not use it in essays, articles, or anything that needs to sound authoritative. You can use 「よう」 instead in the following fashion. もう売り切れのようだ。 It appears that it is sold-out already. このピザはお好み焼きのように見える。 This pizza looks like okonomiyaki.',
      grammars: ['みたい'],
      keywords: ['mitai','look like'],
    ),
    // TODO: change notesection in below section to conform
    Section(
      heading: 'Guessing at an outcome using 「～そう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('いい (i-adj) – good'),
                  Text('バランス – balance'),
                  Text('崩れる 【くず・れる】 (ru-verb) – to collapse; to crumble'),
                  Text('一瞬 【いっ・しゅん】 – an instant'),
                  Text('倒れる 【たお・れる】 (ru-verb) – to collapse; to fall'),
                  Text('この – this （abbr. of これの）'),
                  Text('辺り 【あた・り】 – vicinity'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('漬物 【つけ・もの】 – pickled vegetable'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('これ – this'),
                  Text('結構 【けっ・こう】 – fairly, reasonably'),
                  Text('やはり／やっぱり – as I thought'),
                  Text('高い 【たか・い】 (i-adj) – high; tall; expensive'),
                  Text('お前 【お・まえ】 – you (casual)'),
                  Text('金髪 【きん・ぱつ】 – blond hair'),
                  Text('女 【おんな】 – woman; girl'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('もう – already'),
                  Text('～時 【～じ】 – counter for hours'),
                  Text('なる (u-verb) – to become'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('ただ – free of charge; only'),
                  Text('試合 【し・あい】 – match, game'),
                  Text('その – that （abbr. of それの）'),
                  Text('人 【ひと】 – person'),
                  Text('学生 【がく・せい】 – student'),
                  Text('かわいい (i-adj) – cute'),
                  Text('かわいそう (i-adj) – pitiable'),
                  Text('犬 【いぬ】 – dog'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The problem with English is that the expression, “seems like” has too many meanings. It can mean similarity in appearance, similarity in behavior or even that current evidence points to a likely outcome. We will now learn how to say the third meaning: how to indicate a likely outcome given the situation.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Just like the grammar we have learned so far in this lesson, we can use this grammar by simply attaching 「そう」 to the end of verbs, and adjectives. However, there are four important different cases. Actually, I just noticed this but the conjugation rules are exactly the same as the 「～',
                  ),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 grammar we learned in the last section. The only difference is that for the adjective 「',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'いい',
                    ),
                  ),
                  const TextSpan(
                    text: '」, you need to change it to 「',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'よさ',
                    ),
                  ),
                  const TextSpan(
                    text: '」 before attaching 「そう」 to create 「',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(
                      text: 'よさ',
                    ),
                  ),
                  const TextSpan(
                    text: 'そう」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Notes',
                content: NumberedList(
                  items: [
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'Verbs must be changed to the ',
                          ),
                          TextSpan(
                            text: 'stem',
                            style: TextStyle(
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                          TextSpan(
                            text: '.',
                          ),
                        ],
                      ),
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text:
                                'The 「い」 in i-adjectives must be dropped except for 「',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(
                            text: '」.',
                          ),
                        ],
                      ),
                    ),
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: '「',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'いい',
                            ),
                          ),
                          const TextSpan(
                            text: '」 must first be conjugated to 「',
                          ),
                          VocabTooltip(
                            message: ii,
                            inlineSpan: const TextSpan(
                              text: 'よさ',
                            ),
                          ),
                          const TextSpan(
                            text: '」.',
                          ),
                        ],
                      ),
                    ),
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text:
                                'For all negatives, the 「い」 must be replaced with 「さ」.',
                          ),
                        ],
                      ),
                    ),
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text:
                                'This grammar does not work with plain nouns.',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Heading(
                  text: '1. Verb must be changed to the stem.', level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'For ru-verbs, remove the 「る」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: baransu,
                              inlineSpan: const TextSpan(
                                text: 'バランス',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kuzureru,
                              inlineSpan: const TextSpan(
                                text: '崩れて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: isshun,
                              inlineSpan: const TextSpan(
                                text: '一瞬',
                              ),
                            ),
                            VocabTooltip(
                              message: taoreru,
                              inlineSpan: TextSpan(
                                text: '倒れ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'そうだった。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Losing my balance, I seemed likely to fall for a moment.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For u-verbs, change the / u / vowel sound to an / i / vowel sound.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: atari,
                              inlineSpan: const TextSpan(
                                text: '辺り',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あり',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'そうだけどな。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It seems likely that it would be around here but…'),
                    ],
                  ),
                ],
              ),
              const Heading(
                  text: '2. The 「い」 in i-adjectives must be dropped.',
                  level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In the next example, the 「い」 has been dropped from 「',
                  ),
                  VocabTooltip(
                    message: oishii,
                    inlineSpan: const TextSpan(
                      text: 'おいしい',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: tsukemono,
                              inlineSpan: const TextSpan(
                                text: '漬物',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: TextSpan(
                                text: 'おいし',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'そう！'),
                          ],
                        ),
                      ),
                      const Text(
                          'I bet this pickled vegetable is tasty! (This pickled vegetable looks good!)'),
                    ],
                  ),
                ],
              ),
              const Heading(
                  text: '3. 「いい」 must first be conjugated to 「よさ」',
                  level: 2),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The only exception to rule 2 is the adjective 「いい」. When using this grammar with 「いい」, you must first change it to 「よさ」.',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: kekkyoku,
                              inlineSpan: const TextSpan(
                                text: '結構',
                              ),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'よさ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'そう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'だけど、'),
                            VocabTooltip(
                              message: yappari,
                              inlineSpan: const TextSpan(
                                text: 'やっぱり',
                              ),
                            ),
                            VocabTooltip(
                              message: takai,
                              inlineSpan: const TextSpan(
                                text: '高い',
                              ),
                            ),
                            const TextSpan(text: 'よね。'),
                          ],
                        ),
                      ),
                      const Text(
                          'This one also seems to be good but, as expected, it’s expensive, huh?'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Nothing needs to be done for na-adjectives.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: omae,
                              inlineSpan: const TextSpan(
                                text: 'お前',
                              ),
                            ),
                            const TextSpan(text: 'なら、'),
                            VocabTooltip(
                              message: kinpatsu,
                              inlineSpan: const TextSpan(
                                text: '金髪',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: onna,
                              inlineSpan: const TextSpan(
                                text: '女',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: TextSpan(
                                text: '好き',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(
                              text: 'そうだな。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Knowing you, I bet you like blond-haired girls.'),
                    ],
                  ),
                ],
              ),
              const Heading(
                  text:
                      '4. For all negatives, the 「い」 must be replaced with 「さ」.',
                  level: 2),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The negative of 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: '来る',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 so when used with 「～そう」, it becomes 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(
                      text: 'こなさ',
                    ),
                  ),
                  const TextSpan(
                    text: 'そう」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: juuji,
                              inlineSpan: const TextSpan(
                                text: '10時',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(text: 'から、'),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: TextSpan(
                                text: '来なさ',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'そう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: 'だね。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'Since it already became 10:00, it’s likely that (person) won’t come.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: tada,
                              inlineSpan: const TextSpan(
                                text: 'ただ',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: shiai,
                              inlineSpan: const TextSpan(
                                text: '試合',
                              ),
                            ),
                            TextSpan(
                              text: 'じゃなさそうだ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text('This isn’t likely to be an ordinary match.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Identical to the 「～',
                  ),
                  VocabTooltip(
                    message: sugiru,
                    inlineSpan: const TextSpan(
                      text: 'すぎる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 grammar, i-adjectives that are derived from the negative 「～ない」 like 「',
                  ),
                  VocabTooltip(
                    message: mottainai,
                    inlineSpan: const TextSpan(
                      text: 'もったいない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: nasakenai,
                    inlineSpan: const TextSpan(
                      text: '情けない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 also follow this rule as well (which would be 「',
                  ),
                  VocabTooltip(
                    message: mottainai,
                    inlineSpan: const TextSpan(
                      text: 'もったいなさ',
                    ),
                  ),
                  const TextSpan(
                    text: 'そう」 and 「',
                  ),
                  VocabTooltip(
                    message: nasakenai,
                    inlineSpan: const TextSpan(
                      text: '情けなさ',
                    ),
                  ),
                  const TextSpan(
                    text: 'そう」 in this case).',
                  ),
                ],
              ),
              const Heading(
                  text: '5. This grammar does not work with plain nouns.',
                  level: 2),
              BulletedList(
                items: [
                  Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: sono,
                          inlineSpan: const TextSpan(
                            text: 'その',
                          ),
                        ),
                        VocabTooltip(
                          message: hito,
                          inlineSpan: const TextSpan(
                            text: '人',
                          ),
                        ),
                        const TextSpan(text: 'は'),
                        VocabTooltip(
                          message: gakusei,
                          inlineSpan: const TextSpan(
                            text: '学生',
                          ),
                        ),
                        TextSpan(
                          text: 'そう',
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                        const TextSpan(text: '。'),
                      ],
                    ),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'There are other grammar we have already covered that can be used to indicate that something is likely to be something else.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            TextSpan(
                              text: 'でしょう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('That person is probably student.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: gakusei,
                              inlineSpan: const TextSpan(
                                text: '学生',
                              ),
                            ),
                            TextSpan(
                              text: 'だろう',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('That person is probably student.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Be careful never to use 「',
                  ),
                  VocabTooltip(
                    message: kawaii,
                    inlineSpan: const TextSpan(
                      text: 'かわいい',
                    ),
                  ),
                  const TextSpan(
                    text: '」 with this grammar. 「',
                  ),
                  VocabTooltip(
                    message: kawaisou,
                    inlineSpan: const TextSpan(
                      text: 'かわいそう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a completely different word used when you feel sorry for something or someone. 「',
                  ),
                  VocabTooltip(
                    message: kawaii,
                    inlineSpan: const TextSpan(
                      text: 'かわいい',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 means, “to look cute” already so you never need to use any of the grammar in this lesson to say something looks cute.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kawaisou,
                              inlineSpan: const TextSpan(
                                text: 'かわいそう',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Oh, this poor dog.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kawaii,
                              inlineSpan: const TextSpan(
                                text: 'かわいい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('This dog is cute.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary いい (i-adj) – good バランス – balance 崩れる 【くず・れる】 (ru-verb) – to collapse; to crumble 一瞬 【いっ・しゅん】 – an instant 倒れる 【たお・れる】 (ru-verb) – to collapse; to fall この – this （abbr. of これの） 辺り 【あた・り】 – vicinity ある (u-verb) – to exist (inanimate) 漬物 【つけ・もの】 – pickled vegetable おいしい (i-adj) – tasty これ – this 結構 【けっ・こう】 – fairly, reasonably やはり／やっぱり – as I thought 高い 【たか・い】 (i-adj) – high; tall; expensive お前 【お・まえ】 – you (casual) 金髪 【きん・ぱつ】 – blond hair 女 【おんな】 – woman; girl 好き 【す・き】 (na-adj) – likable; desirable もう – already～時 【～じ】 – counter for hours なる (u-verb) – to become 来る 【く・る】 (exception) – to come ただ – free of charge; only 試合 【し・あい】 – match, game その – that （abbr. of それの） 人 【ひと】 – person 学生 【がく・せい】 – student かわいい (i-adj) – cute かわいそう (i-adj) – pitiable 犬 【いぬ】 – dog The problem with English is that the expression, “seems like” has too many meanings. It can mean similarity in appearance, similarity in behavior or even that current evidence points to a likely outcome. We will now learn how to say the third meaning: how to indicate a likely outcome given the situation. Just like the grammar we have learned so far in this lesson, we can use this grammar by simply attaching 「そう」 to the end of verbs, and adjectives. However, there are four important different cases. Actually, I just noticed this but the conjugation rules are exactly the same as the 「～すぎる」 grammar we learned in the last section. The only difference is that for the adjective 「いい」, you need to change it to 「よさ」 before attaching 「そう」 to create 「よさそう」. Verbs must be changed to the stem. The 「い」 in i-adjectives must be dropped except for 「いい」. 「いい」 must first be conjugated to 「よさ」. For all negatives, the 「い」 must be replaced with 「さ」. This grammar does not work with plain nouns.  Verb must be changed to the stem. For ru-verbs, remove the 「る」. バランスが崩れて、一瞬倒れそうだった。 Losing my balance, I seemed likely to fall for a moment. For u-verbs, change the / u / vowel sound to an / i / vowel sound. この辺りにありそうだけどな。 It seems likely that it would be around here but…  The 「い」 in i-adjectives must be dropped. In the next example, the 「い」 has been dropped from 「おいしい」. この漬物はおいしそう！ I bet this pickled vegetable is tasty! (This pickled vegetable looks good!) 「いい」 must first be conjugated to 「よさ」The only exception to rule 2 is the adjective 「いい」. When using this grammar with 「いい」, you must first change it to 「よさ」. これも結構よさそうだけど、やっぱり高いよね。 This one also seems to be good but, as expected, it’s expensive, huh? Nothing needs to be done for na-adjectives. お前なら、金髪の女が好きそうだな。 Knowing you, I bet you like blond-haired girls. For all negatives, the 「い」 must be replaced with 「さ」. The negative of 「来る」 is 「こない」 so when used with 「～そう」, it becomes 「こなさそう」. もう10時になったから、来なさそうだね。 Since it already became 10:00, it’s likely that (person) won’t come. これはただの試合じゃなさそうだ。 This isn’t likely to be an ordinary match. Identical to the 「～すぎる」 grammar, i-adjectives that are derived from the negative 「～ない」 like 「もったいない」 or 「情けない」 also follow this rule as well (which would be 「もったいなさそう」 and 「情けなさそう」 in this case). This grammar does not work with plain nouns. その人は学生そう。 There are other grammar we have already covered that can be used to indicate that something is likely to be something else. その人は学生でしょう。 That person is probably student. その人は学生だろう。 That person is probably student.」 is a completely different word used when you feel sorry for something or someone. 「Be careful never to use 「かわいい」 with this grammar. 「かわいそう」 means, “to look cute” already so you never need to use any of the grammar in this lesson to say something looks cute. かわいいこの犬はかわいそう。 Oh, this poor dog. この犬はかわいい。 This dog is cute.',
      grammars: ['～そう'],
      keywords: ['sou','seem like'],
    ),
    Section(
      heading: 'Expressing hearsay using 「～そうだ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('明日 【あした】 – tomorrow'),
                  Text('雨 【あめ】 – rain'),
                  Text('降る 【ふ・る】(u-verb) – to precipitate'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('高校生 【こう・こう・せい】 – high school student'),
                  Text('今日 【きょう】 – today'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                  Text('来る 【く・る】 (exception) – to come'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The reason that there are so many annoying rules to using 「～そう」 is to distinguish it from this next grammar we will learn. This is a useful grammar for talking about things you heard that doesn’t necessary have anything to do with how you yourself, think or feel. Unlike the last grammar we learned, you can simply attach 「そうだ」 to verbs and i-adjectives. For na-adjectives and nouns, you must indicate the state-of-being by adding 「だ」 to the noun/na-adjective. Also, notice that 「そう」 itself must always end in 「だ」、「です」、or 「でございます」. These differences are what distinguishes this grammar from the one we learned in the last section. There are no tenses for this grammar.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ame,
                              inlineSpan: const TextSpan(
                                text: '雨',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: furu,
                              inlineSpan: const TextSpan(
                                text: '降る',
                              ),
                            ),
                            TextSpan(
                              text: 'そうだ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I hear that it’s going to rain tomorrow.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: au,
                              inlineSpan: const TextSpan(
                                text: '会い',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行った',
                              ),
                            ),
                            TextSpan(
                              text: 'そうです',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('I heard he went to meet everyday.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Don’t forget to add 「だ」 for nouns or na-adjectives.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: koukousei,
                              inlineSpan: const TextSpan(
                                text: '高校生',
                              ),
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            const TextSpan(text: 'そうです。'),
                          ],
                        ),
                      ),
                      const Text('I hear that he is a high school student.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'When starting the sentence with this grammar, you also need to add 「だ」 just like you do with 「だから」.',
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ａ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kyou,
                            inlineSpan: const TextSpan(
                              text: '今日',
                            ),
                          ),
                          const TextSpan(
                            text: '、',
                          ),
                          VocabTooltip(
                            message: tanaka,
                            inlineSpan: const TextSpan(
                              text: '田中',
                            ),
                          ),
                          VocabTooltip(
                            message: san,
                            inlineSpan: const TextSpan(
                              text: 'さん',
                            ),
                          ),
                          const TextSpan(
                            text: 'は',
                          ),
                          VocabTooltip(
                            message: kuru,
                            inlineSpan: const TextSpan(
                              text: 'こない',
                            ),
                          ),
                          const TextSpan(
                            text: 'の？',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('A'),
                    text: Text('Is Tanaka-san not coming today?'),
                    english: true,
                  ),
                ],
              ),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'だそうです',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('So I hear.'),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 明日 【あした】 – tomorrow 雨 【あめ】 – rain 降る 【ふ・る】(u-verb) – to precipitate 毎日 【まい・にち】 – everyday 会う 【あ・う】 (u-verb) – to meet 行く 【い・く】 (u-verb) – to go 彼 【かれ】 – he; boyfriend 高校生 【こう・こう・せい】 – high school student 今日 【きょう】 – today 田中 【た・なか】 – Tanaka (last name) 来る 【く・る】 (exception) – to come The reason that there are so many annoying rules to using 「～そう」 is to distinguish it from this next grammar we will learn. This is a useful grammar for talking about things you heard that doesn’t necessary have anything to do with how you yourself, think or feel. Unlike the last grammar we learned, you can simply attach 「そうだ」 to verbs and i-adjectives. For na-adjectives and nouns, you must indicate the state-of-being by adding 「だ」 to the noun/na-adjective. Also, notice that 「そう」 itself must always end in 「だ」、「です」、or 「でございます」. These differences are what distinguishes this grammar from the one we learned in the last section. There are no tenses for this grammar. 明日、雨が降るそうだ。 I hear that it’s going to rain tomorrow. 毎日会いに行ったそうです。 I heard he went to meet everyday. Don’t forget to add 「だ」 for nouns or na-adjectives. 彼は、高校生だそうです。 I hear that he is a high school student. When starting the sentence with this grammar, you also need to add 「だ」 just like you do with 「だから」. Ａ：今日、田中さんはこないの？ A: Is Tanaka-san not coming today? Ｂ：だそうです。 B：So I hear.',
      grammars: ['～そうだ'],
      keywords: ['hearsay','sound like','sou da'],
    ),
    Section(
      heading: 'Expressing hearsay or behavior using 「～らしい」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('今日 【きょう】 – today'),
                  Text('田中 【た・なか】 – Tanaka (last name)'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('あの – that (over there) （abbr. of あれの）'),
                  Text('人 【ひと】 – person'),
                  Text('何 【なん】 – what'),
                  Text('美由紀 【み・ゆ・き】 – Miyuki (first name)'),
                  Text('友達 【とも・だち】 – friend'),
                  Text('子 【こ】 – child'),
                  Text('子供 【こ・ども】 – child'),
                  Text('大人 【おとな】 – adult'),
                  Text('する (exception) – to do'),
                  Text('つもり – intention, plan'),
                  Text('大騒ぎ 【おお・さわ・ぎ】 – big commotion'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「らしい」 can be directly attached to nouns, adjectives, or verbs to show that things appear to be a certain way due to what you’ve heard. This is different from 「～そうだ」 because 「～そうだ」 indicates something you heard about specifically while 「らしい」 means things seem to be a certain way based on some things you heard about the subject. 「らしい」 conjugates like a normal i-adjective.',
                  ),
                ],
              ),
              const Heading(text: 'Example 1', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: kyou,
                          inlineSpan: const TextSpan(
                            text: '今日',
                          ),
                        ),
                        const TextSpan(
                          text: '、',
                        ),
                        VocabTooltip(
                          message: tanaka,
                          inlineSpan: const TextSpan(
                            text: '田中',
                          ),
                        ),
                        VocabTooltip(
                          message: san,
                          inlineSpan: const TextSpan(
                            text: 'さん',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: kuru,
                          inlineSpan: const TextSpan(
                            text: 'こない',
                          ),
                        ),
                        const TextSpan(
                          text: 'の？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('Is Tanaka-san not coming today?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: kuru,
                            inlineSpan: const TextSpan(
                              text: 'こない',
                            ),
                          ),
                          TextSpan(
                            text: 'らしい',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: '。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text('Seems like it (based on what I heard).'),
                  ),
                ],
              ),
              const Heading(text: 'Example 2', level: 2),
              Dialogue(items: [
                DialogueLine(
                  speaker: const Text('Ａ'),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: ano,
                          inlineSpan: const TextSpan(
                            text: 'あの',
                          ),
                        ),
                        VocabTooltip(
                          message: hito,
                          inlineSpan: const TextSpan(
                            text: '人',
                          ),
                        ),
                        const TextSpan(
                          text: 'は',
                        ),
                        VocabTooltip(
                          message: nan,
                          inlineSpan: const TextSpan(
                            text: '何',
                          ),
                        ),
                        const TextSpan(
                          text: 'なの？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('A'),
                  text: Text('What is that person over there?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('Ｂ'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: miyuki,
                            inlineSpan: const TextSpan(
                              text: '美由紀',
                            ),
                          ),
                          VocabTooltip(
                            message: san,
                            inlineSpan: const TextSpan(
                              text: 'さん',
                            ),
                          ),
                          const TextSpan(
                            text: 'の',
                          ),
                          VocabTooltip(
                            message: tomodachi,
                            inlineSpan: const TextSpan(
                              text: '友達',
                            ),
                          ),
                          TextSpan(
                            text: 'らしい',
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          const TextSpan(
                            text: 'ですよ。',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('B'),
                    text: Text(
                        'Seems to be Miyuki-san’s friend (based on what I heard).'),
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Another way to use 「らしい」 is to indicate that a person seems to be a certain thing due to his behavior.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ano,
                              inlineSpan: const TextSpan(
                                text: 'あの',
                              ),
                            ),
                            VocabTooltip(
                              message: ko,
                              inlineSpan: const TextSpan(
                                text: '子',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kodomo,
                              inlineSpan: const TextSpan(
                                text: '子供',
                              ),
                            ),
                            TextSpan(
                              text: 'らしくない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('That child does not act like a child.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: otona,
                              inlineSpan: const TextSpan(
                                text: '大人',
                              ),
                            ),
                            TextSpan(
                              text: 'らしく',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            VocabTooltip(
                              message: tsumori,
                              inlineSpan: const TextSpan(
                                text: 'つもり',
                              ),
                            ),
                            const TextSpan(text: 'だったのに、'),
                            VocabTooltip(
                              message: oosawagi,
                              inlineSpan: const TextSpan(
                                text: '大騒ぎ',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Despite the fact that I planned to act like an adult, I ended up making a big ruckus.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 今日 【きょう】 – today 田中 【た・なか】 – Tanaka (last name) 来る 【く・る】 (exception) – to come あの – that (over there) （abbr. of あれの） 人 【ひと】 – person 何 【なん】 – what 美由紀 【み・ゆ・き】 – Miyuki (first name) 友達 【とも・だち】 – friend 子 【こ】 – child 子供 【こ・ども】 – child 大人 【おとな】 – adult する (exception) – to do つもり – intention, plan 大騒ぎ 【おお・さわ・ぎ】 – big commotion 「らしい」 can be directly attached to nouns, adjectives, or verbs to show that things appear to be a certain way due to what you’ve heard. This is different from 「～そうだ」 because 「～そうだ」 indicates something you heard about specifically while 「らしい」 means things seem to be a certain way based on some things you heard about the subject. 「らしい」 conjugates like a normal i-adjective. Example 1 Ａ：今日、田中さんはこないの？ A: Is Tanaka-san not coming today? Ｂ：こないらしい。 B：Seems like it (based on what I heard). Example 2 Ａ：あの人は何なの？ A: What is that person over there? Ｂ：美由紀さんの友達らしいですよ。 B：Seems to be Miyuki-san’s friend (based on what I heard). Another way to use 「らしい」 is to indicate that a person seems to be a certain thing due to his behavior. あの子は子供らしくない。 That child does not act like a child. 大人らしくするつもりだったのに、大騒ぎしてしまった。 Despite the fact that I planned to act like an adult, I ended up making a big ruckus.',
      grammars: ['～らしい'],
      keywords: ['hearsay','sound like','rashii','act like'],
    ),
    Section(
      heading: '「っぽい」: slang expression of similarity',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('あの – that (over there) （abbr. of あれの'),
                  Text('人 【ひと】 – person'),
                  Text('韓国人 【かん・こく・じん】 – Korean person'),
                  Text('皆 【みんな】 – everybody'),
                  Text('もう – already'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('恭子 【きょう・こ】 – Kyouko (first name)'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('女 【おんな】 – woman; girl'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'A really casual way to express similarity is to attach 「っぽい」 to the word that reflects the resemblance. Because this is a very casual expression, you can use it as a casual version for all the different types of expression for similarity covered above.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        ' 「 っぽい」 conjugates just like an i-adjective, as seen by example 3 below.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ano,
                              inlineSpan: const TextSpan(
                                text: 'あの',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: kankokujin,
                              inlineSpan: const TextSpan(
                                text: '韓国人',
                              ),
                            ),
                            TextSpan(
                              text: 'っぽい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'よね。'),
                          ],
                        ),
                      ),
                      const Text(
                          'That person looks a little like Korean person, huh?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            TextSpan(
                              text: 'っぽい',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It appears that everybody ate everything already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyouko,
                              inlineSpan: const TextSpan(
                                text: '恭子',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: zenzen,
                              inlineSpan: const TextSpan(
                                text: '全然',
                              ),
                            ),
                            VocabTooltip(
                              message: onna,
                              inlineSpan: const TextSpan(
                                text: '女',
                              ),
                            ),
                            TextSpan(
                              text: 'っぽくない',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'ね。'),
                          ],
                        ),
                      ),
                      const Text('Kyouko is not womanly at all, huh?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary あの – that (over there) （abbr. of あれの人 【ひと】 – person 韓国人 【かん・こく・じん】 – Korean person 皆 【みんな】 – everybody もう – already 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 恭子 【きょう・こ】 – Kyouko (first name) 全然 【ぜん・ぜん】 – not at all (when used with negative) 女 【おんな】 – woman; girl A really casual way to express similarity is to attach 「っぽい」 to the word that reflects the resemblance. Because this is a very casual expression, you can use it as a casual version for all the different types of expression for similarity covered above. 「 っぽい」 conjugates just like an i-adjective, as seen by example 3 below. あの人はちょっと韓国人っぽいよね。 That person looks a little like Korean person, huh? みんなで、もう全部食べてしまったっぽいよ。 It appears that everybody ate everything already. 恭子は全然女っぽくないね。 Kyouko is not womanly at all, huh?',
      grammars: ['っぽい'],
      keywords: ['ppoi','-like','-y','-ish'],
    ),
  ],
);
