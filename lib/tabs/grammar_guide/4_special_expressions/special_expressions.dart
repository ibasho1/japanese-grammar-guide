import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson specialExpressions = Lesson(
  title: 'Chapter Overview',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'I have decided to call this next section “Special Expressions” only because with the exception of the first few lessons, most of the grammar here applies to more specific areas than the grammar we have covered so far. These special expressions, while individually not vital, are, as a collection, necessary for regular everyday conversations. We are slowly entering the stage where we’ve built the toolbox and we now need to acquire the little tools that will make the toolbox complete. Now that we covered most of the base, it is time to look at all the little itty gritty bits. You are welcome to skip around the lessons, however; the examples will assume that you have gone over all previous sections.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'I have decided to call this next section “Special Expressions” only because with the exception of the first few lessons, most of the grammar here applies to more specific areas than the grammar we have covered so far. These special expressions, while individually not vital, are, as a collection, necessary for regular everyday conversations. We are slowly entering the stage where we’ve built the toolbox and we now need to acquire the little tools that will make the toolbox complete. Now that we covered most of the base, it is time to look at all the little itty gritty bits. You are welcome to skip around the lessons, however; the examples will assume that you have gone over all previous sections.',
      grammars: [],
      keywords: ['special expressions','chapter','overview'],
    ),
  ],
);
