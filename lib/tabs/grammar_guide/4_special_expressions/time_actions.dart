import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson timeActions = Lesson(
  title: 'Time-Specific Actions',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In this lesson, we will go over various ways to express actions that take place in a certain time-frame. In particular, we will learn how to say: 1) an action has just been completed, 2) an action is taken immediately after another action took place, 3) an action occurs while another action is ongoing, and 4) one continuously repeats an action.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this lesson, we will go over various ways to express actions that take place in a certain time-frame. In particular, we will learn how to say: 1) an action has just been completed, 2) an action is taken immediately after another action took place, 3) an action occurs while another action is ongoing, and 4) one continuously repeats an action.',
      grammars: [],
      keywords: ['time actions','time-specific',],
    ),
    Section(
      heading: 'Expressing what just happened with 「～ばかり」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【たべ・る】 (ru-verb) – to eat'),
                  Text('すみません – sorry (polite)'),
                  Text('今 【いま】 – now'),
                  Text('お腹 【お・なか】 – stomach'),
                  Text('いっぱい – full'),
                  Text('キロ – kilo'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('凄い 【すご・い】 (i-adj) – to a great extent'),
                  Text('疲れる 【つか・れる】 (ru-verb) – to get tired'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('昼ご飯 【ひる・ご・はん】 – lunch'),
                  Text('もう – already'),
                  Text('空く 【す・く】 (u-verb) – to become empty'),
                  Text('まさか – no way, you can’t mean to say'),
                  Text('起きる 【お・きる】 (ru-verb) – to wake; to occur'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is a very useful grammar that is used to indicate that one has just finished doing something. For instance, the first time I really wished I knew how to say something like this was when I wanted to politely decline an invitation to eat because I had just eaten. To do this, take the past tense of verb that you want to indicate as just being completed and add 「ばかり」. This is used with only the past tense of verbs and is not to be confused with the 「ばかり」 used with nouns to express amounts.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Just like the other type of 「ばかり」 we have covered before, in slang, you can hear people use 「ばっか」 instead of 「ばかり」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「ばかり」 for actions just completed',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'To indicate that an action has ended just recently, take the past tense of the verb and add 「ばかり」.'),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べた',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ばかり',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'For casual speech, you can abbreviate 「ばかり」 to just 「ばっか」.',
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べた',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ばかり',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べた',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ばっか',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption:
                                      'You can treat the result as you would with any noun.'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: taberu,
                                                    inlineSpan: const TextSpan(
                                                      text: '食べた',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'ばかり（だ）',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('Just ate'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: taberu,
                                                    inlineSpan: const TextSpan(
                                                      text: '食べた',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'ばかりじゃない',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('Didn’t just eat'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sumimasen,
                              inlineSpan: const TextSpan(
                                text: 'すみません',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'なので、'),
                            VocabTooltip(
                              message: onaka,
                              inlineSpan: const TextSpan(
                                text: 'お腹',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ippai,
                              inlineSpan: const TextSpan(
                                text: 'いっぱい',
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text('Sorry, but I’m full having just eaten.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: juukiro,
                              inlineSpan: const TextSpan(
                                text: '10キロ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: hashiru,
                              inlineSpan: TextSpan(
                                text: '走った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: sugoi,
                              inlineSpan: const TextSpan(
                                text: '凄く',
                              ),
                            ),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: const TextSpan(
                                text: '疲れた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I just ran 10 kilometers and am really tired.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                text: '帰った',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text('I got back home just now.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'Here are some examples of the abbreviated version.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hirugohan,
                              inlineSpan: const TextSpan(
                                text: '昼ご飯',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ばっか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'なのに、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: onaka,
                              inlineSpan: const TextSpan(
                                text: 'お腹',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suku,
                              inlineSpan: const TextSpan(
                                text: '空いた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Despite the fact that I just ate lunch, I’m hungry already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: masaka,
                              inlineSpan: const TextSpan(
                                text: 'まさか',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            VocabTooltip(
                              message: okiru,
                              inlineSpan: TextSpan(
                                text: '起きた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ばっか',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: 'なの？'),
                          ],
                        ),
                      ),
                      const Text('No way, did you wake up just now?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【たべ・る】 (ru-verb) – to eat すみません – sorry (polite) 今 【いま】 – now お腹 【お・なか】 – stomach いっぱい – full キロ – kilo 走る 【はし・る】 (u-verb) – to run 凄い 【すご・い】 (i-adj) – to a great extent 疲れる 【つか・れる】 (ru-verb) – to get tired 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home 昼ご飯 【ひる・ご・はん】 – lunch もう – already 空く 【す・く】 (u-verb) – to become empty まさか – no way, you can’t mean to say 起きる 【お・きる】 (ru-verb) – to wake; to occur This is a very useful grammar that is used to indicate that one has just finished doing something. For instance, the first time I really wished I knew how to say something like this was when I wanted to politely decline an invitation to eat because I had just eaten. To do this, take the past tense of verb that you want to indicate as just being completed and add 「ばかり」. This is used with only the past tense of verbs and is not to be confused with the 「ばかり」 used with nouns to express amounts. Just like the other type of 「ばかり」 we have covered before, in slang, you can hear people use 「ばっか」 instead of 「ばかり」. Using 「ばかり」 for actions just completed To indicate that an action has ended just recently, take the past tense of the verb and add 「ばかり」. Example: 食べる → 食べた → 食べたばかり For casual speech, you can abbreviate 「ばかり」 to just 「ばっか」. Example: 食べたばかり → 食べたばっか Examples すみません、今食べたばかりなので、お腹がいっぱいです。 Sorry, but I’m full having just eaten. 10キロを走ったばかりで、凄く疲れた。 I just ran 10 kilometers and am really tired. 今、家に帰ったばかりです。 I got back home just now. Here are some examples of the abbreviated version. 昼ご飯を食べたばっかなのに、もうお腹が空いた。 Despite the fact that I just ate lunch, I’m hungry already. まさか、今起きたばっかなの？ No way, did you wake up just now?',
      grammars: ['～ばかり'],
      keywords: ['bakari','just'],
    ),
    Section(
      heading: 'Express what occurred immediately after with 「とたん」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('開ける 【あ・ける】 (ru-verb) – to open'),
                  Text('取る 【と・る】 (u-verb) – to take'),
                  Text('窓 【まど】 – window'),
                  Text('猫 【ねこ】 – cat'),
                  Text('跳ぶ 【と・ぶ】 (u-verb) – to jump'),
                  Text('映画 【えい・が】 – movie'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('トイレ – bathroom; toilet'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('眠い 【ねむ・い】(i-adj) – sleepy'),
                  Text('なる (u-verb) – to become'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Kind of as a supplement to 「ばかり」, we will cover one way to say something happened as soon as something else occurs. To use this grammar, add 「とたん」 to the past tense of the first action that happened. It is also common to add the 「に」 target particle to indicate that specific point in time.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「とたん」 to describe what happened immediately after',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                  text:
                                      'Change the verb that happened first to the '),
                              TextSpan(
                                text: 'past tense',
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(text: ' and attach 「とたん」 or 「とたんに」.'),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: akeru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '開け',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: akeru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '開け',
                                        ),
                                        TextSpan(
                                          text: 'た',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: akeru,
                                    inlineSpan: const TextSpan(
                                      text: '開けた',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'とたん（に）',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: toru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '取',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: toru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '取',
                                        ),
                                        TextSpan(
                                          text: 'った',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: toru,
                                    inlineSpan: const TextSpan(
                                      text: '取った',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'とたん（に）',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const Text(
                      '※Note: You can only use this grammar for things that happen outside your control.',
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mado,
                              inlineSpan: const TextSpan(
                                text: '窓',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: akeru,
                              inlineSpan: TextSpan(
                                text: '開けた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'とたんに',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: tobu,
                              inlineSpan: const TextSpan(
                                text: '跳んで',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: 'いった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('As soon as I opened window, cat jumped out.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For many more examples, check these examples sentences from our old trusty WWWJDIC.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'An important thing to realize is that you can only use this grammar for things that occur immediately after something else and not for an action that you, yourself carry out. For instance, compare the following two sentences.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: const TextSpan(
                                text: '観た',
                              ),
                            ),
                            const TextSpan(text: 'とたんに、'),
                            VocabTooltip(
                              message: toire,
                              inlineSpan: TextSpan(
                                text: 'トイレ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行きました',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    decoration: TextDecoration.lineThrough),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(You carried out the action of going to the bathroom so this is not correct.)'),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: const TextSpan(
                                text: '観た',
                              ),
                            ),
                            const TextSpan(text: 'とたんに、'),
                            VocabTooltip(
                              message: nemui,
                              inlineSpan: TextSpan(
                                text: '眠く',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なりました',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(Since becoming sleepy is something that happened outside your control, this sentence is ok.)'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 開ける 【あ・ける】 (ru-verb) – to open 取る 【と・る】 (u-verb) – to take 窓 【まど】 – window 猫 【ねこ】 – cat 跳ぶ 【と・ぶ】 (u-verb) – to jump 映画 【えい・が】 – movie 観る 【み・る】 (ru-verb) – to watch トイレ – bathroom; toilet 行く 【い・く】 (u-verb) – to go 眠い 【ねむ・い】(i-adj) – sleepy なる (u-verb) – to become Kind of as a supplement to 「ばかり」, we will cover one way to say something happened as soon as something else occurs. To use this grammar, add 「とたん」 to the past tense of the first action that happened. It is also common to add the 「に」 target particle to indicate that specific point in time. Using 「とたん」 to describe what happened immediately after Change the verb that happened first to the past tense and attach 「とたん」 or 「とたんに」. Examples: 開ける → 開けた → 開けたとたん（に） 取る → 取った → 取ったとたん（に）※Note: You can only use this grammar for things that happen outside your control. Examples 窓を開けたとたんに、猫が跳んでいった。 As soon as I opened window, cat jumped out. For many more examples, check these examples sentences from our old trusty WWWJDIC. An important thing to realize is that you can only use this grammar for things that occur immediately after something else and not for an action that you, yourself carry out. For instance, compare the following two sentences. 映画を観たとたんに、トイレに行きました。(You carried out the action of going to the bathroom so this is not correct.) 映画を観たとたんに、眠くなりました。(Since becoming sleepy is something that happened outside your control, this sentence is ok.)',
      grammars: ['とたん（に）'],
      keywords: ['totan ni','totan','immediate','just after'],
    ),
    Section(
      heading: 'Using 「ながら」 for two concurrent actions',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('テレビ – TV, television'),
                  Text('観る 【み・る】 (ru-verb) – to watch'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('する (exception) – to do'),
                  Text('音楽 【おん・がく】 – music'),
                  Text('聴く 【き・く】 (u-verb) – to listen (e.g. to music)'),
                  Text('学校 【がっ・こう】 – school'),
                  Text('歩く 【ある・く】 (u-verb) – to walk'),
                  Text('好き 【す・き】 (na-adj) – likable'),
                  Text('相手 【あい・て】 – other party'),
                  Text('何 【なに／なん】 – what'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('自分 【じ・ぶん】 – oneself'),
                  Text('気持ち 【き・も・ち】 – feeling'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('欲しい 【ほ・しい】 (i-adj) – desirable'),
                  Text('単なる 【たん・なる】 – simply'),
                  Text('わがまま (na-adj) – selfish'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('ポップコーン – popcorn'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('映画 【えい・が】 – movie'),
                  Text('口笛 【くち・ぶえ】 – whistle'),
                  Text('手紙 【て・がみ】 – letter'),
                  Text('書く 【か・く】 (u-verb) – to write'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can use 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to express that one action is taking place in conjunction with another action. To use 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, you must change the first verb to the stem and append 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text: '」. Though probably rare, you can also attach 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to the negative of the verb to express the negative. This grammar has no tense since it is determined by the second verb.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「ながら」 for concurrent actions',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Change the first verb to the ',
                              ),
                              const TextSpan(
                                text: 'stem',
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              const TextSpan(
                                text: ' and append 「',
                              ),
                              VocabTooltip(
                                message: nagara,
                                inlineSpan: const TextSpan(
                                  text: 'ながら',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '走',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '走',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: const TextSpan(
                                      text: '走り',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nagara,
                                    inlineSpan: TextSpan(
                                      text: 'ながら',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For the negative, attach 「',
                              ),
                              VocabTooltip(
                                message: nagara,
                                inlineSpan: const TextSpan(
                                  text: 'ながら',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '走',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '走',
                                        ),
                                        TextSpan(
                                          text: 'らない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hashiru,
                                    inlineSpan: const TextSpan(
                                      text: '走らない',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nagara,
                                    inlineSpan: TextSpan(
                                      text: 'ながら',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: TextSpan(
                                text: '観',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: TextSpan(
                                text: 'ながら',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Do homework while watching TV.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ongaku,
                              inlineSpan: const TextSpan(
                                text: '音楽',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kikuListen,
                              inlineSpan: TextSpan(
                                text: '聴き',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: TextSpan(
                                text: 'ながら',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: gakkou,
                              inlineSpan: const TextSpan(
                                text: '学校',
                              ),
                            ),
                            const TextSpan(text: 'へ'),
                            VocabTooltip(
                              message: aruku,
                              inlineSpan: const TextSpan(
                                text: '歩く',
                              ),
                            ),
                            const TextSpan(text: 'のが'),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Like to walk to school while listening to music.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aite,
                              inlineSpan: const TextSpan(
                                text: '相手',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言わない',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: TextSpan(
                                text: 'ながら',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: jibun,
                              inlineSpan: const TextSpan(
                                text: '自分',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kimochi,
                              inlineSpan: const TextSpan(
                                text: '気持ち',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: const TextSpan(
                                text: 'わかって',
                              ),
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: const TextSpan(
                                text: 'ほしい',
                              ),
                            ),
                            const TextSpan(text: 'のは'),
                            VocabTooltip(
                              message: tannaru,
                              inlineSpan: const TextSpan(
                                text: '単なる',
                              ),
                            ),
                            VocabTooltip(
                              message: wagamama,
                              inlineSpan: const TextSpan(
                                text: 'わがまま',
                              ),
                            ),
                            const TextSpan(text: 'だと'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思わない',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text(
                          'Don’t you think that wanting the other person to understand one’s feelings while not saying anything is just simply selfishness?'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Notice that the sentence ends with the main verb just like it always does. This means that the main action of the sentence is the verb that ends the clause. The 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 simply describes another action that is also taking place. For example, if we switched the verbs in the first example to say, 「',
                  ),
                  VocabTooltip(
                    message: shukudai,
                    inlineSpan: const TextSpan(
                      text: '宿題',
                    ),
                  ),
                  const TextSpan(
                    text: 'を',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'し',
                    ),
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text: '、',
                  ),
                  VocabTooltip(
                    message: terebi,
                    inlineSpan: const TextSpan(
                      text: 'テレビ',
                    ),
                  ),
                  const TextSpan(
                    text: 'を',
                  ),
                  VocabTooltip(
                    message: miruWatch,
                    inlineSpan: const TextSpan(
                      text: '観る',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '。」, this changes the sentence to say, “Watch TV while doing homework.” In other words, the main action, in this case, becomes watching TV and the action of doing homework is describing an action that is taking place at the same time.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The tense is controlled by the main verb so the verb used with 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text: '」 cannot have a tense.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: poppukoon,
                              inlineSpan: const TextSpan(
                                text: 'ポップコーン',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べ',
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: const TextSpan(
                                text: 'ながら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: TextSpan(
                                text: '観る',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Watch',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                                text: ' movie while eating popcorn.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: poppukoon,
                              inlineSpan: const TextSpan(
                                text: 'ポップコーン',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べ',
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: const TextSpan(
                                text: 'ながら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: eiga,
                              inlineSpan: const TextSpan(
                                text: '映画',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miruWatch,
                              inlineSpan: TextSpan(
                                text: '観る',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Watched',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(
                                text: ' movie while eating popcorn.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kuchibue,
                              inlineSpan: const TextSpan(
                                text: '口笛',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'し',
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: const TextSpan(
                                text: 'ながら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: tegami,
                              inlineSpan: const TextSpan(
                                text: '手紙',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kaku,
                              inlineSpan: TextSpan(
                                text: '書いていた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Was writing',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: ' letter while whistling.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 走る 【はし・る】 (u-verb) – to run テレビ – TV, television 観る 【み・る】 (ru-verb) – to watch 宿題 【しゅく・だい】 – homework する (exception) – to do 音楽 【おん・がく】 – music 聴く 【き・く】 (u-verb) – to listen (e.g. to music) 学校 【がっ・こう】 – school 歩く 【ある・く】 (u-verb) – to walk 好き 【す・き】 (na-adj) – likable 相手 【あい・て】 – other party 何 【なに／なん】 – what 言う 【い・う】 (u-verb) – to say 自分 【じ・ぶん】 – oneself 気持ち 【き・も・ち】 – feeling 分かる 【わ・かる】 (u-verb) – to understand 欲しい 【ほ・しい】 (i-adj) – desirable 単なる 【たん・なる】 – simply わがまま (na-adj) – selfish 思う 【おも・う】 (u-verb) – to think ポップコーン – popcorn 食べる 【た・べる】 (ru-verb) – to eat 映画 【えい・が】 – movie 口笛 【くち・ぶえ】 – whistle 手紙 【て・がみ】 – letter 書く 【か・く】 (u-verb) – to write You can use 「ながら」 to express that one action is taking place in conjunction with another action. To use 「ながら」, you must change the first verb to the stem and append 「ながら」. Though probably rare, you can also attach 「ながら」 to the negative of the verb to express the negative. This grammar has no tense since it is determined by the second verb. Using 「ながら」 for concurrent actions Change the first verb to the stem and append 「ながら」. Example: 走る → 走り → 走りながら For the negative, attach 「ながら」. Example: 走る → 走らない → 走らないながら Examples テレビを観ながら、宿題をする。 Do homework while watching TV. 音楽を聴きながら、学校へ歩くのが好き。 Like to walk to school while listening to music. 相手に何も言わないながら、自分の気持ちをわかってほしいのは単なるわがままだと思わない？ Don’t you think that wanting the other person to understand one’s feelings while not saying anything is just simply selfishness? Notice that the sentence ends with the main verb just like it always does. This means that the main action of the sentence is the verb that ends the clause. The 「ながら」 simply describes another action that is also taking place. For example, if we switched the verbs in the first example to say, 「宿題をしながら、テレビを観る。」, this changes the sentence to say, “Watch TV while doing homework.” In other words, the main action, in this case, becomes watching TV and the action of doing homework is describing an action that is taking place at the same time. The tense is controlled by the main verb so the verb used with 「ながら」 cannot have a tense. ポップコーンを食べながら、映画を観る。 Watch movie while eating popcorn. ポップコーンを食べながら、映画を観る。 Watched movie while eating popcorn. 口笛をしながら、手紙を書いていた。 Was writing letter while whistling.',
      grammars: ['～ながら'],
      keywords: ['nagara','while','verb'],
    ),
    Section(
      heading: 'Using 「ながら」 with state-of-being',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('残念 【ざん・ねん】 (na-adj) – unfortunate'),
                  Text('貧乏 【びん・ぼう】 (na-adj) – poor'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('いっぱい – full'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('今日 【きょう】 – today'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('なる (u-verb) – to become'),
                  Text('高級 【こう・きゅう】 (na-adj) – high class, high grade'),
                  Text('バッグ – bag'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('初心者 【しょ・しん・しゃ】 – beginner'),
                  Text('実力 【じつ・りょく】 – actual ability'),
                  Text('プロ – pro'),
                  Text('同じ 【おな・じ】 – same'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'A more advanced use of 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is to use it with the implied state-of-being. In other words, you can use it with nouns or adjectives to talk about what something is while something else. The implied state-of-being means that you must not use the declarative 「だ」, you just attach 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to the noun or adjective. For example, a common way this grammar is used is to say, “While it’s unfortunate, something something…” In Japanese, this would become 「',
                  ),
                  VocabTooltip(
                    message: zannen,
                    inlineSpan: const TextSpan(
                      text: '残念',
                    ),
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: TextSpan(
                      text: 'ながら',
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                    ),
                  ),
                  const TextSpan(
                    text: '・・・」.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You can also attach the inclusive 「も」 particle to 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to get 「',
                  ),
                  VocabTooltip(
                    message: nagaramo,
                    inlineSpan: const TextSpan(
                      text: 'ながらも',
                    ),
                  ),
                  const TextSpan(
                    text: '」. This changes the meaning from “while” to “',
                  ),
                  const TextSpan(
                    text: 'even',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  const TextSpan(
                    text: ' while”.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「ながら」 or 「ながらも」 with state-of-being',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                  text:
                                      'To say [X] is something while something else, attach 「'),
                              VocabTooltip(
                                message: nagara,
                                inlineSpan: const TextSpan(
                                  text: 'ながら',
                                ),
                              ),
                              const TextSpan(
                                text: '」 to [X].',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: zannen,
                                    inlineSpan: const TextSpan(
                                      text: '残念',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: zannen,
                                    inlineSpan: const TextSpan(
                                      text: '残念',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nagara,
                                    inlineSpan: TextSpan(
                                      text: 'ながら',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'To say [X] is something even while something else, attach 「',
                              ),
                              VocabTooltip(
                                message: nagaramo,
                                inlineSpan: const TextSpan(
                                  text: 'ながらも',
                                ),
                              ),
                              const TextSpan(
                                text: '」 to [X].',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: binbou,
                                    inlineSpan: const TextSpan(
                                      text: '貧乏',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: binbou,
                                    inlineSpan: const TextSpan(
                                      text: '貧乏',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: nagaramo,
                                    inlineSpan: TextSpan(
                                      text: 'ながらも',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: ippai,
                              inlineSpan: const TextSpan(
                                text: 'いっぱい',
                              ),
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入って',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: zannen,
                              inlineSpan: const TextSpan(
                                text: '残念',
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: TextSpan(
                                text: 'ながら',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kyou,
                              inlineSpan: const TextSpan(
                                text: '今日',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行けなく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なりました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'While it’s unfortunate, a lot of work came in and it became so that I can’t go today.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: binbou,
                              inlineSpan: const TextSpan(
                                text: '貧乏',
                              ),
                            ),
                            VocabTooltip(
                              message: nagaramo,
                              inlineSpan: TextSpan(
                                text: 'ながらも',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: koukyuu,
                              inlineSpan: const TextSpan(
                                text: '高級',
                              ),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: baggu,
                              inlineSpan: const TextSpan(
                                text: 'バッグ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買っちゃった',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Even while I’m poor, I ended up buying a high quality bag.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: shoshinsha,
                              inlineSpan: const TextSpan(
                                text: '初心者',
                              ),
                            ),
                            VocabTooltip(
                              message: nagaramo,
                              inlineSpan: TextSpan(
                                text: 'ながらも',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: jitsuryoku,
                              inlineSpan: const TextSpan(
                                text: '実力',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: puro,
                              inlineSpan: const TextSpan(
                                text: 'プロ',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: onaji,
                              inlineSpan: const TextSpan(
                                text: '同じ',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Even while he is a beginner, his actual skills are the same as a pro.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 残念 【ざん・ねん】 (na-adj) – unfortunate 貧乏 【びん・ぼう】 (na-adj) – poor 仕事 【し・ごと】 – job いっぱい – full 入る 【はい・る】 (u-verb) – to enter 今日 【きょう】 – today 行く 【い・く】 (u-verb) – to go なる (u-verb) – to become 高級 【こう・きゅう】 (na-adj) – high class, high grade バッグ – bag 買う 【か・う】 (u-verb) – to buy 彼 【かれ】 – he; boyfriend 初心者 【しょ・しん・しゃ】 – beginner 実力 【じつ・りょく】 – actual ability プロ – pro 同じ 【おな・じ】 – same A more advanced use of 「ながら」 is to use it with the implied state-of-being. In other words, you can use it with nouns or adjectives to talk about what something is while something else. The implied state-of-being means that you must not use the declarative 「だ」, you just attach 「ながら」 to the noun or adjective. For example, a common way this grammar is used is to say, “While it’s unfortunate, something something…” In Japanese, this would become 「残念ながら・・・」. You can also attach the inclusive 「も」 particle to 「ながら」 to get 「ながらも」. This changes the meaning from “while” to “even while”. Using 「ながら」 or 「ながらも」 with state-of-being To say [X] is something while something else, attach 「ながら」 to [X]. Example: 残念 → 残念ながら To say [X] is something even while something else, attach 「ながらも」 to [X]. Example: 貧乏 → 貧乏ながらも Examples 仕事がいっぱい入って、残念ながら、今日は行けなくなりました。 While it’s unfortunate, a lot of work came in and it became so that I can’t go today. 貧乏ながらも、高級なバッグを買っちゃったよ。 Even while I’m poor, I ended up buying a high quality bag. 彼は、初心者ながらも、実力はプロと同じだ。 Even while he is a beginner, his actual skills are the same as a pro.',
      grammars: ['ながら','ながらも'],
      keywords: ['nagara','nagaramo','while','even though'],
    ),
    Section(
      heading: 'To repeat something with reckless abandon using 「まくる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('やる (u-verb) – to do'),
                  Text('ゲーム – game'),
                  Text('はまる (u-verb) – to get hooked'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('パソコン – computer, PC'),
                  Text('アメリカ – America'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('時 【とき】 – time'),
                  Text('コーラ – cola'),
                  Text('飲む 【の・む】 (u-verb) – to drink'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'The WWWJDIC very succinctly defines the definition of this verb as a “verb suffix to indicate reckless abandon to the activity”. Unfortunately, it doesn’t go on to tell you exactly how it’s actually used. Actually, there’s not much to explain. You take the stem of the verb and simply attach 「まくる」. However, since this is a continuing activity, it is an '),
                  TextSpan(
                    text: 'enduring state',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' unless you’re going to do it in the future. This is a very casual expression.'),
                ],
              ),
              NotesSection(
                heading: 'Using 「まくる」 for frequent actions',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(text: 'Change the first verb to the '),
                              TextSpan(
                                text: 'stem',
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(
                                text: ' and append 「まくっている」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: yaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'や',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yaru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'や',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yaru,
                                    inlineSpan: const TextSpan(
                                      text: 'やり',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まくっている',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption:
                                      'You can use all the normal conjugations you would expect with any other verb.'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: ' ',
                                      ),
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Non-Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yaru,
                                                    inlineSpan: const TextSpan(
                                                      text: 'やり',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'まくっている',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('doing all the time'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yaru,
                                                    inlineSpan: const TextSpan(
                                                      text: 'やり',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'まくっていない',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('don’t do all the time'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yaru,
                                                    inlineSpan: const TextSpan(
                                                      text: 'やり',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'まくっていた',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('did all the time'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: yaru,
                                                    inlineSpan: const TextSpan(
                                                      text: 'やり',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'まくっていなかった',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text(
                                                'didn’t do all the time'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: geemu,
                              inlineSpan: const TextSpan(
                                text: 'ゲーム',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hamaru,
                              inlineSpan: const TextSpan(
                                text: 'はまっちゃって',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            VocabTooltip(
                              message: pasokon,
                              inlineSpan: const TextSpan(
                                text: 'パソコン',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: tsukau,
                              inlineSpan: TextSpan(
                                text: '使い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まくっている',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Having gotten hooked by games, I do nothing but use the computer lately.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: amerika,
                              inlineSpan: const TextSpan(
                                text: 'アメリカ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いた',
                              ),
                            ),
                            VocabTooltip(
                              message: toki,
                              inlineSpan: const TextSpan(
                                text: '時',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: koora,
                              inlineSpan: const TextSpan(
                                text: 'コーラ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nomu,
                              inlineSpan: TextSpan(
                                text: '飲み',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まくっていた',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'When I was in the US, I drank coke like all the time.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary やる (u-verb) – to do ゲーム – game はまる (u-verb) – to get hooked 最近 【さい・きん】 – recent; lately パソコン – computer, PC アメリカ – America いる (ru-verb) – to exist (animate) 時 【とき】 – time コーラ – cola 飲む 【の・む】 (u-verb) – to drink The WWWJDIC very succinctly defines the definition of this verb as a “verb suffix to indicate reckless abandon to the activity”. Unfortunately, it doesn’t go on to tell you exactly how it’s actually used. Actually, there’s not much to explain. You take the stem of the verb and simply attach 「まくる」. However, since this is a continuing activity, it is an enduring state unless you’re going to do it in the future. This is a very casual expression. Using 「まくる」 for frequent actions Change the first verb to the stem and append 「まくっている」. Example: やる → やり → やりまくっている Examples ゲームにはまっちゃって、最近パソコンを使いまくっているよ。 Having gotten hooked by games, I do nothing but use the computer lately. アメリカにいた時はコーラを飲みまくっていた。 When I was in the US, I drank coke like all the time.',
      grammars: ['まくる'],
      keywords: [],
    ),
  ],
);
