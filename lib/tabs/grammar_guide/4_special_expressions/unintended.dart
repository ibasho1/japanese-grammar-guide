import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/dialogue.dart';
import 'package:japanese_grammar_guide/customs/dialogue_line.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

// TODO: Add example headings
Lesson unintended = Lesson(
  title: 'Unintended Actions',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  const TextSpan(
                      text:
                          'This is the first of many useful tools that will become essential in your day-to-day conversations. We will now learn how to express an action that has taken place unintentionally often with unsatisfactory results. This is primarily done by the verb 「'),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(text: '」. Let’s look at an example.'),
                ],
              ),
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('康介 【こう・すけ】 – Kousuke (first name)'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('やる (u-verb) – to do'),
                  Text(
                      'しまう (u-verb) – to do something by accident; to finish completely'),
                ],
              ),
              Dialogue(items: [
                DialogueLine(
                  speaker: Text.rich(
                    TextSpan(children: [
                      VocabTooltip(
                        message: kousuke,
                        inlineSpan: const TextSpan(
                          text: '康介',
                        ),
                      ),
                    ]),
                  ),
                  text: Text.rich(
                    TextSpan(
                      children: [
                        VocabTooltip(
                          message: shukudai,
                          inlineSpan: const TextSpan(
                            text: '宿題',
                          ),
                        ),
                        const TextSpan(
                          text: 'を',
                        ),
                        VocabTooltip(
                          message: yaru,
                          inlineSpan: const TextSpan(
                            text: 'やった',
                          ),
                        ),
                        const TextSpan(
                          text: '？',
                        ),
                      ],
                    ),
                  ),
                ),
                const DialogueLine(
                  speaker: Text('Kousuke'),
                  text: Text('Did you do homework?'),
                  english: true,
                ),
              ]),
              Dialogue(
                items: [
                  DialogueLine(
                    speaker: const Text('アリス'),
                    text: Text.rich(
                      TextSpan(
                        children: [
                          VocabTooltip(
                            message: shimau,
                            inlineSpan: TextSpan(
                              text: 'しまった',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                          ),
                          const TextSpan(
                            text: '！',
                          ),
                        ],
                      ),
                    ),
                  ),
                  const DialogueLine(
                    speaker: Text('Alice'),
                    text: Text('Oh no! (I screwed up!)'),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This is the first of many useful tools that will become essential in your day-to-day conversations. We will now learn how to express an action that has taken place unintentionally often with unsatisfactory results. This is primarily done by the verb 「しまう」. Let’s look at an example. Vocabulary 康介 【こう・すけ】 – Kousuke (first name) 宿題 【しゅく・だい】 – homework やる (u-verb) – to do しまう (u-verb) – to do something by accident; to finish completely 康介：宿題をやった？ Kousuke: Did you do homework? アリス：しまった！ Alice：Oh no! (I screwed up!)',
      grammars: ['しまう'],
      keywords: ['shimau', 'unintended','shimatta'],
    ),
    Section(
      heading: 'Using 「しまう」 with other verbs',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      'しまう (u-verb) – to do something by accident; to finish completely'),
                  Text('その – that （abbr. of それの）'),
                  Text('ケーキ – cake'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('毎日 【まい・にち】 – everyday'),
                  Text('キロ – kilo'),
                  Text('太る 【ふと・る】 (u-verb) – to become fatter'),
                  Text('ちゃんと – properly'),
                  Text('痩せる 【や・せる】 (ru-verb) – to become thin'),
                  Text('結局 【けっ・きょく】 – eventually'),
                  Text('嫌 【いや】 (na-adj) disagreeable; unpleasant'),
                  Text('こと – event, matter'),
                  Text('する (exception) – to do'),
                  Text('ごめん – sorry'),
                  Text('待つ 【ま・つ】 (u-verb) – to wait'),
                  Text('金魚 【きん・ぎょ】 – goldfish'),
                  Text('もう – already'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'When 「',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is used in this sense, it is normal to attach it to the te-form of another verb to express an action that is done or happened unintentionally. As is common with this type of grammar, the tense is decided by the tense of 「',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: keeki,
                              inlineSpan: const TextSpan(
                                text: 'ケーキ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べて',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Oops, I ate that whole cake.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mainichi,
                              inlineSpan: const TextSpan(
                                text: '毎日',
                              ),
                            ),
                            VocabTooltip(
                              message: keeki,
                              inlineSpan: const TextSpan(
                                text: 'ケーキ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nikiro,
                              inlineSpan: const TextSpan(
                                text: '２キロ',
                              ),
                            ),
                            VocabTooltip(
                              message: futoru,
                              inlineSpan: TextSpan(
                                text: '太って',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまいました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I ate cake everyday and I (unintentionally) gained two kilograms.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chanto,
                              inlineSpan: const TextSpan(
                                text: 'ちゃんと',
                              ),
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べない',
                              ),
                            ),
                            const TextSpan(text: 'と、'),
                            VocabTooltip(
                              message: yaseru,
                              inlineSpan: TextSpan(
                                text: '痩せて',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまいます',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'If you don’t eat properly, you’ll (unintentionally) lose weight you know.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kekkyoku,
                              inlineSpan: const TextSpan(
                                text: '結局',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: iya,
                              inlineSpan: const TextSpan(
                                text: '嫌',
                              ),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'させて',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'In the end, I (unintentionally) made [someone] do something distasteful.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gomen,
                              inlineSpan: const TextSpan(
                                text: 'ごめん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: matsu,
                              inlineSpan: TextSpan(
                                text: '待たせて',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまって',
                              ),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text(
                          'Sorry about (unintentionally) making you wait!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kingyo,
                              inlineSpan: const TextSpan(
                                text: '金魚',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: shinu,
                              inlineSpan: TextSpan(
                                text: '死んで',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('The goldfish died already (oops).'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary しまう (u-verb) – to do something by accident; to finish completely その – that （abbr. of それの） ケーキ – cake 全部 【ぜん・ぶ】 – everything 食べる 【た・べる】 (ru-verb) – to eat 毎日 【まい・にち】 – everyday キロ – kilo 太る 【ふと・る】 (u-verb) – to become fatter ちゃんと – properly 痩せる 【や・せる】 (ru-verb) – to become thin 結局 【けっ・きょく】 – eventually 嫌 【いや】 (na-adj) disagreeable; unpleasant こと – event, matter する (exception) – to do ごめん – sorry 待つ 【ま・つ】 (u-verb) – to wait 金魚 【きん・ぎょ】 – goldfish もう – already 死ぬ 【し・ぬ】 (u-verb) – to die When 「しまう」 is used in this sense, it is normal to attach it to the te-form of another verb to express an action that is done or happened unintentionally. As is common with this type of grammar, the tense is decided by the tense of 「しまう」. そのケーキを全部食べてしまった。 Oops, I ate that whole cake. 毎日ケーキを食べて、２キロ太ってしまいました。 I ate cake everyday and I (unintentionally) gained two kilograms. ちゃんと食べないと、痩せてしまいますよ。 If you don’t eat properly, you’ll (unintentionally) lose weight you know. 結局、嫌なことをさせてしまった。 In the end, I (unintentionally) made [someone] do something distasteful. ごめん、待たせてしまって！ Sorry about (unintentionally) making you wait! 金魚がもう死んでしまった。 The goldfish died already (oops).',
      grammars: ['～てしまう'],
      keywords: ['teshimau','accidentally','unintentionally','end up'],
    ),
    Section(
      heading: 'Using the casual version of 「～てしまう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      'しまう (u-verb) – to do something by accident; to finish completely'),
                  Text('金魚 【きん・ぎょ】 – goldfish'),
                  Text('もう – already'),
                  Text('死ぬ 【し・ぬ】 (u-verb) – to die'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('いい (i-adj) – good'),
                  Text('皆 【みんな】 – everybody'),
                  Text('どっか – somewhere （abbr. of どこか）'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('そろそろ – gradually; soon'),
                  Text('遅い 【おそ・い】 (i-adj) – late'),
                  Text('なる (u-verb) – to become'),
                  Text('また – again'),
                  Text('遅刻 【ち・こく】 – tardiness'),
                  Text('する (exception) – to do'),
                  Text('ごめん – sorry'),
                  Text('つい – just (now); unintentionally'),
                  Text('お前 【お・まえ】 – you (casual)'),
                  Text('呼ぶ 【よ・ぶ】 (u-verb) – to call'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'In casual speech, the 「～て',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is often substituted by 「～ちゃう」 while 「～で',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is substituted by 「じゃう」. Both 「～ちゃう」 and 「～じゃう」 conjugate just like regular u-verbs.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kingyo,
                              inlineSpan: const TextSpan(
                                text: '金魚',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: shinu,
                              inlineSpan: TextSpan(children: [
                                const TextSpan(text: '死んで'),
                                TextSpan(
                                  text: 'じゃった',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ]),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('The goldfish died already.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(children: [
                                const TextSpan(text: '帰っ'),
                                TextSpan(
                                  text: 'ちゃって',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ]),
                            ),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text('Is it ok if I went home already?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: dokka,
                              inlineSpan: const TextSpan(
                                text: 'どっか',
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(children: [
                                const TextSpan(text: '行っ'),
                                TextSpan(
                                  text: 'ちゃって',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ]),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('Everybody went off somewhere.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sorosoro,
                              inlineSpan: const TextSpan(
                                text: 'そろそろ',
                              ),
                            ),
                            VocabTooltip(
                              message: osoi,
                              inlineSpan: TextSpan(children: [
                                const TextSpan(text: '遅くなっ'),
                                TextSpan(
                                  text: 'ちゃう',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ]),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('It’ll gradually become late, you know.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'There is yet another very colloquial version of 「～て',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text: '」 and 「～で',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 where it is replaced by 「～ちまう」 and 「～じまう」 respectively. Unlike the cuter 「～ちゃう」 and 「～じゃう」 slang, this version conjures an image of rough and coarse middle-aged man.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mata,
                              inlineSpan: const TextSpan(
                                text: 'また',
                              ),
                            ),
                            VocabTooltip(
                              message: chikoku,
                              inlineSpan: const TextSpan(
                                text: '遅刻',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しちまった',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text('Darn, I’m late again.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gomen,
                              inlineSpan: const TextSpan(
                                text: 'ごめん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: tsui,
                              inlineSpan: const TextSpan(
                                text: 'つい',
                              ),
                            ),
                            VocabTooltip(
                              message: omae,
                              inlineSpan: const TextSpan(
                                text: 'お前',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: yobu,
                              inlineSpan: TextSpan(children: [
                                const TextSpan(text: '呼ん'),
                                TextSpan(
                                  text: 'じまった',
                                  style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  ),
                                ),
                              ]),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Sorry, I just ended up calling you unconsciously.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary しまう (u-verb) – to do something by accident; to finish completely 金魚 【きん・ぎょ】 – goldfish もう – already 死ぬ 【し・ぬ】 (u-verb) – to die 帰る 【かえ・る】 (u-verb) – to go home いい (i-adj) – good 皆 【みんな】 – everybody どっか – somewhere （abbr. of どこか） 行く 【い・く】 (u-verb) – to go そろそろ – gradually; soon 遅い 【おそ・い】 (i-adj) – late なる (u-verb) – to become また – again 遅刻 【ち・こく】 – tardiness する (exception) – to do ごめん – sorry つい – just (now); unintentionally お前 【お・まえ】 – you (casual) 呼ぶ 【よ・ぶ】 (u-verb) – to call In casual speech, the 「～てしまう」 is often substituted by 「～ちゃう」 while 「～でしまう」 is substituted by 「じゃう」. Both 「～ちゃう」 and 「～じゃう」 conjugate just like regular u-verbs. 金魚がもう死んでじゃった。 The goldfish died already. もう帰っちゃっていい？ Is it ok if I went home already? みんな、どっか行っちゃってよ。 Everybody went off somewhere. そろそろ遅くなっちゃうよ。 It’ll gradually become late, you know. There is yet another very colloquial version of 「～てしまう」 and 「～でしまう」 where it is replaced by 「～ちまう」 and 「～じまう」 respectively. Unlike the cuter 「～ちゃう」 and 「～じゃう」 slang, this version conjures an image of rough and coarse middle-aged man. また遅刻しちまったよ。 Darn, I’m late again. ごめん、ついお前を呼んじまった。 Sorry, I just ended up calling you unconsciously.',
      grammars: ['～ちゃう','～じゃう'],
      keywords: ['casual','chau','jau','accidentally','unintentionally'],
    ),
    Section(
      heading: 'Another meaning of 「しまう」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      'しまう (u-verb) – to do something by accident; to finish completely'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('やる (u-verb) – to do'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'You may have noticed that 「',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 has another definition meaning “to finish something completely”. You may want to consider this a totally separate verb from the 「',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 we have covered so far. Occasionally but not usually, 「',
                  ),
                  VocabTooltip(
                    message: shimau,
                    inlineSpan: const TextSpan(
                      text: 'しまう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 will have this meaning rather than the unintended action.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: yaru,
                              inlineSpan: const TextSpan(
                                text: 'やって',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまいなさい',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Finish your homework completely.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary しまう (u-verb) – to do something by accident; to finish completely 宿題 【しゅく・だい】 – homework やる (u-verb) – to do You may have noticed that 「しまう」 has another definition meaning “to finish something completely”. You may want to consider this a totally separate verb from the 「しまう」 we have covered so far. Occasionally but not usually, 「しまう」 will have this meaning rather than the unintended action. 宿題をやってしまいなさい。 Finish your homework completely.',
      grammars: [],
      keywords: ['shimau','completely','finish'],
    ),
  ],
);
