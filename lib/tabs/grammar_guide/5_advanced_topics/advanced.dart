import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson advanced = Lesson(
  title: 'Chapter Overview',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Whew! We’ve come a long way from learning the basic phonetic alphabet to covering almost all the grammar you’re going to need for daily conversations. But wait, we’re not finished yet! In fact, things are going to get even more challenging and interesting because, especially toward the latter part of this section, we are going to learn grammar that only '),
                  TextSpan(
                    text: 'might',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' come in handy. In my experience, the most useful things are easiest to learn as they come up again and again. However, in order to completely master a language, we also must work hard to conquer the bigger area of things that don’t come up very often and yet every native Japanese speaker instinctively understands. Believe it or not, even the more obscure grammar '),
                  TextSpan(
                    text: 'will',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(
                      text:
                          ' come up eventually leaving you wondering what it’s supposed to mean. That’s why '),
                  TextSpan(
                    text: 'I',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  TextSpan(text: ' bothered to learn them at least.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Whew! We’ve come a long way from learning the basic phonetic alphabet to covering almost all the grammar you’re going to need for daily conversations. But wait, we’re not finished yet! In fact, things are going to get even more challenging and interesting because, especially toward the latter part of this section, we are going to learn grammar that only might come in handy. In my experience, the most useful things are easiest to learn as they come up again and again. However, in order to completely master a language, we also must work hard to conquer the bigger area of things that don’t come up very often and yet every native Japanese speaker instinctively understands. Believe it or not, even the more obscure grammar will come up eventually leaving you wondering what it’s supposed to mean. That’s why I bothered to learn them at least.',
      grammars: [],
      keywords: ['advanced'],
    ),
  ],
);
