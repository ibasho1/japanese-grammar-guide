import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson covered = Lesson(
  title: 'Covered by Something',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'This is a short lesson to cover several specialized expressions that describe the state of being covered by something. Mostly, we will focus on the differences between 「だらけ」、「まみれ」 and 「ずくめ」.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'This is a short lesson to cover several specialized expressions that describe the state of being covered by something. Mostly, we will focus on the differences between 「だらけ」、「まみれ」 and 「ずくめ」.',
      grammars: [],
      keywords: ['covered'],
    ),
    Section(
      heading:
          'Using 「だらけ」 when an object is riddled everywhere with something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('間違い 【ま・ちが・い】 – mistake'),
                  Text('ゴミ – garbage'),
                  Text('埃 【ほこり】 – dust'),
                  Text('この – this （abbr. of これの）'),
                  Text('ドキュメント – document'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('役に立つ 【やく・に・たつ】 (u-verb) – to be useful'),
                  Text('携帯 【けい・たい】 – handheld (phone)'),
                  Text('～年 【～ねん】 – counter for year'),
                  Text('使う 【つか・う】 (u-verb) – to use'),
                  Text('傷 【き・ず】 – injury; scratch; scrape'),
                  Text('なる (u-verb) – to become'),
                  Text('テレビ – TV, television'),
                  Text('ちゃんと – properly'),
                  Text('拭く 【ふ・く】 (u-verb) – to wipe; to dry'),
                  Text('くれる (ru-verb) – to give'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「だらけ」 is usually used when something is riddled everywhere. It generally carries a negative connotation. As a result, you’ll often see 「だらけ」 used with expressions like 「',
                  ),
                  VocabTooltip(
                    message: machigai,
                    inlineSpan: const TextSpan(
                      text: '間違い',
                    ),
                  ),
                  const TextSpan(
                    text: 'だらけ」, 「',
                  ),
                  VocabTooltip(
                    message: gomi,
                    inlineSpan: const TextSpan(
                      text: 'ゴミ',
                    ),
                  ),
                  const TextSpan(
                    text: 'だらけ」, or 「',
                  ),
                  VocabTooltip(
                    message: hokori,
                    inlineSpan: const TextSpan(
                      text: '埃',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'だらけ」. There is no conjugation rule to cover here, all you need to do is attach 「だらけ」 to the noun that is just all over the place. You should treat the result just like you would a regular noun.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「だらけ」 to describe the state of being riddled everywhere by something',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「だらけ」 to the noun that is covering the object or place.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: machigai,
                                    inlineSpan: const TextSpan(
                                      text: '間違い',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: machigai,
                                    inlineSpan: const TextSpan(
                                      text: '間違い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だらけ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (riddled with mistakes)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hokori,
                                    inlineSpan: const TextSpan(
                                      text: '埃',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hokori,
                                    inlineSpan: const TextSpan(
                                      text: '埃',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'だらけ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (riddled with dust)',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: dokyumento,
                              inlineSpan: const TextSpan(
                                text: 'ドキュメント',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: machigai,
                              inlineSpan: TextSpan(
                                text: '間違い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'だらけ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: zenzen,
                              inlineSpan: const TextSpan(
                                text: '全然',
                              ),
                            ),
                            VocabTooltip(
                              message: yakunitatsu,
                              inlineSpan: const TextSpan(
                                text: '役に立たない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'This document is just riddled with mistakes and is not useful at all.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: keitai,
                              inlineSpan: const TextSpan(
                                text: '携帯',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: ninenkan,
                              inlineSpan: const TextSpan(
                                text: '２年間',
                              ),
                            ),
                            VocabTooltip(
                              message: tsukau,
                              inlineSpan: const TextSpan(
                                text: '使ってたら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kizu,
                              inlineSpan: TextSpan(
                                text: '傷',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'だらけ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'After using cell phone for 2 years, it became covered with scratches.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '※Notice how the 「の」 particle is used to modify since 「だらけ」 functions like a noun.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: hokori,
                              inlineSpan: TextSpan(
                                text: '埃',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'だらけ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: chanto,
                              inlineSpan: const TextSpan(
                                text: 'ちゃんと',
                              ),
                            ),
                            VocabTooltip(
                              message: fuku,
                              inlineSpan: const TextSpan(
                                text: '拭いて',
                              ),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれない',
                              ),
                            ),
                            const TextSpan(text: '？'),
                          ],
                        ),
                      ),
                      const Text(
                          'Can you properly dust this TV completely covered in dust?'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 間違い 【ま・ちが・い】 – mistake ゴミ – garbage 埃 【ほこり】 – dust この – this （abbr. of これの） ドキュメント – document 全然 【ぜん・ぜん】 – not at all (when used with negative) 役に立つ 【やく・に・たつ】 (u-verb) – to be useful 携帯 【けい・たい】 – handheld (phone)～年 【～ねん】 – counter for year 使う 【つか・う】 (u-verb) – to use 傷 【き・ず】 – injury; scratch; scrape なる (u-verb) – to become テレビ – TV, television ちゃんと – properly 拭く 【ふ・く】 (u-verb) – to wipe; to dry くれる (ru-verb) – to give 「だらけ」 is usually used when something is riddled everywhere. It generally carries a negative connotation. As a result, you’ll often see 「だらけ」 used with expressions like 「間違いだらけ」, 「ゴミだらけ」, or 「埃だらけ」. There is no conjugation rule to cover here, all you need to do is attach 「だらけ」 to the noun that is just all over the place. You should treat the result just like you would a regular noun. Using 「だらけ」 to describe the state of being riddled everywhere by something Attach 「だらけ」 to the noun that is covering the object or place. Examples: 間違い → 間違いだらけ (riddled with mistakes) 埃 → 埃だらけ (riddled with dust) Examples このドキュメントは間違いだらけで、全然役に立たない。 This document is just riddled with mistakes and is not useful at all. 携帯を２年間使ってたら、傷だらけになった。 After using cell phone for 2 years, it became covered with scratches.※Notice how the 「の」 particle is used to modify since 「だらけ」 functions like a noun. この埃だらけのテレビをちゃんと拭いてくれない？ Can you properly dust this TV completely covered in dust?',
      grammars: ['だらけ'],
      keywords: ['darake', 'riddled'],
    ),
    Section(
      heading: 'Using 「まみれ」 to describe a covering',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('間違い 【ま・ちが・い】 – mistake'),
                  Text('血【ち】 – blood'),
                  Text('油 【あぶら】 – oil'),
                  Text('ゴミ – garbage'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('なる (u-verb) – to become'),
                  Text('車 【くるま】 – car'),
                  Text('修理 【しゅう・り】 – repair'),
                  Text('頑張る 【がん・ば・る】 (u-verb) – to try one’s best'),
                  Text('たった – only, merely'),
                  Text('キロ – kilo'),
                  Text('走る 【はし・る】 (u-verb) – to run'),
                  Text('汗 【あせ】 – sweat'),
                  Text('情けない 【なさ・けない】 (i-adj) – shameful; deplorable'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「まみれ」 may seem very similar to 「だらけ」 but there are some very important subtle differences. First, it’s only used for actually physical objects so you can’t say things like 「',
                  ),
                  VocabTooltip(
                    message: machigai,
                    inlineSpan: const TextSpan(
                      text: '間違い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'まみれ」 like you can with 「だらけ」. Plus, you can only use it for things that literally cover the object. In other words, you can’t use it to mean “littered” or “riddled” like we have done with 「だらけ」. So you can use it for things like liquids and dust, but you can’t use it for things like scratches and garbage.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'The grammatical rules are the same as 「だらけ」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「まみれ」 to describe a covering by sticking',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Like 「だらけ」, you attach 「まみれ」 to the noun that is doing covering.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: chi,
                                    inlineSpan: const TextSpan(
                                      text: '血',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: chi,
                                    inlineSpan: const TextSpan(
                                      text: '血',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まみれ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (covered in blood)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: abura,
                                    inlineSpan: const TextSpan(
                                      text: '油',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: abura,
                                    inlineSpan: const TextSpan(
                                      text: '油',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まみれ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (covered in oil)',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'You can only use 「まみれ」 for physical objects that literally covers the object.',
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: machigai,
                                    inlineSpan: const TextSpan(
                                      text: '間違い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まみれ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' (not a physical object)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gomi,
                                    inlineSpan: const TextSpan(
                                      text: 'ゴミ',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まみれ',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' (doesn’t actually cover anything)',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: abura,
                              inlineSpan: TextSpan(
                                text: '油',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まみれ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なり',
                              ),
                            ),
                            VocabTooltip(
                              message: nagara,
                              inlineSpan: const TextSpan(
                                text: 'ながら',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kuruma,
                              inlineSpan: const TextSpan(
                                text: '車',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: shuuri,
                              inlineSpan: const TextSpan(
                                text: '修理',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ganbaru,
                              inlineSpan: const TextSpan(
                                text: '頑張りました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'While becoming covered in oil, he worked hard at fixing the car.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tatta,
                              inlineSpan: const TextSpan(
                                text: 'たった',
                              ),
                            ),
                            VocabTooltip(
                              message: ichikiro,
                              inlineSpan: const TextSpan(
                                text: '１キロ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: hashiru,
                              inlineSpan: const TextSpan(
                                text: '走った',
                              ),
                            ),
                            const TextSpan(text: 'だけで、'),
                            VocabTooltip(
                              message: ase,
                              inlineSpan: TextSpan(
                                text: '汗',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まみれ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            const TextSpan(text: 'のは'),
                            VocabTooltip(
                              message: nasakenai,
                              inlineSpan: const TextSpan(
                                text: '情けない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s pitiful that one gets covered in sweat from running just 1 kilometer.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 間違い 【ま・ちが・い】 – mistake 血【ち】 – blood 油 【あぶら】 – oil ゴミ – garbage 彼 【かれ】 – he; boyfriend なる (u-verb) – to become 車 【くるま】 – car 修理 【しゅう・り】 – repair 頑張る 【がん・ば・る】 (u-verb) – to try one’s best たった – only, merely キロ – kilo 走る 【はし・る】 (u-verb) – to run 汗 【あせ】 – sweat 情けない 【なさ・けない】 (i-adj) – shameful; deplorable 「まみれ」 may seem very similar to 「だらけ」 but there are some very important subtle differences. First, it’s only used for actually physical objects so you can’t say things like 「間違いまみれ」 like you can with 「だらけ」. Plus, you can only use it for things that literally cover the object. In other words, you can’t use it to mean “littered” or “riddled” like we have done with 「だらけ」. So you can use it for things like liquids and dust, but you can’t use it for things like scratches and garbage. The grammatical rules are the same as 「だらけ」. Using 「まみれ」 to describe a covering by sticking Like 「だらけ」, you attach 「まみれ」 to the noun that is doing covering. Examples: 血 → 血まみれ (covered in blood) 油 → 油まみれ (covered in oil) You can only use 「まみれ」 for physical objects that literally covers the object. Example: 間違いまみれ (not a physical object) ゴミまみれ (doesn’t actually cover anything) Examples 彼は油まみれになりながら、車の修理に頑張りました。 While becoming covered in oil, he worked hard at fixing the car. たった１キロを走っただけで、汗まみれになるのは情けない。 It’s pitiful that one gets covered in sweat from running just 1 kilometer.',
      grammars: ['まみれ'],
      keywords: ['mamire','covered'],
    ),
    Section(
      heading: 'Using 「ずくめ」 to express entirety',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('黒 【くろ】 – black'),
                  Text('白 【しろ】 – white'),
                  Text('いい (i-adj) – good'),
                  Text('こと – event, matter'),
                  Text('団体 【だん・たい】 – group'),
                  Text('去年 【きょ・ねん】 – last year'),
                  Text('ニュース – news'),
                  Text('なる (u-verb) – to become'),
                  Text('この – this （abbr. of これの）'),
                  Text('シェーク – shake'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('栄養 【えい・よう】 – nutrition'),
                  Text('たっぷり – filled with'),
                  Text('体 【からだ】 – body'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The 「大辞林」 dictionary describes exactly what 「ずくめ」 means very well.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '名詞およびそれに準ずる語句に付いて、何から何まで、そればかりであることを表す。すべて…である。\n「うそ―の言いわけ」「いいこと―」「黒―の服装」「結構―」',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In other words, 「ずくめ」 describes something that applies to the whole thing. For instance, if we were talking about the human body, the expression “is [X] from head to toe” might be close to what 「ずくめ」 means.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In actuality, 「ずくめ」 is an expression that is rarely used and usually with a color to describe people completely dressed in that color. For example, you can see what 「',
                  ),
                  VocabTooltip(
                    message: kuro,
                    inlineSpan: const TextSpan(
                      text: '黒',
                    ),
                  ),
                  const TextSpan(
                    text: 'ずくめ」 looks like via ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'Google Images',
                    ),
                    uri:
                        'http://images.google.com/images?um=1&hl=en&client=firefox-a&rls=org.mozilla%3Aen-US%3Aofficial&q=%E9%BB%92%E3%81%9A%E3%81%8F%E3%82%81&btnG=Search+Images',
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Grammatically, 「ずくめ」 works in exactly the same ways as 「だらけ」 and 「まみれ」.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「ずくめ」 to describe something that applies to the whole thing',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「ずくめ」 to the noun that applies to the whole thing.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: shiro,
                                    inlineSpan: const TextSpan(
                                      text: '白',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: shiro,
                                    inlineSpan: const TextSpan(
                                      text: '白',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ずくめ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: ii,
                                    inlineSpan: const TextSpan(
                                      text: 'いい',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: koto,
                                    inlineSpan: const TextSpan(
                                      text: 'こと',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: ii,
                                    inlineSpan: const TextSpan(
                                      text: 'いい',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: koto,
                                    inlineSpan: const TextSpan(
                                      text: 'こと',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ずくめ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shiro,
                              inlineSpan: TextSpan(
                                text: '白',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ずくめ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: dantai,
                              inlineSpan: const TextSpan(
                                text: '団体',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kyonen,
                              inlineSpan: const TextSpan(
                                text: '去年',
                              ),
                            ),
                            VocabTooltip(
                              message: nyuusu,
                              inlineSpan: const TextSpan(
                                text: 'ニュース',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なっていた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The organization dressed all in white was on the news last year.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: sheeku,
                              inlineSpan: const TextSpan(
                                text: 'シェーク',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: const TextSpan(
                                text: 'おいしい',
                              ),
                            ),
                            const TextSpan(text: 'し、'),
                            VocabTooltip(
                              message: eiyou,
                              inlineSpan: const TextSpan(
                                text: '栄養',
                              ),
                            ),
                            VocabTooltip(
                              message: tappuri,
                              inlineSpan: const TextSpan(
                                text: 'たっぷり',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: karada,
                              inlineSpan: const TextSpan(
                                text: '体',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'ですから、'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: TextSpan(
                                text: 'いい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: TextSpan(
                                text: 'こと',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ずくめ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'ですよ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'This shake is tasty and filled with nutrients, it’s good for (your) body so it’s entirely good things.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 黒 【くろ】 – black 白 【しろ】 – white いい (i-adj) – good こと – event, matter 団体 【だん・たい】 – group 去年 【きょ・ねん】 – last year ニュース – news なる (u-verb) – to become この – this （abbr. of これの） シェーク – shake おいしい (i-adj) – tasty 栄養 【えい・よう】 – nutrition たっぷり – filled with 体 【からだ】 – body The 「大辞林」 dictionary describes exactly what 「ずくめ」 means very well. 名詞およびそれに準ずる語句に付いて、何から何まで、そればかりであることを表す。すべて…である。「うそ―の言いわけ」「いいこと―」「黒―の服装」「結構―」 In other words, 「ずくめ」 describes something that applies to the whole thing. For instance, if we were talking about the human body, the expression “is [X] from head to toe” might be close to what 「ずくめ」 means. In actuality, 「ずくめ」 is an expression that is rarely used and usually with a color to describe people completely dressed in that color. For example, you can see what 「黒ずくめ」 looks like via Google Images. Grammatically, 「ずくめ」 works in exactly the same ways as 「だらけ」 and 「まみれ」. Using 「ずくめ」 to describe something that applies to the whole thing Attach 「ずくめ」 to the noun that applies to the whole thing. Examples: 白 → 白ずくめ いいこと → いいことずくめ Examples 白ずくめ団体は去年ニュースになっていた。 The organization dressed all in white was on the news last year. このシェークは、おいしいし、栄養たっぷりで体にいいですから、いいことずくめですよ。 This shake is tasty and filled with nutrients, it’s good for (your) body so it’s entirely good things.',
      grammars: ['ずくめ'],
      keywords: ['zukume','dressed'],
    ),
  ],
);
