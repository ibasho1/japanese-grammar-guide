import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson even = Lesson(
  title: 'The Minimum Expectation',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this section, we’ll learn various ways to express the minimum expectation. This grammar is not used as often as you might think as there are many situations where a simpler expression would sound more natural, but you should still become familiar with it.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this section, we’ll learn various ways to express the minimum expectation. This grammar is not used as often as you might think as there are many situations where a simpler expression would sound more natural, but you should still become familiar with it.',
      grammars: [],
      keywords: ['even'],
    ),
    Section(
      heading: 'Using 「（で）さえ」 to describe the minimum requirement',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('私 【わたし】 – me; myself; I'),
                  Text('子供 【こ・ども】 – child'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('多い 【おお・い】 (i-adj) – numerous'),
                  Text('トイレ – bathroom; toilet'),
                  Text('時間 【じ・かん】 – time'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('お金 【お・かね】 – money'),
                  Text('何 【なに／なん】 – what'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('お弁当 【お・べん・とう】 – box lunch'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('あんた – you (slang)'),
                  Text('楽ちん 【らく・ちん】 (na-adj) – easy'),
                  Text('ビタミン – vitamin'),
                  Text('健康 【けん・こう】 – health'),
                  Text('保証 【ほ・しょう】 – guarantee'),
                  Text('する (exception) – to do'),
                  Text('自分 【じ・ぶん】 – oneself'),
                  Text('過ち 【あやま・ち】 – fault, error'),
                  Text('認める 【みと・める】 (ru-verb) – to recognize, to acknowledge'),
                  Text('問題 【もん・だい】 – problem'),
                  Text('解決 【かい・けつ】 – resolution'),
                  Text('教科書 【きょう・か・しょ】 – textbook'),
                  Text('もっと – more'),
                  Text('ちゃんと – properly'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('合格 【ごう・かく】 – pass (as in an exam)'),
                  Text('一言 【ひと・こと】 – a few words'),
                  Text('くれる (ru-verb) – to give'),
                  Text('こんな – this sort of'),
                  Text('こと – event, matter'),
                  Text('なる (u-verb) – to become'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In English, we might say, “not even close” to show that not even the minimum expectation has been met. In Japanese, we can express this by attaching 「さえ」 to the object or verb that miserably failed to reach what one would consider to be a bare minimum requirement. Conversely, you can also use the same grammar in a positive sense to express something is all you need.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「（で）さえ」 to describe the minimum requirement',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For nouns:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「さえ」 or 「でさえ」 to the minimum requirement.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: watashi,
                                    inlineSpan: const TextSpan(
                                      text: '私',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' – even me',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kodomo,
                                    inlineSpan: const TextSpan(
                                      text: '子供',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'でさえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' – even children',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Change the verb to the stem and attach 「さえ」. For verbs in te-form, attach 「さえ」 to 「て／で」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'き',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行き',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言っ',
                                        ),
                                        TextSpan(
                                          text: 'て',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kureru,
                                    inlineSpan: const TextSpan(
                                      text: 'くれる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言っ',
                                        ),
                                        TextSpan(
                                          text: 'て',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: kureru,
                                    inlineSpan: const TextSpan(
                                      text: 'くれる',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読ん',
                                        ),
                                        TextSpan(
                                          text: 'で',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: iru,
                                    inlineSpan: const TextSpan(
                                      text: 'いる',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '読ん',
                                        ),
                                        TextSpan(
                                          text: 'で',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'さえ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: iru,
                                    inlineSpan: const TextSpan(
                                      text: 'いる',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: ooi,
                              inlineSpan: const TextSpan(
                                text: '多',
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: const TextSpan(
                                text: 'すぎて',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: toire,
                              inlineSpan: const TextSpan(
                                text: 'トイレ',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'There was so much homework, I didn’t even have time to go to the bathroom.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'あれば',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'でも',
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: '出来る',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The least you need is money and you can do anything.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: obentou,
                              inlineSpan: const TextSpan(
                                text: 'お弁当',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買う',
                              ),
                            ),
                            VocabTooltip(
                              message: okane,
                              inlineSpan: const TextSpan(
                                text: 'お金',
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I didn’t even have money to buy lunch.',
                      ),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text: 'For nouns ',
                  ),
                  TextSpan(
                    text: 'only',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  TextSpan(
                    text:
                        ', you can add 「で」 and use 「でさえ」 instead of just 「さえ」. There are no grammatical differences but it does sound a bit more emphatic.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'でさえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: '出来れば',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: anta,
                              inlineSpan: const TextSpan(
                                text: 'あんた',
                              ),
                            ),
                            const TextSpan(
                              text: 'には',
                            ),
                            VocabTooltip(
                              message: rakuchin,
                              inlineSpan: const TextSpan(
                                text: '楽ちん',
                              ),
                            ),
                            const TextSpan(
                              text: 'でしょう。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If even I can do it, it should be a breeze for you.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'You can also attach 「さえ」 to the stem of verbs to express a minimum action for a result. This is usually followed up immediately by 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to show that the minimum action is done (or not). If the verb happens to be in a te-form, 「さえ」 can also be attached directly to the end of the 「て」 or 「で」 of the te-form.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: bitamin,
                              inlineSpan: const TextSpan(
                                text: 'ビタミン',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: TextSpan(
                                text: '食べ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'すれば',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kenkou,
                              inlineSpan: const TextSpan(
                                text: '健康',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hoshou,
                              inlineSpan: const TextSpan(
                                text: '保証',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'されます',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you just eat vitamins, your health will be guaranteed.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jibun,
                              inlineSpan: const TextSpan(
                                text: '自分',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: ayamachi,
                              inlineSpan: const TextSpan(
                                text: '過ち',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: mitomeru,
                              inlineSpan: TextSpan(
                                text: '認め',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しなければ',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: mondai,
                              inlineSpan: const TextSpan(
                                text: '問題',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kaiketsu,
                              inlineSpan: const TextSpan(
                                text: '解決',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The problem won’t be solved if you don’t even recognize your own mistake, you know.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyoukasho,
                              inlineSpan: const TextSpan(
                                text: '教科書',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: motto,
                              inlineSpan: const TextSpan(
                                text: 'もっと',
                              ),
                            ),
                            VocabTooltip(
                              message: chanto,
                              inlineSpan: const TextSpan(
                                text: 'ちゃんと',
                              ),
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読んで',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いれば',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: goukaku,
                              inlineSpan: const TextSpan(
                                text: '合格',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できた',
                              ),
                            ),
                            const TextSpan(
                              text: 'のに。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If only I had read the textbook more properly, I could have passed.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hitokoto,
                              inlineSpan: const TextSpan(
                                text: '一言',
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'さえ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kureru,
                              inlineSpan: const TextSpan(
                                text: 'くれれば',
                              ),
                            ),
                            VocabTooltip(
                              message: konna,
                              inlineSpan: const TextSpan(
                                text: 'こんな',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'ならなかった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If you only had said something things wouldn’t have turned out like this.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 私 【わたし】 – me; myself; I 子供 【こ・ども】 – child 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go 言う 【い・う】 (u-verb) – to say 読む 【よ・む】 (u-verb) – to read 宿題 【しゅく・だい】 – homework 多い 【おお・い】 (i-adj) – numerous トイレ – bathroom; toilet 時間 【じ・かん】 – time ある (u-verb) – to exist (inanimate) お金 【お・かね】 – money 何 【なに／なん】 – what 出来る 【で・き・る】 (ru-verb) – to be able to do お弁当 【お・べん・とう】 – box lunch 買う 【か・う】 (u-verb) – to buy あんた – you (slang) 楽ちん 【らく・ちん】 (na-adj) – easy ビタミン – vitamin 健康 【けん・こう】 – health 保証 【ほ・しょう】 – guarantee する (exception) – to do 自分 【じ・ぶん】 – oneself 過ち 【あやま・ち】 – fault, error 認める 【みと・める】 (ru-verb) – to recognize, to acknowledge 問題 【もん・だい】 – problem 解決 【かい・けつ】 – resolution 教科書 【きょう・か・しょ】 – textbook もっと – more ちゃんと – properly いる (ru-verb) – to exist (animate) 合格 【ごう・かく】 – pass (as in an exam) 一言 【ひと・こと】 – a few words くれる (ru-verb) – to give こんな – this sort of こと – event, matter なる (u-verb) – to become In English, we might say, “not even close” to show that not even the minimum expectation has been met. In Japanese, we can express this by attaching 「さえ」 to the object or verb that miserably failed to reach what one would consider to be a bare minimum requirement. Conversely, you can also use the same grammar in a positive sense to express something is all you need. Using 「（で）さえ」 to describe the minimum requirement For nouns: Attach 「さえ」 or 「でさえ」 to the minimum requirement. Examples: 私さえ – even me 子供でさえ – even children For verbs: Change the verb to the stem and attach 「さえ」. For verbs in te-form, attach 「さえ」 to 「て／で」. Examples: 食べる → 食べさえ行く → 行き → 行きさえ言ってくれる → 言ってさえくれる読んでいる → 読んでさえいる Examples 宿題が多すぎて、トイレに行く時間さえなかった。 There was so much homework, I didn’t even have time to go to the bathroom. お金さえあれば、何でも出来るよ。 The least you need is money and you can do anything. お弁当を買うお金さえなかった。 I didn’t even have money to buy lunch. For nouns only, you can add 「で」 and use 「でさえ」 instead of just 「さえ」. There are no grammatical differences but it does sound a bit more emphatic. 私でさえ出来れば、あんたには楽ちんでしょう。 If even I can do it, it should be a breeze for you. You can also attach 「さえ」 to the stem of verbs to express a minimum action for a result. This is usually followed up immediately by 「する」 to show that the minimum action is done (or not). If the verb happens to be in a te-form, 「さえ」 can also be attached directly to the end of the 「て」 or 「で」 of the te-form. ビタミンを食べさえすれば、健康が保証されますよ。 If you just eat vitamins, your health will be guaranteed. 自分の過ちを認めさえしなければ、問題は解決しないよ。 The problem won’t be solved if you don’t even recognize your own mistake, you know. 教科書をもっとちゃんと読んでさえいれば、合格できたのに。 If only I had read the textbook more properly, I could have passed. 一言言ってさえくれればこんなことにならなかった。 If you only had said something things wouldn’t have turned out like this.',
      grammars: ['（で）さえ'],
      keywords: ['sae','even'],
    ),
    Section(
      heading: '「（で）すら」 – older version of 「（で）さえ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('私 【わたし】 – me; myself; I'),
                  Text('子供 【こ・ども】 – child'),
                  Text('この – this （abbr. of これの）'),
                  Text('天才 【てん・さい】 – genius'),
                  Text('分かる 【わ・かる】 (u-verb) – to understand'),
                  Text('緊張 【きん・ちょう】 – nervousness'),
                  Text('する (exception) – to do'),
                  Text('ちらっと – a peek'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('こと – event, matter'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('人 【ひと】 – person'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('知る 【し・る】 (u-verb) – to know'),
                  Text('生徒 【せい・と】 – student'),
                  Text('いる (ru-verb) – to exist (animate)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「（で）すら」 is a older variation of 「（で）さえ」 that is not as commonly used. It is essentially interchangeable with 「（で）さえ」 except that it is generally used only with nouns.',
                  ),
                ],
              ),
              NotesSection(
                heading: '「（で）すら」 is used in the same way as 「（で）さえ」 for nouns',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For nouns:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「すら」 or 「ですら」 to the minimum requirement.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: watashi,
                                    inlineSpan: const TextSpan(
                                      text: '私',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'すら',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' – even me',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kodomo,
                                    inlineSpan: const TextSpan(
                                      text: '子供',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ですら',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' – even children',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: tensai,
                              inlineSpan: const TextSpan(
                                text: '天才',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'ですら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: const TextSpan(
                                text: 'わからなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Even a genius such as myself couldn’t solve it.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: tensai,
                              inlineSpan: const TextSpan(
                                text: '天才',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            TextSpan(
                              text: 'ですら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: wakaru,
                              inlineSpan: const TextSpan(
                                text: 'わからなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Even a genius such as myself couldn’t solve it.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kinchou,
                              inlineSpan: const TextSpan(
                                text: '緊張',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'し',
                              ),
                            ),
                            VocabTooltip(
                              message: sugiru,
                              inlineSpan: const TextSpan(
                                text: 'すぎて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: chiratto,
                              inlineSpan: const TextSpan(
                                text: 'ちらっと',
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見る',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            TextSpan(
                              text: 'すら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: '出来ませんでした',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I was so nervous that I couldn’t even take a quick peek.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: '「'),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: '」の'),
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            TextSpan(
                              text: 'すら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: shiru,
                              inlineSpan: const TextSpan(
                                text: '知らない',
                              ),
                            ),
                            VocabTooltip(
                              message: seito,
                              inlineSpan: const TextSpan(
                                text: '生徒',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いない',
                              ),
                            ),
                            const TextSpan(text: 'でしょ！'),
                          ],
                        ),
                      ),
                      const Text(
                          'There are no students that don’t even know the 「人」 Kanji!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 私 【わたし】 – me; myself; I 子供 【こ・ども】 – child この – this （abbr. of これの） 天才 【てん・さい】 – genius 分かる 【わ・かる】 (u-verb) – to understand 緊張 【きん・ちょう】 – nervousness する (exception) – to do ちらっと – a peek 見る 【み・る】 (ru-verb) – to see こと – event, matter 出来る 【で・き・る】 (ru-verb) – to be able to do 人 【ひと】 – person 漢字 【かん・じ】 – Kanji 知る 【し・る】 (u-verb) – to know 生徒 【せい・と】 – student いる (ru-verb) – to exist (animate) 「（で）すら」 is a older variation of 「（で）さえ」 that is not as commonly used. It is essentially interchangeable with 「（で）さえ」 except that it is generally used only with nouns. 「（で）すら」 is used in the same way as 「（で）さえ」 for nouns For nouns: Attach 「すら」 or 「ですら」 to the minimum requirement. Examples: 私すら – even me 子供ですら – even children Examples この天才の私ですらわからなかった。 Even a genius such as myself couldn’t solve it. この天才の私ですらわからなかった。 Even a genius such as myself couldn’t solve it. 私は緊張しすぎて、ちらっと見ることすら出来ませんでした。 I was so nervous that I couldn’t even take a quick peek. 「人」の漢字すら知らない生徒は、いないでしょ！ There are no students that don’t even know the 「人」 Kanji!',
      grammars: ['（で）すら'],
      keywords: ['sura','even'],
    ),
    Section(
      heading: '「おろか」 – it’s not even worth considering',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('愚か 【おろ・か】 (na-adj) – foolish'),
                  Text('漢字 【かん・じ】 – Kanji'),
                  Text('ひらがな – Hiragana'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('結婚 【けっ・こん】 – marriage'),
                  Text('～ヶ月 【～か・げつ】 – counter for span of month(s)'),
                  Text(
                      '付き合う 【つ・き・あ・う】 (u-verb) – to go out with; to accompany'),
                  Text('結局 【けっ・きょく】 – eventually'),
                  Text('別れる 【わか・れる】 (ru-verb) – to separate; to break up'),
                  Text('大学 【だい・がく】 – college'),
                  Text('高校 【こう・こう】 – high school'),
                  Text('卒業 【そつ・ぎょう】 – graduate'),
                  Text('する (exception) – to do'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'This grammar comes from the adjective 「',
                  ),
                  VocabTooltip(
                    message: oroka,
                    inlineSpan: const TextSpan(
                      text: '愚か',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 which means to be foolish or stupid. However, in this case, you’re not making fun of something, rather by using 「おろか」, you can indicate that something is so ridiculous that it’s not even worth considering. In English, we might say something like, “Are you kidding? I can’t touch my knees much less do a full split!” In this example, the full split is so beyond the person’s abilities that it would be foolish to even consider it.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            TextSpan(
                              text: 'おろか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: hiragana,
                              inlineSpan: const TextSpan(
                                text: 'ひらがな',
                              ),
                            ),
                            const TextSpan(text: 'さえ'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読めない',
                              ),
                            ),
                            const TextSpan(text: 'よ！'),
                          ],
                        ),
                      ),
                      const Text(
                          'Forget about Kanji, I can’t even read Hiragana!'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kekkon,
                              inlineSpan: const TextSpan(
                                text: '結婚',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            TextSpan(
                              text: 'おろか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nikagetsu,
                              inlineSpan: const TextSpan(
                                text: '2ヶ月',
                              ),
                            ),
                            VocabTooltip(
                              message: tsukiau,
                              inlineSpan: const TextSpan(
                                text: '付き合って',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kekkyoku,
                              inlineSpan: const TextSpan(
                                text: '結局',
                              ),
                            ),
                            VocabTooltip(
                              message: wakareru,
                              inlineSpan: const TextSpan(
                                text: '別れて',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'We eventually broke up after going out two months much less get married.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            TextSpan(
                              text: 'おろか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: koukou,
                              inlineSpan: const TextSpan(
                                text: '高校',
                              ),
                            ),
                            const TextSpan(text: 'すら'),
                            VocabTooltip(
                              message: sotsugyou,
                              inlineSpan: const TextSpan(
                                text: '卒業',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'I didn’t even graduate from high school much less college.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This grammar is rarely used and is primarily useful for JLPT level 1. The expression 「どころか」 is far more common and has a similar meaning. However, unlike 「おろか」 which is used as an adjective, 「どころか」 is attached directly to the noun, adjective, or verb.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanji,
                              inlineSpan: const TextSpan(
                                text: '漢字',
                              ),
                            ),
                            TextSpan(
                              text: 'どころか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: hiragana,
                              inlineSpan: const TextSpan(
                                text: 'ひらがな',
                              ),
                            ),
                            const TextSpan(text: 'さえ'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: const TextSpan(
                                text: '読めない',
                              ),
                            ),
                            const TextSpan(text: 'よ！'),
                          ],
                        ),
                      ),
                      const Text(
                          'Forget about Kanji, I can’t even read Hiragana!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 愚か 【おろ・か】 (na-adj) – foolish 漢字 【かん・じ】 – Kanji ひらがな – Hiragana 読む 【よ・む】 (u-verb) – to read 結婚 【けっ・こん】 – marriage～ヶ月 【～か・げつ】 – counter for span of month(s) 付き合う 【つ・き・あ・う】 (u-verb) – to go out with; to accompany 結局 【けっ・きょく】 – eventually 別れる 【わか・れる】 (ru-verb) – to separate; to break up 大学 【だい・がく】 – college 高校 【こう・こう】 – high school 卒業 【そつ・ぎょう】 – graduate する (exception) – to do This grammar comes from the adjective 「愚か」 which means to be foolish or stupid. However, in this case, you’re not making fun of something, rather by using 「おろか」, you can indicate that something is so ridiculous that it’s not even worth considering. In English, we might say something like, “Are you kidding? I can’t touch my knees much less do a full split!” In this example, the full split is so beyond the person’s abilities that it would be foolish to even consider it. Examples 漢字はおろか、ひらがなさえ読めないよ！ Forget about Kanji, I can’t even read Hiragana! 結婚はおろか、2ヶ月付き合って、結局別れてしまった。 We eventually broke up after going out two months much less get married. 大学はおろか、高校すら卒業しなかった。 I didn’t even graduate from high school much less college. This grammar is rarely used and is primarily useful for JLPT level 1. The expression 「どころか」 is far more common and has a similar meaning. However, unlike 「おろか」 which is used as an adjective, 「どころか」 is attached directly to the noun, adjective, or verb. 漢字どころか、ひらがなさえ読めないよ！ Forget about Kanji, I can’t even read Hiragana!',
      grammars: ['おろか','どころか'],
      keywords: ['oroka','dokoroka','much less','let alone'],
    ),
  ],
);
