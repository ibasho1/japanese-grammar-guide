import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson feasibility = Lesson(
  title: 'Expressing Non-Feasibility',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We learned how to express feasibility in the section on the potential form quite a while ways back. In this section, we’ll learn some advanced and specialized ways to express certain types of feasibility or the lack thereof. Like much of the grammar in the Advanced Section, the grammar covered here is mostly used for written works and rarely used in regular speech.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We learned how to express feasibility in the section on the potential form quite a while ways back. In this section, we’ll learn some advanced and specialized ways to express certain types of feasibility or the lack thereof. Like much of the grammar in the Advanced Section, the grammar covered here is mostly used for written works and rarely used in regular speech.',
      grammars: [],
      keywords: ['feasibility'],
    ),
    Section(
      heading: 'Expressing the inability to not do using 「～ざるを得ない」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('得る 【え・る】 (ru-verb) – to obtain'),
                  Text('意図 【い・と】 – intention; aim; design'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('この – this （abbr. of これの）'),
                  Text('テレビ – TV, television'),
                  Text('これ – this'),
                  Text('以上 【い・じょう】 – greater or equal'),
                  Text('壊れる 【こわ・れる】 (ru-verb) – to break'),
                  Text('新しい 【あたら・しい】(i-adj) – new'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('ずっと – the whole time, all along'),
                  Text('我慢 【が・まん】 – tolerance; self-control'),
                  Text('状態 【じょう・たい】 – situation'),
                  Text('歯医者 【は・い・しゃ】 – dentist'),
                  Text('上司 【じょう・し】 – superior; boss'),
                  Text('話 【はなし】 – story'),
                  Text('聞く 【き・く】 (u-verb) – to ask; to listen'),
                  Text('どうしても – by any means, no matter what'),
                  Text('海外 【かい・がい】 – overseas'),
                  Text('出張 【しゅっ・ちょう】 – business trip'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This grammar is used when there’s something that just can’t be helped and must be done. It is the negative version of the grammar we previously covered for something that has to be done. It uses the negative of the verb 「',
                  ),
                  VocabTooltip(
                    message: eru,
                    inlineSpan: const TextSpan(text: '得る'),
                  ),
                  const TextSpan(
                    text:
                        '」 or “obtain”, to roughly mean that “one cannot obtain not doing of an action”. This means that you can’t not do something even if you wanted to. As a result of the use of double negatives, this grammar carries a slight suggestion that you really don’t want to do it, but you have to because it can’t be helped. Really, the negative connotation is the only difference between this grammar and the grammar we covered in this “have to” section. That, and the fact that this grammar is fancier and more advanced.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This grammar uses an archaic negative form of verbs that ends in 「～ざる」. It is really not used in modern Japanese with the exception of this grammar and some expressions such as 「',
                  ),
                  VocabTooltip(
                    message: ito,
                    inlineSpan: const TextSpan(text: '意図'),
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'せざる'),
                  ),
                  const TextSpan(
                    text:
                        '」. The rules for conjugation are the same as the negative verbs, except this grammar attaches 「ざる」 instead. To reiterate, all you have to do is conjugate the verb to the negative form and then replace the 「ない」 with 「ざる」. The two exception verbs are 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text: '」 which becomes 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'せざる'),
                  ),
                  const TextSpan(
                    text: '」 and 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(text: 'くる'),
                  ),
                  const TextSpan(
                    text: '」 which becomes 「',
                  ),
                  VocabTooltip(
                    message: kuru,
                    inlineSpan: const TextSpan(text: 'こざる'),
                  ),
                  const TextSpan(
                    text:
                        '」. Finally, all that’s left to be done is to attach 「を',
                  ),
                  VocabTooltip(
                    message: eru,
                    inlineSpan: const TextSpan(text: '得ない'),
                  ),
                  const TextSpan(
                    text:
                        '」 to the verb. It is also not uncommon to use Hiragana instead of the Kanji.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「～ざるを得ない」 for actions that must be done',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'To say that you can’t not do something replace the 「ない」 part of the negative verb with 「ざる」, then attach 「を',
                              ),
                              VocabTooltip(
                                message: eru,
                                inlineSpan: const TextSpan(
                                  text: '得ない',
                                ),
                              ),
                              const TextSpan(
                                text: '」 to the end of the verb.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'ざる',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べざる',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'を',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: eru,
                                    inlineSpan: TextSpan(
                                      text: '得ない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行か',
                                        ),
                                        TextSpan(
                                          text: 'ざる',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '行かざる',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'を',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: eru,
                                    inlineSpan: TextSpan(
                                      text: '得ない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'せざる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'せざる',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'を',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: eru,
                                    inlineSpan: TextSpan(
                                      text: 'えない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'くる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'こざる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'こざる',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'を',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: eru,
                                    inlineSpan: TextSpan(
                                      text: 'えない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            VocabTooltip(
                              message: kowareru,
                              inlineSpan: const TextSpan(
                                text: '壊れたら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: atarashii,
                              inlineSpan: const TextSpan(
                                text: '新しい',
                              ),
                            ),
                            const TextSpan(
                              text: 'のを',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: TextSpan(
                                text: '買わざる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: eru,
                              inlineSpan: TextSpan(
                                text: '得ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'な。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If this TV breaks even more, there’s no choice but to buy a new one.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: zutto,
                              inlineSpan: const TextSpan(
                                text: 'ずっと',
                              ),
                            ),
                            VocabTooltip(
                              message: gaman,
                              inlineSpan: const TextSpan(
                                text: '我慢',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: 'が、',
                            ),
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: joutai,
                              inlineSpan: const TextSpan(
                                text: '状態',
                              ),
                            ),
                            const TextSpan(
                              text: 'だと',
                            ),
                            VocabTooltip(
                              message: haisha,
                              inlineSpan: const TextSpan(
                                text: '歯医者',
                              ),
                            ),
                            VocabTooltip(
                              message: san,
                              inlineSpan: const TextSpan(
                                text: 'さん',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行かざる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: eru,
                              inlineSpan: TextSpan(
                                text: '得ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I tolerated it all this time but in this situation, I can’t not go to the dentist.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: joushi,
                              inlineSpan: const TextSpan(
                                text: '上司',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: hanashi,
                              inlineSpan: const TextSpan(
                                text: '話',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: kiku,
                              inlineSpan: const TextSpan(
                                text: '聞く',
                              ),
                            ),
                            const TextSpan(
                              text: 'と、',
                            ),
                            VocabTooltip(
                              message: doushitemo,
                              inlineSpan: const TextSpan(
                                text: 'どうしても',
                              ),
                            ),
                            VocabTooltip(
                              message: kaigai,
                              inlineSpan: const TextSpan(
                                text: '海外',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: shucchou,
                              inlineSpan: const TextSpan(
                                text: '出張',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'せざる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: eru,
                              inlineSpan: TextSpan(
                                text: '得ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: const TextSpan(
                                text: 'よう',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Hearing the story from the boss, it seems like I can’t not go on a business trip overseas no matter what.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 得る 【え・る】 (ru-verb) – to obtain 意図 【い・と】 – intention; aim; design する (exception) – to do 来る 【く・る】 (exception) – to come 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go この – this （abbr. of これの） テレビ – TV, television これ – this 以上 【い・じょう】 – greater or equal 壊れる 【こわ・れる】 (ru-verb) – to break 新しい 【あたら・しい】(i-adj) – new 買う 【か・う】 (u-verb) – to buy ずっと – the whole time, all along 我慢 【が・まん】 – tolerance; self-control 状態 【じょう・たい】 – situation 歯医者 【は・い・しゃ】 – dentist 上司 【じょう・し】 – superior; boss 話 【はなし】 – story 聞く 【き・く】 (u-verb) – to ask; to listen どうしても – by any means, no matter what 海外 【かい・がい】 – overseas 出張 【しゅっ・ちょう】 – business trip This grammar is used when there’s something that just can’t be helped and must be done. It is the negative version of the grammar we previously covered for something that has to be done. It uses the negative of the verb 「得る」 or “obtain”, to roughly mean that “one cannot obtain not doing of an action”. This means that you can’t not do something even if you wanted to. As a result of the use of double negatives, this grammar carries a slight suggestion that you really don’t want to do it, but you have to because it can’t be helped. Really, the negative connotation is the only difference between this grammar and the grammar we covered in this “have to” section. That, and the fact that this grammar is fancier and more advanced. This grammar uses an archaic negative form of verbs that ends in 「～ざる」. It is really not used in modern Japanese with the exception of this grammar and some expressions such as 「意図せざる」. The rules for conjugation are the same as the negative verbs, except this grammar attaches 「ざる」 instead. To reiterate, all you have to do is conjugate the verb to the negative form and then replace the 「ない」 with 「ざる」. The two exception verbs are 「する」 which becomes 「せざる」 and 「くる」 which becomes 「こざる」. Finally, all that’s left to be done is to attach 「を得ない」 to the verb. It is also not uncommon to use Hiragana instead of the Kanji. Using 「～ざるを得ない」 for actions that must be done To say that you can’t not do something replace the 「ない」 part of the negative verb with 「ざる」, then attach 「を得ない」 to the end of the verb. Examples: 食る → 食ない → 食べざる → 食べざるを得ない 行く → 行かない → 行かざる → 行かざるを得ない Exceptions: する → せざる → せざるをえない くる → こざる → こざるをえない Examples このテレビがこれ以上壊れたら、新しいのを買わざるを得ないな。 If this TV breaks even more, there’s no choice but to buy a new one. ずっと我慢してきたが、この状態だと歯医者さんに行かざるを得ない。 I tolerated it all this time but in this situation, I can’t not go to the dentist. 上司の話を聞くと、どうしても海外に出張をせざるを得ないようです。 Hearing the story from the boss, it seems like I can’t not go on a business trip overseas no matter what.',
      grammars: ['～ざるを得ない'],
      keywords: ['zaru o enai', 'no choice'],
    ),
    Section(
      heading:
          'Expressing the inability to stop doing something using 「やむを得ない」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('止む 【や・む】 (u-verb) – to stop'),
                  Text('仕方 【し・かた】 – way, method'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('しょうがない – it can’t be helped, nothing can be done'),
                  Text('得る 【え・る】 (ru-verb) – to obtain'),
                  Text('事由 【じ・ゆう】 – reason; cause'),
                  Text('手続 【て・つづき】 – procedure, paperwork'),
                  Text('遅れる 【おく・れる】 (ru-verb) – to be late'),
                  Text('場合 【ば・あい】 – case, situation'),
                  Text('必ず 【かなら・ず】 – without exception, without fail'),
                  Text('連絡 【れん・らく】 – contact'),
                  Text('この – this （abbr. of これの）'),
                  Text('仕事 【し・ごと】 – job'),
                  Text('厳しい 【きび・しい】 (i-adj) – strict'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('不景気 【ふ・けい・き】 – recession, depression'),
                  Text('新しい 【あたら・しい】(i-adj) – new'),
                  Text('見つかる 【み・つかる】 (u-verb) – to be found'),
                  Text('状態 【じょう・たい】 – situation'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This grammar is very similar to the one we just learned above except that it uses the verb 「',
                  ),
                  VocabTooltip(
                    message: yamu,
                    inlineSpan: const TextSpan(
                      text: '止む',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to say that one cannot obtain the stopping of something. Remember that we normally can’t just attach the 「を」 direct object particle to verbs, so this is really a set expression. Just like the previous grammar we learned, it is used to describe something that one is forced to do due to some circumstances. The difference here is that this is a complete phrase, which can be used for a general situation that doesn’t involve any specific action. In other words, you’re not actually forced to ',
                  ),
                  const TextSpan(
                    text: 'do',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' something; rather it describes a situation that cannot be helped. If you have already learned 「',
                  ),
                  VocabTooltip(
                    message: shikata,
                    inlineSpan: const TextSpan(
                      text: '仕方',
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: shouganai,
                    inlineSpan: const TextSpan(
                      text: 'しょうがない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, this grammar means pretty much the same thing. The difference lies in whether you want to say, “Looks like we’re stuck” vs “Due to circumstances beyond our control…”',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Since this is a set expression, there are really no grammar points to discuss. You only need to take the phrase and use it as you would any regular relative clause.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yamu,
                              inlineSpan: TextSpan(
                                text: 'やむ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: eru,
                              inlineSpan: TextSpan(
                                text: '得ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: jiyuu,
                              inlineSpan: const TextSpan(
                                text: '事由',
                              ),
                            ),
                            const TextSpan(
                              text: 'により',
                            ),
                            VocabTooltip(
                              message: tetsudzuki,
                              inlineSpan: const TextSpan(
                                text: '手続',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: okureru,
                              inlineSpan: const TextSpan(
                                text: '遅れた',
                              ),
                            ),
                            VocabTooltip(
                              message: baai,
                              inlineSpan: const TextSpan(
                                text: '場合',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kanarazu,
                              inlineSpan: const TextSpan(
                                text: '必ず',
                              ),
                            ),
                            const TextSpan(
                              text: 'ご',
                            ),
                            VocabTooltip(
                              message: renraku,
                              inlineSpan: const TextSpan(
                                text: '連絡',
                              ),
                            ),
                            VocabTooltip(
                              message: kudasai,
                              inlineSpan: const TextSpan(
                                text: '下さい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'If the paperwork should be late due to uncontrollable circumstance, please make sure to contact us.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kibishii,
                              inlineSpan: const TextSpan(
                                text: '厳しい',
                              ),
                            ),
                            const TextSpan(
                              text: 'かもしれませんが、',
                            ),
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: fukeiki,
                              inlineSpan: const TextSpan(
                                text: '不景気',
                              ),
                            ),
                            const TextSpan(
                              text: 'では',
                            ),
                            VocabTooltip(
                              message: atarashii,
                              inlineSpan: const TextSpan(
                                text: '新しい',
                              ),
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: mitsukaru,
                              inlineSpan: const TextSpan(
                                text: '見つからない',
                              ),
                            ),
                            const TextSpan(
                              text: 'ので',
                            ),
                            VocabTooltip(
                              message: yamu,
                              inlineSpan: TextSpan(
                                text: 'やむ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'を',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: eru,
                              inlineSpan: TextSpan(
                                text: '得ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: joutai,
                              inlineSpan: const TextSpan(
                                text: '状態',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'This job may be bad but because (I) can’t find a new job due to the recent economic downturn, it’s a situation where nothing can be done.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 止む 【や・む】 (u-verb) – to stop 仕方 【し・かた】 – way, method ある (u-verb) – to exist (inanimate) しょうがない – it can’t be helped, nothing can be done 得る 【え・る】 (ru-verb) – to obtain 事由 【じ・ゆう】 – reason; cause 手続 【て・つづき】 – procedure, paperwork 遅れる 【おく・れる】 (ru-verb) – to be late 場合 【ば・あい】 – case, situation 必ず 【かなら・ず】 – without exception, without fail 連絡 【れん・らく】 – contact この – this （abbr. of これの） 仕事 【し・ごと】 – job 厳しい 【きび・しい】 (i-adj) – strict 最近 【さい・きん】 – recent; lately 不景気 【ふ・けい・き】 – recession, depression 新しい 【あたら・しい】(i-adj) – new 見つかる 【み・つかる】 (u-verb) – to be found 状態 【じょう・たい】 – situation This grammar is very similar to the one we just learned above except that it uses the verb 「止む」 to say that one cannot obtain the stopping of something. Remember that we normally can’t just attach the 「を」 direct object particle to verbs, so this is really a set expression. Just like the previous grammar we learned, it is used to describe something that one is forced to do due to some circumstances. The difference here is that this is a complete phrase, which can be used for a general situation that doesn’t involve any specific action. In other words, you’re not actually forced to do something; rather it describes a situation that cannot be helped. If you have already learned 「仕方がない」 or 「しょうがない」, this grammar means pretty much the same thing. The difference lies in whether you want to say, “Looks like we’re stuck” vs “Due to circumstances beyond our control…” Since this is a set expression, there are really no grammar points to discuss. You only need to take the phrase and use it as you would any regular relative clause. Examples やむを得ない事由により手続が遅れた場合、必ずご連絡下さい。 If the paperwork should be late due to uncontrollable circumstance, please make sure to contact us. この仕事は厳しいかもしれませんが、最近の不景気では新しい仕事が見つからないのでやむを得ない状態です。 This job may be bad but because (I) can’t find a new job due to the recent economic downturn, it’s a situation where nothing can be done.',
      grammars: ['やむを得ない'],
      keywords: ['yamu o enai','can’t stop'],
    ),
    Section(
      heading: 'Expressing what cannot be done with 「～かねる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      'かねる (ru-verb) – to be unable to; to find difficult (unpleasant, awkward, painful) to do'),
                  Text('決める 【き・める】 (ru-verb) – to decide'),
                  Text('する (exception) – to do'),
                  Text('なる (u-verb) – to become'),
                  Text('この – this （abbr. of これの）'),
                  Text('場 【ば】 – place, spot'),
                  Text('ちょっと – a little'),
                  Text('また – again'),
                  Text('別途 【べっ・と】 – separate'),
                  Text('会議 【かい・ぎ】 – meeting'),
                  Text('設ける 【もう・ける】 (ru-verb) – to establish'),
                  Text('個人 【こ・じん】 – personal'),
                  Text('情報 【じょう・ほう】 – information'),
                  Text('漏洩 【ろう・えい】 – disclosure; leakage'),
                  Text('速やか 【すみ・やか】 (na-adj) – speedy; prompt'),
                  Text('対応 【たい・おう】 – dealing with; support'),
                  Text('願う 【ねが・う】 (u-verb) – to wish; to request'),
                  Text('致す 【いた・す】 (u-verb) – to do (humble)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The meaning and usage of 「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is covered pretty well in this ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'jeKai entry',
                    ),
                    uri:
                        'https://web.archive.org/web/20111021114608/http://www.jekai.org/entries/aa/00/np/aa00np03.htm',
                  ),
                  const TextSpan(
                    text:
                        ' with plenty of examples. While much of this is a repetition of what’s written there, 「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a ru-verb that is used as a suffix to other verbs to express a person’s inability, reluctance, or refusal to do something.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is often used in the negative as 「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to indicate that there is a possibility that the verb in question might happen. As the jeKai entry mentions, this is usually in reference to something bad, which you might express in English as, “there is a risk that…” or “there is a fear that…”',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'One important thing that the jeKai doesn’t mention is how you would go about using this grammar. It’s not difficult and you may have already guessed from the example sentences that all you need to do is just attach 「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: kaneru,
                    inlineSpan: const TextSpan(
                      text: 'かねない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to the stem of the verb.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「～かねる」 for things that cannot be done',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'To say that something cannot be done using 「',
                              ),
                              VocabTooltip(
                                message: kaneru,
                                inlineSpan: const TextSpan(
                                  text: 'かねる',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    '」, change the verb to the stem and attach 「',
                              ),
                              VocabTooltip(
                                message: kaneru,
                                inlineSpan: const TextSpan(
                                  text: 'かねる',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kimeru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '決め',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kimeru,
                                    inlineSpan: const TextSpan(
                                      text: '決め',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kaneru,
                                    inlineSpan: TextSpan(
                                      text: 'かねる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kaneru,
                                    inlineSpan: TextSpan(
                                      text: 'かねる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: '「',
                              ),
                              VocabTooltip(
                                message: kaneru,
                                inlineSpan: const TextSpan(
                                  text: 'かねる',
                                ),
                              ),
                              const TextSpan(
                                text: '」 is a ru-verb so use the negative 「',
                              ),
                              VocabTooltip(
                                message: kaneru,
                                inlineSpan: const TextSpan(
                                  text: 'かねない',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    '」 to say that something (bad) might happen.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なり',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kaneru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'かね',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なり',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'かね',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kaneru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: 'かね',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: kaneru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'かね',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: ba,
                              inlineSpan: const TextSpan(
                                text: '場',
                              ),
                            ),
                            const TextSpan(
                              text: 'では',
                            ),
                            VocabTooltip(
                              message: chotto,
                              inlineSpan: const TextSpan(
                                text: 'ちょっと',
                              ),
                            ),
                            VocabTooltip(
                              message: kimeru,
                              inlineSpan: TextSpan(
                                text: '決め',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kaneru,
                              inlineSpan: TextSpan(
                                text: 'かねます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ので、',
                            ),
                            VocabTooltip(
                              message: mata,
                              inlineSpan: const TextSpan(
                                text: 'また',
                              ),
                            ),
                            VocabTooltip(
                              message: betto,
                              inlineSpan: const TextSpan(
                                text: '別途',
                              ),
                            ),
                            VocabTooltip(
                              message: kaigi,
                              inlineSpan: const TextSpan(
                                text: '会議',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: moukeru,
                              inlineSpan: const TextSpan(
                                text: '設けましょう',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Since making a decision here is impossible, let’s set up a separate meeting again.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            const TextSpan(
                              text: 'ままでは、',
                            ),
                            VocabTooltip(
                              message: kojin,
                              inlineSpan: const TextSpan(
                                text: '個人',
                              ),
                            ),
                            VocabTooltip(
                              message: jouhou,
                              inlineSpan: const TextSpan(
                                text: '情報',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: rouei,
                              inlineSpan: const TextSpan(
                                text: '漏洩',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'し',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: kaneru,
                              inlineSpan: TextSpan(
                                text: 'かねない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'ので、',
                            ),
                            VocabTooltip(
                              message: sumiyaka,
                              inlineSpan: const TextSpan(
                                text: '速やか',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: taiou,
                              inlineSpan: const TextSpan(
                                text: '対応',
                              ),
                            ),
                            const TextSpan(
                              text: 'をお',
                            ),
                            VocabTooltip(
                              message: negau,
                              inlineSpan: const TextSpan(
                                text: '願い',
                              ),
                            ),
                            VocabTooltip(
                              message: itasu,
                              inlineSpan: const TextSpan(
                                text: '致します',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'At this rate, there is a possibility that personal information might leak so I request that this be dealt with promptly.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary かねる (ru-verb) – to be unable to; to find difficult (unpleasant, awkward, painful) to do 決める 【き・める】 (ru-verb) – to decide する (exception) – to do なる (u-verb) – to become この – this （abbr. of これの） 場 【ば】 – place, spot ちょっと – a little また – again 別途 【べっ・と】 – separate 会議 【かい・ぎ】 – meeting 設ける 【もう・ける】 (ru-verb) – to establish 個人 【こ・じん】 – personal 情報 【じょう・ほう】 – information 漏洩 【ろう・えい】 – disclosure; leakage 速やか 【すみ・やか】 (na-adj) – speedy; prompt 対応 【たい・おう】 – dealing with; support 願う 【ねが・う】 (u-verb) – to wish; to request 致す 【いた・す】 (u-verb) – to do (humble) The meaning and usage of 「かねる」 is covered pretty well in this jeKai entry with plenty of examples. While much of this is a repetition of what’s written there, 「かねる」 is a ru-verb that is used as a suffix to other verbs to express a person’s inability, reluctance, or refusal to do something. 「かねる」 is often used in the negative as 「かねない」 to indicate that there is a possibility that the verb in question might happen. As the jeKai entry mentions, this is usually in reference to something bad, which you might express in English as, “there is a risk that…” or “there is a fear that…” One important thing that the jeKai doesn’t mention is how you would go about using this grammar. It’s not difficult and you may have already guessed from the example sentences that all you need to do is just attach 「かねる」 or 「かねない」 to the stem of the verb. Using 「～かねる」 for things that cannot be done To say that something cannot be done using 「かねる」, change the verb to the stem and attach 「かねる」. Examples: 決める → 決めかねる する → しかねる「かねる」 is a ru-verb so use the negative 「かねない」 to say that something (bad) might happen. Examples: なる → なりかねる → なりかねない する → しかねる → しかねない Examples この場ではちょっと決めかねますので、また別途会議を設けましょう。 Since making a decision here is impossible, let’s set up a separate meeting again. このままでは、個人情報が漏洩しかねないので、速やかに対応をお願い致します。 At this rate, there is a possibility that personal information might leak so I request that this be dealt with promptly.',
      grammars: ['～かねる'],
      keywords: ['kaneru','kanenai','possibility','impossible'],
    ),
  ],
);
