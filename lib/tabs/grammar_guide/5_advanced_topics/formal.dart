import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson formal = Lesson(
  title: 'Formal Expressions',
  sections: [
    Section(
      heading: 'What do you mean by formal expressions?',
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'So far we have learned casual, polite, and honorific/humble types of languages. So what do I mean by formal expressions? I think we are all aware of the type of language I am talking about. We hear it in speeches, read it in reports, and see it on documentaries. While discussing good writing style is beyond the scope of this guide, we will go over some of the grammar that you will commonly find in this type of language. Which is not to say that it won’t appear in regular everyday speech. (Because it does.)'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'So far we have learned casual, polite, and honorific/humble types of languages. So what do I mean by formal expressions? I think we are all aware of the type of language I am talking about. We hear it in speeches, read it in reports, and see it on documentaries. While discussing good writing style is beyond the scope of this guide, we will go over some of the grammar that you will commonly find in this type of language. Which is not to say that it won’t appear in regular everyday speech. (Because it does.)',
      grammars: [],
      keywords: ['formal'],
    ),
    Section(
      heading: 'Using 「である」 for formal state-of-being',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('我輩 【わが・はい】 – I; we'),
                  Text('猫 【ねこ】 – cat'),
                  Text('夏目 【なつ・め】 – Natsume (last name)'),
                  Text('漱石 【そう・せき】 – Souseki (first name)'),
                  Text('お任せ 【お・まか・せ】 – leaving a decision to someone else'),
                  Text('表示 【ひょう・じ】 – display'),
                  Text('混合物 【こん・ごう・ぶつ】 – mixture, amalgam'),
                  Text('種類 【しゅ・るい】 – type, kind, category'),
                  Text('以上 【い・じょう】 – greater or equal'),
                  Text('純物質 【じゅん・ぶっ・しつ】 – pure material'),
                  Text('混じりあう 【ま・じりあう】 (u-verb) – to mix together'),
                  Text('物質 【ぶっ・しつ】 – pure material'),
                  Text('何 【なに／なん】 – what'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We have already learned how to speak with your friends in casual speech, your superiors in polite speech, and your customers in honorific / humble speech. We’ve learned 「だ」、「です」、and 「でございます」 to express a state-of-being for these different levels of politeness. There is one more type of state-of-being that is primarily used to state facts in a neutral, official sounding manner – 「である」. Just like the others, you tack 「である」 on to the adjective or noun that represents the state.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: wagahai,
                              inlineSpan: const TextSpan(
                                text: '吾輩',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            TextSpan(
                              text: 'である',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text:
                                  'I am a cat. (This is the title of a famous novel by ',
                            ),
                            VocabTooltip(
                              message: natsume,
                              inlineSpan: const TextSpan(
                                text: '夏目',
                              ),
                            ),
                            VocabTooltip(
                              message: souseki,
                              inlineSpan: const TextSpan(
                                text: '漱石',
                              ),
                            ),
                            const TextSpan(
                              text: ')',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Since I’m too lazy to look up facts, let’s trot on over to the Japanese version of ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'Wikipedia',
                    ),
                    uri: 'https://ja.wikipedia.org/',
                  ),
                  const TextSpan(
                    text: ' and look at some random articles by clicking on 「',
                  ),
                  VocabTooltip(
                    message: omakase,
                    inlineSpan: const TextSpan(
                      text: 'おまかせ',
                    ),
                  ),
                  VocabTooltip(
                    message: hyouji,
                    inlineSpan: const TextSpan(
                      text: '表示',
                    ),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: '(こんごうぶつ, mixture)とは、2',
                            ),
                            VocabTooltip(
                              message: shurui,
                              inlineSpan: const TextSpan(
                                text: '種類',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: junbusshitsu,
                              inlineSpan: const TextSpan(
                                text: '純物質',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: majiriau,
                              inlineSpan: const TextSpan(
                                text: '混じりあっている',
                              ),
                            ),
                            VocabTooltip(
                              message: busshitsu,
                              inlineSpan: const TextSpan(
                                text: '物質',
                              ),
                            ),
                            TextSpan(
                              text: 'である',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。(',
                            ),
                            ExternalLink(
                              inlineSpan: const TextSpan(
                                text: 'Wikipedia – 混合物',
                              ),
                              uri:
                                  'http://ja.wikipedia.org/wiki/%E6%B7%B7%E5%90%88%E7%89%A9',
                            ),
                            const TextSpan(
                              text: ', July 2004)',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                          'An amalgam is a mixture of two or more pure materials.'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'To give you an idea of how changing the 「である」 changes the tone, I’ve included some fake content around that sentence.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: TextSpan(
                                text: '何',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: shurui,
                              inlineSpan: const TextSpan(
                                text: '種類',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: junbusshitsu,
                              inlineSpan: const TextSpan(
                                text: '純物質',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: majiriau,
                              inlineSpan: const TextSpan(
                                text: '混じりあっている',
                              ),
                            ),
                            VocabTooltip(
                              message: busshitsu,
                              inlineSpan: const TextSpan(
                                text: '物質',
                              ),
                            ),
                            TextSpan(
                              text: 'だ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: TextSpan(
                                text: '何',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ですか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: shurui,
                              inlineSpan: const TextSpan(
                                text: '種類',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: junbusshitsu,
                              inlineSpan: const TextSpan(
                                text: '純物質',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: majiriau,
                              inlineSpan: const TextSpan(
                                text: '混じりあっている',
                              ),
                            ),
                            VocabTooltip(
                              message: busshitsu,
                              inlineSpan: const TextSpan(
                                text: '物質',
                              ),
                            ),
                            TextSpan(
                              text: 'です',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: nan,
                              inlineSpan: TextSpan(
                                text: '何',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'でしょうか',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: shurui,
                              inlineSpan: const TextSpan(
                                text: '種類',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: junbusshitsu,
                              inlineSpan: const TextSpan(
                                text: '純物質',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: majiriau,
                              inlineSpan: const TextSpan(
                                text: '混じりあっている',
                              ),
                            ),
                            VocabTooltip(
                              message: busshitsu,
                              inlineSpan: const TextSpan(
                                text: '物質',
                              ),
                            ),
                            TextSpan(
                              text: 'でございます',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            TextSpan(
                              text: 'とは',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '？',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongoubutsu,
                              inlineSpan: const TextSpan(
                                text: '混合物',
                              ),
                            ),
                            const TextSpan(
                              text: 'は、',
                            ),
                            VocabTooltip(
                              message: shurui,
                              inlineSpan: const TextSpan(
                                text: '種類',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: junbusshitsu,
                              inlineSpan: const TextSpan(
                                text: '純物質',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: majiriau,
                              inlineSpan: const TextSpan(
                                text: '混じりあっている',
                              ),
                            ),
                            VocabTooltip(
                              message: busshitsu,
                              inlineSpan: const TextSpan(
                                text: '物質',
                              ),
                            ),
                            TextSpan(
                              text: 'である ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 我輩 【わが・はい】 – I; we 猫 【ねこ】 – cat 夏目 【なつ・め】 – Natsume (last name) 漱石 【そう・せき】 – Souseki (first name) お任せ 【お・まか・せ】 – leaving a decision to someone else 表示 【ひょう・じ】 – display 混合物 【こん・ごう・ぶつ】 – mixture, amalgam 種類 【しゅ・るい】 – type, kind, category 以上 【い・じょう】 – greater or equal 純物質 【じゅん・ぶっ・しつ】 – pure material 混じりあう 【ま・じりあう】 (u-verb) – to mix together 物質 【ぶっ・しつ】 – pure material 何 【なに／なん】 – what We have already learned how to speak with your friends in casual speech, your superiors in polite speech, and your customers in honorific / humble speech. We’ve learned 「だ」、「です」、and 「でございます」 to express a state-of-being for these different levels of politeness. There is one more type of state-of-being that is primarily used to state facts in a neutral, official sounding manner – 「である」. Just like the others, you tack 「である」 on to the adjective or noun that represents the state. Examples 吾輩は猫である。 I am a cat. (This is the title of a famous novel by 夏目漱石) Since I’m too lazy to look up facts, let’s trot on over to the Japanese version of Wikipedia and look at some random articles by clicking on 「おまかせ表示」. 混合物(こんごうぶつ, mixture)とは、2種類以上の純物質が混じりあっている物質である。(Wikipedia – 混合物, July 2004) An amalgam is a mixture of two or more pure materials. To give you an idea of how changing the 「である」 changes the tone, I’ve included some fake content around that sentence. 混合物は何？混合物は、種類以上の純物質が混じりあっている物質だ。混合物は何ですか？混合物は、種類以上の純物質が混じりあっている物質です。混合物は何でしょうか？混合物は、種類以上の純物質が混じりあっている物質でございます。混合物とは？混合物は、種類以上の純物質が混じりあっている物質である 。',
      grammars: ['である'],
      keywords: ['dearu','state-of-being','formal'],
    ),
    Section(
      heading: 'Negative of 「である」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('それ – that'),
                  Text('不公平 【ふ・こう・へい】 – unfair'),
                  Text('言語 【げん・ご】 – language'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('マスター – master'),
                  Text('する (exception) – to do'),
                  Text('こと – event, matter'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('学生 【がく・せい】 – student'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Because the negative of 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ある'),
                  ),
                  const TextSpan(
                    text: '」 is 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(text: 'ない'),
                  ),
                  const TextSpan(
                    text:
                        '」, you might expect the opposite of 「である」 to be 「でない」. However, for some reason I’m not aware of, you need to insert the topic particle before 「ない」 to get 「ではない」.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: fukouhei,
                              inlineSpan: const TextSpan(
                                text: '不公平',
                              ),
                            ),
                            TextSpan(
                              text: 'ではない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'でしょうか。'),
                          ],
                        ),
                      ),
                      const Text('Wouldn’t you consider that to be unfair?'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gengo,
                              inlineSpan: const TextSpan(
                                text: '言語',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: const TextSpan(
                                text: '簡単',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: masutaa,
                              inlineSpan: const TextSpan(
                                text: 'マスター',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できる',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            TextSpan(
                              text: 'ではない',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Language is not something that can be mastered easily.'),
                    ],
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「である」 to sound official',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「である」 to the verb or adjective that the state-of-being applies to.',
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'である',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'For the negative, attach 「ではない」 to the verb or adjective that the state-of-being applies to.',
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: gakusei,
                                    inlineSpan: const TextSpan(
                                      text: '学生',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ではない',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'For the past tense state-of-being, apply the regular past tenses of 「',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              const TextSpan(
                                text: '」.',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: kDefaultParagraphPadding,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const TableCaption(
                              caption: 'Complete conjugation chart for 「である」'),
                          Table(
                            border: TableBorder.all(
                              color: Theme.of(context).colorScheme.outline,
                            ),
                            children: [
                              const TableRow(
                                children: [
                                  TableHeading(
                                    text: ' ',
                                  ),
                                  TableHeading(
                                    text: 'Positive',
                                  ),
                                  TableHeading(
                                    text: '	Negative',
                                  )
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Non-Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: gakusei,
                                                inlineSpan: const TextSpan(
                                                  text: '学生',
                                                ),
                                              ),
                                              TextSpan(
                                                text: 'である',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('is student'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: gakusei,
                                                inlineSpan: const TextSpan(
                                                  text: '学生',
                                                ),
                                              ),
                                              TextSpan(
                                                text: 'ではない',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('is not student'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: gakusei,
                                                inlineSpan: const TextSpan(
                                                  text: '学生',
                                                ),
                                              ),
                                              TextSpan(
                                                text: 'であった',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('was student'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: gakusei,
                                                inlineSpan: const TextSpan(
                                                  text: '学生',
                                                ),
                                              ),
                                              TextSpan(
                                                text: 'ではなかった',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .primary),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('was not student'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ある (u-verb) – to exist (inanimate) それ – that 不公平 【ふ・こう・へい】 – unfair 言語 【げん・ご】 – language 簡単 【かん・たん】 (na-adj) – simple マスター – master する (exception) – to do こと – event, matter 出来る 【で・き・る】 (ru-verb) – to be able to do 学生 【がく・せい】 – student Because the negative of 「ある」 is 「ない」, you might expect the opposite of 「である」 to be 「でない」. However, for some reason I’m not aware of, you need to insert the topic particle before 「ない」 to get 「ではない」. Examples それは不公平ではないでしょうか。 Wouldn’t you consider that to be unfair? 言語は簡単にマスターできることではない。 Language is not something that can be mastered easily. Using 「である」 to sound official Attach 「である」 to the verb or adjective that the state-of-being applies to. Example: 学生 → 学生である For the negative, attach 「ではない」 to the verb or adjective that the state-of-being applies to. Example: 学生 → 学生ではない For the past tense state-of-being, apply the regular past tenses of 「ある」.',
      grammars: ['ではない'],
      keywords: ['dewanai','dehanai','formal'],
    ),
    Section(
      heading: 'Sequential relative clauses in formal language',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('花火 【はな・び】 – fireworks'),
                  Text('火薬 【か・やく】 – gunpowder'),
                  Text('金属 【きん・ぞく】 – metal'),
                  Text('粉末 【ふん・まつ】 – fine powder'),
                  Text('混ぜる 【ま・ぜる】 (ru-verb) – to mix'),
                  Text('物 【もの】 – object'),
                  Text('火 【ひ】 – flame, light'),
                  Text('付ける 【つ・ける】 (ru-verb) – to attach'),
                  Text('燃焼時 【ねん・しょう・じ】 – at time of combustion'),
                  Text('火花 【ひ・ばな】 – spark'),
                  Text('楽しむ 【たの・しむ】 (u-verb) – to enjoy'),
                  Text('ため – for the sake/benefit of'),
                  Text('企業内 【き・ぎょう・ない】 – company-internal'),
                  Text('顧客 【こ・きゃく】 – customer, client'),
                  Text('データ – data'),
                  Text('利用 【り・よう】 – usage'),
                  Text('する (exception) – to do'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('行方 【ゆく・え】 – whereabouts'),
                  Text('調べる 【しら・べる】 (ru-verb) – to investigate'),
                  Text('こと – event, matter'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('封筒 【ふう・とう】 – envelope'),
                  Text('写真 【しゃ・しん】 – photograph'),
                  Text('数枚 【すう・まい】 – several sheets (flat objects)'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('手紙 【て・がみ】 – letter'),
                  Text(
                      '添える　【そ・える】 (ru-verb) – to garnish; to accompany (as a card does a gift)'),
                  Text('この – this （abbr. of これの）'),
                  Text('ファイル – file'),
                  Text('パスワード – password'),
                  Text('設定 【せっ・てい】 – setting'),
                  Text('開く 【ひら・く】 (u-verb) – to open'),
                  Text('～際 【～さい】 – on the occasion of'),
                  Text('それ – that'),
                  Text('入力 【にゅう・りょく】 – input'),
                  Text('必要 【ひつ・よう】 – necessity'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'In the Compound Sentence lesson, we learned how to use the te-form of verbs to express multiples sequential actions in one sentence. This practice, however, is used only in regular everyday speech. Formal speeches, narration, and written publications employ the verb stem instead of the te-form to describe sequential actions. Particularly, newspaper articles, in the interest of brevity, always prefer verb stems to the te-form.',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hanabi,
                              inlineSpan: const TextSpan(
                                text: '花火',
                              ),
                            ),
                            const TextSpan(text: '（はなび）は、'),
                            VocabTooltip(
                              message: kayaku,
                              inlineSpan: const TextSpan(
                                text: '火薬',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kinzoku,
                              inlineSpan: const TextSpan(
                                text: '金属',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: funmatsu,
                              inlineSpan: const TextSpan(
                                text: '粉末',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: mazeru,
                              inlineSpan: const TextSpan(
                                text: '混ぜた',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: 'もの',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hi,
                              inlineSpan: const TextSpan(
                                text: '火',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: tsukeru,
                              inlineSpan: TextSpan(
                                text: '付け',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nenshouji,
                              inlineSpan: const TextSpan(
                                text: '燃焼時',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hibana,
                              inlineSpan: const TextSpan(
                                text: '火花',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: tanoshimu,
                              inlineSpan: const TextSpan(
                                text: '楽しむ',
                              ),
                            ),
                            VocabTooltip(
                              message: tame,
                              inlineSpan: const TextSpan(
                                text: 'ため',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: 'もの',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            ExternalLink(
                              inlineSpan: const TextSpan(
                                text: 'Wikipedia – 花火',
                              ),
                              uri:
                                  'https://ja.wikipedia.org/wiki/%E8%8A%B1%E7%81%AB',
                            ),
                            const TextSpan(
                              text:
                                  '（, August 2004）Fireworks are for the enjoyment of sparks created from combustion created by lighting up a mixture of gunpowder and metal powder.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kigyounai,
                              inlineSpan: const TextSpan(
                                text: '企業内',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kokyaku,
                              inlineSpan: const TextSpan(
                                text: '顧客',
                              ),
                            ),
                            VocabTooltip(
                              message: deeta,
                              inlineSpan: const TextSpan(
                                text: 'データ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: riyou,
                              inlineSpan: const TextSpan(
                                text: '利用',
                              ),
                            ),
                            TextSpan(
                              text: 'し',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: yukue,
                              inlineSpan: const TextSpan(
                                text: '行方',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: shiraberu,
                              inlineSpan: const TextSpan(
                                text: '調べる',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: '出来た',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Was able to investigate his whereabouts using the company’s internal customer data.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For the 「～ている」 forms, the stem becomes 「～てい」 but because that doesn’t fit very well into the middle of a sentence, it is common to use the humble form of 「',
                  ),
                  VocabTooltip(
                    message: iru,
                    inlineSpan: const TextSpan(
                      text: 'いる',
                    ),
                  ),
                  const TextSpan(
                    text: '」 which you will remember is 「',
                  ),
                  VocabTooltip(
                    message: oru,
                    inlineSpan: const TextSpan(
                      text: 'おる',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. This is simply so you can employ 「おり」 to connect relative clauses instead of just 「い」. It has nothing to do with the humble aspect of 「',
                  ),
                  VocabTooltip(
                    message: oru,
                    inlineSpan: const TextSpan(
                      text: 'おる',
                    ),
                  ),
                  const TextSpan(
                    text: '」. ',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: fuutou,
                              inlineSpan: const TextSpan(
                                text: '封筒',
                              ),
                            ),
                            const TextSpan(text: 'には'),
                            VocabTooltip(
                              message: shashin,
                              inlineSpan: const TextSpan(
                                text: '写真',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suumai,
                              inlineSpan: const TextSpan(
                                text: '数枚',
                              ),
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入って',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'おり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: tegami,
                              inlineSpan: const TextSpan(
                                text: '手紙',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: soeru,
                              inlineSpan: const TextSpan(
                                text: '添えられていた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Several photos were inside the envelope, and a letter was attached.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: fairu,
                              inlineSpan: const TextSpan(
                                text: 'ファイル',
                              ),
                            ),
                            const TextSpan(text: 'には'),
                            VocabTooltip(
                              message: pasuwaado,
                              inlineSpan: const TextSpan(
                                text: 'パスワード',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: settei,
                              inlineSpan: const TextSpan(
                                text: '設定',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'されて',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'おり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: hiraku,
                              inlineSpan: const TextSpan(
                                text: '開く',
                              ),
                            ),
                            VocabTooltip(
                              message: saiOccasion,
                              inlineSpan: const TextSpan(
                                text: '際',
                              ),
                            ),
                            const TextSpan(text: 'には'),
                            VocabTooltip(
                              message: sore,
                              inlineSpan: const TextSpan(
                                text: 'それ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nyuuryoku,
                              inlineSpan: const TextSpan(
                                text: '入力',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            VocabTooltip(
                              message: hitsuyou,
                              inlineSpan: const TextSpan(
                                text: '必要',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ある',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'A password has been set on this file, and it needs to entered when opening.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 花火 【はな・び】 – fireworks 火薬 【か・やく】 – gunpowder 金属 【きん・ぞく】 – metal 粉末 【ふん・まつ】 – fine powder 混ぜる 【ま・ぜる】 (ru-verb) – to mix 物 【もの】 – object 火 【ひ】 – flame, light 付ける 【つ・ける】 (ru-verb) – to attach 燃焼時 【ねん・しょう・じ】 – at time of combustion 火花 【ひ・ばな】 – spark 楽しむ 【たの・しむ】 (u-verb) – to enjoy ため – for the sake/benefit of 企業内 【き・ぎょう・ない】 – company-internal 顧客 【こ・きゃく】 – customer, client データ – data 利用 【り・よう】 – usage する (exception) – to do 彼 【かれ】 – he; boyfriend 行方 【ゆく・え】 – whereabouts 調べる 【しら・べる】 (ru-verb) – to investigate こと – event, matter 出来る 【で・き・る】 (ru-verb) – to be able to do 封筒 【ふう・とう】 – envelope 写真 【しゃ・しん】 – photograph 数枚 【すう・まい】 – several sheets (flat objects) 入る 【はい・る】 (u-verb) – to enter 手紙 【て・がみ】 – letter 添える　【そ・える】 (ru-verb) – to garnish; to accompany (as a card does a gift) この – this （abbr. of これの） ファイル – file パスワード – password 設定 【せっ・てい】 – setting 開く 【ひら・く】 (u-verb) – to open～際 【～さい】 – on the occasion of それ – that 入力 【にゅう・りょく】 – input 必要 【ひつ・よう】 – necessity ある (u-verb) – to exist (inanimate) In the Compound Sentence lesson, we learned how to use the te-form of verbs to express multiples sequential actions in one sentence. This practice, however, is used only in regular everyday speech. Formal speeches, narration, and written publications employ the verb stem instead of the te-form to describe sequential actions. Particularly, newspaper articles, in the interest of brevity, always prefer verb stems to the te-form. Examples 花火（はなび）は、火薬と金属の粉末を混ぜたものに火を付け、燃焼時の火花を楽しむためのもの。 Wikipedia – 花火（, August 2004） Fireworks are for the enjoyment of sparks created from combustion created by lighting up a mixture of gunpowder and metal powder. 企業内の顧客データを利用し彼の行方を調べることが出来た。 Was able to investigate his whereabouts using the company’s internal customer data. For the 「～ている」 forms, the stem becomes 「～てい」 but because that doesn’t fit very well into the middle of a sentence, it is common to use the humble form of 「いる」 which you will remember is 「おる」. This is simply so you can employ 「おり」 to connect relative clauses instead of just 「い」. It has nothing to do with the humble aspect of 「おる」. 封筒には写真が数枚入っており、手紙が添えられていた。 Several photos were inside the envelope, and a letter was attached. このファイルにはパスワードが設定されており、開く際にはそれを入力する必要がある。 A password has been set on this file, and it needs to entered when opening. ',
      grammars: ['～ており'],
      keywords: ['formal','te-ori','te','compound sentence'],
    ),
  ],
);
