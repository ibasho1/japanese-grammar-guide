import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson immediate = Lesson(
  title: 'Immediate Events',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this section, we will be covering some advanced grammar that describe an action that takes place right after something else has occurred. I suggest you look over this section if you are really serious about completely mastering Japanese, or if you plan to take the level 1 JLPT exam, or if you enjoy reading a lot of Japanese literature.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this section, we will be covering some advanced grammar that describe an action that takes place right after something else has occurred. I suggest you look over this section if you are really serious about completely mastering Japanese, or if you plan to take the level 1 JLPT exam, or if you enjoy reading a lot of Japanese literature.',
      grammars: [],
      keywords: ['immediate'],
    ),
    Section(
      heading: 'Using 「が早いか」 to describe the instant something occurred',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('教授 【きょう・じゅ】 – professor'),
                  Text('姿 【すがた】 – figure'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('教室 【きょう・しつ】 – classroom'),
                  Text('逃げ出す 【に・げ・だ・す】 (u-verb) – to run away'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('口 【くち】 – mouth'),
                  Text('中 【なか】 – inside'),
                  Text('放り込む 【ほう・り・こ・む】 (u-verb) – to throw into'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'The phrase 「が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is used to describe something that happened the instant something else occurred.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'While very similar to the 「とたんに」 grammar, it has a strong emphasis on how soon one thing occurred after another as if it’s almost simultaneous. This grammar is rarely used outside of Japanese language tests.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'To use this grammar, you attach 「が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'か」 to the first verb, then you describe the event that happened the next instant. While it’s conventional to use the non-past tense (dictionary form) for the first verb, you can also use the past tense. For example, you can say either 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: TextSpan(
                      text: '言う',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text: 'か」 or 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: TextSpan(
                      text: '言った',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'か」. The curious thing about this grammar is that the 「が」 particle comes right after the verb. Remember, you can do this ',
                  ),
                  const TextSpan(
                    text: 'only',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text: ' with this specific grammatical phrase.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「が早いか」 to describe what happened the instant something occurred',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Attach 「が',
                              ),
                              VocabTooltip(
                                message: hayai,
                                inlineSpan: const TextSpan(
                                  text: '早い',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    'か」 to the non-past or past tense of the verb that just occurred.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言う',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言う',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      text: '早い',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'か',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言',
                                        ),
                                        TextSpan(
                                          text: 'う',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言',
                                        ),
                                        TextSpan(
                                          text: 'った',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言った',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      text: '早い',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'か',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const Text(
                      'You can only use this grammar only for events that are directly related.',
                    ),
                    const Text(
                      'You can only use this grammar only for events that actually happened (past tense).',
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: kyouju,
                              inlineSpan: const TextSpan(
                                text: '教授',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: sugata,
                              inlineSpan: const TextSpan(
                                text: '姿',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見る',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: TextSpan(
                                text: '早い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kyoushitsu,
                              inlineSpan: const TextSpan(
                                text: '教室',
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: nigeru,
                              inlineSpan: const TextSpan(
                                text: '逃げ',
                              ),
                            ),
                            VocabTooltip(
                              message: dasu,
                              inlineSpan: const TextSpan(
                                text: '出した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The instant (she) saw the professor’s figure, (she) ran away from the classroom.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: '「'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: 'みよう',
                              ),
                            ),
                            const TextSpan(text: '」と'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言う',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: TextSpan(
                                text: '早い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kuchi,
                              inlineSpan: const TextSpan(
                                text: '口',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: naka,
                              inlineSpan: const TextSpan(
                                text: '中',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hourikomu,
                              inlineSpan: const TextSpan(
                                text: '放り込んだ',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The instant (he) said “let’s try eating it”, he threw (it) into his mouth.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(text: '「'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べて',
                              ),
                            ),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: 'みよう',
                              ),
                            ),
                            const TextSpan(text: '」と'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言った',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: TextSpan(
                                text: '早い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'か',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kuchi,
                              inlineSpan: const TextSpan(
                                text: '口',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: naka,
                              inlineSpan: const TextSpan(
                                text: '中',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hourikomu,
                              inlineSpan: const TextSpan(
                                text: '放り込んだ',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The instant (he) said “let’s try eating it”, he threw (it) into his mouth.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 早い 【はや・い】 (i-adj) – fast; early 言う 【い・う】 (u-verb) – to say 彼女 【かの・じょ】 – she; girlfriend 教授 【きょう・じゅ】 – professor 姿 【すがた】 – figure 見る 【み・る】 (ru-verb) – to see 教室 【きょう・しつ】 – classroom 逃げ出す 【に・げ・だ・す】 (u-verb) – to run away 食べる 【た・べる】 (ru-verb) – to eat 口 【くち】 – mouth 中 【なか】 – inside 放り込む 【ほう・り・こ・む】 (u-verb) – to throw into The phrase 「が早い」 is used to describe something that happened the instant something else occurred. While very similar to the 「とたんに」 grammar, it has a strong emphasis on how soon one thing occurred after another as if it’s almost simultaneous. This grammar is rarely used outside of Japanese language tests. To use this grammar, you attach 「が早いか」 to the first verb, then you describe the event that happened the next instant. While it’s conventional to use the non-past tense (dictionary form) for the first verb, you can also use the past tense. For example, you can say either 「言うが早いか」 or 「言ったが早いか」. The curious thing about this grammar is that the 「が」 particle comes right after the verb. Remember, you can do this only with this specific grammatical phrase. Using 「が早いか」 to describe what happened the instant something occurred Attach 「が早いか」 to the non-past or past tense of the verb that just occurred. Examples: 言う → 言うが早いか 言う → 言った → 言ったが早いか You can only use this grammar only for events that are directly related. You can only use this grammar only for events that actually happened (past tense). Examples 彼女は、教授の姿を見るが早いか、教室から逃げ出した。 The instant (she) saw the professor’s figure, (she) ran away from the classroom. 「食べてみよう」と言うが早いか、口の中に放り込んだ。 The instant (he) said “let’s try eating it”, he threw (it) into his mouth. 「食べてみよう」と言ったが早いか、口の中に放り込んだ。 The instant (he) said “let’s try eating it”, he threw (it) into his mouth.',
      grammars: ['が早いか'],
      keywords: ['gahayaika','instant'],
    ),
    Section(
      heading: 'Using 「や／や否や」 to describe what happened right after',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('否定 【ひ・てい】 – denial'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('顔 【かお】 – face'),
                  Text('何 【なに／なん】 – what'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('する (exception) – to do'),
                  Text('搭乗 【とう・じょう】 – boarding'),
                  Text('アナウンス – announcement'),
                  Text('聞こえる 【き・こえる】 (ru-verb) – to be audible'),
                  Text('皆 【みんな】 – everybody'),
                  Text('ゲート – gate'),
                  Text('方 【ほう】 – direction, way'),
                  Text('走り出す 【はし・り・だ・す】 (u-verb) – to break into a run'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The 「や」 or 「や否や」（やいなや） phrase, when appended to a verb, is used to described something that happened right after that verb. Its meaning is essential the same as 「が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'か」. It is also another type of grammar that is not really used in regular conversational Japanese.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「否」 （read here as 「いな」） is a Kanji meaning “no” used in words like 「',
                  ),
                  VocabTooltip(
                    message: hitei,
                    inlineSpan: const TextSpan(
                      text: '否定',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. The literal meaning of this grammar is “whether the action was taken or not”. In other words, the second action is taken before you even take the time to determine whether the first event really happened or not.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'You can use this grammar by attaching 「や」 or 「や否や」 to the dictionary form of the first verb that occurred. Since this grammar is used for events that already have occurred, the second verb is usually in the past tense. However, you can use the dictionary tense to indicate that the events happen regularly. ',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「や／や否や」 to describe what happened right after',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「や」 or 「や否や」（やいなや） to the dictionary form of the first verb that occurred.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'や',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'や否や',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const Text(
                      'This grammar is almost always used for events that actually happened (past tense).',
                    ),
                    const Text(
                      'This grammar can be used with the present tense for regularly occurring events.',
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kao,
                              inlineSpan: const TextSpan(
                                text: '顔',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見る',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'や',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nanika,
                              inlineSpan: const TextSpan(
                                text: '何か',
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言おう',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(He) tried to say something as soon as he saw my face.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: toujou,
                              inlineSpan: const TextSpan(
                                text: '搭乗',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: anaunsu,
                              inlineSpan: const TextSpan(
                                text: 'アナウンス',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kikoeru,
                              inlineSpan: TextSpan(
                                text: '聞こえる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'や否や',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: geeto,
                              inlineSpan: const TextSpan(
                                text: 'ゲート',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'へ'),
                            VocabTooltip(
                              message: hashiridasu,
                              inlineSpan: const TextSpan(
                                text: '走り出した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'As soon as the announcement to board was audible, everybody started running toward the gate.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 早い 【はや・い】 (i-adj) – fast; early 否定 【ひ・てい】 – denial 見る 【み・る】 (ru-verb) – to see 私 【わたし】 – me, myself, I 顔 【かお】 – face 何 【なに／なん】 – what 言う 【い・う】 (u-verb) – to say する (exception) – to do 搭乗 【とう・じょう】 – boarding アナウンス – announcement 聞こえる 【き・こえる】 (ru-verb) – to be audible 皆 【みんな】 – everybody ゲート – gate 方 【ほう】 – direction, way 走り出す 【はし・り・だ・す】 (u-verb) – to break into a run The 「や」 or 「や否や」（やいなや） phrase, when appended to a verb, is used to described something that happened right after that verb. Its meaning is essential the same as 「が早いか」. It is also another type of grammar that is not really used in regular conversational Japanese. 「否」 （read here as 「いな」） is a Kanji meaning “no” used in words like 「否定」. The literal meaning of this grammar is “whether the action was taken or not”. In other words, the second action is taken before you even take the time to determine whether the first event really happened or not. You can use this grammar by attaching 「や」 or 「や否や」 to the dictionary form of the first verb that occurred. Since this grammar is used for events that already have occurred, the second verb is usually in the past tense. However, you can use the dictionary tense to indicate that the events happen regularly. Using 「や／や否や」 to describe what happened right after Attach 「や」 or 「や否や」（やいなや） to the dictionary form of the first verb that occurred. Examples: 見る → 見るや見る → 見るや否や This grammar is almost always used for events that actually happened (past tense). This grammar can be used with the present tense for regularly occurring events. Examples 私の顔を見るや、何か言おうとした。(He) tried to say something as soon as he saw my face. 搭乗のアナウンスが聞こえるや否や、みんながゲートの方へ走り出した。 As soon as the announcement to board was audible, everybody started running toward the gate.',
      grammars: ['や','や否や'],
      keywords: ['ya','yainaya','as soon as','right after'],
    ),
    Section(
      heading:
          'Using 「そばから」 to describe an event that repeatedly occurs soon after',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('する (exception) – to do'),
                  Text('子供 【こ・ども】 – child'),
                  Text('掃除 【そう・じ】 – cleaning'),
                  Text(
                      '散らかす 【ち・らかす】 (u-verb) – to scatter around; to leave untidy'),
                  Text('もう – already'),
                  Text('あきらめる (ru-verb) – to give up'),
                  Text('なる (u-verb) – to become'),
                  Text('教科書 【きょう・か・しょ】 – textbook'),
                  Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「そばから」 is yet another grammar that describes an event that happens right after another. However, unlike the expressions we have covered so far, 「そばから」 implies that the events are a recurring pattern. For example, you would use this grammar to express the fact that you just clean and clean your room only for it to get dirty again soon after.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Besides this difference, the rules for using this expression are exactly the same as 「が',
                  ),
                  VocabTooltip(
                    message: hayai,
                    inlineSpan: const TextSpan(
                      text: '早い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'か」 and 「や否や」. Just attach 「そばから」 to the dictionary form of the first verb that occurred. The past tense, though rare, also appears to be acceptable. However, the event that immediately follows is usually expressed with the non-past dictionary form because this grammar is used for repeated events and not a specific event in the past.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「そばから」 to describe an event that repeatedly occurs soon after',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「そばから」 to the dictionary form of the first verb that occurred.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: const TextSpan(
                                      text: '読む',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: yomu,
                                    inlineSpan: const TextSpan(
                                      text: '読む',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'そばから',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'そばから',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const Text(
                      'This grammar implies that the events occur repeatedly.',
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kodomo,
                              inlineSpan: const TextSpan(
                                text: '子供',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: souji,
                              inlineSpan: const TextSpan(
                                text: '掃除',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'そばから',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: chirakasu,
                              inlineSpan: const TextSpan(
                                text: '散らかす',
                              ),
                            ),
                            const TextSpan(text: 'から、'),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: akirameru,
                              inlineSpan: const TextSpan(
                                text: 'あきらめたくなった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The child messes up (the room) [repeatedly] as soon as I clean so I already became wanting to give up.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyoukasho,
                              inlineSpan: const TextSpan(
                                text: '教科書',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読んだ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'そばから',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: const TextSpan(
                                text: '忘れて',
                              ),
                            ),
                            VocabTooltip(
                              message: shimau,
                              inlineSpan: const TextSpan(
                                text: 'しまう',
                              ),
                            ),
                            const TextSpan(text: 'ので'),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Forget [repeatedly] right after I read the textbook so I can’t study.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 早い 【はや・い】 (i-adj) – fast; early 読む 【よ・む】 (u-verb) – to read する (exception) – to do 子供 【こ・ども】 – child 掃除 【そう・じ】 – cleaning 散らかす 【ち・らかす】 (u-verb) – to scatter around; to leave untidy もう – already あきらめる (ru-verb) – to give up なる (u-verb) – to become 教科書 【きょう・か・しょ】 – textbook 忘れる 【わす・れる】 (ru-verb) – to forget 勉強 【べん・きょう】 – study 出来る 【で・き・る】 (ru-verb) – to be able to do 「そばから」 is yet another grammar that describes an event that happens right after another. However, unlike the expressions we have covered so far, 「そばから」 implies that the events are a recurring pattern. For example, you would use this grammar to express the fact that you just clean and clean your room only for it to get dirty again soon after. Besides this difference, the rules for using this expression are exactly the same as 「が早いか」 and 「や否や」. Just attach 「そばから」 to the dictionary form of the first verb that occurred. The past tense, though rare, also appears to be acceptable. However, the event that immediately follows is usually expressed with the non-past dictionary form because this grammar is used for repeated events and not a specific event in the past. Using 「そばから」 to describe an event that repeatedly occurs soon after Attach 「そばから」 to the dictionary form of the first verb that occurred. Examples: 読む → 読むそばから する → するそばから This grammar implies that the events occur repeatedly. Examples 子供が掃除するそばから散らかすから、もうあきらめたくなった。 The child messes up (the room) [repeatedly] as soon as I clean so I already became wanting to give up. 教科書を読んだそばから忘れてしまうので勉強ができない。 Forget [repeatedly] right after I read the textbook so I can’t study.',
      grammars: ['そばから'],
      keywords: ['sobakara','repeatedly','after','keep'],
    ),
  ],
);
