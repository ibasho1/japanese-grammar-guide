import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson other = Lesson(
  title: 'Other Grammar',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'Hopefully, you’ve managed to get a good grasp of how grammar works in Japanese and how to use them to communicate your thoughts in the Japanese way. In this final section, we’ll be covering some left-over grammar that I couldn’t fit into a larger category.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Hopefully, you’ve managed to get a good grasp of how grammar works in Japanese and how to use them to communicate your thoughts in the Japanese way. In this final section, we’ll be covering some left-over grammar that I couldn’t fit into a larger category.',
      grammars: [],
      keywords: ['other grammar','other'],
    ),
    Section(
      heading: 'Using 「思いきや」 to describe something unexpected',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('昼間 【ひる・ま】 – daytime'),
                  Text('絶対 【ぜっ・たい】 (na-adj) – absolutely, unconditionally'),
                  Text('込む 【こ・む】 (u-verb) – to become crowded'),
                  Text('一人 【ひとり】 – 1 person; alone'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('この – this （abbr. of これの）'),
                  Text('レストラン – restaurant'),
                  Text('安い 【やす・い】 (i-adj) – cheap'),
                  Text('会計 【かい・けい】 – accountant; bill'),
                  Text('千円 【せん・えん】 – 1,000 yen'),
                  Text('以上 【い・じょう】 – greater or equal'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This is a grammar I learned out of a book and was surprised to actually hear it used in real life on a number of occasions. You use this grammar when you think one thing, but much to your astonishment, things actually turn out to be very different. You use it in the same way as you would express any thoughts, by using the quotation 「と」 and 「',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                  const TextSpan(
                    text: '」. The only difference is that you use 「',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思い',
                    ),
                  ),
                  const TextSpan(
                    text: 'きや」 instead of 「',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思う',
                    ),
                  ),
                  const TextSpan(
                    text: '」. There is no tense in 「',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'きや」, or rather, since the results already went against your expectations, the original thought is implicitly understood to be past tense.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「思いきや」 to describe something unforeseen or unexpected',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Attach 「',
                              ),
                              VocabTooltip(
                                message: omou,
                                inlineSpan: const TextSpan(
                                  text: '思い',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    'きや」 to the thought using the quotation 「と」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ある',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ある',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'と',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ある',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'と',
                                  ),
                                  VocabTooltip(
                                    message: omoi,
                                    inlineSpan: TextSpan(
                                      text: '思い',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'きや',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hiruma,
                              inlineSpan: const TextSpan(
                                text: '昼間',
                              ),
                            ),
                            const TextSpan(text: 'だから'),
                            VocabTooltip(
                              message: zettai,
                              inlineSpan: const TextSpan(
                                text: '絶対',
                              ),
                            ),
                            VocabTooltip(
                              message: komu,
                              inlineSpan: TextSpan(
                                text: '込んでいる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'きや',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: hitori,
                              inlineSpan: const TextSpan(
                                text: '一人',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: iru,
                              inlineSpan: const TextSpan(
                                text: 'いなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Despite having thought that it must be crowded since it was afternoon, (surprisingly) not a single person was there.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: resutoran,
                              inlineSpan: const TextSpan(
                                text: 'レストラン',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: yasui,
                              inlineSpan: TextSpan(
                                text: '安い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'と',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'きや',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kaikei,
                              inlineSpan: const TextSpan(
                                text: '会計',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: gosenen,
                              inlineSpan: const TextSpan(
                                text: '5千円',
                              ),
                            ),
                            VocabTooltip(
                              message: ijou,
                              inlineSpan: const TextSpan(
                                text: '以上',
                              ),
                            ),
                            const TextSpan(text: 'だった！'),
                          ],
                        ),
                      ),
                      const Text(
                          'Thought this restaurant would be cheap but (surprisingly) the bill was over 5,000 yen!'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 思う 【おも・う】 (u-verb) – to think ある (u-verb) – to exist (inanimate) 昼間 【ひる・ま】 – daytime 絶対 【ぜっ・たい】 (na-adj) – absolutely, unconditionally 込む 【こ・む】 (u-verb) – to become crowded 一人 【ひとり】 – 1 person; alone いる (ru-verb) – to exist (animate) この – this （abbr. of これの） レストラン – restaurant 安い 【やす・い】 (i-adj) – cheap 会計 【かい・けい】 – accountant; bill 千円 【せん・えん】 – 1,000 yen 以上 【い・じょう】 – greater or equal This is a grammar I learned out of a book and was surprised to actually hear it used in real life on a number of occasions. You use this grammar when you think one thing, but much to your astonishment, things actually turn out to be very different. You use it in the same way as you would express any thoughts, by using the quotation 「と」 and 「思う」. The only difference is that you use 「思いきや」 instead of 「思う」. There is no tense in 「思いきや」, or rather, since the results already went against your expectations, the original thought is implicitly understood to be past tense. Using 「思いきや」 to describe something unforeseen or unexpected Attach 「思いきや」 to the thought using the quotation 「と」. Examples: ある → あると → あると思いきや Examples 昼間だから絶対込んでいると思いきや、一人もいなかった。 Despite having thought that it must be crowded since it was afternoon, (surprisingly) not a single person was there. このレストランは安いと思いきや、会計は5千円以上だった！ Thought this restaurant would be cheap but (surprisingly) the bill was over 5,000 yen!',
      grammars: ['思いきや'],
      keywords: ['unexpected', 'unforseen', 'astonishment', 'surprise', 'おもいきや'],
    ),
    Section(
      heading: 'Using 「～がてら」 to do two things at one time',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('散歩 【さん・ぽ】 – walk, stroll'),
                  Text('作る 【つく・る】 (u-verb) – to make'),
                  Text('タバコ – tobacco; cigarettes'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('博物館 【はく・ぶつ・かん】 – museum'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('お土産 【お・みやげ】 – souvenir'),
                  Text('つもり – intention, plan'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This rather formal and seldom-used grammar is used to indicate that two actions were done at the same time. The nuance is a bit difference from 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 in that some or all of the time spent on doing one action was also used to do another action as an aside. Remember, 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is used to describe two exactly concurrent actions.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The interesting thing about this grammar is that no verb is required. You can just attach it a noun, and the verb “to do” is inferred. For instance, “while taking a stroll” can simply be expressed as 「',
                  ),
                  VocabTooltip(
                    message: sanpo,
                    inlineSpan: const TextSpan(
                      text: '散歩',
                    ),
                  ),
                  TextSpan(
                    text: 'がてら',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  const TextSpan(
                    text:
                        '」. In the case where you want to employ a different verb, you also have the option of attaching 「がてら」 to the stem similar to the 「',
                  ),
                  VocabTooltip(
                    message: nagara,
                    inlineSpan: const TextSpan(
                      text: 'ながら',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 usage. In addition, the verb or noun that is accompanied by 「がてら」 is the main action while the following action is the one done on the side.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「～がてら」 to do two things at one time',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Attach 「がてら」 to the noun or verb stem of the main action. In case of a noun, the verb 「',
                              ),
                              VocabTooltip(
                                message: suru,
                                inlineSpan: const TextSpan(
                                  text: 'する',
                                ),
                              ),
                              const TextSpan(
                                text: '」 is inferred.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: sanpo,
                                    inlineSpan: const TextSpan(
                                      text: '散歩',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: sanpo,
                                    inlineSpan: const TextSpan(
                                      text: '散歩',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がてら',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: tsukuru,
                                    inlineSpan: const TextSpan(
                                      text: '作る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tsukuru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '作',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: tsukuru,
                                    inlineSpan: const TextSpan(
                                      text: '作り',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がてら',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sanpo,
                              inlineSpan: TextSpan(
                                text: '散歩',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がてら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: tabako,
                              inlineSpan: const TextSpan(
                                text: 'タバコ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買い',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行きました',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'While taking a stroll, I also used that time to buy cigarettes.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hakubutsukan,
                              inlineSpan: const TextSpan(
                                text: '博物館',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がてら',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'に、'),
                            VocabTooltip(
                              message: omiyage,
                              inlineSpan: const TextSpan(
                                text: 'お土産',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買う',
                              ),
                            ),
                            VocabTooltip(
                              message: tsumori,
                              inlineSpan: const TextSpan(
                                text: 'つもり',
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text(
                          'While seeing the museum, I plan to also use that time to buy souvenirs.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 散歩 【さん・ぽ】 – walk, stroll 作る 【つく・る】 (u-verb) – to make タバコ – tobacco; cigarettes 買う 【か・う】 (u-verb) – to buy 行く 【い・く】 (u-verb) – to go 博物館 【はく・ぶつ・かん】 – museum 見る 【み・る】 (ru-verb) – to see お土産 【お・みやげ】 – souvenir つもり – intention, plan This rather formal and seldom-used grammar is used to indicate that two actions were done at the same time. The nuance is a bit difference from 「ながら」 in that some or all of the time spent on doing one action was also used to do another action as an aside. Remember, 「ながら」 is used to describe two exactly concurrent actions. The interesting thing about this grammar is that no verb is required. You can just attach it a noun, and the verb “to do” is inferred. For instance, “while taking a stroll” can simply be expressed as 「散歩がてら」. In the case where you want to employ a different verb, you also have the option of attaching 「がてら」 to the stem similar to the 「ながら」 usage. In addition, the verb or noun that is accompanied by 「がてら」 is the main action while the following action is the one done on the side. Using 「～がてら」 to do two things at one time Attach 「がてら」 to the noun or verb stem of the main action. In case of a noun, the verb 「する」 is inferred. Examples: 散歩 → 散歩がてら 作る → 作り → 作りがてら Examples 散歩がてら、タバコを買いに行きました。 While taking a stroll, I also used that time to buy cigarettes. 博物館を見がてらに、お土産を買うつもりです。 While seeing the museum, I plan to also use that time to buy souvenirs.',
      grammars: ['～がてら'],
      keywords: ['がてら', 'formal', 'simultaneous', 'simultaneously', 'ながら'],
    ),
    Section(
      heading: 'Using 「～あげく（挙句）」 to describe a bad result',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text(
                      '挙句 【あげ・く】 – in the end (after a long process); at last'),
                  Text('喧嘩 【けん・か】 – quarrel'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('事情 【じ・じょう】 – circumstances'),
                  Text('～時間 【～じ・かん】 – counter for span of hours'),
                  Text('掛ける 【か・ける】 (ru-verb) – to hang; to take (time, money)'),
                  Text('説明 【せつ・めい】 – explanation'),
                  Text('する (exception) – to do'),
                  Text('納得 【なっ・とく】 – understanding; agreement'),
                  Text('もらう (u-verb) – to receive'),
                  Text('先生 【せん・せい】 – teacher'),
                  Text('相談 【そう・だん】 – consultation'),
                  Text('退学 【たい・がく】 – dropping out of school'),
                  Text('こと – event, matter'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: ageku,
                    inlineSpan: const TextSpan(
                      text: 'あげく',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a grammar used to describe a result, usually negative, that came about after a great deal of effort. The rule for this grammar is very simple. You modify the verb or noun that was carried out with 「',
                  ),
                  VocabTooltip(
                    message: ageku,
                    inlineSpan: const TextSpan(
                      text: 'あげく',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 and then describe the final result that came about from that verb or noun. Because this grammar is used to describe a result from an action already completed, it is used with the past tense of the verb. 「',
                  ),
                  VocabTooltip(
                    message: ageku,
                    inlineSpan: const TextSpan(
                      text: 'あげく',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is essentially treated the same as any noun. In other words, you would need the 「の」 particle to modify another noun.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「',
                  ),
                  VocabTooltip(
                    message: agekunohate,
                    inlineSpan: const TextSpan(
                      text: 'あげくの果て',
                    ),
                  ),
                  const TextSpan(
                    text: '」 is another stronger version of this grammar.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「～あげく」 to describe a final result',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'Attach 「',
                              ),
                              VocabTooltip(
                                message: ageku,
                                inlineSpan: const TextSpan(
                                  text: 'あげく',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    '」 to the verb or noun that created the end result （「の」 particle is required for nouns）.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kenka,
                                    inlineSpan: const TextSpan(
                                      text: 'けんか',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kenka,
                                    inlineSpan: const TextSpan(
                                      text: 'けんか',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'の',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: ageku,
                                    inlineSpan: TextSpan(
                                      text: 'あげく',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kangaeru,
                                    inlineSpan: const TextSpan(
                                      text: '考えた',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kangaeru,
                                    inlineSpan: const TextSpan(
                                      text: '考えた',
                                    ),
                                  ),
                                  VocabTooltip(
                                    message: ageku,
                                    inlineSpan: TextSpan(
                                      text: 'あげく',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jijou,
                              inlineSpan: const TextSpan(
                                text: '事情',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: nijikan,
                              inlineSpan: const TextSpan(
                                text: '2時間',
                              ),
                            ),
                            VocabTooltip(
                              message: kakeruTake,
                              inlineSpan: const TextSpan(
                                text: 'かけて',
                              ),
                            ),
                            VocabTooltip(
                              message: setsumei,
                              inlineSpan: const TextSpan(
                                text: '説明',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            VocabTooltip(
                              message: ageku,
                              inlineSpan: TextSpan(
                                text: 'あげく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: nattoku,
                              inlineSpan: const TextSpan(
                                text: '納得',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            VocabTooltip(
                              message: morau,
                              inlineSpan: const TextSpan(
                                text: 'もらえなかった',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(After a great deal of) explaining the circumstances for 2 hours, (in the end), couldn’t receive understanding.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sensei,
                              inlineSpan: const TextSpan(
                                text: '先生',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: soudan,
                              inlineSpan: const TextSpan(
                                text: '相談',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: ageku,
                              inlineSpan: TextSpan(
                                text: 'あげく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: taigaku,
                              inlineSpan: const TextSpan(
                                text: '退学',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'しない',
                              ),
                            ),
                            VocabTooltip(
                              message: koto,
                              inlineSpan: const TextSpan(
                                text: 'こと',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          '(After much) consulting with teacher, (in the end), decided on not dropping out of school.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 挙句 【あげ・く】 – in the end (after a long process); at last 喧嘩 【けん・か】 – quarrel 考える 【かんが・える】 (ru-verb) – to think 事情 【じ・じょう】 – circumstances～時間 【～じ・かん】 – counter for span of hours 掛ける 【か・ける】 (ru-verb) – to hang; to take (time, money) 説明 【せつ・めい】 – explanation する (exception) – to do 納得 【なっ・とく】 – understanding; agreement もらう (u-verb) – to receive 先生 【せん・せい】 – teacher 相談 【そう・だん】 – consultation 退学 【たい・がく】 – dropping out of school こと – event, matter 「あげく」 is a grammar used to describe a result, usually negative, that came about after a great deal of effort. The rule for this grammar is very simple. You modify the verb or noun that was carried out with 「あげく」 and then describe the final result that came about from that verb or noun. Because this grammar is used to describe a result from an action already completed, it is used with the past tense of the verb. 「あげく」 is essentially treated the same as any noun. In other words, you would need the 「の」 particle to modify another noun. 「あげくの果て」 is another stronger version of this grammar. Using 「～あげく」 to describe a final result Attach 「あげく」 to the verb or noun that created the end result （「の」 particle is required for nouns）. Examples: けんか → けんかのあげく 考えた → 考えたあげく Examples 事情を2時間かけて説明したあげく、納得してもらえなかった。(After a great deal of) explaining the circumstances for 2 hours, (in the end), couldn’t receive understanding. 先生と相談のあげく、退学をしないことにした。(After much) consulting with teacher, (in the end), decided on not dropping out of school.',
      grammars: ['～あげく（挙句）'],
      keywords: ['ageku','in the end','after'],
    ),
  ],
);
