import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson should = Lesson(
  title: 'Things That Should Be a Certain Way',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this lesson, we’ll learn how to express the way things are supposed depending on what we mean by “supposed”. While the first two grammar points 「はず」 and 「べき」 come up often and are quite useful, you’ll rarely ever encounter 「べく」 or 「べからず」. You can safely skip those lessons unless you are studying for the JLPT.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this lesson, we’ll learn how to express the way things are supposed depending on what we mean by “supposed”. While the first two grammar points 「はず」 and 「べき」 come up often and are quite useful, you’ll rarely ever encounter 「べく」 or 「べからず」. You can safely skip those lessons unless you are studying for the JLPT.',
      grammars: [],
      keywords: ['should','ought'],
    ),
    Section(
      heading: 'Using 「はず」 to describe an expectation',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('日曜日 【にち・よう・び】 – Sunday'),
                  Text('可能 【か・のう】 (na-adj) – possible'),
                  Text('おいしい (i-adj) – tasty'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('彼【かれ】 – he; boyfriend'),
                  Text('漫画 【まん・が】 – comic book'),
                  Text('マニア – mania'),
                  Text('これ – this'),
                  Text('～ら – pluralizing suffix'),
                  Text('もう – already'),
                  Text('全部 【ぜん・ぶ】 – everything'),
                  Text('読む 【よ・む】 (u-verb) – to read'),
                  Text('この – this （abbr. of これの）'),
                  Text('料理 【りょう・り】 – cooking; cuisine; dish'),
                  Text('焦げる 【こ・げる】 (ru-verb) – to burn, to be burned'),
                  Text('まずい (i-adj) – unpleasant'),
                  Text('色々 【いろ・いろ】 (na-adj) – various'),
                  Text('予定 【よ・てい】 – plans, arrangement'),
                  Text('する (exception) – to do'),
                  Text('今年 【こ・とし】 – this year'),
                  Text('楽しい 【たの・しい】 (i-adj) – fun'),
                  Text('クリスマス – Christmas'),
                  Text('そう – (things are) that way'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('直す 【なお・す】 (u-verb) – to correct, to fix'),
                  Text('打合せ 【うち・あわ・せ】 – meeting'),
                  Text('毎週 【まい・しゅう】 – every week'),
                  Text('～時 【～じ】 – counter for hours'),
                  Text('始まる 【はじ・まる】 (u-verb) – to begin'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The first grammar we will learn is 「はず」, which is used to express something that was or is supposed to be. You can treat 「はず」 just like a regular noun as it is usually attached to the adjective or verb that is supposed to be or supposed to happen.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The only thing to be careful about here is expressing an expectation of something ',
                  ),
                  const TextSpan(
                    text: 'not',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' happening. To do this, you must use the negative existence verb 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 to say that such an expectation does not exist. This might be in the form of 「～はず',
                  ),
                  TextSpan(
                    text: 'が',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「～はず',
                  ),
                  TextSpan(
                    text: 'は',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ない',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 depending on which particle you want to use. The negative conjugation 「はず',
                  ),
                  TextSpan(
                    text: 'じゃない',
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                  const TextSpan(
                    text:
                        '」 is really only used when you want to confirm in a positive sense such as 「～はずじゃないか？」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「はず」 to describe an expectation',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Use 「はず」 just like a regular noun to modify the expected thing.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nichiyoubi,
                                    inlineSpan: const TextSpan(
                                      text: '日曜日',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'の',
                                  ),
                                  TextSpan(
                                    text: 'はず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (noun)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kanou,
                                    inlineSpan: const TextSpan(
                                      text: '可能',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'な',
                                  ),
                                  TextSpan(
                                    text: 'はず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (na-adjective)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: oishii,
                                    inlineSpan: const TextSpan(
                                      text: 'おいしい',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'はず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (i-adjective)',
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kaeru,
                                    inlineSpan: const TextSpan(
                                      text: '帰る',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'はず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' (verb)',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'For the case where you expect the negative, use the 「',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ない',
                                ),
                              ),
                              const TextSpan(
                                text: '」 verb for nonexistence.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kaeru,
                                    inlineSpan: const TextSpan(
                                      text: '帰る',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'はず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kaeru,
                                    inlineSpan: const TextSpan(
                                      text: '帰る',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'はずが',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: TextSpan(
                                      text: 'ない',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: manga,
                              inlineSpan: const TextSpan(
                                text: '漫画',
                              ),
                            ),
                            VocabTooltip(
                              message: mania,
                              inlineSpan: const TextSpan(
                                text: 'マニア',
                              ),
                            ),
                            const TextSpan(
                              text: 'だから、',
                            ),
                            VocabTooltip(
                              message: kore,
                              inlineSpan: const TextSpan(
                                text: 'これ',
                              ),
                            ),
                            VocabTooltip(
                              message: ra,
                              inlineSpan: const TextSpan(
                                text: 'ら',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: mou,
                              inlineSpan: const TextSpan(
                                text: 'もう',
                              ),
                            ),
                            VocabTooltip(
                              message: zenbu,
                              inlineSpan: const TextSpan(
                                text: '全部',
                              ),
                            ),
                            VocabTooltip(
                              message: yomu,
                              inlineSpan: TextSpan(
                                text: '読んだ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'はず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'だよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'He has a mania for comic book(s) so I expect he read all these already.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: ryouri,
                              inlineSpan: const TextSpan(
                                text: '料理',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: oishii,
                              inlineSpan: TextSpan(
                                text: 'おいしい',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'はず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'だったが、',
                            ),
                            VocabTooltip(
                              message: kogeru,
                              inlineSpan: const TextSpan(
                                text: '焦げ',
                              ),
                            ),
                            const TextSpan(
                              text: 'ちゃって、',
                            ),
                            VocabTooltip(
                              message: mazui,
                              inlineSpan: const TextSpan(
                                text: 'まずく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'This dish was expected to be tasty but it burned and became distasteful.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: iroiro,
                              inlineSpan: const TextSpan(
                                text: '色々',
                              ),
                            ),
                            VocabTooltip(
                              message: yotei,
                              inlineSpan: const TextSpan(
                                text: '予定',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'してある',
                              ),
                            ),
                            const TextSpan(
                              text: 'から、',
                            ),
                            VocabTooltip(
                              message: kotoshi,
                              inlineSpan: const TextSpan(
                                text: '今年',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: tanoshii,
                              inlineSpan: const TextSpan(
                                text: '楽しい',
                              ),
                            ),
                            VocabTooltip(
                              message: kurisumasu,
                              inlineSpan: TextSpan(
                                text: 'クリスマス',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'のはず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Because various things have been planned out, I expect a fun Christmas this year.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sou,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: const TextSpan(
                                text: '簡単',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naosu,
                              inlineSpan: TextSpan(
                                text: '直せる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'はずが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ない',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'It’s not supposed to be that easy to fix.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: uchiawase,
                              inlineSpan: const TextSpan(
                                text: '打合せ',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: maishuu,
                              inlineSpan: const TextSpan(
                                text: '毎週',
                              ),
                            ),
                            VocabTooltip(
                              message: niji,
                              inlineSpan: const TextSpan(
                                text: '２時',
                              ),
                            ),
                            const TextSpan(
                              text: 'から',
                            ),
                            VocabTooltip(
                              message: hajimaru,
                              inlineSpan: TextSpan(
                                text: '始まる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'はず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'じゃないですか？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'This meeting is supposed to start every week at 2 o’clock, isn’t it?',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Here are more examples from the ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'WWWJDIC',
                    ),
                    uri:
                        'http://nihongo.monash.edu/cgi-bin/wwwjdic?1Q%C8%A6_0__',
                  ),
                  const TextSpan(
                    text: '. You may also want to check out the jeKai entry.',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'jeKai entry',
                    ),
                    uri:
                        'https://web.archive.org/web/20200121193040/http://www.jekai.org/entries/aa/00/nn/aa00nn81.htm',
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ある (u-verb) – to exist (inanimate) 日曜日 【にち・よう・び】 – Sunday 可能 【か・のう】 (na-adj) – possible おいしい (i-adj) – tasty 帰る 【かえ・る】 (u-verb) – to go home 彼【かれ】 – he; boyfriend 漫画 【まん・が】 – comic book マニア – mania これ – this～ら – pluralizing suffix もう – already 全部 【ぜん・ぶ】 – everything 読む 【よ・む】 (u-verb) – to read この – this （abbr. of これの） 料理 【りょう・り】 – cooking; cuisine; dish 焦げる 【こ・げる】 (ru-verb) – to burn, to be burned まずい (i-adj) – unpleasant 色々 【いろ・いろ】 (na-adj) – various 予定 【よ・てい】 – plans, arrangement する (exception) – to do 今年 【こ・とし】 – this year 楽しい 【たの・しい】 (i-adj) – fun クリスマス – Christmas そう – (things are) that way 簡単 【かん・たん】 (na-adj) – simple 直す 【なお・す】 (u-verb) – to correct, to fix 打合せ 【うち・あわ・せ】 – meeting 毎週 【まい・しゅう】 – every week～時 【～じ】 – counter for hours 始まる 【はじ・まる】 (u-verb) – to begin The first grammar we will learn is 「はず」, which is used to express something that was or is supposed to be. You can treat 「はず」 just like a regular noun as it is usually attached to the adjective or verb that is supposed to be or supposed to happen. The only thing to be careful about here is expressing an expectation of something not happening. To do this, you must use the negative existence verb 「ない」 to say that such an expectation does not exist. This might be in the form of 「～はずがない」 or 「～はずはない」 depending on which particle you want to use. The negative conjugation 「はずじゃない」 is really only used when you want to confirm in a positive sense such as 「～はずじゃないか？」. Using 「はず」 to describe an expectation Use 「はず」 just like a regular noun to modify the expected thing. Examples: 日曜日のはず (noun) 可能なはず (na-adjective) おいしいはず (i-adjective) 帰るはず (verb) For the case where you expect the negative, use the 「ない」 verb for nonexistence. Example: 帰るはず → 帰るはずがない Examples 彼は漫画マニアだから、これらをもう全部読んだはずだよ。 He has a mania for comic book(s) so I expect he read all these already. この料理はおいしいはずだったが、焦げちゃって、まずくなった。 This dish was expected to be tasty but it burned and became distasteful. 色々予定してあるから、今年は楽しいクリスマスのはず。 Because various things have been planned out, I expect a fun Christmas this year. そう簡単に直せるはずがないよ。 It’s not supposed to be that easy to fix. 打合せは毎週２時から始まるはずじゃないですか？ This meeting is supposed to start every week at 2 o’clock, isn’t it? Here are more examples from the WWWJDIC. You may also want to check out the jeKai entry.jeKai entry.',
      grammars: ['はず'],
      keywords: ['hazu','expectation','supposed'],
    ),
    Section(
      heading: 'Using 「べき」 to describe actions one should do',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('絶対 【ぜっ・たい】 (na-adj) – absolutely, unconditionally'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('強い 【つよ・い】 (i-adj) – strong'),
                  Text('推奨 【すい・しょう】 – recommendation'),
                  Text('する (exception) – to do'),
                  Text('擦る 【す・る】 (u-verb) – to rub'),
                  Text('行う 【おこな・う】 (u-verb) – to conduct, to carry out'),
                  Text('何 【なに／なん】 – what'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('前 【まえ】 – front; before'),
                  Text('本当 【ほん・とう】 – real'),
                  Text('必要 【ひつ・よう】 – necessity'),
                  Text('どう – how'),
                  Text('いい (i-adj) – good'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('例え 【たと・え】 – example'),
                  Text('国 【くに】 – country'),
                  Text('国民 【こく・みん】 – people, citizen'),
                  Text('騙す 【だま・す】 (u-verb) – to trick, to cheat, to deceive'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('預金者 【よ・きん・しゃ】 – depositor'),
                  Text('大手 【おお・て】 – large corporation'),
                  Text('銀行 【ぎん・こう】 – bank'),
                  Text('相手 【あい・て】 – other party'),
                  Text('取る 【と・る】 (u-verb) – to take'),
                  Text('訴訟 【そ・しょう】 – litigation, lawsuit'),
                  Text('起こす 【お・こす】 (u-verb) – to cause, to wake someone'),
                  Text('ケース – case'),
                  Text('出る 【で・る】 (ru-verb) – to come out'),
                  Text('金融庁 【きん・ゆう・ちょう】 – Financial Services Agency'),
                  Text('被害者 【ひ・がい・しゃ】 – victim'),
                  Text('救済 【きゅう・さい】 – relief, aid'),
                  Text('優先 【ゆう・せん】 – preference, priority, precedence'),
                  Text('金融 【きん・ゆう】 – financing'),
                  Text('機関 【き・かん】 – institution'),
                  Text('犯罪 【はん・ざい】 – crime'),
                  Text('防止 【ぼう・し】 – prevention'),
                  Text('対策 【たい・さく】 – measure'),
                  Text('強化 【きょう・か】 – strengthen'),
                  Text('促す 【うなが・す】 (u-verb) – to urge'),
                  Text('判断 【はん・だん】 – judgement, decision'),
                  Text('朝日 【あさ・ひ】 – Asahi'),
                  Text('新聞 【しん・ぶん】 – newspaper'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「べき」 is a verb suffix used to describe something that is supposed to be done. This suffix is commonly defined as “should”, however, one must realize that it cannot be used to make suggestions like the sentence, “You should go to the doctor.” If you use 「べき」, it sounds more like, “You are supposed to go to the doctor.” 「べき」 has a much stronger tone and makes you sound like a know-it-all telling people what to do. For making suggestions, it is customary to use the comparison 「',
                  ),
                  VocabTooltip(
                    message: hou,
                    inlineSpan: const TextSpan(text: '方'),
                  ),
                  const TextSpan(
                    text: 'が',
                  ),
                  VocabTooltip(
                    message: ii,
                    inlineSpan: const TextSpan(text: 'いい'),
                  ),
                  const TextSpan(
                    text:
                        '」 grammar instead. For this reason, this grammar is almost never used to directly tell someone what to do. It is usually used in reference to oneself where you can be as bossy as you want or in a neutral context where circumstances dictate what is proper and what is not. One such example would be a sentence like, “We are supposed to raise our kids properly with a good education.”',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Unlike the 「はず」 grammar, there is no expectation that something is going to happen. Rather, this grammar describes what one should do in a given circumstance. In Japanese, you might define it as meaning 「',
                  ),
                  VocabTooltip(
                    message: zettai,
                    inlineSpan: const TextSpan(text: '絶対'),
                  ),
                  const TextSpan(
                    text: 'ではないが、',
                  ),
                  VocabTooltip(
                    message: tsuyoi,
                    inlineSpan: const TextSpan(text: '強く'),
                  ),
                  VocabTooltip(
                    message: suishou,
                    inlineSpan: const TextSpan(text: '推奨'),
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'されている'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'There is very little of grammatical interest. 「べき」 works just like a regular noun and so you can conjugate it as 「べきじゃない」、「べきだった」, and so on. The only thing to note here is that when you’re using it with 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text:
                        '」, the verb meaning “to do”, you can optionally drop the 「る」 from 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text: 'べき」 to produce 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'す'),
                  ),
                  const TextSpan(
                    text: 'べき」. You can do this with this verb ',
                  ),
                  const TextSpan(
                    text: 'only',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                  const TextSpan(
                    text:
                        ' and it does not apply for any other verbs even if the verb is written as 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(text: 'する'),
                  ),
                  const TextSpan(
                    text: '」 such as 「',
                  ),
                  VocabTooltip(
                    message: suruRub,
                    inlineSpan: const TextSpan(text: '擦る'),
                  ),
                  const TextSpan(
                    text: '」, the verb meaning “to rub”.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「べき」 for actions that should be done',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「べき」 to the action that should be done.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べき',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べき',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text: 'For the generic “to do ” verb 「',
                              ),
                              VocabTooltip(
                                message: suru,
                                inlineSpan: const TextSpan(
                                  text: 'する',
                                ),
                              ),
                              const TextSpan(
                                text: '」 only, you can remove the 「る」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'す',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋',
                                  ),
                                  const TextSpan(
                                    text: 'べき',
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'す',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べき',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: nanika,
                              inlineSpan: const TextSpan(
                                text: '何か',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買う',
                              ),
                            ),
                            VocabTooltip(
                              message: mae,
                              inlineSpan: const TextSpan(
                                text: '前',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hontou,
                              inlineSpan: const TextSpan(
                                text: '本当',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hitsuyou,
                              inlineSpan: const TextSpan(
                                text: '必要',
                              ),
                            ),
                            const TextSpan(text: 'かどうかを'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よく',
                              ),
                            ),
                            VocabTooltip(
                              message: kangaeru,
                              inlineSpan: TextSpan(
                                text: '考える',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べき',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text: 'Before buying something, one '),
                            TextSpan(
                              text: 'should',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                                text:
                                    ' think well on whether it’s really necessary or not.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tatoe,
                              inlineSpan: const TextSpan(
                                text: '例え',
                              ),
                            ),
                            VocabTooltip(
                              message: kuni,
                              inlineSpan: const TextSpan(
                                text: '国',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: tame,
                              inlineSpan: const TextSpan(
                                text: 'ため',
                              ),
                            ),
                            const TextSpan(text: 'であっても、'),
                            VocabTooltip(
                              message: kokumin,
                              inlineSpan: const TextSpan(
                                text: '国民',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: damasu,
                              inlineSpan: TextSpan(
                                text: '騙す',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べき',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'ではないと'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: const TextSpan(
                                text: '思う',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text:
                                    'Even if it is, for example, for the country, I don’t think the country’s citizens '),
                            TextSpan(
                              text: 'should',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: ' be deceived.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: yokinsha,
                              inlineSpan: const TextSpan(
                                text: '預金者',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: oote,
                              inlineSpan: const TextSpan(
                                text: '大手',
                              ),
                            ),
                            VocabTooltip(
                              message: ginkou,
                              inlineSpan: const TextSpan(
                                text: '銀行',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: aite,
                              inlineSpan: const TextSpan(
                                text: '相手',
                              ),
                            ),
                            VocabTooltip(
                              message: toru,
                              inlineSpan: const TextSpan(
                                text: '取って',
                              ),
                            ),
                            VocabTooltip(
                              message: soshou,
                              inlineSpan: const TextSpan(
                                text: '訴訟',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: okosu,
                              inlineSpan: const TextSpan(
                                text: '起こす',
                              ),
                            ),
                            VocabTooltip(
                              message: keesu,
                              inlineSpan: const TextSpan(
                                text: 'ケース',
                              ),
                            ),
                            const TextSpan(text: 'も'),
                            VocabTooltip(
                              message: deru,
                              inlineSpan: const TextSpan(
                                text: '出て',
                              ),
                            ),
                            const TextSpan(text: 'おり、'),
                            VocabTooltip(
                              message: kinyuuchou,
                              inlineSpan: const TextSpan(
                                text: '金融庁',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: higaisha,
                              inlineSpan: const TextSpan(
                                text: '被害者',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kyuusai,
                              inlineSpan: const TextSpan(
                                text: '救済',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: yuusen,
                              inlineSpan: const TextSpan(
                                text: '優先',
                              ),
                            ),
                            VocabTooltip(
                              message: suberu,
                              inlineSpan: const TextSpan(
                                text: 'させて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: kinyuu,
                              inlineSpan: const TextSpan(
                                text: '金融',
                              ),
                            ),
                            VocabTooltip(
                              message: kikan,
                              inlineSpan: const TextSpan(
                                text: '機関',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hanzai,
                              inlineSpan: const TextSpan(
                                text: '犯罪',
                              ),
                            ),
                            VocabTooltip(
                              message: boushi,
                              inlineSpan: const TextSpan(
                                text: '防止',
                              ),
                            ),
                            VocabTooltip(
                              message: taisaku,
                              inlineSpan: const TextSpan(
                                text: '対策',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kyouka,
                              inlineSpan: const TextSpan(
                                text: '強化',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: unagasu,
                              inlineSpan: TextSpan(
                                text: '促す',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べき',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'だと'),
                            VocabTooltip(
                              message: handan,
                              inlineSpan: const TextSpan(
                                text: '判断',
                              ),
                            ),
                            const TextSpan(text: '。（'),
                            VocabTooltip(
                              message: asahi,
                              inlineSpan: const TextSpan(
                                text: '朝日',
                              ),
                            ),
                            VocabTooltip(
                              message: shinbun,
                              inlineSpan: const TextSpan(
                                text: '新聞',
                              ),
                            ),
                            const TextSpan(text: '）'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text:
                                    'With cases coming out of depositors suing large banks, the Financial Services Agency decided it '),
                            TextSpan(
                              text: 'should',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                                text:
                                    ' prioritize relief for victims and urge banks to strengthen measures for crime prevention.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 絶対 【ぜっ・たい】 (na-adj) – absolutely, unconditionally ある (u-verb) – to exist (inanimate) 強い 【つよ・い】 (i-adj) – strong 推奨 【すい・しょう】 – recommendation する (exception) – to do 擦る 【す・る】 (u-verb) – to rub 行う 【おこな・う】 (u-verb) – to conduct, to carry out 何 【なに／なん】 – what 買う 【か・う】 (u-verb) – to buy 前 【まえ】 – front; before 本当 【ほん・とう】 – real 必要 【ひつ・よう】 – necessity どう – how いい (i-adj) – good 考える 【かんが・える】 (ru-verb) – to think 例え 【たと・え】 – example 国 【くに】 – country 国民 【こく・みん】 – people, citizen 騙す 【だま・す】 (u-verb) – to trick, to cheat, to deceive 思う 【おも・う】 (u-verb) – to think 預金者 【よ・きん・しゃ】 – depositor 大手 【おお・て】 – large corporation 銀行 【ぎん・こう】 – bank 相手 【あい・て】 – other party 取る 【と・る】 (u-verb) – to take 訴訟 【そ・しょう】 – litigation, lawsuit 起こす 【お・こす】 (u-verb) – to cause, to wake someone ケース – case 出る 【で・る】 (ru-verb) – to come out 金融庁 【きん・ゆう・ちょう】 – Financial Services Agency 被害者 【ひ・がい・しゃ】 – victim 救済 【きゅう・さい】 – relief, aid 優先 【ゆう・せん】 – preference, priority, precedence 金融 【きん・ゆう】 – financing 機関 【き・かん】 – institution 犯罪 【はん・ざい】 – crime 防止 【ぼう・し】 – prevention 対策 【たい・さく】 – measure 強化 【きょう・か】 – strengthen 促す 【うなが・す】 (u-verb) – to urge 判断 【はん・だん】 – judgement, decision 朝日 【あさ・ひ】 – Asahi 新聞 【しん・ぶん】 – newspaper 「べき」 is a verb suffix used to describe something that is supposed to be done. This suffix is commonly defined as “should”, however, one must realize that it cannot be used to make suggestions like the sentence, “You should go to the doctor.” If you use 「べき」, it sounds more like, “You are supposed to go to the doctor.” 「べき」 has a much stronger tone and makes you sound like a know-it-all telling people what to do. For making suggestions, it is customary to use the comparison 「方がいい」 grammar instead. For this reason, this grammar is almost never used to directly tell someone what to do. It is usually used in reference to oneself where you can be as bossy as you want or in a neutral context where circumstances dictate what is proper and what is not. One such example would be a sentence like, “We are supposed to raise our kids properly with a good education.” Unlike the 「はず」 grammar, there is no expectation that something is going to happen. Rather, this grammar describes what one should do in a given circumstance. In Japanese, you might define it as meaning 「絶対ではないが、強く推奨されている」. There is very little of grammatical interest. 「べき」 works just like a regular noun and so you can conjugate it as 「べきじゃない」、「べきだった」, and so on. The only thing to note here is that when you’re using it with 「する」, the verb meaning “to do”, you can optionally drop the 「る」 from 「するべき」 to produce 「すべき」. You can do this with this verb only and it does not apply for any other verbs even if the verb is written as 「する」 such as 「擦る」, the verb meaning “to rub”. Using 「べき」 for actions that should be done Attach 「べき」 to the action that should be done. Examples: 行う → 行うべき する → するべき For the generic “to do ” verb 「する」 only, you can remove the 「る」. Example: する＋べき → すべき Examples 何かを買う前に本当に必要かどうかをよく考えるべきだ。 Before buying something, one should think well on whether it’s really necessary or not. 例え国のためであっても、国民を騙すべきではないと思う。 Even if it is, for example, for the country, I don’t think the country’s citizens should be deceived. 預金者が大手銀行を相手取って訴訟を起こすケースも出ており、金融庁は被害者の救済を優先させて、金融機関に犯罪防止対策の強化を促すべきだと判断。（朝日新聞） With cases coming out of depositors suing large banks, the Financial Services Agency decided it should prioritize relief for victims and urge banks to strengthen measures for crime prevention.',
      grammars: ['べき'],
      keywords: ['beki','should'],
    ),
    Section(
      heading: 'Using 「べく」 to describe what one tries to do',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('連用形 【れん・よう・けい】 – conjunctive form'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('準備 【じゅん・び】 – preparations'),
                  Text('する (exception) – to do'),
                  Text('始める 【はじ・める】 (ru-verb) – to begin'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('行う 【おこな・う】 (u-verb) – to conduct, to carry out'),
                  Text('試験 【し・けん】 – exam'),
                  Text('合格 【ごう・かく】 – pass (as in an exam)'),
                  Text('皆 【みんな】 – everybody'),
                  Text('一生懸命 【いっ・しょう・けん・めい】 – with utmost effort'),
                  Text('勉強 【べん・きょう】 – study'),
                  Text('今後 【こん・ご】 – from now on'),
                  Text('お客様 【お・きゃく・さま】 – guest, customer'),
                  Text('対話 【たい・わ】 – interaction'),
                  Text('窓口 【まど・ぐち】 – teller window, counter; point of contact'),
                  Text('より – more'),
                  Text('充実 【じゅう・じつ】 – fulfilled'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('参る 【まい・る】 (u-verb) – to go; to come (humble)'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'Grammatically, 「べく」 is really a conjunctive form （',
                  ),
                  VocabTooltip(
                    message: renyoukei,
                    inlineSpan: const TextSpan(
                      text: '連用形',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '） of 「べき」, similar to what the te-form does to connect another phrase. However, what needs mentioning here is that by changing it into a conjunctive and adding a predicate, the meaning of 「べく」 changes from the ordinary meaning of 「べき」. While 「べき」 describes a strong suggestion, changing it to 「べく」 allows you to describe what one did in order to carry out that suggestion. Take a look that the following examples to see how the meaning changes.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                text: '帰る',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べき',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Should',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: ' go home early.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: TextSpan(
                                text: '帰る',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べく',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: junbi,
                              inlineSpan: const TextSpan(
                                text: '準備',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'し',
                              ),
                            ),
                            VocabTooltip(
                              message: hajimeru,
                              inlineSpan: const TextSpan(
                                text: '始めた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'In trying to',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                                text:
                                    ' go home early, started the preparations.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'As we can see in this example, adding the 「',
                  ),
                  VocabTooltip(
                    message: junbi,
                    inlineSpan: const TextSpan(
                      text: '準備',
                    ),
                  ),
                  const TextSpan(text: 'を'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'し',
                    ),
                  ),
                  VocabTooltip(
                    message: hajimeru,
                    inlineSpan: const TextSpan(
                      text: '始めた',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 tells us what the subject did in order to carry out the action he/she was supposed to do. In this way we can define 「べく」 as meaning, “in order to” or “in an effort to”. Similarly, 「べく」 might mean the Japanese equivalent of 「',
                  ),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'しよう',
                    ),
                  ),
                  const TextSpan(
                    text: 'と',
                  ),
                  VocabTooltip(
                    message: omou,
                    inlineSpan: const TextSpan(
                      text: '思って',
                    ),
                  ),
                  const TextSpan(
                    text: '」 or 「',
                  ),
                  VocabTooltip(
                    message: dekiru,
                    inlineSpan: const TextSpan(
                      text: 'できる',
                    ),
                  ),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(
                    text:
                        'に」. This is a very seldom-used old-fashioned expression and is merely presented here to completely cover all aspects of 「べき」.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「べく」 for actions that are attempted to be done',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「べく」 to the action that is attempted to be done.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Same as 「べき」, you can remove the 「る」 for the generic “to do ” verb 「',
                              ),
                              VocabTooltip(
                                message: suru,
                                inlineSpan: const TextSpan(
                                  text: 'する',
                                ),
                              ),
                              const TextSpan(
                                text: '」 only.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'す',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋',
                                  ),
                                  const TextSpan(
                                    text: 'べく',
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'す',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べく',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: shiken,
                              inlineSpan: const TextSpan(
                                text: '試験',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: goukaku,
                              inlineSpan: const TextSpan(
                                text: '合格',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'す',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べく',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: '皆',
                              ),
                            ),
                            VocabTooltip(
                              message: isshoukenmei,
                              inlineSpan: const TextSpan(
                                text: '一生懸命',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: benkyou,
                              inlineSpan: const TextSpan(
                                text: '勉強',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'している',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'Everybody is studying very hard ',
                            ),
                            TextSpan(
                              text: 'in an effort',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' to pass the exam.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongo,
                              inlineSpan: const TextSpan(
                                text: '今後',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: okyakusama,
                              inlineSpan: const TextSpan(
                                text: 'お客様',
                              ),
                            ),
                            const TextSpan(
                              text: 'との',
                            ),
                            VocabTooltip(
                              message: taiwa,
                              inlineSpan: const TextSpan(
                                text: '対話',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: madoguchi,
                              inlineSpan: const TextSpan(
                                text: '窓口',
                              ),
                            ),
                            const TextSpan(
                              text: 'と',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            const TextSpan(
                              text: '、より',
                            ),
                            VocabTooltip(
                              message: juujitsu,
                              inlineSpan: const TextSpan(
                                text: '充実',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'して',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: 'いく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べく',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: doryoku,
                              inlineSpan: const TextSpan(
                                text: '努力',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'して',
                              ),
                            ),
                            VocabTooltip(
                              message: mairu,
                              inlineSpan: const TextSpan(
                                text: 'まいり',
                              ),
                            ),
                            const TextSpan(
                              text: 'ます。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'We are working from here ',
                            ),
                            TextSpan(
                              text: 'in an effort',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text:
                                  ' to provide a enriched window for customer interaction.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 連用形 【れん・よう・けい】 – conjunctive form 早い 【はや・い】 (i-adj) – fast; early 帰る 【かえ・る】 (u-verb) – to go home 準備 【じゅん・び】 – preparations する (exception) – to do 始める 【はじ・める】 (ru-verb) – to begin 思う 【おも・う】 (u-verb) – to think 出来る 【で・き・る】 (ru-verb) – to be able to do 行う 【おこな・う】 (u-verb) – to conduct, to carry out 試験 【し・けん】 – exam 合格 【ごう・かく】 – pass (as in an exam) 皆 【みんな】 – everybody 一生懸命 【いっ・しょう・けん・めい】 – with utmost effort 勉強 【べん・きょう】 – study 今後 【こん・ご】 – from now on お客様 【お・きゃく・さま】 – guest, customer 対話 【たい・わ】 – interaction 窓口 【まど・ぐち】 – teller window, counter; point of contact より – more 充実 【じゅう・じつ】 – fulfilled 行く 【い・く】 (u-verb) – to go 参る 【まい・る】 (u-verb) – to go; to come (humble) Grammatically, 「べく」 is really a conjunctive form （連用形） of 「べき」, similar to what the te-form does to connect another phrase. However, what needs mentioning here is that by changing it into a conjunctive and adding a predicate, the meaning of 「べく」 changes from the ordinary meaning of 「べき」. While 「べき」 describes a strong suggestion, changing it to 「べく」 allows you to describe what one did in order to carry out that suggestion. Take a look that the following examples to see how the meaning changes. 早く帰るべき。 Should go home early. 早く帰るべく、準備をし始めた。 In trying to go home early, started the preparations. As we can see in this example, adding the 「準備をし始めた」 tells us what the subject did in order to carry out the action he/she was supposed to do. In this way we can define 「べく」 as meaning, “in order to” or “in an effort to”. Similarly, 「べく」 might mean the Japanese equivalent of 「しようと思って」 or 「できるように」. This is a very seldom-used old-fashioned expression and is merely presented here to completely cover all aspects of 「べき」. Using 「べく」 for actions that are attempted to be done Attach 「べく」 to the action that is attempted to be done. Examples: 行う → 行うべく する → するべく Same as 「べき」, you can remove the 「る」 for the generic “to do ” verb 「する」 only. Example: する＋べく → すべく Examples 試験に合格すべく、皆一生懸命に勉強している。 Everybody is studying very hard in an effort to pass the exam. 今後もお客様との対話の窓口として、より充実していくべく努力してまいります。 We are working from here in an effort to provide a enriched window for customer interaction.',
      grammars: ['べく'],
      keywords: ['beku','attempt'],
    ),
    Section(
      heading: 'Using 「べからず」 to describe things one must not do',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('未然形 【み・ぜん・けい】 – imperfective form'),
                  Text('行う 【おこな・う】 (u-verb) – to conduct, to carry out'),
                  Text('する (exception) – to do'),
                  Text('捨てる 【す・てる】 (ru-verb) – to throw away'),
                  Text('安全 【あん・ぜん】 – safety'),
                  Text('措置 【そ・ち】 – measures'),
                  Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'Moving on to yet another from of 「べき」 is 「べからず」. This is undoubtedly related to the 「ず」 negative ending we learned in a previous section. However, it seems to be a conjugation of an old ',
                  ),
                  VocabTooltip(
                    message: mizenkei,
                    inlineSpan: const TextSpan(
                      text: '未然形',
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' of 「べから」. I have no idea what that means and you don’t have to either. The only thing we need to take away from this is that 「べからず」 expresses the opposite meaning of 「べき」 as an action that one must ',
                  ),
                  const TextSpan(
                    text: 'not',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const TextSpan(
                    text:
                        ' do. I suppose the short and abrupt ending of the 「ず」 form makes this more useful for laying out rules. In fact, searching around on google comes up with a bunch of 「べし･ベからず」 or “do’s and don’ts”. （べし is an older form of べき, which I doubt you’ll ever need.)',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「べからず」 for actions that must not be done',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「べからず」 to the action that must not be done.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: okonau,
                                    inlineSpan: const TextSpan(
                                      text: '行う',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べからず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べからず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'Same as 「べき」, you can remove the 「る」 for the generic “to do ” verb 「',
                              ),
                              VocabTooltip(
                                message: suru,
                                inlineSpan: const TextSpan(
                                  text: 'する',
                                ),
                              ),
                              const TextSpan(
                                text: '」 only.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'す',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '＋',
                                  ),
                                  const TextSpan(
                                    text: 'べからず',
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'す',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'べからず',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: gomi,
                              inlineSpan: const TextSpan(
                                text: 'ゴミ',
                              ),
                            ),
                            VocabTooltip(
                              message: suteru,
                              inlineSpan: TextSpan(
                                text: '捨てる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べからず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'You ',
                            ),
                            TextSpan(
                              text: 'must not',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' throw away trash.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: anzen,
                              inlineSpan: const TextSpan(
                                text: '安全',
                              ),
                            ),
                            VocabTooltip(
                              message: sochi,
                              inlineSpan: const TextSpan(
                                text: '措置',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: TextSpan(
                                text: '忘れる',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'べからず',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: 'You ',
                            ),
                            TextSpan(
                              text: 'must not',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: ' forget the safety measures.',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 未然形 【み・ぜん・けい】 – imperfective form 行う 【おこな・う】 (u-verb) – to conduct, to carry out する (exception) – to do 捨てる 【す・てる】 (ru-verb) – to throw away 安全 【あん・ぜん】 – safety 措置 【そ・ち】 – measures 忘れる 【わす・れる】 (ru-verb) – to forget Moving on to yet another from of 「べき」 is 「べからず」. This is undoubtedly related to the 「ず」 negative ending we learned in a previous section. However, it seems to be a conjugation of an old 未然形 of 「べから」. I have no idea what that means and you don’t have to either. The only thing we need to take away from this is that 「べからず」 expresses the opposite meaning of 「べき」 as an action that one must not do. I suppose the short and abrupt ending of the 「ず」 form makes this more useful for laying out rules. In fact, searching around on google comes up with a bunch of 「べし･ベからず」 or “do’s and don’ts”. （べし is an older form of べき, which I doubt you’ll ever need.) Using 「べからず」 for actions that must not be done Attach 「べからず」 to the action that must not be done. Examples: 行う → 行うべからず する → するべからず Same as 「べき」, you can remove the 「る」 for the generic “to do ” verb 「する」 only. Example: する＋べからず → すべからず Examples ゴミ捨てるべからず。 You must not throw away trash. 安全措置を忘れるべからず。 You must not forget the safety measures.',
      grammars: ['べからず'],
      keywords: ['bekarazu','must not'],
    ),
  ],
);
