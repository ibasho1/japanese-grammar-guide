import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson signs = Lesson(
  title: 'Showing Signs of Something',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this lesson, we’ll learn various expressions involving how to describe people who are expressing themselves without words. For example, we’ll learn how to say expressions in Japanese such as “They '),
                  TextSpan(
                    text: 'acted',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(text: ' as if they were saying goodbye,” “He '),
                  TextSpan(
                    text: 'acted',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(text: ' disgusted,” and “She '),
                  TextSpan(
                    text: 'acts',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(text: ' like she wants to go.”'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this lesson, we’ll learn various expressions involving how to describe people who are expressing themselves without words. For example, we’ll learn how to say expressions in Japanese such as “They acted as if they were saying goodbye,” “He acted disgusted,” and “She acts like she wants to go.”',
      grammars: [],
      keywords: ['signs'],
    ),
    Section(
      heading: 'Showing outward signs of an emotion using 「～がる」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('嫌 【いや】 (na-adj) disagreeable; unpleasant'),
                  Text('怖い 【こわ・い】 (i-adj) – scary'),
                  Text('嬉しい 【うれ・しい】 (i-adj) – happy'),
                  Text('恥ずかしい 【は・ずかしい】 (i-adj) – embarrassing'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('する (exception) – to do'),
                  Text('何 【なに／なん】 – what'),
                  Text('いる (ru-verb) – to exist (animate)'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('朝 【あさ】 – morning'),
                  Text('起こす 【お・こす】 (u-verb) – to cause, to wake someone'),
                  Text('タイプ – type'),
                  Text('うち – referring to one’s in-group, i.e. company, etc.'),
                  Text('子供 【こ・ども】 – child'),
                  Text('プール – pool'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('理由 【り・ゆう】 – reason'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('欲しい 【ほ・しい】 (i-adj) – desirable'),
                  Text('カレー – curry'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house'),
                  Text('帰る 【かえ・る】 (u-verb) – to go home'),
                  Text('すぐ – soon'),
                  Text('パソコン – computer, PC'),
                  Text('使う 【つか・う】 (u-verb) – to use'),
                  Text('皆 【みんな】 – everybody'),
                  Text('イタリア – Italy'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('私 【わたし】 – me, myself, I'),
                  Text('予算 【よ・さん】 – budget'),
                  Text('どう – how'),
                  Text('とても – very'),
                  Text('怪しい 【あや・しい】 (i-adj) – suspicious; dubious; doubtful'),
                  Text('妻 【つま】 – wife'),
                  Text('バッグ – bag'),
                  Text('そんな – that sort of'),
                  Text('もん – object （short for もの）'),
                  Text('買う 【か・う】 (u-verb) – to buy'),
                  Text('訳 【わけ】 – meaning; reason; can be deduced'),
                  Text(
                      '恥ずかしがり屋 【は・ずかしがり・や】 – one who easily feels or acts embarrassed'),
                  Text('寒がり屋 【さむ・がり・や】 – one who easily feels cold'),
                  Text('暑がり屋 【あつ・がり・や】 – one who easily feels hot'),
                  Text('ミネソタ – Minnesota'),
                  Text('暮らす 【く・らす】 (u-verb) – to live'),
                  Text('辛い 【つら・い】 (i-adj) – harsh'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The 「～がる」 grammar is used when you want to make an observation about how someone is feeling. This is simply an observation based on some type of sign(s). Therefore, you would not use it for your own emotions since guessing about your own emotions is not necessary. This grammar can only be used with adjectives so you can use this grammar to say, “He is acting scared,” but you cannot say “He acted surprised,” because “to be surprised” is a verb in Japanese and not an adjective. This grammar is also commonly used with a certain set of adjectives related to emotions such as: 「',
                  ),
                  VocabTooltip(
                    message: iya,
                    inlineSpan: const TextSpan(text: '嫌'),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: kowai,
                    inlineSpan: const TextSpan(text: '怖い'),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: ureshii,
                    inlineSpan: const TextSpan(text: '嬉しい'),
                  ),
                  const TextSpan(
                    text: '」、or 「',
                  ),
                  VocabTooltip(
                    message: hazukashii,
                    inlineSpan: const TextSpan(text: '恥ずかしい'),
                  ),
                  const TextSpan(
                    text: '」.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「～がる」 for observing the emotions or feelings of others',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For i-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Remove the last 「い」 from the i-adjective and then attach 「がる」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kowai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '怖',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kowai,
                                    inlineSpan: const TextSpan(
                                      text: '怖',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がる',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For na-adjectives:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Attach 「がる」 to the end of the na-adjective.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iya,
                                    inlineSpan: const TextSpan(
                                      text: '嫌',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iya,
                                    inlineSpan: const TextSpan(
                                      text: '嫌',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がる',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: kDefaultParagraphPadding,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const TableCaption(
                              caption:
                                  'All adjectives that are conjugated with 「～がる」 become an u-verb'),
                          Table(
                            border: TableBorder.all(
                              color: Theme.of(context).colorScheme.outline,
                            ),
                            children: [
                              const TableRow(
                                children: [
                                  TableHeading(
                                    text: ' ',
                                  ),
                                  TableHeading(
                                    text: 'Positive',
                                  ),
                                  TableHeading(
                                    text: '	Negative',
                                  )
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Non-Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: kowai,
                                                inlineSpan: const TextSpan(
                                                  text: '怖',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がる',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('act scared'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: kowai,
                                                inlineSpan: const TextSpan(
                                                  text: '怖',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がらない',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('not act scared'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: kowai,
                                                inlineSpan: const TextSpan(
                                                  text: '怖',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がった',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('acted scared'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: kowai,
                                                inlineSpan: const TextSpan(
                                                  text: '怖',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がらなかった',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('didn’t act scared'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きて',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ！',
                            ),
                            VocabTooltip(
                              message: nani,
                              inlineSpan: const TextSpan(
                                text: '何',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: hazukashii,
                              inlineSpan: TextSpan(
                                text: '恥ずかし',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がっている',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'の？',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Hurry up and come here. What are you acting all embarrassed for?',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: asa,
                              inlineSpan: const TextSpan(
                                text: '朝',
                              ),
                            ),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: const TextSpan(
                                text: '早く',
                              ),
                            ),
                            VocabTooltip(
                              message: okosu,
                              inlineSpan: const TextSpan(
                                text: '起こされる',
                              ),
                            ),
                            const TextSpan(
                              text: 'のを',
                            ),
                            VocabTooltip(
                              message: iya,
                              inlineSpan: TextSpan(
                                text: '嫌',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がる',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: taipu,
                              inlineSpan: const TextSpan(
                                text: 'タイプ',
                              ),
                            ),
                            const TextSpan(
                              text: 'です。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My girlfriend is the type to show dislike towards getting woken up early in the morning.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: uchi,
                              inlineSpan: const TextSpan(
                                text: 'うち',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: kodomo,
                              inlineSpan: const TextSpan(
                                text: '子供',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: puuru,
                              inlineSpan: const TextSpan(
                                text: 'プール',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入る',
                              ),
                            ),
                            const TextSpan(
                              text: 'のを',
                            ),
                            VocabTooltip(
                              message: riyuu,
                              inlineSpan: const TextSpan(
                                text: '理由',
                              ),
                            ),
                            const TextSpan(
                              text: 'も',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'なく',
                              ),
                            ),
                            VocabTooltip(
                              message: kowai,
                              inlineSpan: TextSpan(
                                text: '怖',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がる',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Our child acts afraid about entering a pool without any reason.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'This grammar is also used to observe very frankly on what you think someone other than yourself wants. This involves the adjective 「',
                  ),
                  VocabTooltip(
                    message: hoshii,
                    inlineSpan: const TextSpan(text: '欲しい'),
                  ),
                  const TextSpan(
                    text:
                        '」 for things one wants or the 「～たい」 conjugation for actions one wants to do, which is essentially a verb conjugated to an i-adjective. This type of grammar is more suited for things like narration in a story and is rarely used in this fashion for normal conversations because of its impersonal style of observation. For casual conversations, it is more common to use 「でしょう」 such as in, 「',
                  ),
                  VocabTooltip(
                    message: karee,
                    inlineSpan: const TextSpan(text: 'カレー'),
                  ),
                  const TextSpan(
                    text: 'を',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(text: '食べたい'),
                  ),
                  const TextSpan(
                    text:
                        'でしょう。」. For polite conversations, it is normal to not make any assumptions at all or to use the 「よね」 sentence ending such as in 「',
                  ),
                  VocabTooltip(
                    message: karee,
                    inlineSpan: const TextSpan(text: 'カレー'),
                  ),
                  const TextSpan(
                    text: 'を',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(text: '食べたい'),
                  ),
                  const TextSpan(
                    text: 'ですか。」 or 「',
                  ),
                  VocabTooltip(
                    message: karee,
                    inlineSpan: const TextSpan(text: 'カレー'),
                  ),
                  const TextSpan(
                    text: 'を',
                  ),
                  VocabTooltip(
                    message: taberu,
                    inlineSpan: const TextSpan(text: '食べたい'),
                  ),
                  const TextSpan(
                    text: 'ですよね。」',
                  ),
                ],
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ie,
                              inlineSpan: const TextSpan(
                                text: '家',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kaeru,
                              inlineSpan: const TextSpan(
                                text: '帰ったら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sugu,
                              inlineSpan: const TextSpan(
                                text: 'すぐ',
                              ),
                            ),
                            VocabTooltip(
                              message: pasokon,
                              inlineSpan: const TextSpan(
                                text: 'パソコン',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: tsukau,
                              inlineSpan: TextSpan(
                                text: '使いた',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がる',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        '(He) soon acts like wanting to use computer as soon as (he) gets home.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            VocabTooltip(
                              message: itaria,
                              inlineSpan: const TextSpan(
                                text: 'イタリア',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: TextSpan(
                                text: '行きた',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'がってる',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'んだけど、',
                            ),
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: yosan,
                              inlineSpan: const TextSpan(
                                text: '予算',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行ける',
                              ),
                            ),
                            const TextSpan(
                              text: 'かどうかは',
                            ),
                            VocabTooltip(
                              message: totemo,
                              inlineSpan: const TextSpan(
                                text: 'とても',
                              ),
                            ),
                            VocabTooltip(
                              message: ayashii,
                              inlineSpan: const TextSpan(
                                text: '怪しい',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Everybody is acting like they want to go to Italy but it’s suspicious whether I can go or not going by my budget.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: tsuma,
                              inlineSpan: const TextSpan(
                                text: '妻',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: ruiviton,
                              inlineSpan: const TextSpan(
                                text: 'ルイヴィトン',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: baggu,
                              inlineSpan: const TextSpan(
                                text: 'バッグ',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: hoshii,
                              inlineSpan: TextSpan(
                                text: '欲し',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がっている',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'んだけど、',
                            ),
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: mon,
                              inlineSpan: const TextSpan(
                                text: 'もん',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: kau,
                              inlineSpan: const TextSpan(
                                text: '買える',
                              ),
                            ),
                            const TextSpan(
                              text: 'わけ',
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: 'でしょう！',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'My wife was showing signs of wanting a Louis Vuitton bag but there’s no way I can buy something like that!',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        '「～がる」 is also used with 「屋」 to indicate a type of person that often feels a certain way such as 「',
                  ),
                  VocabTooltip(
                    message: hazukashihgariya,
                    inlineSpan: const TextSpan(text: '恥ずかしがり屋'),
                  ),
                  const TextSpan(
                    text: '」 (one who easily feels or acts embarrassed)、 「',
                  ),
                  VocabTooltip(
                    message: samugariya,
                    inlineSpan: const TextSpan(text: '寒がり屋'),
                  ),
                  const TextSpan(
                    text: '」 (one who easily feels cold)、or 「',
                  ),
                  VocabTooltip(
                    message: atsugariya,
                    inlineSpan: const TextSpan(text: '暑がり屋'),
                  ),
                  const TextSpan(
                    text: '」 (one who easily feels hot).',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: watashi,
                              inlineSpan: const TextSpan(
                                text: '私',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: samugariya,
                              inlineSpan: TextSpan(
                                text: '寒がり屋',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'だから、',
                            ),
                            VocabTooltip(
                              message: minesota,
                              inlineSpan: const TextSpan(
                                text: 'ミネソタ',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: kurasu,
                              inlineSpan: const TextSpan(
                                text: '暮らす',
                              ),
                            ),
                            const TextSpan(
                              text: 'のは',
                            ),
                            VocabTooltip(
                              message: karai,
                              inlineSpan: const TextSpan(
                                text: '辛かった',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'I’m the type who easily gets cold and so living in Minnesota was painful.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 嫌 【いや】 (na-adj) disagreeable; unpleasant 怖い 【こわ・い】 (i-adj) – scary 嬉しい 【うれ・しい】 (i-adj) – happy 恥ずかしい 【は・ずかしい】 (i-adj) – embarrassing 早い 【はや・い】 (i-adj) – fast; early する (exception) – to do 何 【なに／なん】 – what いる (ru-verb) – to exist (animate) 彼女 【かの・じょ】 – she; girlfriend 朝 【あさ】 – morning 起こす 【お・こす】 (u-verb) – to cause, to wake someone タイプ – type うち – referring to one’s in-group, i.e. company, etc. 子供 【こ・ども】 – child プール – pool 入る 【はい・る】 (u-verb) – to enter 理由 【り・ゆう】 – reason ある (u-verb) – to exist (inanimate) 欲しい 【ほ・しい】 (i-adj) – desirable カレー – curry 食べる 【た・べる】 (ru-verb) – to eat 家 【1) うち; 2) いえ】 – 1) one’s own home; 2) house 帰る 【かえ・る】 (u-verb) – to go home すぐ – soon パソコン – computer, PC 使う 【つか・う】 (u-verb) – to use 皆 【みんな】 – everybody イタリア – Italy 行く 【い・く】 (u-verb) – to go 私 【わたし】 – me, myself, I 予算 【よ・さん】 – budget どう – how とても – very 怪しい 【あや・しい】 (i-adj) – suspicious; dubious; doubtful 妻 【つま】 – wife バッグ – bag そんな – that sort of もん – object （short for もの） 買う 【か・う】 (u-verb) – to buy 訳 【わけ】 – meaning; reason; can be deduced 恥ずかしがり屋 【は・ずかしがり・や】 – one who easily feels or acts embarrassed 寒がり屋 【さむ・がり・や】 – one who easily feels cold 暑がり屋 【あつ・がり・や】 – one who easily feels hot ミネソタ – Minnesota 暮らす 【く・らす】 (u-verb) – to live 辛い 【つら・い】 (i-adj) – harsh The 「～がる」 grammar is used when you want to make an observation about how someone is feeling. This is simply an observation based on some type of sign(s). Therefore, you would not use it for your own emotions since guessing about your own emotions is not necessary. This grammar can only be used with adjectives so you can use this grammar to say, “He is acting scared,” but you cannot say “He acted surprised,” because “to be surprised” is a verb in Japanese and not an adjective. This grammar is also commonly used with a certain set of adjectives related to emotions such as: 「嫌」、「怖い」、「嬉しい」、or 「恥ずかしい」. Using 「～がる」 for observing the emotions or feelings of others For i-adjectives: Remove the last 「い」 from the i-adjective and then attach 「がる」. Example: 怖い → 怖がる For na-adjectives: Attach 「がる」 to the end of the na-adjective. Example: 嫌 → 嫌がる Examples 早くきてよ！ 何を恥ずかしがっているの？ Hurry up and come here. What are you acting all embarrassed for? 彼女は朝早く起こされるのを嫌がるタイプです。 My girlfriend is the type to show dislike towards getting woken up early in the morning. うちの子供はプールに入るのを理由もなく怖がる。 Our child acts afraid about entering a pool without any reason. This grammar is also used to observe very frankly on what you think someone other than yourself wants. This involves the adjective 「欲しい」 for things one wants or the 「～たい」 conjugation for actions one wants to do, which is essentially a verb conjugated to an i-adjective. This type of grammar is more suited for things like narration in a story and is rarely used in this fashion for normal conversations because of its impersonal style of observation. For casual conversations, it is more common to use 「でしょう」 such as in, 「カレーを食べたいでしょう。」. For polite conversations, it is normal to not make any assumptions at all or to use the 「よね」 sentence ending such as in 「カレーを食べたいですか。」 or 「カレーを食べたいですよね。」 Examples 家に帰ったら、すぐパソコンを使いたがる。(He) soon acts like wanting to use computer as soon as (he) gets home. みんなイタリアに行きたがってるんだけど、私の予算で行けるかどうかはとても怪しい。 Everybody is acting like they want to go to Italy but it’s suspicious whether I can go or not going by my budget. 妻はルイヴィトンのバッグを欲しがっているんだけど、そんなもん、買えるわけないでしょう！ My wife was showing signs of wanting a Louis Vuitton bag but there’s no way I can buy something like that! 「～がる」 is also used with 「屋」 to indicate a type of person that often feels a certain way such as 「恥ずかしがり」 (one who easily feels or acts embarrassed)、 「屋寒がり屋」 (one who easily feels cold)、or 「暑がり屋」 (one who easily feels hot). 私は寒がり屋だから、ミネソタで暮らすのは辛かった。 I’m the type who easily gets cold and so living in Minnesota was painful.',
      grammars: ['～がる'],
      keywords: ['garu','adjective','emotion','feeling'],
    ),
    Section(
      heading: 'Using 「ばかり」 to act as if one might do something',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('ボール – ball'),
                  Text('爆発 【ばく・はつ】 – explosion'),
                  Text('する (exception) – to do'),
                  Text('膨らむ 【ふく・らむ】 (u-verb) – to expand; to swell'),
                  Text('あんた – you (slang)'),
                  Text('関係 【かん・けい】 – relation, relationship'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('彼女 【かの・じょ】 – she; girlfriend'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('無視 【む・し】 – ignore'),
                  Text('昨日【きのう】 – yesterday'),
                  Text('喧嘩 【けん・か】 – quarrel'),
                  Text('何 【なに／なん】 – what'),
                  Text('平気 【へい・き】 (na-adj) – coolness; calmness'),
                  Text('顔 【かお】 – face'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We just learned how to observe the emotions and feelings of other by using 「～がる」 with adjectives. But what about verbs? Indeed, there is a separate grammar used to express the fact that someone else looks like they are about to do something but actually does not. Similar to the 「～がる」 grammar, this is usually not used in normal everyday conversations. I have seen it several times in books and novels but have yet to hear this grammar in a conversation.',
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For the regular non-past, non-negative verb, you must first conjugate the verb to the negative ending with 「ん」, which was covered here. Then, you just attach 「ばかり」 to the end of the verb. For all other conjugations, nothing else is necessary except to just add 「ばかり」 to the verb. The most common verb used with this grammar is 「',
                  ),
                  VocabTooltip(
                    message: iu,
                    inlineSpan: const TextSpan(
                      text: '言う',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 . It is also usually used with the 「に」 target particle attached to the end of 「ばかり」.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This grammar is completely different from the 「ばかり」 used to express amounts and the 「ばかり」 used to express the proximity of an action. ',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「ばかり」 to indicate that one seems to want to do something',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For present, non-negative:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text:
                                    ' Conjugate the verb to the 「ん」 negative form and attach 「ばかり」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言',
                                        ),
                                        TextSpan(
                                          text: 'う',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言わ',
                                        ),
                                        TextSpan(
                                          text: 'ない',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '言わ',
                                        ),
                                        TextSpan(
                                          text: 'ん',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言わん',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ばかり',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For all other tenses:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach 「ばかり」 to the end of the verb.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言わなかった',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iu,
                                    inlineSpan: const TextSpan(
                                      text: '言わなかった',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'ばかり',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: kDefaultParagraphPadding,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const TableCaption(
                              caption: 'Summary of basic conjugations'),
                          Table(
                            border: TableBorder.all(
                              color: Theme.of(context).colorScheme.outline,
                            ),
                            children: [
                              const TableRow(
                                children: [
                                  TableHeading(
                                    text: ' ',
                                  ),
                                  TableHeading(
                                    text: 'Positive',
                                  ),
                                  TableHeading(
                                    text: '	Negative',
                                  )
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Non-Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: iu,
                                                inlineSpan: const TextSpan(
                                                  text: '言わん',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'ばかり',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('as if to say'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: iu,
                                                inlineSpan: const TextSpan(
                                                  text: '言わない',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'ばかり',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('as if [she] doesn’t say'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: iu,
                                                inlineSpan: const TextSpan(
                                                  text: '言った',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'ばかり',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('as if [she] said'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: iu,
                                                inlineSpan: const TextSpan(
                                                  text: '言わなかった',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'ばかり',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('as if [she] didn’t say'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: booru,
                              inlineSpan: const TextSpan(
                                text: 'ボール',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: bakuhatsu,
                              inlineSpan: const TextSpan(
                                text: '爆発',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'せん',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'に、',
                            ),
                            VocabTooltip(
                              message: fukuramu,
                              inlineSpan: const TextSpan(
                                text: '膨らんでいた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'The ball was expanding as if it was going to explode.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                              text: '「',
                            ),
                            VocabTooltip(
                              message: anta,
                              inlineSpan: const TextSpan(
                                text: 'あんた',
                              ),
                            ),
                            const TextSpan(
                              text: 'とは',
                            ),
                            VocabTooltip(
                              message: kankei,
                              inlineSpan: const TextSpan(
                                text: '関係',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(
                              text: '」と',
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言わん',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: kanojo,
                              inlineSpan: const TextSpan(
                                text: '彼女',
                              ),
                            ),
                            const TextSpan(
                              text: 'は',
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: mushi,
                              inlineSpan: const TextSpan(
                                text: '無視',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'していた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'She ignored him as if to say, “You have nothing to do with this.”',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kinou,
                              inlineSpan: const TextSpan(
                                text: '昨日',
                              ),
                            ),
                            const TextSpan(
                              text: 'の',
                            ),
                            VocabTooltip(
                              message: kenka,
                              inlineSpan: const TextSpan(
                                text: '喧嘩',
                              ),
                            ),
                            const TextSpan(
                              text: 'で',
                            ),
                            VocabTooltip(
                              message: nanimo,
                              inlineSpan: const TextSpan(
                                text: '何も',
                              ),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: TextSpan(
                                text: '言わなかった',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'ばかり',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                              text: 'に、',
                            ),
                            VocabTooltip(
                              message: heiki,
                              inlineSpan: const TextSpan(
                                text: '平気',
                              ),
                            ),
                            const TextSpan(
                              text: 'な',
                            ),
                            VocabTooltip(
                              message: kao,
                              inlineSpan: const TextSpan(
                                text: '顔',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'している',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Has a calm face as if [he] didn’t say anything during the fight yesterday.',
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 言う 【い・う】 (u-verb) – to say ボール – ball 爆発 【ばく・はつ】 – explosion する (exception) – to do 膨らむ 【ふく・らむ】 (u-verb) – to expand; to swell あんた – you (slang) 関係 【かん・けい】 – relation, relationship ある (u-verb) – to exist (inanimate) 彼女 【かの・じょ】 – she; girlfriend 彼 【かれ】 – he; boyfriend 無視 【む・し】 – ignore 昨日【きのう】 – yesterday 喧嘩 【けん・か】 – quarrel 何 【なに／なん】 – what 平気 【へい・き】 (na-adj) – coolness; calmness 顔 【かお】 – face We just learned how to observe the emotions and feelings of other by using 「～がる」 with adjectives. But what about verbs? Indeed, there is a separate grammar used to express the fact that someone else looks like they are about to do something but actually does not. Similar to the 「～がる」 grammar, this is usually not used in normal everyday conversations. I have seen it several times in books and novels but have yet to hear this grammar in a conversation. For the regular non-past, non-negative verb, you must first conjugate the verb to the negative ending with 「ん」, which was covered here. Then, you just attach 「ばかり」 to the end of the verb. For all other conjugations, nothing else is necessary except to just add 「ばかり」 to the verb. The most common verb used with this grammar is 「言う」 . It is also usually used with the 「に」 target particle attached to the end of 「ばかり」. This grammar is completely different from the 「ばかり」 used to express amounts and the 「ばかり」 used to express the proximity of an action. Using 「ばかり」 to indicate that one seems to want to do something For present, non-negative: Conjugate the verb to the 「ん」 negative form and attach 「ばかり」. Example: 言う → 言わない → 言わん → 言わんばかり For all other tenses: Attach 「ばかり」 to the end of the verb. Example: 言わなかった → 言わなかったばかり Examples ボールは爆発せんばかりに、膨らんでいた。 The ball was expanding as if it was going to explode. 「あんたとは関係ない」と言わんばかりに彼女は彼を無視していた。 She ignored him as if to say, “You have nothing to do with this.”昨日の喧嘩で何も言わなかったばかりに、平気な顔をしている。 Has a calm face as if [he] didn’t say anything during the fight yesterday.',
      grammars: ['ばかり'],
      keywords: ['bakari','might','as if','as though','verb'],
    ),
    Section(
      heading: 'Using 「めく」 to indicate an atmosphere of a state',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('謎 【なぞ】 – puzzle'),
                  Text('秘密 【ひ・みつ】 – secret'),
                  Text('皮肉 【ひ・にく】 – irony'),
                  Text('紅葉 【こう・よう】 – leaves changing color'),
                  Text('始まる 【はじ・まる】 (u-verb) – to begin'),
                  Text('すっかり – completely'),
                  Text('秋 【あき】 – autumn'),
                  Text('空気 【くう・き】 – air; atmosphere'),
                  Text('なる (u-verb) – to become'),
                  Text('そんな – that sort of'),
                  Text('顔 【かお】 – face'),
                  Text('する (exception) – to do'),
                  Text('うまい (i-adj) – skillful; delicious'),
                  Text('説明 【せつ・めい】 – explanation'),
                  Text('出来る 【で・き・る】 (ru-verb) – to be able to do'),
                  Text('いつも – always'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('～方 【～かた】 – way of doing ～'),
                  Text('皆 【みんな】 – everybody'),
                  Text('嫌 【いや】 (na-adj) disagreeable; unpleasant'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'By now, you’re probably thinking, “Ok, we’ve done adjectives and verbs. What about nouns?” As a matter of fact, there is a similar grammar that is used usually for nouns and na-adjectives. It is used to indicate that something is showing the signs of a certain state. Unlike the 「～がる」 grammar, there is no action that indicates anything; merely the atmosphere gives off the impression of the state. Just like the previous grammar we learned in this section, this grammar has a list of commonly used nouns such as 「',
                  ),
                  VocabTooltip(
                    message: nazo,
                    inlineSpan: const TextSpan(
                      text: '謎',
                    ),
                  ),
                  const TextSpan(
                    text: '」、「',
                  ),
                  VocabTooltip(
                    message: himitsu,
                    inlineSpan: const TextSpan(
                      text: '秘密',
                    ),
                  ),
                  const TextSpan(
                    text: '」、or 「',
                  ),
                  VocabTooltip(
                    message: hiniku,
                    inlineSpan: const TextSpan(
                      text: '皮肉',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. This grammar is used by simply attaching 「めく」 to the noun or na-adjective. The result then becomes a regular u-verb.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「めく」 to indicate that one seems to want to do something',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「めく」 to the noun or na-adjective. The result then becomes a regular u-verb.',
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: nazo,
                                    inlineSpan: const TextSpan(
                                      text: '謎',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: nazo,
                                    inlineSpan: const TextSpan(
                                      text: '謎',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'めく',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: kDefaultParagraphPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const TableCaption(
                                  caption: 'Summary of basic conjugations'),
                              Table(
                                border: TableBorder.all(
                                  color: Theme.of(context).colorScheme.outline,
                                ),
                                children: [
                                  const TableRow(
                                    children: [
                                      TableHeading(
                                        text: ' ',
                                      ),
                                      TableHeading(
                                        text: 'Positive',
                                      ),
                                      TableHeading(
                                        text: '	Negative',
                                      )
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Non-Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: nazo,
                                                    inlineSpan: const TextSpan(
                                                      text: '謎',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'めく',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('puzzling atmosphere'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  const TextSpan(
                                                    text: '*',
                                                  ),
                                                  VocabTooltip(
                                                    message: nazo,
                                                    inlineSpan: const TextSpan(
                                                      text: '謎',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'めかない',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text(
                                                'not puzzling atmosphere'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: [
                                      const TableHeading(
                                        text: 'Past',
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  VocabTooltip(
                                                    message: nazo,
                                                    inlineSpan: const TextSpan(
                                                      text: '謎',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'めいた',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text('puzzled atmosphere'),
                                          ],
                                        ),
                                      ),
                                      TableData(
                                        centered: false,
                                        content: Column(
                                          children: [
                                            Text.rich(
                                              TextSpan(
                                                children: [
                                                  const TextSpan(
                                                    text: '*',
                                                  ),
                                                  VocabTooltip(
                                                    message: nazo,
                                                    inlineSpan: const TextSpan(
                                                      text: '謎',
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'めかなかった',
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const Text(
                                                'not puzzled atmosphere'),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const Text(
                      '*The negatives conjugations are theoretically possible but are not likely used. The most common usage is the past tense.',
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kouyou,
                              inlineSpan: const TextSpan(
                                text: '紅葉',
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: hajimaru,
                              inlineSpan: const TextSpan(
                                text: '始まり',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: sukkari,
                              inlineSpan: const TextSpan(
                                text: 'すっかり',
                              ),
                            ),
                            VocabTooltip(
                              message: aki,
                              inlineSpan: TextSpan(
                                text: '秋',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'めいた',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kuuki,
                              inlineSpan: const TextSpan(
                                text: '空気',
                              ),
                            ),
                            const TextSpan(
                              text: 'に',
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なって',
                              ),
                            ),
                            VocabTooltip(
                              message: kuru,
                              inlineSpan: const TextSpan(
                                text: 'きた',
                              ),
                            ),
                            const TextSpan(
                              text: '。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'With the leaves starting to change color, the air came to become quite autumn like.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: nazo,
                              inlineSpan: TextSpan(
                                text: '謎',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'めいた',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: kao,
                              inlineSpan: const TextSpan(
                                text: '顔',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'されて',
                              ),
                            ),
                            const TextSpan(
                              text: 'も、',
                            ),
                            VocabTooltip(
                              message: umai,
                              inlineSpan: const TextSpan(
                                text: 'うまく',
                              ),
                            ),
                            VocabTooltip(
                              message: setsumei,
                              inlineSpan: const TextSpan(
                                text: '説明',
                              ),
                            ),
                            VocabTooltip(
                              message: dekiru,
                              inlineSpan: const TextSpan(
                                text: 'できない',
                              ),
                            ),
                            const TextSpan(
                              text: 'よ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'Even having that kind of puzzled look done to me, I can’t explain it very well, you know.',
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: itsumo,
                              inlineSpan: const TextSpan(
                                text: 'いつも',
                              ),
                            ),
                            VocabTooltip(
                              message: hiniku,
                              inlineSpan: TextSpan(
                                text: '皮肉',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'めいた',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: '言い',
                              ),
                            ),
                            VocabTooltip(
                              message: kataWay,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'したら',
                              ),
                            ),
                            const TextSpan(
                              text: '、',
                            ),
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(
                              text: 'を',
                            ),
                            VocabTooltip(
                              message: iya,
                              inlineSpan: const TextSpan(
                                text: '嫌',
                              ),
                            ),
                            const TextSpan(
                              text: 'がらせるよ。',
                            ),
                          ],
                        ),
                      ),
                      const Text(
                        'You’ll make everyone dislike you if you keep speaking with that ironic tone, you know.',
                      ),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'For a whole slew of additional real world examples, check out the ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'jeKai entry',
                    ),
                    uri:
                        'https://web.archive.org/web/20111021175935/http://www.jekai.org/entries/aa/00/np/aa00np21.htm',
                  ),
                  const TextSpan(
                    text:
                        '. It states that the grammar can be used for adverbs and other parts of speech but none of the numerous examples show this and even assuming it’s possible, it’s probably not practiced in reality.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 謎 【なぞ】 – puzzle 秘密 【ひ・みつ】 – secret 皮肉 【ひ・にく】 – irony 紅葉 【こう・よう】 – leaves changing color 始まる 【はじ・まる】 (u-verb) – to begin すっかり – completely 秋 【あき】 – autumn 空気 【くう・き】 – air; atmosphere なる (u-verb) – to become そんな – that sort of 顔 【かお】 – face する (exception) – to do うまい (i-adj) – skillful; delicious 説明 【せつ・めい】 – explanation 出来る 【で・き・る】 (ru-verb) – to be able to do いつも – always 言う 【い・う】 (u-verb) – to say～方 【～かた】 – way of doing ～皆 【みんな】 – everybody 嫌 【いや】 (na-adj) disagreeable; unpleasant By now, you’re probably thinking, “Ok, we’ve done adjectives and verbs. What about nouns?” As a matter of fact, there is a similar grammar that is used usually for nouns and na-adjectives. It is used to indicate that something is showing the signs of a certain state. Unlike the 「～がる」 grammar, there is no action that indicates anything; merely the atmosphere gives off the impression of the state. Just like the previous grammar we learned in this section, this grammar has a list of commonly used nouns such as 「謎」、「秘密」、or 「皮肉」. This grammar is used by simply attaching 「めく」 to the noun or na-adjective. The result then becomes a regular u-verb. Using 「めく」 to indicate that one seems to want to do something Attach 「めく」 to the noun or na-adjective. The result then becomes a regular u-verb. Example: 謎 → 謎めく *The negatives conjugations are theoretically possible but are not likely used. The most common usage is the past tense. Examples 紅葉が始まり、すっかり秋めいた空気になってきた。 With the leaves starting to change color, the air came to become quite autumn like. そんな謎めいた顔をされても、うまく説明できないよ。 Even having that kind of puzzled look done to me, I can’t explain it very well, you know. いつも皮肉めいた言い方をしたら、みんなを嫌がらせるよ。 You’ll make everyone dislike you if you keep speaking with that ironic tone, you know. For a whole slew of additional real world examples, check out the jeKai entry. It states that the grammar can be used for adverbs and other parts of speech but none of the numerous examples show this and even assuming it’s possible, it’s probably not practiced in reality.',
      grammars: ['めく'],
      keywords: ['meku','atmosphere'],
    ),
  ],
);
