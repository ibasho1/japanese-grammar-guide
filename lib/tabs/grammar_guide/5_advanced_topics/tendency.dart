import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/table_caption.dart';
import 'package:japanese_grammar_guide/customs/table_data.dart';
import 'package:japanese_grammar_guide/customs/table_heading.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson tendency = Lesson(
  title: 'Tendencies',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'In this lesson, we will go over various types of grammar that deal with tendencies. Like much of the Advanced Section, all the grammar in this lesson are used mostly in written works and are generally not used in conversational Japanese.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'In this lesson, we will go over various types of grammar that deal with tendencies. Like much of the Advanced Section, all the grammar in this lesson are used mostly in written works and are generally not used in conversational Japanese.',
      grammars: [],
      keywords: ['tendency'],
    ),
    Section(
      heading: 'Saying something is prone to occur using 「～がち」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('なる (u-verb) – to become'),
                  Text('病気 【びょう・き】 – disease; sickness'),
                  Text('確定 【かく・てい】 – decision; settlement'),
                  Text('申告 【しん・こく】 – report; statement; filing a return'),
                  Text('確定申告 【かく・てい・しん・こく】 – final income tax return'),
                  Text('忘れる 【わす・れる】 (ru-verb) – to forget'),
                  Text('手続 【て・つづき】 – procedure, paperwork'),
                  Text('留守 【るす】 – being away from home'),
                  Text('家庭 【か・てい】 – household'),
                  Text('犬 【いぬ】 – dog'),
                  Text('猫 【ねこ】 – cat'),
                  Text('勧め 【すす・め】 – recommendation'),
                  Text('父親 【ちち・おや】 – father'),
                  Text('皆 【みんな】 – everybody'),
                  Text('心配 【しん・ぱい】 – worry; concern'),
                  Text('する (exception) – to do'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'This is arguably the most useful grammar in this lesson in terms of practically. By that, I mean that it’s the only grammar here that you might actually hear in a regular conversation though again, it is far more common in a written context.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'With this grammar, you can say that something is likely to occur by simply attaching 「がち」 to the stem of the verb. While, 「がち」 is a suffix, it works in much same way as a noun or na-adjective. In other words, the result becomes a description of something as being likely. This means that we can do things like modifying nouns by attaching 「な」 and other things we’re used to doing with na-adjectives. You can also say that something is prone to ',
                  ),
                  TextSpan(
                    text: 'be',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  TextSpan(
                    text: ' something by attaching 「がち」 to the noun.',
                  ),
                ],
              ),
              NotesSection(
                heading:
                    'Using 「～がち」 as a description of an action prone to occur',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach 「がち」 to the stem of the verb.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がち',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なり',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がち',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For nouns:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach 「がち」 to the appropriate noun.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: byouki,
                                    inlineSpan: const TextSpan(
                                      text: '病気',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: byouki,
                                    inlineSpan: const TextSpan(
                                      text: '病気',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'がち',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: kDefaultParagraphPadding,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const TableCaption(
                              caption:
                                  'All adjectives that are conjugated with 「～がち」 become a noun/na-adjective'),
                          Table(
                            border: TableBorder.all(
                              color: Theme.of(context).colorScheme.outline,
                            ),
                            children: [
                              const TableRow(
                                children: [
                                  TableHeading(
                                    text: ' ',
                                  ),
                                  TableHeading(
                                    text: 'Positive',
                                  ),
                                  TableHeading(
                                    text: '	Negative',
                                  )
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Non-Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: naru,
                                                inlineSpan: const TextSpan(
                                                  text: 'なり',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がち',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('prone to become'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: naru,
                                                inlineSpan: const TextSpan(
                                                  text: 'なり',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がちじゃない',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('is not prone to become'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const TableHeading(
                                    text: 'Past',
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: naru,
                                                inlineSpan: const TextSpan(
                                                  text: 'なり',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がちだった',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('was prone to become'),
                                      ],
                                    ),
                                  ),
                                  TableData(
                                    centered: false,
                                    content: Column(
                                      children: [
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              VocabTooltip(
                                                message: naru,
                                                inlineSpan: const TextSpan(
                                                  text: 'なり',
                                                ),
                                              ),
                                              const TextSpan(
                                                text: 'がちじゃなかった',
                                              ),
                                            ],
                                          ),
                                        ),
                                        const Text('was not prone to become'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kakutei,
                              inlineSpan: const TextSpan(
                                text: '確定',
                              ),
                            ),
                            VocabTooltip(
                              message: shinkoku,
                              inlineSpan: const TextSpan(
                                text: '申告',
                              ),
                            ),
                            VocabTooltip(
                              message: wasureru,
                              inlineSpan: TextSpan(
                                text: '忘れ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がち',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: tetsudzuki,
                              inlineSpan: const TextSpan(
                                text: '手続',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hitotsu,
                              inlineSpan: const TextSpan(
                                text: 'ひとつ',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Filing income taxes is one of those processes that one is prone to forget.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: rusu,
                              inlineSpan: TextSpan(
                                text: '留守',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がち',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'なご'),
                            VocabTooltip(
                              message: katei,
                              inlineSpan: const TextSpan(
                                text: '家庭',
                              ),
                            ),
                            const TextSpan(text: 'には、'),
                            VocabTooltip(
                              message: inu,
                              inlineSpan: const TextSpan(
                                text: '犬',
                              ),
                            ),
                            const TextSpan(text: 'よりも、'),
                            VocabTooltip(
                              message: neko,
                              inlineSpan: const TextSpan(
                                text: '猫',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hou,
                              inlineSpan: const TextSpan(
                                text: '方',
                              ),
                            ),
                            const TextSpan(text: 'がお'),
                            VocabTooltip(
                              message: susume,
                              inlineSpan: const TextSpan(
                                text: 'すすめ',
                              ),
                            ),
                            const TextSpan(text: 'です。'),
                          ],
                        ),
                      ),
                      const Text(
                          'For families that tend to be away from home, cats are recommended over dogs.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: chichioya,
                              inlineSpan: const TextSpan(
                                text: '父親',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: byouki,
                              inlineSpan: TextSpan(
                                text: '病気',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'がち',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: minna,
                              inlineSpan: const TextSpan(
                                text: 'みんな',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: shinpai,
                              inlineSpan: const TextSpan(
                                text: '心配',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'している',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Father is prone to illness and everybody is worried.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'For more examples, check out the ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'WWWJDIC examples',
                    ),
                    uri:
                        'http://nihongo.monash.edu/cgi-bin/wwwjdic?1Q%BE%A1%A4%C1_0_%A4%AC%A4%C1_',
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見る 【み・る】 (ru-verb) – to see なる (u-verb) – to become 病気 【びょう・き】 – disease; sickness 確定 【かく・てい】 – decision; settlement 申告 【しん・こく】 – report; statement; filing a return 確定申告 【かく・てい・しん・こく】 – final income tax return 忘れる 【わす・れる】 (ru-verb) – to forget 手続 【て・つづき】 – procedure, paperwork 留守 【るす】 – being away from home 家庭 【か・てい】 – household 犬 【いぬ】 – dog 猫 【ねこ】 – cat 勧め 【すす・め】 – recommendation 父親 【ちち・おや】 – father 皆 【みんな】 – everybody 心配 【しん・ぱい】 – worry; concern する (exception) – to do This is arguably the most useful grammar in this lesson in terms of practically. By that, I mean that it’s the only grammar here that you might actually hear in a regular conversation though again, it is far more common in a written context. With this grammar, you can say that something is likely to occur by simply attaching 「がち」 to the stem of the verb. While, 「がち」 is a suffix, it works in much same way as a noun or na-adjective. In other words, the result becomes a description of something as being likely. This means that we can do things like modifying nouns by attaching 「な」 and other things we’re used to doing with na-adjectives. You can also say that something is prone to be something by attaching 「がち」 to the noun. Using 「～がち」 as a description of an action prone to occur For verbs: Attach 「がち」 to the stem of the verb. Examples: 見る → 見がち なる → なり → なりがち For nouns: Attach 「がち」 to the appropriate noun. Example: 病気 → 病気がち Examples 確定申告忘れがちな手続のひとつだ。 Filing income taxes is one of those processes that one is prone to forget. 留守がちなご家庭には、犬よりも、猫の方がおすすめです。 For families that tend to be away from home, cats are recommended over dogs. 父親は病気がちで、みんなが心配している。 Father is prone to illness and everybody is worried. For more examples, check out the WWWJDIC examples.',
      grammars: ['～がち'],
      keywords: ['gachi', 'prone'],
    ),
    Section(
      heading: 'Describing an ongoing occurrence using 「～つつ」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('テレビ – TV, television'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('寝る 【ね・る】 (ru-verb) – to sleep'),
                  Text('思う 【おも・う】 (u-verb) – to think'),
                  Text('なる (u-verb) – to become'),
                  Text('二日酔い 【ふつ・か・よい】 – hangover'),
                  Text('痛む 【いた・む】 (u-verb) – to feel pain'),
                  Text(
                      '押さえる 【おさ・える】 (ru-verb) – to hold something down; to grasp'),
                  Text('トイレ – bathroom; toilet'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('体 【からだ】 – body'),
                  Text('いい (i-adj) – good'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('全然 【ぜん・ぜん】 – not at all (when used with negative)'),
                  Text('運動 【うん・どう】 – exercise'),
                  Text('する (exception) – to do'),
                  Text('電気 【でん・き】 – electricity; (electric) light'),
                  Text('製品 【せい・ひん】 – manufactured goods, product'),
                  Text('発展 【はっ・てん】 – development; growth; advancement'),
                  Text('つれる (ru-verb) – to lead'),
                  Text('ハードディスク – hard disk'),
                  Text('容量 【よう・りょう】 – capacity'),
                  Text('ますます – increasingly'),
                  Text('大きい 【おお・きい】(i-adj) – big'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('今 【いま】 – now'),
                  Text('日本 【に・ほん】 – Japan'),
                  Text('終身 【しゅう・しん】 – lifetime'),
                  Text('雇用 【こ・よう】 – employment'),
                  Text('年功 【ねん・こう】 – long service'),
                  Text('序列 【じょ・れつ】 – order'),
                  Text('年功序列 【ねん・こう・じょ・れつ】 – seniority system'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('慣行 【かん・こう】 – customary practice'),
                  Text('崩れる 【くず・れる】 (ru-verb) – to collapse; to crumble'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        '「つつ」 is a verb modifier that can be attached to the stem of verbs to express an ongoing occurrence. Though the meaning stays essentially the same, there are essentially two ways to use this grammar. The first is almost identical to the 「～ながら」 grammar. You can use 「つつ」 to describe an action that is taking place while another action is ongoing. However, there are several major differences between 「つつ」 and 「～ながら」. First, the tone of 「つつ」 is very different from that of 「～ながら」 and you would rarely, if ever, use it for regular everyday occurrences. To go along with this, 「つつ」 is more appropriate for more literary or abstract actions such as those involving emotions or thoughts. Second, 「～ながら」 is used to describe an auxiliary action that takes place while the main action is going on. However, with 「つつ」, both actions have equal weight.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'For example, it would sound very strange to say the following.',
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'つつ',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: neruSleep,
                              inlineSpan: const TextSpan(
                                text: '寝ちゃ',
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'ダメ',
                              ),
                            ),
                            const TextSpan(text: 'よ！'),
                          ],
                        ),
                      ),
                      const Text('(Sounds unnatural)'),
                    ],
                  ),
                ],
              ),
              BulletedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: terebi,
                              inlineSpan: const TextSpan(
                                text: 'テレビ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: TextSpan(
                                text: '見',
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                            TextSpan(
                              text: 'ながら',
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: neruSleep,
                              inlineSpan: const TextSpan(
                                text: '寝ちゃ',
                              ),
                            ),
                            VocabTooltip(
                              message: dame,
                              inlineSpan: const TextSpan(
                                text: 'ダメ',
                              ),
                            ),
                            const TextSpan(text: 'よ！'),
                          ],
                        ),
                      ),
                      const Text('Don’t watch TV while sleeping!'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'The second way to use this grammar is to express the existence of a continuing process by using 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, the verb for existence. Everything is the same as before except that you attach 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text: '」 to 「つつ」 to produce 「～つつ',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」. This is often used in magazine or newspaper articles to describe a certain trend or tide.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「～つつ」 to describe a repetitive occurrence',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'To describe an ongoing action, attach 「つつ」 to the stem of the verb.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'つつ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: omou,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '思',
                                        ),
                                        TextSpan(
                                          text: 'う',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: omou,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '思',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: omou,
                                    inlineSpan: const TextSpan(
                                      text: '思い',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'つつ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                  text:
                                      'To show the existence of a trend or tide, add 「'),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              const TextSpan(
                                text: '」 to 「つつ」.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なり',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'つつ',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なり',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'つつ',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: TextSpan(
                                      text: 'ある',
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: futsukayoi,
                              inlineSpan: const TextSpan(
                                text: '二日酔い',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: itamu,
                              inlineSpan: const TextSpan(
                                text: '痛む',
                              ),
                            ),
                            VocabTooltip(
                              message: atama,
                              inlineSpan: const TextSpan(
                                text: '頭',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: osaeru,
                              inlineSpan: TextSpan(
                                text: '押さえ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'つつ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: toire,
                              inlineSpan: const TextSpan(
                                text: 'トイレ',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: const TextSpan(
                                text: '入った',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Went into the bathroom while holding an aching head from a hangover.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: karada,
                              inlineSpan: const TextSpan(
                                text: '体',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'よくない',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: omou,
                              inlineSpan: TextSpan(
                                text: '思い',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'つつ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: zenzen,
                              inlineSpan: const TextSpan(
                                text: '全然',
                              ),
                            ),
                            VocabTooltip(
                              message: undou,
                              inlineSpan: const TextSpan(
                                text: '運動',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'してない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'While thinking it’s bad for body, haven’t exercised at all recently.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: denki,
                              inlineSpan: const TextSpan(
                                text: '電気',
                              ),
                            ),
                            VocabTooltip(
                              message: seihin,
                              inlineSpan: const TextSpan(
                                text: '製品',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: hatten,
                              inlineSpan: const TextSpan(
                                text: '発展',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: tsureru,
                              inlineSpan: const TextSpan(
                                text: 'つれて',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: haadodisuku,
                              inlineSpan: const TextSpan(
                                text: 'ハードディスク',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: youryou,
                              inlineSpan: const TextSpan(
                                text: '容量',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: masumasu,
                              inlineSpan: const TextSpan(
                                text: 'ますます',
                              ),
                            ),
                            VocabTooltip(
                              message: ookii,
                              inlineSpan: const TextSpan(
                                text: '大きく',
                              ),
                            ),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: TextSpan(
                                text: 'なり',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'つつ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Lead by the advancement of electronic products, hard disk drive capacities are becoming ever larger.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ima,
                              inlineSpan: const TextSpan(
                                text: '今',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: nihon,
                              inlineSpan: const TextSpan(
                                text: '日本',
                              ),
                            ),
                            const TextSpan(text: 'では、'),
                            VocabTooltip(
                              message: shuushin,
                              inlineSpan: const TextSpan(
                                text: '終身',
                              ),
                            ),
                            VocabTooltip(
                              message: koyou,
                              inlineSpan: const TextSpan(
                                text: '雇用',
                              ),
                            ),
                            const TextSpan(text: 'や'),
                            VocabTooltip(
                              message: nenkou,
                              inlineSpan: const TextSpan(
                                text: '年功',
                              ),
                            ),
                            VocabTooltip(
                              message: joretsu,
                              inlineSpan: const TextSpan(
                                text: '序列',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: 'いう',
                              ),
                            ),
                            VocabTooltip(
                              message: koyou,
                              inlineSpan: const TextSpan(
                                text: '雇用',
                              ),
                            ),
                            VocabTooltip(
                              message: kankou,
                              inlineSpan: const TextSpan(
                                text: '慣行',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: kuzureru,
                              inlineSpan: TextSpan(
                                text: '崩れ',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'つつ',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'In today’s Japan, hiring practices like life-time employment and age-based ranking are tending to break down.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: 'For more examples, check out the ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'WWWJDIC examples',
                    ),
                    uri:
                        'http://nihongo.monash.edu/cgi-bin/wwwjdic?1Q%A4%C4%A4%C4_0__',
                  ),
                  const TextSpan(
                    text: '.',
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary テレビ – TV, television 見る 【み・る】 (ru-verb) – to see 寝る 【ね・る】 (ru-verb) – to sleep 思う 【おも・う】 (u-verb) – to think なる (u-verb) – to become 二日酔い 【ふつ・か・よい】 – hangover 痛む 【いた・む】 (u-verb) – to feel pain 押さえる 【おさ・える】 (ru-verb) – to hold something down; to grasp トイレ – bathroom; toilet 入る 【はい・る】 (u-verb) – to enter 体 【からだ】 – body いい (i-adj) – good 最近 【さい・きん】 – recent; lately 全然 【ぜん・ぜん】 – not at all (when used with negative) 運動 【うん・どう】 – exercise する (exception) – to do 電気 【でん・き】 – electricity; (electric) light 製品 【せい・ひん】 – manufactured goods, product 発展 【はっ・てん】 – development; growth; advancement つれる (ru-verb) – to lead ハードディスク – hard disk 容量 【よう・りょう】 – capacity ますます – increasingly 大きい 【おお・きい】(i-adj) – big ある (u-verb) – to exist (inanimate) 今 【いま】 – now 日本 【に・ほん】 – Japan 終身 【しゅう・しん】 – lifetime 雇用 【こ・よう】 – employment 年功 【ねん・こう】 – long service 序列 【じょ・れつ】 – order 年功序列 【ねん・こう・じょ・れつ】 – seniority system 言う 【い・う】 (u-verb) – to say 慣行 【かん・こう】 – customary practice 崩れる 【くず・れる】 (ru-verb) – to collapse; to crumble 「つつ」 is a verb modifier that can be attached to the stem of verbs to express an ongoing occurrence. Though the meaning stays essentially the same, there are essentially two ways to use this grammar. The first is almost identical to the 「～ながら」 grammar. You can use 「つつ」 to describe an action that is taking place while another action is ongoing. However, there are several major differences between 「つつ」 and 「～ながら」. First, the tone of 「つつ」 is very different from that of 「～ながら」 and you would rarely, if ever, use it for regular everyday occurrences. To go along with this, 「つつ」 is more appropriate for more literary or abstract actions such as those involving emotions or thoughts. Second, 「～ながら」 is used to describe an auxiliary action that takes place while the main action is going on. However, with 「つつ」, both actions have equal weight. For example, it would sound very strange to say the following. テレビを見つつ、寝ちゃダメよ！(Sounds unnatural) テレビを見ながら、寝ちゃダメよ！ Don’t watch TV while sleeping! The second way to use this grammar is to express the existence of a continuing process by using 「ある」, the verb for existence. Everything is the same as before except that you attach 「ある」 to 「つつ」 to produce 「～つつある」. This is often used in magazine or newspaper articles to describe a certain trend or tide. Using 「～つつ」 to describe a repetitive occurrence To describe an ongoing action, attach 「つつ」 to the stem of the verb. Examples: 見る → 見つつ 思う → 思い → 思いつつ To show the existence of a trend or tide, add 「ある」 to 「つつ」. Example: なる → なり → なりつつ → なりつつある Examples 二日酔いで痛む頭を押さえつつ、トイレに入った。 Went into the bathroom while holding an aching head from a hangover. 体によくないと思いつつ、最近は全然運動してない。 While thinking it’s bad for body, haven’t exercised at all recently. 電気製品の発展につれて、ハードディスクの容量はますます大きくなりつつある。 Lead by the advancement of electronic products, hard disk drive capacities are becoming ever larger. 今の日本では、終身雇用や年功序列という雇用慣行が崩れつつある。 In today’s Japan, hiring practices like life-time employment and age-based ranking are tending to break down. For more examples, check out the WWWJDIC examples.',
      grammars: ['～つつ'],
      keywords: ['tsutsu','while','nagara','tendency'],
    ),
    Section(
      heading: 'Describing a negative tendency using 「きらいがある」',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('嫌い 【きら・い】 (na-adj) – distasteful, hateful'),
                  Text('依存症 【い・ぞん・しょう】 – dependence; addiction'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('多い 【おお・い】 (i-adj) – numerous'),
                  Text('大学生 【だい・がく・せい】 – college student'),
                  Text('締切日 【しめ・きり・び】 – closing day; deadline'),
                  Text('ぎりぎり – at the last moment; just barely'),
                  Text('宿題 【しゅく・だい】 – homework'),
                  Text('やる (u-verb) – to do'),
                  Text('コーディング – coding'),
                  Text('好き 【す・き】 (na-adj) – likable; desirable'),
                  Text('開発者 【かい・はつ・しゃ】 – developer'),
                  Text('ちゃんと – properly'),
                  Text('する (exception) – to do'),
                  Text('ドキュメント – document'),
                  Text('作成 【さく・せい】 – creation'),
                  Text('十分 【じゅう・ぶん】 – sufficient, adequate'),
                  Text('テスト – test'),
                  Text('怠る 【おこた・る】 (u-verb) – to shirk'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text: '「きらいが',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 is a fixed expression used to describe a bad tendency or habit. I suspect that 「きらい」 here ',
                  ),
                  const TextSpan(
                    text: 'might',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  const TextSpan(
                    text: ' have something to do with the word for hateful: 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌い',
                    ),
                  ),
                  const TextSpan(
                    text: '」. However, unlike 「',
                  ),
                  VocabTooltip(
                    message: kirai,
                    inlineSpan: const TextSpan(
                      text: '嫌い',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」, which is a na-adjective, the 「きらい」 in this grammar functions as a noun. This is made plain by the fact that the 「が」 particle comes right after 「きらい」, which is not allowed for adjectives. The rest of the phrase is simply expressing the fact that the negative tendency exists.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「きらいがある」 to describe a negative tendency',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              const TextSpan(
                                text:
                                    'The 「きらい」 in this grammar functions as a noun. 「',
                              ),
                              VocabTooltip(
                                message: aru,
                                inlineSpan: const TextSpan(
                                  text: 'ある',
                                ),
                              ),
                              const TextSpan(
                                text:
                                    '」 is simply the existence verb for inanimate objects.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: izonshou,
                                    inlineSpan: const TextSpan(
                                      text: '依存症',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'の',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: 'きらいが',
                                  ),
                                  VocabTooltip(
                                    message: aru,
                                    inlineSpan: const TextSpan(
                                      text: 'ある',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '。',
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ooku,
                              inlineSpan: const TextSpan(
                                text: '多く',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: daigakusei,
                              inlineSpan: const TextSpan(
                                text: '大学生',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: shimekiribi,
                              inlineSpan: const TextSpan(
                                text: '締切日',
                              ),
                            ),
                            VocabTooltip(
                              message: girigiri,
                              inlineSpan: const TextSpan(
                                text: 'ぎりぎり',
                              ),
                            ),
                            const TextSpan(text: 'まで、'),
                            VocabTooltip(
                              message: shukudai,
                              inlineSpan: const TextSpan(
                                text: '宿題',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: yaru,
                              inlineSpan: const TextSpan(
                                text: 'やらない',
                              ),
                            ),
                            TextSpan(
                              text: 'きらいが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'A lot of college students have a bad tendency of not doing their homework until just barely it’s due date.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: koodingu,
                              inlineSpan: const TextSpan(
                                text: 'コーディング',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: suki,
                              inlineSpan: const TextSpan(
                                text: '好き',
                              ),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: kaihatsusha,
                              inlineSpan: const TextSpan(
                                text: '開発者',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: chanto,
                              inlineSpan: const TextSpan(
                                text: 'ちゃんと',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'した',
                              ),
                            ),
                            VocabTooltip(
                              message: dokyumento,
                              inlineSpan: const TextSpan(
                                text: 'ドキュメント',
                              ),
                            ),
                            VocabTooltip(
                              message: sakusei,
                              inlineSpan: const TextSpan(
                                text: '作成',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: juubun,
                              inlineSpan: const TextSpan(
                                text: '十分',
                              ),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: tesuto,
                              inlineSpan: const TextSpan(
                                text: 'テスト',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: okotaru,
                              inlineSpan: const TextSpan(
                                text: '怠る',
                              ),
                            ),
                            TextSpan(
                              text: 'きらいが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Developers that like coding have a bad tendency to neglect proper documents and adequate testing.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 嫌い 【きら・い】 (na-adj) – distasteful, hateful 依存症 【い・ぞん・しょう】 – dependence; addiction ある (u-verb) – to exist (inanimate) 多い 【おお・い】 (i-adj) – numerous 大学生 【だい・がく・せい】 – college student 締切日 【しめ・きり・び】 – closing day; deadline ぎりぎり – at the last moment; just barely 宿題 【しゅく・だい】 – homework やる (u-verb) – to do コーディング – coding 好き 【す・き】 (na-adj) – likable; desirable 開発者 【かい・はつ・しゃ】 – developer ちゃんと – properly する (exception) – to do ドキュメント – document 作成 【さく・せい】 – creation 十分 【じゅう・ぶん】 – sufficient, adequate テスト – test 怠る 【おこた・る】 (u-verb) – to shirk 「きらいがある」 is a fixed expression used to describe a bad tendency or habit. I suspect that 「きらい」 here might have something to do with the word for hateful: 「嫌い」. However, unlike 「嫌い」, which is a na-adjective, the 「きらい」 in this grammar functions as a noun. This is made plain by the fact that the 「が」 particle comes right after 「きらい」, which is not allowed for adjectives. The rest of the phrase is simply expressing the fact that the negative tendency exists. Using 「きらいがある」 to describe a negative tendency The 「きらい」 in this grammar functions as a noun. 「ある」 is simply the existence verb for inanimate objects. Example: 依存症のきらいがある。 Examples 多くの大学生は、締切日ぎりぎりまで、宿題をやらないきらいがある。 A lot of college students have a bad tendency of not doing their homework until just barely it’s due date. コーディングが好きな開発者は、ちゃんとしたドキュメント作成と十分なテストを怠るきらいがある。 Developers that like coding have a bad tendency to neglect proper documents and adequate testing.',
      grammars: ['きらいがある'],
      keywords: ['kirai ga aru','bad habit','tendency'],
    ),
  ],
);
