import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/tooltip_messages.dart';
import 'package:japanese_grammar_guide/customs/blank_list.dart';
import 'package:japanese_grammar_guide/customs/bulleted_list.dart';
import 'package:japanese_grammar_guide/customs/external_link.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/customs/notes_section.dart';
import 'package:japanese_grammar_guide/customs/numbered_list.dart';
import 'package:japanese_grammar_guide/customs/paragraph.dart';
import 'package:japanese_grammar_guide/customs/vocab_tooltip.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

Lesson volitional2 = Lesson(
  title: 'Advanced Volitional',
  sections: [
    Section(
      content: Builder(
        builder: (BuildContext context) {
          return const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Paragraph(
                texts: [
                  TextSpan(
                      text:
                          'We learned in a previous lesson that the volitional form is used when one is set out to do something. In this section, we’re going to cover some other ways in which the volitional form is used, most notably, the negative volitional form.'),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'We learned in a previous lesson that the volitional form is used when one is set out to do something. In this section, we’re going to cover some other ways in which the volitional form is used, most notably, the negative volitional form.',
      grammars: [],
      keywords: ['volitional'],
    ),
    Section(
      heading: 'Negative volitional',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('する (exception) – to do'),
                  Text('来る 【く・る】 (exception) – to come'),
                  Text('なる (u-verb) – to become'),
                  Text('相手 【あい・て】 – other party'),
                  Text('剣 【けん】 – sword'),
                  Text('達人 【たつ・じん】 – master, expert'),
                  Text('そう – (things are) that way'),
                  Text('簡単 【かん・たん】 (na-adj) – simple'),
                  Text('勝つ 【か・つ】 (u-verb) – to win'),
                  Text('そんな – that sort of'),
                  Text('無茶 【む・ちゃ】 – unreasonable; excessive'),
                  Text('手段 【しゅ・だん】 – method'),
                  Text('認める 【みと・める】 (ru-verb) – to recognize, to acknowledge'),
                  Text('その – that （abbr. of それの）'),
                  Text('時 【とき】 – time'),
                  Text('決して 【けっ・して】 – by no means; decidedly'),
                  Text('彼 【かれ】 – he; boyfriend'),
                  Text('会う 【あ・う】 (u-verb) – to meet'),
                  Text('心 【こころ】 – heart; mind'),
                  Text('決める 【き・める】 (ru-verb) – to decide'),
                  Text('あの – that (over there) （abbr. of あれの）'),
                  Text('人 【ひと】 – person'),
                  Text('～度 【～ど】 – counter for number of times'),
                  Text('嘘 【うそ】 – lie'),
                  Text('つく (u-verb) – to be attached'),
                  Text('誓う 【ちか・う】 (u-verb) – to swear, to pledge'),
                  Text('明日 【あした】 – tomorrow'),
                  Text('やめる (ru-verb) – to stop; to quit'),
                  Text('肉 【にく】 – meat'),
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'You may have seen the negative volitional form in a verb conjugation table and wondered, “What the heck is that used for?” Well the answer is not much, or to put it more accurately, there are various ways it can be used but almost all of them are extremely stiff and formal. In fact, it’s so rare that I only found ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'one explanation',
                    ),
                    uri: 'http://nihongo.monash.edu/wwwverbinf.html',
                  ),
                  const TextSpan(
                    text:
                        ' in English on the web or anywhere else. (I also found ',
                  ),
                  ExternalLink(
                    inlineSpan: const TextSpan(
                      text: 'this one',
                    ),
                    uri:
                        'https://web.archive.org/web/20071220010159/http://www.geocities.jp/niwasaburoo/32kanyuuisi.html#32.10',
                  ),
                  const TextSpan(
                    text: ' in Japanese.)',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The negative volitional is used to express negative intention. This means that there is a will for something to ',
                  ),
                  TextSpan(
                      text: 'not',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  TextSpan(
                    text: ' happen or that someone is set out to ',
                  ),
                  TextSpan(
                      text: 'not',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  TextSpan(
                    text:
                        ' do something. As a result, because one is trying not to do something, it’s probably not going to happen. Essentially, it is a very stiff and formal version of 「でしょう」 and 「だろう」. While this form is practically never used in daily conversations, you might still hear it in movies, etc.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'Verbs are conjugated to the negative volitional by simply attaching 「まい」 to the verb. Another alternative is to attach 「まい」 to the stem. The conjugation for the negative volitional is quite different from those we are used to because it is always the last conjugation to apply even for the masu-form. There is no way to conjugate 「まい」 to the masu-form, you simply attach 「まい」 to the masu-form conjugation.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「まい」 to express a will to not do something',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For ru-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach「まい」 to the verb or verb stem.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見る',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '見',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: const TextSpan(
                                      text: '見',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'For u-verbs:',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: ' Attach 「まい」 to the end of the verb.',
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Exceptions:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: TextSpan(
                                      text: 'する',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' or ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'し',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: TextSpan(
                                      text: 'くる',
                                      style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: kuru,
                                    inlineSpan: const TextSpan(
                                      text: 'くる',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                            'This conjugation must always come last. For masu-form, attach 「まい」 to the masu-form verb.'),
                        const Text(
                          'Example:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        BulletedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'な',
                                        ),
                                        TextSpan(
                                          text: 'り',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: 'なり',
                                        ),
                                        TextSpan(
                                          text: 'ます',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: naru,
                                    inlineSpan: const TextSpan(
                                      text: 'なります',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aite,
                              inlineSpan: const TextSpan(
                                text: '相手',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: ken,
                              inlineSpan: const TextSpan(
                                text: '剣',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: tatsujin,
                              inlineSpan: const TextSpan(
                                text: '達人',
                              ),
                            ),
                            const TextSpan(text: 'だ。'),
                            VocabTooltip(
                              message: sou,
                              inlineSpan: const TextSpan(
                                text: 'そう',
                              ),
                            ),
                            VocabTooltip(
                              message: kantan,
                              inlineSpan: const TextSpan(
                                text: '簡単',
                              ),
                            ),
                            const TextSpan(text: 'には'),
                            VocabTooltip(
                              message: katsu,
                              inlineSpan: TextSpan(
                                text: '勝て',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まい',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Your opponent is a master of the sword. I doubt you can win so easily.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sonna,
                              inlineSpan: const TextSpan(
                                text: 'そんな',
                              ),
                            ),
                            VocabTooltip(
                              message: mucha,
                              inlineSpan: const TextSpan(
                                text: '無茶',
                              ),
                            ),
                            const TextSpan(text: 'な'),
                            VocabTooltip(
                              message: shudan,
                              inlineSpan: const TextSpan(
                                text: '手段',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: mitomeru,
                              inlineSpan: TextSpan(
                                text: '認めます',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まい',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '！'),
                          ],
                        ),
                      ),
                      const Text(
                          'I won’t approve of such an unreasonable method!'),
                    ],
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We already learned that you could use the volitional form to say “let’s” and to express an attempt do something. But that doesn’t mean you can use the negative volitional to say “let’s not” or “try not to”. The tone of this grammar is one of very strong determination to not do something, as you can see in the following examples.',
                  ),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: toki,
                              inlineSpan: const TextSpan(
                                text: '時',
                              ),
                            ),
                            const TextSpan(text: 'までは'),
                            VocabTooltip(
                              message: kesshite,
                              inlineSpan: const TextSpan(
                                text: '決して',
                              ),
                            ),
                            VocabTooltip(
                              message: kare,
                              inlineSpan: const TextSpan(
                                text: '彼',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: au,
                              inlineSpan: TextSpan(
                                text: '会う',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まい',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: kokoro,
                              inlineSpan: const TextSpan(
                                text: '心',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: kimeru,
                              inlineSpan: const TextSpan(
                                text: '決めていた',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Until that time, I had decided in my heart to not meet him by any means.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ano,
                              inlineSpan: const TextSpan(
                                text: 'あの',
                              ),
                            ),
                            VocabTooltip(
                              message: hito,
                              inlineSpan: const TextSpan(
                                text: '人',
                              ),
                            ),
                            const TextSpan(text: 'は、'),
                            VocabTooltip(
                              message: nido,
                              inlineSpan: const TextSpan(
                                text: '二度',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: uso,
                              inlineSpan: const TextSpan(
                                text: '嘘',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: tsuku,
                              inlineSpan: TextSpan(
                                text: 'つく',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まい',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: chikau,
                              inlineSpan: const TextSpan(
                                text: '誓った',
                              ),
                            ),
                            const TextSpan(text: 'のです。'),
                          ],
                        ),
                      ),
                      const Text('That person had sworn to never lie again.'),
                    ],
                  ),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'In order to express “let’s not”, you can use the verb, 「',
                  ),
                  VocabTooltip(
                    message: yameru,
                    inlineSpan: const TextSpan(
                      text: 'やめる',
                    ),
                  ),
                  const TextSpan(
                      text:
                          '」 with the regular volitional. In order to express an effort to not do something, you can use 「'),
                  VocabTooltip(
                    message: you,
                    inlineSpan: const TextSpan(
                      text: 'よう',
                    ),
                  ),
                  const TextSpan(text: 'に'),
                  VocabTooltip(
                    message: suru,
                    inlineSpan: const TextSpan(
                      text: 'する',
                    ),
                  ),
                  const TextSpan(text: '」 with the negative verb.'),
                ],
              ),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: ashita,
                              inlineSpan: const TextSpan(
                                text: '明日',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: iku,
                              inlineSpan: const TextSpan(
                                text: '行く',
                              ),
                            ),
                            const TextSpan(text: 'のを'),
                            VocabTooltip(
                              message: yameru,
                              inlineSpan: TextSpan(
                                text: 'やめよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Let’s not go tomorrow. (lit: Let’s quit going tomorrow.)'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: niku,
                              inlineSpan: const TextSpan(
                                text: '肉',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: taberu,
                              inlineSpan: const TextSpan(
                                text: '食べない',
                              ),
                            ),
                            VocabTooltip(
                              message: you,
                              inlineSpan: TextSpan(
                                text: 'よう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'に',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'している',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text('Trying not to eat meat.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 見る 【み・る】 (ru-verb) – to see 行く 【い・く】 (u-verb) – to go する (exception) – to do 来る 【く・る】 (exception) – to come なる (u-verb) – to become 相手 【あい・て】 – other party 剣 【けん】 – sword 達人 【たつ・じん】 – master, expert そう – (things are) that way 簡単 【かん・たん】 (na-adj) – simple 勝つ 【か・つ】 (u-verb) – to win そんな – that sort of 無茶 【む・ちゃ】 – unreasonable; excessive 手段 【しゅ・だん】 – method 認める 【みと・める】 (ru-verb) – to recognize, to acknowledge その – that （abbr. of それの） 時 【とき】 – time 決して 【けっ・して】 – by no means; decidedly 彼 【かれ】 – he; boyfriend 会う 【あ・う】 (u-verb) – to meet 心 【こころ】 – heart; mind 決める 【き・める】 (ru-verb) – to decide あの – that (over there) （abbr. of あれの） 人 【ひと】 – person～度 【～ど】 – counter for number of times 嘘 【うそ】 – lie つく (u-verb) – to be attached 誓う 【ちか・う】 (u-verb) – to swear, to pledge 明日 【あした】 – tomorrow やめる (ru-verb) – to stop; to quit 肉 【にく】 – meat 食べる 【た・べる】 (ru-verb) – to eat You may have seen the negative volitional form in a verb conjugation table and wondered, “What the heck is that used for?” Well the answer is not much, or to put it more accurately, there are various ways it can be used but almost all of them are extremely stiff and formal. In fact, it’s so rare that I only found  in English on the web or anywhere else. (I also found one explanationthis one in Japanese.) The negative volitional is used to express negative intention. This means that there is a will for something to not happen or that someone is set out to not do something. As a result, because one is trying not to do something, it’s probably not going to happen. Essentially, it is a very stiff and formal version of 「でしょう」 and 「だろう」. While this form is practically never used in daily conversations, you might still hear it in movies, etc. Verbs are conjugated to the negative volitional by simply attaching 「まい」 to the verb. Another alternative is to attach 「まい」 to the stem. The conjugation for the negative volitional is quite different from those we are used to because it is always the last conjugation to apply even for the masu-form. There is no way to conjugate 「まい」 to the masu-form, you simply attach 「まい」 to the masu-form conjugation. Using 「まい」 to express a will to not do something For ru-verbs: Attach 「まい」 to the verb or verb stem. Examples: 見る → 見るまい 見る → 見まい For u-verbs: Attach 「まい」 to the end of the verb. Example: 行くまい Exceptions: する → するまい or しまいくる → くるまい This conjugation must always come last. For masu-form, attach 「まい」 to the masu-form verb. Example: なる → なり → なります → なりますまい Examples 相手は剣の達人だ。そう簡単には勝てまい。 Your opponent is a master of the sword. I doubt you can win so easily. そんな無茶な手段は認めますまい！ I won’t approve of such an unreasonable method! We already learned that you could use the volitional form to say “let’s” and to express an attempt do something. But that doesn’t mean you can use the negative volitional to say “let’s not” or “try not to”. The tone of this grammar is one of very strong determination to not do something, as you can see in the following examples. その時までは決して彼に会うまいと心に決めていた。 Until that time, I had decided in my heart to not meet him by any means. あの人は、二度と嘘をつくまいと誓ったのです。 That person had sworn to never lie again. In order to express “let’s not”, you can use the verb, 「やめる」 with the regular volitional. In order to express an effort to not do something, you can use 「ようにする」 with the negative verb. 明日に行くのをやめよう。 Let’s not go tomorrow. (lit: Let’s quit going tomorrow.) 肉を食べないようにしている。 Trying not to eat meat.',
      grammars: ['〜まい',],
      keywords: ['mai','vollitional','negative','verb'],
    ),
    Section(
      heading: 'Using the volitional to express a lack of relation',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('食べる 【た・べる】 (ru-verb) – to eat'),
                  Text('行く 【い・く】 (u-verb) – to go'),
                  Text('あいつ – that guy (derogatory)'),
                  Text('大学 【だい・がく】 – college'),
                  Text('入る 【はい・る】 (u-verb) – to enter'),
                  Text('俺 【おれ】 – me; myself; I (masculine)'),
                  Text('関係 【かん・けい】 – relation, relationship'),
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('時間 【じ・かん】 – time'),
                  Text('合う 【あ・う】 (u-verb) – to match'),
                  Text('間に合う 【ま・に・あ・う】 – to be in time'),
                  Text('最近 【さい・きん】 – recent; lately'),
                  Text('ウィルス – virus'),
                  Text('強力 【きょう・りょく】 (na-adj) – powerful, strong'),
                  Text('プログラム – program'),
                  Text('実行 【じっ・こう】 – execute'),
                  Text('する (exception) – to do'),
                  Text('ページ – page'),
                  Text('見る 【み・る】 (ru-verb) – to see'),
                  Text('感染 【かん・せん】 – infection'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We will now learn a grammar that’s actually practical for everyday use using the negative volitional grammar. Basically, we can use both volitional and negative volitional forms to say it doesn’t matter whether something is going to happen or not. This is done by attaching 「が」 to both the volitional and the negative volitional form of the verb that doesn’t matter.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using the volitional to express a lack of relation',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「が」 to the volitional and negative volitional form of the verb.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'る',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '食べ',
                                        ),
                                        TextSpan(
                                          text: 'よう',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べよう',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '食べ',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'まい',
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'く',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: miru,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '行',
                                        ),
                                        TextSpan(
                                          text: 'こう',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: '、',
                                  ),
                                  VocabTooltip(
                                    message: taberu,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'まい',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行こう',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  VocabTooltip(
                                    message: iku,
                                    inlineSpan: const TextSpan(
                                      text: '行く',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: 'まい',
                                  ),
                                  TextSpan(
                                    text: 'が',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: aitsu,
                              inlineSpan: const TextSpan(
                                text: 'あいつ',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: daigaku,
                              inlineSpan: const TextSpan(
                                text: '大学',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入ろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: hairu,
                              inlineSpan: TextSpan(
                                text: '入る',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まいが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: ore,
                              inlineSpan: const TextSpan(
                                text: '俺',
                              ),
                            ),
                            const TextSpan(text: 'とは'),
                            VocabTooltip(
                              message: kankei,
                              inlineSpan: const TextSpan(
                                text: '関係',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: 'よ。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Whether that guy is going to college or not, it has nothing to do with me.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: jikan,
                              inlineSpan: const TextSpan(
                                text: '時間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'ある',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まいが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: maniau,
                              inlineSpan: const TextSpan(
                                text: '間に合わせる',
                              ),
                            ),
                            const TextSpan(text: 'しか'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Whether there is time or not, there’s nothing to do but make it on time.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: saikin,
                              inlineSpan: const TextSpan(
                                text: '最近',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: wirusu,
                              inlineSpan: const TextSpan(
                                text: 'ウィルス',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: kyouryoku,
                              inlineSpan: const TextSpan(
                                text: '強力',
                              ),
                            ),
                            const TextSpan(text: 'で、'),
                            VocabTooltip(
                              message: puroguramu,
                              inlineSpan: const TextSpan(
                                text: 'プログラム',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: jikkou,
                              inlineSpan: TextSpan(
                                text: '実行',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'しよう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'が',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'し',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'まいが',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: peeji,
                              inlineSpan: const TextSpan(
                                text: 'ページ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: miru,
                              inlineSpan: const TextSpan(
                                text: '見る',
                              ),
                            ),
                            const TextSpan(text: 'だけで'),
                            VocabTooltip(
                              message: kansen,
                              inlineSpan: const TextSpan(
                                text: '感染',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: 'らしい。'),
                          ],
                        ),
                      ),
                      const Text(
                          'The viruses lately have been strong and whether you run a program or not, I hear it will spread just by looking at the page.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary 食べる 【た・べる】 (ru-verb) – to eat 行く 【い・く】 (u-verb) – to go あいつ – that guy (derogatory) 大学 【だい・がく】 – college 入る 【はい・る】 (u-verb) – to enter 俺 【おれ】 – me; myself; I (masculine) 関係 【かん・けい】 – relation, relationship ある (u-verb) – to exist (inanimate) 時間 【じ・かん】 – time 合う 【あ・う】 (u-verb) – to match 間に合う 【ま・に・あ・う】 – to be in time 最近 【さい・きん】 – recent; lately ウィルス – virus 強力 【きょう・りょく】 (na-adj) – powerful, strong プログラム – program 実行 【じっ・こう】 – execute する (exception) – to do ページ – page 見る 【み・る】 (ru-verb) – to see 感染 【かん・せん】 – infection We will now learn a grammar that’s actually practical for everyday use using the negative volitional grammar. Basically, we can use both volitional and negative volitional forms to say it doesn’t matter whether something is going to happen or not. This is done by attaching 「が」 to both the volitional and the negative volitional form of the verb that doesn’t matter. Using the volitional to express a lack of relation Attach 「が」 to the volitional and negative volitional form of the verb. Examples: 食べる → 食べよう、食べまい → 食べようが食べまいが 行く → 行こう、行くまい → 行こうが行くまいが Examples あいつが大学に入ろうが入るまいが俺とは関係ないよ。 Whether that guy is going to college or not, it has nothing to do with me. 時間があろうがあるまいが間に合わせるしかない。 Whether there is time or not, there’s nothing to do but make it on time. 最近のウィルスは強力で、プログラムを実行しようがしまいが、ページを見るだけで感染するらしい。 The viruses lately have been strong and whether you run a program or not, I hear it will spread just by looking at the page.',
      grammars: ['〜ようが〜まいが'],
      keywords: ['volitional','doesn’t matter'],
    ),
    Section(
      heading: 'Using 「であろう」 to express likelihood',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('困難 【こん・なん】 (na-adj) – difficulty, distress'),
                  Text('する (exception) – to do'),
                  Text('今後 【こん・ご】 – hereafter'),
                  Text('～年 【～ねん】 – counter for year'),
                  Text('人間 【にん・げん】 – human'),
                  Text('直面 【ちょく・めん】 – confrontation'),
                  Text('問題 【もん・だい】 – problem'),
                  Text('正面 【しょう・めん】 – front; facade'),
                  Text('向き合う 【む・き・あ・う】 (u-verb) – to face each other'),
                  Text('自ら 【みずか・ら】 – for one’s self'),
                  Text('解決 【かい・けつ】 – resolution'),
                  Text('はかる (u-verb) – to plan, to devise'),
                  Text('その – that （abbr. of それの）'),
                  Text('ノウハウ – know-how'),
                  Text('次 【つぎ】 – next'),
                  Text('産業 【さん・ぎょう】 – industry'),
                  Text('なる (u-verb) – to become'),
                  Text('シナリオ – scenario'),
                  Text('考える 【かんが・える】 (ru-verb) – to think'),
                  Text('もちろん – of course'),
                  Text('生徒数 【せい・と・すう】 – number of students'),
                  Text('減少 【げん・しょう】 – decline, reduction'),
                  Text('現在 【げん・ざい】 – present time'),
                  Text('学科 【がっ・か】 – course of study'),
                  Text('新設 【しん・せつ】 – newly organized or established'),
                  Text('職業科 【しょく・ぎょう・か】 – occupational studies'),
                  Text('統廃合 【とう・はい・ごう】 – reorganization'),
                  Text('科内 【か・ない】 – within study course'),
                  Text('コース – course'),
                  Text('改編 【かい・へん】 – reorganization'),
                  Text('時代 【じ・だい】 – period, era'),
                  Text('合う 【あ・う】 (u-verb) – to match'),
                  Text('変革 【へん・かく】 – reform'),
                  Text('求める 【もと・める】 (ru-verb) – to request; to seek'),
                ],
              ),
              Paragraph(
                texts: [
                  const TextSpan(
                    text:
                        'We already found out that the negative volitional can be used as kind of a formal version of 「でしょう」 and 「だろう」. You may wonder, how would you do the same thing for the volitional? The answer is to conjugate the verb 「',
                  ),
                  VocabTooltip(
                    message: aru,
                    inlineSpan: const TextSpan(
                      text: 'ある',
                    ),
                  ),
                  const TextSpan(
                    text:
                        '」 from the formal state-of-being 「である」 to the volitional to produce 「であろう」. Remember 「でしょう」 can already be used as a polite form, so this form is even a step above that in formality. We’ll see what kind of language uses this form in the examples.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「であろう」 to express likelihood',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Attach 「であろう」 to the noun, adjective, or verb.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: konnan,
                                    inlineSpan: const TextSpan(
                                      text: '困難',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: konnan,
                                    inlineSpan: const TextSpan(
                                      text: '困難',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'であろう',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: suru,
                                    inlineSpan: const TextSpan(
                                      text: 'する',
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'であろう',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kongo,
                              inlineSpan: const TextSpan(
                                text: '今後',
                              ),
                            ),
                            VocabTooltip(
                              message: gojuunen,
                              inlineSpan: const TextSpan(
                                text: '50年',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: ningen,
                              inlineSpan: const TextSpan(
                                text: '人間',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: chokumen,
                              inlineSpan: const TextSpan(
                                text: '直面',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: TextSpan(
                                text: 'する',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'であろう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            VocabTooltip(
                              message: mondai,
                              inlineSpan: const TextSpan(
                                text: '問題',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: shoumen,
                              inlineSpan: const TextSpan(
                                text: '正面',
                              ),
                            ),
                            const TextSpan(text: 'から'),
                            VocabTooltip(
                              message: mukiau,
                              inlineSpan: const TextSpan(
                                text: '向き合って',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: mizukara,
                              inlineSpan: const TextSpan(
                                text: '自ら',
                              ),
                            ),
                            VocabTooltip(
                              message: kaiketsu,
                              inlineSpan: const TextSpan(
                                text: '解決',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: hakaru,
                              inlineSpan: const TextSpan(
                                text: 'はかり',
                              ),
                            ),
                            const TextSpan(text: 'つつ、'),
                            VocabTooltip(
                              message: sono,
                              inlineSpan: const TextSpan(
                                text: 'その',
                              ),
                            ),
                            VocabTooltip(
                              message: nouhau,
                              inlineSpan: const TextSpan(
                                text: 'ノウハウ',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: tsugi,
                              inlineSpan: const TextSpan(
                                text: '次',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: sangyou,
                              inlineSpan: const TextSpan(
                                text: '産業',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: naru,
                              inlineSpan: const TextSpan(
                                text: 'なる',
                              ),
                            ),
                            VocabTooltip(
                              message: shinario,
                              inlineSpan: const TextSpan(
                                text: 'シナリオ',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: kangaeru,
                              inlineSpan: const TextSpan(
                                text: '考えたい',
                              ),
                            ),
                            const TextSpan(text: '。(from '),
                            ExternalLink(
                              inlineSpan: const TextSpan(
                                text: 'www.jkokuryo.com',
                              ),
                              uri:
                                  'https://web.archive.org/web/20110828040007/http://www.jkokuryo.com/papers/online/20020107.htm',
                            ),
                            const TextSpan(text: ')'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text:
                                    'I would like to directly approach problems that humans have '),
                            TextSpan(
                              text: 'likely encounter',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                                text:
                                    ' the next 50 years and while devising solutions, take that knowledge and think about scenarios that will become the next industry.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: mochiron,
                              inlineSpan: const TextSpan(
                                text: 'もちろん',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: seitosuu,
                              inlineSpan: const TextSpan(
                                text: '生徒数',
                              ),
                            ),
                            VocabTooltip(
                              message: genshou,
                              inlineSpan: const TextSpan(
                                text: '減少',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: genzai,
                              inlineSpan: const TextSpan(
                                text: '現在',
                              ),
                            ),
                            const TextSpan(text: '、'),
                            VocabTooltip(
                              message: gakka,
                              inlineSpan: const TextSpan(
                                text: '学科',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: shinsetsu,
                              inlineSpan: const TextSpan(
                                text: '新設',
                              ),
                            ),
                            const TextSpan(text: 'は'),
                            VocabTooltip(
                              message: konnan,
                              inlineSpan: TextSpan(
                                text: '困難',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            TextSpan(
                              text: 'であろう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: shokugyouka,
                              inlineSpan: const TextSpan(
                                text: '職業科',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: touhaigou,
                              inlineSpan: const TextSpan(
                                text: '統廃合',
                              ),
                            ),
                            const TextSpan(text: 'や'),
                            VocabTooltip(
                              message: kanaiWithinStudy,
                              inlineSpan: const TextSpan(
                                text: '科内',
                              ),
                            ),
                            VocabTooltip(
                              message: koosu,
                              inlineSpan: const TextSpan(
                                text: 'コース',
                              ),
                            ),
                            const TextSpan(text: 'の'),
                            VocabTooltip(
                              message: kaihen,
                              inlineSpan: const TextSpan(
                                text: '改編',
                              ),
                            ),
                            const TextSpan(text: 'などで'),
                            VocabTooltip(
                              message: jidai,
                              inlineSpan: const TextSpan(
                                text: '時代',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: auMatch,
                              inlineSpan: const TextSpan(
                                text: '合わせた',
                              ),
                            ),
                            VocabTooltip(
                              message: henkaku,
                              inlineSpan: const TextSpan(
                                text: '変革',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: motomeru,
                              inlineSpan: const TextSpan(
                                text: '求められている',
                              ),
                            ),
                            const TextSpan(text: 'はずである。(from '),
                            ExternalLink(
                              inlineSpan: const TextSpan(
                                text: 'www1.normanet.ne.jp',
                              ),
                              uri:
                                  'https://web.archive.org/web/20120507045800/http://www1.normanet.ne.jp/~ww100114/library/li-01.htm',
                            ),
                            const TextSpan(text: ')'),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            const TextSpan(
                                text:
                                    'Of course, setting up new courses of study will '),
                            TextSpan(
                              text: 'likely be difficult',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(
                                text:
                                    ' with this period of decreasing student population but with reorganizations of occupational studies and courses within subjects, there is supposed to be demand for reform fit for this period.'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ある (u-verb) – to exist (inanimate) 困難 【こん・なん】 (na-adj) – difficulty, distress する (exception) – to do 今後 【こん・ご】 – hereafter～年 【～ねん】 – counter for year 人間 【にん・げん】 – human 直面 【ちょく・めん】 – confrontation 問題 【もん・だい】 – problem 正面 【しょう・めん】 – front; facade 向き合う 【む・き・あ・う】 (u-verb) – to face each other 自ら 【みずか・ら】 – for one’s self 解決 【かい・けつ】 – resolution はかる (u-verb) – to plan, to devise その – that （abbr. of それの） ノウハウ – know-how 次 【つぎ】 – next 産業 【さん・ぎょう】 – industry なる (u-verb) – to become シナリオ – scenario 考える 【かんが・える】 (ru-verb) – to think もちろん – of course 生徒数 【せい・と・すう】 – number of students 減少 【げん・しょう】 – decline, reduction 現在 【げん・ざい】 – present time 学科 【がっ・か】 – course of study 新設 【しん・せつ】 – newly organized or established 職業科 【しょく・ぎょう・か】 – occupational studies 統廃合 【とう・はい・ごう】 – reorganization 科内 【か・ない】 – within study course コース – course 改編 【かい・へん】 – reorganization 時代 【じ・だい】 – period, era 合う 【あ・う】 (u-verb) – to match 変革 【へん・かく】 – reform 求める 【もと・める】 (ru-verb) – to request; to seek We already found out that the negative volitional can be used as kind of a formal version of 「でしょう」 and 「だろう」. You may wonder, how would you do the same thing for the volitional? The answer is to conjugate the verb 「ある」 from the formal state-of-being 「である」 to the volitional to produce 「であろう」. Remember 「でしょう」 can already be used as a polite form, so this form is even a step above that in formality. We’ll see what kind of language uses this form in the examples. Using 「であろう」 to express likelihood Attach 「であろう」 to the noun, adjective, or verb. Examples: 困難 → 困難であろう する → するであろう Examples 今後50年、人間が直面するであろう問題に正面から向き合って、自ら解決をはかりつつ、そのノウハウが次の産業となるシナリオを考えたい。(from www.jkokuryo.com) I would like to directly approach problems that humans have likely encounter the next 50 years and while devising solutions, take that knowledge and think about scenarios that will become the next industry. もちろん、生徒数減少の現在、学科の新設は困難であろうが、職業科の統廃合や科内コースの改編などで時代に合わせた変革が求められているはずである。(from www1.normanet.ne.jp) Of course, setting up new courses of study will likely be difficult with this period of decreasing student population but with reorganizations of occupational studies and courses within subjects, there is supposed to be demand for reform fit for this period.',
      grammars: ['であろう'],
      keywords: ['dearou','volitional','likely','probably','formal'],
    ),
    Section(
      heading: 'Using 「かろう」 as volitional for 「い」 endings',
      content: Builder(
        builder: (BuildContext context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Heading(text: 'Vocabulary', level: 2),
              const NumberedList(
                items: [
                  Text('ある (u-verb) – to exist (inanimate)'),
                  Text('早い 【はや・い】 (i-adj) – fast; early'),
                  Text('どんな – what kind of'),
                  Text('商品 【しょう・ひん】 – product'),
                  Text('ネット – net'),
                  Text('販売 【はん・ばい】 – selling'),
                  Text('売上 【うり・あげ】 – amount sold, sales'),
                  Text('伸びる 【の・びる】 (ru-verb) – to extend, to lengthen'),
                  Text('言う 【い・う】 (u-verb) – to say'),
                  Text('物 【もの】 – object'),
                  Text('運動 【うん・どう】 – exercise'),
                  Text('始める 【はじ・める】 (ru-verb) – to begin'),
                  Text('遅い 【おそ・い】 (i-adj) – late'),
                  Text('健康 【けん・こう】 – health'),
                  Text('いい (i-adj) – good'),
                  Text('変わる 【か・わる】(u-verb) – to change'),
                  Text('休日 【きゅう・じつ】 – holiday, day off'),
                  Text('この – this （abbr. of これの）'),
                  Text('関係 【かん・けい】 – relation, relationship'),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'We learned in the lesson about formal grammar that 「ではない」 was the negative of 「である」. So how would we say something like 「であろう」 but for the negative? The answer is to use yet another type of volitional for negatives and i-adjectives used only in formal and literary contexts. You can think of this grammar as a very old-fashioned version for i-adjectives and negative 「い」 endings.',
                  ),
                ],
              ),
              const Paragraph(
                texts: [
                  TextSpan(
                    text:
                        'The conjugation rule is simple: remove the last 「い」 and attach 「かろう」. You can use it for negatives and i-adjectives just like the 「かった」 past conjugation form.',
                  ),
                ],
              ),
              NotesSection(
                heading: 'Using 「かろう」 to express volition for 「い」 endings',
                content: BlankList(
                  items: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Drop the last 「い」 and attach 「かろう」.',
                        ),
                        const Text(
                          'Examples:',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        NumberedList(
                          items: [
                            Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'ではな',
                                  ),
                                  TextSpan(
                                    text: 'い',
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  const TextSpan(
                                    text: 'ではな',
                                  ),
                                  TextSpan(
                                    text: 'かろう',
                                    style: TextStyle(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text.rich(
                              TextSpan(
                                children: [
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '早',
                                        ),
                                        TextSpan(
                                          text: 'い',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                              decoration:
                                                  TextDecoration.lineThrough),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const TextSpan(
                                    text: ' → ',
                                  ),
                                  VocabTooltip(
                                    message: hayai,
                                    inlineSpan: TextSpan(
                                      children: [
                                        const TextSpan(
                                          text: '早',
                                        ),
                                        TextSpan(
                                          text: 'かろう',
                                          style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Heading(text: 'Examples', level: 2),
              NumberedList(
                items: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: donna,
                              inlineSpan: const TextSpan(
                                text: 'どんな',
                              ),
                            ),
                            VocabTooltip(
                              message: shouhin,
                              inlineSpan: const TextSpan(
                                text: '商品',
                              ),
                            ),
                            const TextSpan(text: 'でも'),
                            VocabTooltip(
                              message: netto,
                              inlineSpan: const TextSpan(
                                text: 'ネット',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: hanbai,
                              inlineSpan: const TextSpan(
                                text: '販売',
                              ),
                            ),
                            VocabTooltip(
                              message: suru,
                              inlineSpan: const TextSpan(
                                text: 'する',
                              ),
                            ),
                            const TextSpan(text: 'だけで'),
                            VocabTooltip(
                              message: uriage,
                              inlineSpan: const TextSpan(
                                text: '売上',
                              ),
                            ),
                            const TextSpan(text: 'が'),
                            VocabTooltip(
                              message: nobiru,
                              inlineSpan: const TextSpan(
                                text: '伸びる',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: 'いう',
                              ),
                            ),
                            VocabTooltip(
                              message: mono,
                              inlineSpan: const TextSpan(
                                text: 'もの',
                              ),
                            ),
                            TextSpan(
                              text: 'ではなかろう',
                              style: TextStyle(
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'It’s not necessarily the case that sales go up just by selling any type of product on the net.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: undou,
                              inlineSpan: const TextSpan(
                                text: '運動',
                              ),
                            ),
                            const TextSpan(text: 'を'),
                            VocabTooltip(
                              message: hajimeru,
                              inlineSpan: const TextSpan(
                                text: '始める',
                              ),
                            ),
                            const TextSpan(text: 'のが'),
                            VocabTooltip(
                              message: hayai,
                              inlineSpan: TextSpan(
                                text: '早かろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(
                              text: 'が',
                            ),
                            VocabTooltip(
                              message: osoi,
                              inlineSpan: TextSpan(
                                text: '遅かろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: kenkou,
                              inlineSpan: const TextSpan(
                                text: '健康',
                              ),
                            ),
                            const TextSpan(text: 'に'),
                            VocabTooltip(
                              message: ii,
                              inlineSpan: const TextSpan(
                                text: 'いい',
                              ),
                            ),
                            const TextSpan(text: 'と'),
                            VocabTooltip(
                              message: iu,
                              inlineSpan: const TextSpan(
                                text: 'いう',
                              ),
                            ),
                            const TextSpan(text: 'のは'),
                            VocabTooltip(
                              message: kawaru,
                              inlineSpan: const TextSpan(
                                text: '変わりません',
                              ),
                            ),
                            const TextSpan(text: '。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Whether you start exercising early or late, the fact that it’s good for your health doesn’t change.'),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            VocabTooltip(
                              message: kyuujitsu,
                              inlineSpan: const TextSpan(
                                text: '休日',
                              ),
                            ),
                            const TextSpan(text: 'で'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'あろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: TextSpan(
                                text: 'なかろう',
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary),
                              ),
                            ),
                            const TextSpan(text: 'が、'),
                            VocabTooltip(
                              message: kono,
                              inlineSpan: const TextSpan(
                                text: 'この',
                              ),
                            ),
                            VocabTooltip(
                              message: shigoto,
                              inlineSpan: const TextSpan(
                                text: '仕事',
                              ),
                            ),
                            const TextSpan(text: 'では'),
                            VocabTooltip(
                              message: kankei,
                              inlineSpan: const TextSpan(
                                text: '関係',
                              ),
                            ),
                            VocabTooltip(
                              message: aru,
                              inlineSpan: const TextSpan(
                                text: 'ない',
                              ),
                            ),
                            const TextSpan(text: 'みたい。'),
                          ],
                        ),
                      ),
                      const Text(
                          'Whether it’s a holiday or not, it looks like it doesn’t matter for this job.'),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
      plaintext: 'Vocabulary ある (u-verb) – to exist (inanimate) 早い 【はや・い】 (i-adj) – fast; early どんな – what kind of 商品 【しょう・ひん】 – product ネット – net 販売 【はん・ばい】 – selling 売上 【うり・あげ】 – amount sold, sales 伸びる 【の・びる】 (ru-verb) – to extend, to lengthen 言う 【い・う】 (u-verb) – to say 物 【もの】 – object 運動 【うん・どう】 – exercise 始める 【はじ・める】 (ru-verb) – to begin 遅い 【おそ・い】 (i-adj) – late 健康 【けん・こう】 – health いい (i-adj) – good 変わる 【か・わる】(u-verb) – to change 休日 【きゅう・じつ】 – holiday, day off この – this （abbr. of これの） 関係 【かん・けい】 – relation, relationship We learned in the lesson about formal grammar that 「ではない」 was the negative of 「である」. So how would we say something like 「であろう」 but for the negative? The answer is to use yet another type of volitional for negatives and i-adjectives used only in formal and literary contexts. You can think of this grammar as a very old-fashioned version for i-adjectives and negative 「い」 endings. The conjugation rule is simple: remove the last 「い」 and attach 「かろう」. You can use it for negatives and i-adjectives just like the 「かった」 past conjugation form. Using 「かろう」 to express volition for 「い」 endings Drop the last 「い」 and attach 「かろう」. Examples: ではない → ではなかろう 早い → 早かろう Examples どんな商品でもネットで販売するだけで売上が伸びるというものではなかろう。 It’s not necessarily the case that sales go up just by selling any type of product on the net. 運動を始めるのが早かろうが遅かろうが、健康にいいというのは変わりません。 Whether you start exercising early or late, the fact that it’s good for your health doesn’t change. 休日であろうが、なかろうが、この仕事では関係ないみたい。 Whether it’s a holiday or not, it looks like it doesn’t matter for this job.',
      grammars: ['かろう'],
      keywords: ['karou','volitional','i-adjective','negative','formal'],
    ),
  ],
);
