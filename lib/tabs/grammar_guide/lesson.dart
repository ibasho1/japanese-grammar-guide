import 'package:flutter/material.dart';

class Chapter {
  const Chapter({required this.title, required this.lessons});
  final String title;
  final List<Lesson> lessons;
}

class Lesson {
  const Lesson({required this.title, required this.sections, this.index = -1});
  final String title;
  final List<Section> sections;
  final int index;
}

class Section {
  const Section(
      {required this.plaintext, required this.content, this.heading = '', required this.grammars, required this.keywords});
  final String heading;
  final String plaintext;
  final List<String> grammars;
  final List<String> keywords;
  final Builder content;
}
