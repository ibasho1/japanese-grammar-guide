import 'package:japanese_grammar_guide/tabs/grammar_guide/0_before_you_start/introduction.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/1_the_writing_system/hiragana.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/1_the_writing_system/kanji.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/1_the_writing_system/katakana.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/1_the_writing_system/writing.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/adjectives.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/adverbs.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/basic.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/clause.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/in_transitive.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/negative_verbs.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/noun_particles.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/particles_intro.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/past_tense.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/state_of_being.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/verb_particles.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/2_basic_grammar/verbs.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/action_clause.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/compound.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/conditionals.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/define.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/desire.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/essential.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/favors.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/must.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/numbers.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/people.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/polite.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/potential.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/question.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/requests.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/sentence_ending.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/slang.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/suru_naru.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/te_form.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/3_essential_grammar/trying.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/amount.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/cause_pass.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/certainty.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/comparison.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/easy_hard.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/generic_nouns.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/honorific.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/negative_verbs_2.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/no_change.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/reasoning.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/similarity.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/special_expressions.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/time_actions.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/4_special_expressions/unintended.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/advanced.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/covered.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/even.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/feasibility.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/formal.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/immediate.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/other.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/should.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/signs.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/tendency.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/5_advanced_topics/volitional_2.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';

List<Lesson> lessons = [
  introduction,
  writing,
  hiragana,
  katakana,
  kanji,
  basic,
  stateOfBeing,
  particlesIntro,
  adjectives,
  verbs,
  negativeVerbs,
  pastTense,
  verbParticles,
  inTransitive,
  clause,
  nounParticles,
  adverbs,
  essential,
  polite,
  people,
  question,
  compound,
  teForm,
  potential,
  suruNaru,
  conditionals,
  must,
  desire,
  actionClause,
  define,
  trying,
  favors,
  requests,
  numbers,
  slang,
  sentenceEnding,
  specialExpressions,
  causePass,
  honorific,
  unintended,
  genericNoun,
  certainty,
  amount,
  similarity,
  comparison,
  easyHard,
  negativeVerbs2,
  reasoning,
  timeActions,
  noChange,
  advanced,
  formal,
  should,
  even,
  signs,
  feasibility,
  tendency,
  volitional2,
  covered,
  immediate,
  other
];
