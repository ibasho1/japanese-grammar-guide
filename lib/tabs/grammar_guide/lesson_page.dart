import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/bookmarks/bookmarks_manager.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/preferences/preferences.dart';
import 'package:japanese_grammar_guide/routes/router.gr.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson_list.dart';
import 'package:provider/provider.dart';

@RoutePage()
class LessonPage extends StatefulWidget {
  const LessonPage({Key? key, required this.lesson, this.section = 0, this.backEnabled = false})
      : super(key: key);
  final Lesson lesson;
  final int section;
  final bool backEnabled;

  @override
  State<LessonPage> createState() => _LessonPageState();
}

class _LessonPageState extends State<LessonPage> {
  bool _expanded = false;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Scrollable.ensureVisible(
            GlobalObjectKey(widget.section).currentContext!));
  }

  @override
  Widget build(BuildContext context) {
    var bookmarksProvider = Provider.of<BookmarksManager>(context);
    return Scaffold(
        backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
        appBar: AppBar(
          leading: widget.backEnabled ? null : IconButton(
            icon: const Icon(Icons.close),
            onPressed: () {
              AutoRouter.of(context).popUntilRoot();
            },
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(widget.lesson.title),
        ),
        endDrawer: Drawer(
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: (widget.lesson.sections.length + 1),
            itemBuilder: (context, index) {
              if (index == 0) {
                return DrawerHeader(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  child: Text(
                    'Contents',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.inversePrimary,
                    ),
                  ),
                );
              } else {
                return ListTile(
                  title: Text(
                    widget.lesson.sections[index - 1].heading.isNotEmpty
                        ? widget.lesson.sections[index - 1].heading
                        : 'Top',
                  ),
                  onTap: () {
                    Scrollable.ensureVisible(
                        GlobalObjectKey(index - 1).currentContext!);
                  },
                );
              }
            },
          ),
        ),
        body: SelectionArea(
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.background,
                ),
                constraints: const BoxConstraints(maxWidth: 800),
                child: Padding(
                  padding: kDefaultPagePadding,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.lesson.sections
                        .mapIndexed<Widget>((index, section) {
                      if (section.heading.isNotEmpty) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                                Padding(
                                  padding: kDefaultSectionPadding,
                                  key: GlobalObjectKey(index),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Heading(
                                          text: section.heading,
                                          level: 1,
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            Preferences.containsBookmark([
                                              lessons.indexOf(widget.lesson),
                                              index
                                            ])
                                                ? _removeBookmarkInSharedPref([
                                                    lessons
                                                        .indexOf(widget.lesson),
                                                    index
                                                  ])
                                                : _addBookmarkInSharedPref([
                                                    lessons
                                                        .indexOf(widget.lesson),
                                                    index
                                                  ]);
                                          });
                                          bookmarksProvider.updateBookmarks();
                                        },
                                        isSelected: false,
                                        icon: Preferences.containsBookmark([
                                          lessons.indexOf(widget.lesson),
                                          index
                                        ])
                                            ? const Icon(
                                                Icons.bookmark,
                                              )
                                            : const Icon(
                                                Icons.bookmark_border,
                                              ),
                                      ),
                                    ],
                                  ),
                                ),
                              ] +
                              [section.content],
                        );
                      } else {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: kDefaultSectionPadding,
                              key: GlobalObjectKey(index),
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      Preferences.containsBookmark([
                                        lessons.indexOf(widget.lesson),
                                        index
                                      ])
                                          ? _removeBookmarkInSharedPref([
                                              lessons.indexOf(widget.lesson),
                                              index
                                            ])
                                          : _addBookmarkInSharedPref([
                                              lessons.indexOf(widget.lesson),
                                              index
                                            ]);
                                    });
                                    bookmarksProvider.updateBookmarks();
                                  },
                                  isSelected: false,
                                  icon: Preferences.containsBookmark([
                                    lessons.indexOf(widget.lesson),
                                    index
                                  ])
                                      ? const Icon(
                                          Icons.bookmark,
                                        )
                                      : const Icon(
                                          Icons.bookmark_border,
                                        ),
                                ),
                              ),
                            ),
                            Container(
                              child: section.content,
                            ),
                          ],
                        );
                      }
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomSheet: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              onPressed: () {
                setState(() {
                  _expanded = !_expanded;
                });
              },
              icon: _expanded
                  ? const Icon(Icons.expand_less)
                  : const Icon(
                      Icons.expand_more,
                    ),
            ),
            AnimatedContainer(
              duration: Durations.medium1,
              height: _expanded ? 50 : 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 2,
                    child: lessons.indexOf(widget.lesson) != 0
                        ? TextButton(
                            onPressed: () {
                              scrollController.jumpTo(0);
                              _expanded = false;
                              AutoRouter.of(context).replace(
                                LessonRoute(
                                  lesson: lessons[
                                      lessons.indexOf(widget.lesson) - 1],
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Flexible(
                                  child: Icon(
                                    Icons.navigate_before,
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                      overflow: TextOverflow.ellipsis,
                                      lessons[lessons.indexOf(widget.lesson) -
                                              1]
                                          .title),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                  Expanded(
                    flex: 2,
                    child: lessons.indexOf(widget.lesson) != lessons.length - 1
                        ? TextButton(
                            onPressed: () {
                              scrollController.jumpTo(0);
                              _expanded = false;
                              AutoRouter.of(context).replace(
                                LessonRoute(
                                  lesson: lessons[
                                      lessons.indexOf(widget.lesson) + 1],
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Text(
                                    overflow: TextOverflow.ellipsis,
                                    lessons[lessons.indexOf(widget.lesson) + 1]
                                        .title,
                                  ),
                                ),
                                const Flexible(
                                  child: Icon(
                                    Icons.navigate_next,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}

Future<void> _addBookmarkInSharedPref(List<int> intList) async {
  Preferences.addBookmark(intList);
}

Future<void> _removeBookmarkInSharedPref(List<int> intList) async {
  Preferences.removeBookmark(intList);
}
