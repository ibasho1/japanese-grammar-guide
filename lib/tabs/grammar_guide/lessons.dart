import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/bookmarks/bookmarks_manager.dart';
import 'package:japanese_grammar_guide/customs/heading.dart';
import 'package:japanese_grammar_guide/routes/router.gr.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson_list.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/search/guide_search_delegate.dart';
import 'package:provider/provider.dart';

@RoutePage()
class Lessons extends StatelessWidget {
  const Lessons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bookmarks = Provider.of<BookmarksManager>(context);

    bookmarks.loadBookmarks();

    List<Chapter> chapters = [
      Chapter(
        title: 'Before you start',
        lessons: lessons.sublist(0, 1),
      ),
      Chapter(
        title: 'The Writing System',
        lessons: lessons.sublist(1, 5),
      ),
      Chapter(
        title: 'Basic Grammar',
        lessons: lessons.sublist(5, 17),
      ),
      Chapter(
        title: 'Essential Grammar',
        lessons: lessons.sublist(17, 36),
      ),
      Chapter(
        title: 'Special Expressions',
        lessons: lessons.sublist(36, 50),
      ),
      Chapter(
        title: 'Advanced Topics',
        lessons: lessons.sublist(50),
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Grammar Guide'),
        actions: [
          Builder(
            builder: (context) => IconButton(
              icon: const Icon(Icons.bookmarks),
              onPressed: () => Scaffold.of(context).openEndDrawer(),
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ),
          ),
        ],
      ),
      endDrawer: Drawer(
        child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: (bookmarks.bookmarks.length + 1),
          itemBuilder: (context, index) {
            if (index == 0) {
              return DrawerHeader(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primary,
                ),
                child: Text(
                  'Bookmarks',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.inversePrimary,
                  ),
                ),
              );
            } else {
              return ListTile(
                title: Text(
                  lessons[bookmarks.bookmarks[index - 1][0]].title,
                ),
                subtitle: Text(
                  lessons[bookmarks.bookmarks[index - 1][0]]
                          .sections[bookmarks.bookmarks[index - 1][1]]
                          .heading
                          .isEmpty
                      ? 'Top'
                      : lessons[bookmarks.bookmarks[index - 1][0]]
                          .sections[bookmarks.bookmarks[index - 1][1]]
                          .heading,
                ),
                onTap: () {
                  AutoRouter.of(context).push(LessonRoute(
                      lesson: lessons[bookmarks.bookmarks[index - 1][0]],
                      section: bookmarks.bookmarks[index - 1][1]));
                },
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showSearch(context: context, delegate: GuideSearchDelegate());
        },
        tooltip: 'Search',
        child: const Icon(Icons.search),
      ),
      body: ListView.separated(
        itemCount: 6,
        separatorBuilder: (BuildContext context, int index) => const Divider(
          height: 10,
        ),
        itemBuilder: (BuildContext context, int i) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Heading(text: chapters[i].title, level: 3),
              ),
              Column(
                children: chapters[i].lessons.map<Widget>((lesson) {
                  return ListTile(
                    title: Text(lesson.title),
                    subtitle: Text(getSubtitle(lesson.sections),
                          style: TextStyle(color: Theme.of(context).colorScheme.outline),),
                    onTap: () {
                      AutoRouter.of(context).push(LessonRoute(
                        lesson: lesson,
                      ));
                    },
                  );
                }).toList(),
              ),
            ],
          );
        },
      ),
    );
  }
}

String getSubtitle(List<Section> sections) {
  List<String> grammars = [];
  for (var section in sections) {
    grammars += section.grammars;
  }
  return grammars.join(', ');
}