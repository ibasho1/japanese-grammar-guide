import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';
import 'package:japanese_grammar_guide/routes/router.gr.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson.dart';
import 'package:japanese_grammar_guide/tabs/grammar_guide/lesson_list.dart';

class GuideSearchDelegate extends SearchDelegate {
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Get query length in words
    int queryLength = query.split(RegExp(r'\s')).length;

    // Get sections
    Map<Section, Lesson> allSections = {};
    for (var lesson in lessons) {
      for (var section in lesson.sections) {
        allSections.addEntries(<Section, Lesson>{section: lesson}.entries);
      }
    }

    // Initialise rankings
    Map<Section, int> sectionRanking = {
      for (var item in allSections.keys) item: 0
    };

    for (var section in sectionRanking.keys) {
      int factors = 0;

      // Match query against title
      if (ratio(
              query.toLowerCase(), allSections[section]!.title.toLowerCase()) >
          75) {
        if (allSections[section]?.sections.indexOf(section) == 0) {
          sectionRanking.update(
              section,
              (value) => ratio(query.toLowerCase(),
                  allSections[section]!.title.toLowerCase()));
          factors += 1;
        }
      }

      // Match query against heading
      if (section.heading.isNotEmpty) {
        if (ratio(query.toLowerCase(), section.heading.toLowerCase()) > 75) {
          sectionRanking.update(
              section,
              (value) => value +=
                  ratio(query.toLowerCase(), section.heading.toLowerCase()));
          factors += 1;
        }
      }

      // Match query against grammars and keywords if short
      if (queryLength <= 5) {
        for (var grammar in section.grammars) {
          if (ratio(query.toLowerCase(), grammar.toLowerCase()) > 75) {
            sectionRanking.update(
                section,
                (value) =>
                    value += ratio(query.toLowerCase(), grammar.toLowerCase()));
            factors += 1;
          }
        }
        for (var keyword in section.keywords) {
          if (ratio(query.toLowerCase(), keyword.toLowerCase()) > 75) {
            sectionRanking.update(
                section,
                (value) =>
                    value += ratio(query.toLowerCase(), keyword.toLowerCase()));
            factors += 1;
          }
        }
      }

      // Match query against plaintext if medium-long
      // or if there is little match with the above factors
      if (queryLength >= 4 || factors <= 2) {
        // Weight result to rank higher the longer the query until 4 words
        double weight = queryLength >= 4 ? 1 : (0.25 * queryLength);
        sectionRanking.update(
            section,
            (value) => value += (partialRatio(
                        query.toLowerCase(), section.plaintext.toLowerCase()) *
                    weight)
                .round());
        factors += 1;
      }

      // Weight final ranking depending on the number of factors
      sectionRanking.update(section, (value) => value *= (6 - factors));
    }

    var results = Map.fromEntries(sectionRanking.entries.toList()
      ..sort((e1, e2) => e2.value.compareTo(e1.value)));

    return ListView.builder(
      itemCount: (10),
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            allSections[results.keys.elementAt(index)]!.title,
          ),
          subtitle: Text(
            results.keys.elementAt(index).heading.isNotEmpty
                ? results.keys.elementAt(index).heading
                : 'Top',
          ),
          onTap: () {
            AutoRouter.of(context).push(LessonRoute(
              lesson: allSections[results.keys.elementAt(index)]!,
              section: allSections[results.keys.elementAt(index)]!
                  .sections
                  .indexOf(results.keys.elementAt(index)),
              backEnabled: true,
            ));
          },
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return ListView();
  }

  Lesson getLesson(Section section) {
    Lesson sectionLesson = lessons[0];
    for (var lesson in lessons) {
      if (lesson.sections.contains(section)) {
        sectionLesson = lesson;
      }
    }
    return sectionLesson;
  }
}
