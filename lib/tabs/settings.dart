import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/constants/constants.dart';
import 'package:japanese_grammar_guide/preferences/preferences.dart';
import 'package:japanese_grammar_guide/theme/theme_mode_manager.dart';
import 'package:provider/provider.dart';

class Setting {
  String label;
  Column list;
  Setting({
    required this.label,
    required this.list,
  });
}

enum ThemeSetting { system, light, dark }

@RoutePage()
class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var themeProvider = Provider.of<ThemeModeManager>(context);

    String? theme = Preferences.getTheme();

    List<Setting> settings = [
      Setting(
        label: 'Theme',
        list: Column(
          children: [
            ListTile(
              title: const Text('System'),
              leading: Radio<String>(
                value: 'system',
                groupValue: theme,
                onChanged: (String? value) {
                  theme = value;
                  themeProvider.changeTheme(value);
                  _setThemeInSharedPref(value);
                },
              ),
            ),
            ListTile(
              title: const Text('Light'),
              leading: Radio<String>(
                value: 'light',
                groupValue: theme,
                onChanged: (String? value) {
                  theme = value;
                  themeProvider.changeTheme(value);
                  _setThemeInSharedPref(value);
                },
              ),
            ),
            ListTile(
              title: const Text('Dark'),
              leading: Radio<String>(
                value: 'dark',
                groupValue: theme,
                onChanged: (String? value) {
                  theme = value;
                  themeProvider.changeTheme(value);
                  _setThemeInSharedPref(value);
                },
              ),
            ),
          ],
        ),
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Settings'),
      ),
      body: Padding(
        padding: kDefaultPagePadding,
        child: ListView.separated(
          itemCount: 1,
          separatorBuilder: (BuildContext context, int index) => const Divider(
            height: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15, left: 10),
                  child: Text(
                    settings[index].label,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                settings[index].list
              ],
            );
          },
        ),
      ),
    );
  }

  Future<void> _setThemeInSharedPref(String? string) async {
    if (string != null) {
      Preferences.setTheme(string);
    }
  }
}
