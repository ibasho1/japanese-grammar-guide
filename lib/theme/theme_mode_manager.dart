import 'package:flutter/material.dart';
import 'package:japanese_grammar_guide/preferences/preferences.dart';

class ThemeModeManager with ChangeNotifier{
  ThemeMode _themeMode;

  ThemeModeManager(this._themeMode);

  get themeMode => _themeMode;

  loadTheme() {
    _themeMode = Preferences.getThemeMode();
  }

  changeTheme(String? theme) {
    if (theme == 'light') {
      _themeMode = ThemeMode.light;
    }
    else if (theme == 'dark') {
      _themeMode = ThemeMode.dark;
    }
    else {
      _themeMode = ThemeMode.system;
    }
    notifyListeners();
  }
}