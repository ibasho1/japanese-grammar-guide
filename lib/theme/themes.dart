import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  colorSchemeSeed: const Color(0xffbe0029),
  bottomSheetTheme: BottomSheetThemeData(
    surfaceTintColor: Colors.white,
    backgroundColor: Colors.white.withAlpha(150),
  ),
  useMaterial3: true,
);

ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  colorSchemeSeed: const Color(0xffbe0029),
  // dividerColor: Colors.yellow,
  bottomSheetTheme: BottomSheetThemeData(
    surfaceTintColor: Colors.black,
    backgroundColor: Colors.black.withAlpha(150),
  ),
  useMaterial3: true,
);
